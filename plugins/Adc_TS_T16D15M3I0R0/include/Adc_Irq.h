
#ifndef ADC_IRQ_H
#define ADC_IRQ_H

/*******************************************************************************
**                      Includes                                              **
*******************************************************************************/

#include "Std_Types.h"                                                        

#ifdef OS_KERNEL_TYPE                                                           
#include <Os.h>        /* OS interrupt services */                              
#endif


/*******************************************************************************
**                      Public Macro Definitions                              **
*******************************************************************************/

/************************* interrupt CATEGORY *********************************/

#ifdef IRQ_CAT1
#if (IRQ_CAT1 != 1)
#error IRQ_CAT1 already defined with a wrong value
#endif
#else
#define IRQ_CAT1                    (1)
#endif

#ifdef IRQ_CAT23
#if (IRQ_CAT23 != 2)
#error IRQ_CAT23 already defined with a wrong value
#endif
#else
#define IRQ_CAT23                    (2)
#endif



/* The name of the ISRs shall be the same name than the ISR       *
 * functions provided by Infineon                                 */

/*************************ADC0SR0_ISR*********************************/          

#ifdef ADC0SR0_ISR
#define IRQ_ADC0SR0_EXIST      STD_ON
#define IRQ_ADC0_SR0_PRIO      ADC0SR0_ISR_ISR_LEVEL
#define IRQ_ADC0_SR0_CAT       ADC0SR0_ISR_ISR_CATEGORY
#else
#define IRQ_ADC0SR0_EXIST      STD_OFF
#endif

/*************************ADC0SR1_ISR*********************************/          

#ifdef ADC0SR1_ISR
#define IRQ_ADC0SR1_EXIST      STD_ON
#define IRQ_ADC0_SR1_PRIO      ADC0SR1_ISR_ISR_LEVEL
#define IRQ_ADC0_SR1_CAT       ADC0SR1_ISR_ISR_CATEGORY
#else
#define IRQ_ADC0SR1_EXIST      STD_OFF
#endif

/*************************ADC0SR2_ISR*********************************/          

#ifdef ADC0SR2_ISR
#define IRQ_ADC0SR2_EXIST      STD_ON
#define IRQ_ADC0_SR2_PRIO      ADC0SR2_ISR_ISR_LEVEL
#define IRQ_ADC0_SR2_CAT       ADC0SR2_ISR_ISR_CATEGORY
#else
#define IRQ_ADC0SR2_EXIST      STD_OFF
#endif

/*************************ADC1SR0_ISR*********************************/          

#ifdef ADC1SR0_ISR
#define IRQ_ADC1SR0_EXIST      STD_ON
#define IRQ_ADC1_SR0_PRIO      ADC1SR0_ISR_ISR_LEVEL
#define IRQ_ADC1_SR0_CAT       ADC1SR0_ISR_ISR_CATEGORY
#else
#define IRQ_ADC1SR0_EXIST      STD_OFF
#endif

/*************************ADC1SR1_ISR*********************************/          

#ifdef ADC1SR1_ISR
#define IRQ_ADC1SR1_EXIST      STD_ON
#define IRQ_ADC1_SR1_PRIO      ADC1SR1_ISR_ISR_LEVEL
#define IRQ_ADC1_SR1_CAT       ADC1SR1_ISR_ISR_CATEGORY
#else
#define IRQ_ADC1SR1_EXIST      STD_OFF
#endif

/*************************ADC1SR2_ISR*********************************/          

#ifdef ADC1SR2_ISR
#define IRQ_ADC1SR2_EXIST      STD_ON
#define IRQ_ADC1_SR2_PRIO      ADC1SR2_ISR_ISR_LEVEL
#define IRQ_ADC1_SR2_CAT       ADC1SR2_ISR_ISR_CATEGORY
#else
#define IRQ_ADC1SR2_EXIST      STD_OFF
#endif

/*************************ADC2SR0_ISR*********************************/          

#ifdef ADC2SR0_ISR
#define IRQ_ADC2SR0_EXIST      STD_ON
#define IRQ_ADC2_SR0_PRIO      ADC2SR0_ISR_ISR_LEVEL
#define IRQ_ADC2_SR0_CAT       ADC2SR0_ISR_ISR_CATEGORY
#else
#define IRQ_ADC2SR0_EXIST      STD_OFF
#endif

/*************************ADC2SR1_ISR*********************************/          

#ifdef ADC2SR1_ISR
#define IRQ_ADC2SR1_EXIST      STD_ON 
#define IRQ_ADC2_SR1_PRIO      ADC2SR1_ISR_ISR_LEVEL
#define IRQ_ADC2_SR1_CAT       ADC2SR1_ISR_ISR_CATEGORY
#else
#define IRQ_ADC2SR1_EXIST      STD_OFF
#endif

/*************************ADC2SR2_ISR*********************************/          

#ifdef ADC2SR2_ISR
#define IRQ_ADC2SR2_EXIST      STD_ON
#define IRQ_ADC2_SR2_PRIO      ADC2SR2_ISR_ISR_LEVEL
#define IRQ_ADC2_SR2_CAT       ADC2SR2_ISR_ISR_CATEGORY
#else
#define IRQ_ADC2SR2_EXIST      STD_OFF
#endif

/*************************ADC3SR0_ISR*********************************/          

#ifdef ADC3SR0_ISR
#define IRQ_ADC3SR0_EXIST      STD_ON
#define IRQ_ADC3_SR0_PRIO      ADC3SR0_ISR_ISR_LEVEL
#define IRQ_ADC3_SR0_CAT       ADC3SR0_ISR_ISR_CATEGORY
#else
#define IRQ_ADC3SR0_EXIST      STD_OFF
#endif

/*************************ADC3SR1_ISR*********************************/          

#ifdef ADC3SR1_ISR
#define IRQ_ADC3SR1_EXIST      STD_ON
#define IRQ_ADC3_SR1_PRIO      ADC3SR1_ISR_ISR_LEVEL
#define IRQ_ADC3_SR1_CAT       ADC3SR1_ISR_ISR_CATEGORY
#else
#define IRQ_ADC3SR1_EXIST      STD_OFF
#endif

/*************************ADC3SR2_ISR*********************************/          

#ifdef ADC3SR2_ISR
#define IRQ_ADC3SR2_EXIST      STD_ON
#define IRQ_ADC3_SR2_PRIO      ADC3SR2_ISR_ISR_LEVEL
#define IRQ_ADC3_SR2_CAT       ADC3SR2_ISR_ISR_CATEGORY
#else
#define IRQ_ADC3SR2_EXIST      STD_OFF
#endif

/*************************ADC4SR0_ISR*********************************/          

#ifdef ADC4SR0_ISR
#define IRQ_ADC4SR0_EXIST      STD_ON
#define IRQ_ADC4_SR0_PRIO      ADC4SR0_ISR_ISR_LEVEL
#define IRQ_ADC4_SR0_CAT       ADC4SR0_ISR_ISR_CATEGORY
#else
#define IRQ_ADC4SR0_EXIST      STD_OFF
#endif

/*************************ADC4SR1_ISR*********************************/          

#ifdef ADC4SR1_ISR
#define IRQ_ADC4SR1_EXIST      STD_ON
#define IRQ_ADC4_SR1_PRIO      ADC4SR1_ISR_ISR_LEVEL
#define IRQ_ADC4_SR1_CAT       ADC4SR1_ISR_ISR_CATEGORY
#else
#define IRQ_ADC4SR1_EXIST      STD_OFF
#endif

/*************************ADC4SR2_ISR*********************************/          

#ifdef ADC4SR2_ISR
#define IRQ_ADC4SR2_EXIST      STD_ON
#define IRQ_ADC4_SR2_PRIO      ADC4SR2_ISR_ISR_LEVEL
#define IRQ_ADC4_SR2_CAT       ADC4SR2_ISR_ISR_CATEGORY
#else
#define IRQ_ADC4SR2_EXIST      STD_OFF
#endif

/*************************ADC5SR0_ISR*********************************/          

#ifdef ADC5SR0_ISR
#define IRQ_ADC5SR0_EXIST      STD_ON
#define IRQ_ADC5_SR0_PRIO      ADC5SR0_ISR_ISR_LEVEL
#define IRQ_ADC5_SR0_CAT       ADC5SR0_ISR_ISR_CATEGORY
#else
#define IRQ_ADC5SR0_EXIST      STD_OFF
#endif

/*************************ADC5SR1_ISR*********************************/          

#ifdef ADC5SR1_ISR
#define IRQ_ADC5SR1_EXIST      STD_ON
#define IRQ_ADC5_SR1_PRIO      ADC5SR1_ISR_ISR_LEVEL
#define IRQ_ADC5_SR1_CAT       ADC5SR1_ISR_ISR_CATEGORY
#else
#define IRQ_ADC5SR1_EXIST      STD_OFF
#endif

/*************************ADC5SR2_ISR*********************************/          

#ifdef ADC5SR2_ISR
#define IRQ_ADC5SR2_EXIST      STD_ON
#define IRQ_ADC5_SR2_PRIO      ADC5SR2_ISR_ISR_LEVEL
#define IRQ_ADC5_SR2_CAT       ADC5SR2_ISR_ISR_CATEGORY
#else
#define IRQ_ADC5SR2_EXIST      STD_OFF
#endif

/*************************ADC6SR0_ISR*********************************/          

#ifdef ADC6SR0_ISR
#define IRQ_ADC6SR0_EXIST      STD_ON
#define IRQ_ADC6_SR0_PRIO      ADC6SR0_ISR_ISR_LEVEL
#define IRQ_ADC6_SR0_CAT       ADC6SR0_ISR_ISR_CATEGORY
#else
#define IRQ_ADC6SR0_EXIST      STD_OFF
#endif

/*************************ADC6SR1_ISR*********************************/          

#ifdef ADC6SR1_ISR
#define IRQ_ADC6SR1_EXIST      STD_ON
#define IRQ_ADC6_SR1_PRIO      ADC6SR1_ISR_ISR_LEVEL
#define IRQ_ADC6_SR1_CAT       ADC6SR1_ISR_ISR_CATEGORY
#else
#define IRQ_ADC6SR1_EXIST      STD_OFF
#endif

/*************************ADC6SR2_ISR*********************************/          

#ifdef ADC6SR2_ISR
#define IRQ_ADC6SR2_EXIST      STD_ON
#define IRQ_ADC6_SR2_PRIO      ADC6SR2_ISR_ISR_LEVEL
#define IRQ_ADC6_SR2_CAT       ADC6SR2_ISR_ISR_CATEGORY
#else
#define IRQ_ADC6SR2_EXIST      STD_OFF
#endif

/*************************ADC7SR0_ISR*********************************/          

#ifdef ADC7SR0_ISR
#define IRQ_ADC7SR0_EXIST      STD_ON
#define IRQ_ADC7_SR0_PRIO      ADC7SR0_ISR_ISR_LEVEL
#define IRQ_ADC7_SR0_CAT       ADC7SR0_ISR_ISR_CATEGORY
#else
#define IRQ_ADC7SR0_EXIST      STD_OFF
#endif

/*************************ADC7SR1_ISR*********************************/          

#ifdef ADC7SR1_ISR
#define IRQ_ADC7SR1_EXIST      STD_ON
#define IRQ_ADC7_SR1_PRIO      ADC7SR1_ISR_ISR_LEVEL
#define IRQ_ADC7_SR1_CAT       ADC7SR1_ISR_ISR_CATEGORY
#else
#define IRQ_ADC7SR1_EXIST      STD_OFF
#endif

/*************************ADC7SR2_ISR*********************************/          

#ifdef ADC7SR2_ISR
#define IRQ_ADC7SR2_EXIST      STD_ON
#define IRQ_ADC7_SR2_PRIO      ADC7SR2_ISR_ISR_LEVEL
#define IRQ_ADC7_SR2_CAT       ADC7SR2_ISR_ISR_CATEGORY
#else
#define IRQ_ADC7SR2_EXIST      STD_OFF
#endif

/*************************ADC8SR0_ISR*********************************/          

#ifdef ADC8SR0_ISR
#define IRQ_ADC8SR0_EXIST      STD_ON
#define IRQ_ADC8_SR0_PRIO      ADC8SR0_ISR_ISR_LEVEL
#define IRQ_ADC8_SR0_CAT       ADC8SR0_ISR_ISR_CATEGORY
#else
#define IRQ_ADC8SR0_EXIST      STD_OFF
#endif

/*************************ADC8SR1_ISR*********************************/          

#ifdef ADC8SR1_ISR
#define IRQ_ADC8SR1_EXIST      STD_ON
#define IRQ_ADC8_SR1_PRIO      ADC8SR1_ISR_ISR_LEVEL
#define IRQ_ADC8_SR1_CAT       ADC8SR1_ISR_ISR_CATEGORY
#else
#define IRQ_ADC8SR1_EXIST      STD_OFF
#endif

/*************************ADC8SR2_ISR*********************************/          

#ifdef ADC8SR2_ISR
#define IRQ_ADC8SR2_EXIST      STD_ON
#define IRQ_ADC8_SR2_PRIO      ADC8SR2_ISR_ISR_LEVEL
#define IRQ_ADC8_SR2_CAT       ADC8SR2_ISR_ISR_CATEGORY
#else
#define IRQ_ADC8SR2_EXIST      STD_OFF
#endif

/*************************ADC9SR0_ISR*********************************/          

#ifdef ADC9SR0_ISR
#define IRQ_ADC9SR0_EXIST      STD_ON
#define IRQ_ADC9_SR0_PRIO      ADC9SR0_ISR_ISR_LEVEL
#define IRQ_ADC9_SR0_CAT       ADC9SR0_ISR_ISR_CATEGORY
#else
#define IRQ_ADC9SR0_EXIST      STD_OFF
#endif

/*************************ADC9SR1_ISR*********************************/          

#ifdef ADC9SR1_ISR
#define IRQ_ADC9SR1_EXIST      STD_ON
#define IRQ_ADC9_SR1_PRIO      ADC9SR1_ISR_ISR_LEVEL
#define IRQ_ADC9_SR1_CAT       ADC9SR1_ISR_ISR_CATEGORY
#else
#define IRQ_ADC9SR1_EXIST      STD_OFF
#endif

/*************************ADC9SR2_ISR*********************************/          

#ifdef ADC9SR2_ISR
#define IRQ_ADC9SR2_EXIST      STD_ON
#define IRQ_ADC9_SR2_PRIO      ADC9SR2_ISR_ISR_LEVEL
#define IRQ_ADC9_SR2_CAT       ADC9SR2_ISR_ISR_CATEGORY
#else
#define IRQ_ADC9SR2_EXIST      STD_OFF
#endif

/*************************ADC10SR0_ISR*********************************/          

#ifdef ADC10SR0_ISR
#define IRQ_ADC10SR0_EXIST      STD_ON
#define IRQ_ADC10_SR0_PRIO      ADC10SR0_ISR_ISR_LEVEL
#define IRQ_ADC10_SR0_CAT       ADC10SR0_ISR_ISR_CATEGORY
#else
#define IRQ_ADC10SR0_EXIST      STD_OFF
#endif

/*************************ADC10SR1_ISR*********************************/          

#ifdef ADC10SR1_ISR
#define IRQ_ADC10SR1_EXIST      STD_ON
#define IRQ_ADC10_SR1_PRIO      ADC10SR1_ISR_ISR_LEVEL
#define IRQ_ADC10_SR1_CAT       ADC10SR1_ISR_ISR_CATEGORY
#else
#define IRQ_ADC10SR1_EXIST      STD_OFF
#endif

/*************************ADC10SR2_ISR*********************************/          

#ifdef ADC10SR2_ISR
#define IRQ_ADC10SR2_EXIST      STD_ON
#define IRQ_ADC10_SR2_PRIO      ADC10SR2_ISR_ISR_LEVEL
#define IRQ_ADC10_SR2_CAT       ADC10SR2_ISR_ISR_CATEGORY
#else
#define IRQ_ADC10SR2_EXIST      STD_OFF
#endif

/*************************ADCCG0SR0_ISR*********************************/          

#ifdef ADCCG0SR0_ISR
#define IRQ_ADCCG0SRO_EXIST      STD_ON
#define IRQ_ADCCG0_SR0_PRIO      ADCCG0SR0_ISR_ISR_LEVEL
#define IRQ_ADCCG0_SR0_CAT       ADCCG0SR0_ISR_ISR_CATEGORY
#else
#define IRQ_ADCCG0SRO_EXIST      STD_OFF
#endif

/*************************ADCCG0SR1_ISR*********************************/          

#ifdef ADCCG0SR1_ISR
#define IRQ_ADCCG0SR1_EXIST      STD_ON
#define IRQ_ADCCG0_SR1_PRIO      ADCCG0SR1_ISR_ISR_LEVEL
#define IRQ_ADCCG0_SR1_CAT       ADCCG0SR1_ISR_ISR_CATEGORY
#else
#define IRQ_ADCCG0SR1_EXIST      STD_OFF
#endif

/*************************ADCCG0SR2_ISR*********************************/          

#ifdef ADCCG0SR2_ISR
#define IRQ_ADCCG0SR2_EXIST      STD_ON
#define IRQ_ADCCG0_SR2_PRIO      ADCCG0SR2_ISR_ISR_LEVEL
#define IRQ_ADCCG0_SR2_CAT       ADCCG0SR2_ISR_ISR_CATEGORY
#else
#define IRQ_ADCCG0SR2_EXIST      STD_OFF
#endif


#endif /* #ifndef ADC_IRQ_H */

