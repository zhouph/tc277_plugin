/**
 * \file
 *
 * \brief AUTOSAR WdgM
 *
 * This file contains the implementation of the AUTOSAR
 * module WdgM.
 *
 * \author Elektrobit Automotive GmbH, 91058 Erlangen, Germany
 *
 * Copyright 2005 - 2013 Elektrobit Automotive GmbH
 * All rights exclusively reserved for Elektrobit Automotive GmbH,
 * unless expressly agreed to otherwise.
 */

/*
 * Misra-C:2004 Deviations:
 *
 * MISRA-1) Deviated Rule: 19.6 (required)
 * #undef shall not be used
 *
 * Reason:
 * The macro names used for memory mapping are different between
 * AUTOSAR 3.x and AUTOSAR 4.x. For sake of compatibility between
 * these worlds, both macros are defined.
 * Since only one of them will be undefined within the MemMap.h file,
 * all macros are undefined after the memory section ends.
 * This avoids possible redundant macro definitions.
 */

#if (!defined WDGM_LCFG_H)
#define WDGM_LCFG_H

/*==================[inclusions]=================================================================*/

/* !LINKSTO WDGM.EB.ASR32.WDGM024,1 */
#include <WdgM_BSW_Lcfg.h>                                 /* this module's version declarations */

/*==================[macros]=====================================================================*/

/*==================[type definitions]===========================================================*/

/*==================[external function declarations]=============================================*/

/*==================[external constants]=========================================================*/

/*==================[external data]==============================================================*/

/* !LINKSTO WDGM.EB.TIMEPM.WDGM020304,1 */
#if (WDGM_EB_PARTITIONING_ENABLED == STD_ON)
#define WDGM_START_SEC_SHARED_VAR_NO_INIT_UNSPECIFIED
#else
#define WDGM_START_SEC_VAR_NOINIT_UNSPECIFIED
#define WDGM_START_SEC_VAR_NO_INIT_UNSPECIFIED
#endif
#include <MemMap.h>

/* !LINKSTO WDGM.EB.DesignDecision.InternalVariable3,1 */
/* !LINKSTO WDGM.EB.DesignDecision.InternalVariable5,1 */
/* !LINKSTO WDGM.EB.DesignDecision.InternalVariable8,1 */
[!LOOP "WdgMGeneral/WdgMSupervisedEntity/*"!][!//
/* Checkpoint individual run-time data for each SE */
extern VAR(WdgM_EB_CPDataType,WDGM_VAR_NOINIT) WdgM_EB_CPData_[!"@name"!][[!"num:i(count(WdgMCheckpoint/*))"!]];
[!ENDLOOP!][!//

/* !LINKSTO WDGM.EB.TIMEPM.WDGM020304,1 */
#if (WDGM_EB_PARTITIONING_ENABLED == STD_ON)
#define WDGM_STOP_SEC_SHARED_VAR_NO_INIT_UNSPECIFIED
#else
#define WDGM_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#define WDGM_STOP_SEC_VAR_NO_INIT_UNSPECIFIED
#endif
#include <MemMap.h>
/* Deviation MISRA-1 <+4> */
#undef WDGM_START_SEC_VAR_NOINIT_UNSPECIFIED
#undef WDGM_START_SEC_VAR_NO_INIT_UNSPECIFIED
#undef WDGM_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#undef WDGM_STOP_SEC_VAR_NO_INIT_UNSPECIFIED

/*==================[external function definitions]==============================================*/

#endif /* if !defined( WDGM_LCFG_H ) */
/*==================[end of file]================================================================*/
