/**
 * \file
 *
 * \brief AUTOSAR WdgM
 *
 * This file contains the implementation of the AUTOSAR
 * module WdgM.
 *
 * \author Elektrobit Automotive GmbH, 91058 Erlangen, Germany
 *
 * Copyright 2005 - 2013 Elektrobit Automotive GmbH
 * All rights exclusively reserved for Elektrobit Automotive GmbH,
 * unless expressly agreed to otherwise.
 */
[!INCLUDE "WdgM_Cfg.m"!][!//
[!CODE!][!//
[!AUTOSPACING!][!//
#if (!defined WDGM_RTE_LCFG_H)
#define WDGM_RTE_LCFG_H

[!IF "$RteUsageEnabled"!][!//
/*==================[inclusions]=================================================================*/

/* !LINKSTO WDGM.EB.ASR32.WDGM025,1 */
#include <WdgM_Cfg.h>                                  /* included precompile time configuration */
#include <Rte_WdgM.h>

/*==================[macros]=====================================================================*/

/*==================[type definitions]===========================================================*/

[!IF "$DefaultServiceAPI != 'NONE'"!][!//
/** \brief Default function pointer type for mode switch request functions of the
 * RTE */
typedef P2FUNC(Std_ReturnType, RTE_APPL_CODE, WdgM_RteSwitchFunc)(
    Rte_ModeType_WdgMMode mode);
/** \brief Default function pointer type for mode query functions of the RTE */
typedef P2FUNC(Rte_ModeType_WdgMMode, RTE_APPL_CODE, WdgM_RteModeFunc)(void);
[!ENDIF!][!//
[!IF "$ASR32ServiceAPIEnabled"!][!//

 /** \brief AUTOSAR 3.2 function pointer type for mode switch request functions of the
 * RTE */
typedef P2FUNC(Std_ReturnType, RTE_APPL_CODE, WdgM_ASR32_RteSwitchFunc)(
    Rte_ModeType_WdgM_ASR32_Mode mode);
/** \brief AUTOSAR 3.2 function pointer type for mode query functions of the RTE */
typedef P2FUNC(Rte_ModeType_WdgM_ASR32_Mode, RTE_APPL_CODE, WdgM_ASR32_RteModeFunc)(void);
[!ENDIF!][!//
[!IF "$ASR40ServiceAPIEnabled"!][!//

/** \brief AUTOSAR 4.0 function pointer type for mode switch request functions of the
 * RTE */
typedef P2FUNC(Std_ReturnType, RTE_APPL_CODE, WdgM_ASR40_RteSwitchFunc)(
    Rte_ModeType_WdgM_ASR40_Mode mode);
/** \brief AUTOSAR 4.0 function pointer type for mode query functions of the RTE */
typedef P2FUNC(Rte_ModeType_WdgM_ASR40_Mode, RTE_APPL_CODE, WdgM_ASR40_RteModeFunc)(void);
[!ENDIF!][!//

/*==================[external function declarations]=============================================*/

/*==================[external constants]=========================================================*/

#define WDGM_START_SEC_CONST_UNSPECIFIED
#include <MemMap.h>
[!IF "$DefaultServiceAPI != 'NONE'"!][!//

 /** \brief Function pointer array holding the mode switch request functions of
 * the RTE for each supervised entity */
extern CONST(WdgM_RteSwitchFunc, WDGM_CONST) WdgM_RteSwitch[WDGM_EB_SE_NUM];
/** \brief Function pointer array holding the mode query functions of the RTE
 * for each supervised entity */
extern CONST(WdgM_RteModeFunc, WDGM_CONST)   WdgM_RteMode[WDGM_EB_SE_NUM];
[!ENDIF!][!//
[!IF "$ASR32ServiceAPIEnabled"!][!//

/** \brief Function pointer array holding the mode switch request functions of
 * the RTE in AUTOSAR 3.2 for each supervised entity */
extern CONST(WdgM_ASR32_RteSwitchFunc, WDGM_CONST) WdgM_ASR32_RteSwitch[WDGM_EB_SE_NUM];
/** \brief Function pointer array holding the mode query functions of the RTE
 * in AUTOSAR 3.2 for each supervised entity */
extern CONST(WdgM_ASR32_RteModeFunc, WDGM_CONST)   WdgM_ASR32_RteMode[WDGM_EB_SE_NUM];
[!ENDIF!][!//
[!IF "$ASR40ServiceAPIEnabled"!][!//

/** \brief Function pointer array holding the mode switch request functions of
 * the RTE in AUTOSAR 4.0 for each supervised entity */
extern CONST(WdgM_ASR40_RteSwitchFunc, WDGM_CONST) WdgM_ASR40_RteSwitch[WDGM_EB_SE_NUM];
/** \brief Function pointer array holding the mode query functions of the RTE
 * in AUTOSAR 4.0 for each supervised entity */
extern CONST(WdgM_ASR40_RteModeFunc, WDGM_CONST)   WdgM_ASR40_RteMode[WDGM_EB_SE_NUM];
[!ENDIF!][!//

#define WDGM_STOP_SEC_CONST_UNSPECIFIED
#include <MemMap.h>

/*==================[external data]==============================================================*/

[!ENDIF!][!//
#endif /* if !defined( WDGM_RTE_LCFG_H ) */
/*==================[end of file]================================================================*/
[!ENDCODE!][!//
