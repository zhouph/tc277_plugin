/**
 * \file
 *
 * \brief AUTOSAR WdgM
 *
 * This file contains the implementation of the AUTOSAR
 * module WdgM.
 *
 * \author Elektrobit Automotive GmbH, 91058 Erlangen, Germany
 *
 * Copyright 2005 - 2013 Elektrobit Automotive GmbH
 * All rights exclusively reserved for Elektrobit Automotive GmbH,
 * unless expressly agreed to otherwise.
 */

/*
 * Misra-C:2004 Deviations:
 *
 * MISRA-1) Deviated Rule: 5.3 (required)
 * A typedef name shall be a unique identifier.
 *
 * Reason:
 * The typedefs WdgM_ASR32_SupervisedEntityIdType, WdgM_ASR40_SupervisedEntityIdType, and
 * WdgM_ASR40_CheckpointIdType are provided by the WdgM if used from
 * other BSW modules without the usage of an Rte. In case the Rte is used,
 * then the generated WdgM Service Component code contains the same typedefs
 * which may be included by the related Software Components or other Service Components.
 * Note: Name clashes are avoided by checking if typedef is already defined in the Rte.
 */
#if (!defined WDGM_BSW_TYPES_H)
#define WDGM_BSW_TYPES_H

/*==================[inclusions]=================================================================*/

#include <Std_Types.h>                                                         /* standard types */

/* !LINKSTO WDGM.EB.ASR32.WDGM019,1 */
#include <WdgM_Cfg.h>                                      /* this module's version declarations */

/*==================[macros]=====================================================================*/

/*==================[type definitions]===========================================================*/

/* !LINKSTO WDGM.EB.ASR32.WDGM119,1 */
#ifndef RTE_TYPE_WdgM_ASR32_SupervisedEntityIdType
#define RTE_TYPE_WdgM_ASR32_SupervisedEntityIdType
/* !LINKSTO WDGM.EB.ASR32.WDGM104,1 */
/** \brief Supervised entity id
 *
 * This type identifies an individual Supervised Entity for the Watchdog
 * Manager. */
/* Deviation MISRA-1 */
typedef uint16 WdgM_ASR32_SupervisedEntityIdType;
#endif

/* !LINKSTO WDGM.EB.ASR32.WDGM120,1 */
#ifndef RTE_TYPE_WdgM_ASR40_SupervisedEntityIdType
#define RTE_TYPE_WdgM_ASR40_SupervisedEntityIdType
/* !LINKSTO WDGM.EB.ASR32.WDGM103,1 */
/** \brief Supervised entity id
 *
 * This type identifies an individual Supervised Entity for the Watchdog
 * Manager. */
/* Deviation MISRA-1 */
typedef uint16 WdgM_ASR40_SupervisedEntityIdType;
#endif

/* !LINKSTO WDGM.EB.ASR32.WDGM121,1 */
#ifndef RTE_TYPE_WdgM_ASR40_CheckpointIdType
#define RTE_TYPE_WdgM_ASR40_CheckpointIdType
/* !LINKSTO WDGM.EB.ASR32.WDGM103,1 */
/** \brief Checkpoint entity id
 *
 * This type identifies an individual checkpoint entity of a supervised entity
 * for the Watchdog Manager. */
/* Deviation MISRA-1 */
typedef uint16 WdgM_ASR40_CheckpointIdType;
#endif

/** \brief Mode id
 *
 * This type distinguishes the different modes that were configured for the
 * Watchdog Manager. */
typedef uint8 WdgM_ModeType;

/** \brief Local status
 *
 * This type is used to represent the local supervision status of the
 * individual supervised entities.
 */
typedef uint8 WdgM_LocalStatusType;

/** \brief Global status
 *
 * This type is used to represent the global supervision status
 * of the Watchdog Manager. */
typedef uint8 WdgM_GlobalStatusType;

/** \brief configuration of WdgM
 *
 * This is a dummy structure to hold configuration parameters.  As the WdgM is
 * not able to be link or post build time configurable this structure is not
 * used.  It is defined for compatibility only.  A pointer to this structure
 * is passed to the Watchdog Manager initialization function for
 * configuration. */
typedef uint8 WdgM_ConfigType;

/** \brief Init status
 *
 * This type is used to define the expected / actual initialization status
 * The two possible states are:
 * WDGM_EB_INIT_STATUS_INIT ..... WdgM is / shall be initialized
 * WDGM_EB_INIT_STATUS_DEINIT ... WdgM is / shall be de-initialized
 */
typedef uint8 WdgM_EB_InitStatusType;

/** \brief holds SE specific data that lies in the WdgM itself */
typedef struct
{
  uint16 FailedAliveSupervisionRefCycleCnt; /**< failed reference cycle counter for the SE */
  /* !LINKSTO WDGM.ASR40.WDGM200,1 */
  /* !LINKSTO WDGM.EB.DesignDecision.InternalVariable1,1 */
  WdgM_LocalStatusType LocalStatus; /**< local status of this SE */
} WdgM_EB_SEWDataType;

/*==================[external function declarations]=============================================*/

/*==================[internal function declarations]=============================================*/

/*==================[external constants]=========================================================*/

/*==================[internal constants]=========================================================*/

/*==================[external data]==============================================================*/

/*==================[internal data]==============================================================*/

/*==================[external function definitions]==============================================*/

/*==================[internal function definitions]==============================================*/

#endif /* if !defined( WDGM_BSW_TYPES_H ) */
/*==================[end of file]================================================================*/
