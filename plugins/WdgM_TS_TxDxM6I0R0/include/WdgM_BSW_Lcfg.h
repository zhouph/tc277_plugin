/**
 * \file
 *
 * \brief AUTOSAR WdgM
 *
 * This file contains the implementation of the AUTOSAR
 * module WdgM.
 *
 * \author Elektrobit Automotive GmbH, 91058 Erlangen, Germany
 *
 * Copyright 2005 - 2013 Elektrobit Automotive GmbH
 * All rights exclusively reserved for Elektrobit Automotive GmbH,
 * unless expressly agreed to otherwise.
 */

/*
 * Misra-C:2004 Deviations:
 *
 * MISRA-1) Deviated Rule: 19.6 (required)
 * #undef shall not be used
 *
 * Reason:
 * The macro names used for memory mapping are different between
 * AUTOSAR 3.x and AUTOSAR 4.x. For sake of compatibility between
 * these worlds, both macros are defined.
 * Since only one of them will be undefined within the MemMap.h file,
 * all macros are undefined after the memory section ends.
 * This avoids possible redundant macro definitions.
 */

#if (!defined WDGM_BSW_LCFG_H)
#define WDGM_BSW_LCFG_H
/*==================[inclusions]=================================================================*/

#include <Std_Types.h>                                                         /* standard types */
/* !LINKSTO WDGM.EB.ASR32.WDGM023,1 */
#include <WdgM_BSW.h>                                                         /* WdgM_ConfigType */
#include <WdgIf.h>                                                             /* WdgIf_ModeType */

/*==================[macros]=====================================================================*/

#if (defined WDGM_EB_RESULT_CORRECT)
#error WDGM_EB_RESULT_CORRECT is already defined
#endif

/** \brief value for a correct result */
#define WDGM_EB_RESULT_CORRECT 0xB4U

#if (defined WDGM_EB_RESULT_INCORRECT)
#error WDGM_EB_RESULT_INCORRECT is already defined
#endif

/** \brief value for an incorrect result (quite different from correct, but not bit-inverse) */
#define WDGM_EB_RESULT_INCORRECT 0xCBU

#if (defined WDGM_EB_RESULT_NOTEVALUATED)
#error WDGM_EB_RESULT_NOTEVALUATED is already defined
#endif

/** \brief value for a not evaluated result */
#define WDGM_EB_RESULT_NOTEVALUATED 0xA3U

#if (defined WDGM_EB_MAX_ALIVE_COUNTER_VALUE)
#error WDGM_EB_MAX_ALIVE_COUNTER_VALUE is already defined
#endif

/** \brief maximum value for an alive counter */
#define WDGM_EB_MAX_ALIVE_COUNTER_VALUE 0xFFFFUL

#if (defined WDGM_EB_DUMMY_SEID)
#error WDGM_EB_DUMMY_SEID is already defined
#endif

/** \brief dummy SEID in case some SE/CP has no predecessor */
#define WDGM_EB_DUMMY_SEID 255U

#if (defined WDGM_EB_DUMMY_CPID)
#error WDGM_EB_DUMMY_CPID is already defined
#endif

/** \brief dummy CPID in case some SE/CP has no predecessor */
#define WDGM_EB_DUMMY_CPID 255U

#if (defined WDGM_EB_DUMMY_FAILEDREFCYCLETOL)
#error WDGM_EB_DUMMY_FAILEDREFCYCLETOL is already defined
#endif

/** \brief dummy failed reference cycle tolerance in case a SE is not configured in some mode */
#define WDGM_EB_DUMMY_FAILEDREFCYCLETOL 0U

/*==================[type definitions]===========================================================*/

/*------------------[basic type definitions]-----------------------------------------------------*/


/** \brief result type of a checkpoint */
typedef uint8 WdgM_EB_ResultType;

#if (WDGM_EB_LOGICAL_SUPERVISION_ENABLED == STD_ON)
/** \brief identifier of a logical supervision graph */
typedef uint16 WdgM_EB_GraphIdType;
#endif

#if (WDGM_EB_DEADLINE_SUPERVISION_ENABLED == STD_ON)
/** \brief identifier of a deadline supervision entity */
typedef uint16 WdgM_EB_DMIdType;
#endif

/** \brief identifier of a supervised entity
 *
 * This is a deviation from AUTOSAR and is fixed to uint8 in order to use atomic assignments */
typedef uint8 WdgM_EB_SEIdType;

/** \brief identifier of a checkpoint
 *
 * This is a deviation from AUTOSAR and is fixed to uint8 in order to use atomic assignments */
typedef uint8 WdgM_EB_CPIdType;

/** \brief alive counter data */
typedef uint16 WdgM_EB_AliveCounterType;

/** \brief the value of a trigger condition */
typedef uint16 WdgM_EB_TriggerConditionType;

/** \brief the id of a trigger */
typedef uint8 WdgM_EB_TriggerIdType;

/** \brief the id of a watchdog */
typedef uint8 WdgM_EB_WatchdogIdType;

#if (WDGM_EB_DEADLINE_SUPERVISION_ENABLED == STD_ON)
/** \brief a timestamp for deadline monitoring */
typedef uint32 WdgM_EB_TimestampType;
#endif

/** \brief holding a checkpoint */
typedef struct
{
  WdgM_EB_SEIdType SEId;
  WdgM_EB_CPIdType CPId;
} WdgM_EB_CPType;

/** \brief the first expired SEId
 *
 * Holding the SEId and the bit-inverse value */
typedef uint16 WdgM_EB_ExpiredSEIDType;

/*------------------[data type definitions]------------------------------------------------------*/

#if (WDGM_EB_LOGICAL_SUPERVISION_ENABLED == STD_ON)
/* !LINKSTO WDGM.EB.DesignDecision.InternalVariable6,1,
            WDGM.EB.DesignDecision.InternalVariable7,1 */
/** \brief data for a logical supervision graph */
typedef struct
{
  uint32 LastCPData; /**< last checkpoint including bit-inverse data */
  /* !LINKSTO WDGM.EB.DesignDecision.InternalVariable7,1 */
  uint16 IsActiveData; /**< active flag of the graph including bit-inverse data */
} WdgM_EB_GraphDataType;
#endif

#if (WDGM_EB_DEADLINE_SUPERVISION_ENABLED == STD_ON)
/* !LINKSTO WDGM.EB.DesignDecision.InternalVariable4,1 */
/** \brief data for a deadline supervision graph */
typedef struct
{
  WdgM_EB_TimestampType StartTimestampData;
  /**< time-stamp when the start Checkpoint was called */
  WdgM_EB_TimestampType StartTimestampDataInv;
  /**< bit-inverse time-stamp when the start Checkpoint was called */
  uint32 MaxCyclesData;
  /**<  maximum allowed main function cycles until deadline violation */
  uint16 IsActiveData;
  /**< active flag of the Deadline Monitoring including bit-inverse data */
} WdgM_EB_DMDataType;
#endif

/** \brief runtime data of a CP */
typedef struct
{
  /* !LINKSTO WDGM.EB.DesignDecision.InternalVariable3,1 */
  uint32 AliveCounterData; /**< alive counter and bit-inverse of it */
#if (WDGM_EB_DEADLINE_SUPERVISION_ENABLED == STD_ON)
  /* !LINKSTO WDGM.EB.DesignDecision.InternalVariable5,1 */
  uint16 DeadlineData; /**< deadline result and bit-inverse of it */
#endif
#if (WDGM_EB_LOGICAL_SUPERVISION_ENABLED == STD_ON)
  /* !LINKSTO WDGM.EB.DesignDecision.InternalVariable8,1 */
  uint16 LogicalData; /**< logical result and bit-inverse of it */
#endif
} WdgM_EB_CPDataType;


/*------------------[configuration type definitions]---------------------------------------------*/

#if (WDGM_EB_LOGICAL_SUPERVISION_ENABLED == STD_ON)
/** \brief configuration data for logical supervision of a CP */
typedef struct
{
  P2CONST(WdgM_EB_CPType,TYPEDEF,WDGM_CONST) PredCPs;
  /**< pointer to an array of allowed predecessor CPs. NULL_PTR if no predecessors are allowed. */
  WdgM_EB_GraphIdType GraphId; /**< id of the graph the CP belongs to */
  uint8 NumOfPredCPs; /**< number of allowed predecessor CPs. If 0, then this is an initial CP. */
  boolean IsFinal; /**< flag for being a final CP */
} WdgM_EB_CPLogicalCfgType;
#endif

/** \brief configuration data for alive supervision of a CP */
typedef struct
{
  uint16 ExpectedAliveIndication; /**< amount of expected alive indications */
  uint16 SupervisionReferenceCycle;
  /**< amount of supervision cycles needed until checking is performed */

  uint8 MaxMargin; /**< maximum number of additional indications */
  uint8 MinMargin; /**< maximum number of fewer indications */
} WdgM_EB_CPAliveCfgType;

#if (WDGM_EB_DEADLINE_SUPERVISION_ENABLED == STD_ON)
/** \brief configuration data for deadline monitoring of a CP */
typedef struct WdgM_EB_CPDeadlineCfgStructType
{
  WdgM_EB_TimestampType WdgMDeadlineMax; /**< maximum Deadline time */
  WdgM_EB_TimestampType WdgMDeadlineMin; /**< minimum Deadline time */
  uint16 DMMaxMainfunctionCycles; /**< maximum main function calls until deadline violation */
  P2CONST(struct WdgM_EB_CPDeadlineCfgStructType,TYPEDEF,WDGM_CONST) PrevCPDeadlineCfg;
  /**< pointer to deadline configuration in case a start checkpoint is a stop checkpoint
   * of a previous deadline supervision configuration */
  WdgM_EB_DMIdType DMId;
  /**< id of the Deadline Monitoring this CP belongs to in case of a Start CP,
   * in case of a Stop CP, this variable is retrieved via \a PrevCPDeadlineCfg. */
  WdgM_EB_CPIdType StartCPId; /**< Internal Checkpoint id of start checkpoint */
  WdgM_EB_CPIdType StopCPId; /**< Internal Checkpoint id of stop checkpoint */
} WdgM_EB_CPDeadlineCfgType;
#endif

/** \brief configuration data of a CP */
typedef struct
{
  P2CONST(WdgM_EB_CPAliveCfgType,TYPEDEF,WDGM_CONST) AliveCfgPtr[WDGM_EB_MODE_NUM];
  /**< array of pointers to alive supervision configuration */
#if (WDGM_EB_DEADLINE_SUPERVISION_ENABLED == STD_ON)
  P2CONST(WdgM_EB_CPDeadlineCfgType,TYPEDEF,WDGM_CONST) DeadlineCfgPtr[WDGM_EB_MODE_NUM];
  /**< array of pointers to deadline monitoring configuration */
#endif
#if (WDGM_EB_LOGICAL_SUPERVISION_ENABLED == STD_ON)
  P2CONST(WdgM_EB_CPLogicalCfgType,TYPEDEF,WDGM_CONST) ExtIntLogicalCfgPtr[WDGM_EB_MODE_NUM];
  /**< array of pointers to external / internal logical supervision configuration */
#endif
} WdgM_EB_CPCfgType;

/** \brief configuration data of a SE */
typedef struct
{
  P2CONST(WdgM_EB_CPCfgType,TYPEDEF,WDGM_CONST) CPCfg;
  /**< pointer to an array of length NumOfCPs of CP configurations */
  P2VAR(WdgM_EB_CPDataType,TYPEDEF,WDGM_VAR_NOINIT) CPData;
  /**< pointer to an array of length NumOfCPs of CP runtime data */
  WdgM_EB_SEIdType SEId;
  /**< identifier of the external SE */
  WdgM_EB_CPIdType NumOfCPs;
  /**< number of CPs in this SE */
  boolean ErrorRecoveryEnabled;
  /**< True, if Supervised Entity shall never enter state Expired for sake of error recovery */
  uint8 FailedAliveSupervisionRefCycleTol[WDGM_EB_MODE_NUM];
  /**< amount of reference cycles that are tolerable to fail in the given mode */
} WdgM_EB_SECfgType;

/** \brief configuration data for a watchdog trigger */
typedef struct
{
  WdgIf_ModeType WatchdogMode; /**< watchdog mode to use for triggering */
  WdgM_EB_TriggerConditionType TriggerCondition; /**< the value that is used for triggering */
  WdgM_EB_WatchdogIdType WatchdogId; /**< the id of the watchdog to trigger */
} WdgM_EB_TriggerCfgType;

/** \brief configuration data for each mode */
typedef struct
{
  uint32 MaxSupervisionCycleCounter;
  /**< least common multiple of all reference cycle values */
  P2CONST(WdgM_EB_TriggerCfgType, TYPEDEF, WDGM_CONST) TriggerCfg;
  /**< pointer to an array of trigger configurations */
  uint16 ExpiredSupervisionCycleTol;
  /**< amount of supervision cycles to wait in state EXPIRED */
  WdgM_EB_TriggerIdType NumTriggers;
  /**< number of triggers configured for this mode */
} WdgM_EB_ModeCfgType;

/*==================[external function declarations]=============================================*/

/*==================[external constants]=========================================================*/

#define WDGM_START_SEC_CONST_UNSPECIFIED
#include <MemMap.h>

/** \brief configured initial mode id */
extern CONST(WdgM_ModeType,WDGM_CONST) WdgM_EB_InitialModeId;

#if (WDGM_EB_DEINIT_MODE_ENABLED == STD_ON)
/** \brief configured de-init mode id */
extern CONST(WdgM_ModeType,WDGM_CONST) WdgM_EB_DeInitModeId;
#endif

/** \brief array of configuration data for a SE */
extern CONST(WdgM_EB_SECfgType,WDGM_CONST) WdgM_EB_SECfg[WDGM_EB_SE_NUM];

/** \brief array of configuration data for a WdgMMode */
extern CONST(WdgM_EB_ModeCfgType,WDGM_CONST) WdgM_EB_ModeCfg[WDGM_EB_MODE_NUM];

/** \brief array size of configuration data for a WdgMMode */
extern CONST(WdgM_ModeType,WDGM_CONST) WdgM_EB_ModeCfgSize;

#if (WDGM_EB_LOGICAL_SUPERVISION_ENABLED == STD_ON)
/** \brief array length of constant pointers to runtime data of logical supervision graphs */
extern CONST(WdgM_EB_GraphIdType,WDGM_CONST) WdgM_EB_GraphDataSize;
#endif

#if (WDGM_EB_DEADLINE_SUPERVISION_ENABLED == STD_ON)
/** \brief array length of constant pointers to runtime data of deadline supervision */
extern CONST(WdgM_EB_DMIdType,WDGM_CONST) WdgM_EB_DMDataSize;
#endif

#define WDGM_STOP_SEC_CONST_UNSPECIFIED
#include <MemMap.h>

#if (WDGM_EB_CALLERIDS_ENABLED == STD_ON)

#define WDGM_START_SEC_CONST_16BIT
#define WDGM_START_SEC_CONST_16
#include <MemMap.h>

/** \brief List of configured caller IDs */
extern CONST(uint16, WDGM_CONST) WdgM_EB_CallerIds[WDGM_EB_CALLERIDS_NUM];

#define WDGM_STOP_SEC_CONST_16BIT
#define WDGM_STOP_SEC_CONST_16
#include <MemMap.h>
/* Deviation MISRA-1 <+4> */
#undef WDGM_START_SEC_CONST_16BIT
#undef WDGM_START_SEC_CONST_16
#undef WDGM_STOP_SEC_CONST_16BIT
#undef WDGM_STOP_SEC_CONST_16

#endif

/*==================[external data]==============================================================*/

/* !LINKSTO WDGM.EB.TIMEPM.WDGM020304,1 */
#if (WDGM_EB_PARTITIONING_ENABLED == STD_ON)
#define WDGM_START_SEC_SHARED_VAR_NO_INIT_UNSPECIFIED
#else
#define WDGM_START_SEC_VAR_NOINIT_UNSPECIFIED
#define WDGM_START_SEC_VAR_NO_INIT_UNSPECIFIED
#endif
#include <MemMap.h>

/* !LINKSTO WDGM.EB.DesignDecision.InternalVariable6,1,WDGM.EB.DesignDecision.InternalVariable7,1 */
#if (WDGM_EB_LOGICAL_SUPERVISION_ENABLED == STD_ON)
#if (WDGM_EB_GRAPH_NUM > 0)
extern VAR(WdgM_EB_GraphDataType,WDGM_VAR_NOINIT) WdgM_EB_GraphData[WDGM_EB_GRAPH_NUM];
#else
extern VAR(WdgM_EB_GraphDataType,WDGM_VAR_NOINIT) WdgM_EB_GraphData[1];
#endif
#endif

/* !LINKSTO WDGM.EB.DesignDecision.InternalVariable4,1 */
#if (WDGM_EB_DEADLINE_SUPERVISION_ENABLED == STD_ON)
#if (WDGM_EB_DM_NUM > 0)
extern VAR(WdgM_EB_DMDataType,WDGM_VAR_NOINIT) WdgM_EB_DMData[WDGM_EB_DM_NUM];
#else
extern VAR(WdgM_EB_DMDataType,WDGM_VAR_NOINIT) WdgM_EB_DMData[1];
#endif
#endif

/* !LINKSTO WDGM.EB.TIMEPM.WDGM020304,1 */
#if (WDGM_EB_PARTITIONING_ENABLED == STD_ON)
#define WDGM_STOP_SEC_SHARED_VAR_NO_INIT_UNSPECIFIED
#else
#define WDGM_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#define WDGM_STOP_SEC_VAR_NO_INIT_UNSPECIFIED
#endif
#include <MemMap.h>
/* Deviation MISRA-1 <+4> */
#undef WDGM_START_SEC_VAR_NOINIT_UNSPECIFIED
#undef WDGM_START_SEC_VAR_NO_INIT_UNSPECIFIED
#undef WDGM_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#undef WDGM_STOP_SEC_VAR_NO_INIT_UNSPECIFIED

#endif /* if !defined( WDGM_BSW_LCFG_H ) */
/*==================[end of file]================================================================*/
