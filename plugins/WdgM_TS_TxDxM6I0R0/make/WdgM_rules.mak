# \file
#
# \brief AUTOSAR WdgM
#
# This file contains the implementation of the AUTOSAR
# module WdgM.
#
# \author Elektrobit Automotive GmbH, 91058 Erlangen, Germany
#
# Copyright 2005 - 2013 Elektrobit Automotive GmbH
# All rights exclusively reserved for Elektrobit Automotive GmbH,
# unless expressly agreed to otherwise.

#################################################################
# DEFINITIONS

#################################################################
# REGISTRY

WdgM_src_FILES      := \
    $(WdgM_CORE_PATH)\src\WdgM.c \
    $(WdgM_CORE_PATH)\src\WdgM_Rte.c \
    $(WdgM_CORE_PATH)\src\WdgM_ASR32_Rte.c \
    $(WdgM_CORE_PATH)\src\WdgM_ASR40_Rte.c \
    $(WdgM_OUTPUT_PATH)\src\WdgM_Rte_LCfg.c \
    $(WdgM_OUTPUT_PATH)\src\WdgM_BSW_LCfg.c

LIBRARIES_TO_BUILD   += WdgM_src

#################################################################
# RULES
