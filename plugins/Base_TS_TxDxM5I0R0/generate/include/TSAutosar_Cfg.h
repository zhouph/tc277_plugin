#if (!defined AUTOSAR_CFG_H)
#define AUTOSAR_CFG_H
[!AUTOSPACING!]

/**
 * \file
 *
 * \brief AUTOSAR Base
 *
 * This file contains the implementation of the AUTOSAR
 * module Base.
 *
 * \author Elektrobit Automotive GmbH, 91058 Erlangen, Germany
 *
 * Copyright 2005 - 2013 Elektrobit Automotive GmbH
 * All rights exclusively reserved for Elektrobit Automotive GmbH,
 * unless expressly agreed to otherwise.
 */

/*
 * MISRA-C:2004 Deviations:
 *
 * MISRA-1) Deviated Rule: 6.3 (required)
 *   Typedefs that indicate size and signedness should be used in place
 *   of the basic types.
 *
 *   Reason:
 *   Such types need to be defined somewhere and this file provides part
 *   of those type definitions with fixed size and signedness.
 */

/*==================[inclusions]=============================================*/

/*------------------[Macros for Atomic Assignment]---------------------------*/
[!INCLUDE "TypeLists.m"!]
[!/* MACRO GetCType:
     Get C type for a certain AUTOSAR/derived type

     Parameter:
      - PropertyPrefix: prefix of the respective property for the types
      - DerivedType: the AUTOSAR/derived type

     OUT Variables:
      - CType:  the C type corresponding to the AUTOSAR/derived type
*/!][!//
[!MACRO "GetCType","PropertyPrefix", "DerivedType"!]
  [!VAR "property"="concat($PropertyPrefix,$DerivedType,'.Mapping')"!]
  [!IF "ecu:has($property)"!]
    [!VAR "CType"="normalize-space(text:replace(ecu:get($property),'(un)?signed',''))"!]
  [!ELSE!]
    [!ERROR!]
      Base: No mapping to C type found for derived type [!"$DerivedType"!]
    [!ENDERROR!]
  [!ENDIF!]
[!ENDMACRO!]

[!VAR "BitWidthList"="'8 16 32'"!]
[!VAR "BitWidthListLength"="count(text:split($BitWidthList))"!]
[!FOR "i"="1" TO "num:i($BitWidthListLength)"!]
  [!VAR "BitWidth"="text:split($BitWidthList)[num:i($i)]"!]
  [!CALL "GetCType","PropertyPrefix"="'Basetypes.'", "DerivedType"="concat('uint',$BitWidth)"!]
  [!VAR "property"="concat('Basetypes.',$CType,'.AtomicAccess')"!]
  [!IF "ecu:has($property) and (ecu:get($property) = 'true')"!]
#if (defined TS_AtomicAssign[!"$BitWidth"!])
#error TS_AtomicAssign[!"$BitWidth"!] already defined
#endif

/** \brief Assigns the [!"$BitWidth"!] bit entity \p from to \p to in an atomic fashion
 **
 ** This macro assigns the [!"$BitWidth"!] bit entity \p from to \p to in an atomic fashion
 **
 ** The parameters \p from and \p to thus have to be of type \p uint8 or \p sint8.
 **
 ** \param[out] to    [!"$BitWidth"!] bit entity which is the destination of the assignment
 ** \param[in]  from  [!"$BitWidth"!] bit entity which is the source of the assignment
 **
 ** \remarks The parameters of this macro may be used in any way, especially
 **   they can be used more than once. They shall not contain side effects. */
#define TS_AtomicAssign[!"$BitWidth"!](to, from) ((to) = (from))


  [!ENDIF!]
[!ENDFOR!]

/*------------------[Size of Autosar Standard Types]-------------------------*/

[!/* MACRO CreateSizeMacros:
     Create marcos for accessing the size for all types passed.

     Parameter:
      - List: list of types to create the macros
      - PropertyPrefix: prefix of the respective property for the types
*/!][!//
[!MACRO "CreateSizeMacros","List","PropertyPrefix"!]
  [!VAR "ListLength"="count(text:split($List))"!]
  [!FOR "i"="1" TO "num:i($ListLength)"!]
    [!VAR "DerivedType"="text:split($List)[num:i($i)]"!]
    [!CALL "GetCType","PropertyPrefix"="$PropertyPrefix", "DerivedType"="$DerivedType"!]
    [!VAR "property"="concat('Basetypes.',$CType,'.Size')"!]
    [!IF "ecu:has($property)"!]
#if (defined TS_SIZE_[!"$DerivedType"!])
#error TS_SIZE_[!"$DerivedType"!] already defined
#endif
/** \brief Size of derived type [!"$DerivedType"!] */
#define TS_SIZE_[!"$DerivedType"!] [!"ecu:get($property)"!]U


    [!ENDIF!]
  [!ENDFOR!]
[!ENDMACRO!]

[!CALL "CreateSizeMacros", "List"="$AutosarTypeList", "PropertyPrefix"="'Basetypes.'"!]
[!CALL "CreateSizeMacros", "List"="$DerivedTypeList", "PropertyPrefix"="'Derivedtypes.'"!]

/*------------------[Alignment Autosar Standard Types]-----------------------*/

[!/* MACRO CreateAlignmentMacros:
     Create marcos for accessing the alignment for all types passed.

     Parameter:
      - List: list of types to create the macros
      - PropertyPrefix: prefix of the respective property for the types
*/!][!//
[!MACRO "CreateAlignmentMacros","List","PropertyPrefix"!]
  [!VAR "ListLength"="count(text:split($List))"!]
  [!FOR "i"="1" TO  "num:i($ListLength)"!]
    [!VAR "DerivedType"="text:split($List)[num:i($i)]"!]
    [!CALL "GetCType","PropertyPrefix"="$PropertyPrefix", "DerivedType"="$DerivedType"!]
    [!VAR "property"="concat('Basetypes.',$CType,'.Alignment')"!]
    [!IF "ecu:has($property)"!]
#if (defined TS_ALIGNMENT_[!"$DerivedType"!])
#error TS_ALIGNMENT_[!"$DerivedType"!] already defined
#endif
/** \brief Alignment constraints for derived type [!"$DerivedType"!] */
#define TS_ALIGNMENT_[!"$DerivedType"!] [!"ecu:get($property)"!]U


    [!ENDIF!]
  [!ENDFOR!]
[!ENDMACRO!]

[!/* MACRO CreateComplexAlignmentMacros:
      Create marcos for accessing the alignment of complex types.

      Parameter:
       - Type: type to create alignment macros for (either 'array' or 'struct')
       - ListThresholds: list of thresholds of alignments of complex type
       - ListAlignments: list of alignments of complex type
       - DefaultAlignment: default alignment of complex type
 */!][!//
[!MACRO "CreateComplexAlignmentMacros","Type","ListThresholds","ListAlignments","DefaultAlignment"!]
  [!VAR "Thresholds"="text:replaceAll(substring($ListThresholds,2,string-length($ListThresholds)-2),',','')"!]
  [!VAR "Alignments"="text:replaceAll(substring($ListAlignments,2,string-length($ListAlignments)-2),',','')"!]
  [!VAR "Length"="num:i(count(text:split($Alignments)))"!]
  [!IF "$Length > 1"!]
#if (defined TS_ALIGNMENT_[!"$Type"!]_NUM_THRESHOLDS)
#error TS_ALIGNMENT_[!"$Type"!]_NUM_THRESHOLDS already defined
#endif
/** \brief Number of alignment constraints for [!"$Type"!]s */
#define TS_ALIGNMENT_[!"$Type"!]_NUM_THRESHOLDS [!"$Length"!]U

    [!FOR "i"="1" TO "$Length"!]
      [!VAR "Threshold"="text:split($Thresholds)[num:i($i)]"!]
      [!VAR "Alignment"="text:split($Alignments)[num:i($i)]"!]
      [!VAR "Index"="num:i($i - 1)"!]
      [!IF "string-length($Threshold) > 0"!]
#if (defined TS_ALIGNMENT_[!"$Type"!]_THRESHOLD_[!"$Index"!])
#error TS_ALIGNMENT_[!"$Type"!]_THRESHOLD_[!"$Index"!] already defined
#endif
/** \brief Threshold of [!"$i"!]. alignment constraint for [!"$Type"!]s */
#define TS_ALIGNMENT_[!"$Type"!]_THRESHOLD_[!"$Index"!] [!"$Threshold"!]U

      [!ENDIF!]
#if (defined TS_ALIGNMENT_[!"$Type"!]_[!"$Index"!])
#error TS_ALIGNMENT_[!"$Type"!]_[!"$Index"!] already defined
#endif
/** \brief [!"$i"!]. alignment constraint for [!"$Type"!]s */
#define TS_ALIGNMENT_[!"$Type"!]_[!"$Index"!] [!"$Alignment"!]U

    [!ENDFOR!]
  [!ENDIF!]
#if (defined TS_ALIGNMENT_[!"$Type"!])
#error TS_ALIGNMENT_[!"$Type"!] already defined
#endif
/** \brief Default alignment constraint for [!"$Type"!]s */
#define TS_ALIGNMENT_[!"$Type"!] [!"$DefaultAlignment"!]U


[!ENDMACRO!]

[!CALL "CreateAlignmentMacros", "List"="$AutosarTypeList", "PropertyPrefix"="'Basetypes.'"!]
[!CALL "CreateComplexAlignmentMacros", "Type"="'array'", "ListThresholds"="asc:getArrayAlignmentThresholds()", "ListAlignments"="asc:getArrayAlignments()", "DefaultAlignment"="asc:getArrayAlignment()"!]
[!CALL "CreateComplexAlignmentMacros", "Type"="'struct'", "ListThresholds"="asc:getStructAlignmentThresholds()", "ListAlignments"="asc:getStructAlignments()", "DefaultAlignment"="asc:getStructAlignment()"!]
[!CALL "CreateAlignmentMacros", "List"="$DerivedTypeList", "PropertyPrefix"="'Derivedtypes.'"!]
/*==================[type definitions]======================================*/

[!VAR "ListLength"="count(text:split($DerivedTypeList))"!]
[!FOR "i"="1" TO  "num:i($ListLength)"!]
  [!VAR "DerivedType"="text:split($DerivedTypeList)[num:i($i)]"!]
  [!VAR "property"="concat('Derivedtypes.',$DerivedType,'.Mapping')"!]
  [!IF "ecu:has($property)"!]
/* Deviation MISRA-1 <+2> */
/** \brief Type definition of derived type [!"$DerivedType"!] */
typedef [!"ecu:get($property)"!] [!"$DerivedType"!];

  [!ENDIF!]
[!ENDFOR!]

#endif /*(!defined AUTOSAR_CFG_H)*/
