/**
 * \file
 *
 * \brief AUTOSAR CanIf
 *
 * This file contains the implementation of the AUTOSAR
 * module CanIf.
 *
 * \author Elektrobit Automotive GmbH, 91058 Erlangen, Germany
 *
 * Copyright 2005 - 2013 Elektrobit Automotive GmbH
 * All rights exclusively reserved for Elektrobit Automotive GmbH,
 * unless expressly agreed to otherwise.
 */
[!CODE!]
[!AUTOSPACING!]
#if (!defined CANIF_SYMBOLICNAMES_PBCFG_H)
#define CANIF_SYMBOLICNAMES_PBCFG_H


/*==================[includes]===============================================*/
/*==================[macros]=================================================*/

/*------------------[symbolic name values]----------------------------------*/

/** \brief Export symbolic name values for CanIfTxPduIds */

[!LOOP "as:modconf('CanIf')[1]/CanIfInitCfg/*[1]/CanIfTxPduCfg/*"!]

#define CanIfConf_CanIfTxPduCfg_[!"@name"!] [!"CanIfTxPduId"!]U

#if (!defined CANIF_DONT_PROVIDE_LEGACY_SYMBOLIC_NAMES)
/** \brief Export symbolic name value without prefix (AUTOSAR version <= 3.1 rev4) */
#define [!"@name"!] [!"CanIfTxPduId"!]U
/** \brief Export symbolic name value with module abbreviation as prefix only (3.1 rev4 <
AUTOSAR version <= AUTOSAR 4.0 rev2) */
#define CanIf_[!"@name"!] [!"CanIfTxPduId"!]U
#endif /* !defined CANIF_DONT_PROVIDE_LEGACY_SYMBOLIC_NAMES */
[!ENDLOOP!][!//


/** \brief Export symbolic name values for CanIfRxPduIds */

[!LOOP "as:modconf('CanIf')[1]/CanIfInitCfg/*[1]/CanIfRxPduCfg/*"!]

#define CanIfConf_CanIfRxPduCfg_[!"@name"!] [!"CanIfRxPduId"!]U

#if (!defined CANIF_DONT_PROVIDE_LEGACY_SYMBOLIC_NAMES)
/** \brief Export symbolic name value without prefix (AUTOSAR version <= 3.1 rev4) */
#define [!"@name"!] [!"CanIfRxPduId"!]U
/** \brief Export symbolic name value with module abbreviation as prefix only (3.1 rev4 <
AUTOSAR version <= AUTOSAR 4.0 rev2) */
#define CanIf_[!"@name"!] [!"CanIfRxPduId"!]U
#endif /* !defined CANIF_DONT_PROVIDE_LEGACY_SYMBOLIC_NAMES */
[!ENDLOOP!][!//


/** \brief Export symbolic name values for CanIfCtrlIds */

[!LOOP "as:modconf('CanIf')[1]/CanIfCtrlDrvCfg/*/CanIfCtrlCfg/*"!]

#define CanIfConf_[!"../../@name"!]_[!"@name"!] [!"CanIfCtrlId"!]U

#if (!defined CANIF_DONT_PROVIDE_LEGACY_SYMBOLIC_NAMES)
/** \brief Export symbolic name value without prefix (AUTOSAR version <= 3.1 rev4) */
#define [!"../../@name"!]_[!"@name"!] [!"CanIfCtrlId"!]U
/** \brief Export symbolic name value with module abbreviation as prefix only (3.1 rev4 <
AUTOSAR version <= AUTOSAR 4.0 rev2) */
#define CanIf_[!"../../@name"!]_[!"@name"!] [!"CanIfCtrlId"!]U
#endif /* !defined CANIF_DONT_PROVIDE_LEGACY_SYMBOLIC_NAMES */
[!ENDLOOP!][!//


/** \brief Export symbolic name values for CanIfTrcvIds */

[!LOOP "as:modconf('CanIf')[1]/CanIfTrcvDrvCfg/*/CanIfTrcvCfg/*"!]

#define CanIfConf_[!"../../@name"!]_[!"@name"!] [!"CanIfTrcvId"!]U

#if (!defined CANIF_DONT_PROVIDE_LEGACY_SYMBOLIC_NAMES)
/** \brief Export symbolic name value without prefix (AUTOSAR version <= 3.1 rev4) */
#define [!"../../@name"!]_[!"@name"!] [!"CanIfTrcvId"!]U
/** \brief Export symbolic name value with module abbreviation as prefix only (3.1 rev4 <
AUTOSAR version <= AUTOSAR 4.0 rev2) */
#define CanIf_[!"../../@name"!]_[!"@name"!] [!"CanIfTrcvId"!]U
#endif /* !defined CANIF_DONT_PROVIDE_LEGACY_SYMBOLIC_NAMES */
[!ENDLOOP!][!//

/*==================[type definitions]=======================================*/

/*==================[external function declarations]=========================*/

/*==================[internal function declarations]=========================*/

/*==================[external constants]=====================================*/

/*==================[internal constants]=====================================*/

/*==================[external data]==========================================*/

/*==================[internal data]==========================================*/

/*==================[external function definitions]==========================*/

/*==================[internal function definitions]==========================*/

#endif /* if !defined( CANIF_SYMBOLICNAMES_PBCFG_H ) */
/*==================[end of file]============================================*/
[!ENDCODE!]
