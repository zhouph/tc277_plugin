<?xml version="1.0" encoding="UTF-8"?>

<!-- <<<<<<<<<<<<<<<<<<<<<<<<<<<<<< tresos Studio Workflow >>>>>>>>>>>>>>>>>>>>>>>>>>>>> -->

<workflow xmlns="http://www.tresos.de/_projects/tresos/workflow_1_0.xsd"
  xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
  xsi:schemaLocation="http://www.tresos.de/_projects/tresos/workflow_1_0.xsd http://www.tresos.de/_projects/tresos/workflow_1_0.xsd"
  id="ACGPostBuildUpdateWorkflow" version="2.2"
  label="AutoCore Workflow Post-Build Update"
  description="The EB tresos workflow is a step-by-step instruction to guide you through the
    configuration of a project. The workflow represents a sequence of steps to accomplish the task
    of building an application.">

  <group id="PBUPDATE" label="Update the post-build configuration of your software project">
    <description>
      <![CDATA[
        <h2><b>Goal</b></h2>
          <p>To update and re-build the post-build configuration of your basic software project.</p>
          <p>The <i>AutoCore Workflow Post-Build Update</i> helps you to update and re-build the
          post-build configuration of your basic software project.</p>
        <h2><b>Preconditions</b></h2>
          <p>The <i>AutoCore Workflow Post-Build Update</i> continues the <i>AutoCore Workflow
          Post-Build Only</i>. If you did not perform the steps as described in the <i>AutoCore
          Workflow Post-Build Only</i>, go through the
          <a href="workflow://ACGPostBuildOnlyWorkflow/PBONLY">AutoCore Workflow Post-Build Only</a>
          before continuing with this post-build update workflow.</p>
        <h2><u>Procedure</u></h2>
          <p><ol>
            <li>Import an updated system description.</li>
            <li>Modify the module configurations.</li>
            <li>Run Assistants.</li>
            <li>Build a binary from the post-build configuration.</li>
          </ol></p>
        <h2><u>Output</u></h2>
        <blockquote><b>Note:</b>
          <p>Names of dialogs, editors, importers, tables, tabs, etc. are displayed in <i>italic</i>
          font. </p>
          <p>Buttons, text boxes, check boxes, drop-down list boxes and menu entries are displayed
          in <b>bold</b> font. </p>
          <p>File, directory and path names, parameters and command line commands are displayed in
          <tt>Courier</tt> font.</p>
        </blockquote>
        <blockquote><b>Tip:</b>
          <p>A workflow step that is displayed with a green arrow icon is an action step. You may
          start the corresponding action by double-clicking the step or by clicking the <b>Run</b>
          button in the tool bar.</p>
          <p>A workflow step that is displayed with a red cross icon is an unavailable step. With
          your current selection in the <i>Project Explorer</i> view it is not possible to execute
          the corresponding action.</p>
        </blockquote>
      ]]>
    </description>

    <!-- <<<<<<<<<<<<<<<<<<<<<<<<<<<<<< Import SysD >>>>>>>>>>>>>>>>>>>>>>>>>>>>> -->

    <group id="PBUPDATE_IMP" label="Import updated communication matrix description file"
      type="sequence">
      <description>
        <![CDATA[
          <h2><b>Goal</b></h2>
            <p>To import an AUTOSAR system description (ARXML).</p>
            <p>The communication matrix is represented in the system description file format.
            When you import the system description file, it configures big parts of the modules <i>Can</i>,
            <i>CanIf</i>, <i>Com</i>, <i>EcuC</i> and <i>PduR</i> (and other modules). Additionally
            this file contains the configuration data for the modules <i>CanTp</i> and <i>CanNm</i>.
            </p>
          <h2><u>Procedure</u></h2>
            <p><ul>
              <li>To import an updated communication matrix from a system description, proceed to
              step <a href="workflow:///PBUPDATE_IMP_SYSTEM">Import from system description</a>.
              </li>
            </ul></p>
        ]]>
      </description>

      <action id="PBUPDATE_IMP_SYSTEM" label="Import from system description">
        <command description="Import from system description"
          serialization="dreisoft.tresos.launcher2.api.plugin.ImporterExporterCommand"/>

        <description>
          <![CDATA[
            <p><ol>
              <li>Double-click this action step in the <i>Workflows</i> view. The <i>Im- and
              Exporter</i> dialog opens up.</li>
              <li>In the <i>Create, manage and run im- and exporters</i> dialog, select the system
              description importer you have added in the <i>AutoCore Workflow Rte</i>.</li>
              <li>Select the tab <i>All Models</i>, remove all input files by selecting them in the
              <i>Input files</i> list and clicking <b>Remove</b>. Add the new system description
              file by clicking <b>Add...</b> and selecting the desired system description file.
              Disable the check box <b>Validate against XML schema</b> if your system model is
              incomplete, for example only contains the communication stack description. Choose the
              system and the ECU instance to import via the <b>System</b> and the <b>ECU
              instance</b> drop-down box.</li>
              <li>Select the tab <i>System Model Import</i>, enable the check box <b>Enable the
              system model import</b> and select <b>Merge content of selected files into existing
              model</b>. Otherwise the contents of the new system description file are not
              merged with your existing configuration. Select <b>No ECU extract</b> from the <i>Add
              software component ECU extract</i> radio button group to prevent the import of software
              component information.</li>
              <li>Select the tab <i>ECU Configuration Import</i> and enable the check box <b>Enabled
              ECU Configuration import</b>. Further enable the ECU configuration import for each
              post-build capable module in your project (including <i>EcuC</i>) via the respective
              drop-down box. Disable the ECU configuration import for each non post-build capable
              module. Do not change the settings of the checkbox <b>Instance suffix for Frames, PDUs
              and Signals</b>. Disable the creation of a default buffer assignment via the <b>Buffer
              Assignment in Can/CanIf</b> drop-down box.</i>
              <li>To apply your changes, click <b>Apply</b>.</li>
              <li>To import your CAN communication matrix into the ECU configuration, click the
              button <b>Run Importer</b>.</li>
            </ol></p>
          ]]>
        </description>
      </action>
    </group>

    <!-- <<<<<<<<<<<<<<<<<<<<<<<<<<<<<< Configure Basic Software >>>>>>>>>>>>>>>>>>>>>>>>>>>>> -->

    <group id="PBUPDATE_BSW" label="Modify the module configurations" repeatable="false"
      type="sequence">
      <description>
        <![CDATA[
          <h2><b>Goal</b></h2>
            <p>To configure the basic software modules in your project.</p>
          <h2><u>Procedure</u></h2>
            <p>You can change each basic software module configuration either by double-clicking the
            respective action step in the <i>Workflows</i> view or by double-clicking the module in
            the <i>Project Explorer</i> view.</p>
          <blockquote><b>Tip:</b>
            <p>To search for configuration parameter names project-wide, press <b>Ctrl+T</b>.
            </p>
          </blockquote>
            <p>Configure the following module manually:</p>
            <p><ul>
              <li><a href="workflow:///PBUPDATE_BSW_CANIF"><i>CanIf</i></a>: Assign PDUs to HOHs in
              the <i>CanIf</i> module.</li>
            </ul></p>
        ]]>
      </description>

      <action label="Assign PDUs to HOHs in CanIf" id="PBUPDATE_BSW_CANIF">
        <command description="Open CanIf configuration"
          serialization="dreisoft.tresos.launcher2.api.plugin.EditConfigurationCommand(moduleType=CanIf)"
          autoadvance="false"/>

        <description>
          <![CDATA[
            <p>This workflow step is only required and possible, if your project contains both the
            <i>Can Interface (CanIf)</i> and the <i>Can Driver (Can)</i> modules.</p>
            <blockquote><b>Note:</b>
              <p>Since the  <i>Can Driver (Can)</i> module itself is <b>not</b> post-build capable,
              creation of new HOHs or modification of the existing ones (e.g., modification of the
              filter masks or IDs) is not possible/allowed. - The only allowed actions are
              assignment of PDUs to the existing HOHs.</p>
            </blockquote>
            <p>To assign PDUs to already existing HOHs in <i>CanIf</i> module:</p>
            <p><ol>
              <li>Double-click this action step in the <i>Workflows</i> view. The <i>CanIf</i>
              editor opens up in the editor area of EB tresos Studio.</li>
              <li>In the <i>CanIfInitCfg</i> tab, open the first <tt>CanIfInitCfg</tt> by double
              clicking on the respective entry in the <i>CanIfInitCfg</i> list.</li>
              <li>For each newly added entry in the <i>CanIfRxPduCfg</i> tab perform the following
              steps:
                <ol>
                  <li>Double-click the entry to open it.</li>
                  <li>In the tab <i>CanIfRxPduHrhIdRef</i> create one entry for each HOH that can
                  possibly receive this PDU and let <i>CanIfRxPduHrhIdRef</i> reference this HOH.
                  To decide whether a given HOH can possibly receive this PDU (according to the
                  PDU's <i>CanIfRxPduCanId</i> and <i>CanIfRxPduCanIdType</i>), check if the following
                  HOHs are set in a way that the reception takes place:
                 <p><ul>
                 <li><i>CanHandleType</i> (<b>Can Implementation Type</b>),</li>
                 <li><i>CanObjectType</i> (<b>Can MB Type</b>),</li>
                 <li><i>CanIdValue</i> (<b>CanIdValue</b>) parameters,</li>
                 <li>and the <i>CanFilterMaskValue</i> parameters of any linked filter mask via the parameter <i>CanFilterMaskRef</i>.</li>
                  </ul></p>
                  In case multiple HOHs can receive this PDU
                  (due to overlapping filters of the multiple HOHs), you must create multiple
                  <i>CanIfRxPduHrhIdRef</i> references (one for each HOH).</li>
                </ol></li>
              <li>For each newly added entry in the <i>CanIfTxPduCfg</i> tab perform the following
              steps:
                <ol>
                  <li>Open the entry by double clicking on it.</li>
                  <li>If you want to use a
                  <i>CanIfBufferCfg</i> which already exists for the transmission, use the <i>CanIfTxPduBufferRef</i> parameter to reference the <i>CanIfBufferCfg
                  </i> that shall be used for transmission of this PDU. Otherwise create a new
                  <i>CanIfHthCfg</i> and let this newly created <i>CanIfHthCfg</i> reference a HTH
                  in the Can config via <i>CanIfHthIdSymRef</i>. Create a new <i>CanIfBufferCfg</i>
                  and let this new <i>CanIfBufferCfg</i> reference the newly created
                  <i>CanIfHthCfg</i> via <i>CanIfBufferHthRef</i>.</li>
                </ol></li>
            </ol></p>
          ]]>
        </description>
      </action>

      <action label="Sort PDUs in CanIf" id="PBUPDATE_BSW_CANIF_SORT">
        <command description="Open CanIf configuration"
          serialization="dreisoft.tresos.launcher2.api.plugin.EditConfigurationCommand(moduleType=CanIf)"
          autoadvance="false"/>

        <description>
          <![CDATA[
            <h2><b>Goal</b></h2>
            <p>Prepare PDUs for configuration updates.</p>
            <p>PDUs exchanged with non-post-build capable modules (i.e., FrNm and CanNm) should be
               placed first in the list of PDUs to ensure that the PDU IDs of these PDUs do not
               change if the Calculate Handle ID assistant is re-run after the re-import of the
               system description.</p>
            <h2><u>Procedure</u></h2>
            <ol>
              <li>Double-click this action step in the <i>Workflows</i> view. The <i>CanIf</i>
                  editor opens up in the editor area of EB tresos Studio.</li>
              <li>In the <i>CanIfInitCfg</i> tab, open the first <tt>CanIfInitCfg</tt> by double
                  clicking on the respective entry in the <i>CanIfInitCfg</i> list.</li>
              <li>In the <i>CanIfRxPduCfg</i> and <i>CanIfTxPduCfg</i> tab, select one entry and use
                  the arrows above the list to moved the entry up or down in the list.</li>
            </ol>
          ]]>
        </description>
      </action>

      <action label="Sort PDUs in FrIf" id="PBUPDATE_BSW_FRIF_SORT">
        <command description="Open FrIf configuration"
          serialization="dreisoft.tresos.launcher2.api.plugin.EditConfigurationCommand(moduleType=FrIf)"
          autoadvance="false"/>

        <description>
          <![CDATA[
            <h2><b>Goal</b></h2>
            <p>Prepare PDUs for configuration updates.</p>
            <p>PDUs exchanged with non-post-build capable modules (i.e., FrNm and CanNm) should be
               placed first in the list of PDUs to ensure that the PDU IDs of these PDUs do not
               change if the Calculate Handle ID assistant is re-run after the re-import of the
               system description.</p>
            <h2><u>Procedure</u></h2>
            <ol>
              <li>Double-click this action step in the <i>Workflows</i> view. The <i>FrIf</i>
                  editor opens up in the editor area of EB tresos Studio.</li>
              <li>In the <i>FrIfConfig</i> tab, open the first <tt>FrIfConfig</tt> by double
                  clicking on the respective entry in the <i>FrIfConfig</i> list.</li>
              <li>In the <i>FrIfPdu</i> tab, select one entry and use the arrows above the list to
                  moved the entry up or down in the list.</li>
            </ol>
          ]]>
        </description>
      </action>
    </group>

    <!-- <<<<<<<<<<<<<<<<<<<<<<<<<<<<<< Assistants >>>>>>>>>>>>>>>>>>>>>>>>>>>>> -->

    <group id="PBUPDATE_ASS" label="Run assistant dialogs" type="choice">
      <description>
        <![CDATA[
          <p>Assistant dialogs help you with the configuration of complex tasks and thus avoid
          time-consuming configuration work.</p>
          <p>You can use the following assistance dialogs to configure the communication stack:</p>
          <p><ul>
            <li><a href="workflow:///PBUPDATE_ASS_FRAS"><i>FlexRay Interface (FrIf) Joblist
            Editor</i></a></li>
            <li><a href="workflow:///PBUPDATE_ASS_HID"><i>Handle ID Calculator</i></a></li>
          </ul></p>
        ]]>
      </description>

      <action label="Edit FrIf Joblist" id="PBUPDATE_ASS_FRAS">
        <command
          serialization="dreisoft.tresos.guidedconfig.api.plugin.SidebarTriggerCommand(triggerId=frifwizard_trigger)"/>

        <description>
          <![CDATA[
            <h2><b>Goal</b></h2>
              <p>To edit the job list of the FlexRay interface (FrIf). This workflow step
              is only required and possible, if your project contains the <i>FlexRay Interface
              (FrIf)</i> module.</p>
              <p>The <i>FlexRay Interface (FrIf) Joblist Editor</i> creates job triggers and
              communication operations that handle the reception, the indication, and the
              transmission of PDUs (i.&nbsp;e. FlexRay messages). The configuration is hereby performed on
              <i>Frame Triggering</i> granularity.</p>
              <p>The upper half of the <i>FlexRay Interface (FrIf) Joblist Editor</i> displays a
              list of all <i>Frame Triggerings</i> (actually contains the PDUs). In the lower half
              of the <i>FlexRay Interface (FrIf) Joblist Editor</i> you can see a list of all
              created <i>Job Triggers</i> (which define at which instant in the FlexRay communication
              cycle actions can potentially be performed) and the created <i>Communication
              Operations</i> (which define what action shall be performed at this instant).</p>
            <h2><u>Procedure</u></h2>
              <p>To edit the job list of the FlexRay interface (FrIf), follow these steps:</p>
              <p><ol>
                <li>Double-click this action step in the <i>Workflows</i> view to open the
                <i>FlexRay Interface (FrIf) Joblist Editor</i>.</li>
                <li>Select the desired <i>FrIf Cluster</i> via the combo box and click the button
                <b>Next</b>.</li>
                <li>In the lower left corner of the <i>FlexRay Interface (FrIf) Joblist Editor</i>,
                select the desired number of <i>Job Triggers</i> per FlexRay communication cycle and
                click the button <b>AUTO JT</b>. To operate the desired number of <i>Job
                Triggers</i> and distribute them evenly across the FlexRay communication cycle.</li>
                <li>In the lower left corner of the <i>FlexRay Interface (FrIf) Joblist Editor</i>,
                click the button <b>AUTO Assign</b> to assign all of the <i>Frame Triggerings</i>
                depicted in the upper part of the <i>FlexRay Interface (FrIf) Joblist Editor</i> to
                the created <i>Job Triggers</i>. In this way you enable a minimum transmission/reception
                latency and an automatically selection of the appropriate <i>Communication Operations</i>.
                </li>
                <li>Click <b>Finish</b>.</li>
              </ol></p>
            <blockquote><b>Note:</b>
              <p>The automatic creation of <i>Job Triggers</i> and the automatic assignment of
              <i>Frame Triggerings</i> to the job triggers is the most efficient way to create a
              valid job list and hence is the recommended way. For very complex configurations this
              automatic approaches might be undesirable. In this case you have the possibility to
              manually create <i>Job Triggers</i> and to manually assign the <i>Frame
              Triggerings</i> to these created <i>Job Triggers</i>.</p>
            </blockquote>
          ]]>
        </description>
      </action>

      <action id="PBUPDATE_ASS_HID" label="Calculate Handle IDs">
        <command description="Unattended wizards dialog" type="configure"
          serialization="dreisoft.tresos.guidedconfig.api.plugin.AutoConfigureDialogCommand(triggerType=HandleId)"/>
        <command description="Calculate Handle IDs"
          serialization="dreisoft.tresos.guidedconfig.api.plugin.AutoConfigureTriggerCommand(triggerType=HandleId)"/>

        <description>
          <![CDATA[
            <h2><b>Goal</b></h2>
              <p>To calculate the handle-IDs (i.e the PDU-IDs and controller-IDs) for the modules of
              the communication stack.</p>
            <h2><u>Procedure</u></h2>
              <p><ul>
                <li>To open the configuration dialog of the <i>Handle ID Calculator</i>, click the
                book icon in the tool bar of the <i>Workflows</i> view.</li>
                <li>To calculate the handle-IDs, double-click this action step in the
                <i>Workflows</i> view. The <i>Handle ID Calculator</i> opens up and calculates
                automatically all handle-IDs.</li>
              </ul></p>
            <blockquote><b>Note:</b>
              <p>By default the <i>Handle ID Calculator</i> calculates all handle-IDs of the loaded
              modules.</p>
            </blockquote>
          ]]>
        </description>
      </action>
    </group>

    <!-- <<<<<<<<<<<<<<<<<<<<<<<<<<<<<< Application >>>>>>>>>>>>>>>>>>>>>>>>>>>>> -->

    <group id="PBUPDATE_CFG" label="Build your configuration" type="choice">
      <description>
        <![CDATA[
          <h2><b>Goal</b></h2>
            <p>To build a binary of your configuration.</p>
          <h2><u>Procedure</u></h2>
          <p>There are two ways to build a binary representation of your configuration:</p>
          <p><ol>
            <li>Use <a href="workflow:///PBUPDATE_CFG_GENERATOR">EB tresos Studio</a></li>
            <li>Use <a href="workflow:///PBUPDATE_CFG_COMPILER">Compiler Toolchain</a></li>
          </ol></p>
        ]]>
      </description>


      <group id="PBUPDATE_CFG_GENERATOR" label="Build your binary with EB tresos Studio" type="sequence">
        <description>
          <![CDATA[
            <h2><b>Goal</b></h2>
              <p>Build and download your configuration.</p>
            <h2><u>Procedure</u></h2>
            <p><ol>
              <li><a href="workflow:///PBUPDATE_BSW_PBCFGM">Enable binary generation support</a></li>
              <li><a href="workflow:///PBUPDATE_CFG_GENERATE">Build a binary of your post-build
              configuration</a></li>
              <li><a href="workflow:///PBUPDATE_CFG_DOWNLOAD">Download post-build
              configuration</a></li>
            </ol></p>
            <h2><b>Output</b></h2>
              <p>The binary is located in the <i>Project Explorer</i> view in the
              directory <tt>output\generated</tt>.</p>
          ]]>
        </description>

        <action label="Configure the PbcfgM module" id="PBUPDATE_BSW_PBCFGM">
          <command description="Open PbcfgM configuration"
            serialization="dreisoft.tresos.launcher2.api.plugin.EditConfigurationCommand(moduleType=PbcfgM)"
            autoadvance="false"/>

          <description>
            <![CDATA[
              <p>To configure the <i>PbcfgM</i> module:</p>
              <p><ol>
                <li>Double-click this action step in the <i>Workflows</i> view. The <i>PbcfgM</i>
                editor opens up in the editor area of EB tresos Studio.</li>
                <li>In the <i>General</i> tab, enable the option <i>PbcfgMBinarySupportEnable</i>.</li>
                <li>The address of your configuration can be set in the <i>PbcfgMConstCfgAddress</i> field.</li>
              </ol></p>
              <blockquote>
                <b>Note:</b>
                <p>For the binary code generation a separate license is needed. If this license is
                   missing an error is reported in the <i>Problems View</i> window.</p>
              </blockquote>
            ]]>
          </description>
        </action>

        <action label="Generate code" id="PBUPDATE_CFG_GENERATE" repeatable="true">
          <command description="Generate code"
            serialization="dreisoft.tresos.launcher2.api.plugin.GeneratorCommand()"/>

          <description>
            <![CDATA[
              <p>To start the generation of you post-build configuration double-click this action
              step in the <i>Workflows</i> view or click the hammer icon in the tool bar
              of EB tresos Studio.</p>
              <p>The binary <i>PBcfgM.srec</i> is located in the <i>Project Explorer</i> view in the
              directory <tt>output\generated</tt>.</p>
            ]]>
          </description>
        </action>

        <action label="Download post-build configuration" id="PBUPDATE_CFG_DOWNLOAD"
          repeatable="true">
          <description>
            <![CDATA[
              <p>Your finished post-build configuration binary is located in the directory
              <tt>output\generated</tt> as <tt>PBcfgM.srec</tt> (Motorola S-record format) file.</p>
              <p>You can use a debugger (e.g. Lauterbach) to download your post-build configuration
                 to your target ECU.</p>
            ]]>
          </description>
        </action>
      </group>

      <group id="PBUPDATE_CFG_COMPILER" label="Build your binary with the compiler toolchain" type="sequence">
        <description>
          <![CDATA[
            <h2><b>Goal</b></h2>
              <p>To generate code, build your configuration and download your configuration.</p>
            <h2><u>Procedure</u></h2>
            <p><ol>
              <li><a href="workflow:///PBUPDATE_CFG_GENERATE_COMPILER">Generate code</a></li>
              <li><a href="workflow:///PBUPDATE_CFG_BUILD_COMPILER">Build a binary of your post-build
              configuration</a></li>
              <li><a href="workflow:///PBUPDATE_CFG_DOWNLOAD_COMPILER">Download post-build
              configuration</a></li>
            </ol></p>
            <h2><b>Output</b></h2>
              <p>The binaries are located in the <i>Project Explorer</i> view in the
              directory <tt>output\bin</tt>.</p>
          ]]>
        </description>

        <action label="Generate code" id="PBUPDATE_CFG_GENERATE_COMPILER" repeatable="true">
          <command description="Generate code"
            serialization="dreisoft.tresos.launcher2.api.plugin.GeneratorCommand()"/>

          <description>
            <![CDATA[
              <p>To start the generation of your project configuration code double-click this action
              step in the <i>Workflows</i> view or click the hammer icon in the tool bar
              of EB tresos Studio.</p>
            ]]>
          </description>
        </action>

        <action label="Build a binary of your post-build configuration" id="PBUPDATE_CFG_BUILD_COMPILER"
          repeatable="true">
          <description>
            <![CDATA[
              <p>To create a binary containing only the post-build configuration for your target
              platform:</p>
              <p><ol>
                <li>Execute <tt>launch.bat</tt></li>
                <li>To build the dependencies, type <tt>make depend_postBuildBinary</tt> in the
                command line.</li>
                <li>To compile and link your software components, type <tt>make postBuildBinary</tt>.
                </li>
              </ol></p>
              ]]>
          </description>
        </action>

        <action label="Download post-build configuration" id="PBUPDATE_CFG_DOWNLOAD_COMPILER"
          repeatable="true">
          <description>
            <![CDATA[
              <p>Your finished post-build configuration binary is located in the directory
              <tt>output\bin</tt> as <tt>*.bin</tt> (binary image) and <tt>*.hex</tt> (Motorola
              S-record format) file.</p>
              <p>You can use a debugger (e.g. Lauterbach) to download your post-build configuration to
              your target ECU.</p>
            ]]>
          </description>
        </action>
      </group>
    </group>

    <action id="PBUPDATE_FIN" label="Finish">
      <description>
        <![CDATA[
          <p>You have updated your post-build configuration successfully.</p>
        ]]>
      </description>
    </action>
  </group>

</workflow>
