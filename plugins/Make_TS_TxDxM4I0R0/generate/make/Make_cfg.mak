# \file
#
# \brief AUTOSAR Make
#
# This file contains the implementation of the AUTOSAR
# module Make.
#
# \author Elektrobit Automotive GmbH, 91058 Erlangen, Germany
#
# Copyright 2005 - 2013 Elektrobit Automotive GmbH
# All rights exclusively reserved for Elektrobit Automotive GmbH,
# unless expressly agreed to otherwise.

#################################################################
## This makefile export parameters derived from the project
## data to make variables

# Only include these variables once
ifndef PROJECT_MODULES

[!IF "$project != ''"!]
# project name in workspace
TRESOS2_PROJECT_NAME := [!"$project"!]

TRESOS2_ECU_ID       := [!"$ecuid"!]

ifeq ($(ENABLED_PLUGINS),)
# In case enabled modules are NOT specified explicitly
# use all enabled modules (including modules, that are NOT generated)
PROJECT_MODULES := [!LOOP "text:order(text:split($enabledModules))"!] [!"substring-before(., '_' )"!][!ENDLOOP!]

else
# otherwise only use generated modules
PROJECT_MODULES := [!LOOP "text:order(text:split($generateModules))"!] [!"substring-before(., '_' )"!][!ENDLOOP!]

endif

# add tresos2 make plugin if not yet contained in SOFTWARE_MODULES
SOFTWARE_MODULES := $(filter-out tresos2,$(SOFTWARE_MODULES))
SOFTWARE_MODULES += tresos2

# There might already modules added to SOFTWARE_MODULES. So add only what's
# not yet contained SOFTWARE_MODULES.
# Duplicated entries are removed by the sort function.
SOFTWARE_MODULES += $(sort $(filter-out $(SOFTWARE_MODULES), $(PROJECT_MODULES)))

# variables defining module versions
[!LOOP "text:order(text:split($enabledModules))"!]
[!"substring-before(., '_' )"!]_VARIANT := [!"substring-after(., '_' )"!]
[!ENDLOOP!]

# target and derivate
TARGET   := [!"$target"!]
DERIVATE := [!"$derivate"!]

# output path for generated files
GEN_OUTPUT_PATH := [!"$generationDir"!]
[!ELSE!]
ECU_ID          := ECU
GEN_OUTPUT_PATH ?= $(PROJECT_ROOT)\output\generated
[!ENDIF!]

# output path for files created by the build environment
PROJECT_OUTPUT_PATH ?= $(GEN_OUTPUT_PATH)\..

# for compatibility reasons we need the AUTOSAR_BASE_OUTPUT_PATH
AUTOSAR_BASE_OUTPUT_PATH ?= $(GEN_OUTPUT_PATH)

endif

[!AUTOSPACING!][!//
[!// Get OS Vendor Id:
[!// If there is an Os configuration..
[!IF "node:exists(as:modconf('Os'))"!]
[!// ..and inside there's a vendor Id and the Vendor Id is set to one (EB) !]
  [!IF "node:exists(as:modconf('Os')/CommonPublishedInformation/VendorId) and as:modconf('Os')/CommonPublishedInformation/VendorId = 1"!]
# Set Os Vendor to EB
OS_VENDOR := EB
  [!ELSE!]
# Set Os Vendor to other (not EB)
OS_VENDOR := OTHER
  [!ENDIF!]

[!ELSE!]
# No Os configuration found, declare Os Vendor empty
OS_VENDOR :=

[!ENDIF!]