# \file
#
# \brief AUTOSAR Make
#
# This file contains the implementation of the AUTOSAR
# module Make.
#
# \author Elektrobit Automotive GmbH, 91058 Erlangen, Germany
#
# Copyright 2005 - 2013 Elektrobit Automotive GmbH
# All rights exclusively reserved for Elektrobit Automotive GmbH,
# unless expressly agreed to otherwise.

#################################################################
# This file belongs to the implementation of the base_make package.
# It contains functionality to create automatic metric checks.

-include $(SSC_ROOT)\Testing_$(Testing_VARIANT)\make\Testing.mak
