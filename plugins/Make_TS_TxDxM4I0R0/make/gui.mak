# \file
#
# \brief AUTOSAR Make
#
# This file contains the implementation of the AUTOSAR
# module Make.
#
# \author Elektrobit Automotive GmbH, 91058 Erlangen, Germany
#
# Copyright 2005 - 2013 Elektrobit Automotive GmbH
# All rights exclusively reserved for Elektrobit Automotive GmbH,
# unless expressly agreed to otherwise.

#################################################################
# This file belongs to the implementation of the base_make package.
# gui.mak contains some statements to layout the command line output.

#################################################################
# The defines START_BLOCK, END_BLOCK, START_MESSAGE, END_MESSAGE
# are used in the PRE_BUILD_BLOCK and the POST_BUILD_BLOCK.
#
define START_BLOCK
	@echo ****************************************
endef

define END_BLOCK
	@echo ****************************************
endef

define START_MESSAGE
	@echo * [build]
endef

define END_MESSAGE
	@echo * FINISHED FILE:
endef

define SEPARATOR
	@echo *
endef

define START_MESSAGE_MKDIR
	@echo * [mkdir]
endef

define START_MESSAGE_CLEAN
	@echo * [clean]
endef


#################################################################
# Message header
FILE_ERROR_HEADER := FILE MISSING:
ERROR_HEADER      := FOUND CONFIGURATION ERROR:
WARNING_HEADER    := WARNING:


#################################################################
# The define BUILD_FILE_NAME is used in PRE_BUILD_BLOCK and
# POST_BUILD_BLOCK to display the name of the current file.
define BUILD_FILE_NAME
	$(notdir $(subst \,/,$(subst $(DEBUG_SUFFIX),,$@)))
endef

#################################################################
# The following examples shows how the output of the define
# PRE_BUILD_BLOCK looks like:
#
#****************************************
#* BUILDING FILE:        timerTRICOREstm.o
#*
define PRE_BUILD_BLOCK
	$(START_MESSAGE)$(BUILD_FILE_NAME)
endef

#################################################################
# Example output of the define POST_BUILD_BLOCK:
#*
#* FINISHED FILE:        timerTRICOREstm.o
#****************************************
define POST_BUILD_BLOCK
endef
