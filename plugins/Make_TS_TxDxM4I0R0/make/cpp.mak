# \file
#
# \brief AUTOSAR Make
#
# This file contains the implementation of the AUTOSAR
# module Make.
#
# \author Elektrobit Automotive GmbH, 91058 Erlangen, Germany
#
# Copyright 2005 - 2013 Elektrobit Automotive GmbH
# All rights exclusively reserved for Elektrobit Automotive GmbH,
# unless expressly agreed to otherwise.

##################################################################
# VARIABLES
##################################################################

############################################################
# CPP specifies the location of the preprocessor included in
# this plugin
#
# used for:
#  - dependency generation
#  - creating proprocessor output
CPP := $(MAKE_CORE_PATH)\tools\gcc\bin\cpp.exe


##################################################################
# MACROS
##################################################################

############################################################
# PREPROCESSOR_DEFINES contains a compiler independent
# definition of preprocessor defines. These defines must be translated
# into the gcc macro syntax -DKEY=Value.
cppGetPreProcessorDefines = $(foreach DEF,$(PREPROCESSOR_DEFINES),-D$($(DEF)_KEY)=$($(DEF)_VALUE))


##################################################################
# TARGETS
##################################################################

##################################################################
# Rule for forcing the execution of another target
force_cpp:

##################################################################
# RULES
##################################################################

############################################################
# Create files holding options for the preprocessor(CPP)
#
%.cpp_options: force_cpp
	$(PRE_BUILD_BLOCK)
	@$(MKDIR) $(subst /,\,$(dir $@))
	@echo -DEMPTY >  $@
	$(foreach i,$(call unique,$(USER_INCLUDE_PATH) $(CC_INCLUDE_PATH) $(CPP_INCLUDE_PATH) $(ASM_INCLUDE_PATH)), \
	   $(call SC_CMD_AR_HELPER,@echo -I$(subst \,/,$i) >> $@ ))
	@echo $(CPP_OPTS) $(call cppGetPreProcessorDefines) $(subst \,/,$(GCC_INCLUDE_PATH)) >> $@
	@$(DOS2UNIX) $@

.PHONY: force_cpp
