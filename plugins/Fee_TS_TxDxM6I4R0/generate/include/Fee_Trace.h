/**
 * \file
 *
 * \brief AUTOSAR Fee
 *
 * This file contains the implementation of the AUTOSAR
 * module Fee.
 *
 * \author Elektrobit Automotive GmbH, 91058 Erlangen, Germany
 *
 * Copyright 2005 - 2013 Elektrobit Automotive GmbH
 * All rights exclusively reserved for Elektrobit Automotive GmbH,
 * unless expressly agreed to otherwise.
 */
#if (!defined FEE_TRACE_H)
#define FEE_TRACE_H
/*==================[inclusions]============================================*/

[!IF "node:exists(as:modconf('Dbg'))"!]
#include <Dbg.h>
[!ENDIF!]

/*==================[macros]================================================*/

#ifndef DBG_FEE_INIT_ENTRY
/** \brief Entry point of function Fee_Init() */
#define DBG_FEE_INIT_ENTRY()
#endif

#ifndef DBG_FEE_STATE
/** \brief Change of Fee_State */
#define DBG_FEE_STATE(a,b)
#endif

#ifndef DBG_FEE_INIT_EXIT
/** \brief Exit point of function Fee_Init() */
#define DBG_FEE_INIT_EXIT()
#endif

#ifndef DBG_FEE_SETMODE_ENTRY
/** \brief Entry point of function Fee_SetMode() */
#define DBG_FEE_SETMODE_ENTRY(a)
#endif

#ifndef DBG_FEE_SETMODE_EXIT
/** \brief Exit point of function Fee_SetMode() */
#define DBG_FEE_SETMODE_EXIT(a)
#endif

#ifndef DBG_FEE_READ_ENTRY
/** \brief Entry point of function Fee_Read() */
#define DBG_FEE_READ_ENTRY(a,b,c,d)
#endif

#ifndef DBG_FEE_READ_EXIT
/** \brief Exit point of function Fee_Read() */
#define DBG_FEE_READ_EXIT(a,b,c,d,e)
#endif

#ifndef DBG_FEE_WRITE_ENTRY
/** \brief Entry point of function Fee_Write() */
#define DBG_FEE_WRITE_ENTRY(a,b)
#endif

#ifndef DBG_FEE_WRITE_EXIT
/** \brief Exit point of function Fee_Write() */
#define DBG_FEE_WRITE_EXIT(a,b,c)
#endif

#ifndef DBG_FEE_CANCEL_ENTRY
/** \brief Entry point of function Fee_Cancel() */
#define DBG_FEE_CANCEL_ENTRY()
#endif

#ifndef DBG_FEE_CANCEL_EXIT
/** \brief Exit point of function Fee_Cancel() */
#define DBG_FEE_CANCEL_EXIT()
#endif

#ifndef DBG_FEE_GETSTATUS_ENTRY
/** \brief Entry point of function Fee_GetStatus() */
#define DBG_FEE_GETSTATUS_ENTRY()
#endif

#ifndef DBG_FEE_GETSTATUS_EXIT
/** \brief Exit point of function Fee_GetStatus() */
#define DBG_FEE_GETSTATUS_EXIT(a)
#endif

#ifndef DBG_FEE_GETJOBRESULT_ENTRY
/** \brief Entry point of function Fee_GetJobResult() */
#define DBG_FEE_GETJOBRESULT_ENTRY()
#endif

#ifndef DBG_FEE_GETJOBRESULT_EXIT
/** \brief Exit point of function Fee_GetJobResult() */
#define DBG_FEE_GETJOBRESULT_EXIT(a)
#endif

#ifndef DBG_FEE_INVALIDATEBLOCK_ENTRY
/** \brief Entry point of function Fee_InvalidateBlock() */
#define DBG_FEE_INVALIDATEBLOCK_ENTRY(a)
#endif

#ifndef DBG_FEE_INVALIDATEBLOCK_EXIT
/** \brief Exit point of function Fee_InvalidateBlock() */
#define DBG_FEE_INVALIDATEBLOCK_EXIT(a,b)
#endif

#ifndef DBG_FEE_GETVERSIONINFO_ENTRY
/** \brief Entry point of function Fee_GetVersionInfo() */
#define DBG_FEE_GETVERSIONINFO_ENTRY(a)
#endif

#ifndef DBG_FEE_GETVERSIONINFO_EXIT
/** \brief Exit point of function Fee_GetVersionInfo() */
#define DBG_FEE_GETVERSIONINFO_EXIT(a)
#endif

#ifndef DBG_FEE_ERASEIMMEDIATEBLOCK_ENTRY
/** \brief Entry point of function Fee_EraseImmediateBlock() */
#define DBG_FEE_ERASEIMMEDIATEBLOCK_ENTRY(a)
#endif

#ifndef DBG_FEE_ERASEIMMEDIATEBLOCK_EXIT
/** \brief Exit point of function Fee_EraseImmediateBlock() */
#define DBG_FEE_ERASEIMMEDIATEBLOCK_EXIT(a,b)
#endif

#ifndef DBG_FEE_JOBENDNOTIFICATION_ENTRY
/** \brief Entry point of function Fee_JobEndNotification() */
#define DBG_FEE_JOBENDNOTIFICATION_ENTRY()
#endif

#ifndef DBG_FEE_JOBENDNOTIFICATION_EXIT
/** \brief Exit point of function Fee_JobEndNotification() */
#define DBG_FEE_JOBENDNOTIFICATION_EXIT()
#endif

#ifndef DBG_FEE_JOBERRORNOTIFICATION_ENTRY
/** \brief Entry point of function Fee_JobErrorNotification() */
#define DBG_FEE_JOBERRORNOTIFICATION_ENTRY()
#endif

#ifndef DBG_FEE_JOBERRORNOTIFICATION_EXIT
/** \brief Exit point of function Fee_JobErrorNotification() */
#define DBG_FEE_JOBERRORNOTIFICATION_EXIT()
#endif

#ifndef DBG_FEE_MAINFUNCTION_ENTRY
/** \brief Entry point of function Fee_MainFunction() */
#define DBG_FEE_MAINFUNCTION_ENTRY()
#endif

#ifndef DBG_FEE_NEXTSTATE
/** \brief Change of Fee_NextState */
#define DBG_FEE_NEXTSTATE(a,b)
#endif

#ifndef DBG_FEE_MAINFUNCTION_EXIT
/** \brief Exit point of function Fee_MainFunction() */
#define DBG_FEE_MAINFUNCTION_EXIT()
#endif

#ifndef DBG_FEE_PROCESSWRITEORINVALIDATEJOB_ENTRY
/** \brief Entry point of function Fee_ProcessWriteOrInvalidateJob() */
#define DBG_FEE_PROCESSWRITEORINVALIDATEJOB_ENTRY()
#endif

#ifndef DBG_FEE_PROCESSWRITEORINVALIDATEJOB_EXIT
/** \brief Exit point of function Fee_ProcessWriteOrInvalidateJob() */
#define DBG_FEE_PROCESSWRITEORINVALIDATEJOB_EXIT()
#endif

#ifndef DBG_FEE_PROCESSINVALIDATEJOB_ENTRY
/** \brief Entry point of function Fee_ProcessInvalidateJob() */
#define DBG_FEE_PROCESSINVALIDATEJOB_ENTRY()
#endif

#ifndef DBG_FEE_PROCESSINVALIDATEJOB_EXIT
/** \brief Exit point of function Fee_ProcessInvalidateJob() */
#define DBG_FEE_PROCESSINVALIDATEJOB_EXIT()
#endif

#ifndef DBG_FEE_CALCULATESPACEFORSS_ENTRY
/** \brief Entry point of function Fee_CalculateSpaceforSS() */
#define DBG_FEE_CALCULATESPACEFORSS_ENTRY()
#endif

#ifndef DBG_FEE_CALCULATESPACEFORSS_EXIT
/** \brief Exit point of function Fee_CalculateSpaceforSS() */
#define DBG_FEE_CALCULATESPACEFORSS_EXIT(a)
#endif

#ifndef DBG_FEE_SETSECTIONSTATUS_ENTRY
/** \brief Entry point of function Fee_SetSectionStatus() */
#define DBG_FEE_SETSECTIONSTATUS_ENTRY(a)
#endif

#ifndef DBG_FEE_SETSECTIONSTATUS_EXIT
/** \brief Exit point of function Fee_SetSectionStatus() */
#define DBG_FEE_SETSECTIONSTATUS_EXIT(a)
#endif

#ifndef DBG_FEE_FILLCACHE_ENTRY
/** \brief Entry point of function Fee_FillCache() */
#define DBG_FEE_FILLCACHE_ENTRY()
#endif

#ifndef DBG_FEE_FILLCACHE_EXIT
/** \brief Exit point of function Fee_FillCache() */
#define DBG_FEE_FILLCACHE_EXIT(a)
#endif

#ifndef DBG_FEE_NEXTSTATEOFFILLCACHE_ENTRY
/** \brief Entry point of function Fee_NextStateOfFillCache() */
#define DBG_FEE_NEXTSTATEOFFILLCACHE_ENTRY()
#endif

#ifndef DBG_FEE_NEXTSTATEOFFILLCACHE_EXIT
/** \brief Exit point of function Fee_NextStateOfFillCache() */
#define DBG_FEE_NEXTSTATEOFFILLCACHE_EXIT()
#endif

#ifndef DBG_FEE_VALIDATECHECKSUM_ENTRY
/** \brief Entry point of function Fee_ValidateChecksum() */
#define DBG_FEE_VALIDATECHECKSUM_ENTRY(a,b)
#endif

#ifndef DBG_FEE_VALIDATECHECKSUM_EXIT
/** \brief Exit point of function Fee_ValidateChecksum() */
#define DBG_FEE_VALIDATECHECKSUM_EXIT(a,b,c)
#endif

#ifndef DBG_FEE_CHECKBLOCKINFOERASED_ENTRY
/** \brief Entry point of function Fee_CheckBlockInfoErased() */
#define DBG_FEE_CHECKBLOCKINFOERASED_ENTRY(a)
#endif

#ifndef DBG_FEE_CHECKBLOCKINFOERASED_EXIT
/** \brief Exit point of function Fee_CheckBlockInfoErased() */
#define DBG_FEE_CHECKBLOCKINFOERASED_EXIT(a,b)
#endif

#ifndef DBG_FEE_ERASEFLASH_ENTRY
/** \brief Entry point of function Fee_EraseFlash() */
#define DBG_FEE_ERASEFLASH_ENTRY(a)
#endif

#ifndef DBG_FEE_ERASEFLASH_EXIT
/** \brief Exit point of function Fee_EraseFlash() */
#define DBG_FEE_ERASEFLASH_EXIT(a)
#endif

#ifndef DBG_FEE_INITIATEBLOCKSWITCH_ENTRY
/** \brief Entry point of function Fee_InitiateBlockSwitch() */
#define DBG_FEE_INITIATEBLOCKSWITCH_ENTRY()
#endif

#ifndef DBG_FEE_INITIATEBLOCKSWITCH_EXIT
/** \brief Exit point of function Fee_InitiateBlockSwitch() */
#define DBG_FEE_INITIATEBLOCKSWITCH_EXIT()
#endif

#ifndef DBG_FEE_CHKFREESPACEFORCOPY_ENTRY
/** \brief Entry point of function Fee_ChkFreeSpaceForCopy() */
#define DBG_FEE_CHKFREESPACEFORCOPY_ENTRY()
#endif

#ifndef DBG_FEE_CHKFREESPACEFORCOPY_EXIT
/** \brief Exit point of function Fee_ChkFreeSpaceForCopy() */
#define DBG_FEE_CHKFREESPACEFORCOPY_EXIT(a)
#endif

#ifndef DBG_FEE_SWITCHSECTIONFAILURE_ENTRY
/** \brief Entry point of function Fee_SwitchSectionFailure() */
#define DBG_FEE_SWITCHSECTIONFAILURE_ENTRY()
#endif

#ifndef DBG_FEE_SWITCHSECTIONFAILURE_EXIT
/** \brief Exit point of function Fee_SwitchSectionFailure() */
#define DBG_FEE_SWITCHSECTIONFAILURE_EXIT()
#endif

#ifndef DBG_FEE_INITIATEJOB_ENTRY
/** \brief Entry point of function Fee_InitiateJob() */
#define DBG_FEE_INITIATEJOB_ENTRY()
#endif

#ifndef DBG_FEE_INITIATEJOB_EXIT
/** \brief Exit point of function Fee_InitiateJob() */
#define DBG_FEE_INITIATEJOB_EXIT()
#endif

#ifndef DBG_FEE_CHKFREESPACE_ENTRY
/** \brief Entry point of function Fee_ChkFreeSpace() */
#define DBG_FEE_CHKFREESPACE_ENTRY()
#endif

#ifndef DBG_FEE_CHKFREESPACE_EXIT
/** \brief Exit point of function Fee_ChkFreeSpace() */
#define DBG_FEE_CHKFREESPACE_EXIT(a)
#endif

#ifndef DBG_FEE_SEARCHCONFIGTABLE_ENTRY
/** \brief Entry point of function Fee_SearchConfigTable() */
#define DBG_FEE_SEARCHCONFIGTABLE_ENTRY(a)
#endif

#ifndef DBG_FEE_SEARCHCONFIGTABLE_EXIT
/** \brief Exit point of function Fee_SearchConfigTable() */
#define DBG_FEE_SEARCHCONFIGTABLE_EXIT(a,b)
#endif

#ifndef DBG_FEE_CALCULATECHECKSUM_ENTRY
/** \brief Entry point of function Fee_CalculateChecksum() */
#define DBG_FEE_CALCULATECHECKSUM_ENTRY(a,b)
#endif

#ifndef DBG_FEE_CALCULATECHECKSUM_EXIT
/** \brief Exit point of function Fee_CalculateChecksum() */
#define DBG_FEE_CALCULATECHECKSUM_EXIT(a,b,c)
#endif

#ifndef DBG_FEE_FILLBLOCKINFO_ENTRY
/** \brief Entry point of function Fee_FillBlockInfo() */
#define DBG_FEE_FILLBLOCKINFO_ENTRY(a,b,c)
#endif

#ifndef DBG_FEE_FILLBLOCKINFO_EXIT
/** \brief Exit point of function Fee_FillBlockInfo() */
#define DBG_FEE_FILLBLOCKINFO_EXIT(a,b,c)
#endif

#ifndef DBG_FEE_WRITESECTIONHEADER_ENTRY
/** \brief Entry point of function Fee_WriteSectionHeader() */
#define DBG_FEE_WRITESECTIONHEADER_ENTRY(a,b,c)
#endif

#ifndef DBG_FEE_WRITESECTIONHEADER_EXIT
/** \brief Exit point of function Fee_WriteSectionHeader() */
#define DBG_FEE_WRITESECTIONHEADER_EXIT(a,b,c)
#endif

#ifndef DBG_FEE_FLSREAD_ENTRY
/** \brief Entry point of function Fee_FlsRead() */
#define DBG_FEE_FLSREAD_ENTRY(a,b)
#endif

#ifndef DBG_FEE_FLSREAD_EXIT
/** \brief Exit point of function Fee_FlsRead() */
#define DBG_FEE_FLSREAD_EXIT(a,b)
#endif

#ifndef DBG_FEE_STARTINIT2_ENTRY
/** \brief Entry point of function Fee_StartInit2() */
#define DBG_FEE_STARTINIT2_ENTRY()
#endif

#ifndef DBG_FEE_STARTINIT2_EXIT
/** \brief Exit point of function Fee_StartInit2() */
#define DBG_FEE_STARTINIT2_EXIT()
#endif

#ifndef DBG_FEE_SETSTATESONFAILURE_ENTRY
/** \brief Entry point of function Fee_SetStatesOnFailure() */
#define DBG_FEE_SETSTATESONFAILURE_ENTRY()
#endif

#ifndef DBG_FEE_SETSTATESONFAILURE_EXIT
/** \brief Exit point of function Fee_SetStatesOnFailure() */
#define DBG_FEE_SETSTATESONFAILURE_EXIT()
#endif

#ifndef DBG_FEE_INITIATEREAD_ENTRY
/** \brief Entry point of function Fee_InitiateRead() */
#define DBG_FEE_INITIATEREAD_ENTRY()
#endif

#ifndef DBG_FEE_INITIATEREAD_EXIT
/** \brief Exit point of function Fee_InitiateRead() */
#define DBG_FEE_INITIATEREAD_EXIT()
#endif

#ifndef DBG_FEE_SFIDLESTATE_ENTRY
/** \brief Entry point of function Fee_SfIdleState() */
#define DBG_FEE_SFIDLESTATE_ENTRY()
#endif

#ifndef DBG_FEE_SFIDLESTATE_EXIT
/** \brief Exit point of function Fee_SfIdleState() */
#define DBG_FEE_SFIDLESTATE_EXIT()
#endif

#ifndef DBG_FEE_SFONENTRYINITREADHEADER0_ENTRY
/** \brief Entry point of function Fee_SfOnEntryInitReadHeader0() */
#define DBG_FEE_SFONENTRYINITREADHEADER0_ENTRY()
#endif

#ifndef DBG_FEE_SFONENTRYINITREADHEADER0_EXIT
/** \brief Exit point of function Fee_SfOnEntryInitReadHeader0() */
#define DBG_FEE_SFONENTRYINITREADHEADER0_EXIT()
#endif

#ifndef DBG_FEE_SFINITREADHEADER0_ENTRY
/** \brief Entry point of function Fee_SfInitReadHeader0() */
#define DBG_FEE_SFINITREADHEADER0_ENTRY()
#endif

#ifndef DBG_FEE_SFINITREADHEADER0_EXIT
/** \brief Exit point of function Fee_SfInitReadHeader0() */
#define DBG_FEE_SFINITREADHEADER0_EXIT()
#endif

#ifndef DBG_FEE_SFONENTRYINITREADHEADER1_ENTRY
/** \brief Entry point of function Fee_SfOnEntryInitReadHeader1() */
#define DBG_FEE_SFONENTRYINITREADHEADER1_ENTRY()
#endif

#ifndef DBG_FEE_SFONENTRYINITREADHEADER1_EXIT
/** \brief Exit point of function Fee_SfOnEntryInitReadHeader1() */
#define DBG_FEE_SFONENTRYINITREADHEADER1_EXIT()
#endif

#ifndef DBG_FEE_SFINITREADHEADER1_ENTRY
/** \brief Entry point of function Fee_SfInitReadHeader1() */
#define DBG_FEE_SFINITREADHEADER1_ENTRY()
#endif

#ifndef DBG_FEE_SFINITREADHEADER1_EXIT
/** \brief Exit point of function Fee_SfInitReadHeader1() */
#define DBG_FEE_SFINITREADHEADER1_EXIT()
#endif

#ifndef DBG_FEE_SFONENTRYINITFILLCACHE_ENTRY
/** \brief Entry point of function Fee_SfOnEntryInitFillCache() */
#define DBG_FEE_SFONENTRYINITFILLCACHE_ENTRY()
#endif

#ifndef DBG_FEE_SFONENTRYINITFILLCACHE_EXIT
/** \brief Exit point of function Fee_SfOnEntryInitFillCache() */
#define DBG_FEE_SFONENTRYINITFILLCACHE_EXIT()
#endif

#ifndef DBG_FEE_SFINITFILLCACHE_ENTRY
/** \brief Entry point of function Fee_SfInitFillCache() */
#define DBG_FEE_SFINITFILLCACHE_ENTRY()
#endif

#ifndef DBG_FEE_SFINITFILLCACHE_EXIT
/** \brief Exit point of function Fee_SfInitFillCache() */
#define DBG_FEE_SFINITFILLCACHE_EXIT()
#endif

#ifndef DBG_FEE_SFONENTRYINITERASE_ENTRY
/** \brief Entry point of function Fee_SfOnEntryInitErase() */
#define DBG_FEE_SFONENTRYINITERASE_ENTRY()
#endif

#ifndef DBG_FEE_SFONENTRYINITERASE_EXIT
/** \brief Exit point of function Fee_SfOnEntryInitErase() */
#define DBG_FEE_SFONENTRYINITERASE_EXIT()
#endif

#ifndef DBG_FEE_SFINITERASE_ENTRY
/** \brief Entry point of function Fee_SfInitErase() */
#define DBG_FEE_SFINITERASE_ENTRY()
#endif

#ifndef DBG_FEE_SFINITERASE_EXIT
/** \brief Exit point of function Fee_SfInitErase() */
#define DBG_FEE_SFINITERASE_EXIT()
#endif

#ifndef DBG_FEE_SFONENTRYINITCHKLASTBLOCK_ENTRY
/** \brief Entry point of function Fee_SfOnEntryInitChkLastBlock() */
#define DBG_FEE_SFONENTRYINITCHKLASTBLOCK_ENTRY()
#endif

#ifndef DBG_FEE_SFONENTRYINITCHKLASTBLOCK_EXIT
/** \brief Exit point of function Fee_SfOnEntryInitChkLastBlock() */
#define DBG_FEE_SFONENTRYINITCHKLASTBLOCK_EXIT()
#endif

#ifndef DBG_FEE_SFINITCHKLASTBLOCK_ENTRY
/** \brief Entry point of function Fee_SfInitChkLastBlock() */
#define DBG_FEE_SFINITCHKLASTBLOCK_ENTRY()
#endif

#ifndef DBG_FEE_SFINITCHKLASTBLOCK_EXIT
/** \brief Exit point of function Fee_SfInitChkLastBlock() */
#define DBG_FEE_SFINITCHKLASTBLOCK_EXIT()
#endif

#ifndef DBG_FEE_SFONENTRYREADBLOCKNUMBER_ENTRY
/** \brief Entry point of function Fee_SfOnEntryReadBlockNumber() */
#define DBG_FEE_SFONENTRYREADBLOCKNUMBER_ENTRY()
#endif

#ifndef DBG_FEE_SFONENTRYREADBLOCKNUMBER_EXIT
/** \brief Exit point of function Fee_SfOnEntryReadBlockNumber() */
#define DBG_FEE_SFONENTRYREADBLOCKNUMBER_EXIT()
#endif

#ifndef DBG_FEE_SFREADBLOCKNUMBER_ENTRY
/** \brief Entry point of function Fee_SfReadBlockNumber() */
#define DBG_FEE_SFREADBLOCKNUMBER_ENTRY()
#endif

#ifndef DBG_FEE_SFREADBLOCKNUMBER_EXIT
/** \brief Exit point of function Fee_SfReadBlockNumber() */
#define DBG_FEE_SFREADBLOCKNUMBER_EXIT()
#endif

#ifndef DBG_FEE_SFONENTRYREADBLOCKDATA_ENTRY
/** \brief Entry point of function Fee_SfOnEntryReadBlockData() */
#define DBG_FEE_SFONENTRYREADBLOCKDATA_ENTRY()
#endif

#ifndef DBG_FEE_SFONENTRYREADBLOCKDATA_EXIT
/** \brief Exit point of function Fee_SfOnEntryReadBlockData() */
#define DBG_FEE_SFONENTRYREADBLOCKDATA_EXIT()
#endif

#ifndef DBG_FEE_SFREADBLOCKDATA_ENTRY
/** \brief Entry point of function Fee_SfReadBlockData() */
#define DBG_FEE_SFREADBLOCKDATA_ENTRY()
#endif

#ifndef DBG_FEE_SFREADBLOCKDATA_EXIT
/** \brief Exit point of function Fee_SfReadBlockData() */
#define DBG_FEE_SFREADBLOCKDATA_EXIT()
#endif

#ifndef DBG_FEE_SFONENTRYSSREADDATA_ENTRY
/** \brief Entry point of function Fee_SfOnEntrySSReadData() */
#define DBG_FEE_SFONENTRYSSREADDATA_ENTRY()
#endif

#ifndef DBG_FEE_SFONENTRYSSREADDATA_EXIT
/** \brief Exit point of function Fee_SfOnEntrySSReadData() */
#define DBG_FEE_SFONENTRYSSREADDATA_EXIT()
#endif

#ifndef DBG_FEE_SFSSREADDATA_ENTRY
/** \brief Entry point of function Fee_SfSSReadData() */
#define DBG_FEE_SFSSREADDATA_ENTRY()
#endif

#ifndef DBG_FEE_SFSSREADDATA_EXIT
/** \brief Exit point of function Fee_SfSSReadData() */
#define DBG_FEE_SFSSREADDATA_EXIT()
#endif

#ifndef DBG_FEE_SFONENTRYSSCOPYINFO_ENTRY
/** \brief Entry point of function Fee_SfOnEntrySSCopyInfo() */
#define DBG_FEE_SFONENTRYSSCOPYINFO_ENTRY()
#endif

#ifndef DBG_FEE_SFONENTRYSSCOPYINFO_EXIT
/** \brief Exit point of function Fee_SfOnEntrySSCopyInfo() */
#define DBG_FEE_SFONENTRYSSCOPYINFO_EXIT()
#endif

#ifndef DBG_FEE_SFSSCOPYINFO_ENTRY
/** \brief Entry point of function Fee_SfSSCopyInfo() */
#define DBG_FEE_SFSSCOPYINFO_ENTRY()
#endif

#ifndef DBG_FEE_SFSSCOPYINFO_EXIT
/** \brief Exit point of function Fee_SfSSCopyInfo() */
#define DBG_FEE_SFSSCOPYINFO_EXIT()
#endif

#ifndef DBG_FEE_SFONENTRYSSCOPYDATA_ENTRY
/** \brief Entry point of function Fee_SfOnEntrySSCopyData() */
#define DBG_FEE_SFONENTRYSSCOPYDATA_ENTRY()
#endif

#ifndef DBG_FEE_SFONENTRYSSCOPYDATA_EXIT
/** \brief Exit point of function Fee_SfOnEntrySSCopyData() */
#define DBG_FEE_SFONENTRYSSCOPYDATA_EXIT()
#endif

#ifndef DBG_FEE_SFSSCOPYDATA_ENTRY
/** \brief Entry point of function Fee_SfSSCopyData() */
#define DBG_FEE_SFSSCOPYDATA_ENTRY()
#endif

#ifndef DBG_FEE_SFSSCOPYDATA_EXIT
/** \brief Exit point of function Fee_SfSSCopyData() */
#define DBG_FEE_SFSSCOPYDATA_EXIT()
#endif

#ifndef DBG_FEE_SFONENTRYSSWRITECOPIED_ENTRY
/** \brief Entry point of function Fee_SfOnEntrySSWriteCopied() */
#define DBG_FEE_SFONENTRYSSWRITECOPIED_ENTRY()
#endif

#ifndef DBG_FEE_SFONENTRYSSWRITECOPIED_EXIT
/** \brief Exit point of function Fee_SfOnEntrySSWriteCopied() */
#define DBG_FEE_SFONENTRYSSWRITECOPIED_EXIT()
#endif

#ifndef DBG_FEE_SFSSWRITECOPIED_ENTRY
/** \brief Entry point of function Fee_SfSSWriteCopied() */
#define DBG_FEE_SFSSWRITECOPIED_ENTRY()
#endif

#ifndef DBG_FEE_SFSSWRITECOPIED_EXIT
/** \brief Exit point of function Fee_SfSSWriteCopied() */
#define DBG_FEE_SFSSWRITECOPIED_EXIT()
#endif

#ifndef DBG_FEE_SFONENTRYSSERASESECTION_ENTRY
/** \brief Entry point of function Fee_SfOnEntrySSEraseSection() */
#define DBG_FEE_SFONENTRYSSERASESECTION_ENTRY()
#endif

#ifndef DBG_FEE_SFONENTRYSSERASESECTION_EXIT
/** \brief Exit point of function Fee_SfOnEntrySSEraseSection() */
#define DBG_FEE_SFONENTRYSSERASESECTION_EXIT()
#endif

#ifndef DBG_FEE_SFSSERASESECTION_ENTRY
/** \brief Entry point of function Fee_SfSSEraseSection() */
#define DBG_FEE_SFSSERASESECTION_ENTRY()
#endif

#ifndef DBG_FEE_SFSSERASESECTION_EXIT
/** \brief Exit point of function Fee_SfSSEraseSection() */
#define DBG_FEE_SFSSERASESECTION_EXIT()
#endif

#ifndef DBG_FEE_SFSECTIONSWITCHING_ENTRY
/** \brief Entry point of function Fee_SfSectionSwitching() */
#define DBG_FEE_SFSECTIONSWITCHING_ENTRY()
#endif

#ifndef DBG_FEE_SFSECTIONSWITCHING_EXIT
/** \brief Exit point of function Fee_SfSectionSwitching() */
#define DBG_FEE_SFSECTIONSWITCHING_EXIT()
#endif

#ifndef DBG_FEE_SFONENTRYWRITEBLOCKINFO_ENTRY
/** \brief Entry point of function Fee_SfOnEntryWriteBlockInfo() */
#define DBG_FEE_SFONENTRYWRITEBLOCKINFO_ENTRY()
#endif

#ifndef DBG_FEE_SFONENTRYWRITEBLOCKINFO_EXIT
/** \brief Exit point of function Fee_SfOnEntryWriteBlockInfo() */
#define DBG_FEE_SFONENTRYWRITEBLOCKINFO_EXIT()
#endif

#ifndef DBG_FEE_SFWRITEBLOCKINFO_ENTRY
/** \brief Entry point of function Fee_SfWriteBlockInfo() */
#define DBG_FEE_SFWRITEBLOCKINFO_ENTRY()
#endif

#ifndef DBG_FEE_SFWRITEBLOCKINFO_EXIT
/** \brief Exit point of function Fee_SfWriteBlockInfo() */
#define DBG_FEE_SFWRITEBLOCKINFO_EXIT()
#endif

#ifndef DBG_FEE_SFONENTRYWRITEBLOCKDATA_ENTRY
/** \brief Entry point of function Fee_SfOnEntryWriteBlockData() */
#define DBG_FEE_SFONENTRYWRITEBLOCKDATA_ENTRY()
#endif

#ifndef DBG_FEE_SFONENTRYWRITEBLOCKDATA_EXIT
/** \brief Exit point of function Fee_SfOnEntryWriteBlockData() */
#define DBG_FEE_SFONENTRYWRITEBLOCKDATA_EXIT()
#endif

#ifndef DBG_FEE_SFWRITEBLOCKDATA_ENTRY
/** \brief Entry point of function Fee_SfWriteBlockData() */
#define DBG_FEE_SFWRITEBLOCKDATA_ENTRY()
#endif

#ifndef DBG_FEE_SFWRITEBLOCKDATA_EXIT
/** \brief Exit point of function Fee_SfWriteBlockData() */
#define DBG_FEE_SFWRITEBLOCKDATA_EXIT()
#endif

#ifndef DBG_FEE_SFONENTRYWRITESECTIONACTIVE_ENTRY
/** \brief Entry point of function Fee_SfOnEntryWriteSectionActive() */
#define DBG_FEE_SFONENTRYWRITESECTIONACTIVE_ENTRY()
#endif

#ifndef DBG_FEE_SFONENTRYWRITESECTIONACTIVE_EXIT
/** \brief Exit point of function Fee_SfOnEntryWriteSectionActive() */
#define DBG_FEE_SFONENTRYWRITESECTIONACTIVE_EXIT()
#endif

#ifndef DBG_FEE_SFWRITESECTIONACTIVE_ENTRY
/** \brief Entry point of function Fee_SfWriteSectionActive() */
#define DBG_FEE_SFWRITESECTIONACTIVE_ENTRY()
#endif

#ifndef DBG_FEE_SFWRITESECTIONACTIVE_EXIT
/** \brief Exit point of function Fee_SfWriteSectionActive() */
#define DBG_FEE_SFWRITESECTIONACTIVE_EXIT()
#endif

#ifndef DBG_FEE_SFONENTRYWRITESECTIONFULL_ENTRY
/** \brief Entry point of function Fee_SfOnEntryWriteSectionFull() */
#define DBG_FEE_SFONENTRYWRITESECTIONFULL_ENTRY()
#endif

#ifndef DBG_FEE_SFONENTRYWRITESECTIONFULL_EXIT
/** \brief Exit point of function Fee_SfOnEntryWriteSectionFull() */
#define DBG_FEE_SFONENTRYWRITESECTIONFULL_EXIT()
#endif

#ifndef DBG_FEE_SFWRITESECTIONFULL_ENTRY
/** \brief Entry point of function Fee_SfWriteSectionFull() */
#define DBG_FEE_SFWRITESECTIONFULL_ENTRY()
#endif

#ifndef DBG_FEE_SFWRITESECTIONFULL_EXIT
/** \brief Exit point of function Fee_SfWriteSectionFull() */
#define DBG_FEE_SFWRITESECTIONFULL_EXIT()
#endif

#ifndef DBG_FEE_SFONENTRYINVALIDATE_ENTRY
/** \brief Entry point of function Fee_SfOnEntryInvalidate() */
#define DBG_FEE_SFONENTRYINVALIDATE_ENTRY()
#endif

#ifndef DBG_FEE_SFONENTRYINVALIDATE_EXIT
/** \brief Exit point of function Fee_SfOnEntryInvalidate() */
#define DBG_FEE_SFONENTRYINVALIDATE_EXIT()
#endif

#ifndef DBG_FEE_SFINVALIDATE_ENTRY
/** \brief Entry point of function Fee_SfInvalidate() */
#define DBG_FEE_SFINVALIDATE_ENTRY()
#endif

#ifndef DBG_FEE_SFINVALIDATE_EXIT
/** \brief Exit point of function Fee_SfInvalidate() */
#define DBG_FEE_SFINVALIDATE_EXIT()
#endif

/*==================[type definitions]======================================*/

/*==================[external function declarations]========================*/

/*==================[external constants]====================================*/

/*==================[external data]=========================================*/

#endif /* (!defined FEE_TRACE_H) */
/*==================[end of file]===========================================*/
