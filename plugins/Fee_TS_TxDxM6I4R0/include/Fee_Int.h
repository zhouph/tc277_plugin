#if (!defined FEE_INT_H)
#define FEE_INT_H
/**
 * \file
 *
 * \brief AUTOSAR Fee
 *
 * This file contains the implementation of the AUTOSAR
 * module Fee.
 *
 * \author Elektrobit Automotive GmbH, 91058 Erlangen, Germany
 *
 * Copyright 2005 - 2013 Elektrobit Automotive GmbH
 * All rights exclusively reserved for Elektrobit Automotive GmbH,
 * unless expressly agreed to otherwise.
 */

/*  MISRA-C:2004 Deviation List
 *
 *  MISRA-1) Deviated Rule: 19.10 (required)
 *    "In the definition of a function-like macro each instance of a parameter shall be enclosed in
 *     parentheses unless it is used as the operand of # or ##."
 *
 *    Reason: The instance of parameter is an array name.
 */

/*==================[inclusions]============================================*/

#include <Fee_Cfg.h>            /* FEE configuration header */

/*==================[macros]================================================*/

/** \brief Value to indicate section 0 */
#define FEE_SECTION_0                         0U

/** \brief Value to indicate section 1 */
#define FEE_SECTION_1                         1U

/** \brief Value to indicate Active section */
#define FEE_SECTION_STATUS_ACTIVE             0xAAAAU


/** \brief Value to indicate Full section */
#define FEE_SECTION_STATUS_FULL               0x5555U

/** \brief Value to indicate Copied section */
#define FEE_SECTION_STATUS_COPIED             0x2222U

/** \brief Value to indicate address corresponding to an inconsistent block in cache
 * This should be flash address type, as the macro is used to update the cache. */
#define FEE_INCONSISTENT_ADDRESS              0xFFFFFFFFU

/** \brief Value to indicate address corresponding to an invalid block in cache
 * This should be flash address type, as the macro is used to update the cache. */
#define FEE_INVALID_BLOCKADDRESS              0xFFFFFFFEU

/** \brief Value to size of block number */
#define FEE_SIZE_BLOCKNUMBER                  2U

/** \brief Value of startup or switch delay */
#define FEE_DELAY                             5U

/** \brief Number of bytes used for block info copy/write field */
#define FEE_WRITE_COPY_STATUS_LENGTH          1U

/** \brief Number of bytes used for block info checksum */
#define FEE_CHECKSUM_SIZE                     1U

/** \brief Index of copy/write status information in blockinfo */
#define FEE_WRITE_STATUS_INDEX                0U

/** \brief Index of block length information in blockinfo */
#define FEE_BLOCKLENGTH_INDEX                 1U

/** \brief Index of block number information in blockinfo */
#define FEE_BLOCKNUMBER_INDEX                 3U

/** \brief Index of checksum information in blockinfo */
#define FEE_CHECKSUM_INDEX                    5U

/** \brief Value of section status
 *  Higher byte carries the status of section 0
 *  Lower byte carries the status of section 1 */
#define FEE_BOTH_SECTION_INCONSISTENT         0xEEEEU
#define FEE_BOTH_SECTION_ACTIVE               0xAAAAU
#define FEE_BOTH_SECTION_FULL                 0x5555U
#define FEE_BOTH_SECTION_COPIED               0x2222U
#define FEE_BOTH_SECTION_ERASED               0xFFFFU

#define FEE_ACTIVEINCONSISTENT_ACTIVE         0xAEAAU
#define FEE_ACTIVEINCONSISTENT_ERASED         0xAEFFU
#define FEE_ACTIVEINCONSISTENT_FULL           0xAE55U

#define FEE_ACTIVE_ACTIVEINCONSISTENT         0xAAAEU
#define FEE_FULL_ACTIVEINCONSISTENT           0x55AEU

#define FEE_ERASED_INCONSISTENT               0xFFEEU
#define FEE_INCONSISTENT_ERASED               0xEEFFU
#define FEE_INCONSISTENT_FULL                 0xEE55U
#define FEE_INCONSISTENT_ACTIVE               0xEEAAU
#define FEE_ACTIVE_INCONSISTENT               0xAAEEU
#define FEE_FULL_INCONSISTENT                 0x55EEU

#define FEE_COPIED_ERASED                     0x22FFU
#define FEE_ERASED_COPIED                     0xFF22U
#define FEE_COPIED_FULL                       0x2255U
#define FEE_FULL_COPIED                       0x5522U

#define FEE_ERASED_FULL                       0xFF55U
#define FEE_FULL_ERASED                       0x55FFU
#define FEE_ACTIVE_FULL                       0xAA55U
#define FEE_FULL_ACTIVE                       0x55AAU
#define FEE_ACTIVE_COPIED                     0xAA22U
#define FEE_COPIED_ACTIVE                     0x22AAU

#define FEE_ACTIVE_ERASED                     0xAAFFU
#define FEE_ERASED_ACTIVE                     0xFFAAU
/** \brief Value to show status of two sections separately */
#define FEE_SECTION1_INCONSISTENT             0x00EEU
#define FEE_SECTION0_INCONSISTENT             0xEE00U
#define FEE_SECTION1_ACTIVEINCONSISTENT       0x00AEU
#define FEE_SECTION0_ACTIVEINCONSISTENT       0xAE00U
#define FEE_SECTION1_ACTIVE                   0x00AAU
#define FEE_SECTION1_FULL                     0x0055U
#define FEE_SECTION1_COPIED                   0x0022U
#define FEE_SECTION1_ERASED                   0x00FFU
#define FEE_SECTION0_ACTIVE                   0xAA00U
#define FEE_SECTION0_FULL                     0x5500U
#define FEE_SECTION0_COPIED                   0x2200U
#define FEE_SECTION0_ERASED                   0xFF00U

#define FEE_SECTION_ACTIVE(SecNo)             \
  ((SecNo)==0U?FEE_SECTION0_ACTIVE:FEE_SECTION1_ACTIVE)
#define FEE_SECTION_FULL(SecNo)               \
  ((SecNo)==0U?FEE_SECTION0_FULL:FEE_SECTION1_FULL)
#define FEE_SECTION_COPIED(SecNo)             \
  ((SecNo)==0U?FEE_SECTION0_COPIED:FEE_SECTION1_COPIED)
#define FEE_SECTION_ERASED(SecNo)             \
  ((SecNo)==0U?FEE_SECTION0_ERASED:FEE_SECTION1_ERASED)
#define FEE_SECTION_INCONSISTENT(SecNo)       \
  ((SecNo)==0U?FEE_SECTION0_INCONSISTENT:FEE_SECTION1_INCONSISTENT)
#define FEE_SECTION_ACTIVEINCONSISTENT(SecNo)       \
  ((SecNo)==0U?FEE_SECTION0_ACTIVEINCONSISTENT:FEE_SECTION1_ACTIVEINCONSISTENT)

/** \brief Macro to find the other section */
#define FEE_OTHER_SECTION(Section)           ((Section)==0U?1U:0U)

/** \brief Value to indicate that the block is present in
* flash as a result of write request */
#define FEE_WRITE_BLOCK                       0xBBU

/** \brief Value to indicate that the block is copied
* from the inactive section */
#define FEE_BLOCK_COPIED                      0xCCU

/** \brief Value to indicate invalid block size */
#define FEE_INVALID_SIZE                      0U

/** \brief Value to fill unused bytes */
#define FEE_PAD_BYTE                          0x00U

/** \brief Number of bytes in block info */
#define FEE_BLOCKINFO_SIZE                    6U

/** \brief value 0x00 */
#define FEE_INDEX_ZERO                        0U

/** \brief Macro to read the word from the uint8 buffer */
/* Deviation MISRA-1 */
#define FEE_READ_WORD(Buffer, Index)                  \
  ((uint16)((uint16)(Buffer[(Index)+1U])<<8U)|(uint16)(Buffer[(Index)]))

/** \brief Macro to read the word from the uint8 buffer */
/* Deviation MISRA-1 <START>*/
#define FEE_WRITE_WORD(Buffer, Index, Word)                      \
  do {                                                           \
    Buffer[(Index)] =    (uint8)((uint16)(Word) & 0xFFU);                 \
    Buffer[(Index)+1U] = (uint8)((uint16)(Word) >> 8U);                   \
  } while(0)
/* Deviation MISRA-1 <STOP>*/

/** \brief Helper macro to trigger a state change and execute the entry function */
#define FEE_TRIGGER_TRANS(targetState)    \
  (Fee_NextState = (targetState))

/** \brief macro to check whether section switching is in progress or not */
#define SECTION_SWITCHING_INPROGRESS() \
  ((0U == Fee_Gv.FeeDelayTimer) && \
    ((TRUE == Fee_Gv.FeeCopyPending) || \
     (TRUE == Fee_Gv.FeeErasePending)\
    )\
  )


/* \brief Macro to find the address at which the headers are written given the section number */
#define FEE_HEADER_ADDRESS(Section)           \
  ((Section)==0U?FEE_HEADER0_ADDRESS:FEE_HEADER1_ADDRESS)

/* \brief Macro to find the section start address given the section number */
#define FEE_SECTION_START_ADDRESS(SecNo)      \
  ((SecNo)==0U?(Fls_AddressType)FEE_SECTION0_START_ADDRESS: \
               (Fls_AddressType)FEE_SECTION1_START_ADDRESS \
  )

/* \brief Macro to find the section end address given the section number */
#define FEE_SECTION_END_ADDRESS(SecNo)        \
  ((SecNo)==0U?FEE_SECTION0_END_ADDRESS:FEE_SECTION1_END_ADDRESS)

/* \brief Macro to find the section size given the section number */
#define FEE_SECTION_SIZE(SecNo)               \
  ((SecNo)==0U?(Fls_LengthType)FEE_SECTION0_SIZE:(Fls_LengthType)FEE_SECTION1_SIZE)

/* \brief The address of the section at which the block infos are read from.
 *
 * While filling the cache, block infos are read together into the internal buffer.
 * The address from the block infos are read depends upon the buffer size. */
#define FEE_READ_INFO_ADDRESS(Section)        \
  (((Section)==0U?(Fls_AddressType)FEE_HEADER0_ADDRESS: \
                  (Fls_AddressType)FEE_HEADER1_ADDRESS)-FEE_BUFFER_SIZE \
  )

/* \brief The address of the section at which the first data block is written */
#define FEE_DATA_ADDRESS(Section)             \
  (((Section)==0U)?(Fls_AddressType)FEE_SECTION0_START_ADDRESS: \
                   (Fls_AddressType)FEE_SECTION1_START_ADDRESS \
  )

/* \brief The address at which the first block info is written */
#define FEE_FIRST_BLOCK_INFO_ADDRESS(Section) \
  ((Section)==0U?FEE_INFO_ADDRESS_SECTION0:FEE_INFO_ADDRESS_SECTION1)

/* \brief The address at which the first data block is to be written in section 0 */
#define FEE_DATA_ADDRESS_SECTION0             (FEE_SECTION0_START_ADDRESS)

/* \brief The address at which the first block info is to be written in section 0 */
#define FEE_INFO_ADDRESS_SECTION0             (FEE_HEADER0_ADDRESS - FEE_BLOCKINFO_ALIGNED_SIZE)

/* \brief The address at which the first data block is to be written in section 1 */
#define FEE_DATA_ADDRESS_SECTION1             (FEE_SECTION1_START_ADDRESS)

/* \brief The address at which the first block info is to be written in section 1 */
#define FEE_INFO_ADDRESS_SECTION1             (FEE_HEADER1_ADDRESS - FEE_BLOCKINFO_ALIGNED_SIZE)

/*==================[type definitions]======================================*/

/** \brief This type represents the current job of the FEE module */
typedef enum
{
  FEE_NO_JOB,        /**< No job is requested */
  FEE_READ_JOB,      /**< Read job is requested */
  FEE_WRITE_JOB,     /**< Write job is requested */
  FEE_INVALIDATE_JOB /**< Invalidate job / Erase immediate job is requested */
}Fee_Job_t;

/** \brief Structure containing the global variables of the module which
 * can be initilized */
/* !LINKSTO Fee.GlobalVariables,1 */
typedef struct
{
  /* Array to store the cache */
  Fls_AddressType FeeCache[FEE_NO_OF_CONFIGURED_BLOCKS];
  /* Address to which next data block to be written */
  Fls_AddressType FeeDataAddress;
  /* Address to which next block info to be written */
  Fls_AddressType FeeInfoAddress;
  /* Address of last copied block */
  Fls_AddressType FeeLastCopiedBlockAddress;
  /* To save user data buffer pointer during Write*/
  P2CONST(uint8, FEE_VAR_NOINIT, FEE_APPL_DATA) FeeBlockDataWriteBuffPtr;
  /* To save user data buffer pointer during Read*/
  P2VAR(uint8, FEE_VAR_NOINIT, FEE_APPL_DATA) FeeBlockDataReadBuffPtr;
  /* Flash job result */
  MemIf_JobResultType FeeFlashJobResult;
  /* Fee Job result */
  MemIf_JobResultType FeeJobResult;
  /* Module status */
  MemIf_StatusType FeeStatus;
  /* To save the current job */
  Fee_Job_t FeeJob;
  /* Index to the block configuration table for a block number */
  uint16 FeeIntBlockNumber;
  /* Index to the block configuration table for a block number that is cancelled */
  uint16 FeeCancelledBlock;
  /* Index of the last copied block */
  uint16 FeeCopyBlockIndex;
  /* Higher byte section 0 status, lower byte section 1 status */
  uint16 FeeSectionStatus;
  /* To save the BlockReadSize parameter of Fee_Read */
  uint16 FeeLength;
  /* To save the BlockReadOffset parameter of Fee_Read */
  uint16 FeeBlockReadOffset;
  /* Internal buffer */
  uint8 FeeBuffer[FEE_BUFFER_SIZE];
  /* Buffer to write block information */
  uint8 FeeBlockInfoBuffer[FEE_BLOCKINFO_ALIGNED_SIZE];
  /* Active section; new blocks will be written to this section */
  uint8 FeeActiveSection;
  /* Timer counter to count the internal operation delay time;
     after startup1 or after writing currently active section as full */
  uint8 FeeDelayTimer;
  /* Startup part1 pending flag */
  boolean FeeStartup1Pending;
  /* Startup part2 pending flag */
  boolean FeeStartup2Pending;
  /* Copy of blocks pending flag */
  boolean FeeCopyPending;
  /* Erase of section pending flag */
  boolean FeeErasePending;
  /* Switch interrupt flag; set when an immediate write request
  is processed in between a switch operation */
  boolean FeeInterruptSwitch;
  /* Flag to show whether the cache is to be filled from both sections */
  boolean FeeFillBoth;
  /* Flag to show whether the active section is full */
  boolean FeeSectionFull;
  /* Flag to show whether the previous job was cancelled */
  boolean FeeJobCancelled;
  /* Flag to show that flash failure happened */
  boolean FeeFlashFailure;
  /* Flag to get if the repair of the last copied block needs to done during initialization */
  boolean FeeCheckCopyReqd;
}Fee_Gv_t;

/** \brief Structure containing user-configuration of block specific parameters
 ** for FEE module. */
typedef struct
{
  uint16 FeeBlockSize; /**< Size of the block configured in bytes */
  uint16 FeeBlockAlignedSize; /**< Size of the block aligned to the virtual page size */
  uint16 FeeBlockNumber; /**< Block number of the block configured */
  boolean FeeImmediateData; /**< To mark whether the block contains immediate data */
}Fee_BlockConfiguration_t;

/** \brief typedefinition for checksum */
typedef uint8 Fee_ChecksumType;

/** \brief This type represents the current state of the FEE module */
typedef enum {
  FEE_UNINIT,
  FEE_IDLE,
  FEE_INIT_READ_HEADER0,
  FEE_INIT_READ_HEADER1,
  FEE_INIT_ERASE,
  FEE_INIT_FILL_CACHE,
  FEE_INIT_CHK_LASTBLOCK,
  FEE_READ_BLOCKNUMBER,
  FEE_READ_BLOCKDATA,
  FEE_WRITE_BLOCK_INFO,
  FEE_WRITE_BLOCK_DATA,
  FEE_WRITE_SECTION_ACTIVE,
  FEE_WRITE_SECTION_FULL,
  FEE_INVALIDATE,
  FEE_SECTION_SWITCHING,
  FEE_SS_READ_DATA,
  FEE_SS_COPY_INFO,
  FEE_SS_COPY_DATA,
  FEE_SS_WRITE_COPIED,
  FEE_SS_ERASE_SECTION,
  FEE_NUM_STATES,
  FEE_STATE_INVALID
}Fee_State_t;

/*==================[external function declarations]========================*/

/*==================[internal function declarations]========================*/

/*==================[external constants]====================================*/

/*==================[internal constants]====================================*/

/*==================[external data]=========================================*/

#define FEE_START_SEC_VAR_NO_INIT_UNSPECIFIED
#include <MemMap.h>

/* \brief Structure which contains all the state related parameters */
extern VAR(Fee_Gv_t, FEE_VAR_NOINIT) Fee_Gv;

#define FEE_STOP_SEC_VAR_NO_INIT_UNSPECIFIED
#include <MemMap.h>

#define FEE_START_SEC_CONFIG_DATA_UNSPECIFIED
#include <MemMap.h>

/** \brief Fee Block Configuration
 **
 ** Contains the block specific parameters configured by the user of the
 ** FEE module */
extern CONST(Fee_BlockConfiguration_t, FEE_CONST)
  Fee_BlockCfg[FEE_NO_OF_CONFIGURED_BLOCKS];

#define FEE_STOP_SEC_CONFIG_DATA_UNSPECIFIED
#include <MemMap.h>

#define FEE_START_SEC_VAR_INIT_UNSPECIFIED
#include <MemMap.h>

/* \brief Variable which shows the internal state of the module */
extern VAR(Fee_State_t, FEE_VAR) Fee_State;

extern VAR(Fee_State_t, FEE_VAR) Fee_NextState;

#define FEE_STOP_SEC_VAR_INIT_UNSPECIFIED
#include <MemMap.h>

/*==================[internal data]=========================================*/

/*==================[external function definitions]=========================*/

/*==================[internal function definitions]=========================*/

/*==================[end of file]===========================================*/
#endif /* if !defined( FEE_INT_H ) */
