#if (!defined FEE_CBK_H)
#define FEE_CBK_H
/**
 * \file
 *
 * \brief AUTOSAR Fee
 *
 * This file contains the implementation of the AUTOSAR
 * module Fee.
 *
 * \author Elektrobit Automotive GmbH, 91058 Erlangen, Germany
 *
 * Copyright 2005 - 2013 Elektrobit Automotive GmbH
 * All rights exclusively reserved for Elektrobit Automotive GmbH,
 * unless expressly agreed to otherwise.
 */

/*==================[inclusions]============================================*/

#include <Fee_Version.h> /* the module's version information */

/*==================[macros]================================================*/

/*==================[type definitions]======================================*/

/*==================[external function declarations]========================*/
#define FEE_START_SEC_CODE
#include <MemMap.h>

/** \brief Callback function to notify the successful completion of a flash job
 **
 ** This is called by the underlying flash driver to report
 ** the successful completion of an asynchronous operation.
 **
 ** \ServiceID{16}
 ** \Reentrancy{Non Reentrant}
 ** \Synchronicity{Synchronous} */
/* !LINKSTO FEE069,1 */
extern FUNC(void,FEE_CODE) Fee_JobEndNotification(void);

/** \brief Callback function to notify the failure of a flash job
 **
 ** This is called by the underlying flash driver to report
 ** the failure of an asynchronous operation.
 **
 ** \ServiceID{17}
 ** \Reentrancy{Non Reentrant}
 ** \Synchronicity{Synchronous} */
/* !LINKSTO FEE069,1 */
extern FUNC(void,FEE_CODE) Fee_JobErrorNotification(void);

#define FEE_STOP_SEC_CODE
#include <MemMap.h>

/*==================[internal function declarations]========================*/

/*==================[external constants]====================================*/

/*==================[internal constants]====================================*/

/*==================[external data]=========================================*/

/*==================[internal data]=========================================*/

/*==================[external function definitions]=========================*/

/*==================[internal function definitions]=========================*/

/*==================[end of file]===========================================*/
#endif /* if !defined( FEE_CBK_H ) */
