[!/**
 * \file
 *
 * \brief AUTOSAR Dem
 *
 * This file contains the implementation of the AUTOSAR
 * module Dem.
 *
 * \author Elektrobit Automotive GmbH, 91058 Erlangen, Germany
 *
 * Copyright 2005 - 2013 Elektrobit Automotive GmbH
 * All rights exclusively reserved for Elektrobit Automotive GmbH,
 * unless expressly agreed to otherwise.
 */!][!//
[!//
[!/*=== define maximum space needed ===*/!][!//
[!MACRO "Indent", "Length"!][!/*
  */!][!FOR "i" = "1" TO "$Length"!][!/*
    */!][!WS!][!/*
  */!][!ENDFOR!][!/*
*/!][!ENDMACRO!]
