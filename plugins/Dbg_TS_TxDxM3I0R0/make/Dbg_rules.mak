# \file
#
# \brief AUTOSAR Dbg
#
# This file contains the implementation of the AUTOSAR
# module Dbg.
#
# \author Elektrobit Automotive GmbH, 91058 Erlangen, Germany
#
# Copyright 2005 - 2013 Elektrobit Automotive GmbH
# All rights exclusively reserved for Elektrobit Automotive GmbH,
# unless expressly agreed to otherwise.

#################################################################
# DEFINITIONS


#################################################################
# REGISTRY
LIBRARIES_TO_BUILD   += Dbg_src

Dbg_src_FILES += \
    $(Dbg_CORE_PATH)\src\Dbg.c \
    $(Dbg_CORE_PATH)\src\Dbg_Com.c \
    $(Dbg_CORE_PATH)\src\Dbg_Core.c \
    $(Dbg_CORE_PATH)\src\Dbg_Hooks.c \
    $(Dbg_OUTPUT_PATH)\src\Dbg_Rte_Hooks.c \
    $(Dbg_OUTPUT_PATH)\src\Dbg_Trace_Function_Hooks.c \
    $(Dbg_OUTPUT_PATH)\src\Dbg_Trace_State_Hooks.c \
    $(Dbg_OUTPUT_PATH)\src\Dbg_Trace_Generic_Function_Hooks.c \
    $(Dbg_OUTPUT_PATH)\src\Dbg_Trace_Generic_State_Hooks.c

ifeq ($(TARGET), WINDOWS)
ifeq ($(DERIVATE), WIN32X86)
Dbg_src_FILES += \
    $(Dbg_CORE_PATH)\src\Dbg_TcpIpIsr.c \
    $(Dbg_CORE_PATH)\src\Dbg_TcpIpServer.c \
    $(Dbg_CORE_PATH)\src\Dbg_TcpIpWinIf.c
endif
endif

Dbg_lib_FILES        :=

MAKE_CLEAN_RULES     +=
MAKE_GENERATE_RULES  +=
MAKE_COMPILE_RULES   +=
#MAKE_DEBUG_RULES    +=
MAKE_CONFIG_RULES    +=
#MAKE_ADD_RULES      +=

define defineDbgLibOutputPATH
$(1)_OBJ_OUTPUT_PATH := $(Dbg_lib_LIB_OUTPUT_PATH)
endef

$(foreach SRC,$(basename $(notdir $(subst \,/,$(Dbg_lib_FILES)))),$(eval $(call defineDbgLibOutputPATH,$(SRC))))

ifeq ($(TARGET), WINDOWS)
ifeq ($(DERIVATE), WIN32X86)

ifndef WIN32_LIB_PATH
ifeq ($(TOOLCHAIN), vc)
  $(error WIN32_LIB_PATH must be defined and point to the \
      folder containing WSock32.Lib if the standard VirtualBus \
      library is in use)
else
  $(error WIN32_LIB_PATH must be defined and point to the \
      folder containing libwsock32.a if the standard VirtualBus \
      library is in use)
endif # TOOLCHAIN = vc
endif # WIN32_LIB_PATH undefined

ifeq ($(TOOLCHAIN), vc)
LIBRARIES_LINK_ONLY += $(WIN32_LIB_PATH)\WSock32.Lib
else
LIBRARIES_LINK_ONLY += $(WIN32_LIB_PATH)\libwsock32.a
endif

endif # DERIVATE = WIN32X86
endif # TARGET = WINDOWS

#################################################################
# DEPENDENCIES (only for assembler files)
#

#################################################################
# RULES

#################################################################
# Include possible target specific stuff
-include $(Dbg_CORE_PATH)\make\Dbg_rules_$(TARGET)_$(DERIVATE).mak

