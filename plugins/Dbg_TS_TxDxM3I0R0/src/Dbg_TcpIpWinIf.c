/**
 * \file
 *
 * \brief AUTOSAR Dbg
 *
 * This file contains the implementation of the AUTOSAR
 * module Dbg.
 *
 * \author Elektrobit Automotive GmbH, 91058 Erlangen, Germany
 *
 * Copyright 2005 - 2013 Elektrobit Automotive GmbH
 * All rights exclusively reserved for Elektrobit Automotive GmbH,
 * unless expressly agreed to otherwise.
 */

/*
 *  Misra-C:2004 Deviations:
 *
 *  MISRA-1) Deviated Rule: 19.1 (advisory)
 *    '#include' statements in a file should only be preceded by other
 *    preprocessor directives or comments.
 *
 *    Reason:
 *    AUTOSAR compiler and memory abstraction.
 *
*/

#include <Os.h>
#include <Dbg_TcpIpWinIf.h>

#include <Dbg_Cfg_Hw.h>

#if (DBG_TCPIP_SUPPORT == 1)

/*==================[inclusions]============================================*/

/*==================[version check]=========================================*/

/*==================[macros]================================================*/

/*==================[type definitions]======================================*/

/*==================[external function declarations]========================*/

/*==================[internal function declarations]========================*/

/*==================[external constants]====================================*/

/*==================[internal constants]====================================*/

/*==================[external data]=========================================*/

/*==================[internal data]=========================================*/

/*==================[external function definitions]=========================*/
#define DBG_START_SEC_CODE
/* Deviation MISRA-1 */
#include <MemMap.h>

/*
 * We need an interrupt to return from the Windows thread context into the
 * Autosar Os context. Unfortunately, we cannot trigger the interrupt directly
 * from the Windows receive thread, as there are symbol collisions between
 * Autosar Os and Windows (e.g. SetEvent()). Thus, we need an extra helper
 * function Dbg_TriggerRxInterrupt().
 */  
void Dbg_TcpTriggerRxInterrupt(void)
{
  OS_WINDOWSTriggerInterrupt(Dbg_Rx_Interrupt_ISR_VECTOR);
}

#define DBG_STOP_SEC_CODE
/* Deviation MISRA-1 */
#include <MemMap.h>
/*==================[internal function definitions]=========================*/

#endif /* (DBG_TCPIP_SUPPORT == 1) */
/*==================[end of file]===========================================*/
