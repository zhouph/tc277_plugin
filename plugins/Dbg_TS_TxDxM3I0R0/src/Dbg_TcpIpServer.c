/**
 * \file
 *
 * \brief AUTOSAR Dbg
 *
 * This file contains the implementation of the AUTOSAR
 * module Dbg.
 *
 * \author Elektrobit Automotive GmbH, 91058 Erlangen, Germany
 *
 * Copyright 2005 - 2013 Elektrobit Automotive GmbH
 * All rights exclusively reserved for Elektrobit Automotive GmbH,
 * unless expressly agreed to otherwise.
 */

/*
 *  Misra-C:2004 Deviations:
 *
 *  MISRA-1) Deviated Rule: 6.3 (advisory)
 *    'typedefs' that indicate size and signedness should be used
 *    in place of the basic types.
 *
 *    Reason:
 *    Basic types are required by the Tcp/Ip socket API as defined
 *    in the POSIX standard.
 *
*/

/*========================================[inclusions]============================================*/

#include <Dbg_TcpIp.h>
#include <Dbg_TcpIpWinIf.h>
#include <stdio.h>
#include <string.h>
#include <windows.h>
#include <Winsock.h>
#include <Dbg_Cfg_Hw.h>

#if (DBG_TCPIP_SUPPORT == 1)

/*========================================[macros]================================================*/

/*========================================[type definitions]======================================*/

/*========================================[external function declarations]========================*/

/*========================================[internal function declarations]========================*/

static void Dbg_TcpClientThread(void);
static void Dbg_TcpReceiveThread(void);
/* Deviation MISRA-1 */
static void Dbg_TcpTranslateCommand(unsigned char *buf);

/*========================================[external constants]====================================*/

/*========================================[internal constants]====================================*/

/*========================================[external data]=========================================*/

/* Deviation MISRA-1 */
unsigned int    Dbg_TcpServerFlag = 0U;
/* Deviation MISRA-1 */
unsigned int    Dbg_TcpClientFlag = 0U;
/* Deviation MISRA-1 */
unsigned char   Dbg_TcpBuffer[DBG_TCP_BUFLEN] = {0};

CRITICAL_SECTION Dbg_CriticalSection;

/*========================================[internal data]=========================================*/

static SOCKET Dbg_TcpServer;
static SOCKET Dbg_TcpClient;

/*========================================[external function definitions]=========================*/

/* Initialitze the TCP/IP server */
/* Deviation MISRA-1 */
unsigned int Dbg_TcpInit(unsigned short int port)
{
  WSADATA data;
	struct sockaddr_in server_address;

  /* Initialize Winsock */
  if(WSAStartup(MAKEWORD(2U, 2U),&data) != 0U)
  {
    return 0U;
  }

  Dbg_InitializeCriticalSection();

  /* Prepare the server socket address structure */
  memset( (char *) &(server_address), 0, sizeof(server_address));
	server_address.sin_family      = AF_INET;
	server_address.sin_port        = htons(port);
	server_address.sin_addr.s_addr = INADDR_ANY;

  /* Open the socket */
  Dbg_TcpServer = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);
	if(Dbg_TcpServer == INVALID_SOCKET)
	{
    return 0U;
	}

  /* Bind to the socket */
	if(bind( Dbg_TcpServer, (struct sockaddr *)(&(server_address)), sizeof(server_address)) != 0U)
	{
		return 0U;
	}

  /* Start listening on the socket */
  if(listen(Dbg_TcpServer, 10) != 0U)
	{
		return 0U;
	}

  /* Create a thread for accepting connections */
  if(CreateThread(NULL, 0, (LPTHREAD_START_ROUTINE)Dbg_TcpClientThread, NULL, 0, 0) == NULL)
  {
    return 0U;
  }

  Dbg_TcpServerFlag = 1U;

  /* Deviation MISRA-1 */
  return (unsigned int)Dbg_TcpServer;
}

/* Transmit len bytes of buffer */ 
/* Deviation MISRA-1 */
int Dbg_TcpTransmit(unsigned char *buffer, int len)
{
  /* Deviation MISRA-1 */
  int ret;

  /* Deviation MISRA-1 */
  ret=send(Dbg_TcpClient, (char *)buffer, len, 0U);

  if(ret == SOCKET_ERROR)
  {
    return -1;
  }
  else
  {
    return len;
  }
}

void Dbg_InitializeCriticalSection(void)
{
  InitializeCriticalSection(&Dbg_CriticalSection);
}

void Dbg_EnterCriticalSection(void)
{
  EnterCriticalSection(&Dbg_CriticalSection);
}

void Dbg_LeaveCriticalSection(void)
{
  LeaveCriticalSection(&Dbg_CriticalSection);
}

/*========================================[internal function definitions]=========================*/

/* Thread for accepting incoming connections */
static void Dbg_TcpClientThread(void)
{
  /* Deviation MISRA-1 */
  int clientlen;
	struct sockaddr_in client_address;
  clientlen = sizeof(client_address);

  while(1)
  {
    /* Accept the connection */
    Dbg_TcpClient = accept(Dbg_TcpServer, (struct sockaddr*)&client_address, &clientlen);
    if(Dbg_TcpClient != INVALID_SOCKET)
    {
      /* Create a new thread for handling inbound data */
      if(CreateThread(NULL, 0, (LPTHREAD_START_ROUTINE)Dbg_TcpReceiveThread, NULL, 0, 0) == NULL)
      {
        Dbg_TcpClientFlag = 0U;
      }
      else
      {
        Dbg_TcpClientFlag = 1U;
      }
    }
    else
    {
      Dbg_TcpClientFlag = 0U;
    }
  }
}

/* Receive incoming data
 * Since TriggerInterrupt returns immediately, we protect the
 * whole thing with a Windows critical section shared with the
 * Dbg_Rx_Interrupt ISR. Because recv() cannot be called from
 * within a critical section, we copy the received data into
 * a protected buffer which is then processed by the ISR.
 */
static void Dbg_TcpReceiveThread(void)
{
  /* Deviation MISRA-1 */
  int  len;
  /* Deviation MISRA-1 */
  int  i;
  /* Deviation MISRA-1 */
  char buffer[DBG_TCP_BUFLEN];

  do
  {
    len=recv(Dbg_TcpClient, buffer, DBG_TCP_BUFLEN, 0U);
    if(len==SOCKET_ERROR)
    {
      len = 0;
    }
    else
    {
      Dbg_EnterCriticalSection();
      for(i=0U; i<len; i++)
      {
        Dbg_TcpBuffer[i]=buffer[i];
      }
      Dbg_TcpTranslateCommand(Dbg_TcpBuffer);
      Dbg_TcpTriggerRxInterrupt();
      Dbg_LeaveCriticalSection();
    }
  }while(len);

  closesocket(Dbg_TcpClient);

  Dbg_TcpClientFlag = 0U;
}

/* Deviation MISRA-1 */
static void Dbg_TcpTranslateCommand(unsigned char *buf)
{
  switch (buf[0])
  {
      /* global */
    case 'X':
      buf[0]=255;
      break;
    case 'x':
      buf[0]=255;
      break;
    case 'G':
      buf[0]=253;
      buf[1]=1;
      break;
    case 'g':
      buf[0]=253;
      buf[1]=0;
      break;
    case 'T':
      buf[0]=251;
      buf[1]=1;
      break;
    case 't':
      buf[0]=251;
      buf[1]=0;
      break;
      /* did specific */
    case 'D':
      buf[0]=252;
      buf[2]=1;
      break;
    case 'd':
      buf[0]=252;
      buf[2]=0;
      break;
    case 'E':
      buf[0]=250;
      buf[2]=1;
      break;
    case 'e':
      buf[0]=250;
      buf[2]=0;
      break;
    case 'B':
      buf[0]=249;
      buf[2]=1;
      break;
    case 'b':
      buf[0]=249;
      buf[2]=0;
      break;
    case 'C':
      buf[0]=246;
      break;
    case 'c':
      buf[0]=245;
      break;
    case 'M':
      buf[0]=247;
      break;
    case 'm':
      buf[0]=248;
      break;
    case 'S':
      buf[0]=37;
      break;
    default:
      /* Do nothing */
      break;
  }
}

#endif /* (DBG_TCPIP_SUPPORT == 1) */
/** @} doxygen end group definition */
/*========================================[end of file]===========================================*/
