/**
 * \file
 *
 * \brief AUTOSAR Dbg
 *
 * This file contains the implementation of the AUTOSAR
 * module Dbg.
 *
 * \author Elektrobit Automotive GmbH, 91058 Erlangen, Germany
 *
 * Copyright 2005 - 2013 Elektrobit Automotive GmbH
 * All rights exclusively reserved for Elektrobit Automotive GmbH,
 * unless expressly agreed to otherwise.
 */


/**
 ** This file defines all Dbg module used configuration dependend data. The library
 ** is build dependend on this symbols.
 ** The configuration items (generated file macros) are transformed to
 ** symbols to be usable by the library.
 ** */

/*
 *  Misra-C:2004 Deviations:
 *
 *  MISRA-1) Deviated Rule: 11.3 (advisory)
 *    A cast should not be performed between a pointer type and an integral type.
 *
 *    Reason:
 *    Pointers of different type need to be stored in the same structure.
 *    All pointers are converted into uint32, thus no problems are to be
 *    expected.
 *
 *  MISRA-2) Deviated Rule: 19.1 (advisory)
 *    '#include' statements in a file should only be preceded by other
 *    preprocessor directives or comments.
 *
 *    Reason:
 *    AUTOSAR compiler and memory abstraction.
 *
 */

/*==================[inclusions]============================================*/

#include <Dbg.h>
#include <Dbg_Internal.h>
#include <Dbg_Cfg_Internal.h>
#if (DBG_TCPIP_SUPPORT==1)
#include <Dbg_TcpIp.h>
#endif

/*==================[version check]=========================================*/

/*==================[macros]================================================*/

/*------------------[AUTOSAR vendor identification check]-------------------*/

#if (!defined DBG_VENDOR_ID) /* configuration check */
#error DBG_VENDOR_ID must be defined
#endif

#if (DBG_VENDOR_ID != 1U) /* vendor check */
#error DBG_VENDOR_ID has wrong vendor id
#endif

/*------------------[AUTOSAR release version identification check]----------*/

#if (!defined DBG_AR_RELEASE_MAJOR_VERSION) /* configuration check */
#error DBG_AR_RELEASE_MAJOR_VERSION must be defined
#endif

/* major version check */
#if (DBG_AR_RELEASE_MAJOR_VERSION != 4U)
#error DBG_AR_RELEASE_MAJOR_VERSION wrong (!= 4U)
#endif

#if (!defined DBG_AR_RELEASE_MINOR_VERSION) /* configuration check */
#error DBG_AR_RELEASE_MINOR_VERSION must be defined
#endif

/* minor version check */
#if (DBG_AR_RELEASE_MINOR_VERSION != 0U )
#error DBG_AR_RELEASE_MINOR_VERSION wrong (!= 0U)
#endif

#if (!defined DBG_AR_RELEASE_REVISION_VERSION) /* configuration check */
#error DBG_AR_RELEASE_REVISION_VERSION must be defined
#endif

/* revision version check */
#if (DBG_AR_RELEASE_REVISION_VERSION != 3U )
#error DBG_AR_RELEASE_REVISION_VERSION wrong (!= 3U)
#endif

/*------------------[AUTOSAR module version identification check]-----------*/

#if (!defined DBG_SW_MAJOR_VERSION) /* configuration check */
#error DBG_SW_MAJOR_VERSION must be defined
#endif

/* major version check */
#if (DBG_SW_MAJOR_VERSION != 3U)
#error DBG_SW_MAJOR_VERSION wrong (!= 3U)
#endif

#if (!defined DBG_SW_MINOR_VERSION) /* configuration check */
#error DBG_SW_MINOR_VERSION must be defined
#endif

/* minor version check */
#if (DBG_SW_MINOR_VERSION < 0U)
#error DBG_SW_MINOR_VERSION wrong (< 0U)
#endif

#if (!defined DBG_SW_PATCH_VERSION) /* configuration check */
#error DBG_SW_PATCH_VERSION must be defined
#endif

/* patch version check */
#if (DBG_SW_PATCH_VERSION < 4U)
#error DBG_SW_PATCH_VERSION wrong (< 4U)
#endif

/*==================[type definitions]======================================*/

/*==================[external function declarations]========================*/

/*==================[internal function declarations]========================*/

/*==================[external data]=========================================*/

/*==================[external constants]====================================*/

/*------------------[DID config,data and buffer]----------------------------*/

/*
 * !LINKSTO Dbg.FileStructure.Dbg015,1,
 * !        Dbg.Functional.Buffering.Dbg199,1
 */
#define DBG_START_SEC_VAR_UNSPECIFIED
/* Deviation MISRA-2 */
#include <MemMap.h>
#if (DBG_NUM_DYNAMIC_DID > 0)
/*
 * !LINKSTO Dbg.Functional.Buffering.Dbg197,1
 */
VAR (Dbg_AddrSizePairType,DBG_VAR) Dbg_VarDIDAddrSizePairs[DBG_NUM_DYNAMIC_DID];
#endif

VAR (Dbg_StateType,DBG_VAR) Dbg_DIDStates[DBG_NUM_DID]=DBG_DID_STATE_CFG;

VAR (Dbg_BufferType,DBG_VAR) Dbg_RingBuffer[DBG_SIZE_OF_RING_BUFFER];
VAR (Dbg_BufferType,DBG_VAR) Dbg_DirectTxBuffer[DBG_SIZE_OF_DIRECT_BUFFER];
VAR (Dbg_BufferSizeType,DBG_VAR) Dbg_BufferCount=0;

VAR (Dbg_StateType,DBG_VAR) Dbg_State = DBG_DRV_STATE_CFG;

VAR (boolean,DBG_VAR) Dbg_isInit = FALSE;
#define DBG_STOP_SEC_VAR_UNSPECIFIED
/* Deviation MISRA-2 */
#include <MemMap.h>

#define DBG_START_SEC_CONST_UNSPECIFIED
/* Deviation MISRA-2 */
#include <MemMap.h>
/* Deviation MISRA-1 */
CONST(Dbg_AddrSizePairType,DBG_CONST) Dbg_ConstDIDAddrSizePairs[DBG_NUM_CONST_ADDR_SIZE_PAIRS]= DBG_CONST_ADDR_SIZE_CFG;

CONST(Dbg_DIDConfigType,DBG_CONST) Dbg_DIDConfigs[DBG_NUM_DID]=DBG_DID_CFG;


CONST (Dbg_ConfigType,DBG_CONST) Dbg_Config =
 {
    DBG_NUM_STATIC_DID,
    DBG_NUM_DYNAMIC_DID,
    DBG_NUM_PREDEF_DID,
    DBG_NUM_STATIC_DID+DBG_NUM_DYNAMIC_DID, /* number of standdar DID's = start of predefinded DID's */
    DBG_NUM_DID,
    DBG_DRV_DEFAULT_CFG,
    &Dbg_DIDConfigs[0],
    &Dbg_DIDStates[0]
 };

#if (DBG_DEV_ERROR_DETECT==STD_ON)
#if (DBG_NUM_PREDEF_DID!=21)
#error "number of predefined DID's change, must be adapted for lookup DID to API id"
#endif
CONST(uint8,DBG_CONST) Dbg_PredefDidToApiId[DBG_NUM_PREDEF_DID]=
{
    (uint8)DBG_API_I_INVALID, /* not used */
    (uint8)DBG_API_I_INVALID, /* not used */
    (uint8)DBG_API_I_INVALID, /* not used */
    (uint8)DBG_API_I_INVALID, /* not used */
    (uint8)DBG_API_POST_TASK_HOOK,
    (uint8)DBG_API_PRE_TASK_HOOK,
    (uint8)DBG_API_TRACE_RUNNABLE_TERMINATE,
    (uint8)DBG_API_TRACE_RUNNABLE_START,
    (uint8)DBG_API_TRACE_DET_CALL,
    (uint8)DBG_API_TRACE_RTE_COM_CALLBACK,
    (uint8)DBG_API_TRACE_RTE_VFB_SIGNAL_RECEIVE,
    (uint8)DBG_API_TRACE_RTE_VFB_SIGNAL_SEND,
    (uint8)DBG_API_TRACE_RTE_COM_SIGNAL_IV,
    (uint8)DBG_API_TRACE_RTE_COM_SIGNAL_RX,
    (uint8)DBG_API_TRACE_RTE_COM_SIGNAL_TX,
    (uint8)DBG_API_TRACE_DET_CALL,
    (uint8)DBG_API_TRACE_TIMESTAMP,
    (uint8)DBG_API_TRACE_FUNCTION_EXIT,
    (uint8)DBG_API_TRACE_FUNCTION_ENTRY,
    (uint8)DBG_API_I_INVALID, /* not used */
    (uint8)DBG_API_I_INVALID  /* not used */
};
#endif

#define DBG_STOP_SEC_CONST_UNSPECIFIED
/* Deviation MISRA-2 */
#include <MemMap.h>

/*-----------------[TpPduR buffer and state data]---------------------------*/
#if (DBG_COM_VIA_TP_PDUR==STD_ON)

#define DBG_START_SEC_CONST_UNSPECIFIED
/* Deviation MISRA-2 */
#include <MemMap.h>

CONST(Dbg_ComTpPduRConfigType,DBG_CONST) Dbg_ComTpPduRConfig = DBG_COM_TP_PDUR_CFG;

#define DBG_STOP_SEC_CONST_UNSPECIFIED
/* Deviation MISRA-2 */
#include <MemMap.h>

#define DBG_START_SEC_VAR_UNSPECIFIED
/* Deviation MISRA-2 */
#include <MemMap.h>

VAR(uint8,DBG_VAR) Dbg_ComTpBuffer[DBG_COM_TX_PDU_LENGTH+DBG_COM_TP_ADD_CHUNK];
VAR(Dbg_ComTpStateType,DBG_VAR) Dbg_ComTpState[DBG_COM_TP_DOUBLE_BUFFER]=
{
    /* normal DID state */
    /* Deviation MISRA-1 */
    {0,0,0,DBG_COM_TX_PDU_LENGTH,&Dbg_ComTpBuffer[0],NULL_PTR,0},
    /* acknowledge used state */
    /* Deviation MISRA-1 */
    {0,0,0,DBG_COM_TP_ADD_CHUNK,&Dbg_ComTpBuffer[DBG_COM_TX_PDU_LENGTH],NULL_PTR,0}
};

#define DBG_STOP_SEC_VAR_UNSPECIFIED
/* Deviation MISRA-2 */
#include <MemMap.h>

#endif
/*==================[internal constants]====================================*/

/*==================[internal data]=========================================*/

/*==================[external function definitions]=========================*/
FUNC(void, DBG_CODE) Dbg_Init(void)
{
#if ((DBG_TCPIP_SUPPORT==1) && (DBG_DEV_ERROR_DETECT == STD_ON))
    uint32  TcpServer;
#endif

    Dbg_State = Dbg_Config.initState;

    /* must be done prior to the isInit change because the init flag
     * is used by CoreGetTimestamp() to detect that time stamp source (Os or Gpt)
     * is ready to use */
    Dbg_CoreTimestampInit();

    /* used for timestamp processing change */
    Dbg_isInit = TRUE;

#if (DBG_TCPIP_SUPPORT==1)
#if (DBG_DEV_ERROR_DETECT == STD_ON)
    TcpServer = (uint32)Dbg_TcpInit(DBG_TCPIP_PORT);
    if (TcpServer == 0U)
    {
      DBG_DET_REPORT_ERROR(DBG_API_INIT, DBG_E_I_SOCKET);
    }
    else
#else
    (void)Dbg_TcpInit(DBG_TCPIP_PORT);
#endif      
#endif
    {
#if (DBG_LAUTERBACH_TARGET_SUPPORT==STD_ON)
      Dbg_Fdx_Init();
#elif (DBG_LAUTERBACH_DIRECT_SUPPORT==STD_OFF)
      /* enable continous send if TX will not be explicit requested
       * by host */
      if( (Dbg_Config.cfgFlags & DBG_CFG_FLAG_TX_BUFFER_ON_REQ)==0U )
      {
          Dbg_StartContinousSend();
      }
      else
      {
          /* default is no TX */
      }
#endif
    }
}

FUNC(void, DBG_CODE) Dbg_DeInit(void)
{
#if (DBG_LAUTERBACH_DIRECT_SUPPORT==STD_OFF)
    Dbg_StopSend();
#endif /* #if (DBG_LAUTERBACH_DIRECT_SUPPORT==STD_OFF) */
    Dbg_isInit = FALSE;
    Dbg_State = Dbg_Config.defaultState;
    Dbg_CoreTimestampDeInit();
}

/*==================[internal function definitions]=========================*/

/** @} doxygen end group definition */
/*==================[end of file]===========================================*/
