/**
 * \file
 *
 * \brief AUTOSAR Dbg
 *
 * This file contains the implementation of the AUTOSAR
 * module Dbg.
 *
 * \author Elektrobit Automotive GmbH, 91058 Erlangen, Germany
 *
 * Copyright 2005 - 2013 Elektrobit Automotive GmbH
 * All rights exclusively reserved for Elektrobit Automotive GmbH,
 * unless expressly agreed to otherwise.
 */
#if (!defined DBG_CORE_H)
#define DBG_CORE_H

/*
 *  Misra-C:2004 Deviations:
 *
 *  MISRA-1) Deviated Rule: 8.12 (required)
 *    When an array is declared with external linkage, its size shall be
 *    stated explicitely or defined implicitely by initialisation.
 *
 *    Reason:
 *    Value is only calculated in .c file and not available here.
 *
 *  MISRA-2) Deviated Rule: 19.7 (advisory)
 *  A function should be used in preference to a function-like macro.
 *
 *    Reason:
 *    Three reasons for the usage of function-like macros:
 *    - Performance
 *    - Readability
 *    - Possibility to switch Dbg functionality "completely off"
 *
 *  MISRA-3) Deviated Rule: 19.1 (advisory)
 *    '#include' statements in a file should only be preceded by other
 *    preprocessor directives or comments.
 *
 *    Reason:
 *    AUTOSAR compiler and memory abstraction.
 *
*/

/*==================[inclusions]============================================*/
#include <TSAutosar.h>     /* EB specific standard types */
#include <Std_Types.h>     /* AUTOSAR standard types */
#include <Dbg_Types.h>
#include <Dbg_Internal.h>

/*==================[version check]=========================================*/

/*==================[macros]================================================*/
/** \brief merge DID state with global state
 **
 ** This macro returns the current DID state. The specific DID state depends
 ** on his own and the Dbg global state. But not all DBG_STAT_* flags
 ** are merged, some of it must be or'ed.
 **
 ** \note parameter types are Dbg_StateType and big enough for the DBG_STATE_*
 ** values.
 **
 ** Note: I skip the atomic access while reading globaleState twice.
 **      Think the compiler (optimizer) is good using the same register for
 **      it. FIXME: critical lock because it could be that global state
 **      will be changed higher prior.
 **
 ** \param[in] didState current DID state
 ** \param[in] globalState current Dbg overal state
 ** \param[out] returnState current state of the DID used for processing
 */
/* Deviation MISRA-2 */
#define DBG_STATE_GET_OVERAL(didState,globalState,returnState) \
    do{ \
        /* merge global state with current did state */ \
        (returnState)=(Dbg_StateType)(((Dbg_StateType)((didState)&(globalState))&DBG_STATE_MASK)); \
        /* extend additional global state flags */ \
        (returnState)|=(Dbg_StateType)((Dbg_StateType)(globalState)&((Dbg_StateType)(~DBG_STATE_MASK))); \
    }while(0)

/** \brief merge relevant flags
 **
 ** This mask is used by DBG_STATE_GET_OVERAL to merge the state.
 ** All flags out of this mask are or'ed.
 */
#define DBG_STATE_MASK              0x0Fu

/** \brief state flag handling
 **
 ** Flags the Dbg_StateType can have. The first flags must be matching
 ** with the SWS specification because the will be used directly for
 ** transport protocol purpose. The macro DBG_MASK_FRAME_STATE is used
 ** by the code for masking TP relevant flags.
 ** All other flags are used by the implementation.
 */
#define DBG_STATE_INIT              0U
/* used for DID and global state */
#define DBG_STATE_DATA_COLLECT      1U
#define DBG_STATE_TIMESTAMP         2U
#define DBG_STATE_BUFFERED          4U
#define DBG_STATE_ACTIVE            8U
/* additional global state */
#define DBG_STATE_DIRECT_TX         16U
/* Deviation MISRA-2 */
#define DBG_MASK_FRAME_STATE(state) ((state)&0x07U)

/** \brief minimal header file a DID frame can have */
#define DBG_MIN_HEADER_SIZE         2U

/** \brief static state flag handling
 **
 **
 */
#define DBG_S_STATE_INIT            0U
#define DBG_S_STATE_CONFIGURED      1U

/*==================[type definitions]======================================*/
/** \brief type used for core lock state
 **
 ** Each core handled pointer (direct, ring buffer read and ring buffer write)
 ** has his own lock state.
 */
typedef enum
{
    DBG_BUFFER_FULL,
    DBG_BUFFER_EMPTY,
    DBG_BUFFER_LOCKED,
    DBG_BUFFER_READ_LOCKED,
    DBG_BUFFER_WRITE_LOCKED
}Dbg_BufferLockState;

typedef struct
{
  P2VAR(uint8, AUTOMATIC, DBG_APPL_DATA) Src;
  uint8 DID;
} Dbg_BufferInfoWriteType;

typedef struct
{
  P2VAR(uint8, AUTOMATIC, DBG_APPL_DATA) Dst;
  uint8 Size;
} Dbg_BufferInfoReadType;

/*==================[external function declarations]========================*/
#define DBG_START_SEC_CODE
/* Deviation MISRA-3 */
#include <MemMap.h>

#if (DBG_MEMORY_PROTECTION_SUPPORT==STD_OFF)
extern FUNC(void, DBG_CODE) Dbg_CoreMemWrite
(
  P2VAR(void, AUTOMATIC, DBG_APPL_DATA) Params
);
extern FUNC(void, DBG_CODE) Dbg_CoreMemRead
(
  P2VAR(void, AUTOMATIC, DBG_APPL_DATA) Params
);
#endif

#define DBG_STOP_SEC_CODE
/* Deviation MISRA-3 */
#include <MemMap.h>

/*==================[internal function declarations]========================*/

/*==================[external constants]====================================*/

/*==================[internal constants]====================================*/

/*==================[external data]=========================================*/
#define DBG_START_SEC_VAR_UNSPECIFIED
/* Deviation MISRA-3 */
#include <MemMap.h>

/* Deviation MISRA-1 */
extern VAR(Dbg_BufferType,DBG_VAR) Dbg_RingBuffer[];
/* Deviation MISRA-1 */
extern VAR(Dbg_BufferType,DBG_VAR) Dbg_DirectTxBuffer[];
extern VAR(Dbg_BufferSizeType,DBG_VAR) Dbg_BufferCount;

#define DBG_STOP_SEC_VAR_UNSPECIFIED
/* Deviation MISRA-3 */
#include <MemMap.h>
/*==================[internal data]=========================================*/

/*==================[external function definitions]=========================*/

/*==================[internal function definitions]=========================*/

/** @} doxygen end group definition */
#endif /* if !defined( DBG_CORE_H ) */
/*==================[end of file]===========================================*/
