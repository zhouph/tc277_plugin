/**
 * \file
 *
 * \brief AUTOSAR Dbg
 *
 * This file contains the implementation of the AUTOSAR
 * module Dbg.
 *
 * \author Elektrobit Automotive GmbH, 91058 Erlangen, Germany
 *
 * Copyright 2005 - 2013 Elektrobit Automotive GmbH
 * All rights exclusively reserved for Elektrobit Automotive GmbH,
 * unless expressly agreed to otherwise.
 */

/*
 *  Misra-C:2004 Deviations:
 *
 *  MISRA-1) Deviated Rule: 19.1 (advisory)
 *    '#include' statements in a file should only be preceded by other
 *    preprocessor directives or comments.
 *
 *    Reason:
 *    AUTOSAR compiler and memory abstraction.
 *
*/

#if (!defined DBG_TCPIP_WIN_IF_H)
#define DBG_TCPIP_WIN_IF_H

/*==================[includes]==============================================*/

/*==================[version check]=========================================*/

/*==================[macros]================================================*/

/*==================[type definitions]======================================*/

/*==================[external constants]====================================*/

/*==================[external data]=========================================*/

/*==================[external function declarations]========================*/
#define DBG_START_SEC_CODE
/* Deviation MISRA-1 */
#include <MemMap.h>

extern void Dbg_TcpTriggerRxInterrupt(void);

#define DBG_STOP_SEC_CODE
/* Deviation MISRA-1 */
#include <MemMap.h>
#endif /* if !defined( DBG_TCPIP_WIN_IF_H ) */
/*==================[end of file]===========================================*/
