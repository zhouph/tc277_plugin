/**
 * \file
 *
 * \brief AUTOSAR Dbg
 *
 * This file contains the implementation of the AUTOSAR
 * module Dbg.
 *
 * \author Elektrobit Automotive GmbH, 91058 Erlangen, Germany
 *
 * Copyright 2005 - 2013 Elektrobit Automotive GmbH
 * All rights exclusively reserved for Elektrobit Automotive GmbH,
 * unless expressly agreed to otherwise.
 */
#if (!defined DBG_TCPIP_H)
#define DBG_TCPIP_H

/*
 *  Misra-C:2004 Deviations:
 *
 *  MISRA-1) Deviated Rule: 6.3 (advisory)
 *    'typedefs' that indicate size and signedness should be used
 *    in place of the basic types.
 *
 *    Reason:
 *    Basic types are required by the Tcp/Ip socket API as defined
 *    in the POSIX standard.
 *
*/

/*==================[includes]==============================================*/

/*==================[version check]=========================================*/

/*==================[macros]================================================*/

#define DBG_TCP_BUFLEN  8U

/*==================[type definitions]======================================*/

/*==================[external constants]====================================*/

/*==================[external data]=========================================*/

/* Deviation MISRA-1 */
extern unsigned int   Dbg_TcpServerFlag;
/* Deviation MISRA-1 */
extern unsigned int   Dbg_TcpClientFlag;
/* Deviation MISRA-1 */
extern unsigned char  Dbg_TcpBuffer[DBG_TCP_BUFLEN];

/*==================[external function declarations]========================*/

/* Deviation MISRA-1 */
extern unsigned int Dbg_TcpInit(unsigned short int port);

/* Deviation MISRA-1 */
extern int Dbg_TcpTransmit(unsigned char *buffer, int len);

extern void Dbg_InitializeCriticalSection(void);
extern void Dbg_EnterCriticalSection(void);
extern void Dbg_LeaveCriticalSection(void);

#endif /* if !defined( DBG_TCPIP_H ) */
/*==================[end of file]===========================================*/
