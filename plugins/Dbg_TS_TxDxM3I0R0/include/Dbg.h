/**
 * \file
 *
 * \brief AUTOSAR Dbg
 *
 * This file contains the implementation of the AUTOSAR
 * module Dbg.
 *
 * \author Elektrobit Automotive GmbH, 91058 Erlangen, Germany
 *
 * Copyright 2005 - 2013 Elektrobit Automotive GmbH
 * All rights exclusively reserved for Elektrobit Automotive GmbH,
 * unless expressly agreed to otherwise.
 */
#if (!defined DBG_H)
#define DBG_H
/**
 ** Unique functionality provided by single C function implementation will
 ** be dispatched to other API functions with equal behavior by macros.
 **
 ** Note: it is not possible to get function pointers for all API functions
 **
 ** !LINKSTO Dbg.FileStructure.Dbg008,1
 **/

/*
 *  Misra-C:2004 Deviations:
 *
 *  MISRA-1) Deviated Rule: 1.4 (required)
 *  The compiler/linker shall be checked to ensure that 31 character
 *  significance and case sensitivity are supported for external
 *  identifiers.
 *
 *    Reason:
 *    All compilers used in this project support up to 31 characters.
 *
 *  MISRA-2) Deviated Rule: 5.1 (required)
 *  Identifiers (internal and external) shall not rely on the
 *  significance of more than 31 characters.
 *
 *    Reason:
 *    All identifiers differ within the first 31 characters.
 *
 *  MISRA-3) Deviated Rule: 19.7 (advisory)
 *  A function should be used in preference to a function-like macro.
 *
 *    Reason:
 *    Three reasons for the usage of function-like macros:
 *    - Performance
 *    - Readability
 *    - Possibility to switch Dbg functionality "completely off"
 *
 *  MISRA-5) Deviated Rule: 19.1 (advisory)
 *    '#include' statements in a file should only be preceded by other
 *    preprocessor directives or comments.
 *
 *    Reason:
 *    AUTOSAR compiler and memory abstraction.
 */

/*==================[inclusions]============================================*/

/*
 * !LINKSTO Dbg.FileStructure.Dbg016,1,
 * !        Dbg.API.Dbg096,1
 */
#include <TSAutosar.h>     /* EB specific standard types */
#include <Std_Types.h>     /* AUTOSAR standard types */
#include <Dbg_Cfg.h>       /* public configuration part */
#include <Dbg_Version.h>   /* version header */

/*
 * !LINKSTO Dbg.FileStructure.Dbg014, 1
 */
#if (DBG_TRACE_DET==STD_ON)
#include <Det.h>
#endif

#if (DBG_TRACE_GENERIC_FUNC==STD_ON)
#include "Dbg_Trace_Generic_Function_Hooks.h"
#include "Dbg_Trace_Function_Hooks.h"
#endif

#if (DBG_TRACE_GENERIC_STATE==STD_ON)
#include "Dbg_Trace_Generic_State_Hooks.h"
#include "Dbg_Trace_State_Hooks.h"
#endif

/*==================[macros]================================================*/

/*------------------[name of predef DID's]----------------------------------*/
/** \brief predefined DID value */
#define DBG_DID_STATUS                235U
/** \brief predefined DID value */
#define DBG_DID_RESERVED_236          236U
/** \brief predefined DID value */
#define DBG_DID_GENERIC_FUNC          237U
/** \brief predefined DID value */
#define DBG_DID_GENERIC_STATE         238U
/** \brief predefined DID value */
#define DBG_DID_POST_TASK_HOOK        239U
/** \brief predefined DID value */
#define DBG_DID_PRE_TASK_HOOK         240U
/** \brief predefined DID value */
#define DBG_DID_RUNNABLE_TERMINATE    241U
/** \brief predefined DID value */
#define DBG_DID_RUNNABLE_START        242U
/** \brief predefined DID value */
#define DBG_DID_RTE_CALL              243U
/** \brief predefined DID value */
#define DBG_DID_RTE_COM_CALLBACK      244U
/** \brief predefined DID value */
#define DBG_DID_RTE_VFB_RECEIVE       245U
/** \brief predefined DID value */
#define DBG_DID_RTE_VFB_SEND          246U
/** \brief predefined DID value */
#define DBG_DID_RTE_COM_SIG_IV        247U
/** \brief predefined DID value */
#define DBG_DID_RTE_COM_SIG_TX        248U
/** \brief predefined DID value */
#define DBG_DID_RTE_COM_SIG_RX        249U
/** \brief predefined DID value */
#define DBG_DID_DET_CALL              250U
/** \brief predefined DID value */
#define DBG_DID_TIMESTAMP             251U
/** \brief predefined DID value */
#define DBG_DID_FUNC_EXIT             252U
/** \brief predefined DID value */
#define DBG_DID_FUNC_ENTRY            253U
/** \brief predefined DID value */
#define DBG_DID_RESERVED_254          254U
/** \brief predefined DID value */
#define DBG_DID_RESERVED_255          255U

/*------------------[DEV ERROR]---------------------------------------------*/
/** \brief invalid DID development error code
 **
 ** Note: If the DID id is invalid it is not possible
 ** to determine which API function the problem has.
 ** In this case the service id could be one of DBG_API_I_*. */
#define DBG_E_INVALID_DID                                           1U
/** \brief re-entrance problem development error code
 **
 ** Release of write locked memory not possible. Current
 ** write or read lock was preempted and the memory was
 ** release from other content. It is not an expected error. */
#define DBG_E_I_REENTRANCE                                          2U
/** \brief requested buffer size not equal used
 **        development error code
 **
 ** The requested buffer size is not equal the amount
 ** of data to be saved by the function. It is not an
 ** expected error. */
#define DBG_E_I_MEM_SIZE                                            3U
/** \brief confirmation without request development error code
 **
 ** Confirmation was received but no request was done before. This
 ** error occur also if the Dbg module was not initialized before host requests. */
#define DBG_E_I_CONFIRM                                             4U
/** \brief unlock memory fail development error code
 **
 ** The current locked memory pointer does not match the buffer space handled by the
 ** core part. It is not an expected error. */
#define DBG_E_I_MEM_FREE                                            5U
/** \brief module not initialized development error code
 **
 ** In generally the Dbg module runs without initialisation. But some
 ** actions, state changes and functionallity needs it before execution. */
#define DBG_E_I_NO_INIT                                             6U
/** \brief requested action does not match static configuration development error code
 **
 ** For example: Timestamp is statically disabled in the configuration, it is not
 ** possible to enable at runtime. Same for changing DID type from direct
 ** to buffered if no buffer configured. */
#define DBG_E_I_CFG                                                 7U
/** \brief TCP/IP socket creation failed
 **
 ** The Dbg module uses the EB tresos WinCore TCP/IP support, but the creation
 ** of a TCP/IP socket for data transfer failed. */
#define DBG_E_I_SOCKET                                              8U


/** \brief service id of Dbg_Init() */
#define DBG_API_INIT                                            0x01U
/** \brief service id of Dbg_DeInit() */
#define DBG_API_DE_INIT                                         0x24U
/** \brief service id of Dbg_GetVersionInfo() */
#define DBG_API_GET_VERSION_INFO                                0x03U
/** \brief service id of Dbg_CollectDid() */
#define DBG_API_COLLECT_DID                                     0x04U
/** \brief service id of Dbg_TraceFunctionEntry() */
#define DBG_API_TRACE_FUNCTION_ENTRY                            0x05U
/** \brief service id of Dbg_TraceFunctionExit() */
#define DBG_API_TRACE_FUNCTION_EXIT                             0x06U
/** \brief service id of Dbg_PreTaskHook() */
#define DBG_API_PRE_TASK_HOOK                                   0x07U
/** \brief service id of Dbg_PostTaskHook() */
#define DBG_API_POST_TASK_HOOK                                  0x08U
/** \brief service id of Dbg_TraceTimestamp() */
#define DBG_API_TRACE_TIMESTAMP                                 0x09U
/** \brief service id of Dbg_TraceDetCall() */
#define DBG_API_TRACE_DET_CALL                                  0x0aU
/** \brief service id of Dbg_TraceRTEComSignalTx() */
#define DBG_API_TRACE_RTE_COM_SIGNAL_TX                         0x0bU
/** \brief service id of Dbg_TraceRTEComSignalRx() */
#define DBG_API_TRACE_RTE_COM_SIGNAL_RX                         0x0cU
/** \brief service id of Dbg_TraceRTEComSignalIv() */
#define DBG_API_TRACE_RTE_COM_SIGNAL_IV                         0x0dU
/** \brief service id of Dbg_TraceRTEVfbSignalSend() */
/* Deviation MISRA-1 */
/* Deviation MISRA-2 */
#define DBG_API_TRACE_RTE_VFB_SIGNAL_SEND                       0x0eU
/** \brief service id of Dbg_TraceRTEVfbSignalReceive() */
/* Deviation MISRA-1 */
/* Deviation MISRA-2 */
#define DBG_API_TRACE_RTE_VFB_SIGNAL_RECEIVE                    0x0fU
/** \brief service id of Dbg_TraceRTEComCallback() */
#define DBG_API_TRACE_RTE_COM_CALLBACK                          0x10U
/** \brief service id of Dbg_TraceRTECall() */
#define DBG_API_TRACE_RTE_CALL                                  0x11U
/** \brief service id of Dbg_TraceRunnableStart() */
#define DBG_API_TRACE_RUNNABLE_START                            0x12U
/** \brief service id of Dbg_TraceRunnableTerminate() */
/* Deviation MISRA-1 */
/* Deviation MISRA-2 */
#define DBG_API_TRACE_RUNNABLE_TERMINATE                        0x13U
/** \brief service id of Dbg_EnableDidCollection() */
#define DBG_API_ENABLE_DID_COLLECTION                           0x14U
/** \brief service id of Dbg_ActivateDid() */
#define DBG_API_ACTIVATE_DID                                    0x15U
/** \brief service id of Dbg_UseLocalTimestampActivation() */
/* Deviation MISRA-1 */
/* Deviation MISRA-2 */
#define DBG_API_USE_LOCAL_TIMESTAMP_ACTIVATION                  0x16U
/** \brief service id of Dbg_ActivateTimestamp() */
#define DBG_API_ACTIVATE_TIMESTAMP                              0x17U
/** \brief service id of Dbg_ActivateDidBuffering() */
#define DBG_API_ACTIVATE_DID_BUFFERING                          0x18U
/** \brief service id of Dbg_ClearBuffer() */
#define DBG_API_CLEAR_BUFFER                                    0x19U
/** \brief service id of Dbg_SendNextEntries() */
#define DBG_API_SEND_NEXT_ENTRIES                               0x1aU
/** \brief service id of Dbg_StartContinousSend() */
#define DBG_API_START_CONTINOUS_SEND                            0x1bU
/** \brief service id of Dbg_StopSend() */
#define DBG_API_STOP_SEND                                       0x1cU
/** \brief service id of Dbg_SetCycleTime() */
#define DBG_API_SET_CYCLE_TIME                                  0x1dU
/** \brief service id of Dbg_Confirmation() */
#define DBG_API_CONFIRMATION                                    0x20U
/** \brief service id of Dbg_Indication() */
#define DBG_API_INDICATION                                      0x21U
/** \brief service id of Dbg_ComInit() */
#define DBG_API_COM_INIT                                        0x02U
/** \brief service id of Dbg_ComDeInit() */
#define DBG_API_COM_DE_INIT                                     0x25U
/** \brief service id of Dbg_Transmit() */
#define DBG_API_TRANSMIT                                        0x1eU
/** \brief service id of Dbg_TransmitSegmentedData() */
#define DBG_API_TRANSMIT_SEGMENTED_DATA                         0x1fU
/** \brief service id of Dbg_RxIndication() */
#define DBG_API_RX_INDICATION                                   0x22U
/** \brief service id of Dbg_TxConfirmation() */
#define DBG_API_TX_CONFIRMATION                                 0x23U
/** \brief service id of Dbg_PeriodicSamplingFunction() */
/* Deviation MISRA-1 */
/* Deviation MISRA-2 */
#define DBG_API_PERIODIC_SAMPLING_FUNCTION                      0x1eU
/** \brief service id of Dbg_MainFunction() */
#define DBG_API_MAIN_FUNCTION                                   0x30U

/** \brief service id of internal function Dbg_CoreMemWrite() */
#define DBG_API_I_CORE_MEM_WRITE                                0x40U
/** \brief service id of internal function Dbg_CoreMemRead() */
#define DBG_API_I_CORE_MEM_READ                                 0x41U

/** \brief service id of internal function
 **        Dbg_CoreMemWriteLock()
 **
 ** This function will be used by all API functions to
 ** allocate memory.The function returns a pointer to the
 ** memory where data can be saved. The amount of space
 ** depends on DID. The buffer is write locked until
 ** the API function has transfered the data and call
 ** Dbg_CoreMemWriteUnlock(). The function returns a null
 ** pointer if no memory available or Dbg state blocks
 ** further collection */
#define DBG_API_I_CORE_MEM_WRITE_LOCK                           0x50U
/** \brief service id of internal function
 **        Dbg_CoreMemWriteUnlock()
 **
 ** Counterpart of Dbg_CoreMemWriteLock() tagging the
 ** locked memory as valid. Data is now ready to transmit. */
#define DBG_API_I_CORE_MEM_WRITE_UNLOCK                         0x51U
/** \brief service id of internal function
 **        Dbg_CoreMemReadLock()
 **
 ** This function will be used by the Com part to
 ** read DID related frame space. In remains locked until
 ** it is completely transfered to the transport layer used.
 ** If there is no DID data ready to be send the returned
 ** pointer is NULL. */
#define DBG_API_I_CORE_MEM_READ_LOCK                            0x52U
/** \brief service id of internal function
 **        Dbg_CoreMemReadUnock()
 **
 ** Counterpart of Dbg_CoreMemReadLock(). Unlock the read locked
 ** memory. */
#define DBG_API_I_CORE_MEM_READ_UNLOCK                          0x53U
/** \brief service id of internal function
 **        Dbg_Trace_U16_U8_U8_U8()
 **
 ** General function to be used for tracing specific
 ** parameter sequence. */
#define DBG_API_I_TRACE_U16_U8_U8_U8                            0x54U
/** \brief service id of internal function
 **        Dbg_Trace_U16_U8_U8()
 **
 ** General function to be used for tracing specific
 ** parameter sequence. */
#define DBG_API_I_TRACE_U16_U8_U8                               0x55U
/** \brief service id of internal function
 **        Dbg_Trace_U8()
 **
 ** General function to be used for tracing specific
 ** parameter sequence. */
#define DBG_API_I_TRACE_U8                                      0x56U
/** \brief service id of internal function
 **        Dbg_Trace_U16_U8_U8_U8_U32()
 **
 ** General function to be used for tracing specific
 ** parameter sequence. */
/* Deviation MISRA-1 */
/* Deviation MISRA-2 */
#define DBG_API_I_TRACE_U16_U8_U8_U8_U32                        0x57U

/** \brief service id not possible to determine
 **
 ** This API id will be reported if the Dbg module can
 ** not determine the API. */
#define DBG_API_I_INVALID                                       0xFFU


/*==================[macro checks]==========================================*/

/*==================[type definitions]======================================*/
/** \brief used for general Dbg_Activate() function behavior
 **
 ** This type is used to inform the general Dbg_Activate() function what to do.
 ** All DID specific or global state changes are dispatched to this function.
 **
 ** Note: the values of this enum are equal the API id used for development
 ** error support.
 */
typedef enum
{
   DBG_DID_ACTIVE = DBG_API_ACTIVATE_DID,   /**< DBG_API_ACTIVATE_DID */
   DBG_DID_TIMESTAMP_ACTIVE = DBG_API_ACTIVATE_TIMESTAMP, /**< DBG_API_ACTIVATE_TIMESTAMP */
   DBG_DID_BUFFERING_ACTIVE = DBG_API_ACTIVATE_DID_BUFFERING, /**< DBG_API_ACTIVATE_DID_BUFFERING */
   DBG_GLOBAL_ACTIVE = DBG_API_ENABLE_DID_COLLECTION, /**< DBG_API_ENABLE_DID_COLLECTION */
   DBG_GLOBAL_TIMESTAMP_ACTIVE = DBG_API_USE_LOCAL_TIMESTAMP_ACTIVATION /**< DBG_API_USE_LOCAL_TIMESTAMP_ACTIVATION */
}Dbg_activationType;

/*==================[external function declarations]========================*/

/*==================[internal function declarations]========================*/

/*==================[external constants]====================================*/

/*==================[internal constants]====================================*/

/*==================[external data]=========================================*/

/*==================[internal data]=========================================*/

/*==================[external function definitions]=========================*/
#define DBG_START_SEC_CODE
/* Deviation MISRA-5 */
#include <MemMap.h>

/*---------------------------------------------------------------------------*/
/*-----------------[ECU API]-------------------------------------------------*/
/*---------------------------------------------------------------------------*/
/** \brief ECU API: basic initialization of the Dbg module
 **
 ** The Dbg module is designed to be usable right after the compiler startup.
 ** At this point no data can be transmitted to the host and no time stamps
 ** are available but DID data can be saved in the internal ring buffer.
 ** When Dbg_Init() will be called and the "continous send" is configured
 ** this function tried to clean the internal ring buffer right know.
 **
 ** \pre communication stack initialized
 **
 ** \ServiceID{DBG_API_INIT}
 ** \Reentrancy{Non-Reentrant}
 ** \Synchronicity{Synchronous}
 */
extern FUNC(void,DBG_CODE) Dbg_Init(void);

/** \brief ECU API: de-initialization of the Dbg module
 **
 ** This function restore the state before Dbg_Init(). It does not change
 ** individual DID state or global state.
 **
 ** \post Gpt timer (if configured) stoped
 **
 ** \ServiceID{DBG_API_DE_INIT}
 ** \Reentrancy{Non-Reentrant}
 ** \Synchronicity{Synchronous}
 */
extern FUNC(void,DBG_CODE) Dbg_DeInit(void);

/** \brief ECU API: collect DID data
 **
 ** This file contains the implementation of the AUTOSAR module Dbg
 ** function Dbg_CollectDid(). The data of the requested \p Did will be
 ** saved. The transmit depends on the current Dbg module state.
 ** If internal buffer is busy and the data can not be saved the user will
 ** be informed by transmission overflow flag.
 **
 ** \pre Did configured
 ** \pre for standard (static and dynmic) DID's not for predefined
 **
 ** \param[in] Did id of the DID (zero based consecutive)
 **
 ** \ServiceID{DBG_API_COLLECT_DID}
 ** \Reentrancy{Reentrant}
 ** \Synchronicity{Synchronous}
 */
extern FUNC(void,DBG_CODE) Dbg_CollectDid(uint8 Did);

/** \brief Service for performing the processing of the Dbg jobs.
 **
 ** \ServiceID{DBG_API_MAIN_FUNCTION}
 ** \Reentrancy{Reentrant}
 ** \Synchronicity{Synchronous} */
extern FUNC(void,DBG_CODE) Dbg_MainFunction(void);

/** \brief ECU API: UNSUPPORTED yet
 */
/* Deviation MISRA-3 */
#define Dbg_GetVersionInfo()
/** \brief ECU API: UNSUPPORTED yet
 */
/* Deviation MISRA-3 */
#define Dbg_TraceFunctionEntry()
/** \brief ECU API: UNSUPPORTED yet
 */
/* Deviation MISRA-3 */
#define Dbg_TraceFunctionExit()

/** \brief ECU API: UNSUPPORTED yet
 **
 ** Reason: actually the implementation supports lower layer communication
 ** via PduR. It is assumed that the communication stack will be
 ** initialized by the user and is up before Dbg_Init() is called.
 */
/* Deviation MISRA-3 */
#define Dbg_ComInit()
/** \brief ECU API: UNSUPPORTED yet
 **
 ** Reason: actually the implementation supports lower layer communication
 ** via PduR. It is assumed that the communication stack will be
 ** initialized by the user and is up before Dbg_Init() is called.
 */
/* Deviation MISRA-3 */
#define Dbg_ComDeInit()
/** \brief ECU API: UNSUPPORTED yet
 */
/* Deviation MISRA-3 */
#define Dbg_PeriodicSamplingFunction()

/*---------------------------------------------------------------------------*/
/*-----------------[HOST API]------------------------------------------------*/
/*---------------------------------------------------------------------------*/
/** \brief HOST API: clear internal ring buffer
 **
 ** The clear buffer can be done by host command request. If there is currently
 ** no read out of the ring buffer and no write to the ring buffer active, the
 ** request will be done right now. If the in or out is currently locked
 ** by ECU API it will be synchronized with the next unlock.
 **
 ** \ServiceID{DBG_API_CLEAR_BUFFER}
 ** \Reentrancy{Reentrant}
 ** \Synchronicity{Synchronous}
 */
extern FUNC(void,DBG_CODE) Dbg_ClearBuffer(void);

/** \brief HOST API: send next N entries of the buffer
 **
 ** If less then N entries are currently in the buffer, the debugging core module
 ** send the available entries and the missing number of entries the moment
 ** they arrive.
 **
 ** Note: direct and buffered DID's are considered
 **
 ** \param NrOfDids number of DID's to be send (0..127)
 **
 ** \ServiceID{DBG_API_SEND_NEXT_ENTRIES}
 ** \Reentrancy{Reentrant}
 ** \Synchronicity{Synchronous}
 */
extern FUNC(void,DBG_CODE) Dbg_SendNextEntries(uint8 NrOfDids);

/** \brief HOST API: start continous send of data
 **
 ** The initial behavior can be configured. At runtime it can be changed
 ** by a host request.
 ** If the buffer is filled the complete data will be send starting with
 ** the oldest. New data will be send emidiatly when they arrive if the internal
 ** buffer is empty.
 **
 ** \ServiceID{DBG_API_START_CONTINOUS_SEND}
 ** \Reentrancy{Reentrant}
 ** \Synchronicity{Synchronous}
 */
extern FUNC(void,DBG_CODE) Dbg_StartContinousSend(void);

/** \brief HOST API: stop send of data
 **
 ** Transmission state can be changed at runtime by host command.
 ** The collection of data into the ring buffer is not changed by this command.
 **
 ** \ServiceID{DBG_API_STOP_SEND}
 ** \Reentrancy{Reentrant}
 ** \Synchronicity{Synchronous}
 */
extern FUNC(void,DBG_CODE) Dbg_StopSend(void);

/** \brief HOST API: enable/disable specific DID
 **
 ** Specific DID can be enabled/disabled at runtime. The basic active state
 ** can be done in the configuration for each configured DID.
 **
 ** Note: dispatched to common Dbg_Activate() function.
 **
 ** \param[in] DID the id of the DID
 ** \param[in] status if true the DID is active and data will be collected
 **
 ** \ServiceID{DBG_API_ACTIVATE_DID}
 ** \Reentrancy{Reentrant}
 ** \Synchronicity{Synchronous}
 */
/* Deviation MISRA-3 */
#define Dbg_ActivateDid(DID,status) \
    Dbg_Activate((DID),(status),DBG_DID_ACTIVE)

/** \brief HOST API: enable/disable specific DID timestamp
 **
 ** Specific DID timestamp can be enabled/disabled at runtime. The basic
 ** timestamp state can be done in the configuration for each configured DID.
 **
 ** \pre timestamp size is fix and can not be configured at runtime
 **
 ** Note: dispatched to common Dbg_Activate() function.
 **
 ** \param[in] DID the id of the DID
 ** \param[in] status if true the DID timestamp is active
 **
 ** \ServiceID{DBG_API_ACTIVATE_TIMESTAMP}
 ** \Reentrancy{Reentrant}
 ** \Synchronicity{Synchronous}
 */
/* Deviation MISRA-3 */
#define Dbg_ActivateTimestamp(DID,status) \
    Dbg_Activate((DID),(status),DBG_DID_TIMESTAMP_ACTIVE)

/** \brief HOST API: change individual DID storage space
 **
 ** The DID data can be saved in direct or ring buffer. The direct
 ** buffer will be transmitted direct if the data arrive, but additional write
 ** access to the direct buffer while transmitting results in loss of data.
 **
 ** \pre ring buffer configured
 **
 ** Note: dispatched to common Dbg_Activate() function.
 **
 ** \param[in] DID the id of the DID
 ** \param[in] status if true the DID will saved in ring buffer
 **
 ** \ServiceID{DBG_API_ACTIVATE_DID_BUFFERING}
 ** \Reentrancy{Reentrant}
 ** \Synchronicity{Synchronous}
 */
/* Deviation MISRA-3 */
#define Dbg_ActivateDidBuffering(DID,status) \
    Dbg_Activate((DID),(status),DBG_DID_BUFFERING_ACTIVE)

/** \brief HOST API: global DID collection enable/disable
 **
 ** This function enable/disable the global DID collection. It does not
 ** change the communication settings done by Dbg_StopSend(),
 ** Dbg_SendNextEntries() or Dbg_StartContinousSend().
 **
 ** Note: dispatched to common Dbg_Activate() function.
 **
 ** \param[in] status if true the DID's are saved in available buffer
 **
 ** \ServiceID{DBG_API_ENABLE_DID_COLLECTION}
 ** \Reentrancy{Reentrant}
 ** \Synchronicity{Synchronous}
 */
/* Deviation MISRA-3 */
#define Dbg_EnableDidCollection(status) \
    Dbg_Activate(255U,(status),DBG_GLOBAL_ACTIVE)

/** \brief HOST API: global timestamp enable/disable
 **
 ** This function enable/disable the global timestamp at runtime.
 ** Every time a DID is collected the current timestamp will be saved with
 ** the DID data (if timestamp enabled for specific DID). Using this function
 ** the global behavior can be changed. Individual DID state is not changed.
 ** The basic initialisation behavior can be configured.
 **
 ** \pre timestamp usage generally configured
 **
 ** Note: dispatched to common Dbg_Activate() function.
 **
 ** \param[in] status if true the timestamps are saved in DID frames
 **
 ** \ServiceID{DBG_API_USE_LOCAL_TIMESTAMP_ACTIVATION}
 ** \Reentrancy{Reentrant}
 ** \Synchronicity{Synchronous}
 */
/* Deviation MISRA-3 */
#define Dbg_UseLocalTimestampActivation(status) \
    Dbg_Activate(255U,(status),DBG_GLOBAL_TIMESTAMP_ACTIVE);

/** \brief HOST API: UNSUPPORTED yet
 */
/* Deviation MISRA-3 */
#define Dbg_TraceTimestamp()
/** \brief HOST API: UNSUPPORTED yet
 */
/* Deviation MISRA-3 */
#define Dbg_SetCycleTime()

/*---------------------------------------------------------------------------*/
/*-----------------[TRACE HOOK API]------------------------------------------*/
/*---------------------------------------------------------------------------*/

/** \brief TRACE HOOK API: trace OS pre task hook
 **
 ** This function will be used to implement the OS PreTaskHook. If the
 ** appropriate predefined DID is configured all PreTaskHooks are catched
 ** by the Dbg module. The current task id will be traced.
 **
 ** \pre predefined DID pre task hook tracing configured
 ** \pre pre task hook enabled in Os configuration
 **
 ** Note: dispatched to common Dbg_Trace_U8() function.
 **
 ** \param[in] aU8 task id
 **
 ** \ServiceID{DBG_API_PRE_TASK_HOOK}
 ** \Reentrancy{Reentrant}
 ** \Synchronicity{Synchronous}
 */
/* Deviation MISRA-3 */
#define Dbg_PreTaskHook(aU8) \
    Dbg_Trace_U8((uint8)(aU8),DBG_DID_PRE_TASK_HOOK)

/** \brief TRACE HOOK API: trace OS post task hook
 **
 ** This function will be used to implement the OS PostTaskHook. If the
 ** appropriate predefined DID is configured all PostTaskHooks are catched
 ** by the Dbg module. The current task id will be traced.
 **
 ** \pre predefined DID post task hook tracing configured
 ** \pre post task hook enabled in Os configuration
 **
 ** Note: dispatched to common Dbg_Trace_U8() function.
 **
 ** \param[in] aU8 task id
 **
 ** \ServiceID{DBG_API_POST_TASK_HOOK}
 ** \Reentrancy{Reentrant}
 ** \Synchronicity{Synchronous}
 */
/* Deviation MISRA-3 */
#define Dbg_PostTaskHook(aU8) \
    Dbg_Trace_U8((uint8)(aU8),DBG_DID_POST_TASK_HOOK)

/** \brief TRACE HOOK API: trace Develeopment Error Reports
 **
 ** Use this function and this header file to configure Development
 ** Error Tracking callback. The Dbg implemented callback saves
 ** the given data module, instance, api id and error code.
 **
 ** \pre predefined DID develeopment error trace configured
 **
 ** Note: dispatched to common Dbg_Trace_U16_U8_U8_U8() function.
 **
 ** \param[in] aU16 module id
 ** \param[in] bU8 instance id
 ** \param[in] cU8 API id
 ** \param[in] dU8 error code
 **
 ** \ServiceID{DBG_API_TRACE_DET_CALL}
 ** \Reentrancy{Reentrant}
 ** \Synchronicity{Synchronous}
 */
/* Deviation MISRA-3 */
#define Dbg_TraceDetCall(aU16,bU8,cU8,dU8) \
    Dbg_Trace_U16_U8_U8_U8((aU16),(bU8),(cU8),(dU8),DBG_DID_DET_CALL)

/** \brief TRACE HOOK API: RTE Vfb signal send
 **
 ** Each RTE Vfb signal send has his own named hook function, that must
 ** be implemented by the Dbg module.
 ** A hook specific fix tuple of 4 values will be saved for later identifaction
 ** of the appropriate Vfb signal. Implementation of the RTE hooks is done by
 ** the Dbg module using this function.
 **
 ** \pre predefined DID Vfb signal send configured
 ** \pre RTE Vfb signal hook enabled
 **
 ** Note: dispatched to common Dbg_Trace_U16_U8_U8_U8() function.
 **
 ** \param[in] aU16 component id
 ** \param[in] bU8 instance id
 ** \param[in] cU8 port id
 ** \param[in] dU8 data element id
 **
 ** \ServiceID{DBG_API_TRACE_RTE_VFB_SIGNAL_SEND}
 ** \Reentrancy{Reentrant}
 ** \Synchronicity{Synchronous}
 */
/* Deviation MISRA-3 */
#define Dbg_TraceRTEVfbSignalSend(aU16,bU8,cU8,dU8) \
    Dbg_Trace_U16_U8_U8_U8((aU16),(bU8),(cU8),(dU8),DBG_DID_RTE_VFB_SEND)

/** \brief TRACE HOOK API (feature): RTE Vfb signal send with data value
 **
 ** Each RTE Vfb signal send has his own named hook function, that must
 ** be implemented by the Dbg module.
 ** A hook specific fix tuple of 4 values will be saved for later identifaction
 ** of the appropriate Vfb signal. Implementation of the RTE hooks is done by
 ** the Dbg module using this function. The send data value will be collected
 ** additionally.
 **
 ** \pre predefined DID Vfb signal send configured
 ** \pre RTE Vfb signal hook enabled
 ** \pre Dbg global value trace enabled
 **
 ** Note: dispatched to common Dbg_Trace_U16_U8_U8_U8_U32() function.
 **
 ** \param[in] aU16 component id
 ** \param[in] bU8 instance id
 ** \param[in] cU8 port id
 ** \param[in] dU8 data element id
 ** \param[in] eU32 send data value
 **
 ** \ServiceID{DBG_API_TRACE_RTE_VFB_SIGNAL_SEND}
 ** \Reentrancy{Reentrant}
 ** \Synchronicity{Synchronous}
 */
/* Deviation MISRA-3 */
#if (DBG_VALUE_TRACING == STD_ON)
#define Dbg_TraceRTEVfbSignalSendVal(aU16,bU8,cU8,dU8,eU32) \
    Dbg_Trace_U16_U8_U8_U8_U32((aU16),(bU8),(cU8),(dU8),(eU32),DBG_DID_RTE_VFB_SEND)
#endif

/** \brief TRACE HOOK API: RTE Vfb signal receive
 **
 ** Each RTE Vfb signal receive has his own named hook function, that must
 ** be implemented by the Dbg module.
 ** A hook specific fix tuple of 4 values will be saved for later identifaction
 ** of the appropriate Vfb signal. Implementation of the RTE hooks is done by
 ** the Dbg module using this function.
 **
 ** \pre predefined DID Vfb signal received configured
 ** \pre RTE Vfb signal hook enabled
 **
 ** Note: dispatched to common Dbg_Trace_U16_U8_U8_U8() function.
 **
 ** \param[in] aU16 component id
 ** \param[in] bU8 instance id
 ** \param[in] cU8 port id
 ** \param[in] dU8 data element id
 **
 ** \ServiceID{DBG_API_TRACE_RTE_VFB_SIGNAL_RECEIVE}
 ** \Reentrancy{Reentrant}
 ** \Synchronicity{Synchronous}
 */
/* Deviation MISRA-3 */
#define Dbg_TraceRTEVfbSignalReceive(aU16,bU8,cU8,dU8) \
    Dbg_Trace_U16_U8_U8_U8((aU16),(bU8),(cU8),(dU8),DBG_DID_RTE_VFB_RECEIVE)

/** \brief TRACE HOOK API: RTE Vfb signal receive with data value
 **
 ** Each RTE Vfb signal receive has his own named hook function, that must
 ** be implemented by the Dbg module.
 ** A hook specific fix tuple of 4 values will be saved for later identifaction
 ** of the appropriate Vfb signal. Implementation of the RTE hooks is done by
 ** the Dbg module using this function. The received data value will be collected
 ** additionally.
 **
 ** \pre predefined DID Vfb signal received configured
 ** \pre RTE Vfb signal hook enabled
 ** \pre Dbg global value trace enabled
 **
 ** Note: dispatched to common Dbg_Trace_U16_U8_U8_U8_U32() function.
 **
 ** \param[in] aU16 component id
 ** \param[in] bU8 instance id
 ** \param[in] cU8 port id
 ** \param[in] dU8 data element id
 ** \param[in] eU32 received data value
 **
 ** \ServiceID{DBG_API_TRACE_RTE_VFB_SIGNAL_RECEIVE}
 ** \Reentrancy{Reentrant}
 ** \Synchronicity{Synchronous}
 */
/* Deviation MISRA-3 */
#if (DBG_VALUE_TRACING == STD_ON)
#define Dbg_TraceRTEVfbSignalReceiveVal(aU16,bU8,cU8,dU8,eU32) \
    Dbg_Trace_U16_U8_U8_U8_U32((aU16),(bU8),(cU8),(dU8),(eU32),DBG_DID_RTE_VFB_RECEIVE)
#endif

/** \brief TRACE HOOK API: RTE call
 **
 ** Every time the RTE or runnable implementation performs a CALL action
 ** it can be traced using RTE hook function.
 ** A hook specific fix tuple of 4 values will be saved for later identifaction
 ** of the appropriate RTE call. Implementation of the RTE hooks is done by
 ** the Dbg module using this function.
 **
 ** \pre predefined DID Rte call configured
 ** \pre RTE call hook enabled
 **
 ** Note: dispatched to common Dbg_Trace_U16_U8_U8_U8() function.
 **
 ** \param[in] aU16 component id
 ** \param[in] bU8 instance id
 ** \param[in] cU8 port id
 ** \param[in] dU8 service id
 **
 ** \ServiceID{DBG_API_TRACE_RTE_CALL}
 ** \Reentrancy{Reentrant}
 ** \Synchronicity{Synchronous}
 */
/* Deviation MISRA-3 */
#define Dbg_TraceRTECall(aU16,bU8,cU8,dU8) \
    Dbg_Trace_U16_U8_U8_U8((aU16),(bU8),(cU8),(dU8),DBG_DID_RTE_CALL)

/** \brief TRACE HOOK API: RTE runnable start
 **
 ** When the RTE runnable starts execution an hook will be called.
 ** This hook will be implemented by the Dbg module and traced.
 ** For later identification of the runnable a tuple of 3 values will be saved.
 ** Each runnable start has his own tuple.
 **
 ** \pre predefined DID runnable start configured
 ** \pre RTE runnable hook enabled
 **
 ** Note: dispatched to common Dbg_Trace_U16_U8_U8() function.
 **
 ** \param[in] aU16 component id
 ** \param[in] bU8 instance id
 ** \param[in] cU8 runnable id
 **
 ** \ServiceID{DBG_API_TRACE_RUNNABLE_START}
 ** \Reentrancy{Reentrant}
 ** \Synchronicity{Synchronous}
 */
/* Deviation MISRA-3 */
#define Dbg_TraceRunnableStart(aU16,bU8,cU8) \
    Dbg_Trace_U16_U8_U8((aU16),(bU8),(cU8),DBG_DID_RUNNABLE_START)

/** \brief TRACE HOOK API: RTE runnable terminate
 **
 ** When the RTE runnable finish execution an hook will be called.
 ** This hook will be implemented by the Dbg module and traced.
 ** For later identification of the runnable a tuple of 3 values will be saved.
 ** Each runnable termination has his own tuple.
 **
 ** \pre predefined DID runnable terminate configured
 ** \pre RTE runnable hook enabled
 **
 ** Note: dispatched to common Dbg_Trace_U16_U8_U8() function.
 **
 ** \param[in] aU16 component id
 ** \param[in] bU8 instance id
 ** \param[in] cU8 runnable id
 **
 ** \ServiceID{DBG_API_TRACE_RUNNABLE_TERMINATE}
 ** \Reentrancy{Reentrant}
 ** \Synchronicity{Synchronous}
 */
/* Deviation MISRA-3 */
#define Dbg_TraceRunnableTerminate(aU16,bU8,cU8) \
    Dbg_Trace_U16_U8_U8((aU16),(bU8),(cU8),DBG_DID_RUNNABLE_TERMINATE)

/** \brief TRACE HOOK API: UNSUPPORTED yet
 */
/* Deviation MISRA-3 */
#define Dbg_TraceRTEComSignalTx()
/** \brief TRACE HOOK API: UNSUPPORTED yet
 */
/* Deviation MISRA-3 */
#define Dbg_TraceRTEComSignalRx()
/** \brief TRACE HOOK API: UNSUPPORTED yet
 */
/* Deviation MISRA-3 */
#define Dbg_TraceRTEComSignalIv()

#define DBG_STOP_SEC_CODE
/* Deviation MISRA-5 */
#include <MemMap.h>

/*==================[internal function declarations]========================*/
#define DBG_START_SEC_CODE
/* Deviation MISRA-5 */
#include <MemMap.h>

/*-----------------[PRIVATE]------------------------------------------------*/
/** \brief PRIVATE API: state change function
 **
 ** Do not call this function directly, use the provided public API functions
 ** (macros) instead.
 ** All state change functionality of other API functions are dispatched
 ** to this function.
 ** For example: timestamp on/off, DID buffering on/off.
 ** The parameter are transfered to this function as is.
 ** The additional \p activation is used for service request detection.
 **
 ** \pre predefined DID runnable terminate configured
 ** \pre RTE runnable hook enabled
 **
 ** \param[in] Did the DID id (id is not used if activation is global)
 ** \param[in] status switch the state on or off
 ** \param[in] activation type of service requested
 **
 ** \ServiceID{direct associated with Dbg_activationType}
 ** \Reentrancy{Reentrant}
 ** \Synchronicity{Synchronous}
 */
extern FUNC(void, COM_APPL_CODE) Dbg_Activate(uint8 Did, boolean status,Dbg_activationType activation);

/** \brief PRIVATE API: tracing of specific paremeter sequence
 **
 ** Do not call this function directly, use the provided public API functions
 ** (macros) instead.
 ** The parameter are saved in direct call order within the internal buffer.
 ** No endianess or alignment change is done.
 **
 ** \param[in] aU16 first parameter value of type U16
 ** \param[in] bU8 second parameter value of type U8
 ** \param[in] cU8 third parameter value of type U8
 ** \param[in] dU8 fourth parameter value of type U8
 ** \param[in] DID DID id (in generally predefined DID)
 **
 ** \ServiceID{direct associated with caller}
 ** \Reentrancy{Reentrant}
 ** \Synchronicity{Synchronous}
 */
extern FUNC(void,DBG_CODE) Dbg_Trace_U16_U8_U8_U8(uint16 aU16,uint8 bU8,uint8 cU8,uint8 dU8,uint8 DID);

/** \brief PRIVATE API: tracing of specific paremeter sequence
 **
 ** Do not call this function directly, use the provided public API functions
 ** (macros) instead.
 ** The parameter are saved in direct call order within the internal buffer.
 ** No endianess or alignment change is done.
 **
 ** \param[in] aU16 first parameter value of type U16
 ** \param[in] bU8 second parameter value of type U8
 ** \param[in] cU8 third parameter value of type U8
 ** \param[in] DID DID id (in generally predefined DID)
 **
 ** \ServiceID{direct associated with caller}
 ** \Reentrancy{Reentrant}
 ** \Synchronicity{Synchronous}
 */
extern FUNC(void,DBG_CODE) Dbg_Trace_U16_U8_U8(uint16 aU16,uint8 bU8,uint8 cU8,uint8 DID);

/** \brief PRIVATE API: tracing of specific paremeter sequence
 **
 ** Do not call this function directly, use the provided public API functions
 ** (macros) instead.
 ** The parameter are saved in direct call order within the internal buffer.
 ** No endianess or alignment change is done.
 **
 ** \param[in] aU8 first parameter value of type U8
 ** \param[in] DID DID id (in generally predefined DID)
 **
 ** \ServiceID{direct associated with caller}
 ** \Reentrancy{Reentrant}
 ** \Synchronicity{Synchronous}
 */
extern FUNC(void,DBG_CODE) Dbg_Trace_U8(uint8 aU8,uint8 DID);

/** \brief PRIVATE API: tracing of specific paremeter sequence
 **
 ** Do not call this function directly, use the provided public API functions
 ** (macros) instead.
 ** The parameter are saved in direct call order within the internal buffer.
 ** No endianess or alignment change is done.
 **
 ** \param[in] aU16 first parameter value of type U16
 ** \param[in] bU8 second parameter value of type U8
 ** \param[in] cU8 third parameter value of type U8
 ** \param[in] dU8 fourth parameter value of type U8
 ** \param[in] eU32 fourth parameter value of type U32
 ** \param[in] DID DID id (in generally predefined DID)
 **
 ** \ServiceID{direct associated with caller}
 ** \Reentrancy{Reentrant}
 ** \Synchronicity{Synchronous}
 */
extern FUNC(void,DBG_CODE) Dbg_Trace_U16_U8_U8_U8_U32(uint16 aU16,uint8 bU8,uint8 cU8,uint8 dU8,uint32 eU32,uint8 DID);

#define DBG_STOP_SEC_CODE
/* Deviation MISRA-5 */
#include <MemMap.h>


/*==================[internal function definitions]=========================*/

#endif /* if !defined( DBG_H ) */
/*==================[end of file]===========================================*/
