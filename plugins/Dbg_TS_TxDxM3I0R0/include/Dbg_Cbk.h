/**
 * \file
 *
 * \brief AUTOSAR Dbg
 *
 * This file contains the implementation of the AUTOSAR
 * module Dbg.
 *
 * \author Elektrobit Automotive GmbH, 91058 Erlangen, Germany
 *
 * Copyright 2005 - 2013 Elektrobit Automotive GmbH
 * All rights exclusively reserved for Elektrobit Automotive GmbH,
 * unless expressly agreed to otherwise.
 */
#if (!defined DBG_CBK_H)
#define DBG_CBK_H

/**
 ** Dispatching to the current configuration dependent callback
 ** declaration.
 ** Communication via PduR is implemented by Dbg module by default.
 ** All other communication interface must be implemented by the user.
 **
 ** !LINKSTO Dbg.FileStructure.Dbg009,1
 */
/*
 *  Misra-C:2004 Deviations:
 *
 *  MISRA-1) Deviated Rule: 19.1 (advisory)
 *    '#include' statements in a file should only be preceded by other
 *    preprocessor directives or comments.
 *
 *    Reason:
 *    AUTOSAR compiler and memory abstraction.
 *
*/

/*==================[inclusions]============================================*/

#include <TSAutosar.h>     /* EB specific standard types */
#include <Std_Types.h>     /* AUTOSAR standard types */
#include <Dbg.h>           /* public configuration part */

#if (DBG_COM_VIA_TP_PDUR==STD_ON)
/** \brief Tp implementation of the Dbg module that use PduR interface */
#include <ComStack_Types.h>
#endif
/*==================[version check]=========================================*/

/*==================[version check]==========================================*/

/*==================[macros]================================================*/

/*==================[macro checks]==========================================*/

/*==================[type definitions]======================================*/

/*==================[external function declarations]========================*/

/*==================[internal function declarations]========================*/

/*==================[external constants]====================================*/

/*==================[internal constants]====================================*/

/*==================[external data]=========================================*/

/*==================[internal data]=========================================*/

/*==================[external function definitions]=========================*/
#define DBG_START_SEC_CODE
/* Deviation MISRA-1 */
#include <MemMap.h>

#if (DBG_COM_VIA_TP_PDUR==STD_ON)
/** \brief ECU API: indication PduR callback
 **
 ** This function is the Dbg implemented callback to the PduR used for
 ** communication (default).
 ** This function implements the call of Dbg_Indication() and the complete
 ** acknowledge handling based on PduR services. The processing
 ** is synchronized with a possible still running transmission of DID
 ** frames.
 **
 ** \param[in] DbgRxPduId configured Rx Pdu id of the Dbg module
 ** \param[in] PduInfoPtr contains the length (SduLength) of the received
 ** PDU and a pointer to a buffer (SduDataPtr) containing the data.
 **
 ** \ServiceID{DBG_API_RX_INDICATION}
 ** \Reentrancy{Reentrant}
 ** \Synchronicity{Synchronous}
 */
extern FUNC(void, DBG_CODE) Dbg_RxIndication
(
    PduIdType DbgRxPduId,
    P2VAR(PduInfoType, AUTOMATIC, DBG_APPL_DATA) PduInfoPtr
);

/** \brief ECU API: confirmation PduR callback
 **
 ** This function is the Dbg implemented callback to the PduR used for
 ** communication (default). The function will be called by the PduR every
 ** time a PduR_DbgTransmit() was completed.
 ** This function implements the segmentation and ACK processing.
 ** It calls PduR_DbgTransmit() if additional segments of the DID frame must
 ** be transmitted.
 ** Dbg_Confirmation() is called if the DID frame was transfered complete.
 ** Dbg_Confirmation() is not called when an ACK was send.
 **
 ** \param[in] DbgTxPduId PduR configured Tx id of the Dbg Pdu
 **
 ** \ServiceID{DBG_API_TX_CONFIRMATION}
 ** \Reentrancy{Reentrant}
 ** \Synchronicity{Synchronous}
 */
extern FUNC(void, DBG_CODE) Dbg_TxConfirmation
(
    PduIdType DbgTxPduId
);

/** \brief ECU API: additional PduR callback
 **
 ** The PduR_DbgTransmit() is called with all needed information (including data
 ** buffer) to send the frame.
 ** Some communication interfaces (like time triggerd FrIf) discard this data
 ** and using an additional callback when the data is needed (interface optimization
 ** without own buffer).
 ** So PduR_DbgTransmit() is used for trigger only in this case.
 ** The ComTpPduR implementation ensure the data consistency between
 ** PduR_DbgTransmit() and the Dbg_TxConfirmation().
 ** This function copy the ComTpPduR temporarily data to the interface
 ** specific position at the time it is needed.
 **
 ** \param[in] DbgTxPduId PduR configured Tx id of the Dbg Pdu
 ** \param[in] PduInfoPtr contains the length (SduLength) of the PDU and
 ** a pointer to a buffer (SduDataPtr) containing the data.
 **
 ** \ServiceID{none}
 ** \Reentrancy{Reentrant}
 ** \Synchronicity{Synchronous}
 */
extern FUNC(void, DBG_CODE) Dbg_TriggerTransmit
(
    PduIdType DbgTxPduId,
    P2VAR(PduInfoType, AUTOMATIC, DBG_APPL_DATA) PduInfoPtr
);

#endif

/** \brief ECU API: receive of host request
 **
 ** Use this Dbg Com part callback when implementing your own Dbg
 ** specific transport layer different to the Dbg implemented TpPduR.
 ** This function signalize a received host request.
 **
 ** Deviation: In difference to the AUTOSAR SWS the Dbg Com implementation
 **            of the function returns a status. So the user can
 **            deal with or without ACK handling debendend on status.
 **
 ** \param[in] Buffer pointer to the receive buffer
 ** \return status of the processing
 ** \retval E_OK if the request was handled
 ** \retval E_NOT_OK request not accepted (for example not possible, not implemented, ..)
 **
 ** \ServiceID{DBG_API_INDICATION}
 ** \Reentrancy{Reentrant}
 ** \Synchronicity{Synchronous}
 */
extern FUNC(Std_ReturnType, DBG_CODE) Dbg_Indication
(
    P2CONST(uint8, AUTOMATIC, AUTOMATIC) Buffer
);

/** \brief ECU API: transmit confirmation
 **
 ** Use this Dbg Com part callback when implementing your own Dbg
 ** specific transport layer different to the Dbg implemented TpPduR.
 ** This function indicates the complete lower layer processing of the last
 ** DID frame transmitted via Dbg_Transmit().
 ** If the lower layer has detected a tranmission error the function can be
 ** called using E_NOT_OK. In this case the Dbg Com part performs the
 ** error handling. The DID frame remains pending in the Dbg Com part and
 ** wil be requested via Dbg_Transmit() next time it is possible.
 **
 ** Deviation: In difference to the AUTOSAR SWS the Dbg Com implementation
 **            of the function supports a parameter.
 **
 ** \param[in] success if there are lower layer transmission errors detected use
 **                    E_NOT_OK. If all is successfully transmitted E_OK.
 **
 ** \ServiceID{DBG_API_CONFIRMATION}
 ** \Reentrancy{Reentrant}
 ** \Synchronicity{Synchronous}
 */
extern FUNC(void, DBG_CODE) Dbg_Confirmation
(
    Std_ReturnType success
);

#define DBG_STOP_SEC_CODE
/* Deviation MISRA-1 */
#include <MemMap.h>
/*==================[internal function definitions]=========================*/

#endif /* if !defined( DBG_CBK_H ) */
/*==================[end of file]===========================================*/
