/**
 * \file
 *
 * \brief AUTOSAR CanTp
 *
 * This file contains the implementation of the AUTOSAR
 * module CanTp.
 *
 * \author Elektrobit Automotive GmbH, 91058 Erlangen, Germany
 *
 * Copyright 2005 - 2013 Elektrobit Automotive GmbH
 * All rights exclusively reserved for Elektrobit Automotive GmbH,
 * unless expressly agreed to otherwise.
 */
[!CODE!]
/*==================[inclusions]=============================================*/
[!INCLUDE "../include/CanTp_Precompile.m"!][!//
[!INCLUDE "../include/CanTp_PostBuild.m"!][!//
#include <TSAutosar.h>         /* EB specific standard types */
#include <ComStack_Types.h>    /* typedefs for AUTOSAR com stack */
#include <CanTp_Types.h>        /* Module public types */
#include <CanTp_Api.h>         /* Module public API */
#include <CanTp_Internal.h>    /* internal macros and variables */
#include <CanTp_InternalCfg.h> /* CanTp internal configuration */


#if(CANTP_JUMPTABLE_SUPPORT == STD_ON)
#include <CanTp_EntryTable.h>  /* CanTp entry jumptable */
#endif /* CANTP_JUMPTABLE_SUPPORT == STD_ON */

/*==================[macros]=================================================*/

/*==================[type definitions]=======================================*/

/*==================[external function declarations]=========================*/

/*==================[internal function declarations]=========================*/

/*==================[external constants]=====================================*/

/*==================[internal constants]=====================================*/

/*==================[external data]==========================================*/


#define CANTP_START_SEC_VAR_NO_INIT_8
#include <MemMap.h>

[!IF "CanTpGeneral/CanTpChangeParameterApi = 'true'"!][!//
[!IF "CanTpJumpTable/CanTpJumpTableMode != 'CLIENT'"!][!//

VAR(uint8, CANTP_VAR) CanTp_RxNSduStMinValues[CANTP_MAX_RX_NSDUS];

VAR(uint8, CANTP_VAR) CanTp_RxNSduBSValues[CANTP_MAX_RX_NSDUS];

[!ENDIF!][!//
[!ENDIF!][!//

#if(CANTP_DYNAMIC_NSA_ENABLED == STD_ON)
/** \brief If dynamic setting of N_SA values is enabled, the N_SA values are
 *         stored in following array.
 */
VAR(uint8, CANTP_VAR) CanTp_NSaValues[CANTP_MAX_RX_NSDUS + CANTP_MAX_FC_PDUS];
#endif

#define CANTP_STOP_SEC_VAR_NO_INIT_8
#include <MemMap.h>


[!IF "CanTpJumpTable/CanTpJumpTableMode != 'CLIENT'"!][!//


#define CANTP_START_SEC_VAR_NO_INIT_UNSPECIFIED
#include <MemMap.h>

VAR(CanTp_ChannelType, CANTP_VAR) CanTp_Channel[CANTP_MAX_TX_CHANNELS + CANTP_MAX_RX_CHANNELS];

#define CANTP_STOP_SEC_VAR_NO_INIT_UNSPECIFIED
#include <MemMap.h>

[!ENDIF!][!//

#define CANTP_START_SEC_VAR_INIT_UNSPECIFIED
#include <MemMap.h>
/** \brief Internal CanTp variables to be initialized by the C startup code */
/* !LINKSTO CanTp.JTM_0023, 1, CanTp.EB.JTM_11579, 1 */
VAR(CanTp_InitVariableType, CANTP_VAR) CanTp_InitVars =
  {
    CANTP_OFF,                /* default state */
[!IF "CanTpJumpTable/CanTpJumpTableMode != 'OFF'"!][!//
    &CanTp_ExitTable         /* exit jump table */
[!ENDIF!][!//
  };
#define CANTP_STOP_SEC_VAR_INIT_UNSPECIFIED
#include <MemMap.h>

/*==================[internal data]==========================================*/

/*==================[external function definitions]==========================*/

#define CANTP_START_SEC_CODE
#include <MemMap.h>
[!/*
*/!][!IF "CanTpGeneral/CanTpGptUsageEnable = 'true'"!][!/*
*/!][!FOR "I"="0" TO "$numTxChannels - 1"!][!/*
   */!][!VAR "chanRef"!][!CALL "getChanRefByChanId", "chanId"="$I"!][!ENDVAR!][!/*
   */!][!IF "node:ref($chanRef)/CanTpSTminTimeoutHandling = 'Gpt'"!][!/*
     */!][!VAR "callbackName"="node:ref($chanRef)/CanTpGptChannelCallbackName"!]
FUNC(void, CANTP_CODE) [!"$callbackName"!](void)
{
  /* redirect */
[!IF "CanTpJumpTable/CanTpJumpTableMode = 'CLIENT'"!][!//
  (CanTp_EntryTablePtr->STminCallback)( (uint8) [!"num:i($I)"!]U );
[!ELSE!][!//
  CanTp_STminCallback((uint8) [!"num:i($I)"!]U);
[!ENDIF!][!//
}
[!/*
   */!][!ENDIF!][!/*
*/!][!ENDFOR!][!/*
*/!][!ENDIF!][!/* "CanTpGeneral/CanTpGptUsageEnable = 'true'"*/!]

#define CANTP_STOP_SEC_CODE
#include <MemMap.h>

/*==================[internal function definitions]==========================*/

/*==================[end of file]============================================*/
[!ENDCODE!]

