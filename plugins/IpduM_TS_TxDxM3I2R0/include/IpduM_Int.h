/**
 * \file
 *
 * \brief AUTOSAR IpduM
 *
 * This file contains the implementation of the AUTOSAR
 * module IpduM.
 *
 * \author Elektrobit Automotive GmbH, 91058 Erlangen, Germany
 *
 * Copyright 2005 - 2013 Elektrobit Automotive GmbH
 * All rights exclusively reserved for Elektrobit Automotive GmbH,
 * unless expressly agreed to otherwise.
 */
#if (!defined IPDUM_INT_H)
#define IPDUM_INT_H

/*==================[inclusions]============================================*/
#include <IpduM_Cfg.h>

#if (IPDUM_DEV_ERROR_DETECT == STD_ON)
# include <Det.h>
#endif


/*==================[macros]================================================*/
/** \brief macro to enable overwriting of static declaration for testing  purpose */
#ifndef IPDUM_STATIC
#if (IPDUM_AUTOSAR_VERSION == 21)
#define IPDUM_STATIC _STATIC_
#else
#define IPDUM_STATIC STATIC
#endif /* if (IPDUM_AUTOSAR_VERSION == 21) */
#endif /* ifndef IPDUM_STATIC */

/** \brief calculate modulo 8 in byte array  */
#define IPDUM_MODULO_8(i) ((i)&7U)

/** \brief pattern to select bits between startbit and endbit */
#define IPDUM_SELECT_BITMASK(startbit,stopbit) \
  ((uint8)((uint8)(1U<<((uint8)((stopbit)-(startbit))+1U))-1U))

/** \brief pattern to clear all bits between startbit and endbit */
#define IPDUM_CLEAR_BITMASK(startbit,stopbit) \
  ((uint8)~((uint8)(IPDUM_SELECT_BITMASK(startbit,stopbit)<<IPDUM_MODULO_8(startbit))))

/** \brief macro to extract the selector field */
#define IPDUM_GET_SELECTOR(src,startbit,stopbit) \
  ((uint8)((uint8)((src)[(startbit)>>3U]>>((IPDUM_MODULO_8(startbit))))&IPDUM_SELECT_BITMASK(startbit,stopbit)))

/** \brief macro to set the selector field */
#define IPDUM_SET_SELECTOR(dst,value,startbit,stopbit) \
  ((dst)[(startbit)>>3U]=(uint8)((dst)[(startbit)>>3U]&IPDUM_CLEAR_BITMASK(startbit,stopbit))| \
  ((uint8)((value)<<(IPDUM_MODULO_8(startbit)))))

/** \brief macro to convert bit into byte offset */
#define IPDUM_GET_BYTE_OFFSET(bitpos) \
  ((bitpos)>>3U)

/** \brief macro to convert size in bits to bytes */
#define IPDUM_GET_BYTE_SIZE(bitsize) \
  (((bitsize)+7U)>>3U)

/** \brief macro to copy bytewise */
#define IPDUM_COPY(Dest, Src, Size) \
  do \
  { \
    uint8 ii; \
    for (ii=0U;ii<(Size);ii++) \
    { \
      (Dest)[ii] = (Src)[ii]; \
    } \
  } while(0U)

/** \brief maximum size of TX pathway array */
#define IPDUM_TX_PATHWAY_LEN (IpduM_ConfigPtr->TxPathWayLen)

/** \brief maximum size of RX pathway array */
#define IPDUM_RX_PATHWAY_LEN (IpduM_ConfigPtr->RxPathWayLen)

/** \brief mark dynamic PDU for sending */
#define IPDUM_PRIO_NOT_READY_TO_SEND ((uint16)0x100U)

/** \brief Lowest possible priority in the queue */
#define IPDUM_PRIO_LOWEST_POSSIBLE ((uint16)0xFFFFU)

/** \ brief reserved value for IpduM_TxTimeoutType to indicate that a call to
 * PduR_IpduMTransmit() has not yet completed */
#define IPDUM_TRANSMIT_ONGOING 0xFFFFU

/** \ brief reserved value for IpduM_TxTimeoutType to indicate that a call to
 * PduR_IpduMTransmit() has not yet completed but the sending of the PDU is
 * already confirmed */
#define IPDUM_TRANSMIT_ONGOING_CONFIRMED (IPDUM_TRANSMIT_ONGOING - 1U)

/** \ brief Offset in bytes for first TxData element in IpduM_DataMem */
#define IPDUM_TX_DATA_OFFSET  ((uint16)0x0000U)




/*------------------------[Defensive programming]----------------------------*/

#if (defined IPDUM_PRECONDITION_ASSERT)
#error IPDUM_PRECONDITION_ASSERT is already defined
#endif
#if (IPDUM_PRECONDITION_ASSERT_ENABLED == STD_ON)
/** \brief Report an assertion violation to Det
 **
 ** \param[in] Condition Condition which is violated
 ** \param[in] ApiId Service ID of the API function */
#define IPDUM_PRECONDITION_ASSERT(Condition, ApiId) \
  DET_PRECONDITION_ASSERT((Condition), IPDUM_MODULE_ID, IPDUM_INSTANCE_ID, (ApiId))
#else
#define IPDUM_PRECONDITION_ASSERT(Condition, ApiId)
#endif

#if (defined IPDUM_POSTCONDITION_ASSERT)
#error IPDUM_POSTCONDITION_ASSERT is already defined
#endif
#if (IPDUM_POSTCONDITION_ASSERT_ENABLED == STD_ON)
/** \brief Report an assertion violation to Det
 **
 ** \param[in] Condition Condition which is violated
 ** \param[in] ApiId Service ID of the API function */
#define IPDUM_POSTCONDITION_ASSERT(Condition, ApiId) \
  DET_POSTCONDITION_ASSERT((Condition), IPDUM_MODULE_ID, IPDUM_INSTANCE_ID, (ApiId))
#else
#define IPDUM_POSTCONDITION_ASSERT(Condition, ApiId)
#endif

#if (defined IPDUM_INVARIANT_ASSERT)
#error IPDUM_INVARIANT_ASSERT is already defined
#endif
#if (IPDUM_INVARIANT_ASSERT_ENABLED == STD_ON)
/** \brief Report an assertion violation to Det
 **
 ** \param[in] Condition Condition which is violated
 ** \param[in] ApiId Service ID of the API function */
#define IPDUM_INVARIANT_ASSERT(Condition, ApiId) \
  DET_INVARIANT_ASSERT((Condition), IPDUM_MODULE_ID, IPDUM_INSTANCE_ID, (ApiId))
#else
#define IPDUM_INVARIANT_ASSERT(Condition, ApiId)
#endif

#if (defined IPDUM_STATIC_ASSERT)
# error IPDUM_STATIC_ASSERT is already defined
#endif
#if (IPDUM_STATIC_ASSERT_ENABLED == STD_ON)
/** \brief Report an static assertion violation to Det
 **
 ** \param[in] Condition Condition which is violated */
# define IPDUM_STATIC_ASSERT(expr) DET_STATIC_ASSERT(expr)
#else
# define IPDUM_STATIC_ASSERT(expr)
#endif

#if (defined IPDUM_UNREACHABLE_CODE_ASSERT)
#error IPDUM_UNREACHABLE_CODE_ASSERT is already defined
#endif
#if (IPDUM_UNREACHABLE_CODE_ASSERT_ENABLED == STD_ON)
/** \brief Report an unreachable code assertion violation to Det
 **
 ** \param[in] ApiId Service ID of the API function */
#define IPDUM_UNREACHABLE_CODE_ASSERT(ApiId) \
  DET_UNREACHABLE_CODE_ASSERT(IPDUM_MODULE_ID, IPDUM_INSTANCE_ID, (ApiId))
#else
#define IPDUM_UNREACHABLE_CODE_ASSERT(ApiId)
#endif

#if (defined IPDUM_INTERNAL_API_ID)
#error IPDUM_INTERNAL_API_ID is already defined
#endif
/** \brief API ID of module internal functions to be used in assertions */
#define IPDUM_INTERNAL_API_ID DET_INTERNAL_API_ID


/*==================[type definitions]======================================*/
/** \brief definition of the IpduM_StatusType */
typedef uint8 IpduM_StatusType;

/** \brief Definition of constant IPDUM_UNINIT */
#if (defined IPDUM_UNINIT)        /* To prevent double declaration */
#error IPDUM_UNINIT already defined
#endif /* if (defined IPDUM_UNINIT) */

/** \brief Define IPDUM_UNINIT */
#define IPDUM_UNINIT ((IpduM_StatusType) 0x0)

/** \brief Definition of constant IPDUM_INIT */
#if (defined IPDUM_INIT)          /* To prevent double declaration */
#error IPDUM_INIT already defined
#endif /* if (defined IPDUM_INIT) */

/** \brief Define IPDUM_INIT */
#define IPDUM_INIT ((IpduM_StatusType) 0x01U)

/* define function like macro for development error reporting,
 * if development error detection is enabled */
#if (IPDUM_DEV_ERROR_DETECT==STD_ON)

#define IPDUM_DET_REPORT_ERROR(ApiId,ErrorId)    \
  (void)Det_ReportError(IPDUM_MODULE_ID, IPDUM_INSTANCE_ID, (ApiId), (uint8)(ErrorId))
#endif /* if (IPDUM_DEV_ERROR_DETECT == STD_ON) */

/*==================[external function declarations]========================*/

/*==================[internal function declarations]========================*/

/*==================[external constants]====================================*/

/*==================[internal constants]====================================*/

/*==================[external data]=========================================*/

/*==================[internal data]=========================================*/

/*==================[external function definitions]=========================*/

/*==================[internal function definitions]=========================*/

#endif /* if !defined( IPDUM_INT_H ) */
/*==================[end of file]===========================================*/
