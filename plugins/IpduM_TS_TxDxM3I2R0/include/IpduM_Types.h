/**
 * \file
 *
 * \brief AUTOSAR IpduM
 *
 * This file contains the implementation of the AUTOSAR
 * module IpduM.
 *
 * \author Elektrobit Automotive GmbH, 91058 Erlangen, Germany
 *
 * Copyright 2005 - 2013 Elektrobit Automotive GmbH
 * All rights exclusively reserved for Elektrobit Automotive GmbH,
 * unless expressly agreed to otherwise.
 */
#if (!defined IPDUM_TYPES_H)
#define IPDUM_TYPES_H

/*==================[inclusions]============================================*/

#include <ComStack_Types.h>
#include <IpduM_Cfg.h>

/*==================[macros]================================================*/

/*------------------[AUTOSAR module identification]-------------------------*/

#if (defined IPDUM_INSTANCE_ID)
#error IPDUM_INSTANCE_ID already defined
#endif
/** \brief Id of instance of IpduM */
#define IPDUM_INSTANCE_ID  0U

/*------------------[macros for error codes]--------------------------------*/

#if (defined IPDUM_E_PARAM) /* to prevent double declaration */
#error IPDUM_E_PARAM already defined
#endif
/** \brief Development Error Code
 **
 ** API service called with wrong parameter.
 */
#define IPDUM_E_PARAM  0x10


#if (defined IPDUM_E_PARAM_POINTER ) /* to prevent double declaration */
#error IPDUM_E_PARAM_POINTER  already defined
#endif
/** \brief Development Error Code
 **
 ** API service called with a NULL pointer.
 */
#define IPDUM_E_PARAM_POINTER   0x11


#if (defined IPDUM_E_UNINIT) /* to prevent double declaration */
#error IPDUM_E_UNINIT already defined
#endif
/** \brief Development Error Code
 **
 ** API service used without module initialization.
 */
#define IPDUM_E_UNINIT  0x20

/*------------------[macros for service IDs]--------------------------------*/

#if (defined IPDUM_SID_INIT)
#error IPDUM_SID_INIT already defined
#endif
/** \brief AUTOSAR API service ID.
 **
 ** Definition of service ID for IpduM_Init.
 */
#define IPDUM_SID_INIT  0x00U


#if (defined IPDUM_SID_GET_VERSION_INFO)
#error IPDUM_SID_GET_VERSION_INFO already defined
#endif
/** \brief AUTOSAR API service ID.
 **
 ** Definition of service ID for IpduM_GetVersionInfo.
 */
#define IPDUM_SID_GET_VERSION_INFO  0x01U


#if (defined IPDUM_SID_RX_INDICATION)
#error IPDUM_SID_RX_INDICATION already defined
#endif
/** \brief AUTOSAR API service ID.
 **
 ** Definition of service ID for IpduM_RxIndication.
 */
#define IPDUM_SID_RX_INDICATION  0x42U


#if (defined IPDUM_SID_TRANSMIT)
#error IPDUM_SID_TRANSMIT already defined
#endif
/** \brief AUTOSAR API service ID.
 **
 ** Definition of service ID for IpduM_Transmit.
 */
#define IPDUM_SID_TRANSMIT  0x03U


#if (defined IPDUM_SID_TX_CONFIRMATION)
#error IPDUM_SID_TX_CONFIRMATION already defined
#endif
/** \brief AUTOSAR API service ID.
 **
 ** Definition of service ID for IpduM_TxConfirmation.
 */
#define IPDUM_SID_TX_CONFIRMATION  0x40U


#if (defined IPDUM_SID_TRIGGER_TRANSMIT)
#error IPDUM_SID_TRIGGER_TRANSMIT already defined
#endif
/** \brief AUTOSAR API service ID.
 **
 ** Definition of service ID for IpduM_TriggerTransmit.
 */
#define IPDUM_SID_TRIGGER_TRANSMIT  0x41U


#if (defined IPDUM_SID_MAIN_FUNCTION)
#error IPDUM_SID_MAIN_FUNCTION already defined
#endif
/** \brief AUTOSAR API service ID.
 **
 ** Definition of service ID for IpduM_MainFunction.
 */
#define IPDUM_SID_MAIN_FUNCTION  0x10U

/*------------------[macros for Trigger Mode]-------------------------------*/
/* Trigger modes */
#define IPDUM_TRIGGER_NONE                 0U
#define IPDUM_TRIGGER_STATIC               1U
#define IPDUM_TRIGGER_DYNAMIC              2U
#define IPDUM_TRIGGER_STATIC_OR_DYNAMIC    (IPDUM_TRIGGER_STATIC | IPDUM_TRIGGER_DYNAMIC)
#define IPDUM_TRIGGER_INVALID              (~(IPDUM_TRIGGER_STATIC | IPDUM_TRIGGER_DYNAMIC))

/* Reserved for TxConfirmationId */
#define IPDUM_RESERVED_PDUID               ((PduIdType)0xFFFFU)
/*==================[type definitions]======================================*/

/*------------------[common]------------------------------------------------*/

typedef uint16 IpduM_BitfieldType;
typedef uint16 IpduM_BitfieldArraySizeType;
typedef uint8 IpduM_SelectorType;

typedef struct
{
  IpduM_BitfieldType StartBit;
  IpduM_BitfieldType EndBit;
  IpduM_BitfieldType DestinationBit;
} IpduM_CopyBitFieldType;

/* Offset in Bytes in IpduM_DataMem */
typedef uint16 IpduM_DataOffsetType;

typedef uint16 IpduM_TxTimeoutType;

typedef uint16 IpduM_PathWayLenType;

/*------------------[RxPath]------------------------------------------------*/
typedef struct
{
  P2CONST(IpduM_CopyBitFieldType, TYPEDEF, IPDUM_APPL_CONST) CopyBitField;
  PduIdType OutgoingHandleId;
  IpduM_BitfieldArraySizeType CopyBitFieldArraySize;
  IpduM_SelectorType SelectorValue;
} IpduM_RxPartType;

typedef struct
{
#if (IPDUM_STATIC_PART_EXISTS==STD_ON)
  P2CONST(IpduM_RxPartType, TYPEDEF, IPDUM_APPL_CONST) StaticPart;
#endif
  P2CONST(IpduM_RxPartType, TYPEDEF, IPDUM_APPL_CONST) DynamicPart;
  uint16 DynamicPduArraySize;
  IpduM_BitfieldType StartBit;
  IpduM_BitfieldType EndBit;
} IpduM_RxPathWayType;

/*------------------[TxPath]------------------------------------------------*/
typedef uint16 IpduM_TxOffsetType;

typedef struct
{
#if (IPDUM_ZERO_COPY==STD_OFF)
  IpduM_DataOffsetType BufferOffset;   /* Offset in bytes for first element of TxBuffer */
#endif
  uint16 Size; /* Caution: Size in bits */
  IpduM_TxTimeoutType ConfirmationTimeout;
#if (IPDUM_STATIC_PART_EXISTS==STD_ON)
  PduIdType TxStaticConfirmHandleId;
#endif
#if (IPDUM_DYNAMIC_PART_QUEUE==STD_ON)
  IpduM_DataOffsetType QueueOffset;    /* Offset in bytes for first element in queue */
#endif
  IpduM_BitfieldType StartBit;
  IpduM_BitfieldType EndBit;
  IpduM_SelectorType DynamicConfirmArraySize; /* The maximum value is restricted to the maximum selector value */
  uint8 UnusedAreasDefault;
  uint8 TriggerMode;
#if (IPDUM_DYNAMIC_PART_QUEUE==STD_ON)
  uint8 QueueSize;
#endif
} IpduM_TxPathWayType;

typedef struct
{
  P2CONST(IpduM_CopyBitFieldType, TYPEDEF, IPDUM_APPL_CONST) CopyBitField;
  IpduM_BitfieldArraySizeType CopyBitFieldArraySize;
  PduIdType PduRTxConfirmationPduId; /* This value is taken from the PduR configuration parameter PduRDestPduHandleId */
  PduIdType TxConfirmationPduId; /* This value is taken from the IpduM configuration parameter IpduMTxConfirmationPduId */
  PduIdType TxOutgoingPduId; /* This value is taken from the PduR configuration parameter PduRSourcePduHandleId */
#if (IPDUM_DYNAMIC_PART_QUEUE==STD_ON)
  uint8 DynamicPriority;
#endif
#if (IPDUM_AUTOMATIC_SELECTOR==STD_ON)
  IpduM_SelectorType SelectorValue;
#endif
} IpduM_TxPartType;

/*------------------[Configuration]-----------------------------------------*/
typedef struct
{
  P2CONST(IpduM_RxPathWayType, TYPEDEF, IPDUM_APPL_CONST) RxPathWay;
  P2CONST(IpduM_TxPathWayType, TYPEDEF, IPDUM_APPL_CONST) TxPathWay;
  P2CONST(IpduM_TxPartType, TYPEDEF, IPDUM_APPL_CONST) TxPart;
  /* Length of RxPathway array */
  IpduM_PathWayLenType RxPathWayLen;
  /* Number of TxPathway array entries */
  IpduM_PathWayLenType TxPathWayLen;
#if (IPDUM_DEV_ERROR_DETECT==STD_ON)
  /* Length of TxPart array */
  IpduM_PathWayLenType TxPartLen;
#endif
  /* Offset for dynamic part */
  IpduM_TxOffsetType TxDynamicPartOffset;
} IpduM_ConfigType;

/*------------------[Data]--------------------------------------------------*/
typedef struct
{
  /* !LINKSTO IPDUM.EB.IPDUM902,2 */
  PduIdType PdumTxPduId;
  PduIdType PduRTxConfirmationPduId;
  uint16 DynamicPriority;
  /* !LINKSTO IPDUM.EB.IPDUM902,2 */
  uint8 SduData[IPDUM_TX_SDU_SIZE];
} IpduM_QueueEntryType;

typedef struct
{
  IpduM_TxTimeoutType ConfirmationTimeoutCounter;
  PduIdType PduRTxConfirmationPduId; /* ID which the PduR has to be called for confirmation */
#if (IPDUM_DYNAMIC_PART_QUEUE == STD_ON)
  /* Position of next free entry in queue */
  uint8 QueuePos;
#endif
} IpduM_TxDataType;

/*==================[external function declarations]========================*/

/*==================[internal function declarations]========================*/

/*==================[external constants]====================================*/

/*==================[internal constants]====================================*/

/*==================[external data]=========================================*/

/*==================[internal data]=========================================*/

/*==================[external function definitions]=========================*/

/*==================[internal function definitions]=========================*/

#endif /* if !defined( IPDUM_TYPES_H ) */
/*==================[end of file]===========================================*/
