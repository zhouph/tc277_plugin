/**
 * \file
 *
 * \brief AUTOSAR IpduM
 *
 * This file contains the implementation of the AUTOSAR
 * module IpduM.
 *
 * \author Elektrobit Automotive GmbH, 91058 Erlangen, Germany
 *
 * Copyright 2005 - 2013 Elektrobit Automotive GmbH
 * All rights exclusively reserved for Elektrobit Automotive GmbH,
 * unless expressly agreed to otherwise.
 */

/* This file contains the PostBuild configuration of the AUTOSAR module IpduM */

/** \addtogroup IpduM
 ** @{ */

[!AUTOSPACING!]
[!//
/*==================[inclusions]============================================*/
#include <IpduM_PBcfg.h>

[!INCLUDE "../include/IpduM_Macros.m"!]
[!INCLUDE "../include/IpduM_checks.m"!]

/*==================[macros]================================================*/

/*==================[type definitions]======================================*/

/*==================[external function declarations]========================*/

/*==================[internal function declarations]========================*/

#define IPDUM_START_CONFIG_DATA_UNSPECIFIED
#include <MemMap.h>

[!// -----------------------------------------------------------------------------------------------
[!SELECT "IpduMConfig/*[1]"!]
/*==================[internal constants for [!"name(.)"!]]==================*/
[!IF "node:exists(IpduMRxPathway/*)"!]
/*------------------------------Rx------------------------------------------*/
[!/* Looping through all the configured RxPathWays
after ordering the nodes based on the handle ID */!]
[!LOOP "node:order(IpduMRxPathway/*, 'node:IpduMRxIndication/IpduMRxHandleId')"!]
[!VAR "HandleId" = "IpduMRxIndication/IpduMRxHandleId"!]
[!IF "(node:exists(IpduMRxIndication/IpduMRxStaticPart))"!]
      [!VAR "NrOfStaticCopyBitFields" = "num:i(count(IpduMRxIndication/IpduMRxStaticPart/IpduMSegment/*))"!]
/** \brief Array of static part copy bit field structures of IpduMRxPathway with handle ID [!"$HandleId"!] */
static CONST(IpduM_CopyBitFieldType, IPDUM_APPL_CONST) IpduM_Rx_StaticPart_CopyBitField_[!"$HandleId"!][[!"$NrOfStaticCopyBitFields"!]] =
{
  [!LOOP "IpduMRxIndication/IpduMRxStaticPart/IpduMSegment/*"!]
  {
  [!INDENT "4"!]
    /* StartBit */
    [!"IpduMSegmentPosition"!]U,
    /* EndBit */
    [!"num:i(IpduMSegmentLength + IpduMSegmentPosition - 1)"!]U,
    /* DestinationBit */
    [!IF "(node:exists(IpduMDestinationBit))"!]
      [!"IpduMDestinationBit"!]U
    [!ELSE!]
      [!"IpduMSegmentPosition"!]U
    [!ENDIF!]
  [!ENDINDENT!]
  },
  [!ENDLOOP!]
};


[!// -----------------------------------------------------------------------------------------------
/** \brief Static part of IpduMRxPathway with handle ID [!"$HandleId"!] */
static CONST(IpduM_RxPartType, IPDUM_APPL_CONST) IpduM_Rx_StaticPart_[!"$HandleId"!] =
{
[!INDENT "2"!]
  /* CopyBitField */
  IpduM_Rx_StaticPart_CopyBitField_[!"$HandleId"!],
  /* OutgoingHandleId */
  [!CALL "GetOutputPduId","IpduMOutgoingPduRef"="./IpduMRxIndication/IpduMRxStaticPart/IpduMOutgoingStaticPduRef"!]
  [!"$PduID"!]U,
  /* CopyBitFieldArraySize */
  [!"$NrOfStaticCopyBitFields"!]U,
  /* SelectorValue */
  0U
[!ENDINDENT!]
};
[!ENDIF!]


[!// -----------------------------------------------------------------------------------------------
[!VAR "NrOfDynamicCopyBitFields" = "num:i(count(IpduMRxIndication/IpduMRxDynamicPart/*/IpduMSegment/*))"!]
/** \brief Array of dynamic part copy bit field structures of IpduMRxPathway with handle ID [!"$HandleId"!] */
static CONST(IpduM_CopyBitFieldType, IPDUM_APPL_CONST) IpduM_Rx_DynamicPart_CopyBitField_[!"$HandleId"!][[!"($NrOfDynamicCopyBitFields)"!]] =
{
[!LOOP "node:order(IpduMRxIndication/IpduMRxDynamicPart/*, 'node:IpduMRxSelectorValue')"!]
[!LOOP "IpduMSegment/*"!]
  {
  [!INDENT "4"!]
    /* StartBit */
    [!"IpduMSegmentPosition"!]U,
    /* EndBit */
    [!"num:i(IpduMSegmentLength + IpduMSegmentPosition - 1)"!]U,
    /* DestinationBit */
    [!IF "(node:exists(IpduMDestinationBit))"!]
      [!"IpduMDestinationBit"!]U
    [!ELSE!]
      [!"IpduMSegmentPosition"!]U
    [!ENDIF!]
  [!ENDINDENT!]
  },
[!ENDLOOP!]
[!ENDLOOP!]
};


[!// -----------------------------------------------------------------------------------------------
[!VAR "CopyBitFieldIndex" = "0"!]
/** \brief Array of dynamic part structures of IpduMRxPathway with handle ID [!"$HandleId"!] */
static CONST(IpduM_RxPartType, IPDUM_APPL_CONST) IpduM_Rx_DynamicPart_[!"$HandleId"!][[!"num:i(count(IpduMRxIndication/IpduMRxDynamicPart/*))"!]] =
{
[!LOOP "node:order(IpduMRxIndication/IpduMRxDynamicPart/*, 'node:IpduMRxSelectorValue')"!]
  {
  [!INDENT "4"!]
    /* CopyBitField */
    &IpduM_Rx_DynamicPart_CopyBitField_[!"$HandleId"!][[!"num:i($CopyBitFieldIndex)"!]],
    [!VAR "CopyBitFieldIndex" = "$CopyBitFieldIndex + count(IpduMSegment/*)"!]
    /* OutgoingHandleId */
    [!CALL "GetOutputPduId","IpduMOutgoingPduRef"="./IpduMOutgoingDynamicPduRef"!]
    [!"$PduID"!]U,
    /* CopyBitFieldArraySize */
    [!"num:i(count(IpduMSegment/*))"!]U,
    /* SelectorValue */
    [!"IpduMRxSelectorValue"!]U
  [!ENDINDENT!]
  },
  [!ENDLOOP!]
};
[!ENDLOOP!]

[!// -----------------------------------------------------------------------------------------------
[!VAR "NrOfRxPathWays" = "num:i(count(IpduMRxPathway/*))"!]
/** \brief Array of receive pathway structures */
static CONST(IpduM_RxPathWayType, IPDUM_APPL_CONST) IpduM_RxPathWay[[!"$NrOfRxPathWays"!]] =
{
  [!LOOP "node:order(IpduMRxPathway/*, 'node:IpduMRxIndication/IpduMRxHandleId')"!]
  [!VAR "HandleId" = "IpduMRxIndication/IpduMRxHandleId"!]
  {
  [!INDENT "4"!]
    [!INDENT "0"!]#if (IPDUM_STATIC_PART_EXISTS==STD_ON)
    [!ENDINDENT!]
      [!IF "(node:exists(IpduMRxIndication/IpduMRxStaticPart))"!]
        /* StaticPart */
        &IpduM_Rx_StaticPart_[!"$HandleId"!],
      [!ELSE!]
        NULL_PTR,
      [!ENDIF!]
    [!INDENT "0"!]#endif
    [!ENDINDENT!]
    /* DynamicPart */
    IpduM_Rx_DynamicPart_[!"$HandleId"!],
    /* DynamicPduArraySize */
    [!"num:i(count(IpduMRxIndication/IpduMRxDynamicPart/*))"!]U,
    /* SelectorStartBit */
    [!"IpduMRxIndication/IpduMSelectorFieldPosition/IpduMSelectorFieldPosition"!]U,
    /* SelectorEndBit */
    [!"num:i(IpduMRxIndication/IpduMSelectorFieldPosition/IpduMSelectorFieldLength + IpduMRxIndication/IpduMSelectorFieldPosition/IpduMSelectorFieldPosition -1 )"!]U
  [!ENDINDENT!]
  },
  [!ENDLOOP!]
};
[!ENDIF!]

[!// -----------------------------------------------------------------------------------------------
/*------------------------------Tx------------------------------------------*/
[!/*

IpduMTxPathwayExists tells if Tx path way is present in the configuration
*/!][!VAR "IpduMTxPathwayExists" = "num:i(0)"!][!/*

TxDynamicPartOffset is used to distinguish the static PDU from the dynamic one
*/!][!VAR "TxDynamicPartOffset"= "num:i(0)"!][!/*

Offset in bytes
*/!][!VAR "QueueOffset"= "num:i(0)"!][!/*

Offset in bytes
*/!][!VAR "BufferOffset"= "num:i(0)"!][!/*

Is IpduMTxPathway present
*/!][!IF "node:exists(IpduMTxPathway/*)"!]
  [!VAR "TxPathWayTotalCount" = "num:i(count(IpduMTxPathway/*))"!]
  [!IF "$IpduMDynamicPartQueue = 'true'"!]
    [!VAR "TxQueueTotalEntries" = "num:i(sum(IpduMTxPathway/*/IpduMTxRequest/IpduMQueueSize))"!]
  [!ELSE!]
    [!VAR "TxQueueTotalEntries" = "num:i(0)"!]
  [!ENDIF!]
  [!VAR "IpduMTxPathwayExists" = "num:i(1)"!]
  [!LOOP "IpduMTxPathway/*"!][!/*
    Logic to get the number of static parts
    */!]
    [!IF "node:exists(./IpduMTxRequest/IpduMTxStaticPart)"!]
      [!VAR "TxDynamicPartOffset"= "num:i($TxDynamicPartOffset + 1)"!]
    [!ENDIF!]
  [!ENDLOOP!][!/*

  */!][!VAR "StaticIdMax" = "num:i($TxDynamicPartOffset - 1)"!]
  [!VAR "DynamicIdMax" = "node:order(IpduMTxPathway/*/IpduMTxRequest/IpduMTxDynamicPart/*, 'node:IpduMTxDynamicHandleId')[last()]/IpduMTxDynamicHandleId"!]

[!// Generate IpduM_TxCopyBitField Look up table:
[!// a. Look up table begins with static IDs and then completes towards the dynamic ID
[!// b. If the static and/or dynamic part is not consecutive then an error is generated
/** \brief Array of configuration information for Copy Bit Field */
static CONST(IpduM_CopyBitFieldType, IPDUM_APPL_CONST) IpduM_TxCopyBitField[[!"num:i(count(IpduMTxPathway/*/IpduMTxRequest/IpduMTxStaticPart/IpduMSegment/*) + count(IpduMTxPathway/*/IpduMTxRequest/IpduMTxDynamicPart/*/IpduMSegment/*))"!]] =
{
  [!FOR "StaticId" = "0" TO "$StaticIdMax"!]
    [!VAR "StaticIdExist" = "num:i(0)"!]
    [!LOOP "IpduMTxPathway/*"!]
      [!IF "node:exists(./IpduMTxRequest/IpduMTxStaticPart)"!]
        [!IF "$StaticId = num:i(./IpduMTxRequest/IpduMTxStaticPart/IpduMTxStaticHandleId)"!]
          [!VAR "StaticIdExist" = "num:i(1)"!]
          [!LOOP "./IpduMTxRequest/IpduMTxStaticPart/IpduMSegment/*"!]
  {
    /* StartBit */
    [!"./IpduMSegmentPosition"!]U,

    /* EndBit */
    [!"num:i(./IpduMSegmentLength + ./IpduMSegmentPosition - 1)"!]U,

    /* DestinationBit */
    [!IF "(node:exists(./IpduMDestinationBit))"!]
      [!"./IpduMDestinationBit"!]U
    [!ELSE!]
      [!"./IpduMSegmentPosition"!]U
    [!ENDIF!]
  },
          [!ENDLOOP!]
        [!ENDIF!]
      [!ENDIF!]
    [!ENDLOOP!]
    [!IF "$StaticIdExist = 0"!]
      [!ERROR "Static Tx IDs should be zero based and consecutive"!]
    [!ENDIF!]
  [!ENDFOR!]
  [!FOR "DynamicId" = "0" TO "$TxDynamicPartOffset - 1"!]
    [!LOOP "IpduMTxPathway/*/IpduMTxRequest/IpduMTxDynamicPart/*"!]
      [!IF "$DynamicId = num:i(./IpduMTxDynamicHandleId)"!]
        [!ERROR "Static Tx IDs and Dynamic Tx IDs are overlapping"!]
      [!ENDIF!]
    [!ENDLOOP!]
  [!ENDFOR!]
  [!FOR "DynamicId" = "$TxDynamicPartOffset" TO "$DynamicIdMax"!]
    [!VAR "DynamicIdExist" = "num:i(0)"!]
    [!LOOP "IpduMTxPathway/*/IpduMTxRequest/IpduMTxDynamicPart/*"!]
      [!IF "$DynamicId = num:i(./IpduMTxDynamicHandleId)"!]
        [!VAR "DynamicIdExist" = "num:i(1)"!]
        [!LOOP "./IpduMSegment/*"!]
  {
    /* StartBit */
    [!"./IpduMSegmentPosition"!]U,

    /* EndBit */
    [!"num:i(./IpduMSegmentLength + ./IpduMSegmentPosition - 1)"!]U,

    /* DestinationBit */
    [!IF "(node:exists(./IpduMDestinationBit))"!]
      [!"./IpduMDestinationBit"!]U
    [!ELSE!]
      [!"./IpduMSegmentPosition"!]U
    [!ENDIF!]
  },
        [!ENDLOOP!]
      [!ENDIF!]
    [!ENDLOOP!]
    [!IF "$DynamicIdExist = 0"!]
        [!ERROR "Dynamic Tx IDs should be consecutive"!]
    [!ENDIF!]
  [!ENDFOR!]
};

[!// -----------------------------------------------------------------------------------------------
[!// Generate IpduM_TxPart Look up table:
[!// a. Look up table begins with static handle IDs and then completes towards the dynamic handle IDs
[!// b. Selector is assigned as 0 for all static parts.
/** \brief Array of configuration information for Tx Part */
static CONST(IpduM_TxPartType, IPDUM_APPL_CONST) IpduM_TxPart[[!"num:i($DynamicIdMax + 1)"!]] =
{
  [!VAR "TxCopyBitFieldIndex" = "num:i(0)"!]
  [!FOR "StaticId" = "0" TO "$StaticIdMax"!]
    [!LOOP "IpduMTxPathway/*"!]
      [!IF "node:exists(./IpduMTxRequest/IpduMTxStaticPart)"!]
        [!IF "$StaticId = num:i(./IpduMTxRequest/IpduMTxStaticPart/IpduMTxStaticHandleId)"!]
  /* Static part, IpduMTxStaticHandleId = [!"$StaticId"!] */
  {
          [!INDENT "4"!]
            /* CopyBitField */
            &IpduM_TxCopyBitField[[!"$TxCopyBitFieldIndex"!]],

            /* CopyBitFieldArraySize */
            [!"num:i(count(./IpduMTxRequest/IpduMTxStaticPart/IpduMSegment/*))"!]U,

            [!VAR "TxCopyBitFieldIndex" = "num:i($TxCopyBitFieldIndex + count(./IpduMTxRequest/IpduMTxStaticPart/IpduMSegment/*))"!]
            /* PduRTxConfirmationPduId */
            [!IF "./IpduMTxRequest/IpduMTxStaticPart/IpduMTxStaticConfirmation = 'true'"!]
                [!// Get the PDU ID which is referenced by IpduMTxStaticPduRef within PduR
                [!CALL "GetResponsePduId","IpduMPduRef"="./IpduMTxRequest/IpduMTxStaticPart/IpduMTxStaticPduRef"!][!//
                [!"$ResponsePduID"!]U,
            [!ELSE!]
                IPDUM_RESERVED_PDUID, /* Confirmation not enabled */
            [!ENDIF!][!//

            /* TxConfirmationPduId */
            [!"num:i(./IpduMTxRequest/IpduMTxConfirmationPduId)"!]U,

            [!CALL "GetOutputPduId","IpduMOutgoingPduRef"="./IpduMTxRequest/IpduMOutgoingPduRef"!]
            /* TxOutgoingPduId */
            [!"num:i($PduID)"!]U,

[!INDENT "0"!]#if (IPDUM_DYNAMIC_PART_QUEUE==STD_ON)
[!ENDINDENT!]
            /* DynamicPriority */
            0U,
[!INDENT "0"!]#endif
[!ENDINDENT!]
[!INDENT "0"!]#if (IPDUM_AUTOMATIC_SELECTOR==STD_ON)
[!ENDINDENT!]
            /* Selector */
            0U
[!INDENT "0"!]#endif
[!ENDINDENT!]
          [!ENDINDENT!]
  },
        [!ENDIF!]
      [!ENDIF!]
    [!ENDLOOP!]
  [!ENDFOR!]
  [!FOR "DynamicId" = "$TxDynamicPartOffset" TO "$DynamicIdMax"!]
    [!LOOP "IpduMTxPathway/*/IpduMTxRequest/IpduMTxDynamicPart/*"!]
      [!IF "$DynamicId = num:i(./IpduMTxDynamicHandleId)"!]
  /* Dynamic part, IpduMTxDynamicHandleId = [!"$DynamicId"!] */
  {
        [!INDENT "4"!]
          /* CopyBitField */
          &IpduM_TxCopyBitField[[!"$TxCopyBitFieldIndex"!]],

          /* CopyBitFieldArraySize */
          [!"num:i(count(./IpduMSegment/*))"!]U,

          [!VAR "TxCopyBitFieldIndex" = "num:i($TxCopyBitFieldIndex + count(./IpduMSegment/*))"!][!//

          /* PduRTxConfirmationPduId */
          [!IF "./IpduMTxDynamicConfirmation = 'true'"!]
              [!// Get the PDU ID which is referenced by IpduMTxDynamicPduRef
              [!CALL "GetResponsePduId","IpduMPduRef"="./IpduMTxDynamicPduRef"!][!//
              [!"$ResponsePduID"!]U,
          [!ELSE!]
              IPDUM_RESERVED_PDUID, /* Confirmation not enabled */
          [!ENDIF!][!//

          /* TxConfirmationPduId */
          [!"num:i(../../IpduMTxConfirmationPduId)"!]U,

          /* TxOutgoingPduId */
          [!CALL "GetOutputPduId","IpduMOutgoingPduRef"="../../IpduMOutgoingPduRef"!]
          [!"num:i($PduID)"!]U,

[!INDENT "0"!]#if (IPDUM_DYNAMIC_PART_QUEUE==STD_ON)
[!ENDINDENT!]
          /* DynamicPriority */
          [!IF "($IpduMDynamicPartQueue = 'true') and (../../IpduMQueueSize != num:i(0))"!]
            [!"num:i(./IpduMTxDynamicPriority)"!]U,
          [!ELSE!]
            0U,
          [!ENDIF!]
[!INDENT "0"!]#endif
[!ENDINDENT!]
[!INDENT "0"!]#if (IPDUM_AUTOMATIC_SELECTOR==STD_ON)
[!ENDINDENT!]
          /* Selector */
          [!IF "$IpduMTxAutomaticSelector = 'true'"!][!"num:i(./IpduMTxSelectorValue)"!][!ELSE!]0[!ENDIF!]U,
[!INDENT "0"!]#endif
[!ENDINDENT!]
        [!ENDINDENT!]
  },
      [!ENDIF!]
    [!ENDLOOP!]
  [!ENDFOR!]
};

[!// -----------------------------------------------------------------------------------------------
[!// Generate IpduM_TxPathWay Look up table
/** \brief Transmit Pathway array */
static CONST(IpduM_TxPathWayType, IPDUM_APPL_CONST) IpduM_TxPathWay[[!"num:i(count(IpduMTxPathway/*))"!]] =
{
    [!NOCODE!]
    [!// Loop over all TX pathways in order of IpduMTxConfirmationPduId.
    [!ENDNOCODE!]
    [!LOOP "node:order(IpduMTxPathway/*, 'node:value(./IpduMTxRequest/IpduMTxConfirmationPduId)')"!]
        [!NOCODE!]

        [!// BufferOffset
        [!VAR "OutgoingPduLength" = "node:value(as:ref(./IpduMTxRequest/IpduMOutgoingPduRef)/PduLength)"!]
        [!IF "$IpduMZeroCopy = 'false'"!]
          [!VAR "BufferOffsetStr" = "concat('((IpduM_DataOffsetType)(sizeof(IpduM_TxDataType) * ',string(num:i(count(as:modconf('IpduM')[1]/IpduMConfig/*[1]/IpduMTxPathway/*))),'U)) + ((IpduM_DataOffsetType)(sizeof(IpduM_QueueEntryType) * ',string($TxQueueTotalEntries),'U) + ',string($BufferOffset),'U)')"!]
          [!VAR "BufferOffset"= "num:i($BufferOffset + $OutgoingPduLength)"!]
        [!ELSE!]
          [!VAR "BufferOffsetStr" = "'0U'"!]
        [!ENDIF!]

        [!// Count enabled confirmations for the dynamic part
        [!VAR "ConfirmCountWithinTxRequest"= "num:i(count(./IpduMTxRequest/IpduMTxDynamicPart/*[node:value(./IpduMTxDynamicConfirmation) = 'true']))"!]

        [!// ConfirmationTimeout
        [!IF "node:exists(./IpduMTxRequest/IpduMTxConfirmationTimeout)"!]
          [!VAR "ConfirmationTimeout" = "num:i(./IpduMTxRequest/IpduMTxConfirmationTimeout div $IpduMConfigurationTimeBase)"!]
        [!ELSE!]
          [!VAR "ConfirmationTimeout" = "'0'"!]
        [!ENDIF!]

        [!// TxStaticConfirmHandleId
        [!IF "node:containsValue(./IpduMTxRequest/IpduMTxStaticPart/IpduMTxStaticConfirmation, 'true') = 'true'"!]
          [!// Get the response PDU ID which is referenced by IpduMTxStaticPduRef
          [!CALL "GetResponsePduId","IpduMPduRef"="./IpduMTxRequest/IpduMTxStaticPart/IpduMTxStaticPduRef"!]
          [!VAR "TxStaticConfirmHandleId" = "concat(string($ResponsePduID),'U')"!]
        [!ELSE!]
          [!VAR "TxStaticConfirmHandleId" = "'IPDUM_RESERVED_PDUID'"!]
        [!ENDIF!]

        [!// QueueOffset
        [!IF "$IpduMDynamicPartQueue = 'true'"!]
          [!VAR "QueueOffsetStr" = "concat('((IpduM_DataOffsetType)sizeof(IpduM_TxDataType) * ',string(num:i(count(as:modconf('IpduM')[1]/IpduMConfig/*[1]/IpduMTxPathway/*))),'U) + ((IpduM_DataOffsetType)sizeof(IpduM_QueueEntryType) * ',string($QueueOffset),'U)')"!]
          [!VAR "QueueOffset"= "num:i($QueueOffset + ./IpduMTxRequest/IpduMQueueSize)"!]
        [!ELSE!]
          [!VAR "QueueOffsetStr" = "'0U'"!]
        [!ENDIF!]

        [!// UnusedAreasDefault
        [!IF "node:exists(./IpduMTxRequest/IpduMIPduUnusedAreasDefault)"!]
          [!VAR "UnusedAreasDefault" = "num:i(./IpduMTxRequest/IpduMIPduUnusedAreasDefault)"!]
        [!ELSE!]
          [!VAR "UnusedAreasDefault" = "'0'"!]
        [!ENDIF!]

        [!// TriggerMode
        [!IF "./IpduMTxRequest/IpduMTxTriggerMode = 'DYNAMIC_PART_TRIGGER'"!]
          [!VAR "TriggerMode" = "'IPDUM_TRIGGER_DYNAMIC'"!]
        [!ELSEIF "./IpduMTxRequest/IpduMTxTriggerMode = 'NONE'"!]
          [!VAR "TriggerMode" = "'IPDUM_TRIGGER_NONE'"!]
        [!ELSEIF "./IpduMTxRequest/IpduMTxTriggerMode = 'STATIC_OR_DYNAMIC_PART_TRIGGER'"!]
          [!VAR "TriggerMode" = "'IPDUM_TRIGGER_STATIC_OR_DYNAMIC'"!]
        [!ELSE!]
          [!VAR "TriggerMode" = "'IPDUM_TRIGGER_STATIC'"!]
        [!ENDIF!]

        [!// QueueSize
        [!IF "$IpduMDynamicPartQueue = 'true'"!]
          [!VAR "QueueSize" = "num:i(./IpduMTxRequest/IpduMQueueSize)"!]
        [!ELSE!]
          [!VAR "QueueSize" = "'0'"!]
        [!ENDIF!]

        [!// Generate code
        [!ENDNOCODE!]
        [!INDENT "2"!]
          /* Path way [!"node:pos(.)"!]: [!"node:name(.)"!] */
          {
        [!INDENT "4"!]
        [!INDENT "0"!]#if (IPDUM_ZERO_COPY==STD_OFF)
        [!ENDINDENT!]
                /* BufferOffset */
               [!"$BufferOffsetStr"!],
        [!INDENT "0"!]#endif
        [!ENDINDENT!]

                /* Size */
                [!"num:i($OutgoingPduLength * 8)"!]U,

                /* ConfirmationTimeout */
                [!"$ConfirmationTimeout"!]U,

        [!INDENT "0"!]#if (IPDUM_STATIC_PART_EXISTS==STD_ON)
        [!ENDINDENT!]
                /* TxStaticConfirmHandleId */
                [!"$TxStaticConfirmHandleId"!],
        [!INDENT "0"!]#endif[!ENDINDENT!]

        [!INDENT "0"!]#if (IPDUM_DYNAMIC_PART_QUEUE==STD_ON)
        [!ENDINDENT!]
                /* QueueOffset */
                [!"$QueueOffsetStr"!],
        [!INDENT "0"!]#endif
        [!ENDINDENT!]

                /* StartBit of the selector value */
                [!"num:i(./IpduMTxRequest/IpduMSelectorFieldPosition/IpduMSelectorFieldPosition)"!]U,

                /* EndBit of the selector value */
                [!"num:i(./IpduMTxRequest/IpduMSelectorFieldPosition/IpduMSelectorFieldLength + ./IpduMTxRequest/IpduMSelectorFieldPosition/IpduMSelectorFieldPosition - 1)"!]U,

                /* DynamicConfirmArraySize */
                [!"$ConfirmCountWithinTxRequest"!]U,

                /* UnusedAreasDefault */
                [!"$UnusedAreasDefault"!]U,

                /* TriggerMode */
                [!"$TriggerMode"!],

        [!INDENT "0"!]#if (IPDUM_DYNAMIC_PART_QUEUE==STD_ON)
        [!ENDINDENT!]
                /* QueueSize */
                [!"$QueueSize"!]U,
        [!INDENT "0"!]#endif
        [!ENDINDENT!]
        [!ENDINDENT!][!//
          },
        [!ENDINDENT!][!//
    [!ENDLOOP!]
};
[!ENDIF!]

#define IPDUM_STOP_CONFIG_DATA_UNSPECIFIED
#include <MemMap.h>

/*==================[external constants for [!"name(.)"!]]==================*/
#define IPDUM_START_CONFIG_DATA_UNSPECIFIED
#include <MemMap.h>

[!// -----------------------------------------------------------------------------------------------
/** \brief Configuration structure for IpduM */
/* !LINKSTO IPDUM.ASR40.IPDUM096,1 */
/* !LINKSTO IPDUM.ASR40.IPDUM075,1 */
CONST(IpduM_ConfigType, IPDUM_APPL_CONST) [!"name(.)"!] =
{
  /* RxPathWay */
  [!IF "node:exists(IpduMRxPathway/*)"!][!//
  IpduM_RxPathWay,
  [!ELSE!]
  NULL_PTR,
  [!ENDIF!]

  [!IF "$IpduMTxPathwayExists = 0"!]
  /* TxPathWay */
  NULL_PTR,

  /* TxPart */
  NULL_PTR,
  [!ELSE!]
  /* TxPathWay */
  IpduM_TxPathWay,

  /* TxPart */
  IpduM_TxPart,
  [!ENDIF!]

   /* RxPathWayLen, number of RxPathway array entries */
  [!IF "count(IpduMRxPathway/*)>0"!][!//
  [!"$NrOfRxPathWays"!]U,
  [!ELSE!]
  0U,
  [!ENDIF!]

  /* TxPathWayLen, number of TxPathway array entries */
  [!"num:i(count(IpduMTxPathway/*))"!]U,

[!INDENT "0"!]#if (IPDUM_DEV_ERROR_DETECT==STD_ON)
[!ENDINDENT!]
  /* TxPartLen, length of TxPart array */
  [!IF "count(IpduMTxPathway/*)>0"!][!//
  [!"num:i($DynamicIdMax + 1)"!]U,
  [!ELSE!]
  0U,
  [!ENDIF!]
[!INDENT "0"!]#endif
[!ENDINDENT!]

  /* TxDynamicPartOffset */
  [!"$TxDynamicPartOffset"!]U
};

#define IPDUM_STOP_CONFIG_DATA_UNSPECIFIED
#include <MemMap.h>

[!ENDSELECT!]
/*==================[internal data]=========================================*/

/*==================[external data]=========================================*/

/*==================[external function definitions]=========================*/

/*==================[internal function definitions]=========================*/

/** @} doxygen end group definition */
/*==================[end of file]===========================================*/
