[!/**
 * \file
 *
 * \brief AUTOSAR IpduM
 *
 * This file contains the implementation of the AUTOSAR
 * module IpduM.
 *
 * \author Elektrobit Automotive GmbH, 91058 Erlangen, Germany
 *
 * Copyright 2005 - 2013 Elektrobit Automotive GmbH
 * All rights exclusively reserved for Elektrobit Automotive GmbH,
 * unless expressly agreed to otherwise.
 */!][!//
[!NOCODE!]
[!IF "not(var:defined('IPDUM_MACROS_M'))"!]
[!VAR "IPDUM_MACROS_M"="'true'"!]
[!/*
****************************************************************************************************
* Create two lists of PDU names and the corresponding handle IDs.
* - SrcPduRefToHandleIdList will hold all source PDU IDs of all PduR routing paths.
* - DestPduRefToHandleIdList will hold all destination PDU IDs of all PduR routing paths.
* - ComPduRefToHandleIdList will hold all Com HandleIds referenced by request messages.
* Each list is realized as continuous string. The string contains tuples separated by ";". Each
* tuple contains two elements separated by ":". First element of tuple is the PDU reference. The
* Second element of the tuple is the corresponding handle ID.
* A list, for example, looks like this: Pdu_0:321;Pdu_1:322;Pdu_3:323;
* The location of the PDU AUTOSAR/TOP-LEVEL-PACKAGES/EcuC/ELEMENTS/EcuC/EcucPduCollection/Pdu/ is
* not shown in the example.
****************************************************************************************************
*/!]
[!VAR "SrcPduRefToHandleIdList"="''"!]
[!VAR "DestPduRefToHandleIdList"="''"!]
[!VAR "ComPduRefToHandleIdList"="''"!]
[!// Iterate over all routing paths to collect the source and destination PDU IDs
[!LOOP "as:modconf('PduR')[1]/PduRRoutingTables/*[1]/PduRRoutingTable/*[1]/PduRRoutingPath/*"!]
    [!// Get the source PDU ID of a routing path
    [!VAR "SrcHandleId" = "./PduRSrcPdu/PduRSourcePduHandleId"!]
    [!IF "num:isnumber($SrcHandleId) and node:refexists(./PduRSrcPdu/PduRSrcPduRef)"!]
        [!VAR "SrcPduRef" = "node:path(node:ref(./PduRSrcPdu/PduRSrcPduRef))"!]
        [!VAR "SrcPduRefToHandleIdList"!][!"$SrcPduRefToHandleIdList"!];[!"$SrcPduRef"!]:[!"$SrcHandleId"!][!ENDVAR!]
    [!ENDIF!]
    [!// Get the destination PDU IDs of a routing path
    [!LOOP "./PduRDestPdu/*"!]
        [!IF "node:exists(./PduRDestPduHandleId)"!]
            [!VAR "DestHandleId" = "./PduRDestPduHandleId"!]
            [!IF "num:isnumber($DestHandleId) and node:refexists(./PduRDestPduRef)"!]
                [!VAR "DestPduRef" = "node:path(node:ref(./PduRDestPduRef))"!]
                [!VAR "DestPduRefToHandleIdList"!][!"$DestPduRefToHandleIdList"!];[!"$DestPduRef"!]:[!"$DestHandleId"!][!ENDVAR!]
            [!ENDIF!]
        [!ENDIF!]
    [!ENDLOOP!]
[!ENDLOOP!]

[!LOOP "as:modconf('Com')[1]/ComConfig/*[1]/ComIPdu/*"!]
    [!IF "node:exists(./ComIPduHandleId)"!]
            [!VAR "ComHandleId" = "./ComIPduHandleId"!]
            [!IF "num:isnumber($ComHandleId) and node:refexists(./ComPduIdRef)"!]
                [!VAR "ComPduRef" = "node:path(node:ref(./ComPduIdRef))"!]
                [!VAR "ComPduRefToHandleIdList"!][!"$ComPduRefToHandleIdList"!];[!"$ComPduRef"!]:[!"$ComHandleId"!][!ENDVAR!]
            [!ENDIF!]
    [!ENDIF!]
[!ENDLOOP!]

[!/*
****************************************************************************************************
* MACRO to get the ComHandleId with which Com Api has to be called. The macro parameter
* "IpduMRequestMessagePduRef" must reference this PDU. The PDU ID is stored in the global variable "PduID".
****************************************************************************************************
*/!]
[!MACRO "GetComHandleId","IpduMRequestMessagePduRef"!][!NOCODE!]
    [!VAR "IpduMRequestMsgPduRefPath" = "node:path(node:ref($IpduMRequestMessagePduRef))"!]
    [!CALL "GetOutputPduId","IpduMOutgoingPduRef"="$IpduMRequestMessagePduRef"!]
    [!IF "$FoundIds ='true'"!]
      [!VAR "FoundIds" = "false()"!]
      [!LOOP "text:split($ComPduRefToHandleIdList,';')"!]
          [!IF "string(text:split(.,':')[1]) = $IpduMRequestMsgPduRefPath"!]
              [!VAR "PduID" = "text:split(.,':')[2]"!]
              [!VAR "FoundIds" = "true()"!]
          [!ENDIF!]
      [!ENDLOOP!]
      [!IF "$FoundIds = 'false'"!]
          [!ERROR!] The Com does not reference the PDU '[!"$IpduMRequestMsgPduRefPath"!]=[!"$ComPduRefToHandleIdList"!]' [!ENDERROR!]
      [!ENDIF!]
    [!ENDIF!]
[!ENDNOCODE!][!ENDMACRO!]

[!/*
****************************************************************************************************
* MACRO to get the PduR source PDU ID the PduR has to be called with. The macro parameter
* "IpduMOutgoingPduRef" must reference this PDU. The PDU ID is stored in the global variable "PduID".
****************************************************************************************************
*/!]
[!MACRO "GetOutputPduId","IpduMOutgoingPduRef"!][!NOCODE!]
    [!VAR "IpduMOutgoingPduRefPath" = "node:path(node:ref($IpduMOutgoingPduRef))"!]
    [!VAR "FoundIds" = "false()"!]
    [!LOOP "text:split($SrcPduRefToHandleIdList,';')"!]
        [!IF "string(text:split(.,':')[1]) = $IpduMOutgoingPduRefPath"!]
            [!VAR "PduID" = "text:split(.,':')[2]"!]
            [!VAR "FoundIds" = "true()"!]
        [!ENDIF!]
    [!ENDLOOP!]
    [!IF "$FoundIds = 'false'"!]
        [!ERROR!] The PduR does not reference the PDU '[!"node:path(node:ref($IpduMOutgoingPduRef))"!]' [!ENDERROR!]
    [!ENDIF!]
[!ENDNOCODE!][!ENDMACRO!]

[!/*
****************************************************************************************************
* MACRO to get the PduR destination PDU ID the PduR has to be called with. The macro parameter
* "IpduMPduRef" must reference this PDU. The PDU ID is stored in the global variable
* "ResponsePduID". In the case the ID not found the variable FoundReponseId is set to false instead
* of true. A not found ID indicates that the confirmation is not enabled in the PduR.
****************************************************************************************************
*/!]
[!MACRO "GetResponsePduId","IpduMPduRef"!][!NOCODE!]
    [!VAR "IpduMPduRefPath" = "node:path(node:ref($IpduMPduRef))"!]
    [!VAR "FoundReponseId" = "false()"!]
    [!LOOP "text:split($DestPduRefToHandleIdList,';')"!]
        [!IF "string(text:split(.,':')[1]) = $IpduMPduRefPath"!]
            [!VAR "ResponsePduID" = "text:split(.,':')[2]"!]
            [!VAR "FoundReponseId" = "true()"!]
        [!ENDIF!]
    [!ENDLOOP!]
    [!IF "$FoundReponseId = 'false'"!]
        [!ERROR!] Probably the IpduM confirmation is enabled but not the PduR confirmation or no PduR destination does reference this PDU at all. See PDU '[!"node:path(node:ref($IpduMPduRef))"!]' [!ENDERROR!]
    [!ENDIF!]
[!ENDNOCODE!][!ENDMACRO!]

[!/*
****************************************************************************************************
* MACRO to get the string for the symbolic name value. Sets variable "SymbolName" holding either the 
* short name of the referenced container or (if not available) the provided "Oldname".
* "ShortNameRef" must reference the container holding the short name. (AUTOSAR short name)
* "OldName" must hold a unique string for the case that the specification
* conform short name does not exist. This is supplied to be backward compatible.
****************************************************************************************************
*/!]
[!MACRO "GetSymbolName","ShortNameRef","OldName"!][!NOCODE!]
    [!VAR "SymbolName" = "asc:getShortName($ShortNameRef)"!]
    [!IF "$SymbolName = ''"!]
        [!VAR "SymbolName" = "$OldName"!]
    [!ENDIF!]
[!ENDNOCODE!][!ENDMACRO!]

[!/*
****************************************************************************************************
* MACRO to generate all symbolic name values for an ID
****************************************************************************************************
*/!]
[!MACRO "GenSymbols","SymbolicPrefix","SymbolicName","SymbolicIdName","SymbolicValue","PduName"!][!/*
*/!]/*------------------------------------------------------------------------------------------------*/
/* [!"node:path($SymbolicValue)"!] */
#if (defined [!"$SymbolicPrefix"!][!"$SymbolicName"!])
#error [!"$SymbolicPrefix"!][!"$SymbolicName"!] is already defined
#endif
/** \brief Export symbolic name value for [!"$SymbolicIdName"!] */
#define [!"$SymbolicPrefix"!][!"$SymbolicName"!] [!"node:value($SymbolicValue)"!]U /* [!"$PduName"!] */

#if (!defined IPDUM_DONT_PROVIDE_LEGACY_SYMBOLIC_NAMES)
#if (defined [!"$SymbolicName"!])
#error [!"$SymbolicName"!] is already defined
#endif
/** \brief Export symbolic name value without prefix (AUTOSAR version <= 3.1 rev4) */
#define [!"$SymbolicName"!] [!"node:value($SymbolicValue)"!]U

#if (defined IpduM_[!"$SymbolicName"!])
#error IpduM_[!"$SymbolicName"!] is already defined
#endif
/** \brief Export symbolic name value with module abbreviation as prefix only (3.1 rev4 < AUTOSAR version <= AUTOSAR 4.0 rev2) */
#define IpduM_[!"$SymbolicName"!] [!"node:value($SymbolicValue)"!]U
#endif /* !defined IPDUM_DONT_PROVIDE_LEGACY_SYMBOLIC_NAMES */[!//
[!ENDMACRO!]

[!/*
****************************************************************************************************
* MACRO to generate a single symbolic name value for an ID
****************************************************************************************************
*/!]
[!MACRO "GenSymbol","SymbolicPrefix","SymbolicName","SymbolicIdName","SymbolicValue","PduName"!][!/*
*/!]/*------------------------------------------------------------------------------------------------*/
/* [!"node:path($SymbolicValue)"!] */
#if (defined [!"$SymbolicPrefix"!][!"$SymbolicName"!])
#error [!"$SymbolicPrefix"!][!"$SymbolicName"!] is already defined
#endif
/** \brief Export symbolic name value for [!"$SymbolicIdName"!] */
#define [!"$SymbolicPrefix"!][!"$SymbolicName"!] [!"node:value($SymbolicValue)"!]U /* [!"$PduName"!] */
[!ENDMACRO!]
[!ENDIF!]
[!ENDNOCODE!][!//