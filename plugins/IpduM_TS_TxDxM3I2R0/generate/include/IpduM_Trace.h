/**
 * \file
 *
 * \brief AUTOSAR IpduM
 *
 * This file contains the implementation of the AUTOSAR
 * module IpduM.
 *
 * \author Elektrobit Automotive GmbH, 91058 Erlangen, Germany
 *
 * Copyright 2005 - 2013 Elektrobit Automotive GmbH
 * All rights exclusively reserved for Elektrobit Automotive GmbH,
 * unless expressly agreed to otherwise.
 */
#if (!defined IPDUM_TRACE_H)
#define IPDUM_TRACE_H
/*==================[inclusions]============================================*/

[!IF "node:exists(as:modconf('Dbg'))"!]
#include <Dbg.h>
[!ENDIF!]

/*==================[macros]================================================*/

#ifndef DBG_IPDUM_INIT_ENTRY
/** \brief Entry point of function IpduM_Init() */
#define DBG_IPDUM_INIT_ENTRY(a)
#endif

#ifndef DBG_IPDUM_INITSTATUS
/** \brief Change of IpduM_InitStatus */
#define DBG_IPDUM_INITSTATUS(a,b)
#endif

#ifndef DBG_IPDUM_INIT_EXIT
/** \brief Exit point of function IpduM_Init() */
#define DBG_IPDUM_INIT_EXIT(a)
#endif

#ifndef DBG_IPDUM_RXINDICATION_ENTRY
/** \brief Entry point of function IpduM_RxIndication() */
#define DBG_IPDUM_RXINDICATION_ENTRY(a,b)
#endif

#ifndef DBG_IPDUM_RXINDICATION_EXIT
/** \brief Exit point of function IpduM_RxIndication() */
#define DBG_IPDUM_RXINDICATION_EXIT(a,b)
#endif

#ifndef DBG_IPDUM_TRANSMIT_ENTRY
/** \brief Entry point of function IpduM_Transmit() */
#define DBG_IPDUM_TRANSMIT_ENTRY(a,b)
#endif

#ifndef DBG_IPDUM_TRANSMIT_EXIT
/** \brief Exit point of function IpduM_Transmit() */
#define DBG_IPDUM_TRANSMIT_EXIT(a,b,c)
#endif

#ifndef DBG_IPDUM_TRIGGERTRANSMIT_ENTRY
/** \brief Entry point of function IpduM_TriggerTransmit() */
#define DBG_IPDUM_TRIGGERTRANSMIT_ENTRY(a,b)
#endif

#ifndef DBG_IPDUM_TRIGGERTRANSMIT_EXIT
/** \brief Exit point of function IpduM_TriggerTransmit() */
#define DBG_IPDUM_TRIGGERTRANSMIT_EXIT(a,b,c)
#endif

#ifndef DBG_IPDUM_TXCONFIRMATION_ENTRY
/** \brief Entry point of function IpduM_TxConfirmation() */
#define DBG_IPDUM_TXCONFIRMATION_ENTRY(a)
#endif

#ifndef DBG_IPDUM_TXCONFIRMATION_EXIT
/** \brief Exit point of function IpduM_TxConfirmation() */
#define DBG_IPDUM_TXCONFIRMATION_EXIT(a)
#endif

#ifndef DBG_IPDUM_MAINFUNCTION_ENTRY
/** \brief Entry point of function IpduM_MainFunction() */
#define DBG_IPDUM_MAINFUNCTION_ENTRY()
#endif

#ifndef DBG_IPDUM_MAINFUNCTION_EXIT
/** \brief Exit point of function IpduM_MainFunction() */
#define DBG_IPDUM_MAINFUNCTION_EXIT()
#endif

#ifndef DBG_IPDUM_GETVERSIONINFO_ENTRY
/** \brief Entry point of function IpduM_GetVersionInfo() */
#define DBG_IPDUM_GETVERSIONINFO_ENTRY(a)
#endif

#ifndef DBG_IPDUM_GETVERSIONINFO_EXIT
/** \brief Exit point of function IpduM_GetVersionInfo() */
#define DBG_IPDUM_GETVERSIONINFO_EXIT(a)
#endif

#ifndef DBG_IPDUM_BITCOPY_ENTRY
/** \brief Entry point of function IpduM_BitCopy() */
#define DBG_IPDUM_BITCOPY_ENTRY(a,b,c,d)
#endif

#ifndef DBG_IPDUM_BITCOPY_EXIT
/** \brief Exit point of function IpduM_BitCopy() */
#define DBG_IPDUM_BITCOPY_EXIT(a,b,c,d,e)
#endif

#ifndef DBG_IPDUM_PREPAREPDU_ENTRY
/** \brief Entry point of function IpduM_PreparePdu() */
#define DBG_IPDUM_PREPAREPDU_ENTRY(a)
#endif

#ifndef DBG_IPDUM_PREPAREPDU_EXIT
/** \brief Exit point of function IpduM_PreparePdu() */
#define DBG_IPDUM_PREPAREPDU_EXIT(a)
#endif

#ifndef DBG_IPDUM_INSERTQUEUE_ENTRY
/** \brief Entry point of function IpduM_InsertQueue() */
#define DBG_IPDUM_INSERTQUEUE_ENTRY(a,b,c,d)
#endif

#ifndef DBG_IPDUM_INSERTQUEUE_EXIT
/** \brief Exit point of function IpduM_InsertQueue() */
#define DBG_IPDUM_INSERTQUEUE_EXIT(a,b,c,d,e)
#endif

#ifndef DBG_IPDUM_REMOVEQUEUE_ENTRY
/** \brief Entry point of function IpduM_RemoveQueue() */
#define DBG_IPDUM_REMOVEQUEUE_ENTRY(a)
#endif

#ifndef DBG_IPDUM_REMOVEQUEUE_EXIT
/** \brief Exit point of function IpduM_RemoveQueue() */
#define DBG_IPDUM_REMOVEQUEUE_EXIT(a,b)
#endif

#ifndef DBG_IPDUM_SETDYNAMICPDUREADYTOSEND_ENTRY
/** \brief Entry point of function IpduM_SetDynamicPduReadyToSend() */
#define DBG_IPDUM_SETDYNAMICPDUREADYTOSEND_ENTRY(a)
#endif

#ifndef DBG_IPDUM_SETDYNAMICPDUREADYTOSEND_EXIT
/** \brief Exit point of function IpduM_SetDynamicPduReadyToSend() */
#define DBG_IPDUM_SETDYNAMICPDUREADYTOSEND_EXIT(a)
#endif

#ifndef DBG_IPDUM_GETMAXPRIORITYQUEUE_ENTRY
/** \brief Entry point of function IpduM_GetMaxPriorityQueue() */
#define DBG_IPDUM_GETMAXPRIORITYQUEUE_ENTRY(a)
#endif

#ifndef DBG_IPDUM_GETMAXPRIORITYQUEUE_EXIT
/** \brief Exit point of function IpduM_GetMaxPriorityQueue() */
#define DBG_IPDUM_GETMAXPRIORITYQUEUE_EXIT(a,b)
#endif

#ifndef DBG_IPDUM_PROCESSREQUESTPDU_ENTRY
/** \brief Entry point of function IpduM_ProcessRequestPdu() */
#define DBG_IPDUM_PROCESSREQUESTPDU_ENTRY(a,b)
#endif

#ifndef DBG_IPDUM_PROCESSREQUESTPDU_EXIT
/** \brief Exit point of function IpduM_ProcessRequestPdu() */
#define DBG_IPDUM_PROCESSREQUESTPDU_EXIT(a,b,c)
#endif

/*==================[type definitions]======================================*/

/*==================[external function declarations]========================*/

/*==================[external constants]====================================*/

/*==================[external data]=========================================*/

#endif /* (!defined IPDUM_TRACE_H) */
/*==================[end of file]===========================================*/
