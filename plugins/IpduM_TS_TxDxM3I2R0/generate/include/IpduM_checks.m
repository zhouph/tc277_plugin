[!/**
 * \file
 *
 * \brief AUTOSAR IpduM
 *
 * This file contains the implementation of the AUTOSAR
 * module IpduM.
 *
 * \author Elektrobit Automotive GmbH, 91058 Erlangen, Germany
 *
 * Copyright 2005 - 2013 Elektrobit Automotive GmbH
 * All rights exclusively reserved for Elektrobit Automotive GmbH,
 * unless expressly agreed to otherwise.
 */!][!//
[!AUTOSPACING!][!//
[!INDENT "0"!][!//
[!/*

****************************************************************************
* Get all the general parameters in variables.
****************************************************************************
*/!][!//
[!VAR "IpduMConfigurationTimeBase"= "node:value(as:modconf('IpduM')[1]/IpduMGeneral/IpduMConfigurationTimeBase)"!][!//
[!VAR "IpduMDevErrorDetect"= "node:value(as:modconf('IpduM')[1]/IpduMGeneral/IpduMDevErrorDetect)"!][!//
[!VAR "IpduMStaticPartExists"= "node:value(as:modconf('IpduM')[1]/IpduMGeneral/IpduMStaticPartExists)"!][!//
[!VAR "IpduMVersionInfoApi"= "node:value(as:modconf('IpduM')[1]/IpduMGeneral/IpduMVersionInfoApi)"!][!//
[!VAR "IpduMZeroCopy"= "node:value(as:modconf('IpduM')[1]/IpduMGeneral/IpduMZeroCopy)"!][!//
[!VAR "IpduMByteCopy"= "node:value(as:modconf('IpduM')[1]/IpduMGeneral/IpduMByteCopy)"!][!//
[!VAR "IpduMDynamicPartQueue"= "node:value(as:modconf('IpduM')[1]/IpduMGeneral/IpduMDynamicPartQueue)"!][!//
[!VAR "IpduMTxAutomaticSelector"= "node:value(as:modconf('IpduM')[1]/IpduMGeneral/IpduMTxAutomaticSelector)"!][!//
[!/*

****************************************************************************
* Info if SHORT-NAMEs of IpduMRxIndications are not unique.
****************************************************************************
*/!][!//
[!VAR "IpduMRxIndicationNames"= "' '"!][!//
[!SELECT "IpduMConfig/*[1]"!][!//
  [!IF "node:exists(IpduMRxPathway/*)"!][!//
    [!LOOP "IpduMRxPathway/*"!][!//
      [!IF "contains($IpduMRxIndicationNames, concat(' ', name(IpduMRxIndication), ' '))"!][!//
         [!INFO!]The SHORT-NAMEs of the IpduMRxIndication are not unique. The SHORT-NAME " [!"name(IpduMRxIndication)"!] "  is used multiple times.[!ENDINFO!][!//      
      [!ELSE!][!//
        [!VAR "IpduMRxIndicationNames"= "concat($IpduMRxIndicationNames, name(IpduMRxIndication), ' ')"!][!//
      [!ENDIF!][!//
    [!ENDLOOP!][!//
  [!ENDIF!][!//
[!ENDSELECT!][!/*

****************************************************************************
* The sum of the bits of static part and each dynamic part do not exceed the
* IpduMSize * 8.
****************************************************************************
*/!][!//
[!LOOP "as:modconf('IpduM')[1]/IpduMConfig/*[1]/IpduMTxPathway/*"!][!//
  [!IF "./IpduMTxRequest/IpduMByteOrder != 'LITTLE_ENDIAN'"!][!//
    [!ERROR!] IpduM supports only LITTLE ENDIAN. IpduMByteOrder at '[!"node:path(.)"!]' violates this [!ENDERROR!][!//
  [!ENDIF!][!//
  [!VAR "IpduMStaticSize"= "num:i(0)"!][!//
  [!VAR "IpduMDynamicSize"= "num:i(0)"!][!//
  [!IF "node:refvalid(IpduMTxRequest/IpduMOutgoingPduRef)"!][!//
    [!VAR "IpduMTotalSize"= "num:i(as:ref(./IpduMTxRequest/IpduMOutgoingPduRef)/PduLength * 8)"!][!//
  [!ELSE!][!//
    [!VAR "IpduMTotalSize"= "num:i(0)"!][!//
  [!ENDIF!][!//
  [!IF "$IpduMTotalSize != 0"!][!//
    [!IF "node:exists(./IpduMTxRequest/IpduMTxStaticPart)"!][!//
      [!IF "not(node:refexists(./IpduMTxRequest/IpduMTxStaticPart/IpduMTxStaticPduRef))"!][!//
        [!ERROR!] Invalid reference: '[!"node:path(./IpduMTxRequest/IpduMTxStaticPart/IpduMTxStaticPduRef)"!]' [!ENDERROR!][!//
      [!ELSE!][!//
        [!VAR "IpduMStaticSrcPduSize"= "num:i(as:ref(./IpduMTxRequest/IpduMTxStaticPart/IpduMTxStaticPduRef)/PduLength * 8)"!][!//
      [!ENDIF!][!//
      [!LOOP "./IpduMTxRequest/IpduMTxStaticPart/IpduMSegment/*"!][!//
        [!VAR "IpduMStaticSize"= "num:i($IpduMStaticSize + ./IpduMSegmentLength)"!][!//
        [!IF "(num:i(./IpduMSegmentPosition + ./IpduMSegmentLength)) > $IpduMStaticSrcPduSize"!][!//
          [!ERROR!] The segment offset of a static segment shall not exceed the boundary of the referenced static COM I-PDU. segment at " [!"node:path(.)"!] "  violates this [!ENDERROR!][!//
        [!ENDIF!][!//
        [!IF "(num:i(./IpduMDestinationBit + ./IpduMSegmentLength)) > $IpduMTotalSize"!][!//
          [!ERROR!] The destination PDU offset of a static segment shall not exceed the boundary of the referenced IpduM I-PDU. segment at " [!"node:path(.)"!] "  violates this [!ENDERROR!][!//
        [!ENDIF!][!//
      [!ENDLOOP!][!//
    [!ENDIF!][!//
    [!LOOP "./IpduMTxRequest/IpduMTxDynamicPart/*"!][!//
      [!IF "not(node:refexists(./IpduMTxDynamicPduRef))"!][!//
        [!ERROR!] Invalid reference: '[!"node:path(./IpduMTxDynamicPduRef)"!]' [!ENDERROR!][!//
      [!ELSE!][!//
        [!VAR "IpduMDynSrcPduSize"= "num:i(as:ref(./IpduMTxDynamicPduRef)/PduLength * 8)"!][!//
      [!ENDIF!][!//
      [!LOOP "./IpduMSegment/*"!][!//
        [!VAR "IpduMDynamicSize"= "num:i($IpduMDynamicSize + ./IpduMSegmentLength)"!][!//
        [!IF "(num:i(./IpduMSegmentPosition + ./IpduMSegmentLength)) > $IpduMDynSrcPduSize"!][!//
          [!ERROR!] The segment offset of a dynamic segment shall not exceed the boundary of the referenced dynamic COM I-PDU. segment at " [!"node:path(.)"!] "  violates this [!ENDERROR!][!//
        [!ENDIF!][!//
        [!IF "(num:i(./IpduMDestinationBit + ./IpduMSegmentLength)) > $IpduMTotalSize"!][!//
          [!ERROR!] The destination PDU offset of a dynamic segment shall not exceed the boundary of the referenced IpduM I-PDU. segment at " [!"node:path(.)"!] "  violates this [!ENDERROR!][!//
        [!ENDIF!][!//
      [!ENDLOOP!][!//
      [!IF "($IpduMDynamicSize + $IpduMStaticSize) > $IpduMTotalSize"!][!//
        [!ERROR!]Static or dynamic part segments overlaps in IpduM I-PDU at " [!"node:path(../../..)"!] ".  [!ENDERROR!][!//
      [!ENDIF!][!//
      [!VAR "IpduMDynamicSize"= "num:i(0)"!][!//
    [!ENDLOOP!][!//
  [!ENDIF!][!//
[!ENDLOOP!][!//
[!ENDINDENT!][!//
