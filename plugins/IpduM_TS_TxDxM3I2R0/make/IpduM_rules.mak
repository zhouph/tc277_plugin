# \file
#
# \brief AUTOSAR IpduM
#
# This file contains the implementation of the AUTOSAR
# module IpduM.
#
# \author Elektrobit Automotive GmbH, 91058 Erlangen, Germany
#
# Copyright 2005 - 2013 Elektrobit Automotive GmbH
# All rights exclusively reserved for Elektrobit Automotive GmbH,
# unless expressly agreed to otherwise.

#################################################################
# DEFINITIONS


#################################################################
# REGISTRY


LIBRARIES_TO_BUILD  += IpduM_src

IpduM_lib_FILES       +=

IpduM_src_FILES += $(IpduM_CORE_PATH)\src\IpduM.c \
                       $(IpduM_CORE_PATH)\src\IpduM_ComCallout.c \
                       $(IpduM_OUTPUT_PATH)\src\IpduM_PBcfg.c \
                       $(IpduM_OUTPUT_PATH)\src\IpduM_Lcfg.c

CC_FILES_TO_BUILD   +=


CPP_FILES_TO_BUILD  +=
ASM_FILES_TO_BUILD  +=


MAKE_CLEAN_RULES    +=
MAKE_GENERATE_RULES +=
MAKE_COMPILE_RULES  +=
#MAKE_DEBUG_RULES    +=
MAKE_CONFIG_RULES   +=
#MAKE_ADD_RULES      +=

#################################################################
# DEPENDENCIES (only for assembler files)
#

#################################################################
# RULES
