# \file
#
# \brief AUTOSAR Crc
#
# This file contains the implementation of the AUTOSAR
# module Crc.
#
# \author Elektrobit Automotive GmbH, 91058 Erlangen, Germany
#
# Copyright 2005 - 2013 Elektrobit Automotive GmbH
# All rights exclusively reserved for Elektrobit Automotive GmbH,
# unless expressly agreed to otherwise.

#################################################################
# DEFINITIONS

#################################################################
# REGISTRY

Crc_src_FILES      := $(Crc_CORE_PATH)\src\Crc.c

LIBRARIES_TO_BUILD += Crc_src

#################################################################
# DEPENDENCIES (only for assembler files)
#

#################################################################
# RULES




