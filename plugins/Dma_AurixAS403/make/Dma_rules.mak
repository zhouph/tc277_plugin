# \file
#
# \brief AUTOSAR Dma
#
# This file contains the implementation of the AUTOSAR
# module Dma.
#
# \author Elektrobit Automotive GmbH, 91058 Erlangen, Germany
#
# Copyright 2005 - 2013 Elektrobit Automotive GmbH
# All rights exclusively reserved for Elektrobit Automotive GmbH,
# unless expressly agreed to otherwise.

#################################################################
# REGISTRY

LIBRARIES_TO_BUILD     += Dma_src

Dma_src_FILES       += $(Dma_CORE_PATH)\src\Dma.c
Dma_src_FILES       += $(Dma_OUTPUT_PATH)\src\Dma_PBCfg.c

ifeq ($(TS_BUILD_POST_BUILD_BINARY),TRUE)
# If the post build binary shall be built do not compile any files other then the postbuild files.
Dma_src_FILES :=
endif
