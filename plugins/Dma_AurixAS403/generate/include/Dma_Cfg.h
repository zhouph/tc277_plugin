[!/****************************************************************************
**                                                                           **
** Copyright (C) Infineon Technologies (2013)                                **
**                                                                           **
** All rights reserved.                                                      **
**                                                                           **
** This document contains proprietary information belonging to Infineon      **
** Technologies. Passing on and copying of this document, and communication  **
** of its contents is not permitted without prior written authorization.     **
**                                                                           **
*******************************************************************************
**                                                                           **
**  $FILENAME   : Dma_Cfg.h $                                                **
**                                                                           **
**  $CC VERSION : \main\8 $                                                  **
**                                                                           **
**  $DATE       : 2013-10-18 $                                               **
**                                                                           **
**  AUTHOR      : DL-AUTOSAR-Engineering                                     **
**                                                                           **
**  VENDOR      : Infineon Technologies                                      **
**                                                                           **
**  DESCRIPTION : This file contains                                         **
**                Code template for Dma_Cfg.h file                           **
**                                                                           **
**  MAY BE CHANGED BY USER [yes/no]: No                                      **
**                                                                           **
******************************************************************************/!][!//
[!//
/******************************************************************************
**                                                                           **
** Copyright (C) Infineon Technologies (2013)                                **
**                                                                           **
** All rights reserved.                                                      **
**                                                                           **
** This document contains proprietary information belonging to Infineon      **
** Technologies. Passing on and copying of this document, and communication  **
** of its contents is not permitted without prior written authorization.     **
**                                                                           **
*******************************************************************************
**                                                                           **
**  FILENAME  : Dma_Cfg.h                                                    **
**                                                                           **
**  $CC VERSION : \main\8 $                                                  **
**                                                                           **
**  DATE, TIME: [!"$date"!], [!"$time"!]                                         **
**                                                                           **
**  GENERATOR : Build [!"$buildnr"!]                                           **
**                                                                           **
**  AUTHOR    : DL-AUTOSAR-Engineering                                       **
**                                                                           **
**  VENDOR    : Infineon Technologies                                        **
**                                                                           **
**  DESCRIPTION  : DMA configuration generated out of ECU configuration      **
**                 file (Dma.bmd/.xdm)                                       **
**                                                                           **
**  MAY BE CHANGED BY USER [yes/no]: No                                      **
**                                                                           **
******************************************************************************/
/******************************************************************************
**                                                                           **
    TRACEBILITY :[cover parentID = DS_NAS_PR446,ASW:1500,ASW:1801,
                  ASW:1501,ASW:1504,ASW:1507,ASW:1508,ASW:1512,ASW:1513,
                  ASW:1514,ASW:1516,ASW:1517,ASW:1519,ASW:1523,ASW:1524,
                  ASW:1528][/cover]                                          **
******************************************************************************/

#ifndef DMA_CFG_H 
#define DMA_CFG_H 

/*******************************************************************************
**                      Includes                                              **
*******************************************************************************/

/*******************************************************************************
**                      Global Macro Definitions                              **
*******************************************************************************/
[!/* Select MODULE-CONFIGURATION as context-node */!][!//
[!SELECT "as:modconf('Dma')[1]"!][!//
[!//
[!VAR "SwMajorVersion" = "text:split($moduleSoftwareVer, '.')[position()-1 = 0]"!][!//
[!VAR "SwMinorVersion" = "text:split($moduleSoftwareVer, '.')[position()-1 = 1]"!][!//
[!VAR "SwRevisionVersion" = "text:split($moduleSoftwareVer, '.')[position()-1 = 2]"!][!//

#define DMA_VENDOR_ID                   ((uint16)17U)
#define DMA_MODULE_ID                   ((uint16)255U)
#define DMA_MODULE_INSTANCE             ((uint8)[!"DmaGeneralConfiguration/DmaIndex"!]U)

/*SW Version Information*/
#define DMA_SW_MAJOR_VERSION            ([!"$SwMajorVersion"!]U)
#define DMA_SW_MINOR_VERSION            ([!"$SwMinorVersion"!]U)
#define DMA_SW_PATCH_VERSION            ([!"$SwRevisionVersion"!]U)

/*Number of DMA channels present in the controller*/
#define DMA_NUM_OF_CHANNELS             ([!"ecu:get('Dma.NoOfChannels')"!]U)

/* Derived Configuration for DmaDevErrorDetect */
#define DMA_DEV_ERROR_DETECT            ([!//
[!IF "DmaGeneralConfiguration/DmaDevErrorDetect = 'true'"!][!//
STD_ON[!//
[!ELSE!][!//
STD_OFF[!//
[!ENDIF!][!//
)

/* Derived Configuration for DmaVersionInfoApi */
#define DMA_VERSION_INFO_API            ([!//
[!IF "DmaGeneralConfiguration/DmaVersionInfoApi = 'true'"!][!//
STD_ON[!//
[!ELSE!][!//
STD_OFF[!//
[!ENDIF!][!//
)

/* Derived Configuration for DmaDoubleBufferEnable */
#define DMA_DOUBLE_BUFFER_ENABLE      ([!//
[!IF "DmaGeneralConfiguration/DmaDoubleBufferEnable = 'true'"!][!//
STD_ON[!//
[!ELSE!][!//
STD_OFF[!//
[!ENDIF!][!//
)

/* Derived Configuration for DmaDeInitApi */
#define DMA_DEINIT_API                  ([!//
[!IF "DmaGeneralConfiguration/DmaDeInitApi  = 'true'"!][!//
STD_ON[!//
[!ELSE!][!//
STD_OFF[!//
[!ENDIF!][!//
)

/* Derived Configuration for DmaLinkedListEnable*/
#define DMA_LINKED_LIST_ENABLE          ([!//
[!IF "DmaGeneralConfiguration/DmaLinkedListEnable = 'true'"!][!//
STD_ON[!//
[!ELSE!][!//
STD_OFF[!//
[!ENDIF!][!//
)

/* Derived Configuration DmaDebugSupport*/
#define DMA_DEBUG_SUPPORT               ([!//
[!IF "DmaGeneralConfiguration/DmaDebugSupport = 'true'"!][!//
STD_ON[!//
[!ELSE!][!//
STD_OFF[!//
[!ENDIF!][!//
)
[!NOCODE!][!//
[!//
[!VAR "TotalDmaConfig" = "num:i(count(DmaConfigSet/*))"!][!//
[!/* Determine only one DmaConfigSet is configured 
                                        when DmaPBFixedAddress = true.*/!]
[!IF "DmaGeneralConfiguration/DmaPBFixedAddress = 'true'"!][!//
[!//
  [!ASSERT "not($TotalDmaConfig != 1)"!][!//
Config Error: when DmaGeneralConfiguration/DmaPBFixedAddress is set as true, [!//
Only one DmaConfigSet configuration is allowed. [!//
But DmaConfigSet has more than one configuration.[!//
  [!ENDASSERT!][!//
[!//
[!ENDIF!][!//
[!ENDNOCODE!][!//

/* Derived Configuration DmaPBFixedAddress*/
#define DMA_PB_FIXEDADDR                ([!//
[!IF "DmaGeneralConfiguration/DmaPBFixedAddress = 'true'"!][!//
STD_ON[!//
[!ELSE!][!//
STD_OFF[!//
[!ENDIF!][!//
)


[!ENDSELECT!][!//

/*******************************************************************************
**                      Global Type Definitions                               **
*******************************************************************************/

/*******************************************************************************
**                      Global Constant Declarations                          **
*******************************************************************************/

/*******************************************************************************
**                      Global Variable Declarations                          **
*******************************************************************************/

/*******************************************************************************
**                      Global Function Declarations                          **
*******************************************************************************/

/*******************************************************************************
**                      Global Inline Function Definitions                    **
*******************************************************************************/

#endif  /*End of DMA_CFG_H */


