# \file
#
# \brief AUTOSAR Fee
#
# This file contains the implementation of the AUTOSAR
# module Fee.
#
# \author Elektrobit Automotive GmbH, 91058 Erlangen, Germany
#
# Copyright 2005 - 2013 Elektrobit Automotive GmbH
# All rights exclusively reserved for Elektrobit Automotive GmbH,
# unless expressly agreed to otherwise.

#################################################################
# REGISTRY

LIBRARIES_TO_BUILD     += Fee_src

Fee_src_FILES       += $(Fee_CORE_PATH)\src\Fee.c
Fee_src_FILES       += $(Fee_CORE_PATH)\src\Fee_Ver.c
Fee_src_FILES       += $(Fee_OUTPUT_PATH)\src\Fee_PBCfg.c

ifeq ($(TS_BUILD_POST_BUILD_BINARY),TRUE)
# If the post build binary shall be built do not compile any files other then the postbuild files.
Fee_src_FILES :=
endif
