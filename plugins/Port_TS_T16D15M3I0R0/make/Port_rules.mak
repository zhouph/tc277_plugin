# \file
#
# \brief AUTOSAR Port
#
# This file contains the implementation of the AUTOSAR
# module Port.
#
# \author Elektrobit Automotive GmbH, 91058 Erlangen, Germany
#
# Copyright 2005 - 2013 Elektrobit Automotive GmbH
# All rights exclusively reserved for Elektrobit Automotive GmbH,
# unless expressly agreed to otherwise.

#################################################################
# REGISTRY

LIBRARIES_TO_BUILD     += Port_src

Port_src_FILES       += $(Port_CORE_PATH)\src\Port.c
Port_src_FILES       += $(Port_OUTPUT_PATH)\src\Port_PBCfg.c

ifeq ($(TS_BUILD_POST_BUILD_BINARY),TRUE)
# If the post build binary shall be built do not compile any files other then the postbuild files.
Port_src_FILES :=
endif
