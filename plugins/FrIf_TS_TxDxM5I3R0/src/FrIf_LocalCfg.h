/**
 * \file
 *
 * \brief AUTOSAR FrIf
 *
 * This file contains the implementation of the AUTOSAR
 * module FrIf.
 *
 * \author Elektrobit Automotive GmbH, 91058 Erlangen, Germany
 *
 * Copyright 2005 - 2013 Elektrobit Automotive GmbH
 * All rights exclusively reserved for Elektrobit Automotive GmbH,
 * unless expressly agreed to otherwise.
 */

#if !defined _FRIF_LOCALCFG_H_
#define _FRIF_LOCALCFG_H_

/******************************************************************************
 Global Macros
******************************************************************************/

#define FRIF_IMMEDIATE_RX_ENABLE STD_ON
#define FRIF_DECOUPLED_TX_ENABLE STD_ON
#define FRIF_TXCONFIRMATION_ENABLE STD_ON

#endif  /* _FRIF_LOCALCFG_H_ */

