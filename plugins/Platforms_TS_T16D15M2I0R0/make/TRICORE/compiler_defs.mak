# \file
#
# \brief AUTOSAR Platforms
#
# This file contains the implementation of the AUTOSAR
# module Platforms.
#
# \author Elektrobit Automotive GmbH, 91058 Erlangen, Germany
#
# Copyright 2005 - 2013 Elektrobit Automotive GmbH
# All rights exclusively reserved for Elektrobit Automotive GmbH,
# unless expressly agreed to otherwise.

#################################################################
# this file just references the compiler_defs.mak makfiles
# in the TOOLCHAIN directory

ifeq ($(Platforms_CORE_PATH), )
include $(SSC_ROOT)\Platforms_$(Platforms_VARIANT)\make\$(CPU)\$(TOOLCHAIN)\compiler$(DEFS_FILE_SUFFIX)
else
include $(Platforms_CORE_PATH)\make\$(CPU)\$(TOOLCHAIN)\compiler$(DEFS_FILE_SUFFIX)
endif
#################################################################
# Name of the debugger
DB ?= $(TRACE32_APP_PATH)\t32mtc.exe -c $(TRACE32_PATH)\config.t32

DEBUG_OPT_FILE ?= $(BOARD_PROJECT_PATH)\$(DERIVATE).cmm ..\output\bin\$(PROJECT).$(ABS_FILE_SUFFIX)

MAKE_INFO += DEBUG_SCRIPT="$(BOARD_PROJECT_PATH)\$(DERIVATE).cmm"

#################################################################
# PROJECT_BINARY: path to the executable
#
# Unlike FIRST_BUILD_TARGET, this is the .elf file
PROJECT_BINARY = $(BIN_OUTPUT_PATH)\$(PROJECT).$(ABS_FILE_SUFFIX)

