# \file
#
# \brief AUTOSAR Platforms
#
# This file contains the implementation of the AUTOSAR
# module Platforms.
#
# \author Elektrobit Automotive GmbH, 91058 Erlangen, Germany
#
# Copyright 2005 - 2013 Elektrobit Automotive GmbH
# All rights exclusively reserved for Elektrobit Automotive GmbH,
# unless expressly agreed to otherwise.

#################################################################
# The Application Makefile (Makefile.mak) contains the variable
# COMPILER_MODE. This variable shall have one of the following
# values: GLOBAL_OPTION_FILE, LOCAL_OPTION_MODE and NO_OPTION_MODE.
# If GLOBAL_OPTiON_MODE is set, one global parameter file will be
# created for all source files. If LOCAL_OPTION_FILE is set, an
# parameter file will be created for each source file and if
# NO_OPTION_FILE is defined, no option file will be created.

#################################################################
# Define location of file containing information of a run of
# make
#
ifneq ($(COMPILER_MODE), NO_OPTION_FILE)
MAKE_INC_FILE := $(OBJ_OUTPUT_PATH)\$(CPU)_$(TOOLCHAIN).$(INC_FILE_SUFFIX)
MAKE_INA_FILE := $(OBJ_OUTPUT_PATH)\$(CPU)_$(TOOLCHAIN).$(INA_FILE_SUFFIX)
endif

#################################################################
# The next rule generates an option file with the include paths
# and the compiler flags for each source file.
ifeq ($(COMPILER_MODE), LOCAL_OPTION_FILE)

#################################################################
# Compile all C files *.c -> *.obj
#
$(CC_TO_OBJ_BUILD_LIST) :

	$(shell echo $(GET_CC_BUILD_OPTIONS) > $(subst .$(OBJ_FILE_SUFFIX),.$(INC_FILE_SUFFIX),$@ ))
	$(foreach i, $(GET_CC_INCLUDE_FILES), $(shell echo $i >> $(subst .$(OBJ_FILE_SUFFIX),.$(INC_FILE_SUFFIX),$@ )))
ifeq ($(CREATE_PREPROCESSOR_FILE),YES)
	$(CC)  -E -f $(subst .$(OBJ_FILE_SUFFIX),.$(INC_FILE_SUFFIX),$@ ) -co $(getSourceFile) -o $(subst .$(OBJ_FILE_SUFFIX),.$(PRE_FILE_SUFFIX),$@)  
endif
ifeq ($(TASKING_MODE), COMPILER_ASSEMBLER)

	$(shell echo $(GET_ASM_BUILD_OPTIONS) > $(subst .$(OBJ_FILE_SUFFIX),.$(INA_FILE_SUFFIX),$@ ))
	$(foreach i, $(GET_ASM_INCLUDE_FILES), $(shell echo $i >> $(subst .$(OBJ_FILE_SUFFIX),.$(INA_FILE_SUFFIX),$@ )))

	$(CPRE)  -f $(subst .$(OBJ_FILE_SUFFIX),.$(INC_FILE_SUFFIX),$@ ) $(getSourceFile) -o $(subst .$(OBJ_FILE_SUFFIX),.src,$@)

	$(ASM) $(subst .$(OBJ_FILE_SUFFIX),.src,$@) -f  $(subst .$(OBJ_FILE_SUFFIX),.$(INA_FILE_SUFFIX),$@ ) -o $@
else

	$(CC)  -f $(subst .$(OBJ_FILE_SUFFIX),.$(INC_FILE_SUFFIX),$@ ) -co $(getSourceFile) -o $@
endif

else

#################################################################
# The next rule generates a global option file with the include
# pathes and the compiler flags. This file will be used for all
# source files.
ifeq ($(COMPILER_MODE), GLOBAL_OPTION_FILE)


#################################################################
# Create file containing compiler switches
#
$(MAKE_INC_FILE):

	$(shell echo $(GET_CC_BUILD_OPTIONS) > $(OBJ_OUTPUT_PATH)\$(CPU)_$(TOOLCHAIN).$(INC_FILE_SUFFIX) )
	$(foreach i,$(GET_CC_INCLUDE_FILES), $(shell echo $i >> $(OBJ_OUTPUT_PATH)\$(CPU)_$(TOOLCHAIN).$(INC_FILE_SUFFIX) ))
ifeq ($(TASKING_MODE), COMPILER_ASSEMBLER)

	$(shell echo $(GET_ASM_BUILD_OPTIONS) > $(OBJ_OUTPUT_PATH)\$(CPU)_$(TOOLCHAIN).$(INA_FILE_SUFFIX) )
	$(foreach i,$(GET_ASM_INCLUDE_FILES), $(shell echo $i >> $(OBJ_OUTPUT_PATH)\$(CPU)_$(TOOLCHAIN).$(INA_FILE_SUFFIX) ))
endif

$(CC_TO_OBJ_BUILD_LIST) :
ifeq ($(CREATE_PREPROCESSOR_FILE),YES)
	$(CC)  -E -f $(OBJ_OUTPUT_PATH)\$(CPU)_$(TOOLCHAIN).$(INC_FILE_SUFFIX) -co $(getSourceFile) -o $(subst .$(OBJ_FILE_SUFFIX),.$(PRE_FILE_SUFFIX),$@)
endif	
ifeq ($(TASKING_MODE), COMPILER_ASSEMBLER)

	$(CPRE)  -f $(OBJ_OUTPUT_PATH)\$(CPU)_$(TOOLCHAIN).$(INC_FILE_SUFFIX) $(getSourceFile) -o $(subst .$(OBJ_FILE_SUFFIX),.src,$@)

	$(ASM) $(subst .$(OBJ_FILE_SUFFIX),.src,$@) -f $(OBJ_OUTPUT_PATH)\$(CPU)_$(TOOLCHAIN).$(INA_FILE_SUFFIX) -o $@
else

	$(CC)  -f $(OBJ_OUTPUT_PATH)\$(CPU)_$(TOOLCHAIN).$(INC_FILE_SUFFIX) -co $(getSourceFile) -o $@
endif

else

#################################################################
# The last rule to compile C source files does not use an option
# file. If the windows comand line reports an error, the
# GLOBAL_OPTION_FILE or the LOCAL_OPTION_FILE mode shall be used.

$(CC_TO_OBJ_BUILD_LIST) :
ifeq ($(CREATE_PREPROCESSOR_FILE),YES)
	@$(CC)  -E $(GET_CC_BUILD_OPTIONS)  $(GET_CC_INCLUDE_FILES) -co $(getSourceFile) -o $(subst .$(OBJ_FILE_SUFFIX),.$(PRE_FILE_SUFFIX),$@) 
endif
ifeq ($(TASKING_MODE), COMPILER_ASSEMBLER)
	@$(CPRE) $(GET_CC_BUILD_OPTIONS)  $(GET_CC_INCLUDE_FILES) $(getSourceFile) -o $(subst .$(OBJ_FILE_SUFFIX),.src,$@)
	@$(ASM) $(GET_ASM_BUILD_OPTIONS) $(subst .$(OBJ_FILE_SUFFIX),.src,$@) -o $@
else
	@$(CC) $(GET_CC_BUILD_OPTIONS)  $(GET_CC_INCLUDE_FILES) -co $(getSourceFile) -o $@
endif

# endif LOCAL_OPTION_FILE
endif
endif


#################################################################
# Compile all CPP files *.cpp -> *.obj
#
$(if $(strip $(CPP_TO_OBJ_BUILD_LIST)),$(error The plugin tricore_tasking does not support cpp/cc/c++ files. -$(CPP_TO_OBJ_BUILD_LIST)- ))
$(CPP_TO_OBJ_BUILD_LIST) :
	$(PRE_BUILD_BLOCK)
	$(SEPARATOR)
	$(SEPARATOR) Compiling cpp-o
	$(SEPARATOR)
	@echo $@ - $(getSourceFile)
	$(POST_BUILD_BLOCK)


#################################################################
# Compile all assembler files *.asm/*.s -> *.obj
#
#################################################################
# Use a local parameter file
#
ifeq ($(COMPILER_MODE), LOCAL_OPTION_FILE)
$(ASM_TO_OBJ_BUILD_LIST) :
	@$(shell echo $(GET_ASM_BUILD_OPTIONS) > $(subst .$(OBJ_FILE_SUFFIX),.ina,$@ ))
	@$(foreach i, $(GET_ASM_INCLUDE_FILES), $(shell echo $i >> $(subst .$(OBJ_FILE_SUFFIX),.ina,$@ )))
	@$(shell echo $(GET_ASS_BUILD_OPTIONS) > $(subst .$(OBJ_FILE_SUFFIX),.incp,$@ ))
	@$(foreach i, $(GET_ASM_INCLUDE_FILES), $(shell echo $i >> $(subst .$(OBJ_FILE_SUFFIX),.incp,$@ )))
	@$(CPRE) -f $(subst .$(OBJ_FILE_SUFFIX),.incp,$@ ) --preprocess=p $(getSourceFile) > $(OBJ_OUTPUT_PATH)\$(basename $(notdir $(subst \,/,$@))).$(ASM_FILE_SUFFIX_2)
	@$(ASM) -f $(subst .$(OBJ_FILE_SUFFIX),.ina,$@ ) -o $@ $(OBJ_OUTPUT_PATH)\$(basename $(notdir $(subst \,/,$@))).$(ASM_FILE_SUFFIX_2)
else
#################################################################
# Use a global parameter file
#
ifeq ($(COMPILER_MODE), GLOBAL_OPTION_FILE)
$(MAKE_INA_FILE):
	@$(shell echo $(GET_ASM_BUILD_OPTIONS) > $(OBJ_OUTPUT_PATH)\$(CPU)_$(TOOLCHAIN).ina )
	@$(foreach i, $(GET_ASM_INCLUDE_FILES), $(shell echo $i >> $(OBJ_OUTPUT_PATH)\$(CPU)_$(TOOLCHAIN).ina ))
	@$(shell echo $(GET_ASS_BUILD_OPTIONS) > $(OBJ_OUTPUT_PATH)\$(CPU)_$(TOOLCHAIN).incp )
	@$(foreach i, $(GET_ASM_INCLUDE_FILES), $(shell echo $i >> $(OBJ_OUTPUT_PATH)\$(CPU)_$(TOOLCHAIN).incp ))

$(ASM_TO_OBJ_BUILD_LIST):
	@$(CPRE) -f $(OBJ_OUTPUT_PATH)\$(CPU)_$(TOOLCHAIN).incp --preprocess=p $(getSourceFile) > $(OBJ_OUTPUT_PATH)\$(basename $(notdir $(subst \,/,$@))).$(ASM_FILE_SUFFIX_2)

	@$(ASM) -f $(OBJ_OUTPUT_PATH)\$(CPU)_$(TOOLCHAIN).ina -o $@ $(OBJ_OUTPUT_PATH)\$(basename $(notdir $(subst \,/,$@))).$(ASM_FILE_SUFFIX_2)
else
#################################################################
# Use a no parameter file
#
$(ASM_TO_OBJ_BUILD_LIST) :
	@$(CPRE) $(GET_ASS_BUILD_OPTIONS) $(GET_ASM_INCLUDE_FILES) --preprocess=p $(getSourceFile) > $(OBJ_OUTPUT_PATH)\$(basename $(notdir $(subst \,/,$@))).$(ASM_FILE_SUFFIX_2)
	@$(ASM)  $(GET_ASM_BUILD_OPTIONS) -o $@ $(OBJ_OUTPUT_PATH)\$(basename $(notdir $(subst \,/,$@))).$(ASM_FILE_SUFFIX_2)
endif
endif

################################################################
# helper to generate a command that archives a few objects into a lib
# note: the 3rd line (empty) is really needed to serve as a cmd-separator.
#
# args: 1 - cmds to perform
define SC_CMD_AR_HELPER
	$(1)

endef

################################################################
# recursive helper to perform a task on chunks of a list
# note: do not place a space in front of argument 5 of the recursive call,
#       ( $(4),<here>$(wordlist... )). This would result in infinite recursion.
#
# args: 1 - variable-name that contains commands to perform
#       2 - additional arguments to $(1)
#       3 - max. number of elements in a chunk
#       4 - $3+1 (sorry, cannot perform arith in make)
#       5 - list
SC_CMD_LISTPERFORM = $(if $(5),\
	$(call $(1),$(2) $(wordlist 1, $(3), $(5))) \
	$(call SC_CMD_LISTPERFORM, $(1), $(2), $(3), \
		$(4),$(wordlist $(4), $(words $(5)), $(5))) )

################################################################
# Archive object files *.obj -> *.lib
#
$(OBJ_TO_LIB_BUILD_LIST) :
	$(PRE_BUILD_BLOCK)
	$(RM) $@
	$(call SC_CMD_LISTPERFORM,SC_CMD_AR_HELPER,\
		$(LIB) $(AR_OPT) $@ $(GET_LIB_BUILD_OPTIONS),4,5, $+)
ifeq ($(BUILD_MODE),LIB)
	$(SEPARATOR)
	$(SEPARATOR) removing object files
	$(SEPARATOR)
	$(call SC_CMD_LISTPERFORM,SC_CMD_AR_HELPER, $(RM) -f, 5, 6, $(CC_TO_OBJ_BUILD_LIST))
	$(SEPARATOR)
endif
	$(POST_BUILD_BLOCK)

################################################################
# helper to echo data to some file
#
# args: 1 - text to echo
#       2 - destination
define SC_CMD_ECHO_HELPER
	@echo $(1) >> $(2)

endef

#################################################################
# Link all objects and libraries *.obj + *.lib -> .out
#
# in LIB mode, the final link step is not performed
#

# With Tasking compiler v4.0r1 we ran into trouble with the linker call order in some tests.
# To avoid uncorrect linking, the following line was changed. Tasking case 00202846.
# Original:
# $(LINK) $(LINK_OPT) -o $@ -f $(BIN_OUTPUT_PATH)\$(PROJECT)_objects.ldscript -d $(GET_LOC_FILE)
# Changed:
# $(LINK) -o $@ -f $(BIN_OUTPUT_PATH)\$(PROJECT)_objects.ldscript -d $(GET_LOC_FILE) $(LINK_OPT)
ifeq ($(BUILD_MODE),LIB)
$(BIN_OUTPUT_PATH)\$(PROJECT).elf : $(GET_FILES_TO_LINK) $(GET_LOC_FILE)
else
$(BIN_OUTPUT_PATH)\$(PROJECT).elf : $(GET_FILES_TO_LINK) $(GET_LOC_FILE)
	$(PRE_BUILD_BLOCK)
	$(SEPARATOR) create linker script
	-$(RM) $(BIN_OUTPUT_PATH)\$(PROJECT)_objects.ldscript
	$(foreach currentObj, $(GET_FILES_TO_LINK), $(call SC_CMD_ECHO_HELPER,$(currentObj),$(BIN_OUTPUT_PATH)\$(PROJECT)_objects.ldscript))
	$(SEPARATOR) create binary
	$(LINK) -o$@:ELF -o$(BIN_OUTPUT_PATH)\$(PROJECT).$(HEX_FILE_SUFFIX):SREC:4 -f $(BIN_OUTPUT_PATH)\$(PROJECT)_objects.ldscript -d $(GET_LOC_FILE) $(LINK_OPT)
	$(POST_BUILD_BLOCK)
endif

#################################################################
# Remove preprocessor files (*.pre )
#
ifeq ($(CREATE_PREPROCESSOR_FILE),YES)
MAKE_CLEAN_LIB_RULES += clean_lib_pre
LIB_PRE_FILES = $(patsubst %.$(OBJ_FILE_SUFFIX),%.$(PRE_FILE_SUFFIX),$(CC_TO_OBJ_BUILD_LIST))

clean_lib_pre:
	$(SEPARATOR) [clean] 	lib preprocessor files
	$(foreach file,$(LIB_PRE_FILES),$(call SC_CMD_AR_HELPER, $(RM) $(file)))
endif

#################################################################
# Delete all compiler specific files (*.obj, *.lib, *.abs...)
#
compiler_clean :
	$(SEPARATOR)
	$(START_BLOCK)
	$(SEPARATOR) DELETING COMPILER FILES
	$(SEPARATOR)
	$(RM) $(BIN_OUTPUT_PATH)\$(PROJECT).elf
	$(RM) $(BIN_OUTPUT_PATH)\$(PROJECT).$(HEX_FILE_SUFFIX)
	$(RM) $(BIN_OUTPUT_PATH)\$(PROJECT)_objects.ldscript
	$(RM) "$(OBJ_OUTPUT_PATH)\*.$(OBJ_FILE_SUFFIX)"
	$(RM) "$(OBJ_OUTPUT_PATH)\*.src"
	$(RM) "$(OBJ_OUTPUT_PATH)\*.$(INC_FILE_SUFFIX)"
	$(RM) "$(OBJ_OUTPUT_PATH)\*.$(INA_FILE_SUFFIX)"
	$(RM) "$(LIB_OUTPUT_PATH)\*.$(LIB_FILE_SUFFIX)"
	$(RM) "$(TMP_OUTPUT_PATH)\*.*"
	$(SEPARATOR)
	$(END_BLOCK)

