# \file
#
# \brief AUTOSAR Platforms
#
# This file contains the implementation of the AUTOSAR
# module Platforms.
#
# \author Elektrobit Automotive GmbH, 91058 Erlangen, Germany
#
# Copyright 2005 - 2013 Elektrobit Automotive GmbH
# All rights exclusively reserved for Elektrobit Automotive GmbH,
# unless expressly agreed to otherwise.

#################################################################
# CONFIGURATION (in tricore_tasking_cfg.mak)
#
# TOOLPATH_COMPILER (required)
# TASKING_MODE      (required)
# CC_OPT            (required)
# AR_OPT            (required)
# LINK_OPT          (required)
# ASM_OPT           (required)
# EXT_LOCATOR_FILE  (optional)

#################################################################
# REGISTRY
#
PREPARE_CONFIGURATION_INTERFACE      += TOOLPATH_COMPILER \
	TOOLPATH_COMPILER \
	CC_OPT \
	AR_OPT \
	LINK_OPT \
	ASM_OPT \
	EXT_LOCATOR_FILE


CHECK_VARS_WHICH_ARE_REQUIRED        += TOOLPATH_COMPILER \
	TOOLPATH_COMPILER \
	CC_OPT \
	AR_OPT \
	LINK_OPT \
	ASM_OPT

CHECK_VARS_WHICH_ARE_OPTIONAL        += EXT_LOCATOR_FILE

#CHECK_VARS_WITH_ONE_CC_FILE            +=
#CHECK_VARS_WITH_MORE_CC_FILES          +=

#CHECK_VARS_WITH_ONE_CPP_FILE         +=
#CHECK_VARS_WITH_MORE_CPP_FILES       +=

#CHECK_VARS_WITH_ONE_ASM_FILE         +=
#CHECK_VARS_WITH_MORE_ASM_FILES       +=

#CHECK_VARS_WITH_ONE_LIB_FILE         +=
#CHECK_VARS_WITH_MORE_LIB_FILES       +=

#CHECK_VARS_WITH_ONE_OBJ_FILE         +=
#CHECK_VARS_WITH_MORE_OBJ_FILES       +=

CHECK_VARS_WITH_ONE_DIRECTORY          += TOOLPATH_COMPILER
#CHECK_VARS_WITH_MORE_DIRECTORIES      +=

#CHECK_VARS_WITH_ONE_FILE              +=
#CHECK_VARS_WITH_MORE_FILES            +=

CHECK_VARS_WITH_MAX_LENGTH_ONE       += TOOLPATH_COMPILER

#################################################################
# SPECIAL CHECKS

