# \file
#
# \brief AUTOSAR Platforms
#
# This file contains the implementation of the AUTOSAR
# module Platforms.
#
# \author Elektrobit Automotive GmbH, 91058 Erlangen, Germany
#
# Copyright 2005 - 2013 Elektrobit Automotive GmbH
# All rights exclusively reserved for Elektrobit Automotive GmbH,
# unless expressly agreed to otherwise.

##################################################################
# CONFIGURATION
#

Platforms_CORE_PATH           := $(SSC_ROOT)\Platforms_$(Platforms_VARIANT)
Platforms_OUTPUT_PATH         := $(AUTOSAR_BASE_OUTPUT_PATH)

#################################################################

CC_INCLUDE_PATH      += \
   $(Platforms_CORE_PATH)\include  \
   $(Platforms_CORE_PATH)\include\$(CPU) \
   $(Platforms_CORE_PATH)\include\$(CPU)\$(TOOLCHAIN) \