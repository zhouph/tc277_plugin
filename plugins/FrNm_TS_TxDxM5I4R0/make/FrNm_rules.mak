# \file
#
# \brief AUTOSAR FrNm
#
# This file contains the implementation of the AUTOSAR
# module FrNm.
#
# \author Elektrobit Automotive GmbH, 91058 Erlangen, Germany
#
# Copyright 2005 - 2013 Elektrobit Automotive GmbH
# All rights exclusively reserved for Elektrobit Automotive GmbH,
# unless expressly agreed to otherwise.

#################################################################
# DEFINITIONS

#################################################################
# REGISTRY

FrNm_src_FILES      := \
    $(FrNm_CORE_PATH)\src\FrNm.c \
    $(FrNm_CORE_PATH)\src\FrNm_Hsm.c \
    $(FrNm_CORE_PATH)\src\FrNm_HsmFrNmData.c \
    $(FrNm_CORE_PATH)\src\FrNm_HsmFrNmFnct.c \
    $(FrNm_OUTPUT_PATH)\src\FrNm_Cfg.c


LIBRARIES_TO_BUILD   += FrNm_src

#################################################################
# DEPENDENCIES (only for assembler files)
#

#################################################################
# RULES
