/**
 * \file
 *
 * \brief AUTOSAR FrNm
 *
 * This file contains the implementation of the AUTOSAR
 * module FrNm.
 *
 * \author Elektrobit Automotive GmbH, 91058 Erlangen, Germany
 *
 * Copyright 2005 - 2013 Elektrobit Automotive GmbH
 * All rights exclusively reserved for Elektrobit Automotive GmbH,
 * unless expressly agreed to otherwise.
 */
#ifndef FRNM_TYPES_H
#define FRNM_TYPES_H


/*==================[inclusions]=============================================*/

#include <ComStack_Types.h>
#include <TSAutosar.h>
#include <FrNm_Cfg.h>

/*==================[macros]================================================*/

/*==================[type definitions]=======================================*/

/**
 * FrNm identifier information for one particular FrNm cluster
 */
typedef struct
{
#if ((FRNM_ENABLE_NMVOTE == STD_ON) || (FRNM_ENABLE_NMMIXED == STD_ON))
  P2CONST(PduIdType, TYPEDEF, FRNM_CONST) TxVotePduId; /**< Array of TX PDU for Nm vote (with or
                                                            without Nm data) for a cluster */
#endif
#if (FRNM_ENABLE_NMDATA == STD_ON)
  P2CONST(PduIdType, TYPEDEF, FRNM_CONST) TxDataPduId; /**< Array of TX PDU for Nm data which
                                                            doesn't contain vote */
#endif
  PduLengthType PduLength;     /**< Length of FrNm data PDU in units of bytes */
  uint8 NodeId;                /**< NM node ID configured for the respective FlexRay cluster[0..255] */
  NetworkHandleType ChannelId; /**< NM-Network ID configured for the respective FlexRay Channel */
  uint8 ChannelProperty;       /**< ScheduleVariant(0-2), Cbv(3), ReptMessageBit(4), SynchEnabled(5),
                                    VoteInhibition(6), Nm Data(7) */
#if ((FRNM_ENABLE_NMVOTE == STD_ON) || (FRNM_ENABLE_NMMIXED == STD_ON))
  uint8 NoOfTxVotePdus;                                /**< Number of vote with or without data TX
                                                            N-PDUs [0..2] */
#endif
#if (FRNM_ENABLE_NMDATA == STD_ON)
  uint8 NoOfTxDataPdus;                                /**< Number of data TX N-PDUs [0..2] */
#endif
  boolean PnEnabled;           /**< FRNM072_Conf */
} FrNm_ChnIdentifierConfigType;

/**
 * FrNm timing information for one particular FrNm cluster
 */
typedef struct
{
  uint16 MsgTimeoutTime;     /**< Timeout for the transmission an FrNm PDU in units of communication
                                  cycles [0..65535] */
  /* !LINKSTO FRNM309,1 */
  uint16 ReadySleepCnt;      /**< Number of repetitions in the ready sleep state before switches
                                  into bus sleep state [1..65535] */
  uint16 RemoteSleepIndTime; /**< Number of communication cycles that have to pass with all
                                  participating ECUs signalling ready to sleep, till an indication
                                  is sent to the upper layer [0..65535] */
  uint16 RepeatMessageTime;  /**< Number of communication cycles the state machine reside in repeat
                                  message state [0..65535] */
  uint8 DataCycle;           /**< Number of FlexRay communication cycles required to transmit the
                                  data PDU[ 1, 2, 4, 8, 16, 32, 64] */
  uint8 RepititionCycle;     /**< Number of FlexRay communication cycles required to change the
                                  vote PDU [ 1, 2, 4, 8, 16, 32, 64] */
  uint8 VotingCycle;         /**< Number of FlexRay communication cycles required to transmit the
                                  vote PDU[ 1, 2, 4, 8, 16, 32, 64] */
} FrNm_ChnTimingConfigType;

/**
 * FrNm configuration for one particular FrNm cluster
 */
typedef struct
{
  P2VAR(uint8, TYPEDEF, AUTOSAR_COMSTACKDATA) RxPduPtr; /**< cluster specific Pdu receive buffer */
  P2VAR(uint8, TYPEDEF, AUTOSAR_COMSTACKDATA) TxPduPtr; /**< cluster specific Pdu transmit buffer */
  FrNm_ChnIdentifierConfigType ChannelIdentifiers;      /**< cluster specific identifiers */
  FrNm_ChnTimingConfigType ChannelTiming;               /**< cluster specific timing information */
} FrNm_ChnConfigType;


/**
 * Info for one FrNm RX PDU (data or vote) - used in array, where PDU Id is
 * the index
 */
typedef struct
{
  uint8 PduType;       /**< Type of PDU (vote or data) */
  uint8 ChannelHandle; /**< Cluster ID for this PDU */
} FrNm_PduType;

/**
 * Info for one FrNm TX PDU. Contains type of pdu, FrIf
 * TxPduId and corresponding cluster index.
 */
typedef struct
{
  PduIdType TxPduId;        /**< PDU ID defined by FrIf */
  PduIdType TxConfPduId;    /**< Tx Confirmation PduId */
  PduLengthType PduLength;  /**< PDU length defined by EcuC */
  uint8 PduType;            /**< Type of PDU (vote or data or mixed) */
  uint8 ChannelHandle;      /**< Cluster ID for this PDU */
} FrNm_TxPduType;

/*==================[external function declarations]=========================*/
/*==================[internal function declarations]=========================*/
/*==================[external constants]=====================================*/
/*==================[internal constants]=====================================*/
/*==================[external data]==========================================*/
/*==================[internal data]==========================================*/
/*==================[external function definitions]==========================*/
/*==================[internal function definitions]==========================*/
#endif /* if !defined( FRNM_TYPES_H ) */
/*==================[end of file]============================================*/
