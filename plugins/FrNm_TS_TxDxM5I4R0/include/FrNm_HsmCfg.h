/**
 * \file
 *
 * \brief AUTOSAR FrNm
 *
 * This file contains the implementation of the AUTOSAR
 * module FrNm.
 *
 * \author Elektrobit Automotive GmbH, 91058 Erlangen, Germany
 *
 * Copyright 2005 - 2013 Elektrobit Automotive GmbH
 * All rights exclusively reserved for Elektrobit Automotive GmbH,
 * unless expressly agreed to otherwise.
 */

#ifndef FRNM_HSMCFG_H
#define FRNM_HSMCFG_H

/* Configuration options for the hsm */

/*==================[inclusions]============================================*/
#include <FrNm_Cfg.h> /* number of state machines */
/*==================[macros]================================================*/

#if (FRNM_NUMBER_OF_CHANNELS > 1U)
/** \brief Enable multiple instances of the same statemachine */
#define FRNM_HSM_INST_MULTI_ENABLED     STD_ON
#else
#define FRNM_HSM_INST_MULTI_ENABLED     STD_OFF
#endif

/** \brief Number of instances of hsm <HSM_NAME>
 *
 * Definition is only needed if FRNM_HSM_INST_MULTI_ENABLED==STD_ON */
#define FRNM_HSM_FRNM_NUM_INST         FRNM_NUMBER_OF_CHANNELS


/*==================[type definitions]======================================*/

/*==================[external function declarations]========================*/

/*==================[internal function declarations]========================*/

/*==================[external constants]====================================*/

/*==================[internal constants]====================================*/

/*==================[external data]=========================================*/

/*==================[internal data]=========================================*/

/*==================[external function definitions]=========================*/

/*==================[internal function definitions]=========================*/

#endif
/*==================[end of file]===========================================*/
