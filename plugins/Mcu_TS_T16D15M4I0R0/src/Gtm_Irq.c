/******************************************************************************
**                                                                           **
** Copyright (C) Infineon Technologies (2013)                                **
**                                                                           **
** All rights reserved.                                                      **
**                                                                           **
** This document contains proprietary information belonging to Infineon      **
** Technologies. Passing on and copying of this document, and communication  **
** of its contents is not permitted without prior written authorization.     **
**                                                                           **
*******************************************************************************
**                                                                           **
**  $FILENAME   : Gtm_Irq.c $                                                **
**                                                                           **
**  $CC VERSION : \main\15 $                                                 **
**                                                                           **
**  $DATE       : 2014-06-25 $                                               **
**                                                                           **
**  AUTHOR      : DL-AUTOSAR-Engineering                                     **
**                                                                           **
**  VENDOR      : Infineon Technologies                                      **
**                                                                           **
**  DESCRIPTION : This file contains the interrupt frames for the GTM. This  **
**                file is given for evaluation purpose only.                 **
**                                                                           **
**  MAY BE CHANGED BY USER [yes/no]: Yes                                     **
**                                                                           **
******************************************************************************/

/*******************************************************************************
**                      Includes                                              **
*******************************************************************************/
/* Include GTM header file */
#include "Gtm.h"

/*Include Irq Module*/
#include "Gtm_Irq.h"

/* Include Mcal.h to import the library functions */ 
#include "Mcal.h"

/*******************************************************************************
**                      Imported Compiler Switch Checks                       **
*******************************************************************************/

/*******************************************************************************
**                      Private Macro Definitions                             **
*******************************************************************************/

/*******************************************************************************
**                      Private Type Definitions                              **
*******************************************************************************/

/*******************************************************************************
**                      Private Function Declarations                         **
*******************************************************************************/

/*******************************************************************************
**                      Global Constant Definitions                           **
*******************************************************************************/

/*******************************************************************************
**                      Global Variable Definitions                           **
*******************************************************************************/

/*******************************************************************************
**                      Private Constant Definitions                          **
*******************************************************************************/

/*******************************************************************************
**                      Private Variable Definitions                          **
*******************************************************************************/

/*******************************************************************************
**                      Private Function Definitions                          **
*******************************************************************************/

/*******************************************************************************
**                      Global Function Definitions                           **
*******************************************************************************/
#define GTM_START_SEC_CODE
#include "MemMap.h"

/******************************************************************************
** Syntax : void OS_ISR_GTM_ISR_<MOD>[x]_SRy(void)                                     **
**                                                                           **
** Service ID:       NA                                                      **
**                                                                           **
** Sync/Async:       Synchronous                                             **
**                                                                           **
** Reentrancy:       reentrant                                               **
**                                                                           **
** Parameters (in):  none                                                    **
**                                                                           **
** Parameters (out): none                                                    **
**                                                                           **
** Return value:     none                                                    **
**                                                                           **
** Description : Service for GTM Service request for Sub Modules             **
**                                                                           **
*****************************************************************************/

/******************** TOM 0 *************************************/
#if(IRQ_GTM_TOM0SR0_EXIST == (STD_ON))
#if((IRQ_GTM_TOM0_SR0_PRIO > 0) || (IRQ_GTM_TOM0_SR0_CAT == IRQ_CAT23))
#if((IRQ_GTM_TOM0_SR0_PRIO > 0) && (IRQ_GTM_TOM0_SR0_CAT == IRQ_CAT1))
void OS_ISR_GTMTOM0SR0_ISR (void)
#elif IRQ_GTM_TOM0_SR0_CAT == IRQ_CAT23
ISR(GTMTOM0SR0_ISR)
#endif
{
  /* Enable Global Interrupts */
  Mcal_EnableAllInterrupts();
  
  /* Parameter is Channel Number */
  Gtm_IsrTomModule(0, 0);
}
#endif
#endif

#if(IRQ_GTM_TOM0SR1_EXIST == (STD_ON))
#if((IRQ_GTM_TOM0_SR1_PRIO > 0) || (IRQ_GTM_TOM0_SR1_CAT == IRQ_CAT23))
#if((IRQ_GTM_TOM0_SR1_PRIO > 0) && (IRQ_GTM_TOM0_SR1_CAT == IRQ_CAT1))
void OS_ISR_GTMTOM0SR1_ISR (void)
#elif IRQ_GTM_TOM0_SR1_CAT == IRQ_CAT23
ISR(GTMTOM0SR1_ISR)
#endif
{
  /* Enable Global Interrupts */
  Mcal_EnableAllInterrupts();
  /* Parameter is Channel Number */
  Gtm_IsrTomModule(0, 2);
}
#endif
#endif

#if(IRQ_GTM_TOM0SR2_EXIST == (STD_ON))
#if((IRQ_GTM_TOM0_SR2_PRIO > 0) || (IRQ_GTM_TOM0_SR2_CAT == IRQ_CAT23))
#if((IRQ_GTM_TOM0_SR2_PRIO > 0) && (IRQ_GTM_TOM0_SR2_CAT == IRQ_CAT1))
void OS_ISR_GTMTOM0SR2_ISR (void)
#elif IRQ_GTM_TOM0_SR2_CAT == IRQ_CAT23
ISR(GTMTOM0SR2_ISR)
#endif
{
  /* Enable Global Interrupts */
  Mcal_EnableAllInterrupts();
  /* Parameter is Channel Number */
  Gtm_IsrTomModule(0, 4);
}
#endif
#endif

#if(IRQ_GTM_TOM0SR3_EXIST == (STD_ON))
#if((IRQ_GTM_TOM0_SR3_PRIO > 0) || (IRQ_GTM_TOM0_SR3_CAT == IRQ_CAT23))
#if((IRQ_GTM_TOM0_SR3_PRIO > 0) && (IRQ_GTM_TOM0_SR3_CAT == IRQ_CAT1))
void OS_ISR_GTMTOM0SR3_ISR (void)
#elif IRQ_GTM_TOM0_SR3_CAT == IRQ_CAT23
ISR(GTMTOM0SR3_ISR)
#endif
{
  /* Enable Global Interrupts */
  Mcal_EnableAllInterrupts();
  /* Parameter is Channel Number */
  Gtm_IsrTomModule(0, 6);
}
#endif
#endif

#if(IRQ_GTM_TOM0SR4_EXIST == (STD_ON))
#if((IRQ_GTM_TOM0_SR4_PRIO > 0) || (IRQ_GTM_TOM0_SR4_CAT == IRQ_CAT23))
#if((IRQ_GTM_TOM0_SR4_PRIO > 0) && (IRQ_GTM_TOM0_SR4_CAT == IRQ_CAT1))
void OS_ISR_GTMTOM0SR4_ISR (void)
#elif IRQ_GTM_TOM0_SR4_CAT == IRQ_CAT23
ISR(GTMTOM0SR4_ISR)
#endif
{
  /* Enable Global Interrupts */
  Mcal_EnableAllInterrupts();
  /* Parameter is Channel Number */
  Gtm_IsrTomModule(0, 8);
}
#endif
#endif

#if(IRQ_GTM_TOM0SR5_EXIST == (STD_ON))
#if((IRQ_GTM_TOM0_SR5_PRIO > 0) || (IRQ_GTM_TOM0_SR5_CAT == IRQ_CAT23))
#if((IRQ_GTM_TOM0_SR5_PRIO > 0) && (IRQ_GTM_TOM0_SR5_CAT == IRQ_CAT1))
void OS_ISR_GTMTOM0SR5_ISR (void)
#elif IRQ_GTM_TOM0_SR5_CAT == IRQ_CAT23
ISR(GTMTOM0SR5_ISR)
#endif
{
  /* Enable Global Interrupts */
  Mcal_EnableAllInterrupts();
  /* Parameter is Channel Number */
  Gtm_IsrTomModule(0, 10);
}
#endif
#endif

#if(IRQ_GTM_TOM0SR6_EXIST == (STD_ON))
#if((IRQ_GTM_TOM0_SR6_PRIO > 0) || (IRQ_GTM_TOM0_SR6_CAT == IRQ_CAT23))
#if((IRQ_GTM_TOM0_SR6_PRIO > 0) && (IRQ_GTM_TOM0_SR6_CAT == IRQ_CAT1))
void OS_ISR_GTMTOM0SR6_ISR (void)
#elif IRQ_GTM_TOM0_SR6_CAT == IRQ_CAT23
ISR(GTMTOM0SR6_ISR)
#endif
{
  /* Enable Global Interrupts */
  Mcal_EnableAllInterrupts();
  /* Parameter is Channel Number */
  Gtm_IsrTomModule(0, 12);
}
#endif
#endif

#if(IRQ_GTM_TOM0SR7_EXIST == (STD_ON))
#if((IRQ_GTM_TOM0_SR7_PRIO > 0) || (IRQ_GTM_TOM0_SR7_CAT == IRQ_CAT23))
#if((IRQ_GTM_TOM0_SR7_PRIO > 0) && (IRQ_GTM_TOM0_SR7_CAT == IRQ_CAT1))
void OS_ISR_GTMTOM0SR7_ISR (void)
#elif IRQ_GTM_TOM0_SR7_CAT == IRQ_CAT23
ISR(GTMTOM0SR7_ISR)
#endif
{
  /* Enable Global Interrupts */
  Mcal_EnableAllInterrupts();
  /* Parameter is Channel Number */
  Gtm_IsrTomModule(0, 14);
}
#endif
#endif

/******************** TOM 1 *************************************/
#if(IRQ_GTM_TOM1SR0_EXIST == (STD_ON))
#if((IRQ_GTM_TOM1_SR0_PRIO > 0) || (IRQ_GTM_TOM1_SR0_CAT == IRQ_CAT23))
#if((IRQ_GTM_TOM1_SR0_PRIO > 0) && (IRQ_GTM_TOM1_SR0_CAT == IRQ_CAT1))
void OS_ISR_GTMTOM1SR0_ISR (void)
#elif IRQ_GTM_TOM1_SR0_CAT == IRQ_CAT23
ISR(GTMTOM1SR0_ISR)
#endif
{
  /* Enable Global Interrupts */
  Mcal_EnableAllInterrupts();
  /* Parameter is Channel Number */
  Gtm_IsrTomModule(1, 0);
}
#endif
#endif

#if(IRQ_GTM_TOM1SR1_EXIST == (STD_ON))
#if((IRQ_GTM_TOM1_SR1_PRIO > 0) || (IRQ_GTM_TOM1_SR1_CAT == IRQ_CAT23))
#if((IRQ_GTM_TOM1_SR1_PRIO > 0) && (IRQ_GTM_TOM1_SR1_CAT == IRQ_CAT1))
void OS_ISR_GTMTOM1SR1_ISR (void)
#elif IRQ_GTM_TOM1_SR1_CAT == IRQ_CAT23
ISR(GTMTOM1SR1_ISR)
#endif
{
  /* Enable Global Interrupts */
  Mcal_EnableAllInterrupts();
  /* Parameter is Channel Number */
  Gtm_IsrTomModule(1, 2);
}
#endif
#endif

#if(IRQ_GTM_TOM1SR2_EXIST == (STD_ON))
#if((IRQ_GTM_TOM1_SR2_PRIO > 0) || (IRQ_GTM_TOM1_SR2_CAT == IRQ_CAT23))
#if((IRQ_GTM_TOM1_SR2_PRIO > 0) && (IRQ_GTM_TOM1_SR2_CAT == IRQ_CAT1))
void OS_ISR_GTMTOM1SR2_ISR (void)
#elif IRQ_GTM_TOM1_SR2_CAT == IRQ_CAT23
ISR(GTMTOM1SR2_ISR)
#endif
{
  /* Enable Global Interrupts */
  Mcal_EnableAllInterrupts();
  /* Parameter is Channel Number */
  Gtm_IsrTomModule(1, 4);
}
#endif
#endif

#if(IRQ_GTM_TOM1SR3_EXIST == (STD_ON))
#if((IRQ_GTM_TOM1_SR3_PRIO > 0) || (IRQ_GTM_TOM1_SR3_CAT == IRQ_CAT23))
#if((IRQ_GTM_TOM1_SR3_PRIO > 0) && (IRQ_GTM_TOM1_SR3_CAT == IRQ_CAT1))
void OS_ISR_GTMTOM1SR3_ISR (void)
#elif IRQ_GTM_TOM1_SR3_CAT == IRQ_CAT23
ISR(GTMTOM1SR3_ISR)
#endif
{
  /* Enable Global Interrupts */
  Mcal_EnableAllInterrupts();
  /* Parameter is Channel Number */
  Gtm_IsrTomModule(1, 6);
}
#endif
#endif

#if(IRQ_GTM_TOM1SR4_EXIST == (STD_ON))
#if((IRQ_GTM_TOM1_SR4_PRIO > 0) || (IRQ_GTM_TOM1_SR4_CAT == IRQ_CAT23))
#if((IRQ_GTM_TOM1_SR4_PRIO > 0) && (IRQ_GTM_TOM1_SR4_CAT == IRQ_CAT1))
void OS_ISR_GTMTOM1SR4_ISR (void)
#elif IRQ_GTM_TOM1_SR4_CAT == IRQ_CAT23
ISR(GTMTOM1SR4_ISR)
#endif
{
  /* Enable Global Interrupts */
  Mcal_EnableAllInterrupts();
  /* Parameter is Channel Number */
  Gtm_IsrTomModule(1, 8);
}
#endif
#endif

#if(IRQ_GTM_TOM1SR5_EXIST == (STD_ON))
#if((IRQ_GTM_TOM1_SR5_PRIO > 0) || (IRQ_GTM_TOM1_SR5_CAT == IRQ_CAT23))
#if((IRQ_GTM_TOM1_SR5_PRIO > 0) && (IRQ_GTM_TOM1_SR5_CAT == IRQ_CAT1))
void OS_ISR_GTMTOM1SR5_ISR (void)
#elif IRQ_GTM_TOM1_SR5_CAT == IRQ_CAT23
ISR(GTMTOM1SR5_ISR)
#endif
{
  /* Enable Global Interrupts */
  Mcal_EnableAllInterrupts();
  /* Parameter is Channel Number */
  Gtm_IsrTomModule(1, 10);
}
#endif
#endif

#if(IRQ_GTM_TOM1SR6_EXIST == (STD_ON))
#if((IRQ_GTM_TOM1_SR6_PRIO > 0) || (IRQ_GTM_TOM1_SR6_CAT == IRQ_CAT23))
#if((IRQ_GTM_TOM1_SR6_PRIO > 0) && (IRQ_GTM_TOM1_SR6_CAT == IRQ_CAT1))
void OS_ISR_GTMTOM1SR6_ISR (void)
#elif IRQ_GTM_TOM1_SR6_CAT == IRQ_CAT23
ISR(GTMTOM1SR6_ISR)
#endif
{
  /* Enable Global Interrupts */
  Mcal_EnableAllInterrupts();
  /* Parameter is Channel Number */
  Gtm_IsrTomModule(1, 12);
}
#endif
#endif

#if(IRQ_GTM_TOM1SR7_EXIST == (STD_ON))
#if((IRQ_GTM_TOM1_SR7_PRIO > 0) || (IRQ_GTM_TOM1_SR7_CAT == IRQ_CAT23))
#if((IRQ_GTM_TOM1_SR7_PRIO > 0) && (IRQ_GTM_TOM1_SR7_CAT == IRQ_CAT1))
void OS_ISR_GTMTOM1SR7_ISR (void)
#elif IRQ_GTM_TOM1_SR7_CAT == IRQ_CAT23
ISR(GTMTOM1SR7_ISR)
#endif
{
  /* Enable Global Interrupts */
  Mcal_EnableAllInterrupts();
  /* Parameter is Channel Number */
  Gtm_IsrTomModule(1, 14);
}
#endif
#endif

/******************** TOM 2 *************************************/
#if(IRQ_GTM_TOM2SR0_EXIST == (STD_ON))
#if((IRQ_GTM_TOM2_SR0_PRIO > 0) || (IRQ_GTM_TOM2_SR0_CAT == IRQ_CAT23))
#if((IRQ_GTM_TOM2_SR0_PRIO > 0) && (IRQ_GTM_TOM2_SR0_CAT == IRQ_CAT1))
void OS_ISR_GTMTOM2SR0_ISR (void)
#elif IRQ_GTM_TOM2_SR0_CAT == IRQ_CAT23
ISR(GTMTOM2SR0_ISR)
#endif
{
  /* Enable Global Interrupts */
  Mcal_EnableAllInterrupts();
  /* Parameter is Channel Number */
  Gtm_IsrTomModule(2, 0);
}
#endif
#endif

#if(IRQ_GTM_TOM2SR1_EXIST == (STD_ON))
#if((IRQ_GTM_TOM2_SR1_PRIO > 0) || (IRQ_GTM_TOM2_SR1_CAT == IRQ_CAT23))
#if((IRQ_GTM_TOM2_SR1_PRIO > 0) && (IRQ_GTM_TOM2_SR1_CAT == IRQ_CAT1))
void OS_ISR_GTMTOM2SR1_ISR (void)
#elif IRQ_GTM_TOM2_SR1_CAT == IRQ_CAT23
ISR(GTMTOM2SR1_ISR)
#endif
{
  /* Enable Global Interrupts */
  Mcal_EnableAllInterrupts();
  /* Parameter is Channel Number */
  Gtm_IsrTomModule(2, 2);
}
#endif
#endif

#if(IRQ_GTM_TOM2SR2_EXIST == (STD_ON))
#if((IRQ_GTM_TOM2_SR2_PRIO > 0) || (IRQ_GTM_TOM2_SR2_CAT == IRQ_CAT23))
#if((IRQ_GTM_TOM2_SR2_PRIO > 0) && (IRQ_GTM_TOM2_SR2_CAT == IRQ_CAT1))
void OS_ISR_GTMTOM2SR2_ISR (void)
#elif IRQ_GTM_TOM2_SR2_CAT == IRQ_CAT23
ISR(GTMTOM2SR2_ISR)
#endif
{
  /* Enable Global Interrupts */
  Mcal_EnableAllInterrupts();
  /* Parameter is Channel Number */
  Gtm_IsrTomModule(2, 4);
}
#endif
#endif

#if(IRQ_GTM_TOM2SR3_EXIST == (STD_ON))
#if((IRQ_GTM_TOM2_SR3_PRIO > 0) || (IRQ_GTM_TOM2_SR3_CAT == IRQ_CAT23))
#if((IRQ_GTM_TOM2_SR3_PRIO > 0) && (IRQ_GTM_TOM2_SR3_CAT == IRQ_CAT1))
void OS_ISR_GTMTOM2SR3_ISR (void)
#elif IRQ_GTM_TOM2_SR3_CAT == IRQ_CAT23
ISR(GTMTOM2SR3_ISR)
#endif
{
  /* Enable Global Interrupts */
  Mcal_EnableAllInterrupts();
  /* Parameter is Channel Number */
  Gtm_IsrTomModule(2, 6);
}
#endif
#endif

#if(IRQ_GTM_TOM2SR4_EXIST == (STD_ON))
#if((IRQ_GTM_TOM2_SR4_PRIO > 0) || (IRQ_GTM_TOM2_SR4_CAT == IRQ_CAT23))
#if((IRQ_GTM_TOM2_SR4_PRIO > 0) && (IRQ_GTM_TOM2_SR4_CAT == IRQ_CAT1))
void OS_ISR_GTMTOM2SR4_ISR (void)
#elif IRQ_GTM_TOM2_SR4_CAT == IRQ_CAT23
ISR(GTMTOM2SR4_ISR)
#endif
{
  /* Enable Global Interrupts */
  Mcal_EnableAllInterrupts();
  /* Parameter is Channel Number */
  Gtm_IsrTomModule(2, 8);
}
#endif
#endif

#if(IRQ_GTM_TOM2SR5_EXIST == (STD_ON))
#if((IRQ_GTM_TOM2_SR5_PRIO > 0) || (IRQ_GTM_TOM2_SR5_CAT == IRQ_CAT23))
#if((IRQ_GTM_TOM2_SR5_PRIO > 0) && (IRQ_GTM_TOM2_SR5_CAT == IRQ_CAT1))
void OS_ISR_GTMTOM2SR5_ISR (void)
#elif IRQ_GTM_TOM2_SR5_CAT == IRQ_CAT23
ISR(GTMTOM2SR5_ISR)
#endif
{
  /* Enable Global Interrupts */
  Mcal_EnableAllInterrupts();
  /* Parameter is Channel Number */
  Gtm_IsrTomModule(2, 10);
}
#endif
#endif

#if(IRQ_GTM_TOM2SR6_EXIST == (STD_ON))
#if((IRQ_GTM_TOM2_SR6_PRIO > 0) || (IRQ_GTM_TOM2_SR6_CAT == IRQ_CAT23))
#if((IRQ_GTM_TOM2_SR6_PRIO > 0) && (IRQ_GTM_TOM2_SR6_CAT == IRQ_CAT1))
void OS_ISR_GTMTOM2SR6_ISR (void)
#elif IRQ_GTM_TOM2_SR6_CAT == IRQ_CAT23
ISR(GTMTOM2SR6_ISR)
#endif
{
  /* Enable Global Interrupts */
  Mcal_EnableAllInterrupts();
  /* Parameter is Channel Number */
  Gtm_IsrTomModule(2, 12);
}
#endif
#endif

#if(IRQ_GTM_TOM2SR7_EXIST == (STD_ON))
#if((IRQ_GTM_TOM2_SR7_PRIO > 0) || (IRQ_GTM_TOM2_SR7_CAT == IRQ_CAT23))
#if((IRQ_GTM_TOM2_SR7_PRIO > 0) && (IRQ_GTM_TOM2_SR7_CAT == IRQ_CAT1))
void OS_ISR_GTMTOM2SR7_ISR (void)
#elif IRQ_GTM_TOM2_SR7_CAT == IRQ_CAT23
ISR(GTMTOM2SR7_ISR)
#endif
{
  /* Enable Global Interrupts */
  Mcal_EnableAllInterrupts();
  /* Parameter is Channel Number */
  Gtm_IsrTomModule(2, 14);
}
#endif
#endif

/******************** TOM 3 *************************************/
#if(IRQ_GTM_TOM3SR0_EXIST == (STD_ON))
#if((IRQ_GTM_TOM3_SR0_PRIO > 0) || (IRQ_GTM_TOM3_SR0_CAT == IRQ_CAT23))
#if((IRQ_GTM_TOM3_SR0_PRIO > 0) && (IRQ_GTM_TOM3_SR0_CAT == IRQ_CAT1))
void OS_ISR_GTMTOM3SR0_ISR (void)
#elif IRQ_GTM_TOM3_SR0_CAT == IRQ_CAT23
ISR(GTMTOM3SR0_ISR)
#endif
{
  /* Enable Global Interrupts */
  Mcal_EnableAllInterrupts();
  /* Parameter is Channel Number */
  Gtm_IsrTomModule(3, 0);
}
#endif
#endif

#if(IRQ_GTM_TOM3SR1_EXIST == (STD_ON))
#if((IRQ_GTM_TOM3_SR1_PRIO > 0) || (IRQ_GTM_TOM3_SR1_CAT == IRQ_CAT23))
#if((IRQ_GTM_TOM3_SR1_PRIO > 0) && (IRQ_GTM_TOM3_SR1_CAT == IRQ_CAT1))
void OS_ISR_GTMTOM3SR1_ISR (void)
#elif IRQ_GTM_TOM3_SR1_CAT == IRQ_CAT23
ISR(GTMTOM3SR1_ISR)
#endif
{
  /* Enable Global Interrupts */
  Mcal_EnableAllInterrupts();
  /* Parameter is Channel Number */
  Gtm_IsrTomModule(3, 2);
}
#endif
#endif

#if(IRQ_GTM_TOM3SR2_EXIST == (STD_ON))
#if((IRQ_GTM_TOM3_SR2_PRIO > 0) || (IRQ_GTM_TOM3_SR2_CAT == IRQ_CAT23))
#if((IRQ_GTM_TOM3_SR2_PRIO > 0) && (IRQ_GTM_TOM3_SR2_CAT == IRQ_CAT1))
void OS_ISR_GTMTOM3SR2_ISR (void)
#elif IRQ_GTM_TOM3_SR2_CAT == IRQ_CAT23
ISR(GTMTOM3SR2_ISR)
#endif
{
  /* Enable Global Interrupts */
  Mcal_EnableAllInterrupts();
  /* Parameter is Channel Number */
  Gtm_IsrTomModule(3, 4);
}
#endif
#endif

#if(IRQ_GTM_TOM3SR3_EXIST == (STD_ON))
#if((IRQ_GTM_TOM3_SR3_PRIO > 0) || (IRQ_GTM_TOM3_SR3_CAT == IRQ_CAT23))
#if((IRQ_GTM_TOM3_SR3_PRIO > 0) && (IRQ_GTM_TOM3_SR3_CAT == IRQ_CAT1))
void OS_ISR_GTMTOM3SR3_ISR (void)
#elif IRQ_GTM_TOM3_SR3_CAT == IRQ_CAT23
ISR(GTMTOM3SR3_ISR)
#endif
{
  /* Enable Global Interrupts */
  Mcal_EnableAllInterrupts();
  /* Parameter is Channel Number */
  Gtm_IsrTomModule(3, 6);
}
#endif
#endif

#if(IRQ_GTM_TOM3SR4_EXIST == (STD_ON))
#if((IRQ_GTM_TOM3_SR4_PRIO > 0) || (IRQ_GTM_TOM3_SR4_CAT == IRQ_CAT23))
#if((IRQ_GTM_TOM3_SR4_PRIO > 0) && (IRQ_GTM_TOM3_SR4_CAT == IRQ_CAT1))
void OS_ISR_GTMTOM3SR4_ISR (void)
#elif IRQ_GTM_TOM3_SR4_CAT == IRQ_CAT23
ISR(GTMTOM3SR4_ISR)
#endif
{
  /* Enable Global Interrupts */
  Mcal_EnableAllInterrupts();
  /* Parameter is Channel Number */
  Gtm_IsrTomModule(3, 8);
}
#endif
#endif

#if(IRQ_GTM_TOM3SR5_EXIST == (STD_ON))
#if((IRQ_GTM_TOM3_SR5_PRIO > 0) || (IRQ_GTM_TOM3_SR5_CAT == IRQ_CAT23))
#if((IRQ_GTM_TOM3_SR5_PRIO > 0) && (IRQ_GTM_TOM3_SR5_CAT == IRQ_CAT1))
void OS_ISR_GTMTOM3SR5_ISR (void)
#elif IRQ_GTM_TOM3_SR5_CAT == IRQ_CAT23
ISR(GTMTOM3SR5_ISR)
#endif
{
  /* Enable Global Interrupts */
  Mcal_EnableAllInterrupts();
  /* Parameter is Channel Number */
  Gtm_IsrTomModule(3, 10);
}
#endif
#endif

#if(IRQ_GTM_TOM3SR6_EXIST == (STD_ON))
#if((IRQ_GTM_TOM3_SR6_PRIO > 0) || (IRQ_GTM_TOM3_SR6_CAT == IRQ_CAT23))
#if((IRQ_GTM_TOM3_SR6_PRIO > 0) && (IRQ_GTM_TOM3_SR6_CAT == IRQ_CAT1))
void OS_ISR_GTMTOM3SR6_ISR (void)
#elif IRQ_GTM_TOM3_SR6_CAT == IRQ_CAT23
ISR(GTMTOM3SR6_ISR)
#endif
{
  /* Enable Global Interrupts */
  Mcal_EnableAllInterrupts();
  /* Parameter is Channel Number */
  Gtm_IsrTomModule(3, 12);
}
#endif
#endif

#if(IRQ_GTM_TOM3SR7_EXIST == (STD_ON))
#if((IRQ_GTM_TOM3_SR7_PRIO > 0) || (IRQ_GTM_TOM3_SR7_CAT == IRQ_CAT23))
#if((IRQ_GTM_TOM3_SR7_PRIO > 0) && (IRQ_GTM_TOM3_SR7_CAT == IRQ_CAT1))
void OS_ISR_GTMTOM3SR7_ISR (void)
#elif IRQ_GTM_TOM3_SR7_CAT == IRQ_CAT23
ISR(GTMTOM3SR7_ISR)
#endif
{
  /* Enable Global Interrupts */
  Mcal_EnableAllInterrupts();
  /* Parameter is Channel Number */
  Gtm_IsrTomModule(3, 14);
}
#endif
#endif

/******************** TOM 4 *************************************/
#if(IRQ_GTM_TOM4SR0_EXIST == (STD_ON))
#if((IRQ_GTM_TOM4_SR0_PRIO > 0) || (IRQ_GTM_TOM4_SR0_CAT == IRQ_CAT23))
#if((IRQ_GTM_TOM4_SR0_PRIO > 0) && (IRQ_GTM_TOM4_SR0_CAT == IRQ_CAT1))
void OS_ISR_GTMTOM4SR0_ISR (void)
#elif IRQ_GTM_TOM4_SR0_CAT == IRQ_CAT23
ISR(GTMTOM4SR0_ISR)
#endif
{
  /* Enable Global Interrupts */
  Mcal_EnableAllInterrupts();
  /* Parameter is Channel Number */
  Gtm_IsrTomModule(4, 0);
}
#endif
#endif

#if(IRQ_GTM_TOM4SR1_EXIST == (STD_ON))
#if((IRQ_GTM_TOM4_SR1_PRIO > 0) || (IRQ_GTM_TOM4_SR1_CAT == IRQ_CAT23))
#if((IRQ_GTM_TOM4_SR1_PRIO > 0) && (IRQ_GTM_TOM4_SR1_CAT == IRQ_CAT1))
void OS_ISR_GTMTOM4SR1_ISR (void)
#elif IRQ_GTM_TOM4_SR1_CAT == IRQ_CAT23
ISR(GTMTOM4SR1_ISR)
#endif
{
  /* Enable Global Interrupts */
  Mcal_EnableAllInterrupts();
  /* Parameter is Channel Number */
  Gtm_IsrTomModule(4, 2);
}
#endif
#endif

#if(IRQ_GTM_TOM4SR2_EXIST == (STD_ON))
#if((IRQ_GTM_TOM4_SR2_PRIO > 0) || (IRQ_GTM_TOM4_SR2_CAT == IRQ_CAT23))
#if((IRQ_GTM_TOM4_SR2_PRIO > 0) && (IRQ_GTM_TOM4_SR2_CAT == IRQ_CAT1))
void OS_ISR_GTMTOM4SR2_ISR (void)
#elif IRQ_GTM_TOM4_SR2_CAT == IRQ_CAT23
ISR(GTMTOM4SR2_ISR)
#endif
{
  /* Enable Global Interrupts */
  Mcal_EnableAllInterrupts();
  /* Parameter is Channel Number */
  Gtm_IsrTomModule(4, 4);
}
#endif
#endif

#if(IRQ_GTM_TOM4SR3_EXIST == (STD_ON))
#if((IRQ_GTM_TOM4_SR3_PRIO > 0) || (IRQ_GTM_TOM4_SR3_CAT == IRQ_CAT23))
#if((IRQ_GTM_TOM4_SR3_PRIO > 0) && (IRQ_GTM_TOM4_SR3_CAT == IRQ_CAT1))
void OS_ISR_GTMTOM4SR3_ISR (void)
#elif IRQ_GTM_TOM4_SR3_CAT == IRQ_CAT23
ISR(GTMTOM4SR3_ISR)
#endif
{
  /* Enable Global Interrupts */
  Mcal_EnableAllInterrupts();
  /* Parameter is Channel Number */
  Gtm_IsrTomModule(4, 6);
}
#endif
#endif

#if(IRQ_GTM_TOM4SR4_EXIST == (STD_ON))
#if((IRQ_GTM_TOM4_SR4_PRIO > 0) || (IRQ_GTM_TOM4_SR4_CAT == IRQ_CAT23))
#if((IRQ_GTM_TOM4_SR4_PRIO > 0) && (IRQ_GTM_TOM4_SR4_CAT == IRQ_CAT1))
void OS_ISR_GTMTOM4SR4_ISR (void)
#elif IRQ_GTM_TOM4_SR4_CAT == IRQ_CAT23
ISR(GTMTOM4SR4_ISR)
#endif
{
  /* Enable Global Interrupts */
  Mcal_EnableAllInterrupts();
  /* Parameter is Channel Number */
  Gtm_IsrTomModule(4, 8);
}
#endif
#endif

#if(IRQ_GTM_TOM4SR5_EXIST == (STD_ON))
#if((IRQ_GTM_TOM4_SR5_PRIO > 0) || (IRQ_GTM_TOM4_SR5_CAT == IRQ_CAT23))
#if((IRQ_GTM_TOM4_SR5_PRIO > 0) && (IRQ_GTM_TOM4_SR5_CAT == IRQ_CAT1))
void OS_ISR_GTMTOM4SR5_ISR (void)
#elif IRQ_GTM_TOM4_SR5_CAT == IRQ_CAT23
ISR(GTMTOM4SR5_ISR)
#endif
{
  /* Enable Global Interrupts */
  Mcal_EnableAllInterrupts();
  /* Parameter is Channel Number */
  Gtm_IsrTomModule(4, 10);
}
#endif
#endif

#if(IRQ_GTM_TOM4SR6_EXIST == (STD_ON))
#if((IRQ_GTM_TOM4_SR6_PRIO > 0) || (IRQ_GTM_TOM4_SR6_CAT == IRQ_CAT23))
#if((IRQ_GTM_TOM4_SR6_PRIO > 0) && (IRQ_GTM_TOM4_SR6_CAT == IRQ_CAT1))
void OS_ISR_GTMTOM4SR6_ISR (void)
#elif IRQ_GTM_TOM4_SR6_CAT == IRQ_CAT23
ISR(GTMTOM4SR6_ISR)
#endif
{
  /* Enable Global Interrupts */
  Mcal_EnableAllInterrupts();
  /* Parameter is Channel Number */
  Gtm_IsrTomModule(4, 12);
}
#endif
#endif

#if(IRQ_GTM_TOM4SR7_EXIST == (STD_ON))
#if((IRQ_GTM_TOM4_SR7_PRIO > 0) || (IRQ_GTM_TOM4_SR7_CAT == IRQ_CAT23))
#if((IRQ_GTM_TOM4_SR7_PRIO > 0) && (IRQ_GTM_TOM4_SR7_CAT == IRQ_CAT1))
void OS_ISR_GTMTOM4SR7_ISR (void)
#elif IRQ_GTM_TOM4_SR7_CAT == IRQ_CAT23
ISR(GTMTOM4SR7_ISR)
#endif
{
  /* Enable Global Interrupts */
  Mcal_EnableAllInterrupts();
  /* Parameter is Channel Number */
  Gtm_IsrTomModule(4, 14);
}
#endif
#endif

/******************** ATOM 0 *************************************/
#if(IRQ_GTM_ATOM0SR0_EXIST == (STD_ON))
#if((IRQ_GTM_ATOM0_SR0_PRIO > 0) || (IRQ_GTM_ATOM0_SR0_CAT == IRQ_CAT23))
#if((IRQ_GTM_ATOM0_SR0_PRIO > 0) && (IRQ_GTM_ATOM0_SR0_CAT == IRQ_CAT1))
void OS_ISR_GTMATOM0SR0_ISR (void)
#elif IRQ_GTM_ATOM0_SR0_CAT == IRQ_CAT23
ISR(GTMATOM0SR0_ISR)
#endif
{
  /* Enable Global Interrupts */
  Mcal_EnableAllInterrupts();
  /* Parameter is Channel Number */
  Gtm_IsrAtomModule(0, 0);
}
#endif
#endif

#if(IRQ_GTM_ATOM0SR1_EXIST == (STD_ON))
#if((IRQ_GTM_ATOM0_SR1_PRIO > 0) || (IRQ_GTM_ATOM0_SR1_CAT == IRQ_CAT23))
#if((IRQ_GTM_ATOM0_SR1_PRIO > 0) && (IRQ_GTM_ATOM0_SR1_CAT == IRQ_CAT1))
void OS_ISR_ GTMATOM0SR1_ISR (void)
#elif IRQ_GTM_ATOM0_SR1_CAT == IRQ_CAT23
ISR(GTMATOM0SR1_ISR)
#endif
{
  /* Enable Global Interrupts */
  Mcal_EnableAllInterrupts();
  /* Parameter is Channel Number */
  Gtm_IsrAtomModule(0, 2);
}
#endif
#endif

#if(IRQ_GTM_ATOM0SR2_EXIST == (STD_ON))
#if((IRQ_GTM_ATOM0_SR2_PRIO > 0) || (IRQ_GTM_ATOM0_SR2_CAT == IRQ_CAT23))
#if((IRQ_GTM_ATOM0_SR2_PRIO > 0) && (IRQ_GTM_ATOM0_SR2_CAT == IRQ_CAT1))
void OS_ISR_GTMATOM0SR2_ISR (void)
#elif IRQ_GTM_ATOM0_SR2_CAT == IRQ_CAT23
ISR(GTMATOM0SR2_ISR)
#endif
{
  /* Enable Global Interrupts */
  Mcal_EnableAllInterrupts();
  /* Parameter is Channel Number */
  Gtm_IsrAtomModule(0, 4);
}
#endif
#endif

#if(IRQ_GTM_ATOM0SR3_EXIST == (STD_ON))
#if((IRQ_GTM_ATOM0_SR3_PRIO > 0) || (IRQ_GTM_ATOM0_SR3_CAT == IRQ_CAT23))
#if((IRQ_GTM_ATOM0_SR3_PRIO > 0) && (IRQ_GTM_ATOM0_SR3_CAT == IRQ_CAT1))
void OS_ISR_GTMATOM0SR3_ISR (void)
#elif IRQ_GTM_ATOM0_SR3_CAT == IRQ_CAT23
ISR(GTMATOM0SR3_ISR)
#endif
{
  /* Enable Global Interrupts */
  Mcal_EnableAllInterrupts();
  /* Parameter is Channel Number */
  Gtm_IsrAtomModule(0, 6);
}
#endif
#endif

/******************** ATOM 1 *************************************/
#if(IRQ_GTM_ATOM1SR0_EXIST == (STD_ON))
#if((IRQ_GTM_ATOM1_SR0_PRIO > 0) || (IRQ_GTM_ATOM1_SR0_CAT == IRQ_CAT23))
#if((IRQ_GTM_ATOM1_SR0_PRIO > 0) && (IRQ_GTM_ATOM1_SR0_CAT == IRQ_CAT1))
void OS_ISR_GTMATOM1SR0_ISR (void)
#elif IRQ_GTM_ATOM1_SR0_CAT == IRQ_CAT23
ISR(GTMATOM1SR0_ISR)
#endif
{
  /* Enable Global Interrupts */
  Mcal_EnableAllInterrupts();
  /* Parameter is Channel Number */
  Gtm_IsrAtomModule(1, 0);
}
#endif
#endif

#if(IRQ_GTM_ATOM1SR1_EXIST == (STD_ON))
#if((IRQ_GTM_ATOM1_SR1_PRIO > 0) || (IRQ_GTM_ATOM1_SR1_CAT == IRQ_CAT23))
#if((IRQ_GTM_ATOM1_SR1_PRIO > 0) && (IRQ_GTM_ATOM1_SR1_CAT == IRQ_CAT1))
void OS_ISR_GTMATOM1SR1_ISR (void)
#elif IRQ_GTM_ATOM1_SR1_CAT == IRQ_CAT23
ISR(GTMATOM1SR1_ISR)
#endif
{
  /* Enable Global Interrupts */
  Mcal_EnableAllInterrupts();
  /* Parameter is Channel Number */
  Gtm_IsrAtomModule(1, 2);
}
#endif
#endif

#if(IRQ_GTM_ATOM1SR2_EXIST == (STD_ON))
#if((IRQ_GTM_ATOM1_SR2_PRIO > 0) || (IRQ_GTM_ATOM1_SR2_CAT == IRQ_CAT23))
#if((IRQ_GTM_ATOM1_SR2_PRIO > 0) && (IRQ_GTM_ATOM1_SR2_CAT == IRQ_CAT1))
void OS_ISR_GTMATOM1SR2_ISR (void)
#elif IRQ_GTM_ATOM1_SR2_CAT == IRQ_CAT23
ISR(GTMATOM1SR2_ISR)
#endif
{
  /* Enable Global Interrupts */
  Mcal_EnableAllInterrupts();
  /* Parameter is Channel Number */
  Gtm_IsrAtomModule(1, 4);
}
#endif
#endif

#if(IRQ_GTM_ATOM1SR3_EXIST == (STD_ON))
#if((IRQ_GTM_ATOM1_SR3_PRIO > 0) || (IRQ_GTM_ATOM1_SR3_CAT == IRQ_CAT23))
#if((IRQ_GTM_ATOM1_SR3_PRIO > 0) && (IRQ_GTM_ATOM1_SR3_CAT == IRQ_CAT1))
void OS_ISR_GTMATOM1SR3_ISR (void)
#elif IRQ_GTM_ATOM1_SR3_CAT == IRQ_CAT23
ISR(GTMATOM1SR3_ISR)
#endif
{
  /* Enable Global Interrupts */
  Mcal_EnableAllInterrupts();
  /* Parameter is Channel Number */
  Gtm_IsrAtomModule(1, 6);
}
#endif
#endif 

#if(IRQ_GTM_ATOM2SR0_EXIST == (STD_ON))
/******************** ATOM 2 *************************************/
#if((IRQ_GTM_ATOM2_SR0_PRIO > 0) || (IRQ_GTM_ATOM2_SR0_CAT == IRQ_CAT23))
#if((IRQ_GTM_ATOM2_SR0_PRIO > 0) && (IRQ_GTM_ATOM2_SR0_CAT == IRQ_CAT1))
void OS_ISR_GTMATOM2SR0_ISR (void)
#elif IRQ_GTM_ATOM2_SR0_CAT == IRQ_CAT23
ISR(GTMATOM2SR0_ISR)
#endif
{
  /* Enable Global Interrupts */
  Mcal_EnableAllInterrupts();
  /* Parameter is Channel Number */
  Gtm_IsrAtomModule(2, 0);
}
#endif
#endif 

#if(IRQ_GTM_ATOM2SR1_EXIST == (STD_ON))
#if((IRQ_GTM_ATOM2_SR1_PRIO > 0) || (IRQ_GTM_ATOM2_SR1_CAT == IRQ_CAT23))
#if((IRQ_GTM_ATOM2_SR1_PRIO > 0) && (IRQ_GTM_ATOM2_SR1_CAT == IRQ_CAT1))
void OS_ISR_GTMATOM2SR1_ISR (void)
#elif IRQ_GTM_ATOM2_SR1_CAT == IRQ_CAT23
ISR(GTMATOM2SR1_ISR)
#endif
{
  /* Enable Global Interrupts */
  Mcal_EnableAllInterrupts();
  /* Parameter is Channel Number */
  Gtm_IsrAtomModule(2, 2);
}
#endif
#endif 

#if(IRQ_GTM_ATOM2SR2_EXIST == (STD_ON))
#if((IRQ_GTM_ATOM2_SR2_PRIO > 0) || (IRQ_GTM_ATOM2_SR2_CAT == IRQ_CAT23))
#if((IRQ_GTM_ATOM2_SR2_PRIO > 0) && (IRQ_GTM_ATOM2_SR2_CAT == IRQ_CAT1))
void OS_ISR_GTMATOM2SR2_ISR (void)
#elif IRQ_GTM_ATOM2_SR2_CAT == IRQ_CAT23
ISR(GTMATOM2SR2_ISR)
#endif
{
  /* Enable Global Interrupts */
  Mcal_EnableAllInterrupts();
  /* Parameter is Channel Number */
  Gtm_IsrAtomModule(2, 4);
}
#endif
#endif 

#if(IRQ_GTM_ATOM2SR3_EXIST == (STD_ON))
#if((IRQ_GTM_ATOM2_SR3_PRIO > 0) || (IRQ_GTM_ATOM2_SR3_CAT == IRQ_CAT23))
#if((IRQ_GTM_ATOM2_SR3_PRIO > 0) && (IRQ_GTM_ATOM2_SR3_CAT == IRQ_CAT1))
void OS_ISR_GTMATOM2SR3_ISR (void)
#elif IRQ_GTM_ATOM2_SR3_CAT == IRQ_CAT23
ISR(GTMATOM2SR3_ISR)
#endif
{
  /* Enable Global Interrupts */
  Mcal_EnableAllInterrupts();
  /* Parameter is Channel Number */
  Gtm_IsrAtomModule(2, 6);
}
#endif
#endif 

/******************** ATOM 3 *************************************/
#if(IRQ_GTM_ATOM3SR0_EXIST == (STD_ON))
#if((IRQ_GTM_ATOM3_SR0_PRIO > 0) || (IRQ_GTM_ATOM3_SR0_CAT == IRQ_CAT23))
#if((IRQ_GTM_ATOM3_SR0_PRIO > 0) && (IRQ_GTM_ATOM3_SR0_CAT == IRQ_CAT1))
void OS_ISR_GTMATOM3SR0_ISR (void)
#elif IRQ_GTM_ATOM3_SR0_CAT == IRQ_CAT23
ISR(GTMATOM3SR0_ISR)
#endif
{
  /* Enable Global Interrupts */
  Mcal_EnableAllInterrupts();
  /* Parameter is Channel Number */
  Gtm_IsrAtomModule(3, 0);
}
#endif
#endif 

#if(IRQ_GTM_ATOM3SR1_EXIST == (STD_ON))
#if((IRQ_GTM_ATOM3_SR1_PRIO > 0) || (IRQ_GTM_ATOM3_SR1_CAT == IRQ_CAT23))
#if((IRQ_GTM_ATOM3_SR1_PRIO > 0) && (IRQ_GTM_ATOM3_SR1_CAT == IRQ_CAT1))
void OS_ISR_GTMATOM3SR1_ISR (void)
#elif IRQ_GTM_ATOM3_SR1_CAT == IRQ_CAT23
ISR(GTMATOM3SR1_ISR)
#endif
{
  /* Enable Global Interrupts */
  Mcal_EnableAllInterrupts();
  /* Parameter is Channel Number */
  Gtm_IsrAtomModule(3, 2);
}
#endif
#endif 

#if(IRQ_GTM_ATOM3SR2_EXIST == (STD_ON))
#if((IRQ_GTM_ATOM3_SR2_PRIO > 0) || (IRQ_GTM_ATOM3_SR2_CAT == IRQ_CAT23))
#if((IRQ_GTM_ATOM3_SR2_PRIO > 0) && (IRQ_GTM_ATOM3_SR2_CAT == IRQ_CAT1))
void OS_ISR_GTMATOM3SR2_ISR (void)
#elif IRQ_GTM_ATOM3_SR2_CAT == IRQ_CAT23
ISR(GTMATOM3SR2_ISR)
#endif
{
  /* Enable Global Interrupts */
  Mcal_EnableAllInterrupts();
  /* Parameter is Channel Number */
  Gtm_IsrAtomModule(3, 4);
}
#endif
#endif 

#if(IRQ_GTM_ATOM3SR3_EXIST == (STD_ON))
#if((IRQ_GTM_ATOM3_SR3_PRIO > 0) || (IRQ_GTM_ATOM3_SR3_CAT == IRQ_CAT23))
#if((IRQ_GTM_ATOM3_SR3_PRIO > 0) && (IRQ_GTM_ATOM3_SR3_CAT == IRQ_CAT1))
void OS_ISR_GTMATOM3SR3_ISR (void)
#elif IRQ_GTM_ATOM3_SR3_CAT == IRQ_CAT23
ISR(GTMATOM3SR3_ISR)
#endif
{
  /* Enable Global Interrupts */
  Mcal_EnableAllInterrupts();
  /* Parameter is Channel Number */
  Gtm_IsrAtomModule(3, 6);
}
#endif
#endif

/******************** ATOM 4 *************************************/
#if(IRQ_GTM_ATOM4SR0_EXIST == (STD_ON))
#if((IRQ_GTM_ATOM4_SR0_PRIO > 0) || (IRQ_GTM_ATOM4_SR0_CAT == IRQ_CAT23))
#if((IRQ_GTM_ATOM4_SR0_PRIO > 0) && (IRQ_GTM_ATOM4_SR0_CAT == IRQ_CAT1))
void OS_ISR_GTMATOM4SR0_ISR (void)
#elif IRQ_GTM_ATOM4_SR0_CAT == IRQ_CAT23
ISR(GTMATOM4SR0_ISR)
#endif
{
  /* Enable Global Interrupts */
  Mcal_EnableAllInterrupts();
  /* Parameter is Channel Number */
  Gtm_IsrAtomModule(4, 0);
}
#endif
#endif

#if(IRQ_GTM_ATOM4SR1_EXIST == (STD_ON))
#if((IRQ_GTM_ATOM4_SR1_PRIO > 0) || (IRQ_GTM_ATOM4_SR1_CAT == IRQ_CAT23))
#if((IRQ_GTM_ATOM4_SR1_PRIO > 0) && (IRQ_GTM_ATOM4_SR1_CAT == IRQ_CAT1))
void OS_ISR_GTMATOM4SR1_ISR (void)
#elif IRQ_GTM_ATOM4_SR1_CAT == IRQ_CAT23
ISR(GTMATOM4SR1_ISR)
#endif
{
  /* Enable Global Interrupts */
  Mcal_EnableAllInterrupts();
  /* Parameter is Channel Number */
  Gtm_IsrAtomModule(4, 2);
}
#endif
#endif

#if(IRQ_GTM_ATOM4SR2_EXIST == (STD_ON))
#if((IRQ_GTM_ATOM4_SR2_PRIO > 0) || (IRQ_GTM_ATOM4_SR2_CAT == IRQ_CAT23))
#if((IRQ_GTM_ATOM4_SR2_PRIO > 0) && (IRQ_GTM_ATOM4_SR2_CAT == IRQ_CAT1))
void OS_ISR_GTMATOM4SR2_ISR (void)
#elif IRQ_GTM_ATOM4_SR2_CAT == IRQ_CAT23
ISR(GTMATOM4SR2_ISR)
#endif
{
  /* Enable Global Interrupts */
  Mcal_EnableAllInterrupts();
  /* Parameter is Channel Number */
  Gtm_IsrAtomModule(4, 4);
}
#endif
#endif

#if(IRQ_GTM_ATOM4SR3_EXIST == (STD_ON))
#if((IRQ_GTM_ATOM4_SR3_PRIO > 0) || (IRQ_GTM_ATOM4_SR3_CAT == IRQ_CAT23))
#if((IRQ_GTM_ATOM4_SR3_PRIO > 0) && (IRQ_GTM_ATOM4_SR3_CAT == IRQ_CAT1))
void OS_ISR_GTMATOM4SR3_ISR (void)
#elif IRQ_GTM_ATOM4_SR3_CAT == IRQ_CAT23
ISR(GTMATOM4SR3_ISR)
#endif
{
  /* Enable Global Interrupts */
  Mcal_EnableAllInterrupts();
  /* Parameter is Channel Number */
  Gtm_IsrAtomModule(4, 6);
}
#endif
#endif

/******************** ATOM 5 *************************************/
#if(IRQ_GTM_ATOM5SR0_EXIST == (STD_ON))
#if((IRQ_GTM_ATOM5_SR0_PRIO > 0) || (IRQ_GTM_ATOM5_SR0_CAT == IRQ_CAT23))
#if((IRQ_GTM_ATOM5_SR0_PRIO > 0) && (IRQ_GTM_ATOM5_SR0_CAT == IRQ_CAT1))
void OS_ISR_GTMATOM5SR0_ISR (void)
#elif IRQ_GTM_ATOM5_SR0_CAT == IRQ_CAT23
ISR(GTMATOM5SR0_ISR)
#endif
{
  /* Enable Global Interrupts */
  Mcal_EnableAllInterrupts();
  /* Parameter is Channel Number */
  Gtm_IsrAtomModule(5, 0);
}
#endif
#endif

#if(IRQ_GTM_ATOM5SR1_EXIST == (STD_ON))
#if((IRQ_GTM_ATOM5_SR1_PRIO > 0) || (IRQ_GTM_ATOM5_SR1_CAT == IRQ_CAT23))
#if((IRQ_GTM_ATOM5_SR1_PRIO > 0) && (IRQ_GTM_ATOM5_SR1_CAT == IRQ_CAT1))
void OS_ISR_GTMATOM5SR1_ISR (void)
#elif IRQ_GTM_ATOM5_SR1_CAT == IRQ_CAT23
ISR(GTMATOM5SR1_ISR)
#endif
{
  /* Enable Global Interrupts */
  Mcal_EnableAllInterrupts();
  /* Parameter is Channel Number */
  Gtm_IsrAtomModule(5, 2);
}
#endif
#endif

#if(IRQ_GTM_ATOM5SR2_EXIST == (STD_ON))
#if((IRQ_GTM_ATOM5_SR2_PRIO > 0) || (IRQ_GTM_ATOM5_SR2_CAT == IRQ_CAT23))
#if((IRQ_GTM_ATOM5_SR2_PRIO > 0) && (IRQ_GTM_ATOM5_SR2_CAT == IRQ_CAT1))
void OS_ISR_GTMATOM5SR2_ISR (void)
#elif IRQ_GTM_ATOM5_SR2_CAT == IRQ_CAT23
ISR(GTMATOM5SR2_ISR)
#endif
{
  /* Enable Global Interrupts */
  Mcal_EnableAllInterrupts();
  /* Parameter is Channel Number */
  Gtm_IsrAtomModule(5, 4);
}
#endif
#endif

#if(IRQ_GTM_ATOM5SR3_EXIST == (STD_ON))
#if((IRQ_GTM_ATOM5_SR3_PRIO > 0) || (IRQ_GTM_ATOM5_SR3_CAT == IRQ_CAT23))
#if((IRQ_GTM_ATOM5_SR3_PRIO > 0) && (IRQ_GTM_ATOM5_SR3_CAT == IRQ_CAT1))
void OS_ISR_GTMATOM5SR3_ISR (void)
#elif IRQ_GTM_ATOM5_SR3_CAT == IRQ_CAT23
ISR(GTMATOM5SR3_ISR)
#endif
{
  /* Enable Global Interrupts */
  Mcal_EnableAllInterrupts();
  /* Parameter is Channel Number */
  Gtm_IsrAtomModule(5, 6);
}
#endif
#endif

/******************** ATOM 6 *************************************/
#if(IRQ_GTM_ATOM6SR0_EXIST == (STD_ON))
#if((IRQ_GTM_ATOM6_SR0_PRIO > 0) || (IRQ_GTM_ATOM6_SR0_CAT == IRQ_CAT23))
#if((IRQ_GTM_ATOM6_SR0_PRIO > 0) && (IRQ_GTM_ATOM6_SR0_CAT == IRQ_CAT1))
void OS_ISR_GTMATOM6SR0_ISR (void)
#elif IRQ_GTM_ATOM6_SR0_CAT == IRQ_CAT23
ISR(GTMATOM6SR0_ISR)
#endif
{
  /* Enable Global Interrupts */
  Mcal_EnableAllInterrupts();
  /* Parameter is Channel Number */
  Gtm_IsrAtomModule(6, 0);
}
#endif
#endif

#if(IRQ_GTM_ATOM6SR1_EXIST == (STD_ON))
#if((IRQ_GTM_ATOM6_SR1_PRIO > 0) || (IRQ_GTM_ATOM6_SR1_CAT == IRQ_CAT23))
#if((IRQ_GTM_ATOM6_SR1_PRIO > 0) && (IRQ_GTM_ATOM6_SR1_CAT == IRQ_CAT1))
void OS_ISR_GTMATOM6SR1_ISR (void)
#elif IRQ_GTM_ATOM6_SR1_CAT == IRQ_CAT23
ISR(GTMATOM6SR1_ISR)
#endif
{
  /* Enable Global Interrupts */
  Mcal_EnableAllInterrupts();
  /* Parameter is Channel Number */
  Gtm_IsrAtomModule(6, 2);
}
#endif
#endif

#if(IRQ_GTM_ATOM6SR2_EXIST == (STD_ON))
#if((IRQ_GTM_ATOM6_SR2_PRIO > 0) || (IRQ_GTM_ATOM6_SR2_CAT == IRQ_CAT23))
#if((IRQ_GTM_ATOM6_SR2_PRIO > 0) && (IRQ_GTM_ATOM6_SR2_CAT == IRQ_CAT1))
void OS_ISR_GTMATOM6SR2_ISR (void)
#elif IRQ_GTM_ATOM6_SR2_CAT == IRQ_CAT23
ISR(GTMATOM6SR2_ISR)
#endif
{
  /* Enable Global Interrupts */
  Mcal_EnableAllInterrupts();
  /* Parameter is Channel Number */
  Gtm_IsrAtomModule(6, 4);
}
#endif
#endif

#if(IRQ_GTM_ATOM6SR3_EXIST == (STD_ON))
#if((IRQ_GTM_ATOM6_SR3_PRIO > 0) || (IRQ_GTM_ATOM6_SR3_CAT == IRQ_CAT23))
#if((IRQ_GTM_ATOM6_SR3_PRIO > 0) && (IRQ_GTM_ATOM6_SR3_CAT == IRQ_CAT1))
void OS_ISR_GTMATOM6SR3_ISR (void)
#elif IRQ_GTM_ATOM6_SR3_CAT == IRQ_CAT23
ISR(GTMATOM6SR3_ISR)
#endif
{
  /* Enable Global Interrupts */
  Mcal_EnableAllInterrupts();
  /* Parameter is Channel Number */
  Gtm_IsrAtomModule(6, 6);
}
#endif
#endif 

/******************** ATOM 7 *************************************/
#if(IRQ_GTM_ATOM7SR0_EXIST == (STD_ON))
#if((IRQ_GTM_ATOM7_SR0_PRIO > 0) || (IRQ_GTM_ATOM7_SR0_CAT == IRQ_CAT23))
#if((IRQ_GTM_ATOM7_SR0_PRIO > 0) && (IRQ_GTM_ATOM7_SR0_CAT == IRQ_CAT1))
void OS_ISR_GTMATOM7SR0_ISR (void)
#elif IRQ_GTM_ATOM7_SR0_CAT == IRQ_CAT23
ISR(GTMATOM7SR0_ISR)
#endif
{
  /* Enable Global Interrupts */
  Mcal_EnableAllInterrupts();
  /* Parameter is Channel Number */
  Gtm_IsrAtomModule(7, 0);
}
#endif
#endif 

#if(IRQ_GTM_ATOM7SR1_EXIST == (STD_ON))
#if((IRQ_GTM_ATOM7_SR1_PRIO > 0) || (IRQ_GTM_ATOM7_SR1_CAT == IRQ_CAT23))
#if((IRQ_GTM_ATOM7_SR1_PRIO > 0) && (IRQ_GTM_ATOM7_SR1_CAT == IRQ_CAT1))
void OS_ISR_GTMATOM7SR1_ISR (void)
#elif IRQ_GTM_ATOM7_SR1_CAT == IRQ_CAT23
ISR(GTMATOM7SR1_ISR)
#endif
{
  /* Enable Global Interrupts */
  Mcal_EnableAllInterrupts();
  /* Parameter is Channel Number */
  Gtm_IsrAtomModule(7, 2);
}
#endif
#endif 

#if(IRQ_GTM_ATOM7SR2_EXIST == (STD_ON))
#if((IRQ_GTM_ATOM7_SR2_PRIO > 0) || (IRQ_GTM_ATOM7_SR2_CAT == IRQ_CAT23))
#if((IRQ_GTM_ATOM7_SR2_PRIO > 0) && (IRQ_GTM_ATOM7_SR2_CAT == IRQ_CAT1))
void OS_ISR_GTMATOM7SR2_ISR (void)
#elif IRQ_GTM_ATOM7_SR2_CAT == IRQ_CAT23
ISR(GTMATOM7SR2_ISR)
#endif
{
  /* Enable Global Interrupts */
  Mcal_EnableAllInterrupts();
  /* Parameter is Channel Number */
  Gtm_IsrAtomModule(7, 4);
}
#endif
#endif 

#if(IRQ_GTM_ATOM7SR3_EXIST == (STD_ON))
#if((IRQ_GTM_ATOM7_SR3_PRIO > 0) || (IRQ_GTM_ATOM7_SR3_CAT == IRQ_CAT23))
#if((IRQ_GTM_ATOM7_SR3_PRIO > 0) && (IRQ_GTM_ATOM7_SR3_CAT == IRQ_CAT1))
void OS_ISR_GTMATOM7SR3_ISR (void)
#elif IRQ_GTM_ATOM7_SR3_CAT == IRQ_CAT23
ISR(GTMATOM7SR3_ISR)
#endif
{
  /* Enable Global Interrupts */
  Mcal_EnableAllInterrupts();
  /* Parameter is Channel Number */
  Gtm_IsrAtomModule(7, 6);
}
#endif
#endif

/******************** ATOM 8 *************************************/
#if(IRQ_GTM_ATOM8SR0_EXIST == (STD_ON))
#if((IRQ_GTM_ATOM8_SR0_PRIO > 0) || (IRQ_GTM_ATOM8_SR0_CAT == IRQ_CAT23))
#if((IRQ_GTM_ATOM8_SR0_PRIO > 0) && (IRQ_GTM_ATOM8_SR0_CAT == IRQ_CAT1))
void OS_ISR_GTMATOM8SR0_ISR (void)
#elif IRQ_GTM_ATOM8_SR0_CAT == IRQ_CAT23
ISR(GTMATOM8SR0_ISR)
#endif
{
  /* Enable Global Interrupts */
  Mcal_EnableAllInterrupts();
  /* Parameter is Channel Number */
  Gtm_IsrAtomModule(8, 0);
}
#endif
#endif

#if(IRQ_GTM_ATOM8SR1_EXIST == (STD_ON))
#if((IRQ_GTM_ATOM8_SR1_PRIO > 0) || (IRQ_GTM_ATOM8_SR1_CAT == IRQ_CAT23))
#if((IRQ_GTM_ATOM8_SR1_PRIO > 0) && (IRQ_GTM_ATOM8_SR1_CAT == IRQ_CAT1))
void OS_ISR_GTMATOM8SR1_ISR (void)
#elif IRQ_GTM_ATOM8_SR1_CAT == IRQ_CAT23
ISR(GTMATOM8SR1_ISR)
#endif
{
  /* Enable Global Interrupts */
  Mcal_EnableAllInterrupts();
  /* Parameter is Channel Number */
  Gtm_IsrAtomModule(8, 2);
}
#endif
#endif

#if(IRQ_GTM_ATOM8SR2_EXIST == (STD_ON))
#if((IRQ_GTM_ATOM8_SR2_PRIO > 0) || (IRQ_GTM_ATOM8_SR2_CAT == IRQ_CAT23))
#if((IRQ_GTM_ATOM8_SR2_PRIO > 0) && (IRQ_GTM_ATOM8_SR2_CAT == IRQ_CAT1))
void OS_ISR_GTMATOM8SR2_ISR (void)
#elif IRQ_GTM_ATOM8_SR2_CAT == IRQ_CAT23
ISR(GTMATOM8SR2_ISR)
#endif
{
  /* Enable Global Interrupts */
  Mcal_EnableAllInterrupts();
  /* Parameter is Channel Number */
  Gtm_IsrAtomModule(8, 4);
}
#endif
#endif

#if(IRQ_GTM_ATOM8SR3_EXIST == (STD_ON))
#if((IRQ_GTM_ATOM8_SR3_PRIO > 0) || (IRQ_GTM_ATOM8_SR3_CAT == IRQ_CAT23))
#if((IRQ_GTM_ATOM8_SR3_PRIO > 0) && (IRQ_GTM_ATOM8_SR3_CAT == IRQ_CAT1))
void OS_ISR_GTMATOM8SR3_ISR (void)
#elif IRQ_GTM_ATOM8_SR3_CAT == IRQ_CAT23
ISR(GTMATOM8SR3_ISR)
#endif
{
  /* Enable Global Interrupts */
  Mcal_EnableAllInterrupts();
  /* Parameter is Channel Number */
  Gtm_IsrAtomModule(8, 6);
}
#endif
#endif

/******************** TIM 0 *************************************/
#if(IRQ_GTM_TIM0SR0_EXIST == (STD_ON))
#if((IRQ_GTM_TIM0_SR0_PRIO > 0) || (IRQ_GTM_TIM0_SR0_CAT == IRQ_CAT23))
#if((IRQ_GTM_TIM0_SR0_PRIO > 0) && (IRQ_GTM_TIM0_SR0_CAT == IRQ_CAT1))
void OS_ISR_ GTMTIM0SR0_ISR (void)
#elif IRQ_GTM_TIM0_SR0_CAT == IRQ_CAT23
ISR(GTMTIM0SR0_ISR)
#endif
{
  /* Enable Global Interrupts */
  Mcal_EnableAllInterrupts();
  /* Parameter is Channel Number */
  Gtm_IsrTimModule(0, 0);
}
#endif
#endif

#if(IRQ_GTM_TIM0SR1_EXIST == (STD_ON))
#if((IRQ_GTM_TIM0_SR1_PRIO > 0) || (IRQ_GTM_TIM0_SR1_CAT == IRQ_CAT23))
#if((IRQ_GTM_TIM0_SR1_PRIO > 0) && (IRQ_GTM_TIM0_SR1_CAT == IRQ_CAT1))
void OS_ISR_GTMTIM0SR1_ISR (void)
#elif IRQ_GTM_TIM0_SR1_CAT == IRQ_CAT23
ISR(GTMTIM0SR1_ISR)
#endif
{
  /* Enable Global Interrupts */
  Mcal_EnableAllInterrupts();
  /* Parameter is Channel Number */
  Gtm_IsrTimModule(0, 1);
}
#endif
#endif

#if(IRQ_GTM_TIM0SR2_EXIST == (STD_ON))
#if((IRQ_GTM_TIM0_SR2_PRIO > 0) || (IRQ_GTM_TIM0_SR2_CAT == IRQ_CAT23))
#if((IRQ_GTM_TIM0_SR2_PRIO > 0) && (IRQ_GTM_TIM0_SR2_CAT == IRQ_CAT1))
void OS_ISR_GTMTIM0SR2_ISR (void)
#elif IRQ_GTM_TIM0_SR2_CAT == IRQ_CAT23
ISR(GTMTIM0SR2_ISR)
#endif
{
  /* Enable Global Interrupts */
  Mcal_EnableAllInterrupts();
  /* Parameter is Channel Number */
  Gtm_IsrTimModule(0, 2);
}
#endif
#endif

#if(IRQ_GTM_TIM0SR3_EXIST == (STD_ON))
#if((IRQ_GTM_TIM0_SR3_PRIO > 0) || (IRQ_GTM_TIM0_SR3_CAT == IRQ_CAT23))
#if((IRQ_GTM_TIM0_SR3_PRIO > 0) && (IRQ_GTM_TIM0_SR3_CAT == IRQ_CAT1))
void OS_ISR_GTMTIM0SR3_ISR (void)
#elif IRQ_GTM_TIM0_SR3_CAT == IRQ_CAT23
ISR(GTMTIM0SR3_ISR)
#endif
{
  /* Enable Global Interrupts */
  Mcal_EnableAllInterrupts();
  /* Parameter is Channel Number */
  Gtm_IsrTimModule(0, 3);
}
#endif
#endif

#if(IRQ_GTM_TIM0SR4_EXIST == (STD_ON))
#if((IRQ_GTM_TIM0_SR4_PRIO > 0) || (IRQ_GTM_TIM0_SR4_CAT == IRQ_CAT23))
#if((IRQ_GTM_TIM0_SR4_PRIO > 0) && (IRQ_GTM_TIM0_SR4_CAT == IRQ_CAT1))
void OS_ISR_GTMTIM0SR4_ISR (void)
#elif IRQ_GTM_TIM0_SR4_CAT == IRQ_CAT23
ISR(GTMTIM0SR4_ISR)
#endif
{
  /* Enable Global Interrupts */
  Mcal_EnableAllInterrupts();
  /* Parameter is Channel Number */
  Gtm_IsrTimModule(0, 4);
}
#endif
#endif

#if(IRQ_GTM_TIM0SR5_EXIST == (STD_ON))
#if((IRQ_GTM_TIM0_SR5_PRIO > 0) || (IRQ_GTM_TIM0_SR5_CAT == IRQ_CAT23))
#if((IRQ_GTM_TIM0_SR5_PRIO > 0) && (IRQ_GTM_TIM0_SR5_CAT == IRQ_CAT1))
void OS_ISR_GTMTIM0SR5_ISR (void)
#elif IRQ_GTM_TIM0_SR5_CAT == IRQ_CAT23
ISR(GTMTIM0SR5_ISR)
#endif
{
  /* Enable Global Interrupts */
  Mcal_EnableAllInterrupts();
  /* Parameter is Channel Number */
  Gtm_IsrTimModule(0, 5);
}
#endif
#endif

#if(IRQ_GTM_TIM0SR6_EXIST == (STD_ON))
#if((IRQ_GTM_TIM0_SR6_PRIO > 0) || (IRQ_GTM_TIM0_SR6_CAT == IRQ_CAT23))
#if((IRQ_GTM_TIM0_SR6_PRIO > 0) && (IRQ_GTM_TIM0_SR6_CAT == IRQ_CAT1))
void OS_ISR_GTMTIM0SR6_ISR (void)
#elif IRQ_GTM_TIM0_SR6_CAT == IRQ_CAT23
ISR(GTMTIM0SR6_ISR)
#endif
{
  /* Enable Global Interrupts */
  Mcal_EnableAllInterrupts();
  /* Parameter is Channel Number */
  Gtm_IsrTimModule(0, 6);
}
#endif
#endif

#if(IRQ_GTM_TIM0SR7_EXIST == (STD_ON))
#if((IRQ_GTM_TIM0_SR7_PRIO > 0) || (IRQ_GTM_TIM0_SR7_CAT == IRQ_CAT23))
#if((IRQ_GTM_TIM0_SR7_PRIO > 0) && (IRQ_GTM_TIM0_SR7_CAT == IRQ_CAT1))
void OS_ISR_GTMTIM0SR7_ISR (void)
#elif IRQ_GTM_TIM0_SR7_CAT == IRQ_CAT23
ISR(GTMTIM0SR7_ISR)
#endif
{
  /* Enable Global Interrupts */
  Mcal_EnableAllInterrupts();
  /* Parameter is Channel Number */
  Gtm_IsrTimModule(0, 7);
}
#endif
#endif 

/******************** TIM 1 *************************************/
#if(IRQ_GTM_TIM1SR0_EXIST == (STD_ON))
#if((IRQ_GTM_TIM1_SR0_PRIO > 0) || (IRQ_GTM_TIM1_SR0_CAT == IRQ_CAT23))
#if((IRQ_GTM_TIM1_SR0_PRIO > 0) && (IRQ_GTM_TIM1_SR0_CAT == IRQ_CAT1))
void OS_ISR_GTMTIM1SR0_ISR (void)
#elif IRQ_GTM_TIM1_SR0_CAT == IRQ_CAT23
ISR(GTMTIM1SR0_ISR)
#endif
{
  /* Enable Global Interrupts */
  Mcal_EnableAllInterrupts();
  /* Parameter is Channel Number */
  Gtm_IsrTimModule(1, 0);
}
#endif
#endif 

#if(IRQ_GTM_TIM1SR1_EXIST == (STD_ON))
#if((IRQ_GTM_TIM1_SR1_PRIO > 0) || (IRQ_GTM_TIM1_SR1_CAT == IRQ_CAT23))
#if((IRQ_GTM_TIM1_SR1_PRIO > 0) && (IRQ_GTM_TIM1_SR1_CAT == IRQ_CAT1))
void OS_ISR_GTMTIM1SR1_ISR (void)
#elif IRQ_GTM_TIM1_SR1_CAT == IRQ_CAT23
ISR(GTMTIM1SR1_ISR)
#endif
{
  /* Enable Global Interrupts */
  Mcal_EnableAllInterrupts();
  /* Parameter is Channel Number */
  Gtm_IsrTimModule(1, 1);
}
#endif
#endif 

#if(IRQ_GTM_TIM1SR2_EXIST == (STD_ON))
#if((IRQ_GTM_TIM1_SR2_PRIO > 0) || (IRQ_GTM_TIM1_SR2_CAT == IRQ_CAT23))
#if((IRQ_GTM_TIM1_SR2_PRIO > 0) && (IRQ_GTM_TIM1_SR2_CAT == IRQ_CAT1))
void OS_ISR_GTMTIM1SR2_ISR (void)
#elif IRQ_GTM_TIM1_SR2_CAT == IRQ_CAT23
ISR(GTMTIM1SR2_ISR)
#endif
{
  /* Enable Global Interrupts */
  Mcal_EnableAllInterrupts();
  /* Parameter is Channel Number */
  Gtm_IsrTimModule(1, 2);
}
#endif
#endif 

#if(IRQ_GTM_TIM1SR3_EXIST == (STD_ON))
#if((IRQ_GTM_TIM1_SR3_PRIO > 0) || (IRQ_GTM_TIM1_SR3_CAT == IRQ_CAT23))
#if((IRQ_GTM_TIM1_SR3_PRIO > 0) && (IRQ_GTM_TIM1_SR3_CAT == IRQ_CAT1))
void OS_ISR_GTMTIM1SR3_ISR (void)
#elif IRQ_GTM_TIM1_SR3_CAT == IRQ_CAT23
ISR(GTMTIM1SR3_ISR)
#endif
{
  /* Enable Global Interrupts */
  Mcal_EnableAllInterrupts();
  /* Parameter is Channel Number */
  Gtm_IsrTimModule(1, 3);
}
#endif
#endif 

#if(IRQ_GTM_TIM1SR4_EXIST == (STD_ON))
#if((IRQ_GTM_TIM1_SR4_PRIO > 0) || (IRQ_GTM_TIM1_SR4_CAT == IRQ_CAT23))
#if((IRQ_GTM_TIM1_SR4_PRIO > 0) && (IRQ_GTM_TIM1_SR4_CAT == IRQ_CAT1))
void OS_ISR_GTMTIM1SR4_ISR (void)
#elif IRQ_GTM_TIM1_SR4_CAT == IRQ_CAT23
ISR(GTMTIM1SR4_ISR)
#endif
{
  /* Enable Global Interrupts */
  Mcal_EnableAllInterrupts();
  /* Parameter is Channel Number */
  Gtm_IsrTimModule(1, 4);
}
#endif

#endif 

#if(IRQ_GTM_TIM1SR5_EXIST == (STD_ON))
#if((IRQ_GTM_TIM1_SR5_PRIO > 0) || (IRQ_GTM_TIM1_SR5_CAT == IRQ_CAT23))
#if((IRQ_GTM_TIM1_SR5_PRIO > 0) && (IRQ_GTM_TIM1_SR5_CAT == IRQ_CAT1))
void OS_ISR_GTMTIM1SR5_ISR (void)
#elif IRQ_GTM_TIM1_SR5_CAT == IRQ_CAT23
ISR(GTMTIM1SR5_ISR)
#endif
{
  /* Enable Global Interrupts */
  Mcal_EnableAllInterrupts();
  /* Parameter is Channel Number */
  Gtm_IsrTimModule(1, 5);
}
#endif
#endif 

#if(IRQ_GTM_TIM1SR6_EXIST == (STD_ON))
#if((IRQ_GTM_TIM1_SR6_PRIO > 0) || (IRQ_GTM_TIM1_SR6_CAT == IRQ_CAT23))
#if((IRQ_GTM_TIM1_SR6_PRIO > 0) && (IRQ_GTM_TIM1_SR6_CAT == IRQ_CAT1))
void OS_ISR_GTMTIM1SR6_ISR (void)
#elif IRQ_GTM_TIM1_SR6_CAT == IRQ_CAT23
ISR(GTMTIM1SR6_ISR)
#endif
{
  /* Enable Global Interrupts */
  Mcal_EnableAllInterrupts();
  /* Parameter is Channel Number */
  Gtm_IsrTimModule(1, 6);
}
#endif
#endif 

#if(IRQ_GTM_TIM1SR7_EXIST == (STD_ON))
#if((IRQ_GTM_TIM1_SR7_PRIO > 0) || (IRQ_GTM_TIM1_SR7_CAT == IRQ_CAT23))
#if((IRQ_GTM_TIM1_SR7_PRIO > 0) && (IRQ_GTM_TIM1_SR7_CAT == IRQ_CAT1))
void OS_ISR_GTMTIM1SR7_ISR (void)
#elif IRQ_GTM_TIM1_SR7_CAT == IRQ_CAT23
ISR(GTMTIM1SR7_ISR)
#endif
{
  /* Enable Global Interrupts */
  Mcal_EnableAllInterrupts();
  /* Parameter is Channel Number */
  Gtm_IsrTimModule(1, 7);
}
#endif

#endif 

#if(IRQ_GTM_TIM2SR0_EXIST == (STD_ON))
/******************** TIM 2 *************************************/

#if((IRQ_GTM_TIM2_SR0_PRIO > 0) || (IRQ_GTM_TIM2_SR0_CAT == IRQ_CAT23))
#if((IRQ_GTM_TIM2_SR0_PRIO > 0) && (IRQ_GTM_TIM2_SR0_CAT == IRQ_CAT1))
void OS_ISR_GTMTIM2SR0_ISR (void)
#elif IRQ_GTM_TIM2_SR0_CAT == IRQ_CAT23
ISR(GTMTIM2SR0_ISR)
#endif
{
  /* Enable Global Interrupts */
  Mcal_EnableAllInterrupts();
  /* Parameter is Channel Number */
  Gtm_IsrTimModule(2, 0);
}
#endif
#endif 

#if(IRQ_GTM_TIM2SR1_EXIST == (STD_ON))
#if((IRQ_GTM_TIM2_SR1_PRIO > 0) || (IRQ_GTM_TIM2_SR1_CAT == IRQ_CAT23))
#if((IRQ_GTM_TIM2_SR1_PRIO > 0) && (IRQ_GTM_TIM2_SR1_CAT == IRQ_CAT1))
void OS_ISR_GTMTIM2SR1_ISR (void)
#elif IRQ_GTM_TIM2_SR1_CAT == IRQ_CAT23
ISR(GTMTIM2SR1_ISR)
#endif
{
  /* Enable Global Interrupts */
  Mcal_EnableAllInterrupts();
  /* Parameter is Channel Number */
  Gtm_IsrTimModule(2, 1);
}
#endif
#endif 

#if(IRQ_GTM_TIM2SR2_EXIST == (STD_ON))
#if((IRQ_GTM_TIM2_SR2_PRIO > 0) || (IRQ_GTM_TIM2_SR2_CAT == IRQ_CAT23))
#if((IRQ_GTM_TIM2_SR2_PRIO > 0) && (IRQ_GTM_TIM2_SR2_CAT == IRQ_CAT1))
void OS_ISR_GTMTIM2SR2_ISR (void)
#elif IRQ_GTM_TIM2_SR2_CAT == IRQ_CAT23
ISR(GTMTIM2SR2_ISR)
#endif
{
  /* Enable Global Interrupts */
  Mcal_EnableAllInterrupts();
  /* Parameter is Channel Number */
  Gtm_IsrTimModule(2, 2);
}
#endif
#endif 

#if(IRQ_GTM_TIM2SR3_EXIST == (STD_ON))
#if((IRQ_GTM_TIM2_SR3_PRIO > 0) || (IRQ_GTM_TIM2_SR3_CAT == IRQ_CAT23))
#if((IRQ_GTM_TIM2_SR3_PRIO > 0) && (IRQ_GTM_TIM2_SR3_CAT == IRQ_CAT1))
void OS_ISR_GTMTIM2SR3_ISR (void)
#elif IRQ_GTM_TIM2_SR3_CAT == IRQ_CAT23
ISR(GTMTIM2SR3_ISR)
#endif
{
  /* Enable Global Interrupts */
  Mcal_EnableAllInterrupts();
  /* Parameter is Channel Number */
  Gtm_IsrTimModule(2, 3);
}
#endif
#endif 

#if(IRQ_GTM_TIM2SR4_EXIST == (STD_ON))
#if((IRQ_GTM_TIM2_SR4_PRIO > 0) || (IRQ_GTM_TIM2_SR4_CAT == IRQ_CAT23))
#if((IRQ_GTM_TIM2_SR4_PRIO > 0) && (IRQ_GTM_TIM2_SR4_CAT == IRQ_CAT1))
void OS_ISR_GTMTIM2SR4_ISR (void)
#elif IRQ_GTM_TIM2_SR4_CAT == IRQ_CAT23
ISR(GTMTIM2SR4_ISR)
#endif
{
  /* Enable Global Interrupts */
  Mcal_EnableAllInterrupts();
  /* Parameter is Channel Number */
  Gtm_IsrTimModule(2, 4);
}
#endif
#endif 

#if(IRQ_GTM_TIM2SR5_EXIST == (STD_ON))
#if((IRQ_GTM_TIM2_SR5_PRIO > 0) || (IRQ_GTM_TIM2_SR5_CAT == IRQ_CAT23))
#if((IRQ_GTM_TIM2_SR5_PRIO > 0) && (IRQ_GTM_TIM2_SR5_CAT == IRQ_CAT1))
void OS_ISR_GTMTIM2SR5_ISR (void)
#elif IRQ_GTM_TIM2_SR5_CAT == IRQ_CAT23
ISR(GTMTIM2SR5_ISR)
#endif
{
  /* Enable Global Interrupts */
  Mcal_EnableAllInterrupts();
  /* Parameter is Channel Number */
  Gtm_IsrTimModule(2, 5);
}
#endif
#endif 

#if(IRQ_GTM_TIM2SR6_EXIST == (STD_ON))
#if((IRQ_GTM_TIM2_SR6_PRIO > 0) || (IRQ_GTM_TIM2_SR6_CAT == IRQ_CAT23))
#if((IRQ_GTM_TIM2_SR6_PRIO > 0) && (IRQ_GTM_TIM2_SR6_CAT == IRQ_CAT1))
void OS_ISR_GTMTIM2SR6_ISR (void)
#elif IRQ_GTM_TIM2_SR6_CAT == IRQ_CAT23
ISR(GTMTIM2SR6_ISR)
#endif
{
  /* Enable Global Interrupts */
  Mcal_EnableAllInterrupts();
  /* Parameter is Channel Number */
  Gtm_IsrTimModule(2, 6);
}
#endif
#endif 

#if(IRQ_GTM_TIM2SR7_EXIST == (STD_ON))
#if((IRQ_GTM_TIM2_SR7_PRIO > 0) || (IRQ_GTM_TIM2_SR7_CAT == IRQ_CAT23))
#if((IRQ_GTM_TIM2_SR7_PRIO > 0) && (IRQ_GTM_TIM2_SR7_CAT == IRQ_CAT1))
void OS_ISR_GTMTIM2SR7_ISR (void)
#elif IRQ_GTM_TIM2_SR7_CAT == IRQ_CAT23
ISR(GTMTIM2SR7_ISR)
#endif
{
  /* Enable Global Interrupts */
  Mcal_EnableAllInterrupts();
  /* Parameter is Channel Number */
  Gtm_IsrTimModule(2, 7);
}
#endif
#endif 

/******************** TIM 3 *************************************/
#if(IRQ_GTM_TIM3SR0_EXIST == (STD_ON))


#if((IRQ_GTM_TIM3_SR0_PRIO > 0) || (IRQ_GTM_TIM3_SR0_CAT == IRQ_CAT23))
#if((IRQ_GTM_TIM3_SR0_PRIO > 0) && (IRQ_GTM_TIM3_SR0_CAT == IRQ_CAT1))
void OS_ISR_GTMTIM3SR0_ISR (void)
#elif IRQ_GTM_TIM3_SR0_CAT == IRQ_CAT23
ISR(GTMTIM3SR0_ISR)
#endif
{
  /* Enable Global Interrupts */
  Mcal_EnableAllInterrupts();
  /* Parameter is Channel Number */
  Gtm_IsrTimModule(3, 0);
}
#endif
#endif 

#if(IRQ_GTM_TIM3SR1_EXIST == (STD_ON))
#if((IRQ_GTM_TIM3_SR1_PRIO > 0) || (IRQ_GTM_TIM3_SR1_CAT == IRQ_CAT23))
#if((IRQ_GTM_TIM3_SR1_PRIO > 0) && (IRQ_GTM_TIM3_SR1_CAT == IRQ_CAT1))
void OS_ISR_GTMTIM3SR1_ISR (void)
#elif IRQ_GTM_TIM3_SR1_CAT == IRQ_CAT23
ISR(GTMTIM3SR1_ISR)
#endif
{
  /* Enable Global Interrupts */
  Mcal_EnableAllInterrupts();
  /* Parameter is Channel Number */
  Gtm_IsrTimModule(3, 1);
}
#endif
#endif 

#if(IRQ_GTM_TIM3SR2_EXIST == (STD_ON))
#if((IRQ_GTM_TIM3_SR2_PRIO > 0) || (IRQ_GTM_TIM3_SR2_CAT == IRQ_CAT23))
#if((IRQ_GTM_TIM3_SR2_PRIO > 0) && (IRQ_GTM_TIM3_SR2_CAT == IRQ_CAT1))
void OS_ISR_GTMTIM3SR2_ISR (void)
#elif IRQ_GTM_TIM3_SR2_CAT == IRQ_CAT23
ISR(GTMTIM3SR2_ISR)
#endif
{
  /* Enable Global Interrupts */
  Mcal_EnableAllInterrupts();
  /* Parameter is Channel Number */
  Gtm_IsrTimModule(3, 2);
}
#endif
#endif 

#if(IRQ_GTM_TIM3SR3_EXIST == (STD_ON))
#if((IRQ_GTM_TIM3_SR3_PRIO > 0) || (IRQ_GTM_TIM3_SR3_CAT == IRQ_CAT23))
#if((IRQ_GTM_TIM3_SR3_PRIO > 0) && (IRQ_GTM_TIM3_SR3_CAT == IRQ_CAT1))
void OS_ISR_GTMTIM3SR3_ISR (void)
#elif IRQ_GTM_TIM3_SR3_CAT == IRQ_CAT23
ISR(GTMTIM3SR3_ISR)
#endif
{
  /* Enable Global Interrupts */
  Mcal_EnableAllInterrupts();
  /* Parameter is Channel Number */
  Gtm_IsrTimModule(3, 3);
}
#endif
#endif 

#if(IRQ_GTM_TIM3SR4_EXIST == (STD_ON))
#if((IRQ_GTM_TIM3_SR4_PRIO > 0) || (IRQ_GTM_TIM3_SR4_CAT == IRQ_CAT23))
#if((IRQ_GTM_TIM3_SR4_PRIO > 0) && (IRQ_GTM_TIM3_SR4_CAT == IRQ_CAT1))
void OS_ISR_GTMTIM3SR4_ISR (void)
#elif IRQ_GTM_TIM3_SR4_CAT == IRQ_CAT23
ISR(GTMTIM3SR4_ISR)
#endif
{
  /* Enable Global Interrupts */
  Mcal_EnableAllInterrupts();
  /* Parameter is Channel Number */
  Gtm_IsrTimModule(3, 4);
}
#endif
#endif 

#if(IRQ_GTM_TIM3SR5_EXIST == (STD_ON))
#if((IRQ_GTM_TIM3_SR5_PRIO > 0) || (IRQ_GTM_TIM3_SR5_CAT == IRQ_CAT23))
#if((IRQ_GTM_TIM3_SR5_PRIO > 0) && (IRQ_GTM_TIM3_SR5_CAT == IRQ_CAT1))
void OS_ISR_GTMTIM3SR5_ISR (void)
#elif IRQ_GTM_TIM3_SR5_CAT == IRQ_CAT23
ISR(GTMTIM3SR5_ISR)
#endif
{
  /* Enable Global Interrupts */
  Mcal_EnableAllInterrupts();
  /* Parameter is Channel Number */
  Gtm_IsrTimModule(3, 5);
}
#endif
#endif 

#if(IRQ_GTM_TIM3SR6_EXIST == (STD_ON))
#if((IRQ_GTM_TIM3_SR6_PRIO > 0) || (IRQ_GTM_TIM3_SR6_CAT == IRQ_CAT23))
#if((IRQ_GTM_TIM3_SR6_PRIO > 0) && (IRQ_GTM_TIM3_SR6_CAT == IRQ_CAT1))
void OS_ISR_GTMTIM3SR6_ISR (void)
#elif IRQ_GTM_TIM3_SR6_CAT == IRQ_CAT23
ISR(GTMTIM3SR6_ISR)
#endif
{
  /* Enable Global Interrupts */
  Mcal_EnableAllInterrupts();
  /* Parameter is Channel Number */
  Gtm_IsrTimModule(3, 6);
}
#endif
#endif 

#if(IRQ_GTM_TIM3SR7_EXIST == (STD_ON))
#if((IRQ_GTM_TIM3_SR7_PRIO > 0) || (IRQ_GTM_TIM3_SR7_CAT == IRQ_CAT23))
#if((IRQ_GTM_TIM3_SR7_PRIO > 0) && (IRQ_GTM_TIM3_SR7_CAT == IRQ_CAT1))
void OS_ISR_GTMTIM3SR7_ISR (void)
#elif IRQ_GTM_TIM3_SR7_CAT == IRQ_CAT23
ISR(GTMTIM3SR7_ISR)
#endif
{
  /* Enable Global Interrupts */
  Mcal_EnableAllInterrupts();
  /* Parameter is Channel Number */
  Gtm_IsrTimModule(3, 7);
}
#endif
#endif

/******************** TIM 4 *************************************/
#if(IRQ_GTM_TIM4SR0_EXIST == (STD_ON))
#if((IRQ_GTM_TIM4_SR0_PRIO > 0) || (IRQ_GTM_TIM4_SR0_CAT == IRQ_CAT23))
#if((IRQ_GTM_TIM4_SR0_PRIO > 0) && (IRQ_GTM_TIM4_SR0_CAT == IRQ_CAT1))
void OS_ISR_GTMTIM4SR0_ISR (void)
#elif IRQ_GTM_TIM4_SR0_CAT == IRQ_CAT23
ISR(GTMTIM4SR0_ISR)
#endif
{
  /* Enable Global Interrupts */
  Mcal_EnableAllInterrupts();
  /* Parameter is Channel Number */
  Gtm_IsrTimModule(4, 0);
}
#endif
#endif

#if(IRQ_GTM_TIM4SR1_EXIST == (STD_ON))
#if((IRQ_GTM_TIM4_SR1_PRIO > 0) || (IRQ_GTM_TIM4_SR1_CAT == IRQ_CAT23))
#if((IRQ_GTM_TIM4_SR1_PRIO > 0) && (IRQ_GTM_TIM4_SR1_CAT == IRQ_CAT1))
void OS_ISR_GTMTIM4SR1_ISR (void)
#elif IRQ_GTM_TIM4_SR1_CAT == IRQ_CAT23
ISR(GTMTIM4SR1_ISR)
#endif
{
  /* Enable Global Interrupts */
  Mcal_EnableAllInterrupts();
  /* Parameter is Channel Number */
  Gtm_IsrTimModule(4, 1);
}
#endif
#endif

#if(IRQ_GTM_TIM4SR2_EXIST == (STD_ON))
#if((IRQ_GTM_TIM4_SR2_PRIO > 0) || (IRQ_GTM_TIM4_SR2_CAT == IRQ_CAT23))
#if((IRQ_GTM_TIM4_SR2_PRIO > 0) && (IRQ_GTM_TIM4_SR2_CAT == IRQ_CAT1))
void OS_ISR_GTMTIM4SR2_ISR (void)
#elif IRQ_GTM_TIM4_SR2_CAT == IRQ_CAT23
ISR(GTMTIM4SR2_ISR)
#endif
{
  /* Enable Global Interrupts */
  Mcal_EnableAllInterrupts();
  /* Parameter is Channel Number */
  Gtm_IsrTimModule(4, 2);
}
#endif
#endif

#if(IRQ_GTM_TIM4SR3_EXIST == (STD_ON))
#if((IRQ_GTM_TIM4_SR3_PRIO > 0) || (IRQ_GTM_TIM4_SR3_CAT == IRQ_CAT23))
#if((IRQ_GTM_TIM4_SR3_PRIO > 0) && (IRQ_GTM_TIM4_SR3_CAT == IRQ_CAT1))
void OS_ISR_GTMTIM4SR3_ISR (void)
#elif IRQ_GTM_TIM4_SR3_CAT == IRQ_CAT23
ISR(GTMTIM4SR3_ISR)
#endif
{
  /* Enable Global Interrupts */
  Mcal_EnableAllInterrupts();
  /* Parameter is Channel Number */
  Gtm_IsrTimModule(4, 3);
}
#endif
#endif

#if(IRQ_GTM_TIM4SR4_EXIST == (STD_ON))
#if((IRQ_GTM_TIM4_SR4_PRIO > 0) || (IRQ_GTM_TIM4_SR4_CAT == IRQ_CAT23))
#if((IRQ_GTM_TIM4_SR4_PRIO > 0) && (IRQ_GTM_TIM4_SR4_CAT == IRQ_CAT1))
void OS_ISR_GTMTIM4SR4_ISR (void)
#elif IRQ_GTM_TIM4_SR4_CAT == IRQ_CAT23
ISR(GTMTIM4SR4_ISR)
#endif
{
  /* Enable Global Interrupts */
  Mcal_EnableAllInterrupts();
  /* Parameter is Channel Number */
  Gtm_IsrTimModule(4, 4);
}
#endif
#endif

#if(IRQ_GTM_TIM4SR5_EXIST == (STD_ON))
#if((IRQ_GTM_TIM4_SR5_PRIO > 0) || (IRQ_GTM_TIM4_SR5_CAT == IRQ_CAT23))
#if((IRQ_GTM_TIM4_SR5_PRIO > 0) && (IRQ_GTM_TIM4_SR5_CAT == IRQ_CAT1))
void OS_ISR_GTMTIM4SR5_ISR (void)
#elif IRQ_GTM_TIM4_SR5_CAT == IRQ_CAT23
ISR(GTMTIM4SR5_ISR)
#endif
{
  /* Enable Global Interrupts */
  Mcal_EnableAllInterrupts();
  /* Parameter is Channel Number */
  Gtm_IsrTimModule(4, 5);
}
#endif
#endif

#if(IRQ_GTM_TIM4SR6_EXIST == (STD_ON))
#if((IRQ_GTM_TIM4_SR6_PRIO > 0) || (IRQ_GTM_TIM4_SR6_CAT == IRQ_CAT23))
#if((IRQ_GTM_TIM4_SR6_PRIO > 0) && (IRQ_GTM_TIM4_SR6_CAT == IRQ_CAT1))
void OS_ISR_GTMTIM4SR6_ISR (void)
#elif IRQ_GTM_TIM4_SR6_CAT == IRQ_CAT23
ISR(GTMTIM4SR6_ISR)
#endif
{
  /* Enable Global Interrupts */
  Mcal_EnableAllInterrupts();
  /* Parameter is Channel Number */
  Gtm_IsrTimModule(4, 6);
}
#endif
#endif

#if(IRQ_GTM_TIM4SR7_EXIST == (STD_ON))
#if((IRQ_GTM_TIM4_SR7_PRIO > 0) || (IRQ_GTM_TIM4_SR7_CAT == IRQ_CAT23))
#if((IRQ_GTM_TIM4_SR7_PRIO > 0) && (IRQ_GTM_TIM4_SR7_CAT == IRQ_CAT1))
void OS_ISR_GTMTIM4SR7_ISR (void)
#elif IRQ_GTM_TIM4_SR7_CAT == IRQ_CAT23
ISR(GTMTIM4SR7_ISR)
#endif
{
  /* Enable Global Interrupts */
  Mcal_EnableAllInterrupts();
  /* Parameter is Channel Number */
  Gtm_IsrTimModule(4, 7);
}
#endif
#endif

/******************** TIM 5 *************************************/
#if(IRQ_GTM_TIM5SR0_EXIST == (STD_ON))
#if((IRQ_GTM_TIM5_SR0_PRIO > 0) || (IRQ_GTM_TIM5_SR0_CAT == IRQ_CAT23))
#if((IRQ_GTM_TIM5_SR0_PRIO > 0) && (IRQ_GTM_TIM5_SR0_CAT == IRQ_CAT1))
void OS_ISR_GTMTIM5SR0_ISR (void)
#elif IRQ_GTM_TIM5_SR0_CAT == IRQ_CAT23
ISR(GTMTIM5SR0_ISR)
#endif
{
  /* Enable Global Interrupts */
  Mcal_EnableAllInterrupts();
  /* Parameter is Channel Number */
  Gtm_IsrTimModule(4, 0);
}
#endif
#endif

#if(IRQ_GTM_TIM5SR1_EXIST == (STD_ON))
#if((IRQ_GTM_TIM5_SR1_PRIO > 0) || (IRQ_GTM_TIM5_SR1_CAT == IRQ_CAT23))
#if((IRQ_GTM_TIM5_SR1_PRIO > 0) && (IRQ_GTM_TIM5_SR1_CAT == IRQ_CAT1))
void OS_ISR_GTMTIM5SR1_ISR (void)
#elif IRQ_GTM_TIM5_SR1_CAT == IRQ_CAT23
ISR(GTMTIM5SR2_ISR)
#endif
{
  /* Enable Global Interrupts */
  Mcal_EnableAllInterrupts();
  /* Parameter is Channel Number */
  Gtm_IsrTimModule(4, 1);
}
#endif
#endif

#if(IRQ_GTM_TIM5SR2_EXIST == (STD_ON))
#if((IRQ_GTM_TIM5_SR2_PRIO > 0) || (IRQ_GTM_TIM5_SR2_CAT == IRQ_CAT23))
#if((IRQ_GTM_TIM5_SR2_PRIO > 0) && (IRQ_GTM_TIM5_SR2_CAT == IRQ_CAT1))
void OS_ISR_GTMTIM5SR2_ISR (void)
#elif IRQ_GTM_TIM5_SR2_CAT == IRQ_CAT23
ISR(GTMTIM5SR2_ISR)
#endif
{
  /* Enable Global Interrupts */
  Mcal_EnableAllInterrupts();
  /* Parameter is Channel Number */
  Gtm_IsrTimModule(4, 2);
}
#endif
#endif

#if(IRQ_GTM_TIM5SR3_EXIST == (STD_ON))
#if((IRQ_GTM_TIM5_SR3_PRIO > 0) || (IRQ_GTM_TIM5_SR3_CAT == IRQ_CAT23))
#if((IRQ_GTM_TIM5_SR3_PRIO > 0) && (IRQ_GTM_TIM5_SR3_CAT == IRQ_CAT1))
void OS_ISR_GTMTIM5SR3_ISR (void)
#elif IRQ_GTM_TIM5_SR3_CAT == IRQ_CAT23
ISR(GTMTIM5SR3_ISR)
#endif
{
  /* Enable Global Interrupts */
  Mcal_EnableAllInterrupts();
  /* Parameter is Channel Number */
  Gtm_IsrTimModule(4, 3);
}
#endif
#endif

#if(IRQ_GTM_TIM5SR4_EXIST == (STD_ON))
#if((IRQ_GTM_TIM5_SR4_PRIO > 0) || (IRQ_GTM_TIM5_SR4_CAT == IRQ_CAT23))
#if((IRQ_GTM_TIM5_SR4_PRIO > 0) && (IRQ_GTM_TIM5_SR4_CAT == IRQ_CAT1))
void OS_ISR_GTMTIM5SR4_ISR (void)
#elif IRQ_GTM_TIM5_SR4_CAT == IRQ_CAT23
ISR(GTMTIM5SR4_ISR)
#endif
{
  /* Enable Global Interrupts */
  Mcal_EnableAllInterrupts();
  /* Parameter is Channel Number */
  Gtm_IsrTimModule(4, 4);
}
#endif
#endif

#if(IRQ_GTM_TIM5SR5_EXIST == (STD_ON))
#if((IRQ_GTM_TIM5_SR5_PRIO > 0) || (IRQ_GTM_TIM5_SR5_CAT == IRQ_CAT23))
#if((IRQ_GTM_TIM5_SR5_PRIO > 0) && (IRQ_GTM_TIM5_SR5_CAT == IRQ_CAT1))
void OS_ISR_GTMTIM5SR5_ISR (void)
#elif IRQ_GTM_TIM5_SR5_CAT == IRQ_CAT23
ISR(GTMTIM5SR5_ISR)
#endif
{
  /* Enable Global Interrupts */
  Mcal_EnableAllInterrupts();
  /* Parameter is Channel Number */
  Gtm_IsrTimModule(4, 5);
}
#endif
#endif

#if(IRQ_GTM_TIM5SR6_EXIST == (STD_ON))
#if((IRQ_GTM_TIM5_SR6_PRIO > 0) || (IRQ_GTM_TIM5_SR6_CAT == IRQ_CAT23))
#if((IRQ_GTM_TIM5_SR6_PRIO > 0) && (IRQ_GTM_TIM5_SR6_CAT == IRQ_CAT1))
void OS_ISR_GTMTIM5SR6_ISR (void)
#elif IRQ_GTM_TIM5_SR6_CAT == IRQ_CAT23
ISR(GTMTIM5SR6_ISR)
#endif
{
  /* Enable Global Interrupts */
  Mcal_EnableAllInterrupts();
  /* Parameter is Channel Number */
  Gtm_IsrTimModule(4, 6);
}
#endif
#endif

#if(IRQ_GTM_TIM5SR7_EXIST == (STD_ON))
#if((IRQ_GTM_TIM5_SR7_PRIO > 0) || (IRQ_GTM_TIM5_SR7_CAT == IRQ_CAT23))
#if((IRQ_GTM_TIM5_SR7_PRIO > 0) && (IRQ_GTM_TIM5_SR7_CAT == IRQ_CAT1))
void OS_ISR_GTMTIM5SR7_ISR (void)
#elif IRQ_GTM_TIM5_SR7_CAT == IRQ_CAT23
ISR(GTMTIM5SR7_ISR)
#endif
{
  /* Enable Global Interrupts */
  Mcal_EnableAllInterrupts();
  /* Parameter is Channel Number */
  Gtm_IsrTimModule(4, 7);
}
#endif
#endif

#if(IRQ_GTM_AEI_EXIST == (STD_ON))
#if((IRQ_GTM_AEI_PRIO > 0) || (IRQ_GTM_AEI_CAT == IRQ_CAT23))
#if((IRQ_GTM_AEI_PRIO > 0) && (IRQ_GTM_AEI_CAT == IRQ_CAT1))
void OS_ISR_GTMAEI_ISR (void)
#elif IRQ_GTM_AEI_CAT == IRQ_CAT23
ISR(GTMAEI_ISR)
#endif
{
  /* Enable Global Interrupts */
  Mcal_EnableAllInterrupts();

}
#endif
#endif 

#if(IRQ_GTM_ARUSR0_EXIST == (STD_ON))
#if((IRQ_GTM_ARU_SR0_PRIO > 0) || (IRQ_GTM_ARU_SR0_CAT == IRQ_CAT23))
#if((IRQ_GTM_ARU_SR0_PRIO > 0) && (IRQ_GTM_ARU_SR0_CAT == IRQ_CAT1))
void OS_ISR_ GTMARUSR0_ISR (void)
#elif IRQ_GTM_ARU_SR0_CAT == IRQ_CAT23
ISR(GTMARUSR0_ISR)
#endif
{
  /* Enable Global Interrupts */
  Mcal_EnableAllInterrupts();

}
#endif
#endif 

#if(IRQ_GTM_ARUSR1_EXIST == (STD_ON))
#if((IRQ_GTM_ARU_SR1_PRIO > 0) || (IRQ_GTM_ARU_SR1_CAT == IRQ_CAT23))
#if((IRQ_GTM_ARU_SR1_PRIO > 0) && (IRQ_GTM_ARU_SR1_CAT == IRQ_CAT1))
void OS_ISR_GTMARUSR1_ISR (void)
#elif IRQ_GTM_ARU_SR1_CAT == IRQ_CAT23
ISR(GTMARUSR1_ISR)
#endif
{
  /* Enable Global Interrupts */
  Mcal_EnableAllInterrupts();

}
#endif
#endif 

#if(IRQ_GTM_ARUSR2_EXIST == (STD_ON))
#if((IRQ_GTM_ARU_SR2_PRIO > 0) || (IRQ_GTM_ARU_SR2_CAT == IRQ_CAT23))
#if((IRQ_GTM_ARU_SR2_PRIO > 0) && (IRQ_GTM_ARU_SR2_CAT == IRQ_CAT1))
void OS_ISR_GTMARUSR2_ISR (void)
#elif IRQ_GTM_ARU_SR2_CAT == IRQ_CAT23
ISR(GTMARUSR2_ISR)
#endif
{
  /* Enable Global Interrupts */
  Mcal_EnableAllInterrupts();

}
#endif
#endif

#if (IRQ_GTM_BRC_EXIST == (STD_ON))
#if((IRQ_GTM_BRC_PRIO > 0) || (IRQ_GTM_BRC_CAT == IRQ_CAT23))
#if((IRQ_GTM_BRC_PRIO > 0) && (IRQ_GTM_BRC_CAT == IRQ_CAT1))
void OS_ISR_GTMBRC_ISR (void)
#elif IRQ_GTM_BRC_CAT == IRQ_CAT23
ISR(GTMBRC_ISR)
#endif
{
  /* Enable Global Interrupts */
  Mcal_EnableAllInterrupts();

}
#endif
#endif 

#if(IRQ_GTM_CMP_EXIST == (STD_ON))
#if((IRQ_GTM_CMP_PRIO > 0) || (IRQ_GTM_CMP_CAT == IRQ_CAT23))
#if((IRQ_GTM_CMP_PRIO > 0) && (IRQ_GTM_CMP_CAT == IRQ_CAT1))
void OS_ISR_GTMCMP_ISR (void)
#elif IRQ_GTM_CMP_CAT == IRQ_CAT23
ISR(GTMCMP_ISR)
#endif
{
  /* Enable Global Interrupts */
  Mcal_EnableAllInterrupts();

}
#endif

#endif /* #if(IRQ_GTM_CMP_EXIST == (STD_ON)) */

#if(IRQ_GTM_SPE0_EXIST == (STD_ON))

#if((IRQ_GTM_SPE0_PRIO > 0) || (IRQ_GTM_SPE0_CAT == IRQ_CAT23))
#if((IRQ_GTM_SPE0_PRIO > 0) && (IRQ_GTM_SPE0_CAT == IRQ_CAT1))
void OS_ISR_GTMSPE0_ISR (void)
#elif IRQ_GTM_SPE0_CAT == IRQ_CAT23
ISR(GTMSPE0_ISR)
#endif
{
  /* Enable Global Interrupts */
  Mcal_EnableAllInterrupts();

}
#endif
#endif

#if(IRQ_GTM_SPE1_EXIST == (STD_ON))
#if((IRQ_GTM_SPE1_PRIO > 0) || (IRQ_GTM_SPE1_CAT == IRQ_CAT23))
#if((IRQ_GTM_SPE1_PRIO > 0) && (IRQ_GTM_SPE1_CAT == IRQ_CAT1))
void OS_ISR_GTMSPE1_ISR (void)
#elif IRQ_GTM_SPE1_CAT == IRQ_CAT23
ISR(GTMSPE1_ISR)
#endif
{
  /* Enable Global Interrupts */
  Mcal_EnableAllInterrupts();

}
#endif
#endif 

#if(IRQ_GTM_PSM0SR0_EXIST == (STD_ON))

#if((IRQ_GTM_PSM0_SR0_PRIO > 0) || (IRQ_GTM_PSM0_SR0_CAT == IRQ_CAT23))
#if((IRQ_GTM_PSM0_SR0_PRIO > 0) && (IRQ_GTM_PSM0_SR0_CAT == IRQ_CAT1))
void OS_ISR_GTMPSM0SR0_ISR (void)
#elif IRQ_GTM_PSM0_SR0_CAT == IRQ_CAT23
ISR(GTMPSM0SR0_ISR)
#endif
{
  /* Enable Global Interrupts */
  Mcal_EnableAllInterrupts();

}
#endif
#endif 

#if(IRQ_GTM_PSM0SR1_EXIST == (STD_ON))
#if((IRQ_GTM_PSM0_SR1_PRIO > 0) || (IRQ_GTM_PSM0_SR1_CAT == IRQ_CAT23))
#if((IRQ_GTM_PSM0_SR1_PRIO > 0) && (IRQ_GTM_PSM0_SR1_CAT == IRQ_CAT1))
void OS_ISR_GTMPSM0SR1_ISR (void)
#elif IRQ_GTM_PSM0_SR1_CAT == IRQ_CAT23
ISR(GTMPSM0SR1_ISR)
#endif
{
  /* Enable Global Interrupts */
  Mcal_EnableAllInterrupts();

}
#endif
#endif 

#if(IRQ_GTM_PSM0SR2_EXIST == (STD_ON))
#if((IRQ_GTM_PSM0_SR2_PRIO > 0) || (IRQ_GTM_PSM0_SR2_CAT == IRQ_CAT23))
#if((IRQ_GTM_PSM0_SR2_PRIO > 0) && (IRQ_GTM_PSM0_SR2_CAT == IRQ_CAT1))
void OS_ISR_GTMPSM0SR2_ISR (void)
#elif IRQ_GTM_PSM0_SR2_CAT == IRQ_CAT23
ISR(GTMPSM0SR2_ISR)
#endif
{
  /* Enable Global Interrupts */
  Mcal_EnableAllInterrupts();

}
#endif
#endif 

#if(IRQ_GTM_PSM0SR3_EXIST == (STD_ON))
#if((IRQ_GTM_PSM0_SR3_PRIO > 0) || (IRQ_GTM_PSM0_SR3_CAT == IRQ_CAT23))
#if((IRQ_GTM_PSM0_SR3_PRIO > 0) && (IRQ_GTM_PSM0_SR3_CAT == IRQ_CAT1))
void OS_ISR_GTMPSM0SR3_ISR (void)
#elif IRQ_GTM_PSM0_SR3_CAT == IRQ_CAT23
ISR(GTMPSM0SR3_ISR)
#endif
{
  /* Enable Global Interrupts */
  Mcal_EnableAllInterrupts();

}
#endif
#endif 

#if(IRQ_GTM_PSM0SR4_EXIST == (STD_ON))
#if((IRQ_GTM_PSM0_SR4_PRIO > 0) || (IRQ_GTM_PSM0_SR4_CAT == IRQ_CAT23))
#if((IRQ_GTM_PSM0_SR4_PRIO > 0) && (IRQ_GTM_PSM0_SR4_CAT == IRQ_CAT1))
void OS_ISR_GTMPSM0SR4_ISR (void)
#elif IRQ_GTM_PSM0_SR4_CAT == IRQ_CAT23
ISR(GTMPSM0SR4_ISR)
#endif
{
  /* Enable Global Interrupts */
  Mcal_EnableAllInterrupts();

}
#endif
#endif 

#if(IRQ_GTM_PSM0SR5_EXIST == (STD_ON))
#if((IRQ_GTM_PSM0_SR5_PRIO > 0) || (IRQ_GTM_PSM0_SR5_CAT == IRQ_CAT23))
#if((IRQ_GTM_PSM0_SR5_PRIO > 0) && (IRQ_GTM_PSM0_SR5_CAT == IRQ_CAT1))
void OS_ISR_GTMPSM0SR5_ISR (void)
#elif IRQ_GTM_PSM0_SR5_CAT == IRQ_CAT23
ISR(GTMPSM0SR5_ISR)
#endif
{
  /* Enable Global Interrupts */
  Mcal_EnableAllInterrupts();

}
#endif
#endif 

#if(IRQ_GTM_PSM0SR6_EXIST == (STD_ON))
#if((IRQ_GTM_PSM0_SR6_PRIO > 0) || (IRQ_GTM_PSM0_SR6_CAT == IRQ_CAT23))
#if((IRQ_GTM_PSM0_SR6_PRIO > 0) && (IRQ_GTM_PSM0_SR6_CAT == IRQ_CAT1))
void OS_ISR_GTMPSM0SR6_ISR (void)
#elif IRQ_GTM_PSM0_SR6_CAT == IRQ_CAT23
ISR(GTMPSM0SR6_ISR)
#endif
{
  /* Enable Global Interrupts */
  Mcal_EnableAllInterrupts();

}
#endif
#endif 

#if(IRQ_GTM_PSM0SR7_EXIST == (STD_ON))
#if((IRQ_GTM_PSM0_SR7_PRIO > 0) || (IRQ_GTM_PSM0_SR7_CAT == IRQ_CAT23))
#if((IRQ_GTM_PSM0_SR7_PRIO > 0) && (IRQ_GTM_PSM0_SR7_CAT == IRQ_CAT1))
void OS_ISR_GTMPSM0SR7_ISR (void)
#elif IRQ_GTM_PSM0_SR7_CAT == IRQ_CAT23
ISR(GTMPSM0SR7_ISR)
#endif
{
  /* Enable Global Interrupts */
  Mcal_EnableAllInterrupts();

}
#endif
#endif

#if(IRQ_GTM_DPLLSR0_EXIST == (STD_ON))

#if((IRQ_GTM_DPLL_SR0_PRIO > 0) || (IRQ_GTM_DPLL_SR0_CAT == IRQ_CAT23))
#if((IRQ_GTM_DPLL_SR0_PRIO > 0) && (IRQ_GTM_DPLL_SR0_CAT == IRQ_CAT1))
void OS_ISR_GTMDPLLSR0_ISR (void)
#elif IRQ_GTM_DPLL_SR0_CAT == IRQ_CAT23
ISR(GTMDPLLSR0_ISR)
#endif
{
  /* Enable Global Interrupts */
  Mcal_EnableAllInterrupts();

}
#endif
#endif

#if(IRQ_GTM_DPLLSR1_EXIST == (STD_ON))
#if((IRQ_GTM_DPLL_SR1_PRIO > 0) || (IRQ_GTM_DPLL_SR1_CAT == IRQ_CAT23))
#if((IRQ_GTM_DPLL_SR1_PRIO > 0) && (IRQ_GTM_DPLL_SR1_CAT == IRQ_CAT1))
void OS_ISR_GTMDPLLSR1_ISR (void)
#elif IRQ_GTM_DPLL_SR1_CAT == IRQ_CAT23
ISR(GTMDPLLSR1_ISR)
#endif
{
  /* Enable Global Interrupts */
  Mcal_EnableAllInterrupts();

}
#endif
#endif

#if(IRQ_GTM_DPLLSR2_EXIST == (STD_ON))
#if((IRQ_GTM_DPLL_SR2_PRIO > 0) || (IRQ_GTM_DPLL_SR2_CAT == IRQ_CAT23))
#if((IRQ_GTM_DPLL_SR2_PRIO > 0) && (IRQ_GTM_DPLL_SR2_CAT == IRQ_CAT1))
void OS_ISR_GTMDPLLSR2_ISR (void)
#elif IRQ_GTM_DPLL_SR2_CAT == IRQ_CAT23
ISR(GTMDPLLSR2_ISR)
#endif
{
  /* Enable Global Interrupts */
  Mcal_EnableAllInterrupts();

}
#endif
#endif

#if(IRQ_GTM_DPLLSR3_EXIST == (STD_ON))
#if((IRQ_GTM_DPLL_SR3_PRIO > 0) || (IRQ_GTM_DPLL_SR3_CAT == IRQ_CAT23))
#if((IRQ_GTM_DPLL_SR3_PRIO > 0) && (IRQ_GTM_DPLL_SR3_CAT == IRQ_CAT1))
void OS_ISR_GTMDPLLSR3_ISR (void)
#elif IRQ_GTM_DPLL_SR3_CAT == IRQ_CAT23
ISR(GTMDPLLSR3_ISR)
#endif
{
  /* Enable Global Interrupts */
  Mcal_EnableAllInterrupts();

}
#endif
#endif

#if(IRQ_GTM_DPLLSR4_EXIST == (STD_ON))
#if((IRQ_GTM_DPLL_SR4_PRIO > 0) || (IRQ_GTM_DPLL_SR4_CAT == IRQ_CAT23))
#if((IRQ_GTM_DPLL_SR4_PRIO > 0) && (IRQ_GTM_DPLL_SR4_CAT == IRQ_CAT1))
void OS_ISR_GTMDPLLSR4_ISR (void)
#elif IRQ_GTM_DPLL_SR4_CAT == IRQ_CAT23
ISR(GTMDPLLSR4_ISR)
#endif
{
  /* Enable Global Interrupts */
  Mcal_EnableAllInterrupts();

}
#endif
#endif

#if(IRQ_GTM_DPLLSR5_EXIST == (STD_ON))
#if((IRQ_GTM_DPLL_SR5_PRIO > 0) || (IRQ_GTM_DPLL_SR5_CAT == IRQ_CAT23))
#if((IRQ_GTM_DPLL_SR5_PRIO > 0) && (IRQ_GTM_DPLL_SR5_CAT == IRQ_CAT1))
void OS_ISR_GTMDPLLSR5_ISR (void)
#elif IRQ_GTM_DPLL_SR5_CAT == IRQ_CAT23
ISR(GTMDPLLSR5_ISR)
#endif
{
  /* Enable Global Interrupts */
  Mcal_EnableAllInterrupts();

}
#endif
#endif

#if(IRQ_GTM_DPLLSR6_EXIST == (STD_ON))
#if((IRQ_GTM_DPLL_SR6_PRIO > 0) || (IRQ_GTM_DPLL_SR6_CAT == IRQ_CAT23))
#if((IRQ_GTM_DPLL_SR6_PRIO > 0) && (IRQ_GTM_DPLL_SR6_CAT == IRQ_CAT1))
void OS_ISR_GTMDPLLSR6_ISR (void)
#elif IRQ_GTM_DPLL_SR6_CAT == IRQ_CAT23
ISR(GTMDPLLSR6_ISR)
#endif
{
  /* Enable Global Interrupts */
  Mcal_EnableAllInterrupts();

}
#endif
#endif

#if(IRQ_GTM_DPLLSR7_EXIST == (STD_ON))
#if((IRQ_GTM_DPLL_SR7_PRIO > 0) || (IRQ_GTM_DPLL_SR7_CAT == IRQ_CAT23))
#if((IRQ_GTM_DPLL_SR7_PRIO > 0) && (IRQ_GTM_DPLL_SR7_CAT == IRQ_CAT1))
void OS_ISR_GTMDPLLSR7_ISR (void)
#elif IRQ_GTM_DPLL_SR7_CAT == IRQ_CAT23
ISR(GTMDPLLSR7_ISR)
#endif
{
  /* Enable Global Interrupts */
  Mcal_EnableAllInterrupts();

}
#endif
#endif

#if(IRQ_GTM_DPLLSR8_EXIST == (STD_ON))
#if((IRQ_GTM_DPLL_SR8_PRIO > 0) || (IRQ_GTM_DPLL_SR8_CAT == IRQ_CAT23))
#if((IRQ_GTM_DPLL_SR8_PRIO > 0) && (IRQ_GTM_DPLL_SR8_CAT == IRQ_CAT1))
void OS_ISR_GTMDPLLSR8_ISR (void)
#elif IRQ_GTM_DPLL_SR8_CAT == IRQ_CAT23
ISR(GTMDPLLSR8_ISR)
#endif
{
  /* Enable Global Interrupts */
  Mcal_EnableAllInterrupts();

}
#endif
#endif

#if(IRQ_GTM_DPLLSR9_EXIST == (STD_ON))
#if((IRQ_GTM_DPLL_SR9_PRIO > 0) || (IRQ_GTM_DPLL_SR9_CAT == IRQ_CAT23))
#if((IRQ_GTM_DPLL_SR9_PRIO > 0) && (IRQ_GTM_DPLL_SR9_CAT == IRQ_CAT1))
void OS_ISR_GTMDPLLSR9_ISR (void)
#elif IRQ_GTM_DPLL_SR9_CAT == IRQ_CAT23
ISR(GTMDPLLSR9_ISR)
#endif
{
  /* Enable Global Interrupts */
  Mcal_EnableAllInterrupts();

}
#endif
#endif

#if(IRQ_GTM_DPLLSR10_EXIST == (STD_ON))
#if((IRQ_GTM_DPLL_SR10_PRIO > 0) || (IRQ_GTM_DPLL_SR10_CAT == IRQ_CAT23))
#if((IRQ_GTM_DPLL_SR10_PRIO > 0) && (IRQ_GTM_DPLL_SR10_CAT == IRQ_CAT1))
void OS_ISR_GTMDPLLSR10_ISR (void)
#elif IRQ_GTM_DPLL_SR10_CAT == IRQ_CAT23
ISR(GTMDPLLSR10_ISR)
#endif
{
  /* Enable Global Interrupts */
  Mcal_EnableAllInterrupts();

}
#endif
#endif

#if(IRQ_GTM_DPLLSR11_EXIST == (STD_ON))
#if((IRQ_GTM_DPLL_SR11_PRIO > 0) || (IRQ_GTM_DPLL_SR11_CAT == IRQ_CAT23))
#if((IRQ_GTM_DPLL_SR11_PRIO > 0) && (IRQ_GTM_DPLL_SR11_CAT == IRQ_CAT1))
void OS_ISR_GTMDPLLSR11_ISR (void)
#elif IRQ_GTM_DPLL_SR11_CAT == IRQ_CAT23
ISR(GTMDPLLSR11_ISR)
#endif
{
  /* Enable Global Interrupts */
  Mcal_EnableAllInterrupts();

}
#endif
#endif

#if(IRQ_GTM_DPLLSR12_EXIST == (STD_ON))
#if((IRQ_GTM_DPLL_SR12_PRIO > 0) || (IRQ_GTM_DPLL_SR12_CAT == IRQ_CAT23))
#if((IRQ_GTM_DPLL_SR12_PRIO > 0) && (IRQ_GTM_DPLL_SR12_CAT == IRQ_CAT1))
void OS_ISR_GTMDPLLSR12_ISR (void)
#elif IRQ_GTM_DPLL_SR12_CAT == IRQ_CAT23
ISR(GTMDPLLSR12_ISR)
#endif
{
  /* Enable Global Interrupts */
  Mcal_EnableAllInterrupts();

}
#endif
#endif

#if(IRQ_GTM_DPLLSR13_EXIST == (STD_ON))
#if((IRQ_GTM_DPLL_SR13_PRIO > 0) || (IRQ_GTM_DPLL_SR13_CAT == IRQ_CAT23))
#if((IRQ_GTM_DPLL_SR13_PRIO > 0) && (IRQ_GTM_DPLL_SR13_CAT == IRQ_CAT1))
void OS_ISR_GTMDPLLSR13_ISR (void)
#elif IRQ_GTM_DPLL_SR13_CAT == IRQ_CAT23
ISR(GTMDPLLSR13_ISR)
#endif
{
  /* Enable Global Interrupts */
  Mcal_EnableAllInterrupts();

}
#endif
#endif

#if(IRQ_GTM_DPLLSR14_EXIST == (STD_ON))
#if((IRQ_GTM_DPLL_SR14_PRIO > 0) || (IRQ_GTM_DPLL_SR14_CAT == IRQ_CAT23))
#if((IRQ_GTM_DPLL_SR14_PRIO > 0) && (IRQ_GTM_DPLL_SR14_CAT == IRQ_CAT1))
void OS_ISR_GTMDPLLSR14_ISR (void)
#elif IRQ_GTM_DPLL_SR14_CAT == IRQ_CAT23
ISR(GTMDPLLSR14_ISR)
#endif
{
  /* Enable Global Interrupts */
  Mcal_EnableAllInterrupts();

}
#endif
#endif

#if(IRQ_GTM_DPLLSR15_EXIST == (STD_ON))
#if((IRQ_GTM_DPLL_SR15_PRIO > 0) || (IRQ_GTM_DPLL_SR15_CAT == IRQ_CAT23))
#if((IRQ_GTM_DPLL_SR15_PRIO > 0) && (IRQ_GTM_DPLL_SR15_CAT == IRQ_CAT1))
void OS_ISR_GTMDPLLSR15_ISR (void)
#elif IRQ_GTM_DPLL_SR15_CAT == IRQ_CAT23
ISR(GTMDPLLSR15_ISR)
#endif
{
  /* Enable Global Interrupts */
  Mcal_EnableAllInterrupts();

}
#endif
#endif

#if(IRQ_GTM_DPLLSR16_EXIST == (STD_ON))
#if((IRQ_GTM_DPLL_SR16_PRIO > 0) || (IRQ_GTM_DPLL_SR16_CAT == IRQ_CAT23))
#if((IRQ_GTM_DPLL_SR16_PRIO > 0) && (IRQ_GTM_DPLL_SR16_CAT == IRQ_CAT1))
void OS_ISR_GTMDPLLSR16_ISR (void)
#elif IRQ_GTM_DPLL_SR16_CAT == IRQ_CAT23
ISR(GTMDPLLSR16_ISR)
#endif
{
  /* Enable Global Interrupts */
  Mcal_EnableAllInterrupts();

}
#endif
#endif

#if(IRQ_GTM_DPLLSR17_EXIST == (STD_ON))
#if((IRQ_GTM_DPLL_SR17_PRIO > 0) || (IRQ_GTM_DPLL_SR17_CAT == IRQ_CAT23))
#if((IRQ_GTM_DPLL_SR17_PRIO > 0) && (IRQ_GTM_DPLL_SR17_CAT == IRQ_CAT1))
void OS_ISR_GTMDPLLSR17_ISR (void)
#elif IRQ_GTM_DPLL_SR17_CAT == IRQ_CAT23
ISR(GTMDPLLSR17_ISR)
#endif
{
  /* Enable Global Interrupts */
  Mcal_EnableAllInterrupts();

}
#endif
#endif

#if(IRQ_GTM_DPLLSR18_EXIST == (STD_ON))
#if((IRQ_GTM_DPLL_SR18_PRIO > 0) || (IRQ_GTM_DPLL_SR18_CAT == IRQ_CAT23))
#if((IRQ_GTM_DPLL_SR18_PRIO > 0) && (IRQ_GTM_DPLL_SR18_CAT == IRQ_CAT1))
void OS_ISR_GTMDPLLSR18_ISR (void)
#elif IRQ_GTM_DPLL_SR18_CAT == IRQ_CAT23
ISR(GTMDPLLSR18_ISR)
#endif
{
  /* Enable Global Interrupts */
  Mcal_EnableAllInterrupts();

}
#endif
#endif

#if(IRQ_GTM_DPLLSR19_EXIST == (STD_ON))
#if((IRQ_GTM_DPLL_SR19_PRIO > 0) || (IRQ_GTM_DPLL_SR19_CAT == IRQ_CAT23))
#if((IRQ_GTM_DPLL_SR19_PRIO > 0) && (IRQ_GTM_DPLL_SR19_CAT == IRQ_CAT1))
void OS_ISR_GTMDPLLSR19_ISR (void)
#elif IRQ_GTM_DPLL_SR19_CAT == IRQ_CAT23
ISR(GTMDPLLSR19_ISR)
#endif
{
  /* Enable Global Interrupts */
  Mcal_EnableAllInterrupts();

}
#endif
#endif

#if(IRQ_GTM_DPLLSR20_EXIST == (STD_ON))
#if((IRQ_GTM_DPLL_SR20_PRIO > 0) || (IRQ_GTM_DPLL_SR20_CAT == IRQ_CAT23))
#if((IRQ_GTM_DPLL_SR20_PRIO > 0) && (IRQ_GTM_DPLL_SR20_CAT == IRQ_CAT1))
void OS_ISR_GTMDPLLSR20_ISR (void)
#elif IRQ_GTM_DPLL_SR20_CAT == IRQ_CAT23
ISR(GTMDPLLSR20_ISR)
#endif
{
  /* Enable Global Interrupts */
  Mcal_EnableAllInterrupts();

}
#endif
#endif

#if(IRQ_GTM_DPLLSR21_EXIST == (STD_ON))
#if((IRQ_GTM_DPLL_SR21_PRIO > 0) || (IRQ_GTM_DPLL_SR21_CAT == IRQ_CAT23))
#if((IRQ_GTM_DPLL_SR21_PRIO > 0) && (IRQ_GTM_DPLL_SR21_CAT == IRQ_CAT1))
void OS_ISR_GTMDPLLSR21_ISR (void)
#elif IRQ_GTM_DPLL_SR21_CAT == IRQ_CAT23
ISR(GTMDPLLSR21_ISR)
#endif
{
  /* Enable Global Interrupts */
  Mcal_EnableAllInterrupts();

}
#endif
#endif

#if(IRQ_GTM_DPLLSR22_EXIST == (STD_ON))
#if((IRQ_GTM_DPLL_SR22_PRIO > 0) || (IRQ_GTM_DPLL_SR22_CAT == IRQ_CAT23))
#if((IRQ_GTM_DPLL_SR22_PRIO > 0) && (IRQ_GTM_DPLL_SR22_CAT == IRQ_CAT1))
void OS_ISR_GTMDPLLSR22_ISR (void)
#elif IRQ_GTM_DPLL_SR22_CAT == IRQ_CAT23
ISR(GTMDPLLSR22_ISR)
#endif
{
  /* Enable Global Interrupts */
  Mcal_EnableAllInterrupts();

}
#endif
#endif

#if(IRQ_GTM_DPLLSR23_EXIST == (STD_ON))
#if((IRQ_GTM_DPLL_SR23_PRIO > 0) || (IRQ_GTM_DPLL_SR23_CAT == IRQ_CAT23))
#if((IRQ_GTM_DPLL_SR23_PRIO > 0) && (IRQ_GTM_DPLL_SR23_CAT == IRQ_CAT1))
void OS_ISR_GTMDPLLSR23_ISR (void)
#elif IRQ_GTM_DPLL_SR23_CAT == IRQ_CAT23
ISR(GTMDPLLSR23_ISR)
#endif
{
  /* Enable Global Interrupts */
  Mcal_EnableAllInterrupts();

}
#endif
#endif

#if(IRQ_GTM_DPLLSR24_EXIST == (STD_ON))
#if((IRQ_GTM_DPLL_SR24_PRIO > 0) || (IRQ_GTM_DPLL_SR24_CAT == IRQ_CAT23))
#if((IRQ_GTM_DPLL_SR24_PRIO > 0) && (IRQ_GTM_DPLL_SR24_CAT == IRQ_CAT1))
void OS_ISR_GTMDPLLSR24_ISR (void)
#elif IRQ_GTM_DPLL_SR24_CAT == IRQ_CAT23
ISR(GTMDPLLSR24_ISR)
#endif
{
  /* Enable Global Interrupts */
  Mcal_EnableAllInterrupts();

}
#endif
#endif

#if(IRQ_GTM_DPLLSR25_EXIST == (STD_ON))
#if((IRQ_GTM_DPLL_SR25_PRIO > 0) || (IRQ_GTM_DPLL_SR25_CAT == IRQ_CAT23))
#if((IRQ_GTM_DPLL_SR25_PRIO > 0) && (IRQ_GTM_DPLL_SR25_CAT == IRQ_CAT1))
void OS_ISR_GTMDPLLSR25_ISR (void)
#elif IRQ_GTM_DPLL_SR25_CAT == IRQ_CAT23
ISR(GTMDPLLSR25_ISR)
#endif
{
  /* Enable Global Interrupts */
  Mcal_EnableAllInterrupts();

}
#endif
#endif

#if(IRQ_GTM_DPLLSR26_EXIST == (STD_ON))
#if((IRQ_GTM_DPLL_SR26_PRIO > 0) || (IRQ_GTM_DPLL_SR26_CAT == IRQ_CAT23))
#if((IRQ_GTM_DPLL_SR26_PRIO > 0) && (IRQ_GTM_DPLL_SR26_CAT == IRQ_CAT1))
void OS_ISR_GTMDPLLSR26_ISR (void)
#elif IRQ_GTM_DPLL_SR26_CAT == IRQ_CAT23
ISR(GTMDPLLSR26_ISR)
#endif
{
  /* Enable Global Interrupts */
  Mcal_EnableAllInterrupts();

}
#endif
#endif 

/****************** MCS0 *******************************************/
#if(IRQ_GTM_MCS0SR0_EXIST == (STD_ON))
#if((IRQ_GTM_MCS0_SR0_PRIO > 0) || (IRQ_GTM_MCS0_SR0_CAT == IRQ_CAT23))
#if((IRQ_GTM_MCS0_SR0_PRIO > 0) && (IRQ_GTM_MCS0_SR0_CAT == IRQ_CAT1))
void OS_ISR_GTMMCS0SR0_ISR (void)
#elif IRQ_GTM_MCS0_SR0_CAT == IRQ_CAT23
ISR(GTMMCS0SR0_ISR)
#endif
{
  /* Enable Global Interrupts */
  Mcal_EnableAllInterrupts();

}
#endif
#endif 

#if(IRQ_GTM_MCS0SR1_EXIST == (STD_ON))
#if((IRQ_GTM_MCS0_SR1_PRIO > 0) || (IRQ_GTM_MCS0_SR1_CAT == IRQ_CAT23))
#if((IRQ_GTM_MCS0_SR1_PRIO > 0) && (IRQ_GTM_MCS0_SR1_CAT == IRQ_CAT1))
void OS_ISR_GTMMCS0SR1_ISR (void)
#elif IRQ_GTM_MCS0_SR1_CAT == IRQ_CAT23
ISR(GTMMCS0SR1_ISR)
#endif
{
  /* Enable Global Interrupts */
  Mcal_EnableAllInterrupts();

}
#endif
#endif 

#if(IRQ_GTM_MCS0SR2_EXIST == (STD_ON))
#if((IRQ_GTM_MCS0_SR2_PRIO > 0) || (IRQ_GTM_MCS0_SR2_CAT == IRQ_CAT23))
#if((IRQ_GTM_MCS0_SR2_PRIO > 0) && (IRQ_GTM_MCS0_SR2_CAT == IRQ_CAT1))
void OS_ISR_GTMMCS0SR2_ISR (void)
#elif IRQ_GTM_MCS0_SR2_CAT == IRQ_CAT23
ISR(GTMMCS0SR2_ISR)
#endif
{
  /* Enable Global Interrupts */
  Mcal_EnableAllInterrupts();

}
#endif
#endif 

#if(IRQ_GTM_MCS0SR3_EXIST == (STD_ON))
#if((IRQ_GTM_MCS0_SR3_PRIO > 0) || (IRQ_GTM_MCS0_SR3_CAT == IRQ_CAT23))
#if((IRQ_GTM_MCS0_SR3_PRIO > 0) && (IRQ_GTM_MCS0_SR3_CAT == IRQ_CAT1))
void OS_ISR_GTMMCS0SR3_ISR (void)
#elif IRQ_GTM_MCS0_SR3_CAT == IRQ_CAT23
ISR(GTMMCS0SR3_ISR)
#endif
{
  /* Enable Global Interrupts */
  Mcal_EnableAllInterrupts();

}
#endif
#endif 

#if(IRQ_GTM_MCS0SR4_EXIST == (STD_ON))
#if((IRQ_GTM_MCS0_SR4_PRIO > 0) || (IRQ_GTM_MCS0_SR4_CAT == IRQ_CAT23))
#if((IRQ_GTM_MCS0_SR4_PRIO > 0) && (IRQ_GTM_MCS0_SR4_CAT == IRQ_CAT1))
void OS_ISR_GTMMCS0SR4_ISR (void)
#elif IRQ_GTM_MCS0_SR4_CAT == IRQ_CAT23
ISR(GTMMCS0SR4_ISR)
#endif
{
  /* Enable Global Interrupts */
  Mcal_EnableAllInterrupts();

}
#endif
#endif 

#if(IRQ_GTM_MCS0SR5_EXIST == (STD_ON))
#if((IRQ_GTM_MCS0_SR5_PRIO > 0) || (IRQ_GTM_MCS0_SR5_CAT == IRQ_CAT23))
#if((IRQ_GTM_MCS0_SR5_PRIO > 0) && (IRQ_GTM_MCS0_SR5_CAT == IRQ_CAT1))
void OS_ISR_GTMMCS0SR5_ISR (void)
#elif IRQ_GTM_MCS0_SR5_CAT == IRQ_CAT23
ISR(GTMMCS0SR5_ISR)
#endif
{
  /* Enable Global Interrupts */
  Mcal_EnableAllInterrupts();

}
#endif
#endif 

#if(IRQ_GTM_MCS0SR6_EXIST == (STD_ON))
#if((IRQ_GTM_MCS0_SR6_PRIO > 0) || (IRQ_GTM_MCS0_SR6_CAT == IRQ_CAT23))
#if((IRQ_GTM_MCS0_SR6_PRIO > 0) && (IRQ_GTM_MCS0_SR6_CAT == IRQ_CAT1))
void OS_ISR_GTMMCS0SR6_ISR (void)
#elif IRQ_GTM_MCS0_SR6_CAT == IRQ_CAT23
ISR(GTMMCS0SR6_ISR)
#endif
{
  /* Enable Global Interrupts */
  Mcal_EnableAllInterrupts();

}
#endif
#endif 

#if(IRQ_GTM_MCS0SR7_EXIST == (STD_ON))
#if((IRQ_GTM_MCS0_SR7_PRIO > 0) || (IRQ_GTM_MCS0_SR7_CAT == IRQ_CAT23))
#if((IRQ_GTM_MCS0_SR7_PRIO > 0) && (IRQ_GTM_MCS0_SR7_CAT == IRQ_CAT1))
void OS_ISR_GTMMCS0SR7_ISR (void)
#elif IRQ_GTM_MCS0_SR7_CAT == IRQ_CAT23
ISR(GTMMCS0SR7_ISR)
#endif
{
  /* Enable Global Interrupts */
  Mcal_EnableAllInterrupts();

}
#endif
#endif 

/****************** MCS1 *******************************************/
#if(IRQ_GTM_MCS1SR0_EXIST == (STD_ON))
#if((IRQ_GTM_MCS1_SR0_PRIO > 0) || (IRQ_GTM_MCS1_SR0_CAT == IRQ_CAT23))
#if((IRQ_GTM_MCS1_SR0_PRIO > 0) && (IRQ_GTM_MCS1_SR0_CAT == IRQ_CAT1))
void OS_ISR_GTMMCS1SR0_ISR (void)
#elif IRQ_GTM_MCS1_SR0_CAT == IRQ_CAT23
ISR(GTMMCS1SR0_ISR)
#endif
{
  /* Enable Global Interrupts */
  Mcal_EnableAllInterrupts();

}
#endif
#endif 

#if(IRQ_GTM_MCS1SR1_EXIST == (STD_ON))
#if((IRQ_GTM_MCS1_SR1_PRIO > 0) || (IRQ_GTM_MCS1_SR1_CAT == IRQ_CAT23))
#if((IRQ_GTM_MCS1_SR1_PRIO > 0) && (IRQ_GTM_MCS1_SR1_CAT == IRQ_CAT1))
void OS_ISR_GTMMCS1SR1_ISR (void)
#elif IRQ_GTM_MCS1_SR1_CAT == IRQ_CAT23
ISR(GTMMCS1SR1_ISR)
#endif
{
  /* Enable Global Interrupts */
  Mcal_EnableAllInterrupts();

}
#endif
#endif 

#if(IRQ_GTM_MCS1SR2_EXIST == (STD_ON))
#if((IRQ_GTM_MCS1_SR2_PRIO > 0) || (IRQ_GTM_MCS1_SR2_CAT == IRQ_CAT23))
#if((IRQ_GTM_MCS1_SR2_PRIO > 0) && (IRQ_GTM_MCS1_SR2_CAT == IRQ_CAT1))
void OS_ISR_GTMMCS1SR2_ISR (void)
#elif IRQ_GTM_MCS1_SR2_CAT == IRQ_CAT23
ISR(GTMMCS1SR2_ISR)
#endif
{
  /* Enable Global Interrupts */
  Mcal_EnableAllInterrupts();

}
#endif
#endif 

#if(IRQ_GTM_MCS1SR3_EXIST == (STD_ON))
#if((IRQ_GTM_MCS1_SR3_PRIO > 0) || (IRQ_GTM_MCS1_SR3_CAT == IRQ_CAT23))
#if((IRQ_GTM_MCS1_SR3_PRIO > 0) && (IRQ_GTM_MCS1_SR3_CAT == IRQ_CAT1))
void OS_ISR_GTMMCS1SR3_ISR (void)
#elif IRQ_GTM_MCS1_SR3_CAT == IRQ_CAT23
ISR(GTMMCS1SR3_ISR)
#endif
{
  /* Enable Global Interrupts */
  Mcal_EnableAllInterrupts();

}
#endif
#endif 

#if(IRQ_GTM_MCS1SR4_EXIST == (STD_ON))
#if((IRQ_GTM_MCS1_SR4_PRIO > 0) || (IRQ_GTM_MCS1_SR4_CAT == IRQ_CAT23))
#if((IRQ_GTM_MCS1_SR4_PRIO > 0) && (IRQ_GTM_MCS1_SR4_CAT == IRQ_CAT1))
void OS_ISR_GTMMCS1SR4_ISR (void)
#elif IRQ_GTM_MCS1_SR4_CAT == IRQ_CAT23
ISR(GTMMCS1SR4_ISR)
#endif
{
  /* Enable Global Interrupts */
  Mcal_EnableAllInterrupts();

}
#endif
#endif 

#if(IRQ_GTM_MCS1SR5_EXIST == (STD_ON))
#if((IRQ_GTM_MCS1_SR5_PRIO > 0) || (IRQ_GTM_MCS1_SR5_CAT == IRQ_CAT23))
#if((IRQ_GTM_MCS1_SR5_PRIO > 0) && (IRQ_GTM_MCS1_SR5_CAT == IRQ_CAT1))
void OS_ISR_GTMMCS1SR5_ISR (void)
#elif IRQ_GTM_MCS1_SR5_CAT == IRQ_CAT23
ISR(GTMMCS1SR5_ISR)
#endif
{
  /* Enable Global Interrupts */
  Mcal_EnableAllInterrupts();

}
#endif
#endif 

#if(IRQ_GTM_MCS1SR6_EXIST == (STD_ON))
#if((IRQ_GTM_MCS1_SR6_PRIO > 0) || (IRQ_GTM_MCS1_SR6_CAT == IRQ_CAT23))
#if((IRQ_GTM_MCS1_SR6_PRIO > 0) && (IRQ_GTM_MCS1_SR6_CAT == IRQ_CAT1))
void OS_ISR_GTMMCS1SR6_ISR (void)
#elif IRQ_GTM_MCS1_SR6_CAT == IRQ_CAT23
ISR(GTMMCS1SR6_ISR)
#endif
{
  /* Enable Global Interrupts */
  Mcal_EnableAllInterrupts();

}
#endif
#endif 

#if(IRQ_GTM_MCS1SR7_EXIST == (STD_ON))
#if((IRQ_GTM_MCS1_SR7_PRIO > 0) || (IRQ_GTM_MCS1_SR7_CAT == IRQ_CAT23))
#if((IRQ_GTM_MCS1_SR7_PRIO > 0) && (IRQ_GTM_MCS1_SR7_CAT == IRQ_CAT1))
void OS_ISR_GTMMCS1SR7_ISR (void)
#elif IRQ_GTM_MCS1_SR7_CAT == IRQ_CAT23
ISR(GTMMCS1SR7_ISR)
#endif
{
  /* Enable Global Interrupts */
  Mcal_EnableAllInterrupts();

}
#endif
#endif

/****************** MCS2 *******************************************/
#if(IRQ_GTM_MCS2SR0_EXIST == (STD_ON))
#if((IRQ_GTM_MCS2_SR0_PRIO > 0) || (IRQ_GTM_MCS2_SR0_CAT == IRQ_CAT23))
#if((IRQ_GTM_MCS2_SR0_PRIO > 0) && (IRQ_GTM_MCS2_SR0_CAT == IRQ_CAT1))
void OS_ISR_GTMMCS2SR0_ISR (void)
#elif IRQ_GTM_MCS2_SR0_CAT == IRQ_CAT23
ISR(GTMMCS2SR0_ISR)
#endif
{
  /* Enable Global Interrupts */
  Mcal_EnableAllInterrupts();

}
#endif
#endif

#if(IRQ_GTM_MCS2SR1_EXIST == (STD_ON))
#if((IRQ_GTM_MCS2_SR1_PRIO > 0) || (IRQ_GTM_MCS2_SR1_CAT == IRQ_CAT23))
#if((IRQ_GTM_MCS2_SR1_PRIO > 0) && (IRQ_GTM_MCS2_SR1_CAT == IRQ_CAT1))
void OS_ISR_GTMMCS2SR1_ISR (void)
#elif IRQ_GTM_MCS2_SR1_CAT == IRQ_CAT23
ISR(GTMMCS2SR1_ISR)
#endif
{
  /* Enable Global Interrupts */
  Mcal_EnableAllInterrupts();

}
#endif
#endif

#if(IRQ_GTM_MCS2SR2_EXIST == (STD_ON))
#if((IRQ_GTM_MCS2_SR2_PRIO > 0) || (IRQ_GTM_MCS2_SR2_CAT == IRQ_CAT23))
#if((IRQ_GTM_MCS2_SR2_PRIO > 0) && (IRQ_GTM_MCS2_SR2_CAT == IRQ_CAT1))
void OS_ISR_GTMMCS2SR2_ISR (void)
#elif IRQ_GTM_MCS2_SR2_CAT == IRQ_CAT23
ISR(GTMMCS2SR2_ISR)
#endif
{
  /* Enable Global Interrupts */
  Mcal_EnableAllInterrupts();

}
#endif
#endif

#if(IRQ_GTM_MCS2SR3_EXIST == (STD_ON))
#if((IRQ_GTM_MCS2_SR3_PRIO > 0) || (IRQ_GTM_MCS2_SR3_CAT == IRQ_CAT23))
#if((IRQ_GTM_MCS2_SR3_PRIO > 0) && (IRQ_GTM_MCS2_SR3_CAT == IRQ_CAT1))
void OS_ISR_GTMMCS2SR3_ISR (void)
#elif IRQ_GTM_MCS2_SR3_CAT == IRQ_CAT23
ISR(GTMMCS2SR3_ISR)
#endif
{
  /* Enable Global Interrupts */
  Mcal_EnableAllInterrupts();

}
#endif
#endif

#if(IRQ_GTM_MCS2SR4_EXIST == (STD_ON))
#if((IRQ_GTM_MCS2_SR4_PRIO > 0) || (IRQ_GTM_MCS2_SR4_CAT == IRQ_CAT23))
#if((IRQ_GTM_MCS2_SR4_PRIO > 0) && (IRQ_GTM_MCS2_SR4_CAT == IRQ_CAT1))
void OS_ISR_GTMMCS2SR4_ISR (void)
#elif IRQ_GTM_MCS2_SR4_CAT == IRQ_CAT23
ISR(GTMMCS2SR4_ISR)
#endif
{
  /* Enable Global Interrupts */
  Mcal_EnableAllInterrupts();

}
#endif
#endif

#if(IRQ_GTM_MCS2SR5_EXIST == (STD_ON))
#if((IRQ_GTM_MCS2_SR5_PRIO > 0) || (IRQ_GTM_MCS2_SR5_CAT == IRQ_CAT23))
#if((IRQ_GTM_MCS2_SR5_PRIO > 0) && (IRQ_GTM_MCS2_SR5_CAT == IRQ_CAT1))
void OS_ISR_GTMMCS2SR5_ISR (void)
#elif IRQ_GTM_MCS2_SR5_CAT == IRQ_CAT23
ISR(GTMMCS2SR5_ISR)
#endif
{
  /* Enable Global Interrupts */
  Mcal_EnableAllInterrupts();

}
#endif
#endif

#if(IRQ_GTM_MCS2SR6_EXIST == (STD_ON))
#if((IRQ_GTM_MCS2_SR6_PRIO > 0) || (IRQ_GTM_MCS2_SR6_CAT == IRQ_CAT23))
#if((IRQ_GTM_MCS2_SR6_PRIO > 0) && (IRQ_GTM_MCS2_SR6_CAT == IRQ_CAT1))
void OS_ISR_GTMMCS2SR6_ISR (void)
#elif IRQ_GTM_MCS2_SR6_CAT == IRQ_CAT23
ISR(GTMMCS2SR6_ISR)
#endif
{
  /* Enable Global Interrupts */
  Mcal_EnableAllInterrupts();

}
#endif
#endif

#if(IRQ_GTM_MCS2SR7_EXIST == (STD_ON))
#if((IRQ_GTM_MCS2_SR7_PRIO > 0) || (IRQ_GTM_MCS2_SR7_CAT == IRQ_CAT23))
#if((IRQ_GTM_MCS2_SR7_PRIO > 0) && (IRQ_GTM_MCS2_SR7_CAT == IRQ_CAT1))
void OS_ISR_GTMMCS2SR7_ISR (void)
#elif IRQ_GTM_MCS2_SR7_CAT == IRQ_CAT23
ISR(GTMMCS2SR7_ISR)
#endif
{
  /* Enable Global Interrupts */
  Mcal_EnableAllInterrupts();

}
#endif
#endif

/****************** MCS3 *******************************************/
#if(IRQ_GTM_MCS3SR0_EXIST == (STD_ON))
#if((IRQ_GTM_MCS3_SR0_PRIO > 0) || (IRQ_GTM_MCS3_SR0_CAT == IRQ_CAT23))
#if((IRQ_GTM_MCS3_SR0_PRIO > 0) && (IRQ_GTM_MCS3_SR0_CAT == IRQ_CAT1))
void OS_ISR_GTMMCS3SR0_ISR (void)
#elif IRQ_GTM_MCS3_SR0_CAT == IRQ_CAT23
ISR(GTMMCS3SR0_ISR)
#endif
{
  /* Enable Global Interrupts */
  Mcal_EnableAllInterrupts();

}
#endif
#endif

#if(IRQ_GTM_MCS3SR1_EXIST == (STD_ON))
#if((IRQ_GTM_MCS3_SR1_PRIO > 0) || (IRQ_GTM_MCS3_SR1_CAT == IRQ_CAT23))
#if((IRQ_GTM_MCS3_SR1_PRIO > 0) && (IRQ_GTM_MCS3_SR1_CAT == IRQ_CAT1))
void OS_ISR_GTMMCS3SR1_ISR (void)
#elif IRQ_GTM_MCS3_SR1_CAT == IRQ_CAT23
ISR(GTMMCS3SR1_ISR)
#endif
{
  /* Enable Global Interrupts */
  Mcal_EnableAllInterrupts();

}
#endif
#endif

#if(IRQ_GTM_MCS3SR2_EXIST == (STD_ON))
#if((IRQ_GTM_MCS3_SR2_PRIO > 0) || (IRQ_GTM_MCS3_SR2_CAT == IRQ_CAT23))
#if((IRQ_GTM_MCS3_SR2_PRIO > 0) && (IRQ_GTM_MCS3_SR2_CAT == IRQ_CAT1))
void OS_ISR_GTMMCS3SR2_ISR (void)
#elif IRQ_GTM_MCS3_SR2_CAT == IRQ_CAT23
ISR(GTMMCS3SR2_ISR)
#endif
{
  /* Enable Global Interrupts */
  Mcal_EnableAllInterrupts();

}
#endif
#endif

#if(IRQ_GTM_MCS3SR3_EXIST == (STD_ON))
#if((IRQ_GTM_MCS3_SR3_PRIO > 0) || (IRQ_GTM_MCS3_SR3_CAT == IRQ_CAT23))
#if((IRQ_GTM_MCS3_SR3_PRIO > 0) && (IRQ_GTM_MCS3_SR3_CAT == IRQ_CAT1))
void OS_ISR_GTMMCS3SR3_ISR (void)
#elif IRQ_GTM_MCS3_SR3_CAT == IRQ_CAT23
ISR(GTMMCS3SR3_ISR)
#endif
{
  /* Enable Global Interrupts */
  Mcal_EnableAllInterrupts();

}
#endif
#endif

#if(IRQ_GTM_MCS3SR4_EXIST == (STD_ON))
#if((IRQ_GTM_MCS3_SR4_PRIO > 0) || (IRQ_GTM_MCS3_SR4_CAT == IRQ_CAT23))
#if((IRQ_GTM_MCS3_SR4_PRIO > 0) && (IRQ_GTM_MCS3_SR4_CAT == IRQ_CAT1))
void OS_ISR_GTMMCS3SR4_ISR (void)
#elif IRQ_GTM_MCS3_SR4_CAT == IRQ_CAT23
ISR(GTMMCS3SR4_ISR)
#endif
{
  /* Enable Global Interrupts */
  Mcal_EnableAllInterrupts();

}
#endif
#endif

#if(IRQ_GTM_MCS3SR5_EXIST == (STD_ON))
#if((IRQ_GTM_MCS3_SR5_PRIO > 0) || (IRQ_GTM_MCS3_SR5_CAT == IRQ_CAT23))
#if((IRQ_GTM_MCS3_SR5_PRIO > 0) && (IRQ_GTM_MCS3_SR5_CAT == IRQ_CAT1))
void OS_ISR_GTMMCS3SR5_ISR (void)
#elif IRQ_GTM_MCS3_SR5_CAT == IRQ_CAT23
ISR(GTMMCS3SR5_ISR)
#endif
{
  /* Enable Global Interrupts */
  Mcal_EnableAllInterrupts();

}
#endif
#endif

#if(IRQ_GTM_MCS3SR6_EXIST == (STD_ON))
#if((IRQ_GTM_MCS3_SR6_PRIO > 0) || (IRQ_GTM_MCS3_SR6_CAT == IRQ_CAT23))
#if((IRQ_GTM_MCS3_SR6_PRIO > 0) && (IRQ_GTM_MCS3_SR6_CAT == IRQ_CAT1))
void OS_ISR_GTMMCS3SR6_ISR (void)
#elif IRQ_GTM_MCS3_SR6_CAT == IRQ_CAT23
ISR(GTMMCS3SR6_ISR)
#endif
{
  /* Enable Global Interrupts */
  Mcal_EnableAllInterrupts();

}
#endif
#endif

#if(IRQ_GTM_MCS3SR7_EXIST == (STD_ON))
#if((IRQ_GTM_MCS3_SR7_PRIO > 0) || (IRQ_GTM_MCS3_SR7_CAT == IRQ_CAT23))
#if((IRQ_GTM_MCS3_SR7_PRIO > 0) && (IRQ_GTM_MCS3_SR7_CAT == IRQ_CAT1))
void OS_ISR_GTMMCS3SR7_ISR (void)
#elif IRQ_GTM_MCS3_SR7_CAT == IRQ_CAT23
ISR(GTMMCS3SR7_ISR)
#endif
{
  /* Enable Global Interrupts */
  Mcal_EnableAllInterrupts();

}
#endif
#endif 

/****************** MCS4 *******************************************/
#if(IRQ_GTM_MCS4SR0_EXIST == (STD_ON))
#if((IRQ_GTM_MCS4_SR0_PRIO > 0) || (IRQ_GTM_MCS4_SR0_CAT == IRQ_CAT23))
#if((IRQ_GTM_MCS4_SR0_PRIO > 0) && (IRQ_GTM_MCS4_SR0_CAT == IRQ_CAT1))
void OS_ISR_GTMMCS4SR0_ISR (void)
#elif IRQ_GTM_MCS4_SR0_CAT == IRQ_CAT23
ISR(GTMMCS4SR0_ISR)
#endif
{
  /* Enable Global Interrupts */
  Mcal_EnableAllInterrupts();

}
#endif
#endif 

#if(IRQ_GTM_MCS4SR1_EXIST == (STD_ON))
#if((IRQ_GTM_MCS4_SR1_PRIO > 0) || (IRQ_GTM_MCS4_SR1_CAT == IRQ_CAT23))
#if((IRQ_GTM_MCS4_SR1_PRIO > 0) && (IRQ_GTM_MCS4_SR1_CAT == IRQ_CAT1))
void OS_ISR_GTMMCS4SR1_ISR (void)
#elif IRQ_GTM_MCS4_SR1_CAT == IRQ_CAT23
ISR(GTMMCS4SR1_ISR)
#endif
{
  /* Enable Global Interrupts */
  Mcal_EnableAllInterrupts();

}
#endif
#endif 

#if(IRQ_GTM_MCS4SR2_EXIST == (STD_ON))
#if((IRQ_GTM_MCS4_SR2_PRIO > 0) || (IRQ_GTM_MCS4_SR2_CAT == IRQ_CAT23))
#if((IRQ_GTM_MCS4_SR2_PRIO > 0) && (IRQ_GTM_MCS4_SR2_CAT == IRQ_CAT1))
void OS_ISR_GTMMCS4SR2_ISR (void)
#elif IRQ_GTM_MCS4_SR2_CAT == IRQ_CAT23
ISR(GTMMCS4SR2_ISR)
#endif
{
  /* Enable Global Interrupts */
  Mcal_EnableAllInterrupts();

}
#endif
#endif 

#if(IRQ_GTM_MCS4SR3_EXIST == (STD_ON))
#if((IRQ_GTM_MCS4_SR3_PRIO > 0) || (IRQ_GTM_MCS4_SR3_CAT == IRQ_CAT23))
#if((IRQ_GTM_MCS4_SR3_PRIO > 0) && (IRQ_GTM_MCS4_SR3_CAT == IRQ_CAT1))
void OS_ISR_GTMMCS4SR3_ISR (void)
#elif IRQ_GTM_MCS4_SR3_CAT == IRQ_CAT23
ISR(GTMMCS4SR3_ISR)
#endif
{
  /* Enable Global Interrupts */
  Mcal_EnableAllInterrupts();

}
#endif
#endif 

#if(IRQ_GTM_MCS4SR4_EXIST == (STD_ON))
#if((IRQ_GTM_MCS4_SR4_PRIO > 0) || (IRQ_GTM_MCS4_SR4_CAT == IRQ_CAT23))
#if((IRQ_GTM_MCS4_SR4_PRIO > 0) && (IRQ_GTM_MCS4_SR4_CAT == IRQ_CAT1))
void OS_ISR_GTMMCS4SR4_ISR (void)
#elif IRQ_GTM_MCS4_SR4_CAT == IRQ_CAT23
ISR(GTMMCS4SR4_ISR)
#endif
{
  /* Enable Global Interrupts */
  Mcal_EnableAllInterrupts();

}
#endif
#endif 

#if(IRQ_GTM_MCS4SR5_EXIST == (STD_ON))
#if((IRQ_GTM_MCS4_SR5_PRIO > 0) || (IRQ_GTM_MCS4_SR5_CAT == IRQ_CAT23))
#if((IRQ_GTM_MCS4_SR5_PRIO > 0) && (IRQ_GTM_MCS4_SR5_CAT == IRQ_CAT1))
void OS_ISR_GTMMCS4SR5_ISR (void)
#elif IRQ_GTM_MCS4_SR5_CAT == IRQ_CAT23
ISR(GTMMCS4SR5_ISR)
#endif
{
  /* Enable Global Interrupts */
  Mcal_EnableAllInterrupts();

}
#endif
#endif 

#if(IRQ_GTM_MCS4SR6_EXIST == (STD_ON))
#if((IRQ_GTM_MCS4_SR6_PRIO > 0) || (IRQ_GTM_MCS4_SR6_CAT == IRQ_CAT23))
#if((IRQ_GTM_MCS4_SR6_PRIO > 0) && (IRQ_GTM_MCS4_SR6_CAT == IRQ_CAT1))
void OS_ISR_GTMMCS4SR6_ISR (void)
#elif IRQ_GTM_MCS4_SR6_CAT == IRQ_CAT23
ISR(GTMMCS4SR6_ISR)
#endif
{
  /* Enable Global Interrupts */
  Mcal_EnableAllInterrupts();

}
#endif
#endif 

#if(IRQ_GTM_MCS4SR7_EXIST == (STD_ON))
#if((IRQ_GTM_MCS4_SR7_PRIO > 0) || (IRQ_GTM_MCS4_SR7_CAT == IRQ_CAT23))
#if((IRQ_GTM_MCS4_SR7_PRIO > 0) && (IRQ_GTM_MCS4_SR7_CAT == IRQ_CAT1))
void OS_ISR_GTMMCS4SR7_ISR (void)
#elif IRQ_GTM_MCS4_SR7_CAT == IRQ_CAT23
ISR(GTMMCS4SR7_ISR)
#endif
{
  /* Enable Global Interrupts */
  Mcal_EnableAllInterrupts();

}
#endif
#endif

/****************** MCS5 *******************************************/
#if(IRQ_GTM_MCS5SR0_EXIST == (STD_ON))
#if((IRQ_GTM_MCS5_SR0_PRIO > 0) || (IRQ_GTM_MCS5_SR0_CAT == IRQ_CAT23))
#if((IRQ_GTM_MCS5_SR0_PRIO > 0) && (IRQ_GTM_MCS5_SR0_CAT == IRQ_CAT1))
void OS_ISR_GTMMCS5SR0_ISR (void)
#elif IRQ_GTM_MCS5_SR0_CAT == IRQ_CAT23
ISR(GTMMCS5SR0_ISR)
#endif
{
  /* Enable Global Interrupts */
  Mcal_EnableAllInterrupts();

}
#endif
#endif

#if(IRQ_GTM_MCS5SR1_EXIST == (STD_ON))
#if((IRQ_GTM_MCS5_SR1_PRIO > 0) || (IRQ_GTM_MCS5_SR1_CAT == IRQ_CAT23))
#if((IRQ_GTM_MCS5_SR1_PRIO > 0) && (IRQ_GTM_MCS5_SR1_CAT == IRQ_CAT1))
void OS_ISR_GTMMCS5SR1_ISR (void)
#elif IRQ_GTM_MCS5_SR1_CAT == IRQ_CAT23
ISR(GTMMCS5SR1_ISR)
#endif
{
  /* Enable Global Interrupts */
  Mcal_EnableAllInterrupts();

}
#endif
#endif

#if(IRQ_GTM_MCS5SR2_EXIST == (STD_ON))
#if((IRQ_GTM_MCS5_SR2_PRIO > 0) || (IRQ_GTM_MCS5_SR2_CAT == IRQ_CAT23))
#if((IRQ_GTM_MCS5_SR2_PRIO > 0) && (IRQ_GTM_MCS5_SR2_CAT == IRQ_CAT1))
void OS_ISR_GTMMCS5SR2_ISR (void)
#elif IRQ_GTM_MCS5_SR2_CAT == IRQ_CAT23
ISR(GTMMCS5SR2_ISR)
#endif
{
  /* Enable Global Interrupts */
  Mcal_EnableAllInterrupts();

}
#endif
#endif

#if(IRQ_GTM_MCS5SR3_EXIST == (STD_ON))
#if((IRQ_GTM_MCS5_SR3_PRIO > 0) || (IRQ_GTM_MCS5_SR3_CAT == IRQ_CAT23))
#if((IRQ_GTM_MCS5_SR3_PRIO > 0) && (IRQ_GTM_MCS5_SR3_CAT == IRQ_CAT1))
void OS_ISR_GTMMCS5SR3_ISR (void)
#elif IRQ_GTM_MCS5_SR3_CAT == IRQ_CAT23
ISR(GTMMCS5SR3_ISR)
#endif
{
  /* Enable Global Interrupts */
  Mcal_EnableAllInterrupts();

}
#endif
#endif

#if(IRQ_GTM_MCS5SR4_EXIST == (STD_ON))
#if((IRQ_GTM_MCS5_SR4_PRIO > 0) || (IRQ_GTM_MCS5_SR4_CAT == IRQ_CAT23))
#if((IRQ_GTM_MCS5_SR4_PRIO > 0) && (IRQ_GTM_MCS5_SR4_CAT == IRQ_CAT1))
void OS_ISR_GTMMCS5SR4_ISR (void)
#elif IRQ_GTM_MCS5_SR4_CAT == IRQ_CAT23
ISR(GTMMCS5SR4_ISR)
#endif
{
  /* Enable Global Interrupts */
  Mcal_EnableAllInterrupts();

}
#endif
#endif

#if(IRQ_GTM_MCS5SR5_EXIST == (STD_ON))
#if((IRQ_GTM_MCS5_SR5_PRIO > 0) || (IRQ_GTM_MCS5_SR5_CAT == IRQ_CAT23))
#if((IRQ_GTM_MCS5_SR5_PRIO > 0) && (IRQ_GTM_MCS5_SR5_CAT == IRQ_CAT1))
void OS_ISR_GTMMCS5SR5_ISR (void)
#elif IRQ_GTM_MCS5_SR5_CAT == IRQ_CAT23
ISR(GTMMCS5SR5_ISR)
#endif
{
  /* Enable Global Interrupts */
  Mcal_EnableAllInterrupts();

}
#endif
#endif

#if(IRQ_GTM_MCS5SR6_EXIST == (STD_ON))
#if((IRQ_GTM_MCS5_SR6_PRIO > 0) || (IRQ_GTM_MCS5_SR6_CAT == IRQ_CAT23))
#if((IRQ_GTM_MCS5_SR6_PRIO > 0) && (IRQ_GTM_MCS5_SR6_CAT == IRQ_CAT1))
void OS_ISR_GTMMCS5SR6_ISR (void)
#elif IRQ_GTM_MCS5_SR6_CAT == IRQ_CAT23
ISR(GTMMCS5SR6_ISR)
#endif
{
  /* Enable Global Interrupts */
  Mcal_EnableAllInterrupts();

}
#endif
#endif

#if(IRQ_GTM_MCS5SR7_EXIST == (STD_ON))
#if((IRQ_GTM_MCS5_SR7_PRIO > 0) || (IRQ_GTM_MCS5_SR7_CAT == IRQ_CAT23))
#if((IRQ_GTM_MCS5_SR7_PRIO > 0) && (IRQ_GTM_MCS5_SR7_CAT == IRQ_CAT1))
void OS_ISR_GTMMCS5SR7_ISR (void)
#elif IRQ_GTM_MCS5_SR7_CAT == IRQ_CAT23
ISR(GTMMCS5SR7_ISR)
#endif
{
  /* Enable Global Interrupts */
  Mcal_EnableAllInterrupts();

}
#endif
#endif 

#if(IRQ_GTM_ERR_EXIST == (STD_ON))
#if((IRQ_GTM_ERR_SR_PRIO > 0) || (IRQ_GTM_ERR_SR_CAT == IRQ_CAT23))
#if((IRQ_GTM_ERR_SR_PRIO > 0) && (IRQ_GTM_ERR_SR_CAT == IRQ_CAT1))
void OS_ISR_GTMERRSR_ISR (void)
#elif IRQ_GTM_ERR_SR_CAT == IRQ_CAT23
ISR(GTMERRSR_ISR)
#endif
{
  /* Enable Global Interrupts */
  Mcal_EnableAllInterrupts();

}
#endif
#endif

#define GTM_STOP_SEC_CODE
#include "MemMap.h"
