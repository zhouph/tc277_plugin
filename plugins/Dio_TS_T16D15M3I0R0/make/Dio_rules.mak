# \file
#
# \brief AUTOSAR Dio
#
# This file contains the implementation of the AUTOSAR
# module Dio.
#
# \author Elektrobit Automotive GmbH, 91058 Erlangen, Germany
#
# Copyright 2005 - 2013 Elektrobit Automotive GmbH
# All rights exclusively reserved for Elektrobit Automotive GmbH,
# unless expressly agreed to otherwise.

#################################################################
# REGISTRY

LIBRARIES_TO_BUILD     += Dio_src

Dio_src_FILES       += $(Dio_CORE_PATH)\src\Dio.c
Dio_src_FILES       += $(Dio_CORE_PATH)\src\Dio_Ver.c
Dio_src_FILES       += $(Dio_OUTPUT_PATH)\src\Dio_PBCfg.c

ifeq ($(TS_BUILD_POST_BUILD_BINARY),TRUE)
# If the post build binary shall be built do not compile any files other then the postbuild files.
Dio_src_FILES :=
endif
