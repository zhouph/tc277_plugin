/**
 * \file
 *
 * \brief AUTOSAR WdgIf
 *
 * This file contains the implementation of the AUTOSAR
 * module WdgIf.
 *
 * \author Elektrobit Automotive GmbH, 91058 Erlangen, Germany
 *
 * Copyright 2005 - 2013 Elektrobit Automotive GmbH
 * All rights exclusively reserved for Elektrobit Automotive GmbH,
 * unless expressly agreed to otherwise.
 */
#if (!defined WDGIF_TYPES_H)
#define WDGIF_TYPES_H
/*==================[inclusions]=============================================*/
#include <Std_Types.h>      /* AUTOSAR standard types */

/*==================[macros]=================================================*/

/*==================[type definitions]=======================================*/

/** \brief Mode type of the WdgIf module
 **
 ** This enumeration type holds the mode types that are passed as parameters
 ** to Wdg_SetMode(). */
typedef enum
{
    WDGIF_OFF_MODE=0,
    /**< In this mode, the watchdog driver is disabled (switched off). */
    WDGIF_SLOW_MODE,
    /**< In this mode, the watchdog driver is set up for a long timeout
         period (slow triggering).  */
    WDGIF_FAST_MODE
    /**< In this mode, the watchdog driver is set up for a short timeout
         period (fast triggering).  */
} WdgIf_ModeType;

/*==================[external function declarations]=========================*/

/*==================[external constants]=====================================*/

/*==================[external data]==========================================*/

#endif /* if !defined( WDGIF_TYPES_H ) */
/*==================[end of file]============================================*/
