/**
 * \file
 *
 * \brief AUTOSAR WdgIf
 *
 * This file contains the implementation of the AUTOSAR
 * module WdgIf.
 *
 * \author Elektrobit Automotive GmbH, 91058 Erlangen, Germany
 *
 * Copyright 2005 - 2013 Elektrobit Automotive GmbH
 * All rights exclusively reserved for Elektrobit Automotive GmbH,
 * unless expressly agreed to otherwise.
 */
#if (!defined WDGIF_WDGDRIVERS_H)
#define WDGIF_WDGDRIVERS_H

/*==================[inclusions]=============================================*/
[!NOCODE!]

[!AUTOSPACING!]

[!CODE!][!//
/* Driver specific header files */
[!ENDCODE!][!//

************************************************************************
*** loop over watchdog references and generate driver specific #includes:
*** generate includes for driver specific API files of format
*** Wdg_<VendorId>_<VendorApiInfix>.h
************************************************************************
[!CODE!][!//
[!LOOP "node:order(WdgIfDevice/*,'WdgIfDeviceIndex')/WdgIfDriverRef"!][!//
[!/* get optional parameter VendorApiInfix
   * 3 cases must be differentiated
   * i)   node VendorApiInfix or CommonPublishedInformation
   *      does not exist (Wdg module violates AUTOSAR)
   *       => EB tresos Studio forbids this since version 2008.b-sr.1 (see TRESOS-1520)
   * ii)  node VendorApiInfix exists but is an empty string
   * iii) node VendorApiInfix exists and is a non-empty string
   */!][!//
[!VAR "WDGIF_VENDORAPIINFIX"!][!//
[!"as:ref(.)/../CommonPublishedInformation/VendorApiInfix"!][!//
[!ENDVAR!][!//
[!//
[!/* check content of VendorApiInfix */!][!//
#include [!//
[!IF "$WDGIF_VENDORAPIINFIX=''"!][!//
[!/* VendorApiInfix is empty; this implies count(WdgIfDevice/*) == 1 */!][!//
<Wdg.h> [!//
[!ELSE!][!//
<Wdg_[!"as:ref(.)/../CommonPublishedInformation/VendorId"!]_[!"$WDGIF_VENDORAPIINFIX"!].h> [!//
[!ENDIF!][!//
/* Watchdog driver API defines */
[!ENDLOOP!][!//
[!ENDCODE!][!//
[!ENDNOCODE!]

/*==================[macros]=================================================*/

/*==================[type definitions]=======================================*/

/*==================[external function declarations]=========================*/

/*==================[external constants]=====================================*/

/*==================[internal constants]=====================================*/

/*==================[external data]==========================================*/

/*==================[internal data]==========================================*/

/*==================[external function definitions]==========================*/

/*==================[internal function definitions]==========================*/

#endif /* if !defined( WDGIF_WDGDRIVERS_H ) */
/*==================[end of file]============================================*/
