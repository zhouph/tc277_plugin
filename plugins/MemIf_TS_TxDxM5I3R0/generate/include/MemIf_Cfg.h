/**
 * \file
 *
 * \brief AUTOSAR MemIf
 *
 * This file contains the implementation of the AUTOSAR
 * module MemIf.
 *
 * \author Elektrobit Automotive GmbH, 91058 Erlangen, Germany
 *
 * Copyright 2005 - 2013 Elektrobit Automotive GmbH
 * All rights exclusively reserved for Elektrobit Automotive GmbH,
 * unless expressly agreed to otherwise.
 */
#ifndef MEMIF_CFG_H
#define MEMIF_CFG_H
[!/*
 The relation
 MemIfGeneral/MemIfNumberOfDevices
   = count(as:modconf('Ea')) + count(as:modconf('Fee'))
 is enforced by the xdm file.
*/!][!//
[!VAR "NumDevices"="num:i(MemIfGeneral/MemIfNumberOfDevices)"!][!//
[!//
[!/*
 Check if the Ea and Fee is able to allow multiple instanciations
 if there are multiple configurations for these modules.
*/!][!//
[!MACRO "HEADER_NAME", "ModName" = "'Ea'"!][!//
[!VAR "Infix"!][!//
[!IF "node:exists(CommonPublishedInformation/VendorApiInfix)"!][!//
[!"CommonPublishedInformation/VendorApiInfix"!][!//
[!ENDIF!][!//
[!ENDVAR!][!//
[!//
[!"$ModName"!][!//
[!IF "$Infix != ''"!][!//
_[!"CommonPublishedInformation/VendorId"!]_[!"$Infix"!][!//
[!ENDIF!][!//
.h[!//
[!ENDMACRO!][!//
[!//
[!MACRO "EXTEND_PREFIXES", "ModName" = "'Ea'"!][!//
[!LOOP "as:modconf($ModName)"!][!//
[!VAR "Infix"!][!//
[!IF "node:exists(CommonPublishedInformation/VendorApiInfix)"!][!//
[!"CommonPublishedInformation/VendorApiInfix"!][!//
[!ENDIF!][!//
[!ENDVAR!][!//
[!//
[!VAR "Prefixes"!][!"$Prefixes"!];&[!"$ModName"!]_[!//
[!IF "$Infix != ''"!][!//
[!"CommonPublishedInformation/VendorId"!]_[!"$Infix"!][!//
[!ENDIF!][!//
[!ENDVAR!][!//
[!ENDLOOP!][!//
[!ENDMACRO!][!//
[!//
[!/* *** first build prefix-list *** */!][!//
[!VAR "Prefixes" = "''"!][!//
[!CALL "EXTEND_PREFIXES", "ModName" = "'Ea'"!][!//
[!CALL "EXTEND_PREFIXES", "ModName" = "'Fee'"!][!//
[!VAR "Prefixes" = "substring-after($Prefixes, ';')"!][!//

/*==================[includes]==============================================*/

/* !LINKSTO MemIf037,1 */
#include <Std_Types.h>                            /* AUTOSAR standard types     */
#include <TSAutosar.h>                            /* EB specific standard types */

/* include lower layer headers */
[!LOOP "as:modconf('Ea')"!][!//
#include <[!CALL "HEADER_NAME", "ModName" = "'Ea'"!]>
[!ENDLOOP!][!//
[!//
[!LOOP "as:modconf('Fee')"!][!//
#include <[!CALL "HEADER_NAME", "ModName" = "'Fee'"!]>
[!ENDLOOP!][!//

/*==================[macros]================================================*/

/** \brief En-/disable development error tracer checks */
#define MEMIF_DEV_ERROR_DETECT   [!//
[!IF "MemIfGeneral/MemIfDevErrorDetect = 'true'"!]STD_ON[!ELSE!]STD_OFF[!ENDIF!]

/** \brief Number of the underlying memory abstraction modules
 **
 ** [!"num:i(count(as:modconf('Ea')))"!] Ea and [!"num:i(count(as:modconf('Fee')))"!] Fee */
#define MEMIF_NUMBER_OF_DEVICES  [!"$NumDevices"!]U

/** \brief En-/disable API version information */
#define MEMIF_VERSION_INFO_API   [!//
[!IF "MemIfGeneral/MemIfVersionInfoApi = 'true'"!]STD_ON[!ELSE!]STD_OFF[!ENDIF!]

/* !LINKSTO MemIf019,1 */
[!IF "$NumDevices = 1"!][!//
[!/* *** determine device prefix *** */!][!//
[!LOOP "as:modconf('Ea')"!][!//
[!VAR "Prefix"!]Ea_[!ENDVAR!][!//
[!ENDLOOP!][!//
[!LOOP "as:modconf('Fee')"!][!//
[!VAR "Prefix"!]Fee_[!ENDVAR!][!//
[!ENDLOOP!][!//
[!//
/* Macro mappings of driver API calls */

#define MemIf_Read(DeviceIndex, BlockNumber, BlockOffset, DataBufferPtr, Length) \
  (TS_PARAM_UNUSED(DeviceIndex),[!"$Prefix"!]Read(BlockNumber, BlockOffset, DataBufferPtr, Length))

#define MemIf_Write(DeviceIndex, BlockNumber, DataBufferPtr) \
  (TS_PARAM_UNUSED(DeviceIndex),[!"$Prefix"!]Write(BlockNumber, DataBufferPtr))

#define MemIf_Cancel(DeviceIndex) \
  (TS_PARAM_UNUSED(DeviceIndex),[!"$Prefix"!]Cancel())

#define MemIf_GetStatus(DeviceIndex) \
  (TS_PARAM_UNUSED(DeviceIndex),[!"$Prefix"!]GetStatus())

#define MemIf_GetJobResult(DeviceIndex) \
  (TS_PARAM_UNUSED(DeviceIndex),[!"$Prefix"!]GetJobResult())

#define MemIf_InvalidateBlock(DeviceIndex, BlockNumber) \
  (TS_PARAM_UNUSED(DeviceIndex),[!"$Prefix"!]InvalidateBlock(BlockNumber))

#define MemIf_EraseImmediateBlock(DeviceIndex, BlockNumber) \
  (TS_PARAM_UNUSED(DeviceIndex),[!"$Prefix"!]EraseImmediateBlock(BlockNumber))

#define MemIf_SetMode(Mode) \
  [!"$Prefix"!]SetMode(Mode)
[!//
[!ENDIF!][!/* NumDevices */!][!//

/* !LINKSTO MemIf020,1 */
[!IF "$NumDevices > 1"!][!//
/* function pointers for the function pointer arrays */
#define MEMIF_SETMODE_FCTPTR \
{\
[!LOOP "text:split($Prefixes, ';')"!][!"."!]SetMode,\
[!ENDLOOP!]}

#define MEMIF_READ_FCTPTR \
{\
[!LOOP "text:split($Prefixes, ';')"!][!"."!]Read,\
[!ENDLOOP!]}

#define MEMIF_WRITE_FCTPTR \
{\
[!LOOP "text:split($Prefixes, ';')"!][!"."!]Write,\
[!ENDLOOP!]}

#define MEMIF_CANCEL_FCTPTR \
{\
[!LOOP "text:split($Prefixes, ';')"!][!"."!]Cancel,\
[!ENDLOOP!]}

#define MEMIF_GETSTATUS_FCTPTR \
{\
[!LOOP "text:split($Prefixes, ';')"!][!"."!]GetStatus,\
[!ENDLOOP!]}

#define MEMIF_GETJOBRESULT_FCTPTR \
{\
[!LOOP "text:split($Prefixes, ';')"!][!"."!]GetJobResult,\
[!ENDLOOP!]}

#define MEMIF_INVALIDATEBLOCK_FCTPTR \
{\
[!LOOP "text:split($Prefixes, ';')"!][!"."!]InvalidateBlock,\
[!ENDLOOP!]}

#define MEMIF_ERASEIMMEDIATEBLOCK_FCTPTR \
{\
[!LOOP "text:split($Prefixes, ';')"!][!"."!]EraseImmediateBlock,\
[!ENDLOOP!]}

[!ENDIF!][!//

/*==================[type definitions]======================================*/

/*==================[external data]=========================================*/

#endif /* ifndef MEMIF_CFG_H */
/*==================[end of file]===========================================*/
