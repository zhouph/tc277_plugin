/* Mk_configuration_protectionaction.c
 *
 * This file contains the configuration for protection actions.
 *
 * (c) Elektrobit Automotive GmbH
 *
 * $Id: Mk_configuration_protectionaction.c 18628 2015-03-02 11:09:43Z nibo2437 $
*/
#include <public/Mk_public_types.h>
#include <private/Mk_errorhandling.h>
#include <Mk_Cfg.h>

/* This array contains function pointers to all available protection actions.
 * These are selected by the return value of the protection hook and started from
 * MK_SysTerminateSelf().
 *
 * Note: The members of this array must correspond to enum mk_protectionaction_e.
 */
const mk_ppafunction_t MK_ppaFunctions[MK_PRO_INVALIDACTION + 1] =
{
	MK_PpaContinue,						/* action for MK_PRO_CONTINUE */
	MK_PpaInvalidProtectionAction,		/* action for PRO_IGNORE */
	MK_PpaTerminateTaskIsr,				/* action for PRO_TERMINATETASKISR */
	MK_PpaTerminateApplication,			/* action for PRO_TERMINATEAPPL */
	MK_PpaTerminateApplicationRestart,	/* action for PRO_TERMINATEAPPL_RESTART */
	MK_PpaShutdown,						/* action for PRO_SHUTDOWN */
	MK_PpaTerminateThread,				/* action for MK_PRO_TERMINATE */
	MK_PpaTerminateObject,				/* action for MK_PRO_TERMINATEALL */
	MK_PpaQuarantineObject,				/* action for MK_PRO_QUARANTINE */
	MK_PpaPanic,						/* action for MK_PRO_PANIC */
	MK_PpaPanicStop,					/* action for MK_PRO_PANICSTOP */
	MK_PpaInvalidProtectionAction		/* invalid action */
};

/* Editor settings; DO NOT DELETE
 * vi:set ts=4:
*/
