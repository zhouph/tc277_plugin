/* Mk_configuration_job.c
 *
 * This file contains the configuration for job queues.
 *
 * (c) Elektrobit Automotive GmbH
 *
 * $Id: Mk_configuration_job.c 15821 2014-04-16 04:55:15Z masa8317 $
*/
#include <public/Mk_public_types.h>
#include <private/Mk_jobqueue.h>
#include <private/Mk_thread.h>
#include <private/Mk_task.h>
#include <Mk_Cfg.h>

/* Job queues
 *
 * Each job queue configuration is copied to its thread state structure at startup. It is done
 * this way because only a subset of the threads has a job queue.
 *
 * Note: if MK_jobQueueCfg or MK_jobQueue is NULL, MK_nJobQueues is <= 0. This fact is used in the code.
*/
const mk_objquantity_t MK_nJobQueues = MK_NJOBQUEUES;

#if MK_JQBUFLEN > 0
mk_jobid_t MK_jqBuf[MK_JQBUFLEN];
#endif

#if MK_NJOBQUEUES > 0

static mk_jobqueue_t MK_jq[MK_NJOBQUEUES];
static const mk_jobqueuecfg_t MK_jqCfg[MK_NJOBQUEUES] = { MK_JOBQUEUECONFIG };

const mk_jobqueuecfg_t * const MK_jobQueueCfg = MK_jqCfg;
mk_jobqueue_t * const MK_jobQueue = MK_jq;

#else

const mk_jobqueuecfg_t * const MK_jobQueueCfg = MK_NULL;
mk_jobqueue_t * const MK_jobQueue = MK_NULL;

#endif

/* Editor settings; DO NOT DELETE
 * vi:set ts=4:
*/
