/* Mk_configuration_application.c
 *
 * This file contains the configuration for applications.
 *
 * (c) Elektrobit Automotive GmbH
 *
 * $Id: Mk_configuration_application.c 15821 2014-04-16 04:55:15Z masa8317 $
*/
#include <public/Mk_public_types.h>
#include <Mk_Cfg.h>

const mk_objquantity_t MK_nApps = MK_NAPPS;

#if MK_NAPPS > 0

/* MK_appRestartTasksArray is an array that contains the taskId of an OS-Application's restart task.
 * The index is the OS-Application's id.
*/
static const mk_objectid_t MK_appRestartTasksArray[MK_NAPPS] = { MK_APPRESTARTTASKS_CONFIG };
const mk_objectid_t * const MK_appRestartTasks = MK_appRestartTasksArray;

/* MK_appStatesArray is an array that contains the state of an OS-Application.
 * The index is the OS-Application's id. It is initialized implicitly by memory initialization.
*/
static mk_appstate_t MK_appStatesArray[MK_NAPPS];
mk_appstate_t * const MK_appStates = MK_appStatesArray;
#else
const mk_objectid_t * const MK_appRestartTasks = MK_NULL;
mk_appstate_t * const MK_appStates = MK_NULL;
#endif

/* Editor settings; DO NOT DELETE
 * vi:set ts=4:
*/
