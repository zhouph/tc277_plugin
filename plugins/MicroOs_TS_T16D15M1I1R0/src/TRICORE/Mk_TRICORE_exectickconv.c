/* Mk_TRICORE_exectickconv.c
 *
 * This file contains the function MK_ExecTickConvert. This function is called by MK_Dispatch()
 * to convert STM ticks to TPS ticks for execution budget protection.
 *
 * (c) Elektrobit Automotive GmbH
 *
 * $Id: Mk_TRICORE_exectickconv.c 15947 2014-04-28 10:16:48Z masa8317 $
*/
#include <public/Mk_public_types.h>
#include <private/TRICORE/Mk_TRICORE_accounting.h>
#include <Mk_board.h>

/* MK_ExecTickConvert()
 *
 * Converts ticks of the STM (which is used for measuring the execution time used) into
 * ticks of the temporal protection system, which is used to regain control if a thread
 * runs for too long without system calls or interrupts.
*/
mk_uint32_t MK_ExecTickConvert(mk_uint32_t stmTicks)
{
	/* Note: this calculation may overflow, but we expect the typical execution budget
	 *       of a task to stay within the bounds of the mk_uint32.
	 *
	 *       If the value actually _does_ overflow, it will lead to an early invocation
	 *       of the microkernel, which will then see that the thread still has budget.
	 *       The thread will then be allowed to execute further.
	*/
	return (stmTicks * MK_BOARD_STMTPSMUL);
}
