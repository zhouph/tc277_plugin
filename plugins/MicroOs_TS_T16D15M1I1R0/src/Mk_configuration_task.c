/* Mk_configuration_task.c
 *
 * This file contains the configuration for tasks.
 *
 * (c) Elektrobit Automotive GmbH
 *
 * $Id: Mk_configuration_task.c 18596 2015-02-26 16:22:13Z stpo8218 $
*/

/* MISRA-C:2004 Deviation List
 *
 * MISRA-1) Deviated Rule: 8.10 (required)
 *  Variable 'MK_taskThreads' should have internal linkage.
 *
 * Reason:
 *  False positive. MK_taskThreads is used in other modules
 *  (e.g. Mk_configuration_job.c), too.
*/

#include <public/Mk_public_types.h>
#include <private/Mk_task.h>
#include <private/Mk_interrupt.h>
#include <private/Mk_thread.h>
#include <private/Mk_event.h>
#include <Mk_Cfg.h>

/* The task tables
 *
 * If there are no tasks, the task dynamic array, the threads and the register stores are not needed.
 *
 * Note: if MK_taskCfg is NULL, MK_nTasks is <= 0. This fact is used in the code.
*/
const mk_objquantity_t MK_nTasks = MK_NTASKS;
const mk_objquantity_t MK_nTaskThreads = MK_NTASKTHREADS;

#if MK_NETASKS > 0
/* Event structures for extended tasks, if there are any
*/
static mk_eventstatus_t MK_eventStatus[MK_NETASKS];
#endif

#if MK_NTASKS > 0

static mk_task_t MK_taskDynamic[MK_NTASKS];
/* Deviation MISRA-1 */
mk_thread_t MK_taskThreads[MK_NTASKTHREADS];
static mk_threadregisters_t MK_taskRegisters[MK_NTASKREGISTERS];

static const mk_taskcfg_t MK_taskCfgTable[MK_NTASKS] = { MK_TASKCONFIG };
const mk_taskcfg_t * const MK_taskCfg = MK_taskCfgTable;

#else

const mk_taskcfg_t * const MK_taskCfg = MK_NULL;

#endif

/* Editor settings; DO NOT DELETE
 * vi:set ts=4:
*/
