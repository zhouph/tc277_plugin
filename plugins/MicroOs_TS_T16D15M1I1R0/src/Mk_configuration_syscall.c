/* Mk_configuration_syscall.c
 *
 * This file contains the configured system call table for the microkernel.
 *
 * (c) Elektrobit Automotive GmbH
 *
 * $Id: Mk_configuration_syscall.c 18706 2015-03-05 21:02:03Z mist8519 $
*/
#include <public/Mk_public_types.h>
#include <private/Mk_syscall.h>
#include <private/Mk_oscall.h>
#include <Mk_Cfg.h>

/* If the processor-specific implementation has defined support for ticker channels,
 * then Mk_HwSysInitTicker is placed in the MK_SC_InitTicker position in the syscall table.
 * If there are no tickers, MK_UnknownSyscall is placed there instead.
*/
#if MK_HWN_TICKER_CHANNELS > 0
#define MK_SYSINITTICKER	MK_HwSysInitTicker
#else
#define MK_SYSINITTICKER	MK_UnknownSyscall
#endif

/* System call table
 *
 * At the moment this table contains fixed entries, i.e. all system calls are
 * populated.
*/
/* !LINKSTO Microkernel.SystemCall.Table, 1
 * !doctype src
*/
mk_syscallfptr_t const MK_syscallTable[MK_NSYSCALL_MK] =
{
	MK_SysTerminateSelf,
	MK_SysActivateTask,
	MK_SysChainTask,
	MK_SysSchedule,
	MK_SysGetResource,
	MK_SysReleaseResource,
	MK_SysShutdown,
	MK_SysStartOs,
	MK_SysSetEvent,
	MK_SysClearEvent,
	MK_SysWaitEvent,
	MK_SysWaitEvent,		/* Implements MK_WaitGetClearEvent */
	MK_SysGetTaskId,
	MK_SysGetTaskState,
	MK_SysGetIsrId,
	MK_SysReportError,
	MK_SysStartForeignThread,
	MK_SysGetApplicationId,
	MK_SysTerminateApplication,
	MK_SysSelftest,
	MK_SysEnableInterruptSource,
	MK_SysDisableInterruptSource,
	MK_SYSINITTICKER
};

/* Editor settings; DO NOT DELETE
 * vi:set ts=4:
*/
