/* Mk_configuration.c
 *
 * This file contains the configuration for stacks and threads.
 *
 * (c) Elektrobit Automotive GmbH
 *
 * $Id: Mk_configuration.c 18595 2015-02-26 15:56:11Z stpo8218 $
*/

/* MISRA-C:2004 Deviation List
 *
 * MISRA-1) Deviated Rule: 11.1 (required)
 *  Conversion shall not be performed between a pointer to a function
 *  and any type other than an integral type.
 *
 * Reason:
 *  By convention the init function "main" returns an integer. Thread
 *  functions however return no value, so the function type is different.
*/
#include <public/Mk_public_types.h>
#include <public/Mk_public_api.h>
#include <public/Mk_error.h>
#include <private/Mk_startup.h>
#include <private/Mk_thread.h>
#include <private/Mk_interrupt.h>
#include <private/Mk_memoryprotection.h>
#include <private/Mk_errorhandling.h>
#include <private/Mk_exceptionhandling.h>
#include <public/Mk_callout.h>
#include <Mk_Cfg.h>

/* Microkernel configuration
 * =========================
 *
 * Kernel stack.
*/
#if (MK_TOOL == MK_diab)
#pragma section MK_STACKS "" ".mk_stack_MK_kernStack" standard RW
#pragma use_section MK_STACKS MK_kernStack
#define MK_ATTR_SS_KERN_STACK
#elif (MK_TOOL == MK_ghs)
#pragma ghs section bss=".mk_stack_MK_kernStack"
#pragma ghs section sbss=".mk_stack_MK_kernStackS"
#define MK_ATTR_SS_KERN_STACK
#elif (MK_TOOL==MK_gnu)
#define MK_ATTR_SS_KERN_STACK __attribute__((section(".mk_stack_MK_kernStack")))
#elif (MK_TOOL==MK_cw)
#define MK_ATTR_SS_KERN_STACK
#pragma push
#pragma section ".mk_stack_MK_kernStack"
#elif (MK_TOOL==MK_tasking)
#pragma section all "mk_stack_MK_kernStack"
#define MK_ATTR_SS_KERN_STACK
#elif (MK_TOOL==MK_ticgt)
#pragma DATA_SECTION(MK_kernStack, ".mk_stack_MK_kernStack")
#define MK_ATTR_SS_KERN_STACK
#else
#error "Unknown or unsupported toolchain. Check your Makefiles!"
#define MK_ATTR_SS_KERN_STACK
#endif

static mk_stackelement_t MK_kernStack[MK_KERNSTACK_NELEMENTS] MK_ATTR_SS_KERN_STACK;

#if (MK_TOOL == MK_diab)
#elif (MK_TOOL == MK_ghs)
#pragma ghs section sbss=default
#pragma ghs section bss=default
#elif (MK_TOOL==MK_gnu)
#elif (MK_TOOL==MK_cw)
#pragma pop
#elif (MK_TOOL==MK_tasking)
#pragma section all default
#elif (MK_TOOL==MK_ticgt)
#else
#error "Unknown or unsupported toolchain. Check your Makefiles!"
#endif

/* Constant for initializing the kernel stack pointer on entry to the kernel.
*/
mk_stackelement_t * const MK_kernelSpInitial = &MK_kernStack[MK_KERNSTACK_NELEMENTS-4];

#if MK_HAS_FPUCRVALUE
/* Constant for initializing FPU control register (if needed).
 * If the initial value is not configured, assume 0.
*/
#ifndef MK_FPUCRVALUE
#define MK_FPUCRVALUE	0u
#endif

const mk_fpucrvalue_t MK_threadFpuCrValue = MK_FPUCRVALUE;

#endif


/* Idle thread configuration
 * =========================
 *
 * This is the configuration for the thread that is automatically started to do nothing when there's
 * nothing else to do.
 * Typically, it runs MK_Idle().
 *
 * Stack for Idle thread
*/
#if (MK_TOOL == MK_diab)
#pragma section MK_STACKS "" ".mk_stack_MK_idleStack" standard RW
#pragma use_section MK_STACKS MK_idleStack
#define MK_ATTR_SS_IDLE_STACK
#elif (MK_TOOL == MK_ghs)
#pragma ghs section bss=".mk_stack_MK_idleStack"
#pragma ghs section sbss=".mk_stack_MK_idleStackS"
#define MK_ATTR_SS_IDLE_STACK
#elif (MK_TOOL==MK_gnu)
#define MK_ATTR_SS_IDLE_STACK __attribute__((section(".mk_stack_MK_idleStack")))
#elif (MK_TOOL==MK_cw)
#define MK_ATTR_SS_IDLE_STACK
#pragma push
#pragma section ".mk_stack_MK_idleStack"
#elif (MK_TOOL==MK_tasking)
#pragma section all "mk_stack_MK_idleStack"
#define MK_ATTR_SS_IDLE_STACK
#elif (MK_TOOL==MK_ticgt)
#pragma DATA_SECTION(MK_idleStack, ".mk_stack_MK_idleStack")
#define MK_ATTR_SS_IDLE_STACK
#else
#error "Unknown or unsupported toolchain. Check your Makefiles!"
#define MK_ATTR_SS_IDLE_STACK
#endif

static mk_stackelement_t MK_idleStack[MK_IDLESTACK_NELEMENTS] MK_ATTR_SS_IDLE_STACK;

#if (MK_TOOL == MK_diab)
#elif (MK_TOOL == MK_ghs)
#pragma ghs section sbss=default
#pragma ghs section bss=default
#elif (MK_TOOL==MK_gnu)
#elif (MK_TOOL==MK_cw)
#pragma pop
#elif (MK_TOOL==MK_tasking)
#pragma section all default
#elif (MK_TOOL==MK_ticgt)
#else
#error "Unknown or unsupported toolchain. Check your Makefiles!"
#endif

/* Thread structures for the idle thread.
*/
mk_thread_t MK_idleThread;								/* = {0} */
static mk_threadregisters_t MK_idleThreadRegisters;		/* = {0} */

/* !LINKSTO		Microkernel.Thread.Idle.Config, 1
 * !description	Idle thread has lowest priority. MK_IDLEFUNCTION and
 * !			MK_IDLEPSW are configuration dependent.
 * !doctype     src
*/
const mk_threadcfg_t MK_idleThreadConfig =
	MK_THREADCFG(	&MK_idleThreadRegisters,
					"mk-idle",
					&MK_idleStack[MK_IDLESTACK_NELEMENTS-4],
					MK_IDLEFUNCTION,
					MK_IDLEPM,
					MK_HWENABLEALLLEVEL,
					MK_THRIRQ_ENABLE,
					MK_THRFPU_DISABLE,
					MK_THRHWS_DEFAULT,
					0,	/* Queueing priority */
					0,	/* Running priority  */
					MK_MEMPART_IDLE,
					MK_OBJID_IDLE,
					MK_OBJTYPE_KERNEL,
					MK_EXECBUDGET_INFINITE,
					MK_NULL,
					MK_APPL_NONE	/* OS-Application Id */
				);

/* !LINKSTO	Microkernel.Thread.Shutdown.Config, 1,
 * !doctype src
*/
const mk_threadcfg_t MK_shutdownThreadConfig =
	MK_THREADCFG(	&MK_idleThreadRegisters,
					"mk-shutdown",
					&MK_idleStack[MK_IDLESTACK_NELEMENTS-4],
					MK_SHUTDOWNFUNCTION,
					MK_SHUTDOWNPM,
					MK_HWDISABLEALLLEVEL,
					MK_THRIRQ_DISABLE,
					MK_THRFPU_DISABLE,
					MK_THRHWS_DEFAULT,
					0,	/* Queueing priority */
					0,	/* Running priority */
					MK_MEMPART_IDLE,
					MK_OBJID_SHUTDOWN,
					MK_OBJTYPE_KERNEL,
					MK_EXECBUDGET_INFINITE,
					MK_NULL,
					MK_APPL_NONE	/* OS-Application Id */
				);


/* Low-priority OS thread
 * ======================
 *
 * This thread runs OS services when called from a thread whose current priority is
 * not greater than the scheduler priority.
 *
 * Stack for low-priority OS thread.
*/
#if (MK_TOOL == MK_diab)
#pragma section MK_STACKS "" ".mk_stack_MK_osThreadLowStack" standard RW
#pragma use_section MK_STACKS MK_osThreadLowStack
#define MK_ATTR_SS_TL_STACK
#elif (MK_TOOL == MK_ghs)
#pragma ghs section bss=".mk_stack_MK_osThreadLowStack"
#pragma ghs section sbss=".mk_stack_MK_osThreadLowStackS"
#define MK_ATTR_SS_TL_STACK
#elif (MK_TOOL==MK_gnu)
#define MK_ATTR_SS_TL_STACK __attribute__((section(".mk_stack_MK_osThreadLowStack")))
#elif (MK_TOOL==MK_cw)
#define MK_ATTR_SS_TL_STACK
#pragma push
#pragma section ".mk_stack_MK_osThreadLowStack"
#elif (MK_TOOL==MK_tasking)
#pragma section all "mk_stack_MK_osThreadLowStack"
#define MK_ATTR_SS_TL_STACK
#elif (MK_TOOL==MK_ticgt)
#pragma DATA_SECTION(MK_osThreadLowStack, ".mk_stack_MK_osThreadLowStack")
#define MK_ATTR_SS_TL_STACK
#else
#error "Unknown or unsupported toolchain. Check your Makefiles!"
#define MK_ATTR_SS_TL_STACK
#endif

static mk_stackelement_t MK_osThreadLowStack[MK_OSLOWSTACK_NELEMENTS] MK_ATTR_SS_TL_STACK;

#if (MK_TOOL == MK_diab)
#elif (MK_TOOL == MK_ghs)
#pragma ghs section sbss=default
#pragma ghs section bss=default
#elif (MK_TOOL==MK_gnu)
#elif (MK_TOOL==MK_cw)
#pragma pop
#elif (MK_TOOL==MK_tasking)
#pragma section all default
#elif (MK_TOOL==MK_ticgt)
#else
#error "Unknown or unsupported toolchain. Check your Makefiles!"
#endif

/* Thread structures for the low-priority OS thread
*/
mk_thread_t MK_osThreadLow;								/* = {0} */
static mk_threadregisters_t MK_osThreadLowRegisters;	/* = {0} */

const mk_threadcfg_t MK_osLowThreadConfig =
	MK_THREADCFG(	&MK_osThreadLowRegisters,
					"os-low",
					&MK_osThreadLowStack[MK_OSLOWSTACK_NELEMENTS-4],
					MK_NULL,
					MK_OSLOWPM,
					MK_HWENABLEALLLEVEL,
					MK_THRIRQ_ENABLE,
					MK_THRFPU_DISABLE,
					MK_THRHWS_DEFAULT,
					MK_OSLOWPRIO,	/* Queueing priority */
					MK_OSLOWPRIO,	/* Running priority */
					MK_MEMPART_OSLOW,
					0,
					MK_OBJTYPE_OS,
					MK_EXECBUDGET_INFINITE,
					MK_NULL,
					MK_APPL_NONE	/* OS-Application Id */
				);


/* High-priority OS thread
 * =======================
 *
 * This thread runs OS services when called from a thread whose current priority is
 * greater than the scheduler priority (for example, ISRs or threads with locked
 * interrupts)
 *
 * Stack for high-priority OS thread
*/
#if (MK_TOOL == MK_diab)
#pragma section MK_STACKS "" ".mk_stack_MK_osThreadHighStack" standard RW
#pragma use_section MK_STACKS MK_osThreadHighStack
#define MK_ATTR_SS_TH_STACK
#elif (MK_TOOL == MK_ghs)
#pragma ghs section bss=".mk_stack_MK_osThreadHighStack"
#pragma ghs section sbss=".mk_stack_MK_osThreadHighStckS"
#define MK_ATTR_SS_TH_STACK
#elif (MK_TOOL==MK_gnu)
#define MK_ATTR_SS_TH_STACK __attribute__((section(".mk_stack_MK_osThreadHighStack")))
#elif (MK_TOOL==MK_cw)
#define MK_ATTR_SS_TH_STACK
#pragma push
#pragma section ".mk_stack_MK_osThreadHighStack"
#elif (MK_TOOL==MK_tasking)
#pragma section all "mk_stack_MK_osThreadHighStack"
#define MK_ATTR_SS_TH_STACK
#elif (MK_TOOL==MK_ticgt)
#pragma DATA_SECTION(MK_osThreadHighStack, ".mk_stack_MK_osThreadHighStack")
#define MK_ATTR_SS_TH_STACK
#else
#error "Unknown or unsupported toolchain. Check your Makefiles!"
#define MK_ATTR_SS_TH_STACK
#endif

static mk_stackelement_t MK_osThreadHighStack[MK_OSHIGHSTACK_NELEMENTS] MK_ATTR_SS_TH_STACK;

#if (MK_TOOL == MK_diab)
#elif (MK_TOOL == MK_ghs)
#pragma ghs section sbss=default
#pragma ghs section bss=default
#elif (MK_TOOL==MK_gnu)
#elif (MK_TOOL==MK_cw)
#pragma pop
#elif (MK_TOOL==MK_tasking)
#pragma section all default
#elif (MK_TOOL==MK_ticgt)
#else
#error "Unknown or unsupported toolchain. Check your Makefiles!"
#endif

/* Thread structures for the high-priority OS thread
*/
mk_thread_t MK_osThreadHigh;							/* = {0} */
static mk_threadregisters_t MK_osThreadHighRegisters;	/* = {0} */

const mk_threadcfg_t MK_osHighThreadConfig =
	MK_THREADCFG(	&MK_osThreadHighRegisters,
					"os-high",
					&MK_osThreadHighStack[MK_OSHIGHSTACK_NELEMENTS-4],
					MK_NULL,
					MK_OSHIGHPM,
					MK_OSHIGHLVL,
					MK_THRIRQ_ENABLE,
					MK_THRFPU_DISABLE,
					MK_THRHWS_DEFAULT,
					MK_OSHIGHPRIO,	/* Queueing priority */
					MK_OSHIGHPRIO,	/* Running priority */
					MK_MEMPART_OSHIGH,
					0,
					MK_OBJTYPE_OS,
					MK_EXECBUDGET_INFINITE,
					MK_NULL,
					MK_APPL_NONE	/* OS-Application Id */
				);

/* Low-priority trusted function thread
 * ====================================
 *
 * This thread runs trusted functions when called from a thread whose current priority is
 * not greater than the scheduler priority.
 *
 * Stack for low-priority trusted function thread.
*/
#if (MK_TOOL == MK_diab)
#pragma section MK_STACKS "" ".mk_stack_MK_tfThreadLowStack" standard RW
#pragma use_section MK_STACKS MK_tfThreadLowStack
#define MK_ATTR_SS_TFTL_STACK
#elif (MK_TOOL == MK_ghs)
#pragma ghs section bss=".mk_stack_MK_tfThreadLowStack"
#pragma ghs section sbss=".mk_stack_MK_tfThreadLowStackS"
#define MK_ATTR_SS_TFTL_STACK
#elif (MK_TOOL==MK_gnu)
#define MK_ATTR_SS_TFTL_STACK __attribute__((section(".mk_stack_MK_tfThreadLowStack")))
#elif (MK_TOOL==MK_cw)
#pragma push
#pragma section ".mk_stack_MK_tfThreadLowStack"
#define MK_ATTR_SS_TFTL_STACK
#elif (MK_TOOL==MK_tasking)
#pragma section all "mk_stack_MK_tfThreadLowStack"
#define MK_ATTR_SS_TFTL_STACK
#elif (MK_TOOL==MK_ticgt)
#pragma DATA_SECTION(MK_tfThreadLowStack, ".mk_stack_MK_tfThreadLowStack")
#define MK_ATTR_SS_TFTL_STACK
#else
#error "Unknown or unsupported toolchain. Check your Makefiles!"
#define MK_ATTR_SS_TFTL_STACK
#endif

static mk_stackelement_t MK_tfThreadLowStack[MK_TFLOWSTACK_NELEMENTS] MK_ATTR_SS_TFTL_STACK;

#if (MK_TOOL == MK_diab)
#elif (MK_TOOL == MK_ghs)
#pragma ghs section sbss=default
#pragma ghs section bss=default
#elif (MK_TOOL==MK_gnu)
#elif (MK_TOOL==MK_cw)
#pragma pop
#elif (MK_TOOL==MK_tasking)
#pragma section all default
#elif (MK_TOOL==MK_ticgt)
#else
#error "Unknown or unsupported toolchain. Check your Makefiles!"
#endif

/* Thread structures for the low-priority trusted function thread
*/
mk_thread_t MK_tfThreadLow;							/* = {0} */
static mk_threadregisters_t MK_tfThreadLowRegisters;	/* = {0} */

const mk_threadcfg_t MK_tfLowThreadConfig =
	MK_THREADCFG(	&MK_tfThreadLowRegisters,
					"tf-low",
					&MK_tfThreadLowStack[MK_TFLOWSTACK_NELEMENTS-4],
					MK_NULL,
					MK_TFLOWPM,
					MK_HWENABLEALLLEVEL,
					MK_THRIRQ_ENABLE,
					MK_THRFPU_DISABLE,
					MK_THRHWS_DEFAULT,
					MK_TFLOWPRIO,	/* Queueing priority */
					MK_TFLOWPRIO,	/* Running priority */
					MK_MEMPART_TFLOW,
					0,
					MK_OBJTYPE_TRUSTEDFUNCTION,
					MK_EXECBUDGET_INFINITE,
					MK_NULL,
					MK_APPL_NONE	/* OS-Application Id */
				);


/* High-priority trusted function thread
 * =====================================
 *
 * This thread runs trusted functions when called from a thread whose current priority is
 * greater than the scheduler priority (for example, ISRs or threads with locked
 * interrupts)
 *
 * Stack for high-priority trusted function thread
*/
#if (MK_TOOL == MK_diab)
#pragma section MK_STACKS "" ".mk_stack_MK_tfThreadHighStack" standard RW
#pragma use_section MK_STACKS MK_tfThreadHighStack
#define MK_ATTR_SS_TFTH_STACK
#elif (MK_TOOL == MK_ghs)
#pragma ghs section bss=".mk_stack_MK_tfThreadHighStack"
#pragma ghs section sbss=".mk_stack_MK_tfThreadHighStckS"
#define MK_ATTR_SS_TFTH_STACK
#elif (MK_TOOL==MK_gnu)
#define MK_ATTR_SS_TFTH_STACK __attribute__((section(".mk_stack_MK_tfThreadHighStack")))
#elif (MK_TOOL==MK_cw)
#pragma push
#pragma section ".mk_stack_MK_tfThreadHighStack"
#define MK_ATTR_SS_TFTH_STACK
#elif (MK_TOOL==MK_tasking)
#pragma section all "mk_stack_MK_tfThreadHighStack"
#define MK_ATTR_SS_TFTH_STACK
#elif (MK_TOOL==MK_ticgt)
#pragma DATA_SECTION(MK_tfThreadHighStack, ".mk_stack_MK_tfThreadHighStack")
#define MK_ATTR_SS_TFTH_STACK
#else
#error "Unknown or unsupported toolchain. Check your Makefiles!"
#define MK_ATTR_SS_TFTH_STACK
#endif

static mk_stackelement_t MK_tfThreadHighStack[MK_TFHIGHSTACK_NELEMENTS] MK_ATTR_SS_TFTH_STACK;

#if (MK_TOOL == MK_diab)
#elif (MK_TOOL == MK_ghs)
#pragma ghs section sbss=default
#pragma ghs section bss=default
#elif (MK_TOOL==MK_gnu)
#elif (MK_TOOL==MK_cw)
#pragma pop
#elif (MK_TOOL==MK_tasking)
#pragma section all default
#elif (MK_TOOL==MK_ticgt)
#else
#error "Unknown or unsupported toolchain. Check your Makefiles!"
#endif

/* Thread structures for the high-priority trusted function thread
*/
mk_thread_t MK_tfThreadHigh;							/* = {0} */
static mk_threadregisters_t MK_tfThreadHighRegisters;	/* = {0} */

const mk_threadcfg_t MK_tfHighThreadConfig =
	MK_THREADCFG(	&MK_tfThreadHighRegisters,
					"tf-high",
					&MK_tfThreadHighStack[MK_TFHIGHSTACK_NELEMENTS-4],
					MK_NULL,
					MK_TFHIGHPM,
					MK_TFHIGHLVL,
					MK_THRIRQ_ENABLE,
					MK_THRFPU_DISABLE,
					MK_THRHWS_DEFAULT,
					MK_TFHIGHPRIO,	/* Queueing priority */
					MK_TFHIGHPRIO,	/* Running priority */
					MK_MEMPART_TFHIGH,
					0,
					MK_OBJTYPE_TRUSTEDFUNCTION,
					MK_EXECBUDGET_INFINITE,
					MK_NULL,
					MK_APPL_NONE	/* OS-Application Id */
				);



#if MK_ERRORHOOK_ENABLED
/* Error-hook thread
 * =================
 *
 * Stack for error-hook thread
*/
#if (MK_TOOL == MK_diab)
#pragma section MK_STACKS "" ".mk_stack_MK_errorHookStack" standard RW
#pragma use_section MK_STACKS MK_errorHookStack
#define MK_ATTR_SS_ERROR_HOOK_STACK
#elif (MK_TOOL == MK_ghs)
#pragma ghs section bss=".mk_stack_MK_errorHookStack"
#pragma ghs section sbss=".mk_stack_MK_errorHookStackS"
#define MK_ATTR_SS_ERROR_HOOK_STACK
#elif (MK_TOOL==MK_gnu)
#define MK_ATTR_SS_ERROR_HOOK_STACK __attribute__((section(".mk_stack_MK_errorHookStack")))
#elif (MK_TOOL==MK_cw)
#define MK_ATTR_SS_ERROR_HOOK_STACK
#pragma push
#pragma section ".mk_stack_MK_errorHookStack"
#elif (MK_TOOL==MK_tasking)
#pragma section all "mk_stack_MK_errorHookStack"
#define MK_ATTR_SS_ERROR_HOOK_STACK
#elif (MK_TOOL==MK_ticgt)
#pragma DATA_SECTION(MK_errorHookStack, ".mk_stack_MK_errorHookStack")
#define MK_ATTR_SS_ERROR_HOOK_STACK
#else
#error "Unknown or unsupported toolchain. Check your Makefiles!"
#define MK_ATTR_SS_ERROR_HOOK_STACK
#endif

static mk_stackelement_t MK_errorHookStack[MK_ERRORHOOKSTACK_NELEMENTS] MK_ATTR_SS_ERROR_HOOK_STACK;

#if (MK_TOOL == MK_diab)
#elif (MK_TOOL == MK_ghs)
#pragma ghs section sbss=default
#pragma ghs section bss=default
#elif (MK_TOOL==MK_gnu)
#elif (MK_TOOL==MK_cw)
#pragma pop
#elif (MK_TOOL==MK_tasking)
#pragma section all default
#elif (MK_TOOL==MK_ticgt)
#else
#error "Unknown or unsupported toolchain. Check your Makefiles!"
#endif

/* Thread structures for the error-hook thread
 * !LINKSTO    Microkernel.Thread.Hook.Config, 1,
 * !           Microkernel.Thread.ErrorHook.Priority.Running, 1
 * !doctype src
*/
static mk_thread_t MK_eHookthread;
static mk_threadregisters_t MK_errorHookThreadRegisters;

const mk_threadcfg_t MK_eHookThreadConfig =
	MK_THREADCFG(	&MK_errorHookThreadRegisters,
					"error-hook",
					&MK_errorHookStack[MK_ERRORHOOKSTACK_NELEMENTS-4],
					ErrorHook,
					MK_ERRORHOOKPM,
					MK_ERRORHOOKLVL,
					MK_THRIRQ_ENABLE,
					MK_THRFPU_DISABLE,
					MK_THRHWS_DEFAULT,
					MK_ERRORHOOKPRIO, /*	Queueing priority */
					MK_ERRORHOOKPRIO, /*	Running priority */
					MK_MEMPART_ERRORHOOK,
					MK_OBJID_GLOBAL,
					MK_OBJTYPE_ERRORHOOK,
					MK_EXECBUDGET_INFINITE,
					MK_NULL,
					MK_APPL_NONE	/* OS-Application Id */
				);
/*
 * !LINKSTO        Microkernel.Thread.ErrorHook.Enabled,1
 * !description    The error hook thread instance is set appropriately.
 * !doctype        src
 */
mk_thread_t * const MK_errorHookThread = &MK_eHookthread;
const mk_threadcfg_t * const MK_errorHookThreadConfig = &MK_eHookThreadConfig;

#else

mk_thread_t * const MK_errorHookThread = MK_NULL;
const mk_threadcfg_t * const MK_errorHookThreadConfig = MK_NULL;

#endif

#if MK_ERRORINFO_ENABLED
/* Error information to be made available to the user
*/
static mk_errorinfo_t MK_errinfo =
{
	MK_sid_UnknownService,
	MK_eid_NoError,
	MK_E_OK,
	0,
	MK_OBJTYPE_UNKNOWN,
	MK_NULL,
	MK_NULL,
	{ 0, 0, 0, 0 }
};
mk_errorinfo_t * const MK_errorInfo = &MK_errinfo;
#else
mk_errorinfo_t * const MK_errorInfo = MK_NULL;
#endif


#if MK_PROTECTIONHOOK_ENABLED
/* Protection-hook thread
 * ======================
 *
 * Stack for protection-hook thread.
*/
#if (MK_TOOL == MK_diab)
#pragma section MK_STACKS "" ".mk_stack_MK_protectionHookStack" standard RW
#pragma use_section MK_STACKS MK_protectionHookStack
#define MK_ATTR_SS_PROT_HOOK_STACK
#elif (MK_TOOL == MK_ghs)
#pragma ghs section bss=".mk_stack_protectionHook"
#pragma ghs section sbss=".mk_stack_protectionHookS"
#define MK_ATTR_SS_PROT_HOOK_STACK
#elif (MK_TOOL==MK_gnu)
#define MK_ATTR_SS_PROT_HOOK_STACK __attribute__((section(".mk_stack_MK_protectionHookStack")))
#elif (MK_TOOL==MK_cw)
#define MK_ATTR_SS_PROT_HOOK_STACK
#pragma push
#pragma section ".mk_stack_MK_protectionHookStack"
#elif (MK_TOOL==MK_tasking)
#pragma section all "mk_stack_MK_protectionHookStack"
#define MK_ATTR_SS_PROT_HOOK_STACK
#elif (MK_TOOL==MK_ticgt)
#pragma DATA_SECTION(MK_protectionHookStack, ".mk_stack_MK_protectionHookStack")
#define MK_ATTR_SS_PROT_HOOK_STACK
#else
#error "Unknown or unsupported toolchain. Check your Makefiles!"
#define MK_ATTR_SS_PROT_HOOK_STACK
#endif

static mk_stackelement_t MK_protectionHookStack[MK_PROTECTIONHOOKSTACK_NELEMENTS] MK_ATTR_SS_PROT_HOOK_STACK;

#if (MK_TOOL == MK_diab)
#elif (MK_TOOL == MK_ghs)
#pragma ghs section sbss=default
#pragma ghs section bss=default
#elif (MK_TOOL==MK_gnu)
#elif (MK_TOOL==MK_cw)
#pragma pop
#elif (MK_TOOL==MK_tasking)
#pragma section all default
#elif (MK_TOOL==MK_ticgt)
#else
#error "Unknown or unsupported toolchain. Check your Makefiles!"
#endif

/* Thread structures for the protection-hook thread
 * !LINKSTO	Microkernel.Thread.ProtectionHook.Priority.Running, 1,
 * !		Microkernel.Thread.Hook.Config, 1
 * !doctype src
 * !doctype src
*/
static mk_thread_t MK_pHookthread;
static mk_threadregisters_t MK_protectionHookThreadRegisters;

const mk_threadcfg_t MK_pHookThreadConfig =
	MK_THREADCFG(	&MK_protectionHookThreadRegisters,
					"protection-hook",
					&MK_protectionHookStack[MK_PROTECTIONHOOKSTACK_NELEMENTS-4],
					ProtectionHook,
					MK_PROTECTIONHOOKPM,
					MK_PROTECTIONHOOKLVL,
					MK_THRIRQ_ENABLE,
					MK_THRFPU_DISABLE,
					MK_THRHWS_DEFAULT,
					MK_PROTECTIONHOOKPRIO,	/* Queueing priority */
					MK_PROTECTIONHOOKPRIO,	/* Running priority */
					MK_MEMPART_PROTECTIONHOOK,
					MK_OBJID_GLOBAL,
					MK_OBJTYPE_PROTECTIONHOOK,
					MK_EXECBUDGET_INFINITE,
					MK_NULL,
					MK_APPL_NONE	/* OS-Application Id */
				);

/*
 * !LINKSTO        Microkernel.Thread.ProtectionHook.Enabled,1
 * !description    The protection hook thread instance is set appropriately.
 * !doctype        src
 */
mk_thread_t * const MK_protectionHookThread = &MK_pHookthread;
const mk_threadcfg_t * const MK_protectionHookThreadConfig = &MK_pHookThreadConfig;

#else

mk_thread_t * const MK_protectionHookThread = MK_NULL;
const mk_threadcfg_t * const MK_protectionHookThreadConfig = MK_NULL;

#endif

#if MK_PROTECTIONINFO_ENABLED
/* Protection-fault information to be made available to the user
*/
static mk_protectioninfo_t MK_protinfo =
{
	MK_sid_UnknownService,
	MK_eid_NoError,
	MK_E_OK,
	MK_NULL
};
mk_protectioninfo_t * const MK_protectionInfo = &MK_protinfo;
#else
mk_protectioninfo_t * const MK_protectionInfo = MK_NULL;
#endif

#if MK_HWHASEXCEPTIONINFO
/* The exception info structure is only available if the hardware actually has one.
*/
static mk_hwexceptioninfo_t MK_exinfo;
mk_hwexceptioninfo_t * const MK_exceptionInfo = &MK_exinfo;
#endif


/* Boot thread
 * ===========
 *
 * This is the configuration for the dummy thread that is assumed to be already running when
 * the kernel starts.
 * Many of the fields are irrelevant because this thread never actually starts as a thread.
 * However, the priorities are important to ensure that it stays at the head of the queue
 * until it terminates.
*/
const mk_threadcfg_t MK_bootThreadConfig =
	MK_THREADCFG(	&MK_osThreadHighRegisters,
					"mk-boot",
					&MK_osThreadHighStack[MK_OSHIGHSTACK_NELEMENTS-4],
					MK_NULL,
					MK_THRMODE_SUPER,
					MK_HWDISABLEALLLEVEL,
					MK_THRIRQ_DISABLE,
					MK_THRFPU_DISABLE,
					MK_THRHWS_DEFAULT,
					MK_INITPRIO+1,	/* Queueing priority */
					MK_INITPRIO+1,	/* Running priority */
					-1,
					MK_OBJID_BOOT,
					MK_OBJTYPE_KERNEL,
					MK_EXECBUDGET_INFINITE,
					MK_NULL,
					MK_APPL_NONE	/* OS-Application Id */
				);


/* Init thread
 * ===========
 *
 * !LINKSTO        Microkernel.Start.MK_StartKernel.Main,1
 * !description    This is the configuration for the thread that is automatically
 * !               started to bring up the system. Typically, it runs main()
 * !               and uses MK_INITFUNCTION, MK_INITPSW and MK_INITLEVEL that must be
 * !               provided by the configuration.
 * !doctype        src
*/
/* Deviation MISRA-1 <+5> */
const mk_threadcfg_t MK_initThreadConfig =
	MK_THREADCFG(	&MK_osThreadLowRegisters,
					"main",
					&MK_osThreadLowStack[MK_OSLOWSTACK_NELEMENTS-4],
					MK_INITFUNCTION,
					MK_INITPM,
					MK_HWDISABLEALLLEVEL,
					MK_THRIRQ_DISABLE,
					MK_THRFPU_DISABLE,
					MK_THRHWS_DEFAULT,
					MK_INITPRIO,	/* Queueing priority */
					MK_INITPRIO,	/* Running priority */
					MK_MEMPART_INIT,
					MK_OBJID_MAIN,
					MK_OBJTYPE_KERNEL,
					MK_EXECBUDGET_INFINITE,
					MK_NULL,
					MK_APPL_NONE	/* OS-Application Id */
				);

#if MK_STARTUPHOOK_ENABLED || MK_SHUTDOWNHOOK_ENABLED
/* Startup- and shutdown-hook threads
 * ==================================
 *
 * Stack for startup- and shutdown-hook threads, collectively known here as "sysctrl-hooks".
 * First we need to determine the size.
*/
#if MK_STARTUPHOOK_ENABLED

#if MK_SHUTDOWNHOOK_ENABLED
/* Both startup- and shutdown-hooks are enabled. Use the larger stack.
*/
#if MK_STARTUPHOOKSTACK_NELEMENTS > MK_SHUTDOWNHOOKSTACK_NELEMENTS
#define MK_SYSCTRLHOOKSTACK_NELEMENTS	MK_STARTUPHOOKSTACK_NELEMENTS
#else
#define MK_SYSCTRLHOOKSTACK_NELEMENTS	MK_SHUTDOWNHOOKSTACK_NELEMENTS
#endif

#else
/* No shutdown hook; use size of startup-hook stack
*/
#define MK_SYSCTRLHOOKSTACK_NELEMENTS	MK_STARTUPHOOKSTACK_NELEMENTS
#endif

#else
/* No startup hook: use size of shutdown hook stack
*/
#define MK_SYSCTRLHOOKSTACK_NELEMENTS	MK_SHUTDOWNHOOKSTACK_NELEMENTS
#endif

#if (MK_TOOL == MK_diab)
#pragma section MK_STACKS "" ".mk_stack_MK_sysctrlHookStack" standard RW
#pragma use_section MK_STACKS MK_sysctrlHookStack
#define MK_ATTR_SS_SYSCTRL_HOOK_STACK
#elif (MK_TOOL == MK_ghs)
#pragma ghs section bss=".mk_stack_MK_sysctrlHookStack"
#pragma ghs section sbss=".mk_stack_MK_sysctrlHookStackS"
#define MK_ATTR_SS_SYSCTRL_HOOK_STACK
#elif (MK_TOOL==MK_gnu)
#define MK_ATTR_SS_SYSCTRL_HOOK_STACK __attribute__((section(".mk_stack_MK_sysctrlHookStack")))
#elif (MK_TOOL==MK_cw)
#define MK_ATTR_SS_SYSCTRL_HOOK_STACK
#pragma push
#pragma section ".mk_stack_MK_sysctrlHookStack"
#elif (MK_TOOL==MK_tasking)
#pragma section all "mk_stack_MK_sysctrlHookStack"
#define MK_ATTR_SS_SYSCTRL_HOOK_STACK
#elif (MK_TOOL==MK_ticgt)
#pragma DATA_SECTION(MK_sysctrlHookStack, ".mk_stack_MK_sysctrlHookStack")
#define MK_ATTR_SS_SYSCTRL_HOOK_STACK
#else
#error "Unknown or unsupported toolchain. Check your Makefiles!"
#endif


static mk_stackelement_t MK_sysctrlHookStack[MK_SYSCTRLHOOKSTACK_NELEMENTS] MK_ATTR_SS_SYSCTRL_HOOK_STACK;

#if (MK_TOOL == MK_diab)
#elif (MK_TOOL == MK_ghs)
#pragma ghs section sbss=default
#pragma ghs section bss=default
#elif (MK_TOOL==MK_gnu)
#elif (MK_TOOL==MK_cw)
#pragma pop
#elif (MK_TOOL==MK_tasking)
#pragma section all default
#elif (MK_TOOL==MK_ticgt)
#else
#error "Unknown or unsupported toolchain. Check your Makefiles!"
#endif

#endif

#if MK_STARTUPHOOK_ENABLED

/* Thread structures for the startup-hook thread
 * !LINKSTO Microkernel.Thread.Hook.Config, 1
 * !doctype src
*/
const mk_threadcfg_t MK_suHookThreadConfig =
	MK_THREADCFG(	&MK_osThreadLowRegisters,
					"startup-hook",
					&MK_sysctrlHookStack[MK_SYSCTRLHOOKSTACK_NELEMENTS-4],
					StartupHook,
					MK_STARTUPHOOKPM,
					MK_STARTUPHOOKLVL,
					MK_THRIRQ_ENABLE,
					MK_THRFPU_DISABLE,
					MK_THRHWS_DEFAULT,
					MK_STARTUPHOOKQPRIO,
					MK_STARTUPHOOKRPRIO,
					MK_MEMPART_STARTUPHOOK,
					MK_OBJID_GLOBAL,
					MK_OBJTYPE_STARTUPHOOK,
					MK_EXECBUDGET_INFINITE,
					MK_NULL,
					MK_APPL_NONE	/* OS-Application Id */
				);

/*
 * !LINKSTO        Microkernel.Thread.Startup.StartOS.Hook.Enabled,1
 * !description    The startup hook thread instance is set appropriately.
 * !doctype        src
 */
const mk_threadcfg_t * const MK_startupHookThreadConfig = &MK_suHookThreadConfig;

#else

const mk_threadcfg_t * const MK_startupHookThreadConfig = MK_NULL;

#endif

#if MK_SHUTDOWNHOOK_ENABLED

/* Thread structures for the shutdown-hook thread
 * !LINKSTO Microkernel.Thread.Hook.Config, 1
 * !doctype src
*/
const mk_threadcfg_t MK_sdHookThreadConfig =
	MK_THREADCFG(	&MK_osThreadHighRegisters,
					"shutdown-hook",
					&MK_sysctrlHookStack[MK_SYSCTRLHOOKSTACK_NELEMENTS-4],
					ShutdownHook,
					MK_SHUTDOWNHOOKPM,
					MK_SHUTDOWNHOOKLVL,
					MK_THRIRQ_ENABLE,
					MK_THRFPU_DISABLE,
					MK_THRHWS_DEFAULT,
					MK_SHUTDOWNHOOKPRIO,	/* Queueing priority */
					MK_SHUTDOWNHOOKPRIO,	/* Running priority */
					MK_MEMPART_SHUTDOWNHOOK,
					MK_OBJID_GLOBAL,
					MK_OBJTYPE_SHUTDOWNHOOK,
					MK_EXECBUDGET_INFINITE,
					MK_NULL,
					MK_APPL_NONE	/* OS-Application Id */
				);

const mk_threadcfg_t * const MK_shutdownHookThreadConfig = &MK_sdHookThreadConfig;
mk_thread_t * const MK_shutdownHookThread = &MK_osThreadHigh;

#else

const mk_threadcfg_t * const MK_shutdownHookThreadConfig = MK_NULL;
mk_thread_t * const MK_shutdownHookThread = MK_NULL;

#endif

/* Selftest thread configuration
 * =========================
 *
 * This is the configuration for the thread run by the kernel's selftest function.
 *
 * Stack for selftest thread
*/

#if (MK_TOOL == MK_diab)
#pragma section MK_STACKS "" ".mk_stack_MK_selftestStack" standard RW
#pragma use_section MK_STACKS MK_selftestStack
#define MK_ATTR_SS_SELFTEST_STACK
#elif (MK_TOOL == MK_ghs)
#pragma ghs section bss=".mk_stack_MK_selftestStack"
#pragma ghs section sbss=".mk_stack_MK_selftestStackS"
#define MK_ATTR_SS_SELFTEST_STACK
#elif (MK_TOOL==MK_gnu)
#define MK_ATTR_SS_SELFTEST_STACK __attribute__((section(".mk_stack_MK_selftestStack")))
#elif (MK_TOOL==MK_cw)
#define MK_ATTR_SS_SELFTEST_STACK
#pragma push
#pragma section ".mk_stack_MK_selftestStack"
#elif (MK_TOOL==MK_tasking)
#pragma section all "mk_stack_MK_selftestStack"
#define MK_ATTR_SS_SELFTEST_STACK
#elif (MK_TOOL==MK_ticgt)
#pragma DATA_SECTION(MK_selftestStack, ".mk_stack_MK_selftestStack")
#define MK_ATTR_SS_SELFTEST_STACK
#else
#error "Unknown or unsupported toolchain. Check your Makefiles!"
#define MK_ATTR_SS_SELFTEST_STACK
#endif

static mk_stackelement_t MK_selftestStack[MK_SELFTESTSTACK_NELEMENTS] MK_ATTR_SS_SELFTEST_STACK;

#if (MK_TOOL == MK_diab)
#elif (MK_TOOL == MK_ghs)
#pragma ghs section sbss=default
#pragma ghs section bss=default
#elif (MK_TOOL==MK_gnu)
#elif (MK_TOOL==MK_cw)
#pragma pop
#elif (MK_TOOL==MK_tasking)
#pragma section all default
#elif (MK_TOOL==MK_ticgt)
#else
#error "Unknown or unsupported toolchain. Check your Makefiles!"
#endif

/* Thread structures for the selftest thread.
*/
const mk_threadcfg_t MK_selftestThreadConfig =
	MK_THREADCFG(	&MK_osThreadHighRegisters,
					"mk-selftest",
					&MK_selftestStack[MK_SELFTESTSTACK_NELEMENTS-4],
					&MK_Idle,
					MK_SELFTESTPM,
					MK_HWENABLEALLLEVEL,
					MK_THRIRQ_ENABLE,
					MK_THRFPU_DISABLE,
					MK_THRHWS_DEFAULT,
					MK_SELFTESTPRIO,	/* Queueing priority */
					MK_SELFTESTPRIO,	/* Running priority  */
					MK_MEMPART_SELFTEST,
					MK_OBJID_SELFTEST,
					MK_OBJTYPE_KERNEL,
					(((MK_MIN_EXECUTIONTIME)+1)*2),
					MK_NULL,
					MK_APPL_NONE	/* OS-Application Id */
				);

/* Timestamp clock factors for the 1u, 10u and 100u conversion functions.
*/
#if MK_HAS_TIMESTAMPCLOCKFACTOR100U
const mk_uint16_t MK_timestampClockFactor100u = MK_TIMESTAMPCLOCKFACTOR100U;
#endif

#if MK_HAS_TIMESTAMPCLOCKFACTOR10U
const mk_uint16_t MK_timestampClockFactor10u = MK_TIMESTAMPCLOCKFACTOR10U;
#endif

#if MK_HAS_TIMESTAMPCLOCKFACTOR1U
const mk_uint16_t MK_timestampClockFactor1u = MK_TIMESTAMPCLOCKFACTOR1U;
#endif

#if MK_HAS_USERPANICSTOP
#define MK_CALLOUTSTARTUPPANIC	MK_USERPANICSTOP
#else
#define MK_CALLOUTSTARTUPPANIC	MK_NULL
#endif

const mk_paniccallout_t MK_startupPanicCallout = MK_CALLOUTSTARTUPPANIC;

/* Editor settings; DO NOT DELETE
 * vi:set ts=4:
*/
