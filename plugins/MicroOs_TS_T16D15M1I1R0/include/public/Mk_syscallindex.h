/* Mk_syscallindex.h
 *
 * This file defines the system call indexes used by the kernel.
 *
 * (c) Elektrobit Automotive GmbH
 *
 * $Id: Mk_syscallindex.h 15851 2014-04-17 10:21:10Z dh $
 *
 * !LINKSTO		Microkernel.SystemCall.HeaderFile,1
 * !doctype		src
*/
#ifndef MK_SYSCALLINDEX_H
#define MK_SYSCALLINDEX_H

/* Microkernel system call indexes. These indexes MUST match the positions of the corresponding
 * functions in the system call table.
 * We need these indexes in assembly-language so they must be #defines
 *
 * The first batch contains the indexes for microkernel services.
*/
/*		1234567890123456789012345678901	*/
#define MK_SC_TerminateSelf				0	/* MK_SysTerminateSelf */
#define MK_SC_ActivateTask				1	/* MK_SysActivateTask */
#define MK_SC_ChainTask					2	/* MK_SysChainTask */
#define MK_SC_Schedule					3	/* MK_SysSchedule */
#define MK_SC_GetResource				4	/* MK_SysGetResource */
#define MK_SC_ReleaseResource			5	/* MK_SysReleaseResource */
#define MK_SC_Shutdown					6	/* MK_SysShutdown */
#define MK_SC_StartOs					7	/* MK_SysStartOs */
#define MK_SC_SetEvent					8	/* MK_SysSetEvent */
#define MK_SC_ClearEvent				9	/* MK_SysClearEvent */
#define MK_SC_WaitEvent					10	/* MK_SysWaitEvent */
#define MK_SC_WaitGetClearEvent			11	/* MK_SysWaitEvent (alias) */
#define MK_SC_GetTaskId					12	/* MK_SysGetTaskId */
#define MK_SC_GetTaskState				13	/* MK_SysGetTaskState */
#define MK_SC_GetIsrId					14	/* MK_SysGetIsrId */
#define MK_SC_ReportError				15	/* MK_SysReportError */
#define MK_SC_StartForeignThread		16	/* MK_SysStartForeignThread */
#define MK_SC_GetApplicationId			17	/* MK_SysGetApplicationId */
#define MK_SC_TerminateApplication		18	/* MK_SysGetApplicationId */
#define MK_SC_Selftest					19	/* MK_SysSelftest */
#define MK_SC_EnableInterruptSource		20	/* MK_SysEnableInterruptSource */
#define MK_SC_DisableInterruptSource	21	/* MK_SysDisableInterruptSource */
#define MK_SC_InitTicker				22	/* MK_SysInitTicker */

/* !LINKSTO Microkernel.SystemCall.IndexRange, 1
 * !doctype src
*/
#define MK_NSYSCALL_MK					23	/* Number of direct microkernel system calls */

/* This batch contains the system call indexes for services that are
 * delegated to the QM-OS
*/
#define MK_OS_SC(n)						(MK_NSYSCALL_MK+(n))
#define MK_SC_SetRelAlarm				MK_OS_SC(0)
#define MK_SC_SetAbsAlarm				MK_OS_SC(1)
#define MK_SC_CancelAlarm				MK_OS_SC(2)
#define MK_SC_IncrementCounter			MK_OS_SC(3)
#define MK_SC_StartScheduleTable		MK_OS_SC(4)
#define MK_SC_StartScheduleTableSync	MK_OS_SC(5)
#define MK_SC_NextScheduleTable			MK_OS_SC(6)
#define MK_SC_StopScheduleTable			MK_OS_SC(7)
#define MK_SC_SyncScheduleTable			MK_OS_SC(8)
#define MK_SC_SetScheduleTableAsync		MK_OS_SC(9)

#define MK_SC_SimTimerAdvance			MK_OS_SC(10)
#define MK_SC_GetCounterValue			MK_OS_SC(11)
#define MK_SC_GetAlarm					MK_OS_SC(12)

#define MK_NSYSCALL						MK_OS_SC(13)		/* The maximum accessible via system calls */

/* These "QM-OS calls" are for internal use and cannot be accessed through a system call
 * However, we need them here for the OS service IDs.
*/
#define MK_SC_INTERNAL(x)				(MK_NSYSCALL+(x))

#define MK_SC_OS_TerminateApplication	MK_SC_INTERNAL(0)
#define MK_SC_OS_StartOs				MK_SC_INTERNAL(1)
#define MK_NSYSCALL_TOTAL				MK_SC_INTERNAL(2)

/* Symbolic names for the first and last QM-OS function
*/
#define MK_FIRST_QMOS_FUNCTION			MK_OS_SC(0)
#define MK_LAST_QMOS_FUNCTION			(MK_NSYSCALL_TOTAL-1)

/* Index range for trusted functions starts after QM-OS functions
*/
#define MK_TRUSTEDFUNCTIONS_MIN			MK_NSYSCALL_TOTAL

/* MK_TRUSTEDFUNCTIONS_MAX is defined in Mk_foreign.h
*/

#endif
/* Editor settings; DO NOT DELETE
 * vi:set ts=4:
*/
