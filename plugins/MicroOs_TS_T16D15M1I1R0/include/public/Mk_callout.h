/* Mk_callout.h
 *
 * This header file defines the AUTOSAR standard callout (hook) functions.
 *
 * (c) Elektrobit Automotive GmbH
 *
 * $Id: Mk_callout.h 15821 2014-04-16 04:55:15Z masa8317 $
*/
#ifndef MK_CALLOUT_H
#define MK_CALLOUT_H

#include <public/Mk_public_types.h>
#include <public/Mk_autosar.h>

#ifndef MK_ASM
/* These are the global hook functions that are currently supported.
*/
void ErrorHook(StatusType);
ProtectionReturnType ProtectionHook(StatusType);

void StartupHook(void);
void ShutdownHook(StatusType);
#endif

#endif
/* Editor settings - DO NOT DELETE
 * vi:set ts=4:
*/
