/* Mk_public_types.h
 *
 * This file declares the public data types used by the kernel.
 * Public types are always visible to the user.
 *
 * (c) Elektrobit Automotive GmbH
 *
 * $Id: Mk_public_types.h 17778 2014-12-02 12:34:49Z masa8317 $
*/

/* MISRA-C:2004 Deviation List
 *
 * MISRA-1) Deviated Rule: 18.1 (required)
 *  All structure or union types shall be complete at the end of a translation unit.
 *
 * Reason:
 *  Information hiding. The structure should be visible in a debugger but not in the application source code.
 *
 * MISRA-2) Deviated Rule: 19.10 (required)
 *  Parameter instance shall be enclosed in parentheses.
 *
 * Reason:
 *  The macro parameters of MK_OFFSETOF denote a type name and a structure member name.
 *  Parentheses are not applicable in these cases.
 *
 * MISRA-3) Deviated Rule: 5.4 (required)
 *  A tag name shall be a unique identifier.
 *
 * Reason:
 *  False positive: struct is legally used in type definition.
*/
#ifndef MK_PUBLIC_TYPES_H
#define MK_PUBLIC_TYPES_H

#include <public/Mk_basic_types.h>
#include <public/Mk_misra.h>

/* MK_NULL
 *
 * A null pointer constant
*/
#define MK_NULL		0

/* mk_boolean_t
 *
 * A boolean type with values MK_TRUE and MK_FALSE.
 * For use where efficiency is more important than size.
 * Not for use in data structures where size is important.
*/
#define MK_FALSE	0
#define MK_TRUE		1

#ifndef MK_ASM
typedef mk_int_fast8_t	mk_boolean_t;
#endif

/* mk_parametertype_t
 *
 * A scalar type that's large enough to hold a system-service parameter or a result code.
*/
#ifndef MK_ASM
typedef mk_address_t	mk_parametertype_t;
#endif

#define MK_PARAMETERTYPE_INVALID 0xffffffffu

/* mk_statusandvalue_t
 *
 * A structure type that holds a dual return value (status code and requested value)
*/
#ifndef MK_ASM
typedef struct mk_statusandvalue_s mk_statusandvalue_t;

struct mk_statusandvalue_s
{
	mk_parametertype_t statusCode;
	mk_parametertype_t requestedValue;
};
#endif

/* mk_reg8_t, mk_reg16_t, mk_reg32_t
 *
 * Types for hardware registers; volatile uint types.
*/
#ifndef MK_ASM
typedef volatile mk_uint8_t mk_reg8_t;
typedef volatile mk_uint16_t mk_reg16_t;
typedef volatile mk_uint32_t mk_reg32_t;
#endif

/* Task states. One of these values is returned by GetTaskState(). Casting to the appropriate
 * type in the debugger will reveal the symbol
*/
#ifndef MK_ASM
typedef enum mk_taskstate_e
{
	MK_TS_INVALID,
	SUSPENDED,
	READY,
	RUNNING,
	WAITING,
	MK_TS_QUARANTINED
} mk_taskstate_t;
#endif

/* Selftest types. Used as parameter of MK_UsrSelftest.
*/
#ifndef MK_ASM
typedef enum mk_selftest_e
{
	MK_SELFTEST_EXECBUDGET,
	MK_SELFTEST_STATE,
	MK_SELFTEST_MPUCONFIG,
	MK_SELFTEST_INTERRUPTCONTROLLER,
	MK_SELFTEST_INVALID
} mk_selftest_t;
#endif

/* Bitmask for selftest state.
*/
#define MK_SELFTEST_STATE_EXECBUDGET MK_U(1)

/* Object ID type. Used for TaskType etc. and internally for things like partition indexes.
 *
 * The object id is signed for two reasons:
 * - permit the use of (-1) for unknown/invalid etc.
 * - reduce the amount of MISRA-mandated typecasting and suffixes
 *
 * Object quantity is simply an integer large enough to hold the number of any type of object.
 *
 * Object type. Used publicly in the error and protection information structures to identify
 * the object type (task, isr etc.)
*/
#ifndef MK_ASM
typedef mk_int_fast16_t	mk_objectid_t;
typedef mk_int_fast16_t mk_objquantity_t;

enum mk_objecttype_e
{
	MK_OBJTYPE_KERNEL,			/* Objects belonging to the microkernel */
	MK_OBJTYPE_OS,				/* Objects belonging to the QM-OS */
	MK_OBJTYPE_TASK,			/* Task objects belonging to the user */
	MK_OBJTYPE_ISR,				/* ISR objects belonging to the user */
	MK_OBJTYPE_TICKER,			/* Task-tickers belonging to the microkernel */
	MK_OBJTYPE_STARTUPHOOK,		/* A startup-hook */
	MK_OBJTYPE_SHUTDOWNHOOK,	/* A shutdown-hook */
	MK_OBJTYPE_ERRORHOOK,		/* An error-hook */
	MK_OBJTYPE_PROTECTIONHOOK,	/* The protection-hook */
	MK_OBJTYPE_TRUSTEDFUNCTION,	/* A trusted function */
	MK_OBJTYPE_UNKNOWN			/* Must be last */
};

typedef enum mk_objecttype_e mk_objecttype_t;
#endif

#define MK_OBJECTID_INVALID	(-1)

#ifndef MK_ASM
/* mk_appstatetype_e provides an enumeration type containing all states an OS-Application can be in
 * Used to quarantine OS-Applications.
*/
enum mk_appstate_e
{
	APPLICATION_ACCESSIBLE,
	APPLICATION_RESTARTING,
	APPLICATION_TERMINATED
};
typedef enum mk_appstate_e mk_appstate_t;
#endif


/* Invalid task and ISR IDs.
*/
#define MK_INVALID_TASK		MK_OBJECTID_INVALID
#define MK_INVALID_ISR		MK_OBJECTID_INVALID
#define MK_APPL_NONE		MK_OBJECTID_INVALID

/* Incomplete definitions of the thread structure.
 * This is needed:
 *  - for the cross-referencing between structures
 *  - for public error handling structures (to make objects visible in the debugger without exposing their internals)
*/
#ifndef MK_ASM
/* Deviation MISRA-1, MISRA-3 */
typedef struct mk_thread_s mk_thread_t;
/* Note: MISRA-C:2012 (draft) Dir 4.8 advises that incomplete declarations should be used where possible
*/
#endif

/* MK_OFFSETOF(type, member) - determine the offset of structure member "member" of type "type".
 *
 * MISRA note:
 * The 0 cast to a pointer isn't a NULL dereference, since we only take the address and then cast
 * it to mk_address_t.
 */
/* Deviation MISRA-2 */
#define MK_OFFSETOF(type, member)		((mk_address_t)&((type *)0)->member)

/* mk_tick_t and mk_time_t
 *
 * These data types are used by the timestamp functions and other timing features provided by the microkernel.
 *
 * mk_tick_t is a 32-bit unsigned variable that can be used for short intervals
 * mk_time_t is a 64-bit unsigned variable (implemented using a struct on 32-bit processors). It can be used for
 *           absolute time and long intervals.
 * MK_MAXTICK is the largest value an mk_tick_t can hold. It is sometimes used to represent "infinite"
*/
#define MK_MAXTICK		0xffffffffu

#ifndef MK_ASM

typedef mk_uint32_t mk_tick_t;

/* mk_time_t for 32-bit processors.
 * Note that the order of the words follows the endianness of the hardware.
*/
typedef struct mk_time_s mk_time_t;

struct mk_time_s
{
#if MK_ENDIAN == MK_BIGENDIAN
	mk_uint32_t timeHi;
	mk_uint32_t timeLo;
#else
	mk_uint32_t timeLo;
	mk_uint32_t timeHi;
#endif
};

#endif


#endif
/* Editor settings; DO NOT DELETE
 * vi:set ts=4:
*/
