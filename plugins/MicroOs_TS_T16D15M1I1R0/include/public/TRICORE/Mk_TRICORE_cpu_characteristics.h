/* Mk_TRICORE_cpu_characteristics.h - Tricore CPU header file
 *
 * (c) Elektrobit Automotive GmbH
 *
 * $Id: Mk_TRICORE_cpu_characteristics.h 17613 2014-10-31 12:14:36Z thdr9337 $
*/
#ifndef MK_TRICORE_CPU_CHARACTERISTICS_H
#define MK_TRICORE_CPU_CHARACTERISTICS_H

#include <public/Mk_defs.h>
#include <public/TRICORE/Mk_TRICORE_defs.h>

/* Include the appropriate header file for the derivative.
*/
#if (MK_CPU == MK_TC277)
#include <public/TRICORE/TC277/Mk_TC277.h>
#elif (MK_CPU == MK_TC23XL)
#include <public/TRICORE/TC23XL/Mk_TC23XL.h>
#elif (MK_CPU == MK_TC1798)
#include <public/TRICORE/TC1798/Mk_TC1798.h>
#elif (MK_CPU == MK_TC22XL)
#include <public/TRICORE/TC22XL/Mk_TC22XL.h>
#else
#error "MK_CPU is not properly defined. Check your makefiles!"
#endif

#endif
/* Editor settings; DO NOT DELETE
 * vi:set ts=4:
*/
