/* Mk_TRICORE_hwsel.h
 *
 * Definitions for TRICORE-specific header file selection.
 *
 * (c) Elektrobit Automotive GmbH
 *
 * $Id: Mk_TRICORE_hwsel.h 15947 2014-04-28 10:16:48Z masa8317 $
*/

/* MISRA-C:2004 Deviation List
 *
 * MISRA-1) Deviated Rule: 19.4 (required)
 *  C macros shall only expand to as braced initialiser, a constant, a parenthesised expression, a type qualifier,
 *  a storage class specifier, or a do-while-zero construct.
 *
 * Reason:
 *  Each of these macros expands to an include file specification, including the < and > delimiters. Such a
 *  construct ought to be permitted according to rule 19.3. The corresponding construct using " as a delimiter
 *  is permitted by accident because it appears to be a constant string, although semantically it is no such
 *  thing. However, an include search that starts in the current directory is not desirable.
*/

#ifndef MK_TRICORE_HWSEL_H
#define MK_TRICORE_HWSEL_H

/* Deviation MISRA-1 <START> */
#define MK_HWSEL_PUB_API					<public/TRICORE/Mk_TRICORE_api.h>
#define MK_HWSEL_PUB_CHARACTERISTICS		<public/TRICORE/Mk_TRICORE_characteristics.h>
#define MK_HWSEL_PUB_EXCEPTIONINFO			<public/TRICORE/Mk_TRICORE_exceptioninfo.h>

#define MK_HWSEL_PRV_ASM					<private/TRICORE/Mk_TRICORE_asm.h>
#define MK_HWSEL_PRV_CORE					<private/TRICORE/Mk_TRICORE_core.h>
#define MK_HWSEL_PRV_EXCEPTIONHANDLING		<private/TRICORE/Mk_TRICORE_exceptionhandling.h>
#define MK_HWSEL_PRV_INTERRUPTCONTROLLER	<private/TRICORE/Mk_TRICORE_interruptcontroller.h>
#define MK_HWSEL_PRV_MEMORYPROTECTION		<private/TRICORE/Mk_TRICORE_memoryprotection.h>
#define MK_HWSEL_PRV_STARTUP				<private/TRICORE/Mk_TRICORE_startup.h>
#define MK_HWSEL_PRV_THREAD					<private/TRICORE/Mk_TRICORE_thread.h>
#define MK_HWSEL_PRV_ACCOUNTING				<private/TRICORE/Mk_TRICORE_accounting.h>
#define MK_HWSEL_PRV_TICKER					<private/TRICORE/Mk_TRICORE_ticker.h>
/* Deviation MISRA-1 <STOP> */

#endif

/* Editor settings; DO NOT DELETE
 * vi:set ts=4:
*/
