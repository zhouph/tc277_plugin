/* Mk_misra.h
 *
 * This file contains some macros for eliminating complaints from MISRA checkers.
 *
 * (c) Elektrobit Automotive GmbH
 *
 * $Id: Mk_misra.h 15821 2014-04-16 04:55:15Z masa8317 $
*/

/* MISRA-C:2004 Deviation List
 *
 * MISRA-1) Deviated Rule: 19.13 (required)
 *  The # and ## preprocessor operators should not be used.
 *
 * Reason:
 *  This macro is used for constructing symbols and constants from a common stem to guarantee consistency.
 *
 * MISRA-2) Deviated Rule: 19.4 (required)
 *  C macros shall only expand to a braced initializer, a constant, a parenthesized expression, a type qualifier,
 *  a storage class specifier, or a do-while-zero construct.
 *
 * Reason:
 *  This macro is used for constructing symbols and constants from a common stem to guarantee consistency. In
 *  many of the places where it is used the recommended rationalizations would not work.
*/
#ifndef MK_MISRA_H
#define MK_MISRA_H

/* MK_U - Appends a 'u' suffix to a token (which must be a numerical literal constant)
 *
 * The suffix is not appended if MK_ASM is defined, that means, it is not appended for literals
 * in assembly code, as most assemblers do not understand such suffixes.
 *
 * Uses MK_CONCAT to add the suffix. This macro must be defined BEFORE MK_CONCAT(), otherwise the
 * preprocessor replaces tokens in the wrong order.
*/
#ifndef MK_ASM
#define MK_U(x)			MK_CONCAT(x,u)
#else
#define MK_U(x)			x
#endif

/* MK_CONCAT() - Concatenation macro.
 *
 * Using this macro (where it works) avoids violation MISRA-C:2004 Rule 19.12 and
 * reduces the number of deviations from Rule 19.13.
 *
 * See also MK_CAT etc. in the assembly-language include files.
*/
/* Deviation MISRA-1, MISRA-2 */
#define MK_CONCAT(a,b)	a##b

#endif

/* Editor settings - DO NOT DELETE
 * vi:set ts=4:
*/
