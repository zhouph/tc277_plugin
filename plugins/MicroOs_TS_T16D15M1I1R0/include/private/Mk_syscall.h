/* Mk_syscall.h - system call handling
 *
 * This file contains definitions for the microkernel's software system call handling.
 *
 * (c) Elektrobit Automotive GmbH
 *
 * $Id: Mk_syscall.h 15821 2014-04-16 04:55:15Z masa8317 $
*/
#ifndef MK_SYSCALL_H
#define MK_SYSCALL_H

#include <private/Mk_oscall.h>
#include <public/Mk_syscallindex.h>

#if	MK_N_OSCALL != (MK_NSYSCALL_TOTAL - MK_OS_SC(0))
#error "Mismatch between OS and microkernel system calls"
#endif

/* This macro specifies the system call number which is used
 * for not implemented system calls (or calls not implemented
 * as system calls but functions).
*/
#define MK_SC_NOT_IMPLEMENTED			MK_NSYSCALL_TOTAL

/* These are not implemented as system calls!
*/
#define MK_SC_GetEvent					MK_SC_NOT_IMPLEMENTED	/* Implemented as microkernel function */
#define MK_SC_GetActiveApplicationMode	MK_SC_NOT_IMPLEMENTED
#define MK_SC_GetAlarmBase				MK_SC_NOT_IMPLEMENTED	/* Implemented as OS function */
#define MK_SC_GetElapsedCounterValue	MK_SC_NOT_IMPLEMENTED	/* Implemented as function calling GetCounterValue */
#define MK_SC_GetScheduleTableStatus	MK_SC_NOT_IMPLEMENTED

#define MK_SC_GetStackInfo				MK_SC_NOT_IMPLEMENTED
#define MK_SC_InitCpuLoad				MK_SC_NOT_IMPLEMENTED
#define MK_SC_GetCpuLoad				MK_SC_NOT_IMPLEMENTED

#define MK_SC_CheckIsrMemoryAccess		MK_SC_NOT_IMPLEMENTED
#define MK_SC_CheckTaskMemoryAccess		MK_SC_NOT_IMPLEMENTED
#define MK_SC_CheckObjectAccess			MK_SC_NOT_IMPLEMENTED
#define MK_SC_CheckObjectOwnership		MK_SC_NOT_IMPLEMENTED

/* MK_SCtoOS() - converts a microkernel system call in the OS range to an OS system call index
*/
#define MK_SCtoOS(n)					((n)-MK_NSYSCALL_MK)

#ifndef MK_ASM

/* Type and global constant for the software system call table
*/
typedef void (* mk_syscallfptr_t)(void);

extern mk_syscallfptr_t const MK_syscallTable[MK_NSYSCALL_MK];

/* The system call handler. This is normally called by the
 * kernel entry code (assembly language)
*/
void MK_Syscall(void);

/* Error handler for unknown system calls.
 * Is called from the system call handler if the index is out of range, and
 * can also be used for unused entries in the table.
*/
void MK_UnknownSyscall(void);

/* These are the individual system call functions
*/
void MK_SysActivateTask(void);
void MK_SysChainTask(void);
void MK_SysClearEvent(void);
void MK_SysGetIsrId(void);
void MK_SysGetResource(void);
void MK_SysGetTaskId(void);
void MK_SysGetTaskState(void);
void MK_SysReleaseResource(void);
void MK_SysReportError(void);
void MK_SysSchedule(void);
void MK_SysSetEvent(void);
void MK_SysShutdown(void);
void MK_SysStartForeignThread(void);
void MK_SysGetApplicationId(void);
void MK_SysSelftest(void);
void MK_SysStartOs(void);
void MK_SysTerminateApplication(void);
void MK_SysTerminateSelf(void);
void MK_SysWaitEvent(void);
void MK_SysEnableInterruptSource(void);
void MK_SysDisableInterruptSource(void);

#endif

#endif
/* Editor settings; DO NOT DELETE
 * vi:set ts=4:
*/
