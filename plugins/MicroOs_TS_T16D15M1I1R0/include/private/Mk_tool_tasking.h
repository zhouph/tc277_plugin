/* Mk_tool_tasking.h - tasking toolchain abstraction
 *
 * This file provides the toolchain abstraction for the Tasking compiler.
 *
 * (c) Elektrobit Automotive GmbH
 *
 * $Id: Mk_tool_tasking.h 15821 2014-04-16 04:55:15Z masa8317 $
*/
#ifndef MK_TOOL_TASKING_H
#define MK_TOOL_TASKING_H

/* MK_PARAM_UNUSED(p) - mark parameter p as unused parameter (or variable).
 * Usage: Function-like.
 *
 * Supported compiler settings do not produce such a warning,
 * hence the implementation is empty.
 *
 * Attention: When changing this implementation, ensure that the
 * resulting binary code does not differ (TRICORE has separate
 * data and address registers, so a non-fully optimizing compiler
 * might add a move from an address to a data register if the
 * parameter in question is a pointer).
 */
#define MK_PARAM_UNUSED(p) do { /* NOTHING */ } while (0)

#endif
/* Editor settings; DO NOT DELETE
 * vi:set ts=4:
*/
