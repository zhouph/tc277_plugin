/* Mk_oscall.h - declaration of the QM-OS call table
 *
 * This header can be included by the OS so that it can define its call table.
 *
 * (c) Elektrobit Automotive GmbH
 *
 * $Id: Mk_oscall.h 18318 2015-02-11 10:22:06Z thdr9337 $
*/

/*
 * MISRA-C:2004 Deviation List
 *
 * MISRA-1) Deviated Rule: 8.9 (required)
 * An identifier with external linkage shall have exactly one external
 * definition.
 *
 * Reason:
 *  The variables used here are defined in the QMOS part.
*/

/* DCG Deviations:
 *
 * DCG-1) Deviated Rule: [OS_C_NAMING_025]
 *  Each function or variable having internal or external linkage shall be prefixed
 *  with a project-specific identifier prefix.
 *
 * Reason:
 *  This is not a violation of the style rule. The prefix in this case is OS_ because the
 *  call table is provided by the QM-OS.
 *
 * DCG-2) Deviated Rule: [OS_PREPROC_020]
 *  #ifndef SOME_MACRO shall only be used when the then-branch contains either
 *  #define SOME_MACRO or #error. It shall not have an else-branch.
 *
 * Reason:
 *  The construct #ifndef OS_ASM is required by the QM-OS. It has identical semantics to MK_ASM.
*/
#ifndef MK_OSCALL_H
#define MK_OSCALL_H

#include <public/Mk_public_types.h>

/* MK_N_OSCALL
 *
 * This is the number of entries in the QM-OS call table. It is equal to the number
 * of QM-OS system calls available plus those extra calls (like StartOS) that are
 * used "internally" by the microkernel.
*/
#define MK_N_OSCALL		15

/* Deviation DCG-1, DCG-2 <START> */
#ifndef MK_ASM
#ifndef OS_ASM

/* Type for a (QM-)OS kernel function.
 *
 * Note: The signature for various OS kernel function differ.
 * However the microkernel takes care that all parameters
 * are passed correctly.
 */
typedef mk_parametertype_t (*mk_oscall_t)(void);

/* This is the table of functions that the microkernel "calls" in a thread when requested
 * to do so via a "QM-OS" system call.
 * These functions are therefore callable from safety-related threads even though the OS
 * is QM
*/
/* Deviation MISRA-1 */
extern mk_oscall_t const OS_callTable[MK_N_OSCALL];

#endif
#endif
/* Deviation DCG-1, DCG-2 <STOP> */

#endif
/* Editor settings; DO NOT DELETE
 * vi:set ts=4:
*/
