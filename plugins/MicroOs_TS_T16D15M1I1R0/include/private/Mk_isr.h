/* Mk_isr.h - ISR configuration
 *
 * This file contains definitions used for the configuration of ISRs.
 *
 * (c) Elektrobit Automotive GmbH
 *
 * $Id: Mk_isr.h 15821 2014-04-16 04:55:15Z masa8317 $
*/
#ifndef MK_ISR_H
#define MK_ISR_H

#include <private/Mk_thread.h>
#include <private/Mk_interrupt.h>
#include <public/Mk_basic_types.h>

#ifndef MK_ASM

/* Types and struct for ISR configuration
*/
typedef struct mk_isrcfg_s mk_isrcfg_t;
typedef void (*mk_isrfunc_t)(void);

struct mk_isrcfg_s
{
	mk_threadcfg_t threadCfg;
	mk_thread_t *thread;
	mk_stackelement_t *stack;
	const mk_irq_t *irq;
};

/* Global constants for ISR configuration
*/
extern const mk_objquantity_t	MK_nIsrs;
extern const mk_isrcfg_t * const MK_isrCfg;

/* Construction macro for ISR
*/
#define MK_ISRCFG(irq, thr, reg, name, stk, sp, ent, pm, ilvl, ie, fpu, hws,	\
				  qpri, rpri, partIdx, iid, exBgt, lkBgt, aid)					\
{																				\
	MK_THREADCFG((reg), (name), (sp), (ent), (pm), (ilvl), (ie), (fpu), (hws),	\
				 (qpri), (rpri), (partIdx), (iid), MK_OBJTYPE_ISR, (exBgt),		\
				 (lkBgt), (aid)),												\
	(thr),																		\
	(stk),																		\
	(irq)																		\
}

#endif

#endif
/* Editor settings; DO NOT DELETE
 * vi:set ts=4:
*/
