/* Mk_tool.h - toolchain abstraction
 *
 * This file only includes the appropriate toolchain abstraction sub-header.
 *
 * (c) Elektrobit Automotive GmbH
 *
 * $Id: Mk_tool.h 15821 2014-04-16 04:55:15Z masa8317 $
*/
#ifndef MK_TOOL_H
#define MK_TOOL_H

#include <public/Mk_defs.h>

/* Include toolchain-specific header. */
#if (MK_TOOL == MK_gnu)
#include <private/Mk_tool_gnu.h>
#elif (MK_TOOL == MK_diab)
#include <private/Mk_tool_diab.h>
#elif (MK_TOOL == MK_tasking)
#include <private/Mk_tool_tasking.h>
#elif (MK_TOOL == MK_ghs)
#include <private/Mk_tool_ghs.h>
#elif (MK_TOOL == MK_ticgt)
#include <private/Mk_tool_ticgt.h>
#else
#error "Unsupported MK_TOOL."
#endif


/* MK_PARAM_UNUSED(p) - mark parameter p as unused parameter (or variable).
 * Usage: Function-like.
 */
#ifndef MK_PARAM_UNUSED
#error "MK_PARAM_UNUSED must be defined by the toolchain header."
#endif

#endif
/* Editor settings; DO NOT DELETE
 * vi:set ts=4:
*/
