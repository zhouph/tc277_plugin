/* Mk_TRICORE_exceptionhandling.h - header file for Tricore exception handling
 *
 * (c) Elektrobit Automotive GmbH
 *
 * $Id: Mk_TRICORE_exceptionhandling.h 15947 2014-04-28 10:16:48Z masa8317 $
*/
#ifndef MK_TRICORE_EXCEPTIONHANDLING_H
#define MK_TRICORE_EXCEPTIONHANDLING_H

#include <public/Mk_public_types.h>
#include <public/Mk_exceptioninfo.h>

/* The TRICORE family implements its own ExceptionIsSane-handling (as part of MK_ExceptionHandlerCommon())
*/
#define MK_HWHASEXCEPTIONISSANE  1
#define MK_HWHASINKERNEL         0

/* Codes passed to MK_UnknownInterrupt() on occurrence of unhandled exception-interrupt.
 * These codes must be outside the range of the known interrupt sources, so they start at 10000
*/
#define MK_UNKINT_NMI	10001

/* Exception setup and common handling functions
*/
void MK_HwSetupExceptions(mk_uint32_t, mk_uint32_t, mk_uint32_t, mk_uint32_t);
void MK_ExceptionHandlerCommon(mk_uint32_t, mk_uint32_t, mk_boolean_t);
void MK_FillExceptionInfo(mk_exceptionclass_t, mk_uint32_t);

#endif
/* Editor settings; DO NOT DELETE
 * vi:set ts=4:
*/
