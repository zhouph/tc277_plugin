/* Mk_TRICORE_interruptcontroller.h - processor-specific header file to define the interrupt controller
 *
 * (c) Elektrobit Automotive GmbH
 *
 * $Id: Mk_TRICORE_interruptcontroller.h 16053 2014-05-09 11:27:59Z nibo2437 $
*/
#ifndef MK_TRICORE_INTERRUPTCONTROLLER_H
#define MK_TRICORE_INTERRUPTCONTROLLER_H

#include <public/Mk_hw_characteristics.h>
#include <private/TRICORE/Mk_TRICORE_core.h>
#include <private/TRICORE/Mk_TRICORE_startup.h>
#include <public/Mk_public_types.h>

/* On all Tricore processors the interrupt vector table is a set of up to 256
 * executable stubs, each of 32 bytes. The priority assigned to the interrupt source
 * determines the vector number.
 *
 * The vector at offset 0 cannot be used because the processor cannot run with a level below 0,
 * so an interrupt source with priority 0 can never interrupt the processor. Therefore
 * the interrupt vectors start at 1 (and the interrupt table must be correctly placed
 * because of that). The missing vector at 0 can be used for startup code if needed.
 *
 * The interrupt sources are controlled by a set of "service request nodes". On some
 * processors they are placed in the address range of the peripherals with which they
 * are associated. On others they are gathered together in an array.
 *
*/


#define MK_INTVEC_OFFSET		0x20u
#define MK_INTVEC_MASK			0xffffe000u

/* Calculate the BIV value from the address by substracting the above defined offset.
*/
#define MK_INTVEC2BIV(table)	(((mk_uint32_t)(table)) - MK_INTVEC_OFFSET)

/* No. of interrupt vectors.
*/
#define MK_HWN_INTERRUPTVECTORS		255

#define MK_HWENABLEALLLEVEL		    0
#define MK_HWDISABLEALLLEVEL	    255

#ifndef MK_ASM
typedef mk_uint32_t mk_hwvectorcode_t;
typedef mk_reg32_t  *mk_hwirqctrlreg_t;
#endif

/* The interrupt stub passes pcxi and the vector number to the TRICORE interrupt dispatcher.
 *
 * MK_TricoreDispatchInterruptSoft() saves the pcxi to the current thread's register store, saves
 * the vector code to a global variable, then calls the generic soft interrupt dispatcher.
*/
#ifndef MK_ASM
void MK_TricoreDispatchInterruptSoft(mk_uint32_t, mk_hwvectorcode_t);

extern mk_hwvectorcode_t MK_intVectorCode;
#endif

#define MK_HwGetVectorCode()		MK_intVectorCode
#define MK_HwVectorCodeToInt(v)		(v)


#if (MK_TRICORE_CORE == MK_TRICORE_TC161)
#if MK_TRICORE_NCORES == 1
#define MK_IRQ_TOS	0u
#else
#define MK_IRQ_TOS	(MK_targetCore << MK_SRC_TOS_SHIFT)
#endif
#else
#define MK_IRQ_TOS	MK_SRC_TOS_CPU
#endif

/* Initialize the SRN by setting SRPN to the configured level, TOS to CPU, SRE to disabled (==zero).
*/
#define MK_HwInitIrq(irq) \
	do { \
		*((irq)->ctrlReg) = ((irq)->level & MK_SRC_SRPN_MASK) | MK_IRQ_TOS; \
	} while (0)

#define MK_HwEnableIrq(irq)     do { *((irq)->ctrlReg) |= MK_SRC_SRE; } while (0)

/* The (mk_uint32_t)-cast in the following macro is totally redundant on TRICORE, because
 * the bitmask is treated as 32-bit anyway. Its sole purpose is to fix MISRA's "underlying
 * type" for the bit-negation's operand to uint32.
*/
#define	MK_HwDisableIrq(irq)	do { *((irq)->ctrlReg) &= ~((mk_uint32_t)MK_SRC_SRE); } while (0)


#ifndef MK_ASM
void MK_SetupInterruptTable(mk_uint32_t);
void MK_HwInitInterruptController(void);
#endif

/* mk_errorid_t MK_HwCheckInterruptController(void)
 *
 * Check the static configuration of the interrupt controller.
 * @return MK_eid_InvalidSelftestFeature since the check is not implemented.
 */
#define MK_HwCheckInterruptController()		(MK_eid_InvalidSelftestFeature)

#endif
/* Editor settings; DO NOT DELETE
 * vi:set ts=4:
*/
