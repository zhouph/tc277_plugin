/* Mk_TRICORE_exceptionfunctions.h - prototypes for exception handling functions.
 *
 * This file contains prototypes for the exception functions that are called by
 * the kernel entry code in the exception vectors.
 *
 * None of these functions can be called from normal C code, so they don't really need
 * prototypes.
 *
 * This file exists to keep code checkers happy. It should only be included by the files
 * that contain exception functions.
 *
 * (c) Elektrobit Automotive GmbH
 *
 * $Id: Mk_TRICORE_exceptionfunctions.h 15947 2014-04-28 10:16:48Z masa8317 $
*/
#ifndef MK_TRICORE_EXCEPTIONFUNCTIONS_H
#define MK_TRICORE_EXCEPTIONFUNCTIONS_H

/* Eight trap handler functions (one per trap vector)
 *
 * The parameters are:
 *	D4 - pcxi value after SVLCX
 *  D5 - d15 value
*/
void MK_HandleVirtualAddressTrap(mk_uint32_t, mk_uint32_t);
void MK_HandleProtectionTrap(mk_uint32_t, mk_uint32_t);
void MK_HandleInstructionTrap(mk_uint32_t, mk_uint32_t);
void MK_HandleContextTrap(mk_uint32_t, mk_uint32_t);
void MK_HandleBusErrorTrap(mk_uint32_t, mk_uint32_t);
void MK_HandleAssertionTrap(mk_uint32_t, mk_uint32_t);
void MK_TricoreSyscall(mk_uint32_t, mk_uint32_t);
void MK_HandleNmi(mk_uint32_t, mk_uint32_t);

/* Other functions that can only be called from the assembly-language entry points
*/
void MK_InitCsaList(void);
void MK_ClearEndInit(void);

#endif
/* Editor settings; DO NOT DELETE
 * vi:set ts=4:
*/
