/* Mk_TRICORE_core.h - Tricore core header
 *
 * This file contains definitions for features that are common across all
 * the Tricore architectures.
 *
 * (c) Elektrobit Automotive GmbH
 *
 * $Id: Mk_TRICORE_core.h 18467 2015-02-20 14:48:52Z nibo2437 $
*/
#ifndef MK_TRICORE_CORE_H
#define MK_TRICORE_CORE_H

#include <public/TRICORE/Mk_TRICORE_cpu_characteristics.h>
#include <public/Mk_public_types.h>

/* CSFR register offsets
 *
 * Only those that are used get defined here.
*/
#define MK_PCXI			0xfe00
#define MK_PSW			0xfe04
#define MK_SYSCON		0xfe14
#define MK_CORE_ID		0xfe1c
#define MK_BIV			0xfe20
#define MK_BTV			0xfe24
#define MK_ISP			0xfe28
#define MK_ICR			0xfe2c
#define MK_FCX			0xfe38
#define MK_LCX			0xfe3c
#define MK_COMPAT		0x9400
#define MK_TPS_CON		0xe400
#define MK_TPS_TIMER0	0xe404
#define MK_PCON0 		0x920C
 
#ifndef MK_ASM

/* The lock level is only 8 bits but we define it to be 32 bits
 * for efficiency
*/
typedef mk_uint32_t mk_hwlocklevel_t;

/* Registers in the lower context (as saved by RSLCX and BISR instructions)
*/
struct mk_lowerctx_s
{
	mk_uint32_t pcxi;
	mk_uint32_t pc;			/* a11: pc where the exception occurred */
	mk_uint32_t a2;
	mk_uint32_t a3;
	mk_uint32_t d0;
	mk_uint32_t d1;
	mk_uint32_t d2;
	mk_uint32_t d3;
	mk_uint32_t a4;
	mk_uint32_t a5;
	mk_uint32_t a6;
	mk_uint32_t a7;
	mk_uint32_t d4;
	mk_uint32_t d5;
	mk_uint32_t d6;
	mk_uint32_t d7;
};

/* Registers in the upper context (as saved by the CALL instruction and by traps and interrupts)
*/
struct mk_upperctx_s
{
	mk_uint32_t pcxi;
	mk_uint32_t psw;
	mk_uint32_t sp;			/* a10 */
	mk_uint32_t ra;			/* a11: return address of the caller or interrupted function! */
	mk_uint32_t d8;
	mk_uint32_t d9;
	mk_uint32_t d10;
	mk_uint32_t d11;
	mk_uint32_t a12;
	mk_uint32_t a13;
	mk_uint32_t a14;
	mk_uint32_t a15;
	mk_uint32_t d12;
	mk_uint32_t d13;
	mk_uint32_t d14;
	mk_uint32_t d15;
};

typedef struct mk_lowerctx_s mk_lowerctx_t;
typedef struct mk_upperctx_s mk_upperctx_t;

/* MK_AllocateTwoCsas() - grab two CSAs from the free list (assembly language)
*/
mk_uint32_t MK_AllocateTwoCsas(void);

/* MK_SwapFcx() - inserts a CSA list into the free list (assembly language)
*/
void MK_SwapFcx(mk_uint32_t, mk_uint32_t *);

#endif

/* The PCXI register
 *
 * This register (and the copies of it in the lower and upper contexts)
 * has the following bit fields:
*/
#define MK_PCX_PCXO				0x0000ffffu			/* Index of CSA in the "array" located at the segment base */
#define MK_PCX_PCXO_SHFT		6					/* No. of bits to shift PCXO to make a pointer */
#define MK_PCX_PCXS				0x000f0000u			/* Segment base, shifted right 12 bits */
#define MK_PCX_PCXS_SHFT		12					/* No. of bits to shift PCXS to make a pointer */
#define MK_PCX_CSA_MASK			(MK_PCX_PCXO|MK_PCX_PCXS)	/* All bits used to get the address of a CSA */

#define MK_CSA_MASK				0xf03fffc0u			/* Bits of an address used to identify a CSA */

#if (MK_TRICORE_CORE == MK_TRICORE_TC161)
#define	MK_PCX_PCPN				0x3fc00000u
#define MK_PCX_PCPN_SHFT		22
#define MK_PCX_PIE				0x00200000u
#define MK_PCX_PIE_SHFT			21
#define MK_PCX_UL				0x00100000u
#define MK_PCX_UL_SHFT			20
#else
/* All TRICORE processors prior to 1.6.1 use the same layout
*/
#define	MK_PCX_PCPN				0xff000000u
#define MK_PCX_PCPN_SHFT		24
#define MK_PCX_PIE				0x00800000u
#define MK_PCX_PIE_SHFT			23
#define MK_PCX_UL				0x00400000u
#define MK_PCX_UL_SHFT			22
#endif

/* MK_PcxToAddr() converts the segment/index parts of a PCXI value to an address
 *
 * WARNING: these macros evaluate their parameter twice, so take care when using them.
*/
#define MK_PcxToAddr(p)				( ( ((p)&MK_PCX_PCXS) << MK_PCX_PCXS_SHFT ) | \
									  ( ((p)&MK_PCX_PCXO) << MK_PCX_PCXO_SHFT ) )
#define MK_AddrToPcx(p)				( ( ((p)>>MK_PCX_PCXS_SHFT) & MK_PCX_PCXS ) | \
									  ( ((p)>>MK_PCX_PCXO_SHFT) & MK_PCX_PCXO ) )

#define MK_GetPcpnFromPcxi(pcxi)	(((pcxi)&MK_PCX_PCPN) >> MK_PCX_PCPN_SHFT)
#define MK_SetPcpnInPcxi(pcxi,lvl)	(((pcxi)&~MK_PCX_PCPN) | ((lvl)<<MK_PCX_PCPN_SHFT))

/* The PSW register
*/
#define MK_PSW_PRS			0x00003000		/* Protection register set (0..3; 2 and 3 are not used by the MK) */
#define MK_PSW_PRS_0		0x00000000			/* PRS 0 is used for the kernel */
#define MK_PSW_PRS_1		0x00001000			/* PRS 1 is used for threads */
#define MK_PSW_PRS_2		0x00002000			/* PRS 2 is used for fast partitions */
#define MK_PSW_PRS_3		0x00003000			/* PRS 3 is used for fast partitions */
#define MK_PSW_S			0x00004000		/* Safe task identifier */
#define MK_PSW_IO			0x00000c00		/* Access privilege (0..3; 3 is reserved) */
#define MK_PSW_IO_U0		0x00000000			/* User-0 mode (least privileged) */
#define MK_PSW_IO_U1		0x00000400			/* User-1 mode (more privileged) */
#define MK_PSW_IO_S			0x00000800			/* Supervisor mode */
#define MK_PSW_IS			0x00000200		/* Interrupt stack */
#define MK_PSW_GW			0x00000100		/* Global register write */
#define MK_PSW_CDE			0x00000080		/* Call depth counter enable */
#define MK_PSW_CDC			0x0000007f		/* Call depth counter mask */
#define MK_PSW_CDC_6		0x00000000			/* 6 bit CDC */
#define MK_PSW_CDC_5		0x00000040			/* 5 bit CDC */
#define MK_PSW_CDC_4		0x00000060			/* 4 bit CDC */
#define MK_PSW_CDC_3		0x00000070			/* 3 bit CDC */
#define MK_PSW_CDC_2		0x00000078			/* 2 bit CDC */
#define MK_PSW_CDC_1		0x0000007c			/* 1 bit CDC */
#define MK_PSW_CDC_T		0x0000007e			/* CDC trace mode */
#define MK_PSW_CDC_DIS		0x0000007f			/* CDC disabled */

/* PSW used in Mk_TRICORE_entry2
*/
#define MK_INITIAL_PSW		(MK_PSW_PRS_0 | MK_PSW_IO_S | MK_PSW_GW | MK_PSW_CDC_DIS )

/* The ICR register
*/
#define MK_ICR_CCPN			0x000000ff		/* Current CPU priority, 0..255 */

#if (MK_TRICORE_CORE == MK_TRICORE_TC161)
#define MK_ICR_IE			0x00008000		/* Interrupts enabled (global) */
/* Interrupt arbitration cycles not configurable in Aurix
*/
#else
/* All TRICORE processors prior to 1.6.1 use the same layout
*/
#define MK_ICR_IE			0x00000100		/* Interrupts enabled (global) */
#define MK_ICR_ARBCYC		0x03000000		/* Arbitration cycles field. Values are ... */
#define MK_ICR_ARBCYC_1		0x03000000			/* 1 cycle (3 vectors) */
#define MK_ICR_ARBCYC_2		0x02000000			/* 2 cycles (15 vectors) */
#define MK_ICR_ARBCYC_3		0x01000000			/* 3 cycles (63 vectors) */
#define MK_ICR_ARBCYC_4		0x00000000			/* 4 cycles (255 vectors) */
#define MK_ICR_ONECYC		0x04000000		/* 1 clock per arbitration cycle (0 means 2 clocks) */
#endif

/* The SYSCON register
 * (Note: this is the SYSCON CSFR, not the SCU.SYSCON register!)
*/
#define MK_SYSCON_FCDSF		0x00000001		/* Free context list depleted sticky flag */
#define MK_SYSCON_PROTEN	0x00000002		/* Memory protection enable */
#define MK_SYSCON_TPROTEN	0x00000004		/* Temporal protection enable */

/* The exception vector table
*/
#define MK_EXCVEC_MASK		0xffffff00u

/* The SRC register
 *
 * Configures interrupt priority and service provider. Additionally provides bits to
 * enable, disable, trigger, and clear the interrupt request.
*/
#define MK_SRC_SRPN_MASK	0x000000ffu		/* Service request priority */

#if (MK_TRICORE_CORE == MK_TRICORE_TC161)
#define	MK_SRC_TOS_MASK		0x00001800u		/* Type of service aka interrupt target */
#define	MK_SRC_TOS_SHIFT	11				/* Number of bits to shift core id to get TOS */
#define MK_SRC_SRE			0x00000400u		/* Service request enabled */
#define MK_SRC_SRR			0x01000000u		/* Service requested aka interrupt pending */
#define MK_SRC_CLRR			0x02000000u		/* Clear service request */
#define MK_SRC_SETR			0x04000000u		/* Set service request aka trigger interrupt */
#else
/* All TRICORE processors prior to 1.6.1 use the same layout
*/
#define	MK_SRC_TOS_MASK		0x00000c00u		/* Type of service aka interrupt target */
#define MK_SRC_TOS_CPU		0x00000000u
#define MK_SRC_SRE			0x00001000u		/* Service request enabled */
#define MK_SRC_SRR			0x00002000u		/* Service requested aka interrupt pending */
#define MK_SRC_CLRR			0x00004000u		/* Clear service request */
#define MK_SRC_SETR			0x00008000u		/* Set service request aka trigger interrupt */
#endif


#endif

/* Editor settings; DO NOT DELETE
 * vi:set ts=4:
*/
