/* Mk_interrupt.h - interrupt handling
 *
 * This file contains definitions for the hardware-independent part of the
 * microkernel's interrupt handling. It also includes the hardware-specific
 * part.
 *
 * (c) Elektrobit Automotive GmbH
 *
 * $Id: Mk_interrupt.h 15821 2014-04-16 04:55:15Z masa8317 $
*/

/* MISRA-C:2004 Deviation List
 *
 * MISRA-1) Deviated Rule: 19.1 (advisory)
 *   include statements shall only be preceded by other preprocessor directives.
 *
 * Reason:
 *   Avoiding circular dependencies between hw-specific header and this file.
 */

#ifndef MK_INTERRUPT_H
#define MK_INTERRUPT_H

#include <public/Mk_hwsel.h>
#include <public/Mk_public_types.h>

#ifndef MK_ASM
typedef struct mk_irq_s mk_irq_t;
#endif

/* Include the hardware-specific interrupt controller header
*/
/* Deviation MISRA-1 */
#include MK_HWSEL_PRV_INTERRUPTCONTROLLER

#ifndef MK_ASM
typedef void (*mk_interruptfunc_t)(mk_objectid_t, mk_hwvectorcode_t);

struct mk_irq_s
{
	mk_hwirqctrlreg_t ctrlReg;
	mk_hwlocklevel_t level;
	mk_uint32_t flags;
};

/* Macro for defining an IRQ
*/
#define MK_IRQCFG(ctrlreg, level, flags) \
{	(ctrlreg),		\
	(level),		\
	(flags)			\
}

/* Bits in flags
*/
#define MK_IRQ_ENABLE	0x01u

/* Function prototypes
*/
void MK_UnknownInterrupt(mk_objectid_t, mk_hwvectorcode_t);
void MK_StartThreadForIsr(mk_objectid_t, mk_hwvectorcode_t);
void MK_InitInterrupts(void);

/* Global constants
*/
extern const mk_irq_t * const MK_irqCfg;
extern const mk_int_fast16_t MK_nIrqs;

#endif

#endif
/* Editor settings; DO NOT DELETE
 * vi:set ts=4:
*/
