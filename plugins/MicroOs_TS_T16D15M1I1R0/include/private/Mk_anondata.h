/* Mk_anondata.h - declarations of anonymous data section boundaries
 *
 * This file declares the linker-generated symbols that mark the boundaries of the "anonymous" data/bss
 * sections and the initial value image for the anonymous data section.
 *
 * The symbols are not variables as such. They are start and end markers of sections that contain
 * variables that have not been assigned to any other sections in the linker script.
 *
 * This file is included exactly once, by the function that initializes these sections.
 *
 * (c) Elektrobit Automotive GmbH
 *
 * $Id: Mk_anondata.h 18295 2015-02-09 15:17:41Z thdr9337 $
*/

/* MISRA-C:2004 Deviation List
 *
 * MISRA-1) Deviated Rule: 8.9 (required)
 * An identifier with external linkage shall have exactly one external
 * definition.
 *
 * Reason:
 *  The variables used here are defined in the linker file.
*/

#ifndef MK_ANONDATA_H
#define MK_ANONDATA_H

#include <public/Mk_basic_types.h>

/* Deviation MISRA-1 <+4> */
extern mk_int8_t MK_ANON_DATA;
extern mk_int8_t MK_ANON_DATA_END;
extern mk_int8_t MK_ANON_BSS;
extern mk_int8_t MK_ANON_BSS_END;

extern mk_int8_t MK_ANON_DATAMANDO;
extern mk_int8_t MK_ANON_DATA_ENDMANDO;
extern mk_int8_t MK_ANON_BSSMANDO;
extern mk_int8_t MK_ANON_BSS_ENDMANDO;
extern mk_int8_t MK_ANON_XCP;
extern mk_int8_t MK_ANON_XCP_END;

#if (MK_TOOL == MK_ghs)
/* If GHS is used, tell toolchain that this linker variable resides in the
 * standard data section, even if sda optimization is turned on.
 * Otherwise, the compiler tries to access this ROM address via RAM sda
 * section offset.
 */
#pragma ghs startdata
#endif
/* Deviation MISRA-1 */
extern const mk_int8_t MK_ANON_IDAT;
extern const mk_int8_t MK_ANON_IDATMANDO;
#if (MK_TOOL == MK_ghs)
#pragma ghs enddata
#endif
#endif
/* Editor settings; DO NOT DELETE
 * vi:set ts=4:
*/
