/* Mk_memoryprotection.h - memory protection configuration and handling
 *
 * This file contains definitions for the hardware-independent part of configuration
 * and handling of memory protection. It also includes the hardware-specific part.
 *
 * (c) Elektrobit Automotive GmbH
 *
 * $Id: Mk_memoryprotection.h 15821 2014-04-16 04:55:15Z masa8317 $
*/

/* MISRA-C:2004 Deviation List
 *
 * MISRA-1) Deviated Rule: 19.12 (required)
 *  There shall be at most one occurrence of the # or ## preprocessor operators in a single macro definition.
 *
 * Reason:
 *  Automatic construction of symbols in a consistent manner.
 *  Additionally, attempting to remove the deviation by means of a concatenation macro introduced exactly
 *  the problem that this rule is intended to eliminate.
 *
 * MISRA-2) Deviated Rule: 19.13 (required)
 *  The # and ## preprocessor operators should not be used.
 *
 * Reason:
 *  Automatic construction of symbols in a consistent manner.
*/
#ifndef MK_MEMORYPROTECTION_H
#define MK_MEMORYPROTECTION_H

#include <public/Mk_hwsel.h>

#include <public/Mk_basic_types.h>
#include <public/Mk_public_types.h>
#include <public/Mk_error.h>
#include <private/Mk_types_forward.h>

#include MK_HWSEL_PRV_MEMORYPROTECTION

#ifndef MK_ASM
/* A memory partition contains a number of regions defined by an array of region mappings
 * !LINKSTO Microkernel.Memory.Config.General.Tables, 1,
*/
struct mk_memorypartition_s
{
	const mk_memoryregionmap_t *regionMap;	/* Pointer to an array of nRegions region references. */
	mk_objquantity_t nRegions;				/* No. of regions in this partition */
};

/* A memory region mapping contains a pointer to a region.
 * For checking purposes it could also contain a partition id or reference, but this implementation doesn't.
 * !LINKSTO Microkernel.Memory.Config.General.Tables, 1,
 * !doctype src
*/
struct mk_memoryregionmap_s
{
	const mk_memoryregion_t *region;
};

/* A memory region is simply an address range. All addresses are such that startaddr <= a < limitaddr are in
 * the region. The region also has access permissions and initialization information
 *
 * Note: the "limit address" actually held in the region structure might be the last address of the region
 * or the first address outside the region, depending on how the memory protection hardware works.
 * This is done to avoid performing arithmetic on constants (+1 or -1) when writing to the MPU, which
 * is time-critical.
 * Preconditions:
 *	- the linker always provides the first address outside the region
 *	- the value in the structure is always the value that is programmed into the MPU
 *	- three macros are provided:
 *		-- MK_LastAddressOfRegion() adjusts a limit address downwards if necessary
 *		-- MK_FirstAddressAboveRegion() adjusts a limit address upwards if necessary
 *		-- MK_AdjustLinkerAddressForLimit() converts the linker address to the correct limit address
 * - the architecture/derivative defines the macro MK_HWMPUUSESLASTADDRESSOFREGION to 0 or 1
*/
struct mk_memoryregion_s
{
	mk_uint32_t *mr_startaddr;			/* Start address of the region */
	mk_uint32_t *mr_bstartaddr;			/* Start address of zero-initialized ("bss") part of region */
	mk_uint32_t *mr_limitaddr;			/* Limit address of the region (see above!) */
	const mk_uint32_t *mr_idata;		/* Address of initialization data */
	mk_uint32_t mr_permissions;			/* Permissions for this region */
#if MK_HAVE_CPUFAMILYMEMORYREGION_T
	mk_cpufamily_memoryregion_t mr_hwextra;		/* architecture specific extension */
#endif
};
#endif /* MK_ASM */

#ifndef MK_HWMPUUSESLASTADDRESSOFREGION
#error "CPU family has not defined MK_HWMPUUSESLASTADDRESSOFREGION"
#endif

#if MK_HWMPUUSESLASTADDRESSOFREGION
#define MK_LastAddressOfRegion(x)			(x)
#define MK_FirstAddressAboveRegion(x)		((mk_uint32_t *)(((mk_address_t)(x))+1u))
#define MK_AdjustLinkerAddressForLimit(x)	((mk_uint32_t *)(((mk_address_t)(x))-1u))
#else
#define MK_LastAddressOfRegion(x)			((mk_uint32_t *)(((mk_address_t)(x))-1u))
#define MK_FirstAddressAboveRegion(x)		(x)
#define MK_AdjustLinkerAddressForLimit(x)	(x)
#endif

#ifndef MK_ASM
extern const mk_objquantity_t MK_nMemoryPartitions;
extern const mk_objquantity_t MK_nMemoryRegions;
extern const mk_memorypartition_t * const MK_memoryPartitions;
extern const mk_memoryregion_t * const MK_memoryRegions;

extern mk_objectid_t MK_currentMemoryPartition;
extern mk_objectid_t MK_firstVariableRegion;
extern mk_objquantity_t MK_nVariableRegions;
extern mk_objquantity_t MK_maxVariableRegions;
#endif /* MK_ASM */

/* Define a memory partition - used by the configuration.
 *
 * Parameters:
 *	- r = index of first memory region mapping in MK_memoryRegionMap[] array
 *	- n = no. of (consecutive) regions that belong to this partition.
 *
 * If n==0, r is not used.
 *
 * !LINKSTO Microkernel.Memory.Config.General.EntryMacro, 1
 * !doctype src
 *
 * Coding guidelines: the rule that no conditional code is allowed in macros does not apply
 * here because the macro expands to a constant initializer, not to executable code.
*/
#define MK_MEMORYPARTITION(r,n)		{(n)==0?MK_NULL:&MK_memRegionMap[(r)], (n)}

/* Define a memory region mapping - used by the configuration.
 *
 * Parameters:
 *	- p = partition index (currently not used: for documentation)
 *	- r = index of memory region in MK_memoryRegion[] array
 *
 * !LINKSTO Microkernel.Memory.Config.General.EntryMacro, 1
 * !doctype src
*/
#define MK_MEMORYREGIONMAP(p,r)		{&MK_memRegion[(r)]}

/* MK_MEMORYREGION - Define a memory region - used by the configuration.
 *
 * Parameters:
 *	start	- is the first address of the region
 *	bstart	- is the first address of the zero-initialized (".bss") part of the region
 *	limit	- is the limit address of the region (i.e. the first address ABOVE the region)
 *	idata	- is the address of the initialization data for the initialized (".data") part of the region
 *	perms	- is the permissions granted for this region; combination of MK_MPERM_* defined by the architecture.
 *  select  - additional hardware-specific selection criteria
 *
 * MK_MR_INIT and MK_MR_NOINIT are simplified forms where the addresses are derived from the name of the
 * region using MK_RSA/MK_BSA/MK_RLA/MK_RDA prefixes.
*/
#if MK_HAVE_CPUFAMILYMEMORYREGION_T
#define MK_MEMORYREGION(start,bstart,limit,idata,perms,hwextra) \
{	(start),	\
	(bstart),	\
	MK_AdjustLinkerAddressForLimit(limit),	\
	(idata),	\
	(perms),	\
	MK_CPUFAMILYMEMORYREGION(hwextra)				\
}

#else /* no architecture specific extension for a memory region */

#define MK_MEMORYREGION(start,bstart,limit,idata,perms,hwextra) \
{	(start),	\
	(bstart),	\
	MK_AdjustLinkerAddressForLimit(limit),	\
	(idata),	\
	(perms)		\
}
#endif

/* !LINKSTO Microkernel.Memory.Config.General.EntryMacro, 1
 * !doctype src
 */
/* Deviation MISRA-1, MISRA-2 <START> */
#define MK_MR_INIT(name, perms, hwextra) \
	MK_MEMORYREGION(&MK_RSA_##name, &MK_BSA_##name, &MK_RLA_##name, &MK_RDA_##name, (perms), hwextra)

#define MK_MR_NOINIT(name, perms, hwextra) \
	MK_MEMORYREGION(&MK_RSA_##name, MK_NULL, &MK_RLA_##name, MK_NULL, (perms), hwextra)
/* Deviation MISRA-1, MISRA-2 <STOP> */

/* Defined memory partitions.
 * 0 must exist if memory protection is configured. The remainder depend on the configuration.
*/
#define MK_MEMPART_GLOBAL		0		/* Always index 0. Defines the "fixed" regions. */

/* Memory protection functions.
*/
#ifndef MK_ASM
void MK_InitMemoryProtection(void);
void MK_InitMemoryRegion(const mk_memoryregion_t*);
void MK_HwEnableMemoryProtection(void);
#endif /* MK_ASM */

#endif

/* Editor settings; DO NOT DELETE
 * vi:set ts=4:
*/
