/* Mk_errorhandling.h
 *
 * This file contains definitions for the microkernel's error handling.
 *
 * (c) Elektrobit Automotive GmbH
 *
 * $Id: Mk_errorhandling.h 18628 2015-03-02 11:09:43Z nibo2437 $
*/
#ifndef MK_ERRORHANDLING_H
#define MK_ERRORHANDLING_H

#include <public/Mk_error.h>
#include <private/Mk_thread.h>

/* Constant pointers to the thread(s) to use for the error- and protection-hooks.
 * These pointers might point to the same thread, or to different threads, depending on the configuration.
 * There might be no hooks configured, in which case these pointers might be NULL.
*/
extern mk_thread_t * const MK_errorHookThread;
extern mk_thread_t * const MK_protectionHookThread;

/* Configuration structures for running the error- and protection-hooks in a thread.
 * There might be no hooks configured, in which case these pointers might be NULL.
*/
extern const mk_threadcfg_t * const MK_errorHookThreadConfig;
extern const mk_threadcfg_t * const MK_protectionHookThreadConfig;

/* Internal functions related to error reporting
*/
mk_boolean_t MK_ReportError(mk_serviceid_t, mk_errorid_t, mk_thread_t*);
void MK_ReportProtectionFault(mk_serviceid_t, mk_errorid_t);

/* Individual protection actions
*/
void MK_PpaContinue(void);
void MK_PpaPanic(void);
void MK_PpaPanicStop(void);
void MK_PpaShutdown(void);
void MK_PpaTerminateObject(void);
void MK_PpaTerminateTaskIsr(void);
void MK_PpaTerminateApplication(void);
void MK_PpaTerminateApplicationRestart(void);
void MK_PpaTerminateThread(void);
void MK_PpaQuarantineObject(void);

/* If the desired protection action is invalid or not implemented,
 * shutdown the microkernel.
*/
#define MK_PpaInvalidProtectionAction MK_PpaShutdown

/* Type used for a protection action
*/
typedef void (*mk_ppafunction_t)(void);

/* Array that contains function pointers to all protection actions
*/
extern const mk_ppafunction_t MK_ppaFunctions[MK_PRO_INVALIDACTION + 1];

#endif
/* Editor settings - DO NOT DELETE
 * vi:set ts=4:
*/
