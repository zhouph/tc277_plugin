/* Mk_softvector.h - software interrupt vectoring
 *
 * This file contains definitions for the hardware-independent part of configuration
 * and handling of the software interrupt vector table. It also includes the
 * hardware-specific part.
 *
 * (c) Elektrobit Automotive GmbH
 *
 * $Id: Mk_softvector.h 15821 2014-04-16 04:55:15Z masa8317 $
*/
#ifndef MK_SOFTVECTOR_H
#define MK_SOFTVECTOR_H

#include <public/Mk_hwsel.h>
#include <public/Mk_public_types.h>
#include <private/Mk_interrupt.h>

/* Include the hardware-specific interrupt controller header
*/
#include MK_HWSEL_PRV_INTERRUPTCONTROLLER

#ifndef MK_ASM

typedef struct mk_softvector_s mk_softvector_t;

struct mk_softvector_s
{
	mk_interruptfunc_t func;
	mk_objectid_t param;
};

/* The software vector table contains an extra (unused) entry to act as a marker.
 * There is a check at startup that verifies that the vector table has been initialized
 * with the correct number of entries.
*/
extern const mk_softvector_t MK_softwareVectorTable[MK_HWN_INTERRUPTVECTORS+1];

/* These are initializer macros for the software interrupt-vector table
*/
#define MK_VECTOR_UNKNOWN(n)	{ MK_UnknownInterrupt, (n) }
#define MK_VECTOR_ISR(n)		{ MK_StartThreadForIsr, (n) }
#define MK_VECTOR_INTERNAL(f,n)	{ (f), (n) }

/* These macros are used to create the end marker of the software interrupt vector table
*/
#define MK_VECTOR_LASTMARK		(32767)									/* 0x7fff */
#define MK_VECTOR_LAST			MK_VECTOR_UNKNOWN(MK_VECTOR_LASTMARK)

void MK_DispatchInterruptSoft(void);

#endif

#endif
/* Editor settings; DO NOT DELETE
 * vi:set ts=4:
*/
