/* Mk_TRICORE_enabletps.s
 *
 * This file contains the function MK_EnableTps(). It globally enables the
 * temporal protection system by setting the respective bit in the SYSCON register
 *
 * (c) Elektrobit Automotive GmbH
 *
 * $Id: Mk_TRICORE_enabletps.s 15947 2014-04-28 10:16:48Z masa8317 $
*/
#include <private/Mk_asm.h>		/* Must be first! */
#include <private/TRICORE/Mk_TRICORE_core.h>

	MK_file(Mk_TRICORE_enabletps.s)

	MK_ASM_SDECL_TEXT
	MK_ASM_SECTION_TEXT
	MK_ASM_ALIGN_TEXT

	MK_global MK_EnableTps

/* void MK_EnableTps(void)
 *
 * This function globally enables the temporal protection system by setting the respective bit in the
 * SYSCON register
 *
*/
MK_EnableTps:

	mov		d2, MK_imm(#, MK_lo(MK_SYSCON_TPROTEN))
	addih	d2, d2, MK_imm(#, MK_hi(MK_SYSCON_TPROTEN))

	mfcr	d1, MK_imm(#, MK_SYSCON)
	or		d1, d2
	mtcr	MK_imm(#, MK_SYSCON), d1

	isync

	ret								/* Back to caller */

/* Editor settings - DO NOT DELETE
 * vi:set ts=4:
*/
