/* Mk_TRICORE_exceptionhandlercommon.c
 *
 * This file contains the function MK_ExceptionHandlerCommon()
 *
 * (c) Elektrobit Automotive GmbH
 *
 * $Id: Mk_TRICORE_exceptionhandlercommon.c 15947 2014-04-28 10:16:48Z masa8317 $
*/
#include <public/Mk_basic_types.h>
#include <public/Mk_public_types.h>
#include <private/TRICORE/Mk_TRICORE_exceptionhandling.h>
#include <private/Mk_thread.h>
#include <private/TRICORE/Mk_TRICORE_thread.h>
#include <private/TRICORE/Mk_TRICORE_core.h>
#include <private/TRICORE/Mk_TRICORE_compiler.h>
#include <private/Mk_panic.h>

/* MK_ExceptionHandlerCommon()
 *
 * This function is called at the beginning of each exception handler. Its job is to
 * store the PCXI and D15 values (head of used CSA list and TIN or system-call index, respectively)
 * into the thread's register store.
 *
 * Before doing so, it checks if the exception is "sane". If the exception occurred within the
 * Microkernel (detected by the IS flag in the saved PSW being 1) the register values are not stored
 * and MK_Panic() is called.
 *
 * !LINKSTO Microkernel.TRICORE.Function.MK_ExceptionHandlerCommon, 1
 * !doctype src
*/
void MK_ExceptionHandlerCommon(mk_uint32_t pcxiValue, mk_uint32_t d15Value, mk_boolean_t returnOnPreempt)
{
	mk_lowerctx_t *lowerCx;
	mk_upperctx_t *upperCx = MK_NULL;

	/* First things first: stop TPS-decrementer to reduce number of in-kernel exceptions
	*/
	MK_MTTPS_TIMER0(0);

	/* Then clear any exception that may have happened up to here
	*/
	MK_MTTPS_CON(0);

	lowerCx = (mk_lowerctx_t *)MK_PcxToAddr(pcxiValue);

	if ( lowerCx == MK_NULL )
	{
		/* This should be unreachable.
		 * !LINKSTO Microkernel.TRICORE.Panic.MK_panic_RegisterStoreIsNotClean, 1
		 * !doctype src
		*/
		MK_Panic(MK_panic_RegisterStoreIsNotClean);
	}
	else
	{
		upperCx = (mk_upperctx_t *)MK_PcxToAddr(lowerCx->pcxi);

		if ( upperCx == MK_NULL )
		{
			/* This should be unreachable.
			 * !LINKSTO Microkernel.TRICORE.Panic.MK_panic_RegisterStoreIsNotClean, 1
			 * !doctype src
			*/
			MK_Panic(MK_panic_RegisterStoreIsNotClean);
		}
	}

	if ( ( upperCx->psw & MK_U(MK_PSW_IS) ) == 0u )
	{
		/* IS flag was zero ==> exception apparently occurred in a thread.
		 *
		 * Check that the current thread is valid before using because using a NULL pointer
		 * on Tricore causes an exception which would eventually lead to a free CSA exhaustion
		 * (FCX==0) exception, which is difficult to debug.
		 * !LINKSTO Microkernel.Panic.MK_panic_ExceptionNoThread, 1
		 * !doctype src
		*/
		if ( MK_threadCurrent == MK_NULL )
		{
			MK_Panic(MK_panic_ExceptionNoThread);
		}
		else
		{
			/* Current thread pointer looks OK.
			 * Save the register values (and the pseudo-register values).
			*/
			MK_threadCurrent->regs->pcxi = pcxiValue;
			MK_threadCurrent->regs->trapCode = d15Value;
			MK_threadCurrent->regs->upper = upperCx;
			MK_threadCurrent->regs->lower = lowerCx;

			if ( MK_threadCurrent == MK_threadQueueHead )
			{
				/* Everything seems to be OK. Continue processing normally.
				*/
			}
			else
			{
				/* The current thread is not at the head of the queue, so something has been tinkering
				 * without dispatching, or maybe the exception occurred in the Microkernel.
				 * !LINKSTO Microkernel.Panic.MK_panic_ExceptionCurrentThreadNotAtHeadOfQueue, 1
				 * !doctype src
				*/
				MK_Panic(MK_panic_ExceptionCurrentThreadNotAtHeadOfQueue);
			}
		}
	}
	else
	{
		/* IS flag was non-zero ==> exception occurred in the Microkernel.
		*/

		if (returnOnPreempt)
		{
			/* Return to the interrupted kernel context
			*/
			MK_TricoreResumeThread(pcxiValue);
		}
		else
		{
			/* !LINKSTO Microkernel.Panic.MK_panic_ExceptionCurrentThreadNotAtHeadOfQueue, 1
			 * !doctype src
			*/
			MK_Panic(MK_panic_ExceptionFromKernel);
		}
	}
}

/* Editor settings; DO NOT DELETE
 * vi:set ts=4:
*/
