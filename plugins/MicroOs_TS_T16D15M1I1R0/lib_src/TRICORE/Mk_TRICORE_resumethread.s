/* Mk_TRICORE_resumethread.s
*
 * This file contains the MK_TricoreResumeThread function
 *
 * (c) Elektrobit Automotive GmbH
 *
 * $Id: Mk_TRICORE_resumethread.s 15947 2014-04-28 10:16:48Z masa8317 $
*/

/* DCG Deviations:
 *
 * DCG-1) Deviated Rule: [OS_ASM_STRUCT_020]
 *  Each assembly file shall contain exactly one return instruction, which is the last instruction.
 *
 * Reason:
 *  The RET instruction in the middle of this function is used to discard a CSA from the in-use list.
 *  The return address is manipulated so that the instruction forms the backward jump of a loop,
 *  and does not return to the caller.
*/
/* Deviation DCG-1 <*> */

#include <private/Mk_asm.h>		/* Must be first! */
#include <private/TRICORE/Mk_TRICORE_core.h>
#include <private/TRICORE/Mk_TRICORE_thread.h>

	MK_file(Mk_TRICORE_resumethread.s)

	MK_global	MK_TricoreResumeThread

	MK_ASM_SDECL_TEXT
	MK_ASM_SECTION_TEXT
	MK_ASM_ALIGN_TEXT

/*
 * MK_TricoreResumeThread(mk_uint32_t pcxi)
 *
 * Resumes a thread by loading its saved registers.
 *
 * Parameter pcxi (in d4) - saved PCXI value.
 *
 * Preconditions:
 *	Interrupts disabled.
 *
 * First we clean up the CSA list used by the kernel.
 * The list will only contain upper contexts caused by CALL instructions,
 * so we can just fix the return address and RET until the PCXI register is empty.
 *
 * To avoid returning to the caller of this function, we load the address of the loop label
 * into the return address register, thus we use the RET instruction as the backward
 * jump in the loop.
 *
 * The upper half of the loop label is loaded into a7 to start with, so that the
 * reload in the loop only needs one instruction.
 *
 * Using optimizing compilers the loop is rarely executed more than two times. In kernel
 * invocations without errors, the loop is sometimes never entered at all.
 *
 * !LINKSTO Microkernel.TRICORE.Function.MK_TricoreResumeThread, 1
 * !doctype src
*/
MK_TricoreResumeThread:
	movh.a	a7, MK_imm(#, MK_hi(MK_ResumeThreadLoop))	/* Upper half of start-of-loop label */

MK_ResumeThreadLoop:
	dsync
	mfcr	d15, MK_imm(#, MK_PCXI)						/* Get current PCXI */
	extr.u	d15, d15, MK_imm(#, 0), MK_imm(#, 20)		/* Just the address parts */
	jz		d15, MK_ResumeThreadDone					/* Jump at end of list? */

	lea		a11, [a7]MK_lo(MK_ResumeThreadLoop)			/* Load start of loop as "return address" */
	ret													/* Discard CSA and loop back */

MK_ResumeThreadDone:									/* No dsync needed here - already done */
	mtcr		MK_imm(#, MK_PCXI), d4					/* Put thread's pcxi back in register */
	isync												/* Force instruction to complete */
	rslcx												/* Restore lower context registers including A11 (RA) */
	rfe													/* Restore upper context registers including PC and PSW */

/* Editor settings: DO NOT DELETE
 * vi:set ts=4:
*/
