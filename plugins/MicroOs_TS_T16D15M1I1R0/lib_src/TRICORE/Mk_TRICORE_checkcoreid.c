/* Mk_TRICORE_checkcoreid.c
 *
 * This file contains the TRICORE startup check function MK_CheckCoreId()
 *
 * (c) Elektrobit Automotive GmbH
 *
 * $Id: Mk_TRICORE_checkcoreid.c 16204 2014-05-21 14:41:23Z nibo2437 $
*/
#include <private/Mk_startup.h>
#include <private/TRICORE/Mk_TRICORE_startup.h>
#include <private/TRICORE/Mk_TRICORE_compiler.h>
#include <private/Mk_panic.h>

/* MK_CheckCoreId()
 *
 * This function verifies that the microkernel is actually running on the core
 * it has been configured to run on. It is called from MK_HwInitProcessor(),
 * but only on derivates that have more than a single core.
 *
*/
void MK_CheckCoreId(void)
{
	/* Refuse to start on another core than the one configured
	*/
	if ( MK_GetCoreId() != MK_targetCore )
	{
		MK_StartupPanic(MK_panic_ExpectedHardwareNotFound);
	}
}

/* Editor settings; DO NOT DELETE
 * vi:set ts=4:
*/
