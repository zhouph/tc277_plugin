/* Mk_TRICORE_exceptiontable.s
 *
 * This file contains the exception table for Tricore processors
 *
 * (c) Elektrobit Automotive GmbH
 *
 * $Id: Mk_TRICORE_exceptiontable.s 15947 2014-04-28 10:16:48Z masa8317 $
*/

/* DCG Deviations:
 *
 * DCG-1) Deviated Rule: [OS_LAYOUT_020]
 *  Source files shall contain at most one routine having external linkage.
 *
 * Reason:
 *  This file contains a set of exception vector/kernel entry routines.
 *  The exception is permitted by the DCG.
 *
 * DCG-2) Deviated Rule: [OS_ASM_STRUCT_020]
 *  Each assembly file shall contain exactly one return instruction, which is the last instruction.
 *
 * Reason:
 *  This file contains a set of exception vector/kernel entry routines, each of which transfers
 *  control to a handler that exits via the dispatcher. A RET instruction at the end of the file would
 *  be unreachable code.
*/
/* Deviation DCG-1 <*> */
/* Deviation DCG-2 <*> */

#include <private/Mk_asm.h>		/* Must be first! */
#include <private/TRICORE/Mk_TRICORE_core.h>

	MK_file(Mk_TRICORE_exceptiontable.s)

/*
 * !LINKSTO        Microkernel.TRICORE.Exception.Handlers, 1
 * !description    The C handler functions for the exceptions
 * !doctype        src
*/
	MK_ASM_SDECL_EXCEPTIONTABLE
	MK_ASM_SECTION_EXCEPTIONTABLE
	MK_ASM_ALIGN_EXCVECTORTABLE

	MK_global	MK_ExceptionTable

	MK_global	MK_Exception0_VirtualAddress
	MK_global	MK_Exception1_Protection
	MK_global	MK_Exception2_Instruction
	MK_global	MK_Exception3_Context
	MK_global	MK_Exception4_BusError
	MK_global	MK_Exception5_Assertion
	MK_global	MK_Exception6_Syscall
	MK_global	MK_Exception7_Nmi

	MK_extern	MK_HandleVirtualAddressTrap
	MK_extern	MK_HandleProtectionTrap
	MK_extern	MK_HandleInstructionTrap
	MK_extern	MK_HandleContextTrap
	MK_extern	MK_HandleBusErrorTrap
	MK_extern	MK_HandleAssertionTrap
	MK_extern	MK_HandleNmi
	MK_extern	MK_TricoreSyscall

/* MK_ExceptionTable
 *
 * The exception table is a set of 8 stub routines spaced at 32-byte intervals
 * from a base address that is aligned on a 256 byte boundary.
 *
 * Each exception has its own Tricore-specific handler function that takes two
 * parameters:
 *  - pcxi (d4) head list of of CSAs that are in use by the thread (after saving lower context)
 *	- trap identification number (d5) - the value placed in d15 by the processor
*/
MK_ExceptionTable:

/*	Entry 0: Virtual address trap
*/
	MK_ASM_ALIGN_VECTORTABLEENTRY
MK_Exception0_VirtualAddress:
	svlcx
	mov		d5, d15
	mov		d15, MK_imm(#, 0)
	dsync
	mfcr	d4, MK_imm(#, MK_PCXI)
	mtcr	MK_imm(#, MK_PCXI), d15
	j		MK_HandleVirtualAddressTrap

/*	Entry 1: Protection trap
*/
	MK_ASM_ALIGN_VECTORTABLEENTRY
MK_Exception1_Protection:
	svlcx
	mov		d5, d15
	mov		d15, MK_imm(#, 0)
	dsync
	mfcr	d4, MK_imm(#, MK_PCXI)
	mtcr	MK_imm(#, MK_PCXI), d15
	j		MK_HandleProtectionTrap

/*	Entry 2: Instruction trap
*/
	MK_ASM_ALIGN_VECTORTABLEENTRY
MK_Exception2_Instruction:
	svlcx
	mov		d5, d15
	mov		d15, MK_imm(#, 0)
	dsync
	mfcr	d4, MK_imm(#, MK_PCXI)
	mtcr	MK_imm(#, MK_PCXI), d15
	j		MK_HandleInstructionTrap

/*	Entry 3: Context trap
*/
	MK_ASM_ALIGN_VECTORTABLEENTRY
MK_Exception3_Context:
	svlcx							/* Note: in case of the (non-recoverable) FCU-exception this triggers
									 *       an endless loop of FCU-exceptions. This is a safe state and is
									 *       as good as any explicit detection of this exception type here.
									*/
	mov		d5, d15
	mov		d15, MK_imm(#, 0)
	dsync
	mfcr	d4, MK_imm(#, MK_PCXI)
	mtcr	MK_imm(#, MK_PCXI), d15
	j		MK_HandleContextTrap

/*	Entry 4: Bus error trap
*/
	MK_ASM_ALIGN_VECTORTABLEENTRY
MK_Exception4_BusError:
	svlcx
	mov		d5, d15
	mov		d15, MK_imm(#, 0)
	dsync
	mfcr	d4, MK_imm(#, MK_PCXI)
	mtcr	MK_imm(#, MK_PCXI), d15
	j		MK_HandleBusErrorTrap

/*	Entry 5: Assertion trap
*/
	MK_ASM_ALIGN_VECTORTABLEENTRY
MK_Exception5_Assertion:
	svlcx
	mov		d5, d15
	mov		d15, MK_imm(#, 0)
	dsync
	mfcr	d4, MK_imm(#, MK_PCXI)
	mtcr	MK_imm(#, MK_PCXI), d15
	j		MK_HandleAssertionTrap

/*	Entry 6: System call
*/
	MK_ASM_ALIGN_VECTORTABLEENTRY
MK_Exception6_Syscall:
	svlcx
	mov		d5, d15
	mov		d15, MK_imm(#, 0)
	dsync
	mfcr	d4, MK_imm(#, MK_PCXI)
	mtcr	MK_imm(#, MK_PCXI), d15
	j		MK_TricoreSyscall

/*	Entry 7: NMI
*/
	MK_ASM_ALIGN_VECTORTABLEENTRY
MK_Exception7_Nmi:
	svlcx
	mov		d5, d15
	mov		d15, MK_imm(#, 0)
	dsync
	mfcr	d4, MK_imm(#, MK_PCXI)
	mtcr	MK_imm(#, MK_PCXI), d15
	j		MK_HandleNmi

/* Editor settings: DO NOT DELETE
 * vi:set ts=4:
*/
