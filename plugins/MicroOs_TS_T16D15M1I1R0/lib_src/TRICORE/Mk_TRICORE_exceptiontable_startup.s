/* Mk_TRICORE_exceptiontable_startup.s
 *
 * This file contains the startup exception table for Tricore processors
 *
 * (c) Elektrobit Automotive GmbH
 *
 * $Id: Mk_TRICORE_exceptiontable_startup.s 15947 2014-04-28 10:16:48Z masa8317 $
*/

/* DCG Deviations:
 *
 * DCG-1) Deviated Rule: [OS_LAYOUT_020]
 *  Source files shall contain at most one routine having external linkage.
 *
 * Reason:
 *  This file contains a set of exception vectors.
 *  The exception is permitted by the DCG.
 *
 * DCG-2) Deviated Rule: [OS_ASM_STRUCT_020]
 *  Each assembly file shall contain exactly one return instruction, which is the last instruction.
 *
 * Reason:
 *  This file contains a set of exception vectors for use during startup, each of which is simply
 *  an endless loop. A RET instruction at the end of the file would be unreachable code.
*/
/* Deviation DCG-1 <*> */
/* Deviation DCG-2 <*> */

#include <private/Mk_asm.h>		/* Must be first! */
#include <private/TRICORE/Mk_TRICORE_core.h>

	MK_file(Mk_TRICORE_exceptiontable_startup.s)

/* The C handler functions for the exceptions
*/
	MK_ASM_SDECL_EXCEPTIONTABLE
	MK_ASM_SECTION_EXCEPTIONTABLE
	MK_ASM_ALIGN_EXCVECTORTABLE

	MK_global	MK_StartupExceptionTable

	MK_global	MK_StartupException0_VirtualAddress
	MK_global	MK_StartupException1_Protection
	MK_global	MK_StartupException2_Instruction
	MK_global	MK_StartupException3_Context
	MK_global	MK_StartupException4_BusError
	MK_global	MK_StartupException5_Assertion
	MK_global	MK_StartupException6_Syscall
	MK_global	MK_StartupException7_Nmi

/* MK_StartupExceptionTable
 *
 * The startup exception table is a set of 8 stub routines spaced at 32-byte intervals
 * from a base address that is aligned on a 256 byte boundary.
 *
 * This exception table is designed for use during startup, before exceptions can be handled
 * by the normal microkernel handlers.
 * Each exception therefore has an endless loop as an emergency stop. A DEBUG instruction
 * is placed into each loop so that if a debugger is attached the developer is notified
 * immediately.
*/
MK_StartupExceptionTable:

/*	Entry 0: Virtual address trap
*/
	MK_ASM_ALIGN_VECTORTABLEENTRY
MK_StartupException0_VirtualAddress:
	dsync
	debug
	j		MK_StartupException0_VirtualAddress

/*	Entry 1: Protection trap
*/
	MK_ASM_ALIGN_VECTORTABLEENTRY
MK_StartupException1_Protection:
	dsync
	debug
	j		MK_StartupException1_Protection

/*	Entry 2: Instruction trap
*/
	MK_ASM_ALIGN_VECTORTABLEENTRY
MK_StartupException2_Instruction:
	dsync
	debug
	j		MK_StartupException2_Instruction

/*	Entry 3: Context trap
*/
	MK_ASM_ALIGN_VECTORTABLEENTRY
MK_StartupException3_Context:
	dsync
	debug
	j		MK_StartupException3_Context

/*	Entry 4: Bus error trap
*/
	MK_ASM_ALIGN_VECTORTABLEENTRY
MK_StartupException4_BusError:
	dsync
	debug
	j		MK_StartupException4_BusError

/*	Entry 5: Assertion trap
*/
	MK_ASM_ALIGN_VECTORTABLEENTRY
MK_StartupException5_Assertion:
	dsync
	debug
	j		MK_StartupException5_Assertion

/*	Entry 6: System call
*/
	MK_ASM_ALIGN_VECTORTABLEENTRY
MK_StartupException6_Syscall:
	dsync
	debug
	j		MK_StartupException6_Syscall

/*	Entry 7: NMI
*/
	MK_ASM_ALIGN_VECTORTABLEENTRY
MK_StartupException7_Nmi:
	dsync
	debug
	j		MK_StartupException7_Nmi

/* Editor settings: DO NOT DELETE
 * vi:set ts=4:
*/
