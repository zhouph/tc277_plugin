/* Mk_u_libgetalarm.c
 *
 * This file contains the GetAlarm() function.
 *
 * (c) Elektrobit Automotive GmbH
 *
 * $Id: Mk_u_libgetalarm.c 15821 2014-04-16 04:55:15Z masa8317 $
*/
#include <public/Mk_public_types.h>
#include <public/Mk_public_api.h>
#include <public/Mk_autosar.h>

/* GetAlarm() - Implements the AUTOSAR service.
 *
 * This function calls the GetAlarm API of the QM-OS via the microkernel's delegation interface.
 * If the returned status code is E_OK, the requested value is placed in the referenced TickType variable.
 * The function returns the status code.
 *
 * !LINKSTO      Microkernel.Function.GetAlarm, 1
 * !doctype      src
*/
StatusType GetAlarm(AlarmType alarmId, TickRefType tickRef)
{
	mk_statusandvalue_t syscallReturn = MK_UsrStartForeignThread2V(MK_SC_GetAlarm,
															(mk_parametertype_t)alarmId, (mk_parametertype_t)tickRef);

	if ( syscallReturn.statusCode == E_OK )
	{
		*tickRef = (TickType)syscallReturn.requestedValue;
	}

	return (StatusType)syscallReturn.statusCode;
}

/* Editor settings; DO NOT DELETE
 * vi:set ts=4:
*/
