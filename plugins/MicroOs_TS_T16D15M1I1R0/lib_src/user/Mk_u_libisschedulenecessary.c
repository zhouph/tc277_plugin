/* Mk_u_libisschedulenecessary.c
 *
 * This file contains the MK_LibIsScheduleNecessary() function.
 *
 * (c) Elektrobit Automotive GmbH
 *
 * $Id: Mk_u_libisschedulenecessary.c 15821 2014-04-16 04:55:15Z masa8317 $
*/
#include <public/Mk_public_types.h>
#include <public/Mk_public_api.h>
#include <private/Mk_thread.h>
#include <private/Mk_resource.h>

/* MK_LibIsScheduleNecessary() returns MK_TRUE if there is a thread of higher
 * priority enqueued behind the current thread.
 * Otherwise it returns MK_FALSE.
 *
 * !LINKSTO      Microkernel.Function.MK_LibIsScheduleNecessary, 1
 * !doctype      src
*/
mk_boolean_t MK_LibIsScheduleNecessary(void)
{
	mk_resource_t *resource;
	mk_threadprio_t lowPrio;
	mk_boolean_t result = MK_FALSE;

	if ( MK_threadCurrent->next != MK_NULL )
	{
		/* Calculate the priority that the thread has to drop to.
		 * As basis, use the queueing priority.
		 */
		lowPrio = MK_threadCurrent->queueingPriority;

		/* However, it should not drop lower than the highest resource taken, so
		 * walk the list of resources and adjust the priority to drop to.
		 */
		resource = MK_threadCurrent->lastResourceTaken;
		while ( resource != MK_NULL )
		{
			if ( resource->ceilingPriority > lowPrio )
			{
				lowPrio = resource->ceilingPriority;
			}
			resource = resource->previousTaken;
		}

		if ( MK_threadCurrent->next->currentPriority > lowPrio )
		{
			result = MK_TRUE;
		}
		/* else ... (omitted)
		 * No higher-priority threads waiting.
		 */
	}

	return result;
}

/* Editor settings; DO NOT DELETE
 * vi:set ts=4:
*/
