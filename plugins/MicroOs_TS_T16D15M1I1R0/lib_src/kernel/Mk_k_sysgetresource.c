/* Mk_k_sysgetresource.c
 *
 * This file contains the function MK_SysGetResource()
 *
 * (c) Elektrobit Automotive GmbH
 *
 * $Id: Mk_k_sysgetresource.c 15908 2014-04-24 07:14:25Z masa8317 $
*/
#include <public/Mk_public_types.h>
#include <private/Mk_syscall.h>
#include <private/Mk_resource.h>
#include <private/Mk_thread.h>
#include <private/Mk_errorhandling.h>
#include <public/Mk_public_api.h>

/* MK_SysGetResource() acquires the resource identified by the parameter.
 *
 * This function is present in the system call table and is called whenever a thread makes
 * a "GetResource" system call.
 *
 * !LINKSTO Microkernel.Function.MK_SysGetResource, 1
 * !doctype src
*/
void MK_SysGetResource(void)
{
	mk_objectid_t resourceId = (mk_objectid_t)MK_HwGetParameter1(MK_threadCurrent);
	mk_resource_t *resource;
	mk_errorid_t errorCode = MK_eid_Unknown;

	if ( (resourceId >= 0) && (resourceId < MK_nResources) )
	{
		errorCode = MK_eid_NoError;

		resource = &MK_resource[resourceId];

		if ( resource->count == 0 )
		{
			resource->count = 1;

			/* Save current thread's resource status in the newly-occupied resource
			*/
			resource->savedPrio = MK_threadCurrent->currentPriority;
			resource->savedLevel = MK_HwGetIntLevel(MK_threadCurrent);
			resource->previousTaken = MK_threadCurrent->lastResourceTaken;

			/* Set the current thread's resource status to the newly-occupied resource
			 * ensuring that priorities and levels don't decrease!
			*/
			resource->owner = MK_threadCurrent;
			MK_threadCurrent->lastResourceTaken = resource;

			/* The lock level does not need to be considered here; if current priority
			 * is greater or equal to the ceiling priority, the lock level has already
			 * been set approriately.
			*/
			if ( MK_threadCurrent->currentPriority < resource->ceilingPriority )
			{
				/* No need to requeue here because the non-reentrant nature of the kernel
				 * guarantees that the current thread is at the head of the thread queue
				*/
				MK_threadCurrent->currentPriority = resource->ceilingPriority;
				MK_HwSetIntLevel(MK_threadCurrent, resource->lockLevel);
			}
		}
		else
		if ( resource->owner == MK_threadCurrent )
		{
			if ( resource->count < resource->maxCount )
			{
				resource->count++;
			}
			else
			{
				errorCode = MK_eid_ResourceNestingLimitExceeded;
			}
		}
		else
		{
			/* This branch could occur legally if an interrupt-lock resource is
			 * acquired from a thread that has a higher priority than the resource's
			 * ceiling priority. For example:
			 *	- ErrorHook : both locking resources
			 *	- ProtectionHook : both locking resources
			 *	- Cat1 ISR : the Cat2-locking resource
			*/
			if ( ( (resourceId == MK_resLockCat1) || (resourceId == MK_resLockCat2) ) &&
				 ( MK_threadCurrent->queueingPriority > resource->ceilingPriority ) )
			{
				/* In these cases (and only these cases!) no error is reported. The thread's
				 * priority is already higher than the resource's ceiling priority, so interrupts
				 * are already locked. See also corresponding branch in Mk_k_sysreleaseresource.c
				*/
			}
			else
			{
				errorCode = MK_eid_ResourceAlreadyOccupied;
			}
		}
	}
	else
	{
		errorCode = MK_eid_InvalidResourceId;
	}

	if ( errorCode == MK_eid_NoError )
	{
		MK_HwSetReturnValue1(MK_threadCurrent, MK_E_OK);
	}
	else
	{
		(void)MK_ReportError(MK_sid_GetResource, errorCode, MK_threadCurrent);
	}
}

/* Editor settings; DO NOT DELETE
 * vi:set ts=4:
*/
