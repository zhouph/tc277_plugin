/* Mk_k_reporterror.c
 *
 * This file contains the function MK_ReportError()
 *
 * (c) Elektrobit Automotive GmbH
 *
 * $Id: Mk_k_reporterror.c 17616 2014-11-03 06:51:02Z stpo8218 $
*/
/* DCG Deviations:
 *
 * DCG-1) Deviated Rule: [OS_C_COMPL_010:vocf]
 *  The code shall adhere to the [HISSRCMETRIC] Metrics.
 *
 * Reason:
 *  Filling the error information structure is the main reason for the high
 *  VOCF value, especially the cases where no parameter information is
 *  available.
*/
/* MISRA-C:2004 Deviation List
 *
 * MISRA-1) Deviated Rule: 12.6 (advisory)
 * The operands of logical operators (&&, || and !) should be effectively Boolean. Expressions that are
 * effectively Boolean should not be used as operands to operators other than (&&, || and !).
 *
 * Reason:
 *  False positive: The macro MK_HwIsRegisterStoreValid is always effectively Boolean.
*/

/* Deviation DCG-1 <*> */

#include <public/Mk_public_types.h>
#include <private/Mk_thread.h>
#include <public/Mk_error.h>
#include <private/Mk_errorhandling.h>

/* MK_ReportError() - report an error
 *
 * An error is reported by some combination (depending on configuration) of
 * - filling the MK_errorInfo structure
 * - calling (starting a thread for) the ErrorHook() function
 * - passing the AUTOSAR error code back to the caller
 *
 * !LINKSTO Microkernel.Function.MK_ReportError, 1
 * !doctype src
*/
mk_boolean_t MK_ReportError(mk_serviceid_t sid, mk_errorid_t eid, mk_thread_t *culprit)
{
	mk_osekerror_t osekError = MK_ErrorInternalToOsek(eid);
	mk_boolean_t fillErrorInfo = MK_TRUE;

	if ( MK_errorHookThread == MK_NULL )
	{
		/* No error-hook thread is configured. Just record the error in the error-info structure
		 * and return the error code.
		*/
	}
	else
	{
		if  ( MK_errorHookThread->state == MK_THS_IDLE )
		{
			/* Error-hook thread is idle. We can start the thread provided it will preempt the current
			 * thread. In any case we can record the error in the error-info structure.
			*/
			if ( MK_errorHookThreadConfig->queuePrio > MK_threadCurrent->currentPriority )
			{
				/* Error-hook thread can preempt current thread. The thread can be started.
				*/
				MK_StartThread(MK_errorHookThread, MK_errorHookThreadConfig);
				MK_errorHookThread->parent = culprit;

				/* Preload the AUTOSAR error code parameter for the ErrorHook function
				*/
				MK_HwSetParameter1(MK_errorHookThread,  osekError);
			}
		}
		else
		{
			if ( (MK_errorHookThread->objectType == MK_OBJTYPE_ERRORHOOK) )
			{
				/* Error hook thread is currently running the error hook --> a nested error. In this case
				 * we do not record the error in the error-info structure.
				*/
				fillErrorInfo = MK_FALSE;
			}
		}
	}

	if ( fillErrorInfo && (MK_errorInfo != MK_NULL) )
	{
		/* Everything we know about the error is stored into the global error-info structure
		 * if there is one. This is also the case if the error hook shares its thread with
		 * another object and this object caused the error (note that fillErrorInfo will be
		 * MK_TRUE in this case).
		 * The "culprit" thread is not guaranteed to be valid after the error hook terminates
		 * but is stored for debugging purposes.
		 * For errors caused directly in a thread (for example, errors reported by library
		 * functions that are called directly) the parameters will be wrong. MK_SysReportError
		 * will sort this out later.
		*/
		MK_errorInfo->serviceId = sid;
		MK_errorInfo->errorId = eid;
		MK_errorInfo->osekError = osekError;
		MK_errorInfo->culprit = culprit;

		if ( culprit == MK_NULL )
		{
			MK_errorInfo->culpritId = 0;
			MK_errorInfo->culpritType = MK_OBJTYPE_KERNEL;
			MK_errorInfo->culpritName = MK_NULL;
			MK_errorInfo->parameter[0] = MK_PARAMETERTYPE_INVALID;
			MK_errorInfo->parameter[1] = MK_PARAMETERTYPE_INVALID;
			MK_errorInfo->parameter[2] = MK_PARAMETERTYPE_INVALID;
			MK_errorInfo->parameter[3] = MK_PARAMETERTYPE_INVALID;
		}
		else
		{
			MK_errorInfo->culpritId = culprit->currentObject;
			MK_errorInfo->culpritType = culprit->objectType;
			MK_errorInfo->culpritName = culprit->name;
			if ( MK_HwIsRegisterStoreValid(culprit) )
			{
				MK_errorInfo->parameter[0] = MK_HwGetParameter1(culprit);
				MK_errorInfo->parameter[1] = MK_HwGetParameter2(culprit);
				MK_errorInfo->parameter[2] = MK_HwGetParameter3(culprit);
				MK_errorInfo->parameter[3] = MK_HwGetParameter4(culprit);
			}
			else
			{
				MK_errorInfo->parameter[0] = MK_PARAMETERTYPE_INVALID;
				MK_errorInfo->parameter[1] = MK_PARAMETERTYPE_INVALID;
				MK_errorInfo->parameter[2] = MK_PARAMETERTYPE_INVALID;
				MK_errorInfo->parameter[3] = MK_PARAMETERTYPE_INVALID;
			}
		}
	}
	else
	{
		/* Indicate that the error-info structure was not filled.
		*/
		fillErrorInfo = MK_FALSE;
	}

	/* Deviation MISRA-1 */
	if ( (culprit != MK_NULL) && (MK_HwIsRegisterStoreValid(culprit)) )
	{
		MK_HwSetReturnValue1(culprit, osekError);
	}

	return fillErrorInfo;
}

/* Editor settings; DO NOT DELETE
 * vi:set ts=4:
*/
