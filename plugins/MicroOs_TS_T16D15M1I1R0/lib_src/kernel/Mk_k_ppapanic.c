/* Mk_k_ppapanic.c
 *
 * This file contains the MK_PpaPanic() function.
 *
 * (c) Elektrobit Automotive GmbH
 *
 * $Id: Mk_k_ppapanic.c 15821 2014-04-16 04:55:15Z masa8317 $
*/
#include <public/Mk_public_types.h>
#include <private/Mk_panic.h>
#include <private/Mk_errorhandling.h>

/* MK_PpaPanic()
 *
 * !LINKSTO Microkernel.Function.MK_PpaPanic, 1
 * !doctype src
*/
void MK_PpaPanic(void)
{
	/* !LINKSTO Microkernel.Panic.MK_panic_PanicFromProtectionHook, 1
	 * !doctype src
	*/
	MK_Panic(MK_panic_PanicFromProtectionHook);
}

/* Editor settings; DO NOT DELETE
 * vi:set ts=4:
*/
