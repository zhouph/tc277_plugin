/* Mk_k_sysselftest.c
 *
 * This file contains the function MK_SysSelftest()
 *
 * (c) Elektrobit Automotive GmbH
 *
 * $Id: Mk_k_sysselftest.c 16024 2014-05-07 10:16:55Z dh $
*/

/* DCG Deviations:
 *
 * DCG-1) Deviated Rule: [OS_C_COMPL_010:vocf]
 *  The code shall adhere to the [HISSRCMETRIC] Metrics.
 *
 * Reason:
 *  The respective actions that are preformed for a selected selftest
 *  feature are very similar. This leads to a high VOCF value.
*/
/* Deviation DCG-1 <*> */

#include <public/Mk_public_types.h>
#include <private/Mk_thread.h>
#include <private/Mk_errorhandling.h>
#include <private/Mk_syscall.h>
#include <private/Mk_memoryprotection.h>
#include <private/Mk_interrupt.h>

/* MK_SysSelftest() - performs selftests on kernel and/or hardware.
 * Depending on the testType parameter, work is delegated to
 * MK_SelftestExecBudget() or MK_GetSelftestState().
 *
 * !LINKSTO Microkernel.Function.MK_SysSelftest, 1
 * !doctype src
*/
void MK_SysSelftest(void)
{
	const mk_uint32_t feature = MK_HwGetParameter1(MK_threadCurrent);
	const mk_uint32_t param = MK_HwGetParameter2(MK_threadCurrent);

	if ( feature == (mk_uint32_t)MK_SELFTEST_EXECBUDGET )
	{
		/* Call the execution protection selftest function
		 * and report error code or MK_E_OK to caller.
		*/
		mk_errorid_t errorCode = MK_eid_Unknown;
		errorCode = MK_SelftestExecBudget(param);

		if ( errorCode == MK_eid_NoError )
		{
			MK_HwSetReturnValue1(MK_threadCurrent, MK_E_OK);
		}
		else
		{
			(void)MK_ReportError(MK_sid_Selftest, errorCode, MK_threadCurrent);
		}
	}
	else
	if ( feature == (mk_uint32_t)MK_SELFTEST_STATE )
	{
		/* Call the selftest state getter and return the state
		 * to the caller instead of an error code.
		*/
		mk_uint32_t state;
		state = MK_GetSelftestState(param);
		MK_HwSetReturnValue1(MK_threadCurrent, state);
	}
	else
	if ( feature == (mk_uint32_t)MK_SELFTEST_MPUCONFIG )
	{
		/* Call the memory protection selftest function
		 * and report error code or MK_E_OK to caller.
		*/
		mk_errorid_t errorCode = MK_eid_Unknown;
		errorCode = MK_HwCheckMemoryProtection();

		if ( errorCode == MK_eid_NoError )
		{
			MK_HwSetReturnValue1(MK_threadCurrent, MK_E_OK);
		}
		else
		{
			(void)MK_ReportError(MK_sid_Selftest, errorCode, MK_threadCurrent);
		}
	}
	else
	if ( feature == (mk_uint32_t)MK_SELFTEST_INTERRUPTCONTROLLER )
	{
		/* Call the interrupt controller selftest function
		 * and report error code or MK_E_OK to caller.
		*/
		mk_errorid_t errorCode = MK_eid_Unknown;
		errorCode = MK_HwCheckInterruptController();

		if ( errorCode == MK_eid_NoError )
		{
			MK_HwSetReturnValue1(MK_threadCurrent, MK_E_OK);
		}
		else
		{
			(void)MK_ReportError(MK_sid_Selftest, errorCode, MK_threadCurrent);
		}
	}
	else
	{
		/* The feature parameter does not designate a valid selftest feature,
		 * so report an error.
		*/
		(void)MK_ReportError(MK_sid_Selftest, MK_eid_InvalidSelftestFeature, MK_threadCurrent);
	}
}

/* Editor settings; DO NOT DELETE
 * vi:set ts=4:
*/
