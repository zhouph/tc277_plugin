/* Mk_k_syswaitevent.c
 *
 * This file contains the MK_SysWaitEvent() function.
 *
 * This function is called by the system call function whenever the WaitEvent system call is made.
 *
 * (c) Elektrobit Automotive GmbH
 *
 * $Id: Mk_k_syswaitevent.c 16010 2014-05-06 09:44:50Z dh $
*/
#include <public/Mk_public_types.h>
#include <public/Mk_syscallindex.h>
#include <private/Mk_syscall.h>
#include <private/Mk_thread.h>
#include <private/Mk_task.h>
#include <private/Mk_errorhandling.h>

/*
 * MK_SysWaitEvent() places the current thread's EXTENDED task into the WAITING state.
 *
 * This function is present in the system call table and is called whenever a thread makes
 * a "WaitEvent" system call.
 *
 * !LINKSTO Microkernel.Function.MK_SysWaitEvent, 1
 * !doctype src
*/
void MK_SysWaitEvent(void)
{
	mk_uint32_t eventsAwaited;
	mk_eventstatus_t *eventStatus;
	mk_uint_fast16_t syscallIndex = (mk_uint_fast16_t)MK_HwGetSyscallIndex(MK_threadCurrent);

	mk_errorid_t errorCode = MK_eid_Unknown;

	if ( MK_threadCurrent->objectType == MK_OBJTYPE_TASK )
	{
		eventStatus = MK_taskCfg[MK_threadCurrent->currentObject].eventStatus;

		if ( eventStatus == MK_NULL )
		{
			errorCode = MK_eid_TaskIsNotExtended;
		}
		else
		{
			errorCode = MK_eid_NoError;

			eventsAwaited = MK_HwGetParameter1(MK_threadCurrent);

			/* Preload an E_OK return value for the task that (might) wait.
			 * At the end of the function it is too late; a new task might already be
			 * sitting in the current thread. This comes *after" reading eventAwaited in
			 * case the 1st parameter and the return value use the same register.
			*/
			MK_HwSetReturnValue1(MK_threadCurrent, MK_E_OK);

			if ( (eventsAwaited == 0u) ||
				 ((eventStatus->pendingEvents & eventsAwaited) != 0u) )
			{
				/* Simply return with E_OK.
				 *
				 * The "eventsAwaited == 0" case for WaitEvent is really an error, but
				 * AUTOSAR has no requirement to detect and report it. Simply handling
				 * the request just like normal would result in the task getting into a
				 * strange "zombie" state.
				 *
				 * In the second case of the decision an awaited event is already set.
				*/
				if ( syscallIndex == MK_SC_WaitGetClearEvent )
				{
					/* If the system call was the WaitGetClear variant, we return (and clear)
					 * the pending events. This has an interesting side effect; WaitGetClearEvent(0) can
					 * be used as a combined Get/Clear without waiting :-)
					*/
					MK_HwSetReturnValue2(MK_threadCurrent, eventStatus->pendingEvents);
					eventStatus->pendingEvents = 0;
				}
			}
			else
			{
				/* None of the awaited events is already pending.
				 * The task enters the WAITING state.
				*/
				eventStatus->awaitedEvents = eventsAwaited;

				/* Place the task into one of the possible  WAITING states.
				*/
				if ( syscallIndex == MK_SC_WaitGetClearEvent )
				{
					eventStatus->waitingState = MK_WAITGETCLEAR;
				}
				else
				{
					eventStatus->waitingState = MK_WAIT;
				}

				/* Dequeue the current thread from the head of the thread queue.
				 * (Precondition for MK_TerminateThread())
				*/
				MK_threadQueueHead = MK_threadQueueHead->next;
				MK_threadCurrent->next = MK_NULL;

				/* Terminate the thread
				 *
				 * Note: do not free thread registers (which currently point to task registers)
				 * because although the thread gets terminated the task does not.
				*/
				MK_TerminateThread(MK_threadCurrent);

				/* A thread has been terminated. If there are no more threads the problem needs
				 * to be handled.
				*/
				if ( MK_threadQueueHead == MK_NULL )
				{
					MK_ThreadQueueEmpty();
				}
			}
		}
	}
	else
	{
		errorCode = MK_eid_ThreadIsNotATask;
	}

	if ( errorCode == MK_eid_NoError )
	{
		/* E_OK is not reported here because the current thread might no longer contain the calling task!
		*/
	}
	else
	{
		if ( syscallIndex == MK_SC_WaitGetClearEvent )
		{
			(void)MK_ReportError(MK_sid_WaitGetClearEvent, errorCode, MK_threadCurrent);
		}
		else
		{
			(void)MK_ReportError(MK_sid_WaitEvent, errorCode, MK_threadCurrent);
		}
	}
}

/* Editor settings; DO NOT DELETE
 * vi:set ts=4:
*/
