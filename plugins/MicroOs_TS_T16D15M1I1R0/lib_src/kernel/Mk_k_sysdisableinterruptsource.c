/* Mk_k_sysdisableinterruptsource.c
 *
 * This file contains the function MK_SysDisableInterruptSource()
 *
 * (c) Elektrobit Automotive GmbH
 *
 * $Id: Mk_k_sysdisableinterruptsource.c 15821 2014-04-16 04:55:15Z masa8317 $
*/
#include <public/Mk_public_types.h>
#include <private/Mk_syscall.h>
#include <private/Mk_thread.h>
#include <private/Mk_isr.h>
#include <private/Mk_errorhandling.h>

/* MK_SysDisableInterruptSource() disables an interrupt source belonging to an ISR.
 * First, a range check on the ISR id is performed.
 * Afterwards, the interrupt source is disabled.
 *
 * !LINKSTO Microkernel.Function.MK_SysDisableInterruptSource, 1
 * !doctype src
*/
void MK_SysDisableInterruptSource(void)
{
	const mk_objectid_t isrId = (mk_objectid_t) MK_HwGetParameter1(MK_threadCurrent);

	/* Range-check on the ISR id
	*/
	if ( (isrId >= 0) && (isrId < MK_nIsrs) )
	{
		/* Interrupt can be disabled regardless of the state of the OS-application that owns it.
		 * A terminated application's interrupts should be disabled anyway, so not checking is
		 * mostly harmless. This will change when AS4.0 semantics are adopted.
		*/
		MK_HwDisableIrq(MK_isrCfg[isrId].irq);
		MK_HwSetReturnValue1(MK_threadCurrent, MK_E_OK);
	}
	else
	{
		(void)MK_ReportError(MK_sid_DisableInterruptSource, MK_eid_InvalidIsrId, MK_threadCurrent);
	}
}

/* Editor settings; DO NOT DELETE
 * vi:set ts=4:
*/
