/* Mk_k_syssetevent.c
 *
 * This file contains the MK_SysSetEvent() function.
 *
 * This function is called by the system call function whenever the SetEvent system call is made.
 *
 * (c) Elektrobit Automotive GmbH
 *
 * $Id: Mk_k_syssetevent.c 15908 2014-04-24 07:14:25Z masa8317 $
*/
#include <public/Mk_public_types.h>
#include <private/Mk_syscall.h>
#include <private/Mk_thread.h>
#include <private/Mk_jobqueue.h>
#include <private/Mk_task.h>
#include <private/Mk_errorhandling.h>

/*
 * MK_SysSetEvent() set the given event(s) in the specified EXTENDED task's pending events state and
 * if the task is waiting for one or more pending events, reactivates it on its thread.
 *
 * This function is present in the system call table and is called whenever a thread makes
 * a "SetEvent" system call.
 *
 * !LINKSTO Microkernel.Function.MK_SysSetEvent, 1
 * !doctype src
*/
void MK_SysSetEvent(void)
{
	mk_objectid_t taskId = (mk_objectid_t)MK_HwGetParameter1(MK_threadCurrent);
	const mk_taskcfg_t *taskCfg;
	const mk_threadcfg_t *threadCfg;
	mk_thread_t *thread;
	mk_eventstatus_t *eventStatus;
	mk_uint32_t eventsToSet;

	mk_errorid_t errorCode = MK_eid_Unknown;

	if ( (taskId >= 0) && (taskId < MK_nTasks) )
	{
		taskCfg = &MK_taskCfg[taskId];
		eventStatus = taskCfg->eventStatus;

		if ( eventStatus == MK_NULL )
		{
			errorCode = MK_eid_TaskIsNotExtended;
		}
		else
		{
			errorCode = MK_eid_NoError;

			/* The task is a valid extended task.
			*/
			eventsToSet = MK_HwGetParameter2(MK_threadCurrent);

			eventStatus->pendingEvents |= eventsToSet;

			if ( (eventStatus->pendingEvents & eventStatus->awaitedEvents) != 0u )
			{
				/* awaitedEvents != 0 is used as an indication that the task is waiting,
				 * so it must be set to 0 when leaving the WAITING state.
				*/
				eventStatus->awaitedEvents = 0u;

				thread = taskCfg->thread;

				if ( thread->state == MK_THS_IDLE )
				{
					threadCfg = &taskCfg->threadCfg;

					thread->parent = MK_NULL;
					MK_RestartThread(thread, threadCfg);

					/* If the task used the WaitGetClearEvent system call, set the status to
					 * the dispatcher that it should do the Get and Clear parts of the API
					*/
					if ( eventStatus->waitingState == MK_WAITGETCLEAR )
					{
						thread->eventStatus = eventStatus;
					}

					/* Clear the task's "waiting" state; the task is no longer WAITING, it is now READY.
					*/
					eventStatus->waitingState = MK_NO_WAIT;
				}
				else
				{
					/* In this branch we don't change the "waiting" state because it gets used
					 * when the task comes out of the job queue (in MK_TerminateThread).
					*/
					MK_JqAppend(thread->jobQueue, (mk_jobid_t)taskId);
				}
			}
		}
	}
	else
	{
		errorCode = MK_eid_InvalidTaskId;
	}

	if ( errorCode == MK_eid_NoError )
	{
		MK_HwSetReturnValue1(MK_threadCurrent, MK_E_OK);
	}
	else
	{
		(void)MK_ReportError(MK_sid_SetEvent, errorCode, MK_threadCurrent);
	}
}

/* Editor settings; DO NOT DELETE
 * vi:set ts=4:
*/
