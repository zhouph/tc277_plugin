/* Mk_k_data.c
 *
 * This file contains the microkernel data (variables and constants) that don't depend on the configuration.
 *
 * (c) Elektrobit Automotive GmbH
 *
 * $Id: Mk_k_data.c 15835 2014-04-16 12:44:31Z dh $
*/
#include <public/Mk_public_types.h>
#include <private/Mk_thread.h>
#include <private/Mk_interrupt.h>
#include <private/Mk_startup.h>
#include <private/Mk_panic.h>
#include <private/Mk_accounting.h>

/* Global variables for thread/queue handling
*/
mk_thread_t *MK_threadCurrent;
mk_thread_t *MK_threadQueueHead;

/* Global variables for memory protection
*/
mk_objectid_t MK_currentMemoryPartition;

mk_objectid_t MK_firstVariableRegion;
mk_objquantity_t MK_nVariableRegions;
mk_objquantity_t MK_maxVariableRegions;

/* Global variable for error/panic handling
*/
mk_panic_t MK_panicReason = MK_panic_None;

/* Global variable for the extended timestamp timer
*/
mk_extendedtime_t MK_extendedTimer;

/* Global variable for the selftest functionality
*/
mk_uint32_t MK_selftestState;

/* Global variable to control the dispatcher's behavior.
*/
mk_boolean_t MK_execBudgetIsConfigured = MK_TRUE;

/* MK_initTest*
 * These variables permit a simple test (on startup) that the .data and .bss sections
 * have both been initialized. See MK_DataInitCheck().
*/
mk_uint32_t MK_initTestData = MK_INITTEST_VALUE;
mk_uint32_t MK_initTestBss;

/* Editor settings; DO NOT DELETE
 * vi:set ts=4:
*/
