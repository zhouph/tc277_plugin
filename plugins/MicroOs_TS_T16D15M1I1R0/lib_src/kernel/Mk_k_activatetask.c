/* Mk_k_activatetask.c
 *
 * This file contains the MK_ActivateTask function.
 *
 * This function is called by the system call function whenever the ActivateTask system call is made.
 *
 * (c) Elektrobit Automotive GmbH
 *
 * $Id: Mk_k_activatetask.c 15821 2014-04-16 04:55:15Z masa8317 $
*/
#include <public/Mk_public_types.h>
#include <private/Mk_thread.h>
#include <private/Mk_task.h>
#include <private/Mk_jobqueue.h>
#include <private/Mk_errorhandling.h>

/*
 * MK_ActivateTask() activates a task.
 *
 * This function is called to activate a task. It can be used in functions called
 * directly from the microkernel, such as internal interrupt routines.
 *
 * Precondition: taskId has been range-checked by the caller.
 *
 * !LINKSTO Microkernel.Function.MK_ActivateTask, 1
 * !doctype src
*/
mk_errorid_t MK_ActivateTask(mk_objectid_t taskId)
{
	const mk_taskcfg_t *taskCfg;
	mk_task_t *task;
	mk_thread_t *thread;
	mk_errorid_t errorCode = MK_eid_Unknown;
	mk_objectid_t aId;

	taskCfg = &MK_taskCfg[taskId];
	task = taskCfg->dynamic;

	if ( task->activationCount < taskCfg->maxActivations )
	{
		task->activationCount++;
		thread = taskCfg->thread;

		if ( taskCfg->eventStatus != MK_NULL )
		{
			/* Start with a clean slate of events.
			*/
			taskCfg->eventStatus->pendingEvents = 0u;
			taskCfg->eventStatus->awaitedEvents = 0u;
		}

		aId = taskCfg->threadCfg.applicationId;

		/* Tasks can be activated when they
		 * - either don't belong to an OS-Application
		 * - or belong to an OS-Application and are accessible
		*/
		if ( (aId == MK_APPL_NONE) || (MK_appStates[aId] == APPLICATION_ACCESSIBLE) )
		{
			if ( thread->state == MK_THS_IDLE )
			{
				MK_StartThread(thread, &taskCfg->threadCfg);
			}
			else
			{
				MK_JqAppend(thread->jobQueue, (mk_jobid_t)taskId);

				if ( taskCfg->eventStatus != MK_NULL )
				{
					/* Extended tasks get their registers preloaded here because when they
					 * are taken off the queue it is assumed they are restarting, not starting.
					 *
					 * Some processors need to allocate some space for register windows, so that
					 * must be done first.
					*/
					MK_HwAllocateThreadRegisters(taskCfg->threadCfg.regs);
					MK_FillThreadRegisters(&taskCfg->threadCfg);
				}
			}
			errorCode = MK_eid_NoError;
		}
		else
		{
			errorCode = MK_eid_Quarantined;
		}
	}
	else
	{
		errorCode = MK_eid_MaxActivationsExceeded;
	}

	return errorCode;
}

/* Editor settings; DO NOT DELETE
 * vi:set ts=4:
*/
