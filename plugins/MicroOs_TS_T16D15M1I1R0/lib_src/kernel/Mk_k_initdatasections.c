/* Mk_k_initdatasections.c
 *
 * This file contains the MK_InitDataSections function from the microkernel
 *
 * (c) Elektrobit Automotive GmbH
 *
 * $Id: Mk_k_initdatasections.c 18240 2015-02-06 11:07:29Z clad2899 $
*/

/* MISRA-C:2004 Deviation List
 *
 * MISRA-1) Deviated Rule: 17.3 (required)
 *  >, >=, <, <= shall not be applied to pointer types except where they point to the same array.
 *
 * Reason:
 *  This loop initializes an entire memory region between two addresses specified in the linker script.
 *  There is no array.
*/
#include <public/Mk_public_types.h>
#include <private/Mk_memoryprotection.h>
#include <private/Mk_startup.h>

#include <private/Mk_anondata.h>

/* MK_InitDataSections() - initialize all .data and .bss sections at startup
 *
 * This function initializes all memory regions by calling MK_InitMemoryRegion() for each
 * region that is configured.
 * In a fully memory-protected system all variables should be allocated to a memory region
 * somewhere. However, support is provided for "anonymous" .data and .bss sections.
 *
 * Note: this function is a *restricted C function*, i.e. it must not rely on the content
 * of global variables.
 *
 * !LINKSTO Microkernel.Function.MK_InitDataSections, 1
 * !doctype src
*/
void MK_InitDataSections(void)
{
	mk_objectid_t i;
	mk_uint32_t j;
	register mk_int8_t const *src;		/* Source of data to copy */
	register mk_int8_t *dst;		/* Destination of data to initialize */

	/* Initialize all configured memory regions.
	 *
	 * Note: MK_memoryRegions could be NULL here, but if it is, MK_nMemoryRegions is 0,
	 * so the loop body is not executed and the null pointer is not dereferenced.
	*/
	for ( i = 0; i < MK_nMemoryRegions; i++ )
	{
		MK_InitMemoryRegion(&MK_memoryRegions[i]);
	}

	/* Initialize the "anonymous" .data section by copying its content from ROM into RAM.
	*/
	j = 0;
	dst = &MK_ANON_DATA;
	src = &MK_ANON_IDAT;
	/* Deviation MISRA-1 */
	while ( &(dst[j]) < &MK_ANON_DATA_END )
	{
		dst[j] = src[j];
		j++;
	}

	/* Clear the anonymous .bss section. ".bss" section contains "uninitialized" C variables, which
	 * must have an initial value of zero according to the C standard.
	*/
	j = 0;
	dst = &MK_ANON_BSS;
	/* Deviation MISRA-1 */
	while ( &(dst[j]) < &MK_ANON_BSS_END )
	{
		dst[j] = 0;
		j++;
	}
	
	/* KCLim : Temp*/
	/* Initialize the "anonymous" .data section by copying its content from ROM into RAM.
	*/
	j = 0;
	dst = &MK_ANON_DATAMANDO;
	src = &MK_ANON_IDATMANDO;
	/* Deviation MISRA-1 */
	while ( &(dst[j]) < &MK_ANON_DATA_ENDMANDO )
	{
		dst[j] = src[j];
		j++;
	}
	
	/* Clear the anonymous .bss section. ".bss" section contains "uninitialized" C variables, which
	 * must have an initial value of zero according to the C standard.
	*/
	j = 0;
	dst = &MK_ANON_BSSMANDO;
	/* Deviation MISRA-1 */
	while ( &(dst[j]) < &MK_ANON_BSS_ENDMANDO )
	{
		dst[j] = 0;
		j++;
	}

	/* Clear the anonymous .bss section. ".bss" section contains "uninitialized" C variables, which
	 * must have an initial value of zero according to the C standard.
	*/
	j = 0;
	dst = &MK_ANON_XCP;
	/* Deviation MISRA-1 */
	while ( &(dst[j]) < &MK_ANON_XCP_END )
	{
		dst[j] = 0;
		j++;
	}
	
	
}

/* Editor settings; DO NOT DELETE
 * vi:set ts=4:
*/
