/* Mk_k_systerminateself.c
 *
 * This file contains the MK_SysTerminateSelf() function.
 *
 * (c) Elektrobit Automotive GmbH
 *
 * $Id: Mk_k_systerminateself.c 18628 2015-03-02 11:09:43Z nibo2437 $
*/

/* MISRA-C:2004 Deviation List
 *
 * MISRA-1) Deviated Rule: 13.7 (required)
 *  Boolean operations whose results are invariant shall not be permitted.
 *
 * Reason:
 *  A range check for both boundaries is safer than for just one boundary.
 *  The protectionAction variable is passed by the caller and could also have invalid values.
*/
#include <public/Mk_public_types.h>
#include <public/Mk_public_api.h>
#include <private/Mk_syscall.h>
#include <private/Mk_thread.h>
#include <private/Mk_task.h>
#include <private/Mk_errorhandling.h>
#include <private/Mk_panic.h>
#include <private/Mk_shutdown.h>

/*
 * MK_SysTerminateSelf() terminates the calling thread.
 *
 * This function is the kernel-side part of the "TerminateSelf()" API, which also implements
 * the TerminateTask() API.
 *
 * !LINKSTO Microkernel.Function.MK_SysTerminateSelf, 1
 * !doctype src
*/
void MK_SysTerminateSelf(void)
{
	if ( MK_threadCurrent->parent == MK_NULL )
	{
		/* Thread has no parent, so it could be a task
		*/
		if ( MK_threadCurrent->objectType == MK_OBJTYPE_TASK )
		{
			/* If the caller is a task, decrement the activation counter
			*/
			MK_taskCfg[MK_threadCurrent->currentObject].dynamic->activationCount--;
		}
	}
	else
	{
		/* Thread has a parent. This could mean:
		 *	- return value of thread main function needs to be returned to parent (OS threads etc.)
		 *	- AUTOSAR error code from global error status needs to be returned to parent (ErrorHook)
		 *	- return value of thread main function needs to be acted upon (ProtectionHook)
		*/
		if ( MK_threadCurrent->objectType == MK_OBJTYPE_PROTECTIONHOOK )
		{
			/* If the terminated thread was the protection hook thread, the first parameter contains
			 * the protection action to perform.
			*/
			mk_protectionaction_t protectionAction = (mk_protectionaction_t) MK_HwGetParameter1(MK_threadCurrent);

			/* Range check on the protection action
			*/
			/* Deviation MISRA-1 */
			if ( (protectionAction < 0) || (protectionAction > MK_PRO_INVALIDACTION) )
			{
				protectionAction = MK_PRO_INVALIDACTION;
			}

			/* Now perform this action
			*/
			(*MK_ppaFunctions[protectionAction])();
		}
		else
		if ( (MK_threadCurrent->objectType == MK_OBJTYPE_ERRORHOOK) ||
			 (MK_threadCurrent->objectType == MK_OBJTYPE_TRUSTEDFUNCTION) )
		{
			/* No special action required
			*/
		}
		else
		if ( MK_threadCurrent->objectType == MK_OBJTYPE_OS )
		{
			/* Copy thread's return values to the parent.
			*/
			if ( MK_HwIsRegisterStoreValid(MK_threadCurrent->parent) )
			{
				MK_HwSetReturnValue1(MK_threadCurrent->parent, MK_HwGetParameter1(MK_threadCurrent));
				MK_HwSetReturnValue2(MK_threadCurrent->parent, MK_HwGetParameter2(MK_threadCurrent));
			}
		}
		else
		{
			/* What's this?
			 * !LINKSTO Microkernel.Panic.MK_panic_UnexpectedThreadWithParent, 1
			 * !doctype src
			*/
			MK_Panic(MK_panic_UnexpectedThreadWithParent);
		}
	}

	/* Terminate the current thread, if it hasn't already happened
	*/
	if ( MK_threadCurrent->state != MK_THS_IDLE )
	{
		/* Dequeue the current thread from the head of the thread queue.
		 * (Precondition for MK_TerminateThread())
		 *
		 * The current thread *must* be at the head of the queue because nothing above activates any
		 * new threads except maybe protection hook actions. In that case, the terminating thread
		 * is the protection hook thread, which is the highest-priority thread in the system.
		*/
		MK_threadQueueHead = MK_threadQueueHead->next;
		MK_threadCurrent->next = MK_NULL;

		/* Terminate the thread
		*/
		MK_HwFreeThreadRegisters(MK_threadCurrent->regs);
		MK_TerminateThread(MK_threadCurrent);


		/* A thread has been terminated. If there are no more threads the problem needs to be handled.
		*/
		if ( MK_threadQueueHead == MK_NULL )
		{
			MK_ThreadQueueEmpty();
		}
	}
}

/* Editor settings; DO NOT DELETE
 * vi:set ts=4:
*/
