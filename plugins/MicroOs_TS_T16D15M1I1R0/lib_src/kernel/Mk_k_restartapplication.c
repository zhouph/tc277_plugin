/* Mk_k_restartapplication.c
 *
 * This file contains the MK_RestartApplication() function.
 *
 * (c) Elektrobit Automotive GmbH
 *
 * $Id: Mk_k_restartapplication.c 15821 2014-04-16 04:55:15Z masa8317 $
*/
#include <public/Mk_public_types.h>
#include <private/Mk_thread.h>
#include <private/Mk_task.h>
#include <private/Mk_panic.h>

/*
 * MK_RestartApplication() terminates all threads belonging to a given OS-Application and starts its restart thread.
 *
 * Preconditions:
 * - applicationId has already been range-checked by the caller (denotes a valid OS-Application)
 *
 * !LINKSTO Microkernel.Function.MK_RestartApplication, 1
 * !doctype src
*/
void MK_RestartApplication(mk_objectid_t applicationId)
{
	/* Determine the OS-Application's restart task
	*/
	mk_objectid_t restartTaskId = MK_appRestartTasks[applicationId];

	/* Terminate the given OS-Application
	 */
	MK_TerminateApplication(applicationId, MK_TRUE);

	if (restartTaskId >= 0)
	{
		/* Now start its restart task.
		 * We don't expect an error here, because
		 * - maxActivations cannot be exceeded
		 * - OS-Application can't be in state quarantined, because the OS-Application
		 *   was running when it called this function.
		 * - restartTaskId was range-checked (this happens in function MK_InitApplications())
		*/
		if (MK_ActivateTask(restartTaskId) != MK_eid_NoError)
		{
			/* !LINKSTO Microkernel.Panic.MK_panic_UnexpectedTaskActivationFailure, 1
			 * !doctype src
			*/
			MK_Panic(MK_panic_UnexpectedTaskActivationFailure);
		}
	}
	/* In case no restart task is configured, this is not an error (according to AUTOSAR).
	*/

}

/* Editor settings; DO NOT DELETE
 * vi:set ts=4:
*/
