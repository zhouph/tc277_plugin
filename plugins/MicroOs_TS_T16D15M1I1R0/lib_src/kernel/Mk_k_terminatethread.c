/* Mk_k_terminatethread.c
 *
 * This file contains the MK_TerminateThread() function.
 *
 * (c) Elektrobit Automotive GmbH
 *
 * $Id: Mk_k_terminatethread.c 16025 2014-05-07 10:23:06Z dh $
*/

/* MISRA-C:2004 Deviation List
 *
 * MISRA-1) Deviated Rule: 2.4 (advisory)
 *  Section of code should not be "commented out".
 *
 * Reason:
 *  This is not code, this is an implementation hint.
*/

#include <public/Mk_public_types.h>
#include <private/Mk_thread.h>
#include <private/Mk_resource.h>
#include <private/Mk_jobqueue.h>
#include <private/Mk_task.h>
#include <private/Mk_isr.h>
#include <private/Mk_panic.h>
/* Deviation MISRA-1 */
/*
#include <Dbg.h>
*/
/*
 * MK_TerminateThread() puts a thread into the idle state (without dequeueing)
 *
 * Preconditions:
 *	- the thread has already been dequeued
 *
 * !LINKSTO Microkernel.Function.MK_TerminateThread, 1
 * !doctype src
*/
void MK_TerminateThread(mk_thread_t *thread)
{
	mk_jobid_t job;

	/* Put thread in the idle state
	*/
	/* Deviation MISRA-1 */
	/*
		MK_TRACE_STATE_THREAD(thread->objectType, thread->currentObject,
								thread->state, MK_THS_IDLE);
	*/
	thread->state = MK_THS_IDLE;

	/* Release all resources held by the thread
	*/
	if ( thread->lastResourceTaken != MK_NULL )
	{
		MK_ClearResourceList(thread->lastResourceTaken);
		thread->lastResourceTaken = MK_NULL;
	}

	/* Re-activate the thread for the next job in the queue ...
	 * ... if there is one.
	 *
	 * If the thread has no job queue associated with it,
	 * it will only run one thread configuration per activation.
	*/
	if ( thread->jobQueue != MK_NULL )
	{
		job = MK_JqRemove(thread->jobQueue);

		if ( job != MK_NOJOB )
		{
			if ( thread->objectType == MK_OBJTYPE_TASK )
			{
				if ( MK_taskCfg[job].eventStatus == MK_NULL )
				{
					/* Basic tasks get started here
					*/
					MK_StartThread(thread, &MK_taskCfg[job].threadCfg);
				}
				else
				{
					/* Extended tasks get restarted here.
					 * If they are newly activated their registers have been preloaded.
					*/
					MK_RestartThread(thread, &MK_taskCfg[job].threadCfg);


					/* If the task used the WaitGetClearEvent system call, set the status to
					 * the dispatcher that it should do the Get and Clear parts of the API
					*/
					if ( MK_taskCfg[job].eventStatus->waitingState == MK_WAITGETCLEAR )
					{
						thread->eventStatus = MK_taskCfg[job].eventStatus;
					}

					/* Clear the task's "waiting" state; the task is no longer WAITING, it is now READY.
					*/
					MK_taskCfg[job].eventStatus->waitingState = MK_NO_WAIT;
				}
			}
			else
			{
				/* !LINKSTO Microkernel.Panic.MK_panic_JobIsNotTask, 1
				 * !doctype src
				*/
				MK_Panic(MK_panic_JobIsNotTask);
			}
		}
	}
}

/* Editor settings; DO NOT DELETE
 * vi:set ts=4:
*/
