/* Mk_k_startforeignthread.c
 *
 * This file contains the function MK_StartForeignThread() and the internal (static function)
 * MK_SelectForeignThread().
 *
 * (c) Elektrobit Automotive GmbH
 *
 * $Id: Mk_k_startforeignthread.c 17615 2014-11-03 06:50:49Z stpo8218 $
*/

/* MISRA-C:2004 Deviation List
 *
 * MISRA-1) Deviated Rule: 11.1 (required)
 *  Conversions shall not be performed between a pointer to a function and any type other
 *  than an integral type.
 *
 * Reason:
 *  The microkernel takes care of the parameter placement for different function types.
 *  It is better to use a function pointer type instead of a data pointer type which may
 *  be of a different size.
*/

/* DCG Deviations:
 *
 * DCG-1) Deviated Rule: [OS_C_COMPL_010:vocf]
 *  The code shall adhere to the [HISSRCMETRIC] Metrics.
 *
 * Reason:
 *  Starting a thread requires a lot of similar actions to be performed
 *  (e.g. when preloading the paramters to the thread's context).
 *  This leads to a high VOCF value.
*/

/* Deviation DCG-1 <*> */
#include <public/Mk_public_types.h>
#include <private/Mk_errorhandling.h>
#include <private/Mk_foreign.h>
#include <private/Mk_syscall.h>

/* This structure contains information about a selected foreign thread.
*/
struct mk_foreignthreadinfo_s
{
	mk_threadfunc_t fptr;						/* Pointer to the foreign thread's main function */
	mk_thread_t *foreignThread;					/* Pointer to the foreign thread */
	const mk_threadcfg_t *foreignThreadCfg;		/* Pointer to the foreign thread's configuration */
	mk_boolean_t isTrustedFunction;				/* Distinguishes between trusted functions and QM-OS functions */
	mk_objectid_t applicationId;				/* Id of the foreign thread's OS-Application */
};
typedef struct mk_foreignthreadinfo_s mk_foreignthreadinfo_t;

static mk_boolean_t MK_SelectForeignThread(mk_objectid_t, mk_boolean_t, mk_foreignthreadinfo_t *);


/* MK_SelectForeignThread()
 *
 * Selects a thread suitable for running a trusted function or a QM-OS function.
 * Reports an error if this is impossible. Returns the success of the operation.
 *
 * Parameters:
 * fIndex (input):				Denotes the function to run.
 * forceOsHighThread (input):	Ensures that the OS high thread is selected for QM-OS functions.
 *								Has no influence on trusted functions.
 * ftInfo (output):				Describes the thread and main function that was selected.
 *								If an error occurs, nothing is written.
 *
 * Return value:
 * MK_TRUE if a thread was selected, MK_FALSE otherwise.
 * In case no thread could be selected, an error is reported using MK_ReportError(),
 * and the members of ftInfo are undefined.
*/
static mk_boolean_t MK_SelectForeignThread(mk_objectid_t fIndex,
										   mk_boolean_t forceOsHighThread,
										   mk_foreignthreadinfo_t *ftInfo)
{
	mk_boolean_t wasSelected = MK_FALSE;
	const mk_threadprio_t currPrio = MK_threadCurrent->currentPriority;

	if ((fIndex >= MK_FIRST_QMOS_FUNCTION) && (fIndex <= MK_LAST_QMOS_FUNCTION))
	{
		/* Not a trusted function, so no special case for pointer parameters is required.
		*/
		ftInfo->isTrustedFunction = MK_FALSE;

		/* Not a trusted function, so the parent's application id is not inherited.
		*/
		ftInfo->applicationId = MK_APPL_NONE;

		/* The user wants to call a QM-OS function, so look it up.
		*/
		/* Deviation MISRA-1 */
		ftInfo->fptr = (mk_threadfunc_t) OS_callTable[fIndex - MK_FIRST_QMOS_FUNCTION];

		/* Get thread by priority.
		*/
		if ((currPrio < MK_osLowThreadConfig.queuePrio) && (!forceOsHighThread))
		{
			ftInfo->foreignThread = &MK_osThreadLow;
			ftInfo->foreignThreadCfg = &MK_osLowThreadConfig;
			wasSelected = MK_TRUE;
		}
		else
		if ((currPrio < MK_osHighThreadConfig.queuePrio) || forceOsHighThread)
		{
			ftInfo->foreignThread = &MK_osThreadHigh;
			ftInfo->foreignThreadCfg = &MK_osHighThreadConfig;
			wasSelected = MK_TRUE;
		}
		else
		{
			/* MK_StartForeignThread() was started from an incorrect context.
			*/
			(void) MK_ReportError((mk_serviceid_t)fIndex, MK_eid_QmOsCallFromWrongContext, MK_threadCurrent);
		}
	}
	else
	if ((MK_nTrustedFunctions > 0) && (fIndex >= MK_TRUSTEDFUNCTIONS_MIN) && (fIndex <= MK_TRUSTEDFUNCTIONS_MAX))
	{
		/* Trusted functions get a pointer parameter and need special treatment
		*/
		ftInfo->isTrustedFunction = MK_TRUE;

		/* Trusted functions inherit the application id of their parent
		 * which is at the head of the thread queue.
		*/
		ftInfo->applicationId = MK_threadQueueHead->applicationId;

		/* The user wants to call a trusted function, so look it up.
		*/
		/* Deviation MISRA-1 */
		ftInfo->fptr = (mk_threadfunc_t) MK_trustedFunctionsPtr[fIndex - MK_TRUSTEDFUNCTIONS_MIN];

		/* Get thread by priority.
		*/
		if ( currPrio < MK_tfLowThreadConfig.queuePrio )
		{
			ftInfo->foreignThread = &MK_tfThreadLow;
			ftInfo->foreignThreadCfg = &MK_tfLowThreadConfig;

			wasSelected = MK_TRUE;
		}
		else
		if ( currPrio < MK_tfHighThreadConfig.queuePrio )
		{
			ftInfo->foreignThread = &MK_tfThreadHigh;
			ftInfo->foreignThreadCfg = &MK_tfHighThreadConfig;

			wasSelected = MK_TRUE;
		}
		else
		{
			/* MK_StartForeignThread() was started from an incorrect context.
			*/
			(void) MK_ReportError(MK_sid_StartForeignThread, MK_eid_TfCallFromWrongContext, MK_threadCurrent);
		}
	}
	else
	{
		/* The caller passed an invalid function ID, i.e. the desired function could not be found.
		*/
		(void) MK_ReportError(MK_sid_StartForeignThread, MK_eid_InvalidForeignFunction, MK_threadCurrent);
	}

	return wasSelected;
}


/* MK_StartForeignThread() starts a thread to run the given function with up to three parameters.
 * The function to run can either be a QM-OS function or a trusted function.
 * If the functionIndex denotes a QM-OS function, the caller can force to use the OS high thread.
 * This is needed in the case the protection hook thread is at the head of the thread queue but will
 * get terminated before the dispatch happens.
 * A list of QM-OS functions is hardcoded in the microkernel.
 * A list of trusted functions is part of the configuration.
 *
 * !LINKSTO Microkernel.Function.MK_StartForeignThread, 1
 * !doctype src
*/
void MK_StartForeignThread(mk_objectid_t functionIndex,
						   mk_parametertype_t param1,
						   mk_parametertype_t param2,
						   mk_parametertype_t param3,
						   mk_boolean_t forceOsHighThread)
{
	mk_foreignthreadinfo_t ftInfo;

	/* Select an appropriate thread to run the foreign function.
	 * If not successful, do nothing, as the errors has already been reported.
	*/
	if (MK_SelectForeignThread(functionIndex, forceOsHighThread, &ftInfo))
	{
		if (ftInfo.foreignThread->state == MK_THS_IDLE)
		{
			/* Start the selected kernel thread to run the function.
			 * MK_RestartThread must be called first because it sets up foreignThread->registers, which is
			 * assumed later, and it sets a default value in foreignThread->currentObject, which we
			 * overwrite later.
			*/
			MK_RestartThread(ftInfo.foreignThread, ftInfo.foreignThreadCfg);

			/* Update the application id to the one set by MK_SelectForeignThread().
			*/
			ftInfo.foreignThread->applicationId = ftInfo.applicationId;

			/* Some processors need to allocate some space for register windows.
			*/
			MK_HwAllocateThreadRegisters(ftInfo.foreignThread->regs);

			/* Set the function to run. Putting the ID into the currentObject allows
			 * error handling to identify the OS service ID...
			*/
			ftInfo.foreignThread->currentObject = functionIndex;
			MK_HwSetMain(ftInfo.foreignThread, ftInfo.fptr);

			/* Set the foreignThread's parent to enable MK_HwThreadReturn to pass the
			 * return value back to the calling thread.
			*/
			if ( functionIndex == MK_SC_OS_StartOs )
			{
				ftInfo.foreignThread->parent = MK_NULL;
			}
			else
			{
				ftInfo.foreignThread->parent = MK_threadCurrent;
				if (MK_HwIsRegisterStoreValid(MK_threadCurrent))
				{
					MK_HwSetReturnValue1(MK_threadCurrent, MK_E_OK);
				}
			}

			MK_HwSetReturnAddress(ftInfo.foreignThread, &MK_HwThreadReturn);

			/* Preload the parameters for the foreign function. Only three parameters are possible!
			*/
			MK_HwSetParameter1(ftInfo.foreignThread, param1);
			MK_HwSetParameter2(ftInfo.foreignThread, param2);
			MK_HwSetParameter3(ftInfo.foreignThread, param3);
			MK_HwSetParameter4(ftInfo.foreignThread, 0);

			if (ftInfo.isTrustedFunction)
			{
				MK_HwSetParametersForTrustedFunction(ftInfo.foreignThread, param1, param2);
			}

			/* Set the remaining registers to the configured values.
			*/
			MK_HwSetPs(ftInfo.foreignThread, ftInfo.foreignThreadCfg->ps);
			MK_HwSetStackPointer(ftInfo.foreignThread, ftInfo.foreignThreadCfg->initialSp);
			MK_HwSetConstantRegisters(ftInfo.foreignThread);
		}
		else
		{
			/* The chosen thread is not idle. This is probably an internal error.
			*/
			(void) MK_ReportError(MK_sid_StartForeignThread, MK_eid_ThreadIsNotIdle, MK_threadCurrent);
		}
	}
}

/* Editor settings; DO NOT DELETE
 * vi:set ts=4:
*/
