/* Mk_k_sysgetapplicationid.c
 *
 * This file contains the function MK_SysGetApplicationId()
 *
 * (c) Elektrobit Automotive GmbH
 *
 * $Id: Mk_k_sysgetapplicationid.c 15821 2014-04-16 04:55:15Z masa8317 $
*/
#include <public/Mk_public_types.h>
#include <private/Mk_thread.h>
#include <private/Mk_syscall.h>
#include <public/Mk_autosar.h>

/* MK_SysGetApplicationId() - returns the ID of the current OS-Application
 *
 * !LINKSTO Microkernel.Function.MK_SysGetApplicationId, 1
 * !doctype src
*/
void MK_SysGetApplicationId(void)
{
	const mk_objectid_t applicationId = MK_GetApplicationId();
	MK_HwSetReturnValue1(MK_threadCurrent, applicationId);
}

/* Editor settings; DO NOT DELETE
 * vi:set ts=4:
*/
