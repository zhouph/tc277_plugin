/* Mk_k_jqremove.c
 *
 * This file contains the MK_JqRemove() function from the microkernel
 *
 * (c) Elektrobit Automotive GmbH
 *
 * $Id: Mk_k_jqremove.c 15821 2014-04-16 04:55:15Z masa8317 $
*/

/* MISRA-C:2004 Deviation List
 *
 * MISRA-1) Deviated Rule: 17.4 (required)
 *  Array indexing shall be the only allowed form of pointer arithmetic.
 *
 * Reason:
 *  Pointers are used here for efficiency. The boundary values are pre-calculated constants.
*/
#include <private/Mk_jobqueue.h>

/* MK_JqRemove() - remove the next job from a job queue and return it.
 *
 * Precondition:
 *	the jobqueue parameter is not NULL
 *
 * Postconditions:
 *	- queue was empty -> no change
 *	- queue was not empty -> next is one more than on entry (if > limit, base)
 *	- queue was not empty -> count is one less than on entry
 *
 * Note: there is no error checking for a NULL job queue; the caller guarantees that is is not NULL.
 *
 * !LINKSTO Microkernel.Function.MK_JqRemove, 1
 * !doctype src
*/
mk_jobid_t MK_JqRemove(mk_jobqueue_t *jq)
{
	mk_jobid_t job;

	if ( jq->count == 0u )
	{
		job = MK_NOJOB;
	}
	else
	{
		job = *jq->next;

		/* Deviation MISRA-1 */
		jq->next++;
		if ( jq->next >= jq->limit )
		{
			jq->next = jq->base;
		}
		jq->count--;
	}

	return job;
}

/* Editor settings; DO NOT DELETE
 * vi:set ts=4:
*/
