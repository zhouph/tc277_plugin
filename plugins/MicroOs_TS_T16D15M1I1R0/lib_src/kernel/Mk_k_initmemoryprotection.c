/* Mk_k_initmemoryprotection.c
 *
 * This file contains the function MK_InitMemoryProtection()
 *
 * (c) Elektrobit Automotive GmbH
 *
 * $Id: Mk_k_initmemoryprotection.c 15821 2014-04-16 04:55:15Z masa8317 $
*/
#include <public/Mk_public_types.h>
#include <private/Mk_memoryprotection.h>
#include <private/Mk_panic.h>

/* MK_InitMemoryProtection() initializes the memory protection subsystem
 *
 * On entry:
 *	- memory protection is disabled
 * On exit (provided memory protection is configured):
 *	- global region descriptors are programmed into MPU
 *	- MK_firstVariableRegion is calculated
 *	- memory protection is enabled
 *
 * !LINKSTO Microkernel.Function.MK_InitMemoryProtection, 1
 * !doctype src
*/
void MK_InitMemoryProtection(void)
{
	mk_objectid_t i;

	if ( MK_nMemoryPartitions > 0 )
	{
		/* Write the static partition to the MPU. Setting the variable region to the
		 * maximum first forces the function to clear the dynamic region too.
		 * The function might update MK_nVariableRegions to be the size of the
		 * static partition. That's wrong, but we overwrite that afterwards.
		*/
		MK_maxVariableRegions = MK_HWN_REGION_DESCRIPTOR;
		MK_nVariableRegions = MK_maxVariableRegions;

		MK_HwSetStaticMemoryPartition(MK_MEMPART_GLOBAL);

		/* Now we calculate the correct values for the variables. At the moment the dynamic region
		 * is empty so we set its size (MK_nVariableRegions) to 0
		*/
		MK_firstVariableRegion = MK_memoryPartitions[MK_MEMPART_GLOBAL].nRegions;
		MK_HwSetFirstVariableRegion(MK_firstVariableRegion);
		MK_maxVariableRegions = MK_HWN_REGION_DESCRIPTOR-MK_firstVariableRegion;
		MK_nVariableRegions = 0;
		MK_currentMemoryPartition = -1;

		/* Now we check all the memory partitions to make sure they fit.
		*/
		for ( i = 1; i < MK_nMemoryPartitions; i++ )
		{
			if ( MK_memoryPartitions[i].nRegions > MK_maxVariableRegions )
			{
				/* !LINKSTO Microkernel.Panic.MK_panic_MemoryPartitionIsTooLarge, 1
				 * !doctype src
				*/
				MK_StartupPanic(MK_panic_MemoryPartitionIsTooLarge);
			}
		}

		MK_HwEnableMemoryProtection();
	}
}

/* Editor settings; DO NOT DELETE
 * vi:set ts=4:
*/
