/* Mk_k_idle.c
 *
 * This file contains the function MK_Idle(). It runs in the idle thread during normal operation
 * and (with interrupts disabled) after shutdown.
 *
 * (c) Elektrobit Automotive GmbH
 *
 * $Id: Mk_k_idle.c 16006 2014-05-06 07:42:44Z dh $
*/
#include <public/Mk_public_types.h>
#include <private/Mk_thread.h>


/* MK_Idle() - the kernel/s default idle loop.
 *
 * The idle thread must never return, and must always be eligible to run. That means it must never
 * terminate itself or use a blocking kernel call.
 *
 * !LINKSTO Microkernel.Function.MK_Idle, 1
 * !doctype src
*/
void MK_Idle(void)
{
	for (;;)
	{
		/* Intentional endless loop
		*/
	}
}

/* Editor settings; DO NOT DELETE
 * vi:set ts=4:
*/
