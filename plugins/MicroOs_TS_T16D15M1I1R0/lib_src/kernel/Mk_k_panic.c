/* Mk_k_panic.c
 *
 * This file contains the MK_Panic function.
 *
 * This function is called by the microkernel whenever an internal problem is detected.
 *
 * (c) Elektrobit Automotive GmbH
 *
 * $Id: Mk_k_panic.c 16025 2014-05-07 10:23:06Z dh $
*/

/* MISRA-C:2004 Deviation List
 *
 * MISRA-1) Deviated Rule: 16.2 (required)
 *  Functions shall not call themselves, either directly or indirectly.
 *
 * Reason:
 *  MK_Panic can call itself again indirectly via MK_Dispatch and MK_ReportProtectionFault,
 *  but this can only happen if the protection hook thread has an execution budget and exceeds it.
 *  In the unlikely case that also the shutdown thread has and exceeded execution budget,
 *  the calls to MK_Dispatch and MK_ReportProtectionFault will result in a call to MK_PanicStop.
 *  So there is a maximum nesting of two calls to MK_Panic possible.
*/

#include <public/Mk_public_types.h>
#include <private/Mk_panic.h>
#include <public/Mk_error.h>
#include <private/Mk_shutdown.h>
#include <private/Mk_thread.h>

/* MK_Panic() reports kernel panics.
 *
 * In most cases it will be possible to reinitialize the thread queues enough to be able to shut
 * down the system in an orderly fashion. However, only one panic is allowed. If another occurs
 * we stop immediately.
 *
 * !LINKSTO Microkernel.Function.MK_Panic, 1
 * !doctype src
*/
/* Deviation MISRA-1 */
void MK_Panic(mk_panic_t panicReason)
{
	if ( MK_panicReason == MK_panic_None )
	{
		MK_panicReason = panicReason;

		MK_Shutdown(MK_E_PANIC);

		/* Now dispatch! This is because this function must *never* return to the caller.
		*/
		MK_Dispatch();
	}

	MK_PanicStop(panicReason);
}

/* Editor settings; DO NOT DELETE
 * vi:set ts=4:
*/
