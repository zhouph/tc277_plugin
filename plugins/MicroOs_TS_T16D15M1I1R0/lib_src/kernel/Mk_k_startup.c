/* Mk_k_startup.c
 *
 * This file contains the MK_Startup() function.
 *
 * (c) Elektrobit Automotive GmbH
 *
 * $Id: Mk_k_startup.c 15821 2014-04-16 04:55:15Z masa8317 $
*/
#include <public/Mk_public_types.h>
#include <public/Mk_public_api.h>
#include <private/Mk_startup.h>
#include <private/Mk_panic.h>

/* MK_Startup
 *
 * The MK_InitHardwareBeforeData() function is called before data initialization. This function
 * performs initialization that has not already been done by the "entry" code but must be done
 * before initialising the data. Typically this will be processor clock initialization to ensure
 * that the data initialization loops run at full speed. Watchdog initialization could also be done
 * here.
 *
 * The MK_InitHardwareAfterData() function is called after data initialization. This function
 * initializes any remaining hardware that the kernel needs.
 *
 * Call MK_StartKernel() which should never return.
 *
 * NOTE: This function is called by the assembly-language "entry"
 * code. At this point there isn't a full C environment available;
 * the .data and .bss sections have not been initialized, so
 * we cannot rely on the content of any variables with global lifetime.
 *
 * !LINKSTO Microkernel.Function.MK_Startup, 1
 * !doctype src
*/
void MK_Startup(mk_uint32_t p1, mk_uint32_t p2, mk_uint32_t p3, mk_uint32_t p4)
{
	/* This effectively compares each one of the key parameters for equality ("==")
	 * with its respective MK_MIDDLEKEY value.
	*/
	if ( ( (p1 ^ MK_MIDDLEKEY_1) | (p2 ^ MK_MIDDLEKEY_2) | (p3 ^ MK_MIDDLEKEY_3) | (p4 ^ MK_MIDDLEKEY_4) ) == 0u )
	{
		MK_InitHardwareBeforeData();

		MK_InitDataSections();

		MK_InitHardwareAfterData();

		MK_StartKernel(p2^MK_U(MK_TRANSKEY_1), p3^MK_U(MK_TRANSKEY_2), p4^MK_U(MK_TRANSKEY_3), p1^MK_U(MK_TRANSKEY_4));
	}
	else
	{
		/* !LINKSTO Microkernel.Panic.MK_panic_IncorrectStartupKey, 1
		 * !doctype src
		*/
		MK_StartupPanic(MK_panic_IncorrectStartupKey);
	}
}

/* Editor settings; DO NOT DELETE
 * vi:set ts=4:
*/
