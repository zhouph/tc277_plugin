/* Mk_k_reportprotectionfault.c
 *
 * This file contains the function MK_ReportProtectionFault()
 *
 * (c) Elektrobit Automotive GmbH
 *
 * $Id: Mk_k_reportprotectionfault.c 15821 2014-04-16 04:55:15Z masa8317 $
*/

/* MISRA-C:2004 Deviation List
 *
 * MISRA-1) Deviated Rule: 16.2 (required)
 *  Functions shall not call themselves, either directly or indirectly.
 *
 * Reason:
 *  MK_ReportProtectionFault can call itself again indirectly via MK_Panic and MK_Dispatch,
 *  but this can only happen if the protection hook thread has an execution budget and exceeds it.
 *  In the unlikely case that also the shutdown thread has and exceeded execution budget,
 *  the calls to MK_Panic, MK_Dispatch and MK_Panic again will result in a call to MK_PanicStop.
 *  So there is a maximum nesting of two calls to MK_ReportProtectionFault possible.
*/

#include <public/Mk_public_types.h>
#include <public/Mk_error.h>
#include <private/Mk_errorhandling.h>
#include <private/Mk_panic.h>
#include <private/Mk_shutdown.h>

/* MK_ReportProtectionFault() - report a protection violation
 *
 * A protection violation is reported by some combination (depending on configuration) of
 * - filling the MK_protectionInfo structure
 * - calling (starting a thread for) the ProtectionHook() function
 * - shutting down
 * - panicking ... if all else fails :-)
 *
 * !LINKSTO Microkernel.Function.MK_ReportProtectionFault, 1
 * !doctype src
*/
/* Deviation MISRA-1 */
void MK_ReportProtectionFault(mk_serviceid_t sid, mk_errorid_t eid)
{
	mk_osekerror_t osekError;

	osekError = MK_ErrorInternalToOsek(eid);

	if ( MK_protectionHookThread == MK_NULL )
	{
		/* No protection hook is configured. Just record the error, then shut down
		*/
		if ( MK_protectionInfo != MK_NULL )
		{
			MK_protectionInfo->serviceId = sid;
			MK_protectionInfo->errorId = eid;
			MK_protectionInfo->osekError = osekError;
			MK_protectionInfo->culprit = MK_threadCurrent;
		}
		MK_Shutdown(osekError);
	}
	else
	if ( MK_protectionHookThread->state == MK_THS_IDLE )
	{
		/* Protection-hook thread is idle. The thread can be started.
		*/
		if ( MK_protectionInfo != MK_NULL )
		{
			MK_protectionInfo->serviceId = sid;
			MK_protectionInfo->errorId = eid;
			MK_protectionInfo->osekError = osekError;
			MK_protectionInfo->culprit = MK_threadCurrent;
		}

		MK_StartThread(MK_protectionHookThread, MK_protectionHookThreadConfig);

		/* This ensures that the error code is handled on termination of the
		 * protection hook thread.
		*/
		MK_protectionHookThread->parent = MK_threadCurrent;

		/* Preload the AUTOSAR error code parameter for the ProtectionHook function
		*/
		MK_HwSetParameter1(MK_protectionHookThread,  osekError);
	}
	else
	{
		/* !LINKSTO Microkernel.Panic.MK_panic_ProtectionFaultInProtectionHook, 1
		 * !doctype src
		*/
		MK_Panic(MK_panic_ProtectionFaultInProtectionHook);
	}
}

/* Editor settings; DO NOT DELETE
 * vi:set ts=4:
*/
