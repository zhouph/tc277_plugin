/* Mk_k_sysgetisrid.c
 *
 * This file contains the function MK_SysGetIsrId()
 *
 * (c) Elektrobit Automotive GmbH
 *
 * $Id: Mk_k_sysgetisrid.c 15821 2014-04-16 04:55:15Z masa8317 $
*/
#include <public/Mk_public_types.h>
#include <private/Mk_syscall.h>
#include <private/Mk_thread.h>

/* MK_SysGetIsrId() - works out what is the current ISR by walking down the thread queue.
 *
 * !LINKSTO Microkernel.Function.MK_SysGetIsrId, 1
 * !doctype src
*/
void MK_SysGetIsrId(void)
{
	/* Find highest-priority ISR thread. Threads in all states are considered (threads in the queue cannot be idle).
	 *
	 * If pure software leveling is ever implemented it might be necessary to ignore NEW threads here.
	*/
	mk_thread_t *thread = MK_FindFirstThread(MK_OBJTYPE_ISR, MK_THS_IDLE);

	if ( thread == MK_NULL )
	{
		MK_HwSetReturnValue1(MK_threadCurrent, MK_INVALID_ISR);
	}
	else
	{
		MK_HwSetReturnValue1(MK_threadCurrent, thread->currentObject);
	}
}

/* Editor settings; DO NOT DELETE
 * vi:set ts=4:
*/
