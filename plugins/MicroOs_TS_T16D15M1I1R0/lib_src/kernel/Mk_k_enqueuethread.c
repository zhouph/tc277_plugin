/* Mk_k_enqueuethread.c
 *
 * This file contains the MK_EnqueueThread function.
 *
 * (c) Elektrobit Automotive GmbH
 *
 * $Id: Mk_k_enqueuethread.c 15821 2014-04-16 04:55:15Z masa8317 $
*/
#include <public/Mk_public_types.h>
#include <private/Mk_thread.h>


/* MK_EnqueueThread
 *
 * Insert a newly-activated thread at the correct place in the thread queue.
 *
 * !LINKSTO Microkernel.Function.MK_EnqueueThread, 1
 * !doctype src
*/
void MK_EnqueueThread(mk_thread_t *thread)
{
	mk_thread_t **pred_next = &MK_threadQueueHead;	/* pointer to the predecessor's "next" field */

	/* Find the first thread in the queue that has a lower priority than the thread-to-insert.
	 * Thus a newly-inserted thread gets placed *after* all higher- or equal-priority threads.
	*/
	while ( (*pred_next != MK_NULL) && ((*pred_next)->currentPriority >= thread->currentPriority) )
	{
		pred_next = &((*pred_next)->next);
	}

	/* Now we insert the thread between the predecessor and the successor.
	*/
	thread->next = *pred_next;
	*pred_next = thread;
}

/* Editor settings; DO NOT DELETE
 * vi:set ts=4:
*/
