/* Mk_k_errorinternaltoosek.c
 *
 * This file contains a table and a function to convert internal error codes
 * to AUTOSAR error codes.
 *
 * (c) Elektrobit Automotive GmbH
 *
 * $Id: Mk_k_errorinternaltoosek.c 15821 2014-04-16 04:55:15Z masa8317 $
*/

/* MISRA-C:2004 Deviation List
 *
 * MISRA-1) Deviated Rule: 8.7 (required)
 *  Objects shall be defined at block scope if they are only accessed from within a single function.
 *
 * Reason:
 *  Readability. Defining and initialising a large constant array inside a small function obscures the
 *  purpose of the function.
 *
 * MISRA-2) Deviated Rule: 13.7 (required)
 *  Boolean operations whose results are invariant shall not be permitted.
 *
 * Reason:
 *  A range check for both boundaries is safer than for just one boundary.
 *  The eid variable is passed by the caller and could also have invalid values.
*/
#include <public/Mk_public_types.h>
#include <public/Mk_error.h>
#include <private/Mk_errorhandling.h>

/* MK_ERRORLOOKUP is used to construct the lookup table. The 'internal' parameter is for documentation only
*/
#define MK_ERRORLOOKUP(internal, osek)		(osek)

/* MK_errorInternalToOsekTable[] - an array to convert internal error codes to AUTOSAR E_OS_xxx codes.
 *
 * This table provides a lookup from internal error code (MK_eid_*) to AUTOSAR-defined error codes
 * (E_OS_*). Each entry in the table contains an AUTOSAR code; the internal code is used as the index.
 * Therefore the entries in this table must be in the same order as the values in the mk_errorid_e
 * enumeration.
 *
 * The macro MK_ERRORLOOKUP() defined above is used to populate the table. The reasons for this are:
 *	- documentation. It's easier to read a macro than a comment after the E_OS_* value
 *	- error-checking. In principle the table could be expanded to a small structure that also contains
 *	  the internal code. Then the table could be checked at startup. Alternatively a "unit test"
 *	  could ensure that the values are correct, or a script could parse the file.
*/
/* Deviation MISRA-1 */
static const mk_osekerror_t MK_errorInternalToOsekTable[MK_eid_Unknown+1]=
{
	MK_ERRORLOOKUP(MK_eid_NoError,							MK_E_OK),

	MK_ERRORLOOKUP(MK_eid_AlignmentException,				E_OS_PROTECTION_EXCEPTION),
	MK_ERRORLOOKUP(MK_eid_ArithmeticException,				E_OS_PROTECTION_EXCEPTION),
	MK_ERRORLOOKUP(MK_eid_CallNestingException,				E_OS_PROTECTION_EXCEPTION),
	MK_ERRORLOOKUP(MK_eid_DebugEventException,				E_OS_PROTECTION_EXCEPTION),
	MK_ERRORLOOKUP(MK_eid_ExecutionBudgetExceeded,			E_OS_PROTECTION_TIME),
	MK_ERRORLOOKUP(MK_eid_HardwareUnavailableException,		E_OS_PROTECTION_EXCEPTION),
	MK_ERRORLOOKUP(MK_eid_IllegalInstructionException,		E_OS_PROTECTION_EXCEPTION),
	MK_ERRORLOOKUP(MK_eid_InvalidForeignFunction,			E_OS_SERVICEID),
	MK_ERRORLOOKUP(MK_eid_InvalidInterruptConfiguration,	E_OS_STATE),
	MK_ERRORLOOKUP(MK_eid_InvalidIsrId,						E_OS_ID),
	MK_ERRORLOOKUP(MK_eid_InvalidMpuConfiguration,			E_OS_STATE),
	MK_ERRORLOOKUP(MK_eid_InvalidOsApplication,				E_OS_ID),
	MK_ERRORLOOKUP(MK_eid_InvalidResourceId,				E_OS_ID),
	MK_ERRORLOOKUP(MK_eid_InvalidRestartParameter,			E_OS_VALUE),
	MK_ERRORLOOKUP(MK_eid_InvalidSelftestFeature,			E_OS_NOFUNC),
	MK_ERRORLOOKUP(MK_eid_InvalidTaskId,					E_OS_ID),
	MK_ERRORLOOKUP(MK_eid_InvalidTickerId,					E_OS_ID),
	MK_ERRORLOOKUP(MK_eid_LockBudgetExceeded,				E_OS_PROTECTION_LOCKED),
	MK_ERRORLOOKUP(MK_eid_MaxActivationsExceeded,			E_OS_LIMIT),
	MK_ERRORLOOKUP(MK_eid_MemoryProtectionException,		E_OS_PROTECTION_MEMORY),
	MK_ERRORLOOKUP(MK_eid_ProcessorSpecificException,		E_OS_PROTECTION_EXCEPTION),
	MK_ERRORLOOKUP(MK_eid_QmOsCallFromWrongContext,			E_OS_CALLEVEL),
	MK_ERRORLOOKUP(MK_eid_Quarantined,						E_OS_STATE),
	MK_ERRORLOOKUP(MK_eid_ResourceAlreadyOccupied,			E_OS_ACCESS),
	MK_ERRORLOOKUP(MK_eid_ResourceNestingLimitExceeded,		E_OS_ACCESS),
	MK_ERRORLOOKUP(MK_eid_ResourceNotOccupied,				E_OS_NOFUNC),
	MK_ERRORLOOKUP(MK_eid_ResourceNotOccupiedByCaller,		E_OS_NOFUNC),
	MK_ERRORLOOKUP(MK_eid_ResourceReleaseSequenceError,		E_OS_NOFUNC),
	MK_ERRORLOOKUP(MK_eid_TaskIsNotExtended,				E_OS_ACCESS),
	MK_ERRORLOOKUP(MK_eid_TfCallFromWrongContext,			E_OS_CALLEVEL),
	MK_ERRORLOOKUP(MK_eid_ThreadIsNotATask,					E_OS_CALLEVEL),
	MK_ERRORLOOKUP(MK_eid_ThreadIsNotIdle,					E_OS_CALLEVEL),
	MK_ERRORLOOKUP(MK_eid_ThreadOccupiesResource,			E_OS_RESOURCE),
	MK_ERRORLOOKUP(MK_eid_WatchdogException,				E_OS_PROTECTION_EXCEPTION),
	MK_ERRORLOOKUP(MK_eid_WithoutPermission,				E_OS_ACCESS),

	MK_ERRORLOOKUP(MK_eid_OsAlarmInUse,						E_OS_STATE),
	MK_ERRORLOOKUP(MK_eid_OsAlarmNotInQueue,				MK_E_INTERNAL),
	MK_ERRORLOOKUP(MK_eid_OsAlarmNotInUse,					E_OS_NOFUNC),
	MK_ERRORLOOKUP(MK_eid_OsCounterIsHw,					E_OS_ID),
	MK_ERRORLOOKUP(MK_eid_OsDifferentCounters,				E_OS_ID),
	MK_ERRORLOOKUP(MK_eid_OsImplicitSyncStartRel,			E_OS_ID),
	MK_ERRORLOOKUP(MK_eid_OsIncrementZero,					E_OS_VALUE),
	MK_ERRORLOOKUP(MK_eid_OsInterruptDisabled,				E_OS_DISABLEDINT),
	MK_ERRORLOOKUP(MK_eid_OsInvalidAlarmId,					E_OS_ID),
	MK_ERRORLOOKUP(MK_eid_OsInvalidApplication,				E_OS_ID),
	MK_ERRORLOOKUP(MK_eid_OsInvalidCounterId,				E_OS_ID),
	MK_ERRORLOOKUP(MK_eid_OsInvalidScheduleId,				E_OS_ID),
	MK_ERRORLOOKUP(MK_eid_OsInvalidStartMode,				E_OS_ID),
	MK_ERRORLOOKUP(MK_eid_OsNotChained,						E_OS_STATE),
	MK_ERRORLOOKUP(MK_eid_OsNotRunning,						E_OS_NOFUNC),
	MK_ERRORLOOKUP(MK_eid_OsNotStopped,						E_OS_STATE),
	MK_ERRORLOOKUP(MK_eid_OsNotSyncable,					E_OS_ID),
	MK_ERRORLOOKUP(MK_eid_OsParameterOutOfRange,			E_OS_VALUE),
	MK_ERRORLOOKUP(MK_eid_OsPermission,						E_OS_ACCESS),
	MK_ERRORLOOKUP(MK_eid_OsQuarantined,					E_OS_STATE),
	MK_ERRORLOOKUP(MK_eid_OsScheduleTableNotIdle,			E_OS_STATE),
	MK_ERRORLOOKUP(MK_eid_OsUnknownSystemCall,				E_OS_NOFUNC),
	MK_ERRORLOOKUP(MK_eid_OsWriteProtect,					E_OS_ILLEGAL_ADDRESS),
	MK_ERRORLOOKUP(MK_eid_OsWrongContext,					E_OS_CALLEVEL),

	MK_ERRORLOOKUP(MK_eid_Unknown,							MK_E_ERROR)
};

/* MK_ErrorInternalToOsek() converts an internal errorcode to an AUTOSAR error code using the above table.
 *
 * !LINKSTO Microkernel.Function.MK_ErrorInternalToOsek, 1
 * !doctype src
*/
mk_osekerror_t MK_ErrorInternalToOsek(mk_errorid_t eid)
{
	mk_osekerror_t osekError;

	/* The >= test here is necessary because the enumerated type mk_errorid_t could be signed and
	 * the incoming value could be less than 0.
	*/
	/* Deviation MISRA-2 */
	if ( (eid >= MK_eid_NoError) && (eid < MK_eid_Unknown) )
	{
		osekError = MK_errorInternalToOsekTable[eid];
	}
	else
	{
		osekError = MK_E_ERROR;
	}

	return osekError;
}

/* Editor settings; DO NOT DELETE
 * vi:set ts=4:
*/
