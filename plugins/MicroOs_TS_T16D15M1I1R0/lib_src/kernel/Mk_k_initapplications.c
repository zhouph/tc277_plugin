/* Mk_k_initapplications.c
 *
 * This file contains the function MK_InitApplications()
 *
 * (c) Elektrobit Automotive GmbH
 *
 * $Id: Mk_k_initapplications.c 15821 2014-04-16 04:55:15Z masa8317 $
*/

#include <public/Mk_public_types.h>
#include <private/Mk_thread.h>
#include <private/Mk_task.h>
#include <private/Mk_isr.h>
#include <private/Mk_errorhandling.h>
#include <private/Mk_panic.h>
#include <private/Mk_systemcontrol.h>

/* MK_InitApplications() performs startup-checks related to OS-Applications.
 *
 * The startup-check ensures that if no OS-Applications are configured, there must not be any task or ISR
 * that belongs to an OS-Application.
 * It also checks that the task ids of restart tasks are in range.
 *
 * There is no need to check threads coming from Mk_configuration.c, because these have no OS-Application Id assigned
 * and they are constant.
 *
 * !LINKSTO Microkernel.Function.MK_InitApplications, 1
 * !doctype src
*/
void MK_InitApplications(void)
{
	mk_objquantity_t i;
	mk_objectid_t appId;
	mk_objectid_t taskId;

	/* Range-check on all OS-Application Ids on tasks
	*/
	for (i = 0; i < MK_nTasks; i++)
	{
		appId = MK_taskCfg[i].threadCfg.applicationId;

		if ( (appId < MK_APPL_NONE) || (appId >= MK_nApps) )
		{
			/* !LINKSTO Microkernel.Panic.MK_panic_UnexpectedOsApplication, 1
			 * !doctype src
			*/
			MK_StartupPanic(MK_panic_UnexpectedOsApplication);
		}
	}

	/* Range-check on all OS-Application Ids on ISRs
	*/
	for (i = 0; i < MK_nIsrs; i++)
	{
		appId = MK_isrCfg[i].threadCfg.applicationId;

		if ( (appId < MK_APPL_NONE) || (appId >= MK_nApps) )
		{
			/* !LINKSTO Microkernel.Panic.MK_panic_UnexpectedOsApplication, 1
			 * !doctype src
			*/
			MK_StartupPanic(MK_panic_UnexpectedOsApplication);
		}
	}

	/* Range-check on all task ids of OS-Applications with restart tasks.
	*/
	for (i = 0; i < MK_nApps; i++)
	{
		taskId = MK_appRestartTasks[i];

		if ( (taskId < MK_INVALID_TASK) || (taskId >= MK_nTasks) )
		{
			/* !LINKSTO Microkernel.Panic.MK_panic_InvalidTaskIdInConfiguration, 1
			 * !doctype src
			*/
			MK_StartupPanic(MK_panic_InvalidTaskIdInConfiguration);
		}

		if ( (taskId > MK_INVALID_TASK) && (MK_taskCfg[taskId].threadCfg.applicationId != i) )
		{
			/* !LINKSTO Microkernel.Panic.MK_panic_UnexpectedOsApplication, 1
			 * !doctype src
			*/
			MK_StartupPanic(MK_panic_UnexpectedOsApplication);
		}
	}
}

/* Editor settings; DO NOT DELETE
 * vi:set ts=4:
*/
