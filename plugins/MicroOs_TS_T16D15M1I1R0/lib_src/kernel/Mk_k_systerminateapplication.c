/* Mk_k_systerminateapplication.c
 *
 * This file contains the function MK_SysTerminateApplication()
 *
 * (c) Elektrobit Automotive GmbH
 *
 * $Id: Mk_k_systerminateapplication.c 15821 2014-04-16 04:55:15Z masa8317 $
*/
#include <public/Mk_public_types.h>
#include <private/Mk_thread.h>
#include <private/Mk_errorhandling.h>
#include <private/Mk_syscall.h>

/* MK_SysTerminateApplication() - terminates the given OS-Application
 * This function basically does range and permission checking.
 * If these checks are successful, the work is delegated to
 * MK_TerminateApplication() and MK_RestartApplication().
 *
 * !LINKSTO Microkernel.Function.MK_SysTerminateApplication, 1
 * !doctype src
*/
void MK_SysTerminateApplication(void)
{
	const mk_objectid_t applicationId = (mk_objectid_t) MK_HwGetParameter1(MK_threadCurrent);
	const mk_boolean_t restart = (mk_boolean_t) MK_HwGetParameter2(MK_threadCurrent);
	mk_errorid_t errorCode = MK_eid_Unknown;

	/* Range-check the terminated OS-Application's Id
	*/
	if ( (applicationId >= 0) && (applicationId < MK_nApps) )
	{
		/* Determine who wants to kill the given OS-Application. This is needed for permission checking.
		*/
		const mk_objectid_t callerApplicationId = MK_GetApplicationId();

		/* Check if the caller's application and the terminated application
		 * are identical. If they are not, we refuse to terminate the application.
		 */
		if (applicationId == callerApplicationId)
		{
			/* Check if the OS-Application needs to be restarted after termination
			*/
			if (restart == MK_TRUE)
			{
				MK_RestartApplication(applicationId);
				errorCode = MK_eid_NoError;
			}
			else if (restart == MK_FALSE)
			{
				MK_TerminateApplication(applicationId, MK_FALSE);
				errorCode = MK_eid_NoError;
			}
			else
			{
				errorCode = MK_eid_InvalidRestartParameter;
			}
		}
		else
		{
			errorCode = MK_eid_WithoutPermission;
		}
	}
	else
	{
		errorCode = MK_eid_InvalidOsApplication;
	}

	if ( errorCode == MK_eid_NoError )
	{
		if (MK_HwIsRegisterStoreValid(MK_threadCurrent))
		{
			MK_HwSetReturnValue1(MK_threadCurrent, MK_E_OK);
		}
	}
	else
	{
		(void)MK_ReportError(MK_sid_TerminateApplication, errorCode, MK_threadCurrent);
	}
}

/* Editor settings; DO NOT DELETE
 * vi:set ts=4:
*/
