/* Mk_k_shutdown.c
 *
 * This file contains the MK_Shutdown() function.
 *
 * This function is called by the kernel whenever it needs to shut down.
 *
 * (c) Elektrobit Automotive GmbH
 *
 * $Id: Mk_k_shutdown.c 15821 2014-04-16 04:55:15Z masa8317 $
*/
#include <public/Mk_public_types.h>
#include <private/Mk_thread.h>
#include <private/Mk_resource.h>
#include <private/Mk_jobqueue.h>
#include <private/Mk_systemcontrol.h>
#include <private/Mk_shutdown.h>

/*
 * MK_Shutdown() shuts down the entire system.
 *
 * All threads get terminated (even the idle thread).
 * Then a new idle thread gets started to run the final shutdown loop.
 * Finally the shutdown-hook thread gets started.
 *
 * !LINKSTO Microkernel.Function.MK_Shutdown, 1
 * !doctype src
*/
void MK_Shutdown(mk_osekerror_t shutdownCode)
{
	mk_thread_t *thread;
	mk_jobqueue_t *jq;

	while ( MK_threadQueueHead != MK_NULL )
	{
		thread = MK_threadQueueHead;

		/* Dequeue the thread.
		*/
		MK_threadQueueHead = thread->next;
		thread->next = MK_NULL;

		/* Put thread in the idle state
		*/
		thread->state = MK_THS_IDLE;

		/* Free thread registers
		*/
		MK_HwFreeThreadRegisters(thread->regs);

		/* Release all resources held by the thread
		*/
		if ( thread->lastResourceTaken != MK_NULL )
		{
			MK_ClearResourceList(thread->lastResourceTaken);
			thread->lastResourceTaken = MK_NULL;
		}

		/* Clear the job queue if there is one.
		*/
		jq = thread->jobQueue;
		if ( jq != MK_NULL )
		{
			jq->next = jq->base;
			jq->free = jq->base;
			jq->count = 0u;
		}
	}

	/* Start the shutdown thread. This initializes the thread queue.
	*/
	MK_StartThread(&MK_idleThread, &MK_shutdownThreadConfig);

	/* Start the shutdown-hook thread if there is one.
	*/
	if ( MK_shutdownHookThreadConfig != MK_NULL )
	{
		MK_StartThread(MK_shutdownHookThread, MK_shutdownHookThreadConfig);

		/* Preload the AUTOSAR error code parameter for the ShutdownHook function
		 */
		MK_HwSetParameter1(MK_shutdownHookThread, shutdownCode);
	}
}

/* Editor settings; DO NOT DELETE
 * vi:set ts=4:
*/
