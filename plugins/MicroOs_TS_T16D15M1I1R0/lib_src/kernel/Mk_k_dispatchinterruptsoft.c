/* Mk_k_dispatchinterruptsoft.c
 *
 * This file contains the MK_DispatchInterruptSoft function.
 *
 * (c) Elektrobit Automotive GmbH
 *
 * $Id: Mk_k_dispatchinterruptsoft.c 15821 2014-04-16 04:55:15Z masa8317 $
*/
#include <public/Mk_public_types.h>
#include <private/Mk_softvector.h>
#include <private/Mk_thread.h>
#include <private/Mk_panic.h>

/* MK_DispatchInterruptSoft()
 *
 * This function handles the interrupt entry for the interrupt controller in software mode.
 * It uses the interrupt vector number obtained from the interrupt controller to determine
 * which handler function to call, and the parameter for the handler function.
 * On return from the handler function the thread dispatcher MK_Dispatch() is called to
 * dispatch the most eligible thread.
 *
 * !LINKSTO Microkernel.Function.MK_DispatchInterruptSoft, 1
 * !doctype src
 */
void MK_DispatchInterruptSoft(void)
{
	mk_hwvectorcode_t vectorCode;
	const mk_softvector_t *vector;

	if ( MK_threadCurrent == MK_NULL )
	{
		/* Paranoia!
		 * If there's no thread we just crashed by writing the registers to it.
		 * !LINKSTO Microkernel.Panic.MK_panic_InterruptNoThread, 1
		 * !doctype src
		*/
		MK_Panic(MK_panic_InterruptNoThread);
	}
	else
	if ( MK_threadCurrent == MK_threadQueueHead )
	{
		/* Get the vector code from the interrupt controller and acknowledge the interrupt
		*/
		vectorCode = MK_HwGetVectorCode();

		/* Get the address of the soft vector. Hopefully this will be optimized to avoid
		 * multiplication and division if the hardware supports the right vector spacing.
		*/
		vector = &MK_softwareVectorTable[MK_HwVectorCodeToInt(vectorCode)];

		/* Call the service routine. This might result in a change to the highest-priority thread.
		*/
		(*vector->func)(vector->param, vectorCode);

		/* Dispatch the highest-priority thread; the dispatcher never returns
		*/
		MK_Dispatch();
	}
	else
	{
		/* Paranoia!
		 * If the current thread is not at the head of the queue something has been tinkering
		 * without dispatching.
		 * !LINKSTO Microkernel.Panic.MK_panic_CurrentThreadNotAtHeadOfQueue, 1
		 * !doctype src
		*/
		MK_Panic(MK_panic_CurrentThreadNotAtHeadOfQueue);
	}
}

/* Editor settings; DO NOT DELETE
 * vi:set ts=4:
*/
