/* Mk_k_clearresourcelist.c
 *
 * This file contains the function MK_ClearResourceList()
 *
 * (c) Elektrobit Automotive GmbH
 *
 * $Id: Mk_k_clearresourcelist.c 15821 2014-04-16 04:55:15Z masa8317 $
*/
#include <public/Mk_public_types.h>
#include <private/Mk_resource.h>

/* MK_ClearResourceList() clears a list of resources
 *
 * It is used when a thread terminates to free all the resources occupied by the thread.
 * The resources do not need to be released in the normal way because the thread will not run again
 * in the current invocation.
 *
 * !LINKSTO Microkernel.Function.MK_ClearResourceList, 1
 * !doctype src
*/
void MK_ClearResourceList(mk_resource_t *headOfList)
{
	while ( headOfList != MK_NULL )
	{
		/* Clear the unhooked resource.
		 * There's no need to clear the savedPrio and savedLevel fields, because
		 * they have no meaning if the resource is not occupied
		 * Leaving the owner field alone here means it indicates the last user of the resource.
		 * The previous taken (link pointer) is meaningless for an unoccupied resource, so isn't cleared
		 * This means we don't need a local variable for the resource-to-clear.
		*/
		headOfList->count = 0;

		/* Unhook the first resource from the list
		*/
		headOfList = headOfList->previousTaken;
	}
}

/* Editor settings; DO NOT DELETE
 * vi:set ts=4:
*/
