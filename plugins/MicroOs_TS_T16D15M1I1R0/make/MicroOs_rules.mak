#################################################################
##
## AUTOSAR MicroOs Operating System Micro Kernel
##
## This makefile contains the rules to built the module.
##
## $Id: MicroOs_rules.mak 18223 2015-02-05 16:09:54Z thdr9337 $
## Release: NIGHTLYBUILD_OS_MK_TRUNK_TRICORE_TC277
##
## (c) Elektrobit Automotive GmbH
##
#################################################################

#################################################################
# DEFINITIONS


#################################################################
# REGISTRY
ifeq ($(BUILD_MODE),LIB)
ifeq ($(MODULE),MicroOs)
#################################################################
# lib build mode settings
LIBRARIES_TO_BUILD     += $(MK_KERNEL_LIB_NAME) $(MK_USER_LIB_NAME)
MicroOs_lib_LIB_VARIANT = $($(LIB_VARIANT)_VARIANT)
else
endif
else
LIBRARIES_TO_BUILD     += $(MK_KERNEL_LIB_NAME) $(MK_USER_LIB_NAME)
MicroOs_lib_LIB_VARIANT = $($(LIB_VARIANT)_VARIANT)
endif

ifeq ($(MK_SRC_LIB),y)
# create a project library from all src\* or generated files
LIBRARIES_TO_BUILD     += $(MK_SRC_LIB_NAME)
else
CC_FILES_TO_BUILD += $($(MK_SRC_C_NAME)_FILES)
ASM_FILES_TO_BUILD += $($(MK_SRC_S_NAME)_FILES)

MicroOs_CC_FILES_TO_BUILD += $($(MK_SRC_C_NAME)_FILES)
MicroOs_ASM_FILES_TO_BUILD += $($(MK_SRC_S_NAME)_FILES)
endif
#################################################################
# DEPENDENCIES (only for assembler files)
#

#################################################################
# RULES

.PHONY: checkmd5

checkmd5:
	@cd "$(TRESOS2_BASE)" && \
  checksums\checkmd5.bat
