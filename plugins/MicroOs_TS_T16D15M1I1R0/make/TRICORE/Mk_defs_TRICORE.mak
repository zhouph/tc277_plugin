#################################################################
#
# NAME:      Mk_defs_TRICORE.mak
#
# $Id: Mk_defs_TRICORE.mak 15947 2014-04-28 10:16:48Z masa8317 $
#
# FUNCTION:  This file is part of the Autosar SC build environment.
# PROJECT :  AutosarOS
#
# TOOLCHAIN: GNU Make 3.8
#
# (c) Elektrobit Automotive GmbH
#
#################################################################

MK_PA_PREFIX=$(MK_CPUFAMILY_PREFIX)TRICORE$(MK_HYPHEN)

#
# you can add TRICORE specific flags here. Currently not used
#
# PREPROCESSOR_DEFINES += xxxx
# MK_CPUFAMILY_LIB_FLAGS += yyyy

# Editor settings: DO NOT DELETE
# vi:set ts=4:
