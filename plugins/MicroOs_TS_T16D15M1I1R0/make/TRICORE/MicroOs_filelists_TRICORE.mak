# MicroOs_filelist_TRICORE.mak - lists of files that go into the microkernel build
#
# This file just contains lists of files that go into various parts of the
# build for the TRICORE Architecture.
# The file is also included into the unsupported make so it must be compatible.
#
# $Id: MicroOs_filelists_TRICORE.mak 15851 2014-04-17 10:21:10Z dh $

# MK_LIBSRCCPUFAMILY_KLIB_BASELIST_C is the list of all the C files in the
# plugin/lib_src/CPUFAMILY directory that go into the kernel library.
# These files are listed without prefix or extension.

MK_LIBSRCCPUFAMILY_KLIB_BASELIST_C +=	hwallocatethreadregisters
MK_LIBSRCCPUFAMILY_KLIB_BASELIST_C +=	freethreadregisters
MK_LIBSRCCPUFAMILY_KLIB_BASELIST_C +=	handlevirtualaddresstrap
MK_LIBSRCCPUFAMILY_KLIB_BASELIST_C +=	handleprotectiontrap
MK_LIBSRCCPUFAMILY_KLIB_BASELIST_C +=	handleinstructiontrap
MK_LIBSRCCPUFAMILY_KLIB_BASELIST_C +=	handlecontexttrap
MK_LIBSRCCPUFAMILY_KLIB_BASELIST_C +=	handlebuserrortrap
MK_LIBSRCCPUFAMILY_KLIB_BASELIST_C +=	handleassertiontrap
MK_LIBSRCCPUFAMILY_KLIB_BASELIST_C +=	handlenmi
MK_LIBSRCCPUFAMILY_KLIB_BASELIST_C +=	hwinitinterruptcontroller
MK_LIBSRCCPUFAMILY_KLIB_BASELIST_C +=	syscall
MK_LIBSRCCPUFAMILY_KLIB_BASELIST_C +=	exceptionhandlercommon
MK_LIBSRCCPUFAMILY_KLIB_BASELIST_C +=	fillexceptioninfo
MK_LIBSRCCPUFAMILY_KLIB_BASELIST_C +=	hwinitprocessor
MK_LIBSRCCPUFAMILY_KLIB_BASELIST_C +=	dispatchinterruptsoft
MK_LIBSRCCPUFAMILY_KLIB_BASELIST_C +=	initcsalist
MK_LIBSRCCPUFAMILY_KLIB_BASELIST_C +=	data

# MK_LIBSRCCPUFAMILY_KLIB_BASELIST_S is the list of all the assembler files in the
# plugin/lib_src/CPUFAMILY directory that go into the kernel library.
# These files are listed without prefix or extension.
MK_LIBSRCCPUFAMILY_KLIB_BASELIST_S +=	resumethread
MK_LIBSRCCPUFAMILY_KLIB_BASELIST_S +=	allocatetwocsas
MK_LIBSRCCPUFAMILY_KLIB_BASELIST_S +=	swapfcx
MK_LIBSRCCPUFAMILY_KLIB_BASELIST_S +=	exceptiontable
MK_LIBSRCCPUFAMILY_KLIB_BASELIST_S +=	exceptiontable_startup
MK_LIBSRCCPUFAMILY_KLIB_BASELIST_S +=	interrupttable
MK_LIBSRCCPUFAMILY_KLIB_BASELIST_S +=	hwenablememoryprotection
MK_LIBSRCCPUFAMILY_KLIB_BASELIST_S +=	hwsetupexceptions
MK_LIBSRCCPUFAMILY_KLIB_BASELIST_S +=	setupinterrupttable
MK_LIBSRCCPUFAMILY_KLIB_BASELIST_S +=	entry2
MK_LIBSRCCPUFAMILY_KLIB_BASELIST_S +=   enabletps

MK_LIBSRCCPUFAMILY_KLIB_BASELIST_S +=	usrwaitgetclearevent

# The system-call stubs are written in assembly language ...
# See the global MicroOs_filelists.mak for details
MK_LIBSRCCPUFAMILY_ULIB_BASELIST_S	+=	$(addprefix usr, $(MK_SYSTEMCALL_BASELIST))
MK_LIBSRCCPUFAMILY_ULIB_BASELIST_S	+=	$(addprefix usr, $(MK_OSCALL_BASELIST))

# Userland function for reading the time from STM.
MK_LIBSRCCPUFAMILY_ULIB_BASELIST_C	+=	hwreadtime

# MK_SRCCPUFAMILY_BASELIST_C is the list of all the C files in the
# plugin/src/CPUFAMILY directory, without prefix or extension.
# These files go directly into the system build.
MK_SRCCPUFAMILY_BASELIST_C	+= TRICORE_configuration
MK_SRCCPUFAMILY_BASELIST_C	+= TRICORE_fillmpucache
MK_SRCCPUFAMILY_BASELIST_C  += TRICORE_exectickconv

# MK_SRCCPUFAMILY_BASELIST_S is the list of all the assembler files in the
# plugin/src/CPUFAMILY directory, without prefix or extension.
# These files go directly into the system build.
#MK_SRCCPUFAMILY_BASELIST_S += TRICORE_foo

# MK_SRCQMCPUFAMILY_BASELIST_C is the list of all the assembler files in the
# plugin/src/QM/CPUFAMILY directory, without prefix or extension.
# These files go directly into the system build.
#MK_SRCQMCPUFAMILY_BASELIST_S += entry
