# MicroOs_filelists_TC277.make - lists of files that go into the microkernel build
#
# $Id: MicroOs_filelists_TC277.mak 14126 2014-01-02 14:32:48Z nibo2437 $
#
# This file just contains lists of files that go into various parts of the
# build for the TRICORE Architecture.
# The file is also included into the unsupported make so it must be compatible.

# MK_LIBSRCCPUFAMILY_KLIB_BASELIST_C is the list of all the C files in the
# plugin/lib_src/CPUFAMILY directory that go into the kernel library.
# These files are listed without prefix or extension.
MK_LIBSRCCPUFAMILY_KLIB_BASELIST_C += writeendinit_tc2xx
MK_LIBSRCCPUFAMILY_KLIB_BASELIST_C += hwsetstaticmemorypartition_tc161
MK_LIBSRCCPUFAMILY_KLIB_BASELIST_C += checkcoreid

# MK_LIBSRCCPUFAMILY_KLIB_BASELIST_S is the list of all the assembler files in the
# plugin/lib_src/CPUFAMILY directory that go into the kernel library.
# These files are listed without prefix or extension.
MK_LIBSRCCPUFAMILY_KLIB_BASELIST_S += writeallmpuregisters_tc161

# No additional files needed for this derivate
#MK_LIBSRCCPUFAMILY_KLIB_BASELIST_S +=

# MK_SRCCPUFAMILY_BASELIST_C is the list of all the C files in the
# plugin/src/CPUFAMILY directory, without prefix or extension.
# These files go directly into the system build.
MK_SRCCPUFAMILY_BASELIST_C	+= TRICORE_hwsetdynamicmemorypartition_tc161

# MK_SRCCPUFAMILY_BASELIST_S is the list of all the assembler files in the
# plugin/src/CPUFAMILY directory, without prefix or extension.
# These files go directly into the system build.
MK_SRCCPUFAMILY_BASELIST_S += TRICORE_writedynamicmpuregisters_tc161

