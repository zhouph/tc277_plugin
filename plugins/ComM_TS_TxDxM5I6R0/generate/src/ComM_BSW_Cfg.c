/**
 * \file
 *
 * \brief AUTOSAR ComM
 *
 * This file contains the implementation of the AUTOSAR
 * module ComM.
 *
 * \author Elektrobit Automotive GmbH, 91058 Erlangen, Germany
 *
 * Copyright 2005 - 2013 Elektrobit Automotive GmbH
 * All rights exclusively reserved for Elektrobit Automotive GmbH,
 * unless expressly agreed to otherwise.
 */
[!AUTOSPACING!][!//

/* !LINKSTO ComM503_Refine,1 */

/*==================[inclusions]============================================*/
[!INCLUDE "../include/ComM_Checks.m"!][!//

#include <Std_Types.h>    /* AUTOSAR standard types */

/* prevent RTE symbols defined in ComM.h to be visible for ComM's .c files */
#define COMM_INTERNAL_USAGE

#include <ComM_BSW.h>                   /* public API symbols */
#include <ComM_BSW_Cfg.h>               /* internal BSW config dependent header */

#if (COMM_CANSM_ACCESS_NEEDED == STD_ON)
#include <CanSM_ComM.h>                 /* Can state manager */
#endif

#if (COMM_FRSM_ACCESS_NEEDED == STD_ON)
#include <FrSm.h>                       /* Flexray state manager */
#endif

#if (COMM_LINSM_ACCESS_NEEDED == STD_ON)
#include <LinSM.h>                      /* Lin state manager */
#endif

/*==================[macros]================================================*/

[!SELECT "ComMConfigSet/*[1]"!][!//

[!LOOP "ComMUser/*"!][!//
/** \brief Number of ComM channels needed by user [!"name(.)"!],
 * ID=[!"ComMUserIdentifier"!] */
#define COMM_NUM_CHANNELS_OF_USER_[!"ComMUserIdentifier"!] [!//
[!IF "as:modconf('ComM')[1]/ComMGeneral/ComMPncSupport = 'true'"!]
[!"num:i(count(../../ComMChannel/*/ComMUserPerChannel/*[as:ref(ComMUserChannel)/ComMUserIdentifier = node:current()/ComMUserIdentifier])+count(node:difference(node:refs(as:modconf('ComM')[1]/ComMConfigSet/*[1]/ComMPnc/*/ComMUserPerPnc/*[as:ref(.)/ComMUserIdentifier=node:current()/ComMUserIdentifier]/../../ComMChannelPerPnc/*)/ComMChannelId, as:modconf('ComM')[1]/ComMConfigSet/*[1]/ComMChannel/*/ComMUserPerChannel/*[as:ref(ComMUserChannel)/ComMUserIdentifier=node:current()/ComMUserIdentifier]/../../ComMChannelId)))"!]U
[!ELSE!][!//
[!"num:i(count(../../ComMChannel/*/ComMUserPerChannel/*[as:ref(ComMUserChannel)/ComMUserIdentifier = node:current()/ComMUserIdentifier]))"!]U
[!ENDIF!][!//

[!ENDLOOP!][!//

[!IF "as:modconf('ComM')[1]/ComMGeneral/ComMPncSupport = 'true'"!]

[!LOOP "ComMPnc/*"!]
[!VAR "count" = "num:i(count(ComMPncComSignal/*[ComMPncComSignalKind='EIRA'][ComMPncComSignalDirection='TX']/ComMPncComSignalRef))"!]
/** \brief Number of Tx EIRA Com Signals mapped to ComMPnc_[!"ComMPncId"!] */
#define COMM_NUM_TX_EIRA_PNC_[!"ComMPncId"!]  [!"$count"!]

[!ENDLOOP!]

[!ENDIF!][!//

/*==================[type definitions]======================================*/

/*==================[external function declarations]========================*/

/*==================[internal function declarations]========================*/

/*==================[internal constants]====================================*/

#define COMM_START_SEC_CONST_8
#include <MemMap.h>

[!LOOP "node:order(ComMChannel/*, 'ComMChannelId')"!][!//
[!IF "(count(ComMUserPerChannel/*) > 0) or ((as:modconf('ComM')[1]/ComMGeneral/ComMPncSupport = 'true') and (count(as:modconf('ComM')[1]/ComMConfigSet/*[1]/ComMPnc/*/ComMChannelPerPnc/*[node:ref(.)/ComMChannelId=node:current()/ComMChannelId])>0) and (ComMNetworkManagement/ComMNmVariant != 'PASSIVE'))"!][!//
/** \brief User IDs for ComM channel [!"name(.)"!], ID=[!"ComMChannelId"!] */
STATIC CONST(uint8, COMM_CONST)
ComM_UsersOfChannel_[!"ComMChannelId"!][COMM_NUM_USERS_OF_CHANNEL_[!"ComMChannelId"!]] =
{
[!LOOP "node:refs(ComMUserPerChannel/*/ComMUserChannel)"!][!//
  [!"ComMUserIdentifier"!]U, /* user [!"name(.)"!] */
[!ENDLOOP!][!//
[!IF "as:modconf('ComM')[1]/ComMGeneral/ComMPncSupport = 'true'"!]
[!LOOP "node:difference(node:refs(as:modconf('ComM')[1]/ComMConfigSet/*[1]/ComMPnc/*/ComMChannelPerPnc/*[node:ref(.)/ComMChannelId=node:current()/ComMChannelId]/../../ComMUserPerPnc/*), node:refs(as:modconf('ComM')[1]/ComMConfigSet/*[1]/ComMChannel/*[ComMChannelId=node:current()/ComMChannelId]/ComMUserPerChannel/*/ComMUserChannel))"!][!//
  [!"node:value(ComMUserIdentifier)"!]U, /* user [!"name(.)"!] via PNC */
[!ENDLOOP!][!//
[!ENDIF!][!//
};
[!ELSE!][!//
/* No user linked to ComM channel, [!"name(.)"!], ID=[!"ComMChannelId"!].
 * Channel will only become active during diagnostic session. */
[!ENDIF!][!//

[!ENDLOOP!][!//

[!LOOP "node:order(ComMUser/*, 'ComMUserIdentifier')"!][!//
/** \brief  Channel IDs of channels needed by
 * user [!"name(.)"!], ID=[!"ComMUserIdentifier"!] */
STATIC CONST(uint8, COMM_CONST)
 ComM_ChannelsOfUser_[!"ComMUserIdentifier"!][COMM_NUM_CHANNELS_OF_USER_[!"ComMUserIdentifier"!]] =
{
[!/* make list of all channel IDs of channels which refer to the user having
     the ID of the current user */!][!//
[!LOOP "../../ComMChannel/*[node:refs(ComMUserPerChannel/*/ComMUserChannel)[ComMUserIdentifier = node:current()/ComMUserIdentifier]]"!][!//
  [!"ComMChannelId"!]U, /* channel [!"name(.)"!] */
[!ENDLOOP!][!//
[!IF "as:modconf('ComM')[1]/ComMGeneral/ComMPncSupport = 'true'"!][!//
[!LOOP "node:difference(node:refs(as:modconf('ComM')[1]/ComMConfigSet/*[1]/ComMPnc/*/ComMUserPerPnc/*[as:ref(.)/ComMUserIdentifier=node:current()/ComMUserIdentifier]/../../ComMChannelPerPnc/*)/ComMChannelId, as:modconf('ComM')[1]/ComMConfigSet/*[1]/ComMChannel/*/ComMUserPerChannel/*[as:ref(ComMUserChannel)/ComMUserIdentifier=node:current()/ComMUserIdentifier]/../../ComMChannelId)"!][!//
  [!"."!]U, /* channel [!"name(.)"!] via PNC */
[!ENDLOOP!][!//
[!ENDIF!][!//
};
[!ENDLOOP!][!//

/*---------------[Partial Network Cluster (PNC)]-----------------*/

/*--------[ComM_PncOfUser_X]---------*/
[!LOOP "node:order(ComMUser/*, 'ComMUserIdentifier')"!][!//
[!IF "count(../../ComMPnc/*[node:refs(ComMUserPerPnc/*)[ComMUserIdentifier = node:current()/ComMUserIdentifier]]) > 0"!][!//
/** \brief Index of Pnc needed by user [!"name(.)"!], ID=[!"ComMUserIdentifier"!] */
STATIC CONST(uint8, COMM_CONST)
 ComM_PncOfUser_[!"ComMUserIdentifier"!][COMM_NUM_PNC_OF_USER_[!"ComMUserIdentifier"!]] =
{
[!/* make list of all Pnc which refer to the user having
     the ID of the current user */!][!//
[!VAR "PncIdx"="0"!]
[!VAR "current_ComMUserIdentifier"="node:current()/ComMUserIdentifier"!]
[!LOOP "node:order(../../ComMPnc/*, 'ComMPncId')"!][!//
[!IF "count(node:refs(ComMUserPerPnc/*)[ComMUserIdentifier = $current_ComMUserIdentifier]) > 0"!][!//
  [!"num:i($PncIdx)"!]U, /* Index of Pnc [!"name(.)"!], PncID [!"ComMPncId"!] */
[!ENDIF!][!//
[!VAR "PncIdx"="num:i($PncIdx)+num:i(1)"!][!//
[!ENDLOOP!][!//
};

[!ENDIF!][!//
[!ENDLOOP!][!//

/*--------[ComM_UsersOfPnc_X]---------*/
[!VAR "PncIdx"="0"!]
[!LOOP "node:order(ComMPnc/*, 'ComMPncId')"!][!//
[!IF "count(ComMUserPerPnc/*) > 0"!][!//
/** \brief User IDs for Pnc[[!"num:i($PncIdx)"!]] [!"name(.)"!], ID=[!"ComMPncId"!] */
STATIC CONST(uint8, COMM_CONST)
 ComM_UsersOfPnc_[!"ComMPncId"!][COMM_NUM_USERS_OF_PNC_[!"ComMPncId"!]] =
{
  [!LOOP "node:order(node:refs(ComMUserPerPnc/*))"!][!//
    [!"ComMUserIdentifier"!]U, /* user [!"name(.)"!] */
  [!ENDLOOP!][!//
};
[!ENDIF!][!//

[!VAR "PncIdx"="num:i($PncIdx)+num:i(1)"!][!//
[!ENDLOOP!][!//

/*--------[ComM_ChannelsOfPnc_X]---------*/
[!VAR "PncIdx"="0"!]
[!LOOP "node:order(ComMPnc/*, 'ComMPncId')"!][!//
[!IF "count(ComMChannelPerPnc/*) > 0"!][!//
/** \brief Channel IDs for Pnc[[!"num:i($PncIdx)"!]] [!"name(.)"!], ID=[!"ComMPncId"!] */
STATIC CONST(uint8, COMM_CONST)
 ComM_ChannelsOfPnc_[!"ComMPncId"!][COMM_NUM_CHANNELS_OF_PNC_[!"ComMPncId"!]] =
{
  [!LOOP "node:order(node:refs(ComMChannelPerPnc/*))"!][!//
    [!"ComMChannelId"!]U, /* Channel ID of [!"name(.)"!] */
  [!ENDLOOP!][!//
};
[!ENDIF!][!//

[!VAR "PncIdx"="num:i($PncIdx)+num:i(1)"!][!//
[!ENDLOOP!][!//


[!IF "as:modconf('ComM')[1]/ComMGeneral/ComMPncSupport = 'true'"!][!//

[!/* Get the size of the largest ComSignal (= COMM_PN_INFO_LENGTH) to be able to initialize the
    following arrays completely */!]
[!VAR "SigSize1" = "0"!][!//
[!VAR "SigSize2" = "0"!][!//
[!/* Get the length of the largest Com signal for array-type signals */!]
[!IF "count(node:refs(ComMPnc/*/ComMPncComSignal/*/ComMPncComSignalRef)[text:contains(string(ComSignalType), 'UINT8_')]) != 0"!][!//
[!VAR "SigSize1" = "num:max(node:refs(ComMPnc/*/ComMPncComSignal/*/ComMPncComSignalRef)[text:contains(string(ComSignalType), 'UINT8_')]/ComSignalLength)"!][!//
[!ENDIF!][!//
[!/* Get the length of the largest Com signal for non array-type signals */!]
[!IF "count(node:refs(ComMPnc/*/ComMPncComSignal/*/ComMPncComSignalRef)[not(text:contains(string(ComSignalType), 'UINT8_'))]) != 0"!][!//
[!VAR "SigSize2" = "num:max(node:foreach(node:refs(ComMPnc/*/ComMPncComSignal/*/ComMPncComSignalRef)[not(text:contains(string(ComSignalType), 'UINT8_'))]/ComSignalType, 'SigType', 'node:when($SigType = "BOOLEAN", "8", substring-after($SigType, "T"))')) div 8 "!][!//
[!ENDIF!][!//

[!IF "$SigSize1 < $SigSize2"!][!//
[!VAR "SigSize1" = "num:i($SigSize2)"!][!//
[!ENDIF!][!//

[!VAR "PncMaskLength" = "num:i((num:max(as:modconf('ComM')[1]/ComMConfigSet/*[1]/ComMPnc/*/ComMPncId) div 8) + 1)"!][!//


[!/* Generate a bit mask array for each unique RX EIRA signal */!]
[!LOOP "node:order(node:refs(ComMPnc/*/ComMPncComSignal/*[ComMPncComSignalKind='EIRA']
        [ComMPncComSignalDirection='RX']/ComMPncComSignalRef), 'ComHandleId')"!]

[!/* Save the Signal Handle first */!]
[!VAR "HandleId" = "ComHandleId"!]

/** \brief Array of Rx Eira Com signal masks for Com Signal [!"node:name(.)"!] */
STATIC CONST(uint8, COMM_CONST) ComM_PncRxEiraMask_[!"node:name(.)"!][COMM_PN_INFO_LENGTH] =
{
[!/* Do this for all bytes in the EIRA */!][!//
  [!FOR "I" = "1" TO "$PncMaskLength"!][!//
    [!VAR "Mask" = "0"!][!/*
    Get all PNCs within the range of this byte
    */!][!LOOP "node:order(as:modconf('ComM')[1]/ComMConfigSet/*[1]/ComMPnc/*[ComMPncId >= num:i(($I - 1) * 8) and
            ComMPncId < num:i($I * 8)], 'ComMPncId')"!][!/*
          Check if this PNC contain the selected signal reference
          */!][!IF "node:containsValue(node:refs(ComMPncComSignal/*[ComMPncComSignalKind='EIRA']
          [ComMPncComSignalDirection='RX']/ComMPncComSignalRef)/ComHandleId, $HandleId)"!][!/*
          If so, set the respective bit
          */!][!VAR "Mask" = "bit:bitset($Mask, ComMPncId)"!][!//
      [!ENDIF!][!//
    [!ENDLOOP!][!//
  [!"num:inttohex(bit:shr($Mask, (($I - 1) * 8)))"!]U,
  [!ENDFOR!][!//
  [!/* Initialize the array completely to avoid MISRA warnings */!][!//
  [!FOR "I" = "$PncMaskLength + 1" TO "$SigSize1"!][!//
  0x00U,
  [!ENDFOR!][!//
};
[!ENDLOOP!]

[!/* Generate a bit mask array for each unique TX EIRA signal */!]
[!LOOP "node:order(node:refs(ComMPnc/*/ComMPncComSignal/*[ComMPncComSignalKind='EIRA']
        [ComMPncComSignalDirection='TX']/ComMPncComSignalRef), 'ComHandleId')"!]
[!/* Save the Signal Handle first */!]
[!VAR "HandleId" = "ComHandleId"!]
/** \brief Array of Tx Eira Com signal masks for Com Signal [!"node:name(.)"!] */
STATIC CONST(uint8, COMM_CONST) ComM_PncTxEiraMask_[!"node:name(.)"!][COMM_PN_INFO_LENGTH] =
{
[!/* Do this for all bytes in the EIRA */!][!//
  [!FOR "I" = "1" TO "num:i((num:max(as:modconf('ComM')[1]/ComMConfigSet/*[1]/ComMPnc/*/ComMPncId) div 8) + 1)"!][!//
    [!VAR "Mask" = "0"!][!/*
    Get all PNCs within the range of this byte
    */!][!LOOP "node:order(as:modconf('ComM')[1]/ComMConfigSet/*[1]/ComMPnc/*[ComMPncId >= num:i(($I - 1) * 8) and
            ComMPncId < num:i($I * 8)], 'ComMPncId')"!][!/*
          Check if this PNC contain the selected signal reference
          */!][!IF "node:containsValue(node:refs(ComMPncComSignal/*[ComMPncComSignalKind='EIRA']
          [ComMPncComSignalDirection='TX']/ComMPncComSignalRef)/ComHandleId, $HandleId)"!][!/*
          If so, set the respective bit
          */!][!VAR "Mask" = "bit:bitset($Mask, ComMPncId)"!][!//
      [!ENDIF!][!//
    [!ENDLOOP!][!//
  [!"num:inttohex(bit:shr($Mask, (($I - 1) * 8)))"!]U,
  [!ENDFOR!][!//
  [!/* Initialize the array completely to avoid MISRA warnings */!][!//
  [!FOR "I" = "$PncMaskLength + 1" TO "$SigSize1"!][!//
  0x00U,
  [!ENDFOR!][!//
};
[!ENDLOOP!]

[!ENDIF!][!//


[!LOOP "node:order(ComMPnc/*, 'ComMPncId')"!]

/** \brief List of Tx Eira Com signals mapped to ComMPnc_[!"ComMPncId"!] */
STATIC CONST(uint8, COMM_CONST) ComM_TxEiraSig_Pnc_[!"ComMPncId"!][COMM_NUM_TX_EIRA_PNC_[!"ComMPncId"!]] =
{
[!LOOP "node:order(node:refs(ComMPncComSignal/*[ComMPncComSignalKind='EIRA'][ComMPncComSignalDirection='TX']/ComMPncComSignalRef), 'ComHandleId')"!]
[!VAR "HandleId" = "ComHandleId"!]
[!VAR "SigIndex" = "0"!]
[!LOOP "node:order(node:refs(as:modconf('ComM')[1]/ComMConfigSet/*[1]/ComMPnc/*/ComMPncComSignal/*[ComMPncComSignalKind='EIRA'][ComMPncComSignalDirection='TX']/ComMPncComSignalRef), 'ComHandleId')"!]
[!IF "ComHandleId = $HandleId"!]
  [!"num:i($SigIndex)"!], /* [!"node:name(.)"!] */
[!ENDIF!]
[!VAR "SigIndex" = "$SigIndex + 1"!]
[!ENDLOOP!]
[!ENDLOOP!]
};

[!ENDLOOP!][!//


[!LOOP "node:order(ComMChannel/*, 'ComMChannelId')"!][!//
[!IF "num:i(count(as:modconf('ComM')[1]/ComMConfigSet/*[1]/ComMPnc/*/ComMChannelPerPnc/*[as:ref(.)/ComMChannelId = node:current()/ComMChannelId])) > 0"!]
/** \brief PNC IDs required for ComM channel "[!"name(.)"!]" */
STATIC CONST(uint8, COMM_CONST)
ComM_PNCsOfChannel_[!"ComMChannelId"!][[!"num:i(count(as:modconf('ComM')[1]/ComMConfigSet/*[1]/ComMPnc/*/ComMChannelPerPnc/*[as:ref(.)/ComMChannelId = node:current()/ComMChannelId]))"!]] =
{
[!VAR "CurrentCh" = "node:name(.)"!][!//
[!LOOP "node:order(as:modconf('ComM')[1]/ComMConfigSet/*[1]/ComMPnc/*[node:contains(node:refs(ComMChannelPerPnc/*), node:current())], 'ComMPncId')"!][!//
  [!"ComMPncId"!]U,
[!ENDLOOP!][!//
};

[!ENDIF!][!//
[!ENDLOOP!][!//

#define COMM_STOP_SEC_CONST_8
#include <MemMap.h>


/*==================[external constants]====================================*/

#define COMM_START_SEC_CONST_8
#include <MemMap.h>

[!IF "count(ComMChannel/*) > 1"!][!//

CONST(uint8, COMM_CONST) ComM_NumUsersOfChannel[COMM_NUM_CHANNELS] =
{
[!LOOP "node:order(ComMChannel/*, 'ComMChannelId')"!][!//
[!IF "(ComMNetworkManagement/ComMNmVariant != 'PASSIVE')"!][!//
  COMM_NUM_USERS_OF_CHANNEL_[!"ComMChannelId"!], /* for channel [!"name(.)"!] */
[!ELSE!][!//
  0U, /* Channel with PASSIVE Variant don't have user(s) mapped to it */
[!ENDIF!][!//
[!ENDLOOP!][!//
};

CONST(NetworkHandleType, COMM_CONST)
  ComM_NmChannelOfChannel[COMM_NUM_CHANNELS] =
{
[!LOOP "node:order(ComMChannel/*, 'ComMChannelId')"!][!//
[!IF "(ComMNetworkManagement/ComMNmVariant = 'FULL') or (ComMNetworkManagement/ComMNmVariant = 'PASSIVE')"!][!//
  [!"num:i(as:modconf('Nm')[1]/NmChannelConfig/*[as:ref(NmComMChannelRef)/ComMChannelId = node:current()/ComMChannelId]/NmChannelId)"!]U,
[!ELSE!][!//
  /* ComM channel [!"name(.)"!], ID=[!"ComMChannelId"!]
   * not associated with NM channel, NM never used */
  0xFFU,
[!ENDIF!][!//
[!ENDLOOP!][!//
};

[!ENDIF!][!//

CONST(uint8, COMM_CONST) ComM_NumChannelsOfUser[COMM_NUM_USERS] =
{
[!LOOP "node:order(ComMUser/*, 'ComMUserIdentifier')"!][!//
  COMM_NUM_CHANNELS_OF_USER_[!"ComMUserIdentifier"!], /* for user [!"name(.)"!] */
[!ENDLOOP!][!//
};

[!IF "as:modconf('ComM')[1]/ComMGeneral/ComMWakeupInhibitionEnabled = 'true'"!]
CONST(uint8, COMM_CONST)
  ComM_EB_NoWakeupNvStorage[COMM_NUM_BYTES_NVM] =
{
[!VAR "VarNoWakeupNvStorage" = "0"!][!//
[!LOOP "node:order(ComMChannel/*, 'ComMChannelId')"!][!//
  [!IF "ComMGlobalNvmBlockDescriptor = 'true'"!][!//
   [!VAR "VarNoWakeupNvStorage" = "bit:or($VarNoWakeupNvStorage,bit:shl(1,num:i(ComMChannelId mod 8)))"!][!//
  [!ENDIF!][!//
  [!IF "((ComMChannelId + 1) mod 8) = 0 "!][!//
    [!"num:i($VarNoWakeupNvStorage)"!]U,
    [!VAR "VarNoWakeupNvStorage" = "0"!][!//
  [!ENDIF!][!//
[!ENDLOOP!][!//
[!IF "((node:order(ComMChannel/*, 'ComMChannelId')[last()]/ComMChannelId + 1) mod 8) != 0"!]
  [!"num:i($VarNoWakeupNvStorage)"!]U
[!ENDIF!][!//
};
[!ENDIF!][!//

/*--------[ ComM_NumPncOfUser[ ] ]---------*/
/** \brief Number of Pncs needed by any user */
CONST(uint8, COMM_CONST) ComM_NumPncOfUser[COMM_NUM_USERS] =
{
[!LOOP "node:order(ComMUser/*, 'ComMUserIdentifier')"!][!//
  COMM_NUM_PNC_OF_USER_[!"ComMUserIdentifier"!], /* for user [!"name(.)"!] */
[!ENDLOOP!][!//
};

/*--------[ ComM_NumUsersOfPnc[ ] ]---------*/
[!IF "count(ComMPnc/*) > 0"!][!//

/** \brief Number of Users configured for each Pnc */
CONST(uint8, COMM_CONST) ComM_NumUsersOfPnc[COMM_NUM_PNC] =
{
[!VAR "PncIdx"="0"!]
[!LOOP "node:order(ComMPnc/*, 'ComMPncId')"!][!//
  COMM_NUM_USERS_OF_PNC_[!"ComMPncId"!], /* for Pnc[[!"num:i($PncIdx)"!]] [!"name(.)"!] */
[!VAR "PncIdx"="num:i($PncIdx)+num:i(1)"!][!//
[!ENDLOOP!][!//
};

/*--------[ ComM_NumChannelsOfPnc[ ] ]---------*/
/** \brief Number of channels required for each Pnc */
CONST(uint8, COMM_CONST) ComM_NumChannelsOfPnc[COMM_NUM_PNC] =
{
[!VAR "PncIdx"="0"!]
[!LOOP "node:order(ComMPnc/*, 'ComMPncId')"!][!//
  COMM_NUM_CHANNELS_OF_PNC_[!"ComMPncId"!], /* for Pnc[[!"num:i($PncIdx)"!]] [!"name(.)"!] */
[!VAR "PncIdx"="num:i($PncIdx)+num:i(1)"!][!//
[!ENDLOOP!][!//
};

/** \brief Number of PNCs required for each Channel */
CONST(uint8, COMM_CONST) ComM_NumPncOfCh[COMM_NUM_CHANNELS] =
{
[!LOOP "node:order(ComMChannel/*, 'ComMChannelId')"!][!//
  [!"num:i(count(as:modconf('ComM')[1]/ComMConfigSet/*[1]/ComMPnc/*/ComMChannelPerPnc/*[as:ref(.)/ComMChannelId = node:current()/ComMChannelId]))"!]U, /* for [!"name(.)"!] */
[!ENDLOOP!][!//
};

/** \brief List of Pnc Ids */
CONST(PNCHandleType, COMM_CONST) ComM_PncID[COMM_NUM_PNC] =
{
[!LOOP "node:order(ComMPnc/*, 'ComMPncId')"!][!//
  [!"ComMPncId"!], /* for PNC [!"name(.)"!] */
[!ENDLOOP!][!//
};

CONST(boolean, COMM_CONST)
  ComM_PncNmRequest[COMM_NUM_CHANNELS] =
{
[!LOOP "node:order(ComMChannel/*, 'ComMChannelId')"!][!//
[!IF "ComMNetworkManagement/ComMPncNmRequest = 'true'"!][!//
 TRUE,
[!ELSE!][!//
 FALSE,
[!ENDIF!][!//
[!ENDLOOP!][!//
};

[!ENDIF!][!//

#define COMM_STOP_SEC_CONST_8
#include <MemMap.h>

#define COMM_START_SEC_CONST_16
#include <MemMap.h>

/* Main function period in ms for each channel */
CONST(uint16, COMM_CONST) ComM_MainFunctionPeriodMs[COMM_NUM_CHANNELS] =
{
[!LOOP "node:order(ComMChannel/*, 'ComMChannelId')"!][!//
  [!"num:i(ComMMainFunctionPeriod * 1000)"!]U, /* for channel [!"name(.)"!] */
[!ENDLOOP!][!//
};

#if ((COMM_NM_VARIANT_LIGHT_NEEDED == STD_ON)      \
     || (COMM_NM_VARIANT_NONE_NEEDED == STD_ON))
/* Timeout after which state "NetReqNoNm" is left when ComMNmVariant='LIGHT' or
 * 'NONE' */
CONST(uint16, COMM_CONST) ComM_NetReqNoNmTimeoutMs[COMM_NUM_CHANNELS] =
{
[!LOOP "node:order(ComMChannel/*, 'ComMChannelId')"!][!//
  /* for channel [!"name(.)"!] */
[!IF "(ComMNetworkManagement/ComMNmVariant = 'LIGHT') or (ComMNetworkManagement/ComMNmVariant = 'NONE')"!][!//
  [!"num:i((../../../../ComMGeneral/ComMTMinFullComModeDuration * 1000) div (ComMMainFunctionPeriod * 1000))"!]U, /* Nm variant 'LIGHT' or 'NONE' */
[!ELSE!][!//
  0xFFU, /* channel with real bus NM support, value never used */
[!ENDIF!][!//
[!ENDLOOP!][!//
};
#endif

#if (COMM_NM_VARIANT_LIGHT_NEEDED == STD_ON)
/* Timeout after which state "ready sleep" is left in ComMNmVariant=LIGHT */
CONST(uint16, COMM_CONST) ComM_ReadySleepNoNmTimeoutMs[COMM_NUM_CHANNELS] =
{
[!LOOP "node:order(ComMChannel/*, 'ComMChannelId')"!][!//
  /* for channel [!"name(.)"!] */
[!IF "(ComMNetworkManagement/ComMNmVariant = 'LIGHT')"!][!//
  [!"num:i((ComMNetworkManagement/ComMNmLightTimeout * 1000) div (ComMMainFunctionPeriod * 1000))"!]U, /* Nm variant LIGHT */
[!ELSEIF "(ComMNetworkManagement/ComMNmVariant = 'NONE')"!][!//
  0U,    /* Nm variant NONE, "ready sleep" state is instantly left */
[!ELSE!][!//
  0xFFU, /* channel with real bus NM support, value never used */
[!ENDIF!][!//
[!ENDLOOP!][!//
};
#endif

/* Maximum number of times that ComM can retry requesting the BusSM for
 * ComMode */
CONST(uint16, COMM_CONST) ComM_BusSMMaxRetryCount[COMM_NUM_CHANNELS] =
{
[!LOOP "node:order(ComMChannel/*, 'ComMChannelId')"!][!//
  [!"num:i(ComMBusSMRequestRetryCount)"!]U, /* [!"name(.)"!] */
[!ENDLOOP!][!//
};

[!IF "as:modconf('ComM')[1]/ComMGeneral/ComMPncSupport = 'true'"!]
CONST(Com_SignalIdType, COMM_CONST) ComM_RxComSignalCfg[COMM_NUM_RX_EIRA_SIGNALS] =
{
[!LOOP "node:order(node:refs(ComMPnc/*/ComMPncComSignal/*[ComMPncComSignalKind='EIRA']
        [ComMPncComSignalDirection='RX']/ComMPncComSignalRef), 'ComHandleId')"!]
  [!"ComHandleId"!],  /* [!"node:name(.)"!] */
[!ENDLOOP!][!//
};

CONST(Com_SignalIdType, COMM_CONST) ComM_TxComSignalCfg[COMM_NUM_TX_EIRA_SIGNALS] =
{
[!LOOP "node:order(node:refs(ComMPnc/*/ComMPncComSignal/*[ComMPncComSignalKind='EIRA'][ComMPncComSignalDirection='TX']/ComMPncComSignalRef), 'ComHandleId')"!]
  [!"ComHandleId"!], /* [!"node:name(.)"!] */
[!ENDLOOP!]
};

[!ENDIF!][!//

#define COMM_STOP_SEC_CONST_16
#include <MemMap.h>

#define COMM_START_SEC_CONST_UNSPECIFIED
#include <MemMap.h>

/* !LINKSTO ComM795,1, ComM796,1, ComM797,1, ComM798,1 */
CONSTP2CONST(uint8, COMM_CONST, COMM_CONST)
  ComM_ChannelsOfUser[COMM_NUM_USERS] =
{
[!LOOP "node:order(ComMUser/*, 'ComMUserIdentifier')"!][!//
  ComM_ChannelsOfUser_[!"ComMUserIdentifier"!], /* for user [!"name(.)"!] */
[!ENDLOOP!][!//
};

/* !LINKSTO ComM795,1, ComM796,1, ComM797,1, ComM798,1 */
CONSTP2CONST(uint8, COMM_CONST, COMM_CONST)
  ComM_UsersOfChannel[COMM_NUM_CHANNELS] =
{
[!LOOP "node:order(ComMChannel/*, 'ComMChannelId')"!][!//
[!IF "(ComMNetworkManagement/ComMNmVariant != 'PASSIVE')"!][!//
  ComM_UsersOfChannel_[!"ComMChannelId"!], /* for channel [!"name(.)"!] */
[!ELSE!][!//
  NULL_PTR,  /* Channel with PASSIVE Variant don't have a user mapped to it */
[!ENDIF!][!//
[!ENDLOOP!][!//
};

CONSTP2CONST(uint8, COMM_CONST, COMM_CONST)
  ComM_PNCsOfChannel[COMM_NUM_CHANNELS] =
{
[!LOOP "node:order(ComMChannel/*, 'ComMChannelId')"!][!//
[!IF "num:i(count(as:modconf('ComM')[1]/ComMConfigSet/*[1]/ComMPnc/*/ComMChannelPerPnc/*[as:ref(.)/ComMChannelId = node:current()/ComMChannelId])) > 0"!]
  ComM_PNCsOfChannel_[!"ComMChannelId"!], /* for Channel [!"name(.)"!] */
[!ELSE!]
  NULL_PTR,
[!ENDIF!]
[!ENDLOOP!][!//
};

[!IF "as:modconf('ComM')[1]/ComMGeneral/ComMModeLimitationEnabled = 'true'"!][!//
CONST(ComM_ASR40_InhibitionStatusType, COMM_CONST)
  ComM_ChannelInhibitionStatusInit[COMM_NUM_BYTES_NVM] =
{
[!VAR "VarComMNoCom" = "0"!][!//
[!LOOP "node:order(ComMChannel/*, 'ComMChannelId')"!][!//
  [!IF "ComMNoCom = 'true'"!][!//
   [!VAR "VarComMNoCom" = "bit:or($VarComMNoCom,bit:shl(1,num:i(ComMChannelId mod 8)))"!][!//
  [!ENDIF!][!//
  [!IF "((ComMChannelId + 1) mod 8) = 0"!][!//
    [!"num:i($VarComMNoCom)"!]U,
    [!VAR "VarComMNoCom" = "0"!][!//
  [!ENDIF!][!//
[!ENDLOOP!][!//
[!IF "((node:order(ComMChannel/*, 'ComMChannelId')[last()]/ComMChannelId + 1) mod 8) != 0"!]
  [!"num:i($VarComMNoCom)"!]U
[!ENDIF!][!//
};
[!ENDIF!][!//

[!IF "(as:modconf('ComM')[1]/ComMGeneral/ComMModeLimitationEnabled = 'true') or (as:modconf('ComM')[1]/ComMGeneral/ComMWakeupInhibitionEnabled = 'true')"!][!//
CONST(ComM_NvDataType, COMM_CONST) ComM_EB_NvROM =
{
  0U, /* InhibitionCounter */
  [!"num:i(as:modconf('ComM')[1]/ComMGeneral/ComMEcuGroupClassification)"!]U, /* ECUGroupClassification */
[!IF "(as:modconf('ComM')[1]/ComMGeneral/ComMWakeupInhibitionEnabled = 'true')"!][!//
  { /* ChannelWakeUpInhibition array */
[!VAR "VarComMNoWakeup" = "0"!][!//
[!LOOP "node:order(ComMChannel/*, 'ComMChannelId')"!][!//
  [!IF "ComMNoWakeup = 'true'"!][!//
    [!VAR "VarComMNoWakeup" = "bit:or($VarComMNoWakeup,bit:shl(1,num:i(ComMChannelId mod 8)))"!][!//
  [!ENDIF!][!//
  [!IF "((ComMChannelId + 1) mod 8) = 0"!][!//
    [!"num:i($VarComMNoWakeup)"!]U,
    [!VAR "VarComMNoWakeup" = "0"!][!//
  [!ENDIF!][!//
[!ENDLOOP!][!//
[!IF "((node:order(ComMChannel/*, 'ComMChannelId')[last()]/ComMChannelId + 1) mod 8) != 0"!]
  [!"num:i($VarComMNoWakeup)"!]U
[!ENDIF!][!//
  }
[!ENDIF!][!//
};
[!ENDIF!][!//


/*--------[ComM_PncOfUser]---------*/
/** \brief Index of Pnc needed by any user */
CONSTP2CONST(uint8, COMM_CONST, COMM_CONST)
  ComM_PncOfUser[COMM_NUM_USERS] =
{
[!LOOP "node:order(ComMUser/*, 'ComMUserIdentifier')"!][!//
[!IF "count(../../ComMPnc/*[node:refs(ComMUserPerPnc/*)[ComMUserIdentifier = node:current()/ComMUserIdentifier]]) > 0"!][!//
  ComM_PncOfUser_[!"ComMUserIdentifier"!], /* for user [!"name(.)"!] */
[!ELSE!][!//
  NULL_PTR, /* No Pnc needed by User[[!"ComMUserIdentifier"!]]. */
[!ENDIF!][!//
[!ENDLOOP!][!//
};

#if (COMM_NUM_PNC > 0)
/*--------[ComM_UsersOfPnc]---------*/
CONSTP2CONST(uint8, COMM_CONST, COMM_CONST)
  ComM_UsersOfPnc[COMM_NUM_PNC] =
{
[!LOOP "node:order(ComMPnc/*, 'ComMPncId')"!][!//
[!IF "count(ComMUserPerPnc/*) > 0"!][!//
  ComM_UsersOfPnc_[!"ComMPncId"!], /* for Pnc [!"name(.)"!] with PncId [!"ComMPncId"!] */
[!ELSE!][!//
  NULL_PTR, /* No User for Pnc [!"name(.)"!] */
[!ENDIF!][!//
[!ENDLOOP!][!//
};

/*--------[ComM_ChannelsOfPnc]---------*/
CONSTP2CONST(uint8, COMM_CONST, COMM_CONST)
  ComM_ChannelsOfPnc[COMM_NUM_PNC] =
{
[!LOOP "node:order(ComMPnc/*, 'ComMPncId')"!][!//
[!IF "count(ComMChannelPerPnc/*) > 0"!][!//
  ComM_ChannelsOfPnc_[!"ComMPncId"!], /* for Pnc [!"name(.)"!] with PncId [!"ComMPncId"!] */
[!ELSE!][!//
  NULL_PTR, /* No Channel for Pnc [!"name(.)"!] */
[!ENDIF!][!//
[!ENDLOOP!][!//
};

#endif

[!IF "as:modconf('ComM')[1]/ComMGeneral/ComMPncSupport = 'true'"!]

CONST(ComM_PncTxEiraSigMapType, COMM_CONST) ComM_PncTxEiraSignalMap[COMM_NUM_PNC] =
{
[!LOOP "node:order(ComMPnc/*, 'ComMPncId')"!]
  /* [!"node:name(.)"!] */
  {
    ComM_TxEiraSig_Pnc_[!"ComMPncId"!],      /* TxSignalList */
    COMM_NUM_TX_EIRA_PNC_[!"ComMPncId"!]     /* NumTxSignal */
  },
[!ENDLOOP!]
};

CONSTP2CONST(uint8, COMM_CONST, COMM_CONST) ComM_PncRxEiraMask[COMM_NUM_RX_EIRA_SIGNALS] =
{
[!LOOP "node:order(node:refs(ComMPnc/*/ComMPncComSignal/*[ComMPncComSignalKind='EIRA']
        [ComMPncComSignalDirection='RX']/ComMPncComSignalRef), 'ComHandleId')"!]
  /* Com Signal: [!"node:name(.)"!] */
  ComM_PncRxEiraMask_[!"node:name(.)"!],
[!ENDLOOP!]
};

CONSTP2CONST(uint8, COMM_CONST, COMM_CONST) ComM_PncTxEiraMask[COMM_NUM_TX_EIRA_SIGNALS] =
{
[!LOOP "node:order(node:refs(ComMPnc/*/ComMPncComSignal/*[ComMPncComSignalKind='EIRA']
        [ComMPncComSignalDirection='TX']/ComMPncComSignalRef), 'ComHandleId')"!]
  /* Com Signal: [!"node:name(.)"!] */
  ComM_PncTxEiraMask_[!"node:name(.)"!],
[!ENDLOOP!]
};

[!ENDIF!]

#define COMM_STOP_SEC_CONST_UNSPECIFIED
#include <MemMap.h>

[!ENDSELECT!][!//

/*==================[internal data]=========================================*/

/*==================[external data]=========================================*/

/*==================[external function definitions]=========================*/

/*==================[internal function definitions]=========================*/

/*==================[end of file]===========================================*/
