[!/**
 * \file
 *
 * \brief AUTOSAR ComM
 *
 * This file contains the implementation of the AUTOSAR
 * module ComM.
 *
 * \author Elektrobit Automotive GmbH, 91058 Erlangen, Germany
 *
 * Copyright 2005 - 2013 Elektrobit Automotive GmbH
 * All rights exclusively reserved for Elektrobit Automotive GmbH,
 * unless expressly agreed to otherwise.
 */!][!//

[!/* These checks were originally present as INVALID checks in ComM.xdm.m4
   * Since these inter-module checks involve parameters from different
   * configuration classes, it's no more possible to keep them in ComM.xdm.m4
   * Refer ASCPROJECT-660 for more details.
   */!]

[!/* *** multiple inclusion protection *** */!]
[!IF "not(var:defined('COMM_CHECKS_M'))"!]
[!VAR "COMM_CHECKS_M" = "'true'"!]

[!NOCODE!][!//

[!/* === Inter-module checks between ComM and Com, FrSM === */!]
[!/* === Following checks access configuration parameters with configuration class "PostBuild" === */!]


[!/* Ensure that the ComMBusSMRequestRetryCount (for each configured channel) is larger
     enough w.r.t the parameter FrSMDurationT3 */!]
[!SELECT "ComMConfigSet/*[1]/ComMChannel/*/ComMBusSMRequestRetryCount"!]
  [!IF "((../ComMBusType = 'COMM_BUS_TYPE_FR') and
      (num:i(as:modconf('FrSM')[1]/FrSMConfig/*/FrSMCluster/*[node:ref(FrSMComMNetworkHandleRef)/ComMChannelId = node:current()/../ComMChannelId]/FrSMDurationT3 * 1000) != 0) and (num:i(../ComMMainFunctionPeriod * . * 1000) <= num:i(as:modconf('FrSM')[1]/FrSMConfig/*/FrSMCluster/*[node:ref(FrSMComMNetworkHandleRef)/ComMChannelId = node:current()/../ComMChannelId]/FrSMDurationT3 * 1000)))"!]
    [!WARNING!]"The value of parameter 'ComMBusSMRequestRetryCount' for channel "[!"node:name(..)"!]" is too small. To avoid the endless transmission of FR startup frames it's recommended to configure (ComMMainFunctionPeriod * ComMBusSMRequestRetryCount) to be greater than timer FrSMDurationT3 configured in FrSM (Recommended value: [!"num:i((as:modconf('FrSM')[1]/FrSMConfig/*/FrSMCluster/*[node:ref(FrSMComMNetworkHandleRef)/ComMChannelId = node:current()/../ComMChannelId]/FrSMDurationT3 div ../ComMMainFunctionPeriod) + 1)"!])."
    [!ENDWARNING!]
  [!ENDIF!]

[!ENDSELECT!]

[!/* Ensure that the length of the referenced ComSignal is sufficient to store at least
     all PN info bits relevant for the PNC it is assigned to */!]
[!SELECT "ComMConfigSet/*[1]/ComMPnc/*/ComMPncComSignal/*/ComMPncComSignalRef"!]
  [!IF "(text:contains(string(node:ref(.)/ComSignalType), 'UINT8_') and num:i(as:ref(.)/ComSignalLength) < num:i((../../../ComMPncId div 8) + 1)) or (not(text:contains(string(as:ref(.)/ComSignalType), 'UINT8_')) and (as:ref(.)/ComBitSize <= ../../../ComMPncId))"!]
     [!ERROR!]"The length of the referenced ComSignal ('ComMPncComSignalRef' for the PNC "[!"node:name(../../..)"!]") is not sufficient to send / receive the EIRA!"
     [!ENDERROR!]
  [!ENDIF!]

[!ENDSELECT!]


[!ENDNOCODE!][!//

[!ENDIF!]

