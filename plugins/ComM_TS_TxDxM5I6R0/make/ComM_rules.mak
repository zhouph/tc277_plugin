# \file
#
# \brief AUTOSAR ComM
#
# This file contains the implementation of the AUTOSAR
# module ComM.
#
# \author Elektrobit Automotive GmbH, 91058 Erlangen, Germany
#
# Copyright 2005 - 2013 Elektrobit Automotive GmbH
# All rights exclusively reserved for Elektrobit Automotive GmbH,
# unless expressly agreed to otherwise.

#################################################################
# DEFINITIONS

#################################################################
# REGISTRY

ComM_src_FILES      := \
   $(ComM_CORE_PATH)\src\ComM.c \
   $(ComM_CORE_PATH)\src\ComM_Hsm.c \
   $(ComM_CORE_PATH)\src\ComM_HsmComMData.c \
   $(ComM_CORE_PATH)\src\ComM_HsmComMFnct.c \
   $(ComM_CORE_PATH)\src\ComM_HsmComMPncData.c \
   $(ComM_CORE_PATH)\src\ComM_HsmComMPncFnct.c \
   $(ComM_CORE_PATH)\src\ComM_Rte.c \
   $(ComM_CORE_PATH)\src\ComM_ASR32_Rte.c \
   $(ComM_CORE_PATH)\src\ComM_ASR40_Rte.c \
   $(ComM_OUTPUT_PATH)\src\ComM_BSW_Cfg.c \
   $(ComM_OUTPUT_PATH)\src\ComM_Rte_Cfg.c \
   $(ComM_OUTPUT_PATH)\src\ComM_MainFunction.c

LIBRARIES_TO_BUILD  += ComM_src

#################################################################
# DEPENDENCIES (only for assembler files)
#

#################################################################
# RULES
