<?xml version="1.0" encoding="UTF-8"?>
<stylesheet version="2.0" xmlns="http://www.w3.org/1999/XSL/Transform" xmlns:my="http://www.elektrobit.com/2013/xgen/local" xmlns:x="http://www.elektrobit.com/2013/xgen" xmlns:xml="http://www.w3.org/XML/1998/namespace" xmlns:fn="http://www.w3.org/2005/xpath-functions" xmlns:xs="http://www.w3.org/2001/XMLSchema">

	<import href="resource://details/generic.xsl" />

	<output method="text" />

	<variable name="pc-config" as="element()*" select="/x:xgen/x:module[@name=$my:module]/x:configuration[@class=$pre-compile][1]" />
	<variable name="config-name-macro" select="concat(fn:upper-case($my:module), '_CONFIG_NAME')" />
	<variable name="pb-config" as="element()*" select="/x:xgen/x:module[@name=$my:module]/x:configuration[@class=$post-build][1]" />
	<variable name="pb-instance" as="element()*" select="$pb-config/x:memory-section/x:instance" />
	<variable name="pb-instance-type" as="element()*" select="$pb-config/x:type[@name = my:resolve-type-references($pb-instance/@type)]" />

	<variable name="misra-h-top-comment">
		<text>/* MISRA-C:2004 Deviations
 *
 * MISRA-1) Deviated Rule: 19.6 (required)
 * "'#undef' shall not be used"
 *
 * Reason:
 * The macros TS_RELOCATABLE_CFG_ENABLE and TS_PB_CFG_PTR_CLASS might also be
 * used by other modules. To avoid that these macros are accidentally used
 * by other modules an undef is used here.
 *
 */</text>
	</variable>

	<variable name="h-undefines">
		<call-template name="undef">
			<with-param name="name">
				<text>TS_RELOCATABLE_CFG_ENABLE</text>
			</with-param>
		</call-template>
		<call-template name="undef">
			<with-param name="name">
				<text>TS_PB_CFG_PTR_CLASS</text>
			</with-param>
		</call-template>
	</variable>

	<variable name="misra-c-top-comment">
		<text>/* MISRA-C:2004 Deviations
 *
 * MISRA-1) Deviated Rule: 19.6 (required)
 * "'#undef' shall not be used"
 *
 *    Reason:
 *    The macros TS_PB_CFG_LAYOUT_TYPE and TS_PB_CFG_NAME must be redefined
 *    if submodules are included within a global layout structure.
 *
 *  MISRA-2) Deviated Rule: 20.6 (required)
 *    The macro `offsetof' shall not be used.
 *
 *    Reason:
 *    Relocatable postbuild macro requires the usage of `offsetof'.
 *
 *  MISRA-3) Deviated Rule: 11.3 (advisory)
 *    A cast should not be performed between a pointer type and an integral
 *    type.
 *
 *    Reason:
 *    Relocatable postbuild macro requires the cast between pointer and
 *    integral type.
 *
 */</text>
	</variable>

	<variable name="top-comment-text">
		<text>
/**
 * \file
 *
 * \brief AUTOSAR Base
 *
 * This file contains the implementation of the AUTOSAR
 * module Base.
 *
 * \author Elektrobit Automotive GmbH, 91058 Erlangen, Germany
 *
 * Copyright 2005 - 2013 Elektrobit Automotive GmbH
 * All rights exclusively reserved for Elektrobit Automotive GmbH,
 * unless expressly agreed to otherwise.
 */
</text>
	</variable>

	<template match="/">
		<call-template name="start-file">
			<with-param name="indentation" select="''" tunnel="yes" />
		</call-template>
		<call-template name="inclusions" />
		<call-template name="macros" />
		<call-template name="type-definitions" />
		<call-template name="external-function-declarations" />
		<call-template name="internal-function-declarations" />
		<call-template name="external-constants" />
		<call-template name="internal-constants" />
		<call-template name="external-data" />
		<call-template name="internal-data" />
		<call-template name="external-function-definitions" />
		<call-template name="internal-function-definitions" />
		<call-template name="end-file" />
	</template>

	<template match="x:type">
		<call-template name="doxygen-comment" />
		<text>typedef </text>
		<apply-templates select="x:reference|x:struct" />
		<text> </text>
		<value-of select="@name" />
		<apply-templates select="./x:reference/@count" />
		<text>;</text>
		<call-template name="attach-comment" />
		<value-of select="$my:NL" />
		<value-of select="$my:NL" />
	</template>

	<template match="x:type/x:struct">
		<text>struct /* </text>
		<value-of select="../@name" />
		<text> */ {</text>
		<value-of select="$my:NL" />
		<apply-templates />
		<text>}</text>
	</template>

	<template match="x:type/x:struct/x:member">
		<value-of select="$my:indent" />
		<apply-templates select="x:compiler-abstraction" />
		<text> </text>
		<value-of select="@name" />
		<apply-templates select="@count" />
		<text>;</text>
		<call-template name="attach-comment" />
		<value-of select="$my:NL" />
	</template>

	<template match="x:var|x:const">
		<value-of select="fn:upper-case(local-name())" />
		<text>( </text>
		<value-of select="../../@type" />
		<text>, </text>
		<value-of select="@memory-class" />
		<text> )</text>
	</template>

	<template match="x:ref2cfg|x:ref2var">
		<text>TS_</text>
		<value-of select="fn:upper-case(local-name())" />
		<text>( </text>
		<value-of select="../../@type" />
		<text> ) </text>
	</template>

	<template match="@count">
		<text>[</text>
		<value-of select="." />
		<text>]</text>
	</template>

	<template name="top-comment">
		<value-of select="fn:replace(my:normalize-end-of-line($top-comment-text), '(\W)Base(\W)', concat('$1', $my:module, '$2'))" />
		<value-of select="$my:NL" />
	</template>

	<template name="start-file">
		<call-template name="top-comment" />
	</template>

	<template name="end-file">
		<call-template name="section">
			<with-param name="text">
				<text>end of file</text>
			</with-param>
		</call-template>
	</template>

	<template name="inclusions">
		<call-template name="section">
			<with-param name="text">
				<text>inclusions</text>
			</with-param>
		</call-template>
	</template>

	<template name="macros">
		<call-template name="section">
			<with-param name="text">
				<text>macros</text>
			</with-param>
		</call-template>
	</template>

	<template name="type-definitions">
		<call-template name="section">
			<with-param name="text">
				<text>type definitions</text>
			</with-param>
		</call-template>
	</template>

	<template name="external-function-declarations">
		<call-template name="section">
			<with-param name="text">
				<text>external function declarations</text>
			</with-param>
		</call-template>
	</template>

	<template name="internal-function-declarations">
		<call-template name="section">
			<with-param name="text">
				<text>internal function declarations</text>
			</with-param>
		</call-template>
	</template>

	<template name="external-constants">
		<call-template name="section">
			<with-param name="text">
				<text>external constants</text>
			</with-param>
		</call-template>
	</template>

	<template name="internal-constants">
		<call-template name="section">
			<with-param name="text">
				<text>internal constants</text>
			</with-param>
		</call-template>
	</template>

	<template name="external-data">
		<call-template name="section">
			<with-param name="text">
				<text>external data</text>
			</with-param>
		</call-template>
	</template>

	<template name="internal-data">
		<call-template name="section">
			<with-param name="text">
				<text>internal data</text>
			</with-param>
		</call-template>
	</template>

	<template name="external-function-definitions">
		<call-template name="section">
			<with-param name="text">
				<text>external function definitions</text>
			</with-param>
		</call-template>
	</template>

	<template name="internal-function-definitions">
		<call-template name="section">
			<with-param name="text">
				<text>internal function definitions</text>
			</with-param>
		</call-template>
	</template>

	<template name="sysinclude">
		<param name="filename" as="xs:string" />

		<text>#include &lt;</text>
		<value-of select="$filename" />
		<text>&gt;</text>
		<value-of select="$my:NL" />
	</template>

	<template name="section">
		<param name="text" as="xs:string" />

		<value-of select="$my:NL" />
		<call-template name="line-comment">
			<with-param name="text">
				<value-of select="$text" />
			</with-param>
			<with-param name="fill">
				<text>=</text>
			</with-param>
		</call-template>
		<value-of select="$my:NL" />
	</template>

	<template name="subsection">
		<param name="text"  as="xs:string" />
		<call-template name="line-comment">
			<with-param name="text">
				<value-of select="$text" />
			</with-param>
			<with-param name="fill">
				<text>-</text>
			</with-param>
		</call-template>
		<value-of select="$my:NL" />
	</template>

	<template name="line-comment">
		<param name="text"  as="xs:string" />
		<param name="fill"  as="xs:string" />

		<text>/*</text>
		<for-each select="1 to 18">
			<value-of select="$fill" />
		</for-each>
		<text>[</text>
		<value-of select="$text" />
		<text>]</text>
		<for-each select="1 to (55 - fn:string-length($text))">
			<value-of select="$fill" />
		</for-each>
		<text>*/</text>
		<value-of select="$my:NL" />
	</template>

	<!-- Attach the contents of the 'comment' attribute of the current element as a C-style comment to the current output line. -->
	<template name="attach-comment">
		<if test="@comment">
			<text> /* </text>
			<value-of select="@comment" />
			<text> */</text>
		</if>
	</template>

	<template name="doxygen-comment">
		<if test="./x:comment">
			<text>/** </text>
			<value-of select="$my:NL" />
			<value-of select="x:comment[1]/text()" />
			<value-of select="$my:NL" />
			<text> */</text>
			<value-of select="$my:NL" />
		</if>
	</template>

	<template name="macro">
		<param name="name" as="xs:string" />
		<param name="value" as="xs:string?" />
		<param name="comment" as="xs:string?" required="no" select="nil" />

		<call-template name="macro-guard">
			<with-param name="name" select="$name" />
		</call-template>
		<call-template name="unguarded-macro">
			<with-param name="name" select="$name" />
			<with-param name="value" select="$value" />
			<with-param name="comment" select="if(exists($comment)) then $comment else nil" />
		</call-template>

		<value-of select="$my:NL" />
		<value-of select="$my:NL" />
	</template>

	<template name="undef">
		<param name="name" as="xs:string" />
		<text>/* Deviation MISRA-1 */</text>
		<value-of select="$my:NL" />
		<text>#undef </text>
		<value-of select="$name" />
		<value-of select="$my:NL" />
	</template>

	<template name="unguarded-macro">
		<param name="name" as="xs:string" />
		<param name="value" as="xs:string?" />
		<param name="comment" as="xs:string?" required="no" select="nil" />

		<if test="exists($comment)">
			<text>/** </text>
			<value-of select="$comment" />
			<text> */</text>
			<value-of select="$my:NL" />
		</if>
		<text>#define </text>
		<value-of select="$name" />
		<if test="exists($value)">
			<text> </text>
			<value-of select="$value" />
		</if>
		<value-of select="$my:NL" />
	</template>

	<template name="macro-guard">
		<param name="name" as="xs:string" />

		<text>#if (defined </text>
		<value-of select="$name" />
		<text>) /* To prevent double definition */</text>
		<value-of select="$my:NL" />

		<text>#error </text>
		<value-of select="$name" />
		<text> already defined</text>
		<value-of select="$my:NL" />

		<text>#endif /* (defined </text>
		<value-of select="$name" />
		<text>) */</text>
		<value-of select="$my:NL" />
		<value-of select="$my:NL" />
	</template>

	<template name="begin-memory-section">
		<param name="name" as="xs:string" />
		<call-template name="memsec">
			<with-param name="name" select="$name" />
			<with-param name="infix">
				<text>START</text>
			</with-param>
		</call-template>
		<value-of select="$my:NL" />
	</template>

	<template name="end-memory-section">
		<param name="name" as="xs:string" />
		<value-of select="$my:NL" />
		<call-template name="memsec">
			<with-param name="name" select="$name" />
			<with-param name="infix">
				<text>STOP</text>
			</with-param>
		</call-template>
	</template>

	<template name="memsec">
		<param name="name" as="xs:string" />
		<param name="infix" as="xs:string" />

		<text>#define </text>
		<value-of select="fn:upper-case($my:module)" />
		<text>_</text>
		<value-of select="$infix" />
		<text>_</text>
		<value-of select="$name" />
		<value-of select="$my:NL" />
		<call-template name="sysinclude">
			<with-param name="filename">
				<text>MemMap.h</text>
			</with-param>
		</call-template>
	</template>

	<template name="indent">
		<param name="indentation" as="xs:string" tunnel="yes" />
		<value-of select="$indentation" />
	</template>

	<function name="my:make-unsigned-value">
		<param name="value" as="xs:integer" />
		<sequence select="concat($value, 'U')" />
	</function>

	<function name="my:make-config-typename-macro">
		<param name="module" as="xs:string" />

		<sequence select="concat(fn:upper-case($module), '_CONST_CONFIG_LAYOUT_TYPE')" />
	</function>

	<function name="my:make-pb-config-name-macro">
		<param name="module" as="xs:string" />

		<sequence select="concat(fn:upper-case($module), '_PB_CONFIG_NAME')" />
	</function>

</stylesheet>
