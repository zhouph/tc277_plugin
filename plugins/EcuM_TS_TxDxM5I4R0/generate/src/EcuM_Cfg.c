/**
 * \file
 *
 * \brief AUTOSAR EcuM
 *
 * This file contains the implementation of the AUTOSAR
 * module EcuM.
 *
 * \author Elektrobit Automotive GmbH, 91058 Erlangen, Germany
 *
 * Copyright 2005 - 2013 Elektrobit Automotive GmbH
 * All rights exclusively reserved for Elektrobit Automotive GmbH,
 * unless expressly agreed to otherwise.
 */
[!AUTOSPACING!][!//
/* !LINKSTO ECUM121_Conf,1 */
[!VAR "EcuMPeriodMainFunction"="EcuMGeneral/EcuMMainFunctionPeriod"!][!//

/*==================[inclusions]============================================*/

#include <Std_Types.h>    /* AUTOSAR standard types            */

#include <Mcu.h>          /* MCU module types                  */
#include <EcuM_Int.h>     /* EcuM private header               */
#include <EcuM_Cbk.h>

[!VAR "ModuleList"="' Mcu '"!]
[!LOOP "EcuMConfiguration/*[1]/EcuMCommonConfiguration/*/EcuMDriverInitItem/*"!][!//
[!/*
    Check VendorApiInfix is enabled for this module, If enabled, then add
    header file <EcuMModuleID>_<VendorId>_<VendorApiInfix>.h.
    Check if module header file was specified by the user, else
    include ModuleId.h
    Only generate unique include directives, check if module ID/module header Id
    was not encontered before
*/!][!//
[!IF "node:exists(./EcuMEnableVendorApiInfix) and (./EcuMEnableVendorApiInfix) = 'true' and node:empty(./EcuMModuleHeaderFile)"!][!//
[!VAR "ModuleRef" = "as:path(as:ref(../../../../EcuMFlexConfiguration/EcuMFlexModuleConfigurationRef/*[starts-with(node:value(.),concat('ASPath:/',node:value(node:current()/EcuMModuleID),'/'))]))"!][!//
[!VAR "VendorID"= "node:value(as:ref($ModuleRef)/../../CommonPublishedInformation/VendorId)"!][!//
[!VAR "VendorApiInfx"= "node:value(as:ref($ModuleRef)/../../CommonPublishedInformation/VendorApiInfix)"!][!//
[!VAR "ModuleId"="name(as:ref($ModuleRef)/../../.)"!][!//
#include <[!"$ModuleId"!]_[!"$VendorID"!]_[!"$VendorApiInfx"!].h>
[!ELSE!][!//
[!IF "node:empty(./EcuMModuleHeaderFile)"!][!//
  [!IF "not(contains($ModuleList,concat(' ',./EcuMModuleID,' ')))"!][!//
#include <[!"./EcuMModuleID"!].h>
    [!VAR "ModuleList"="concat($ModuleList,' ',./EcuMModuleID,' ')"!]
  [!ENDIF!][!//
[!ELSE!][!//
  [!IF "not(contains($ModuleList,concat(' ',./EcuMModuleHeaderFile,' ')))"!][!//
#include <[!"./EcuMModuleHeaderFile"!].h>
    [!VAR "ModuleList"="concat($ModuleList,' ',./EcuMModuleHeaderFile,' ')"!][!//
  [!ENDIF!][!//
[!ENDIF!][!//
[!ENDIF!][!//
[!ENDLOOP!][!//

[!NOCODE!][!//
[!//
[!SELECT "EcuMConfiguration/*[1]/EcuMCommonConfiguration"!][!//
[!/* Check for correct order of invocations */!][!//
[!VAR "ModuleServiceList"="''"!][!// Let ModuleServiceList be initially empty
[!LOOP "EcuMDriverInitListZero | EcuMDriverInitListOne"!][!// Loop through init lists
  [!LOOP "EcuMDriverInitItem/*"!][!// Check each init item
    [!VAR "ModuleServiceList"="concat($ModuleServiceList,' ',EcuMModuleID,'_', EcuMModuleService)"!]
  [!ENDLOOP!]
[!ENDLOOP!]

/* !LINKSTO EcuM2720,1 */
[!VAR "RestartModuleService"="''"!][!// Let RestartModuleService be initially empty
[!VAR "PreviousLocation"="-1"!][!// Let PreviousLocation be initially empty
[!VAR "CurrentLocation"="-1"!][!// Let CurrentLocation be initially empty
[!LOOP "EcuMDriverRestartList"!][!// Loop through Restart List
  [!LOOP "EcuMDriverInitItem/*"!][!// Check each init item
    [!VAR "RestartModuleService"="concat($RestartModuleService,' ',EcuMModuleID,'_', EcuMModuleService)"!]
    [!VAR "CurrentLocation"= "text:indexOf( string($ModuleServiceList), string($RestartModuleService) )"!]
    [!IF "$CurrentLocation > -1"!][!//
      [!IF "$CurrentLocation <= $PreviousLocation"!][!// if the order is not correct display the error
        [!ERROR "Entries in the restart list should be in the same order as in init list zero and one"!]
      [!ENDIF!]
      [!VAR "PreviousLocation" = "$CurrentLocation"!][!// save the current location
    [!ELSE!]
      [!WARNING "There is one entry in the restart list which does not have a counterpart in init list zero or one"!]
    [!ENDIF!]
    [!VAR "RestartModuleService"="''"!][!// empty the string for future iterations
  [!ENDLOOP!]
[!ENDLOOP!]
[!ENDSELECT!][!//
[!ENDNOCODE!][!//

[!/*
****************************************************************************
* MACRO for generating the body of a function initializing the driver
* module of a driver initialization list
****************************************************************************
*/!][!//
[!MACRO "InitializeDriver","DriverListRef","PbConfigMember"!][!//
[!LOOP "as:ref($DriverListRef)/EcuMDriverInitItem/*"!][!//
  /* *** Call service [!"EcuMModuleService"!] of module [!"EcuMModuleID"!] *** */
[!VAR "RefSubString"="concat('/',EcuMModuleID,'/',EcuMModuleID,'/')"!][!//
[!IF "node:exists(./EcuMEnableVendorApiInfix) and ./EcuMEnableVendorApiInfix = 'true'"!][!//
[!VAR "RefSubString"="concat('/',EcuMModuleID,'/')"!][!//
[!VAR "ModuleRef" = "as:path(as:ref(../../../../EcuMFlexConfiguration/EcuMFlexModuleConfigurationRef/*[starts-with(node:value(.),concat('ASPath:/',node:value(node:current()/EcuMModuleID),'/'))]))"!][!//
[!VAR "VendorID"= "node:value(as:ref($ModuleRef)/../../CommonPublishedInformation/VendorId)"!][!//
[!VAR "VendorApiInfx"= "node:value(as:ref($ModuleRef)/../../CommonPublishedInformation/VendorApiInfix)"!][!//
[!VAR "ModuleId"="name(as:ref($ModuleRef)/../../.)"!][!//
  [!"$ModuleId"!]_[!"$VendorID"!]_[!"$VendorApiInfx"!]_[!"EcuMModuleService"!]([!//
[!ELSE!][!//
  [!"EcuMModuleID"!]_[!"EcuMModuleService"!]([!//
[!ENDIF!][!//
[!IF "node:exists(EcuMModuleInitConfigStr) and (string-length(EcuMModuleInitConfigStr) > 0)"!][!//
[!/* Generate function argument even for non-Init services. For user provided
   * modules the name of the init function may not be equal to 'Init', Studio
   * displays a warning in this case. */!][!//
[!"EcuMModuleInitConfigStr"!][!//
[!ELSEIF "((EcuMModuleService = 'Init') or (EcuMModuleService = 'PreInit')) and (count(../../../../EcuMFlexConfiguration/EcuMFlexModuleConfigurationRef/*[contains(.,$RefSubString)]) > 0)"!][!//
&[!"name(as:ref(../../../../EcuMFlexConfiguration/EcuMFlexModuleConfigurationRef/*[contains(.,$RefSubString)]))"!][!//
[!ELSE!][!//
[!/* generate no function argument */!][!//
[!ENDIF!][!//
);
[!ENDLOOP!][!//
[!ENDMACRO!][!//

[!SELECT "EcuMConfiguration/*[1]/EcuMCommonConfiguration"!][!//
/*==================[macros]================================================*/

/*==================[type definitions]======================================*/

/*==================[external function declarations]========================*/

/*==================[internal function declarations]========================*/

/*==================[external constants]====================================*/
#define ECUM_START_SEC_CONST_8
#include <MemMap.h>

/* Wake-up Source ID mapping */
CONST(uint8, ECUM_CONST) EcuM_WksMapping[ECUM_WKS_ID_MAXVAL + 1U] =
{
[!VAR "Index" = "0"!]
[!FOR "i" = "0" TO "num:max(EcuMWakeupSource/*/EcuMWakeupSourceId)"!]
  [!IF "node:exists(EcuMWakeupSource/*[EcuMWakeupSourceId = num:i($i)])"!]
  [!"num:i($Index)"!]U,
  [!VAR "Index" = "$Index + 1"!]
  [!ELSE!]
  ECUM_WKS_INVALID_ID,
  [!ENDIF!]
[!ENDFOR!]
};

#define ECUM_STOP_SEC_CONST_8
#include <MemMap.h>


#define ECUM_START_SEC_CONST_16
#include <MemMap.h>

/* !LINKSTO EcuM4004,1 */
/* timeout for wakeup source */
CONST(uint16, ECUM_CONST) EcuM_WksValTimeout[ECUM_WKSCONFIGNUM] =
{
[!LOOP "node:order(EcuMWakeupSource/*, 'EcuMWakeupSourceId')"!]
  [!"num:i(EcuMValidationTimeout div $EcuMPeriodMainFunction)"!]U, /* Timeout for wakeup source [!"name(.)"!] */
[!ENDLOOP!]
};

#define ECUM_STOP_SEC_CONST_16
#include <MemMap.h>

#define ECUM_START_SEC_CONST_UNSPECIFIED
#include <MemMap.h>


/*------------------[Wakeup Sources]----------------------------------------*/

/* configurations of Wakeup Sources */
CONST(EcuM_WksConfigType, ECUM_CONST) EcuM_WksConfigList[ECUM_WKSCONFIGNUM] =
{
/* !LINKSTO EcuM2166,1 */
[!LOOP "node:order(EcuMWakeupSource/*, 'EcuMWakeupSourceId')"!]
  /* [!"name(.)"!] */
  {
  [!IF "node:refexists(EcuMComMChannelRef)"!]
    [!"num:i(as:ref(EcuMComMChannelRef)/ComMChannelId)"!]U,   /* ComM Channel Id */
  [!ELSE!]
    ECUM_NO_COMM_CH,   /* No associated ComM Channel */
  [!ENDIF!]
  [!IF "node:refexists(EcuMResetReasonRef)"!]
    [!"name(as:ref(EcuMResetReasonRef)/McuResetReason/..)"!],   /* Reset Reason */
  [!ELSE!]
    MCU_RESET_UNDEFINED,   /* No valid Reset Reason */
  [!ENDIF!]
    [!"num:i(EcuMWakeupSourceId)"!]U   /* Wakeup Source Id */
  },
[!ENDLOOP!]
};

/*------------------[Sleep Modes]-------------------------------------------*/
CONST(EcuM_SleepModeConfigType, ECUM_CONST) EcuM_SleepModeConfigList[ECUM_SLEEPMODECONFIGNUM] =
{

[!LOOP "node:order(EcuMSleepMode/*,'node:value(EcuMSleepModeId)')"!][!//
  {
[!INDENT "4"!][!//
    [!//An xdm check ensures that the reference is valid for EcuMSleepModeMcuModeRef.
    /* Sleep mode: [!"name(.)"!], ID: [!"EcuMSleepModeId"!] */
#if (defined [!//
    McuConf_[!"node:name(node:dtos(as:ref(./EcuMSleepModeMcuModeRef)))"!]_[!"name(as:ref(EcuMSleepModeMcuModeRef))"!])
    McuConf_[!"node:name(node:dtos(as:ref(./EcuMSleepModeMcuModeRef)))"!]_[!"name(as:ref(EcuMSleepModeMcuModeRef))"!],
#else
    [!"name(as:ref(EcuMSleepModeMcuModeRef))"!],
#endif
    [!IF "EcuMSleepModeSuspend = 'true'"!]TRUE[!ELSE!]FALSE[!ENDIF!], /* Suspend flag */
    ECUM_[!"name(.)"!]_WKUP_MASK /* Wakeup event mask of sleep mode [!"name(.)"!] */
    [!ENDINDENT!]
  },
[!ENDLOOP!]

};

/*------------------[Errors reporting to Dem]-------------------------------------------*/

[!IF "../../../EcuMGeneral/EcuMIncludeDem = 'true'"!][!//
  [!IF "../../../ReportToDem/EcuMRamChkFailedReportToDem = 'DEM'"!][!//
CONST(Dem_EventIdType, ECUM_CONST) EcuM_DemErrRamChkFailed =
  [!"num:i(as:ref(EcuMDemEventParameterRefs/ECUM_E_RAM_CHECK_FAILED)/DemEventId)"!]U;[!//
  /* Error for Ram check failure */
[!ENDIF!][!//

[!IF "../../../ReportToDem/EcuMCfgDataInconsistentReportToDem = 'DEM'"!][!//
CONST(Dem_EventIdType, ECUM_CONST) EcuM_DemErrConfigDataInconsistent =
  [!"num:i(as:ref(EcuMDemEventParameterRefs/ECUM_E_CONFIGURATION_DATA_INCONSISTENT)/DemEventId)"!]U;[!//
  /* Error for inconsistent configuration */
[!ENDIF!][!//
[!IF "node:exists(EcuMDemEventParameterRefs/ECUM_E_ALL_RUN_REQUESTS_KILLED)"!][!//
CONST(Dem_EventIdType, ECUM_CONST) EcuM_DemErrAllRUNRequestsKilled =
  [!"num:i(as:ref(EcuMDemEventParameterRefs/ECUM_E_ALL_RUN_REQUESTS_KILLED)/DemEventId)"!]U;[!//
[!ENDIF!]
[!ELSE!][!//
  /* Errors cannot be reported to DEM since DEM is not includded */
[!ENDIF!][!//
[!ENDSELECT!][!//

/*------------------[EcuMShutdownCause]-------------------------------------------*/

#if (ECUM_DEV_ERROR_DETECT == STD_ON)
/* Array holding the configured values of Shutdown Causes */
CONST(EcuM_ShutdownCauseType, ECUM_CONST)
   EcuM_ValidShutdownCause[ECUM_SHUTDOWNCAUSECONFIGNUM] =
{
[!SELECT "EcuMConfiguration/*[1]/EcuMFlexConfiguration"!][!//
[!LOOP "EcuMShutdownCause/*"!][!//
  EcuMConf_[!"name(..)"!]_[!"name(.)"!],
[!ENDLOOP!][!//
[!ENDSELECT!]
};
#endif

#define ECUM_STOP_SEC_CONST_UNSPECIFIED
#include <MemMap.h>
/*==================[internal constants]====================================*/

/*==================[external data]=========================================*/

/*==================[internal data]=========================================*/

/*==================[external function definitions]=========================*/
#define ECUM_START_SEC_CODE
#include <MemMap.h>
/*------------------[Callouts from STARTUP state]---------------------------*/
[!SELECT "EcuMConfiguration/*[1]/EcuMCommonConfiguration"!][!//

[!IF "node:exists(EcuMDriverInitListZero)"!][!//
/* Configured Initialization List Zero items */
FUNC(void, ECUM_CODE) EcuM_DefaultInitListZero(void)
{
[!CALL "InitializeDriver","DriverListRef"="as:path(EcuMDriverInitListZero)","PbConfigMember"="'DriverInitZeroConfig'"!]
}
[!ELSE!][!//
/* No content for EcuM_AL_DriverInitZero() configured */
[!ENDIF!][!//

[!IF "node:exists(EcuMDriverInitListOne)"!][!//
/* Configured Initialization List One items */
FUNC(void, ECUM_CODE) EcuM_DefaultInitListOne(void)
{
  [!CALL "InitializeDriver","DriverListRef"="as:path(EcuMDriverInitListOne)","PbConfigMember"="'DriverInitOneConfig'"!][!//
}
[!ENDIF!][!//

/*------------------[Callouts from SLEEP state]--------------------------*/
[!IF "node:exists(EcuMDriverRestartList)"!][!//
/* Configured Driver Restart List items */
FUNC(void, ECUM_CODE) EcuM_DefaultRestartList(void)
{
  [!CALL "InitializeDriver","DriverListRef"="as:path(EcuMDriverRestartList)","PbConfigMember"="'DriverRestartConfig'"!][!//
}
[!ENDIF!][!//
[!ENDSELECT!][!//
#define ECUM_STOP_SEC_CODE
#include <MemMap.h>
/*==================[internal function definitions]=========================*/

/*==================[end of file]===========================================*/
