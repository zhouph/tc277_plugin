/**
 * \file
 *
 * \brief AUTOSAR EcuM
 *
 * This file contains the implementation of the AUTOSAR
 * module EcuM.
 *
 * \author Elektrobit Automotive GmbH, 91058 Erlangen, Germany
 *
 * Copyright 2005 - 2013 Elektrobit Automotive GmbH
 * All rights exclusively reserved for Elektrobit Automotive GmbH,
 * unless expressly agreed to otherwise.
 */

/* !LINKSTO EcuM2988,1 */
[!AUTOSPACING!][!//
[!SELECT "EcuMConfiguration/*[1]"!]
/*==================[inclusions]============================================*/

#include <Os.h>

#include <Std_Types.h>    /* AUTOSAR standard types */

#include <EcuM.h>         /* EcuM public header */
#include <EcuM_Int.h>     /* EcuM private header */

/*==================[macros]================================================*/

/*==================[type definitions]======================================*/

/*==================[external function declarations]========================*/

/*==================[internal function declarations]========================*/

/*==================[internal constants]====================================*/

/*==================[internal data]=========================================*/

/*==================[external constants]====================================*/
#define ECUM_START_SEC_CONST_UNSPECIFIED
#include <MemMap.h>
[!INDENT "0"!][!//
[!VAR "EcuMPeriodMainFunction"="../../EcuMGeneral/EcuMMainFunctionPeriod"!][!//
[!IF "node:exists(./EcuMFixedConfiguration/EcuMRunMinimumDuration) = 'true'"!][!//
[!VAR "EcuMRunMinimumDuration"="./EcuMFixedConfiguration/EcuMRunMinimumDuration"!][!//
[!ENDIF!]
[!VAR "EcuMDefaultSleepMode"="0"!][!//
[!VAR "EcuMDefaultResetMode"="0"!][!//
[!IF "node:refexists(EcuMCommonConfiguration/EcuMDefaultShutdownTarget/EcuMDefaultSleepModeRef)"!][!//
  [!VAR "EcuMDefaultSleepMode"="as:ref(EcuMCommonConfiguration/EcuMDefaultShutdownTarget/EcuMDefaultSleepModeRef)/EcuMSleepModeId"!][!//
[!ELSEIF "node:refexists(EcuMCommonConfiguration/EcuMDefaultShutdownTarget/EcuMDefaultResetModeRef)"!][!//
  [!VAR "EcuMDefaultResetMode"="as:ref(EcuMCommonConfiguration/EcuMDefaultShutdownTarget/EcuMDefaultResetModeRef)/EcuMResetModeId"!][!//
[!ENDIF!][!//

CONST(EcuM_ConfigType, ECUM_CONST) [!"name(.)"!] =
{
  [!//An xdm check ensures that the reference is valid for EcuMDefaultAppMode.
  [!WS "2"!][!"name(as:ref(EcuMCommonConfiguration/EcuMDefaultAppMode))"!],  /* DefaultAppMode */
  [!IF "node:exists(EcuMCommonConfiguration/EcuMDriverInitListOne)"!][!//
    [!WS "2"!]&EcuM_DriverInitListOne,
  [!ELSE!][!//
    NULL_PTR,
  [!ENDIF!][!//
[!//
  [!IF "node:exists(EcuMCommonConfiguration/EcuMDriverRestartList)"!][!//
    [!WS "2"!]&EcuM_DriverRestartList,
  [!ELSE!][!//
    [!WS "2"!]NULL_PTR,
  [!ENDIF!][!//
[!//
  [!IF "EcuMCommonConfiguration/EcuMDefaultShutdownTarget/EcuMDefaultState = 'EcuMStateReset'"!][!//
    [!WS "2"!]ECUM_STATE_RESET[!//
  [!ELSEIF "EcuMCommonConfiguration/EcuMDefaultShutdownTarget/EcuMDefaultState = 'EcuMStateSleep'"!][!//
    [!WS "2"!]ECUM_STATE_SLEEP[!//
  [!ELSE!][!//
    [!WS "2"!]ECUM_STATE_OFF[!//
  [!ENDIF!], /* DefaultShutdownTarget */
[!//
  [!IF "node:refexists(EcuMCommonConfiguration/EcuMDefaultShutdownTarget/EcuMDefaultSleepModeRef)"!][!//
    [!WS "2"!][!"num:i($EcuMDefaultSleepMode)"!]U,      /* DefaultSleepMode */
  [!ELSEIF "node:refexists(EcuMCommonConfiguration/EcuMDefaultShutdownTarget/EcuMDefaultResetModeRef)"!][!//
    [!WS "2"!][!"num:i($EcuMDefaultResetMode)"!]U,      /* DefaultResetMode */
  [!ELSE!][!//
    [!WS "2"!]0U,
  [!ENDIF!]
[!//  
  [!IF "node:exists(./EcuMFixedConfiguration/EcuMRunMinimumDuration) = 'true'"!][!//
    [!"num:i($EcuMRunMinimumDuration div $EcuMPeriodMainFunction)"!]U
  [!ENDIF!][!//
};
[!ENDINDENT!][!//
[!ENDSELECT!]
#define ECUM_STOP_SEC_CONST_UNSPECIFIED
#include <MemMap.h>

/*==================[external data]=========================================*/

/*==================[internal function definitions]=========================*/

/*==================[external function definitions]=========================*/

/*==================[end of file]===========================================*/
