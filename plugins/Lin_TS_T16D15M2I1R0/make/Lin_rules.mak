# \file
#
# \brief AUTOSAR Lin
#
# This file contains the implementation of the AUTOSAR
# module Lin.
#
# \author Elektrobit Automotive GmbH, 91058 Erlangen, Germany
#
# Copyright 2005 - 2013 Elektrobit Automotive GmbH
# All rights exclusively reserved for Elektrobit Automotive GmbH,
# unless expressly agreed to otherwise.

#################################################################
# REGISTRY

LIBRARIES_TO_BUILD     += Lin_src

Lin_src_FILES       += $(Lin_CORE_PATH)\src\Lin_17_AscLin.c
Lin_src_FILES       += $(Lin_CORE_PATH)\src\Lin_17_AscLin_Irq.c
Lin_src_FILES       += $(Lin_OUTPUT_PATH)\src\Lin_17_AscLin_PBCfg.c

ifeq ($(TS_BUILD_POST_BUILD_BINARY),TRUE)
# If the post build binary shall be built do not compile any files other then the postbuild files.
Lin_src_FILES :=
endif
