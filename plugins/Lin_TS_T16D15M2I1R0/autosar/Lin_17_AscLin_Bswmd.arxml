<!--
/*******************************************************************************
**                                                                            **
** Copyright (C) Infineon Technologies (2014)                                 **
**                                                                            **
** All rights reserved.                                                       **
**                                                                            **
** This document contains proprietary information belonging to Infineon       **
** Technologies. Passing on and copying of this document, and communication   **
** of its contents is not permitted without prior written authorization.      **
**                                                                            **
********************************************************************************
**                                                                            **
**  $FILENAME  : Lin_17_Bswmd.arxml  $                                        **
**                                                                            **
**   $CC VERSION : \main\17 $                                                 **
**                                                                            **
**   $DATE       : 2014-08-26 $                                               **
**                                                                            **
**   AUTHOR      : DL-AUTOSAR-Engineering                                     **
**                                                                            **
**   VENDOR      : Infineon Technologies                                      **
**                                                                            **
**   DESCRIPTION : BSW module description for LIN driver                      **
**                                                                            **
**  MAY BE CHANGED BY USER [yes/no]: No                                       **
**                                                                            **
*******************************************************************************/
/*******************************************************************************
/*  TRACEABILITY: [cover parentID=SAS_AS4XX_LIN_PR672,SAS_AS4XX_LIN_PR673,
SAS_AS4XX_LIN_PR674,SAS_AS4XX_LIN_PR675,SAS_AS4XX_LIN_PR676,SAS_AS4XX_LIN_PR677,
SAS_AS4XX_LIN_PR681,SAS_AS4XX_LIN_PR683,SAS_AS4XX_LIN_PR684,SAS_AS4XX_LIN_PR685,
SAS_AS4XX_LIN_PR686,SAS_AS4XX_LIN_PR687,SAS_AS4XX_LIN_PR688,SAS_AS4XX_LIN_PR689,
SAS_AS4XX_LIN_PR690,SAS_AS4XX_LIN_PR693,SAS_AS4XX_LIN_PR694,SAS_AS4XX_LIN_PR695,
SAS_AS4XX_LIN_PR669,SAS_AS4XX_LIN_PR670,SAS_AS4XX_LIN_PR668,]
    [/cover]                                                                  */
**                 XML Data Model for Lin driver                              **
**  [/cover]                                                                  **
********************************************************************************
-->
<AUTOSAR xmlns="http://autosar.org/schema/r4.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://autosar.org/schema/r4.0 AUTOSAR_4-0-3_STRICT.xsd"> 
<AR-PACKAGES>
    <AR-PACKAGE>
        <SHORT-NAME>AUTOSAR_Lin</SHORT-NAME>
        <AR-PACKAGES>
            <AR-PACKAGE>
                <SHORT-NAME>BswModuleDescriptions</SHORT-NAME>
                <CATEGORY>STANDARD</CATEGORY>
                <ELEMENTS>
                    <BSW-MODULE-DESCRIPTION>
                        <SHORT-NAME>Lin</SHORT-NAME>
                        <LONG-NAME>
                            <L-4 L="EN">LIN Driver</L-4>
                        </LONG-NAME>
                        <CATEGORY>BSW_MODULE</CATEGORY>
                        <MODULE-ID>82</MODULE-ID>
                        <PROVIDED-ENTRYS>
                            <BSW-MODULE-ENTRY-REF-CONDITIONAL>
                                <BSW-MODULE-ENTRY-REF DEST="BSW-MODULE-ENTRY">/AUTOSAR_Lin/BswModuleEntrys/Lin_Init</BSW-MODULE-ENTRY-REF>
                            </BSW-MODULE-ENTRY-REF-CONDITIONAL>
                            <BSW-MODULE-ENTRY-REF-CONDITIONAL>
                                <BSW-MODULE-ENTRY-REF DEST="BSW-MODULE-ENTRY">/AUTOSAR_Lin/BswModuleEntrys/Lin_WakeupValidation</BSW-MODULE-ENTRY-REF>
                            </BSW-MODULE-ENTRY-REF-CONDITIONAL>
                            <BSW-MODULE-ENTRY-REF-CONDITIONAL>
                                <BSW-MODULE-ENTRY-REF DEST="BSW-MODULE-ENTRY">/AUTOSAR_Lin/BswModuleEntrys/Lin_GetVersionInfo</BSW-MODULE-ENTRY-REF>
                            </BSW-MODULE-ENTRY-REF-CONDITIONAL>
                            <BSW-MODULE-ENTRY-REF-CONDITIONAL>
                                <BSW-MODULE-ENTRY-REF DEST="BSW-MODULE-ENTRY">/AUTOSAR_Lin/BswModuleEntrys/Lin_SendFrame</BSW-MODULE-ENTRY-REF>
                            </BSW-MODULE-ENTRY-REF-CONDITIONAL>
                            <BSW-MODULE-ENTRY-REF-CONDITIONAL>
                                <BSW-MODULE-ENTRY-REF DEST="BSW-MODULE-ENTRY">/AUTOSAR_Lin/BswModuleEntrys/Lin_GoToSleep</BSW-MODULE-ENTRY-REF>
                            </BSW-MODULE-ENTRY-REF-CONDITIONAL>
                            <BSW-MODULE-ENTRY-REF-CONDITIONAL>
                                <BSW-MODULE-ENTRY-REF DEST="BSW-MODULE-ENTRY">/AUTOSAR_Lin/BswModuleEntrys/Lin_GoToSleepInternal</BSW-MODULE-ENTRY-REF>
                            </BSW-MODULE-ENTRY-REF-CONDITIONAL>
                            <BSW-MODULE-ENTRY-REF-CONDITIONAL>
                                <BSW-MODULE-ENTRY-REF DEST="BSW-MODULE-ENTRY">/AUTOSAR_Lin/BswModuleEntrys/Lin_Wakeup</BSW-MODULE-ENTRY-REF>
                            </BSW-MODULE-ENTRY-REF-CONDITIONAL>
                            <BSW-MODULE-ENTRY-REF-CONDITIONAL>
                                <BSW-MODULE-ENTRY-REF DEST="BSW-MODULE-ENTRY">/AUTOSAR_Lin/BswModuleEntrys/Lin_GetStatus</BSW-MODULE-ENTRY-REF>
                            </BSW-MODULE-ENTRY-REF-CONDITIONAL>
                            <BSW-MODULE-ENTRY-REF-CONDITIONAL>
                                <BSW-MODULE-ENTRY-REF DEST="BSW-MODULE-ENTRY">/AUTOSAR_Lin/BswModuleEntrys/Lin_17_AscLin_IsrReceive</BSW-MODULE-ENTRY-REF>
                            </BSW-MODULE-ENTRY-REF-CONDITIONAL>
                            <BSW-MODULE-ENTRY-REF-CONDITIONAL>
                                <BSW-MODULE-ENTRY-REF DEST="BSW-MODULE-ENTRY">/AUTOSAR_Lin/BswModuleEntrys/Lin_17_AscLin_IsrTransmit</BSW-MODULE-ENTRY-REF>
                            </BSW-MODULE-ENTRY-REF-CONDITIONAL>
                            <BSW-MODULE-ENTRY-REF-CONDITIONAL>
                                <BSW-MODULE-ENTRY-REF DEST="BSW-MODULE-ENTRY">/AUTOSAR_Lin/BswModuleEntrys/Lin_17_AscLin_IsrError</BSW-MODULE-ENTRY-REF>
                            </BSW-MODULE-ENTRY-REF-CONDITIONAL>
                        </PROVIDED-ENTRYS>
                        <BSW-MODULE-DEPENDENCYS>
                            <BSW-MODULE-DEPENDENCY>
                                <SHORT-NAME>DetDependency</SHORT-NAME>
                                <TARGET-MODULE-ID>20</TARGET-MODULE-ID>
                                <REQUIRED-ENTRYS>
                                    <BSW-MODULE-ENTRY-REF-CONDITIONAL>
                                        <BSW-MODULE-ENTRY-REF DEST="BSW-MODULE-ENTRY">/AUTOSAR_Det/BswModuleEntrys/Det_ReportError</BSW-MODULE-ENTRY-REF>
                                    </BSW-MODULE-ENTRY-REF-CONDITIONAL>
                                </REQUIRED-ENTRYS>
                            </BSW-MODULE-DEPENDENCY>
                            <BSW-MODULE-DEPENDENCY>
                                <SHORT-NAME>EcuMDependency</SHORT-NAME>
                                <TARGET-MODULE-ID>10</TARGET-MODULE-ID>
                                <REQUIRED-ENTRYS>
                                    <BSW-MODULE-ENTRY-REF-CONDITIONAL>
                                        <BSW-MODULE-ENTRY-REF DEST="BSW-MODULE-ENTRY">/AUTOSAR_EcuM/BswModuleEntrys/EcuM_SetWakeupEvent</BSW-MODULE-ENTRY-REF>
                                    </BSW-MODULE-ENTRY-REF-CONDITIONAL>
                                    <BSW-MODULE-ENTRY-REF-CONDITIONAL>
                                        <BSW-MODULE-ENTRY-REF DEST="BSW-MODULE-ENTRY">/AUTOSAR_EcuM/BswModuleEntrys/EcuM_CheckWakeup</BSW-MODULE-ENTRY-REF>
                                    </BSW-MODULE-ENTRY-REF-CONDITIONAL>
                                </REQUIRED-ENTRYS>
                            </BSW-MODULE-DEPENDENCY>
                        </BSW-MODULE-DEPENDENCYS>
                        <INTERNAL-BEHAVIORS>
                          <BSW-INTERNAL-BEHAVIOR>
                            <SHORT-NAME>LinBehavior</SHORT-NAME>
                            <STATIC-MEMORYS>
                              <VARIABLE-DATA-PROTOTYPE>
                                <SHORT-NAME>Lin_ChannelInfo</SHORT-NAME>
                                <SW-DATA-DEF-PROPS>
                                  <SW-DATA-DEF-PROPS-VARIANTS>
                                    <SW-DATA-DEF-PROPS-CONDITIONAL>
                                    <IMPLEMENTATION-DATA-TYPE-REF DEST = "IMPLEMENTATION-DATA-TYPE"
                                      >/AUTOSAR_Lin/ImplementationDataTypes/Lin_ChannelInfoType</IMPLEMENTATION-DATA-TYPE-REF>
                                    </SW-DATA-DEF-PROPS-CONDITIONAL>
                                  </SW-DATA-DEF-PROPS-VARIANTS>
                                </SW-DATA-DEF-PROPS>
                                <TYPE-TREF DEST = "IMPLEMENTATION-DATA-TYPE"
                                >/AUTOSAR_Lin/ImplementationDataTypes/Lin_ChannelInfoType</TYPE-TREF>
                              </VARIABLE-DATA-PROTOTYPE>
                            </STATIC-MEMORYS>
                            <ENTITYS>
                              <BSW-CALLED-ENTITY>
                                <SHORT-NAME>Lin_Init</SHORT-NAME>
                                <MINIMUM-START-INTERVAL>0.0</MINIMUM-START-INTERVAL>
                                <IMPLEMENTED-ENTRY-REF DEST="BSW-MODULE-ENTRY"
                                  >/AUTOSAR_Lin/BswModuleEntrys/Lin_17_AscLin_Init</IMPLEMENTED-ENTRY-REF>
                              </BSW-CALLED-ENTITY>
                              <BSW-CALLED-ENTITY>
                                <SHORT-NAME>Lin_WakeupValidation</SHORT-NAME>
                                <MINIMUM-START-INTERVAL>0.0</MINIMUM-START-INTERVAL>
                                <IMPLEMENTED-ENTRY-REF DEST="BSW-MODULE-ENTRY"
                                  >/AUTOSAR_Lin/BswModuleEntrys/Lin_17_AscLin_WakeupValidation</IMPLEMENTED-ENTRY-REF>
                              </BSW-CALLED-ENTITY>
                              <BSW-CALLED-ENTITY>
                                <SHORT-NAME>Lin_GetVersionInfo</SHORT-NAME>
                                <MINIMUM-START-INTERVAL>0.0</MINIMUM-START-INTERVAL>
                                <IMPLEMENTED-ENTRY-REF DEST="BSW-MODULE-ENTRY"
                                  >/AUTOSAR_Lin/BswModuleEntrys/Lin_17_AscLin_GetVersionInfo</IMPLEMENTED-ENTRY-REF>
                              </BSW-CALLED-ENTITY>
                              <BSW-CALLED-ENTITY>
                                <SHORT-NAME>Lin_SendFrame</SHORT-NAME>
                                <MINIMUM-START-INTERVAL>0.0</MINIMUM-START-INTERVAL>
                                <IMPLEMENTED-ENTRY-REF DEST="BSW-MODULE-ENTRY"
                                  >/AUTOSAR_Lin/BswModuleEntrys/Lin_17_AscLin_SendFrame</IMPLEMENTED-ENTRY-REF>
                              </BSW-CALLED-ENTITY>
                              <BSW-CALLED-ENTITY>
                                <SHORT-NAME>Lin_GoToSleep</SHORT-NAME>
                                <MINIMUM-START-INTERVAL>0.0</MINIMUM-START-INTERVAL>
                                <IMPLEMENTED-ENTRY-REF DEST="BSW-MODULE-ENTRY"
                                  >/AUTOSAR_Lin/BswModuleEntrys/Lin_17_AscLin_GoToSleep</IMPLEMENTED-ENTRY-REF>
                              </BSW-CALLED-ENTITY>
                              <BSW-CALLED-ENTITY>
                                <SHORT-NAME>Lin_GoToSleepInternal</SHORT-NAME>
                                <MINIMUM-START-INTERVAL>0.0</MINIMUM-START-INTERVAL>
                                <IMPLEMENTED-ENTRY-REF DEST="BSW-MODULE-ENTRY"
                                  >/AUTOSAR_Lin/BswModuleEntrys/Lin_17_AscLin_GoToSleepInternal</IMPLEMENTED-ENTRY-REF>
                              </BSW-CALLED-ENTITY>
                              <BSW-CALLED-ENTITY>
                                <SHORT-NAME>Lin_Wakeup</SHORT-NAME>
                                <MINIMUM-START-INTERVAL>0.0</MINIMUM-START-INTERVAL>
                                <IMPLEMENTED-ENTRY-REF DEST="BSW-MODULE-ENTRY"
                                  >/AUTOSAR_Lin/BswModuleEntrys/Lin_17_AscLin_Wakeup</IMPLEMENTED-ENTRY-REF>
                              </BSW-CALLED-ENTITY>
                              <BSW-CALLED-ENTITY>
                                <SHORT-NAME>Lin_GetStatus</SHORT-NAME>
                                <MINIMUM-START-INTERVAL>0.0</MINIMUM-START-INTERVAL>
                                <IMPLEMENTED-ENTRY-REF DEST="BSW-MODULE-ENTRY"
                                  >/AUTOSAR_Lin/BswModuleEntrys/Lin_17_AscLin_GetStatus</IMPLEMENTED-ENTRY-REF>
                              </BSW-CALLED-ENTITY>
                              <BSW-INTERRUPT-ENTITY>
                                  <SHORT-NAME>Lin_IsrReceive</SHORT-NAME>
                                  <MINIMUM-START-INTERVAL>0.0</MINIMUM-START-INTERVAL>
                                  <IMPLEMENTED-ENTRY-REF DEST="BSW-MODULE-ENTRY"
                                  >/AUTOSAR_Lin/BswModuleEntrys/Lin_17_AscLin_IsrReceive</IMPLEMENTED-ENTRY-REF>
                                  <INTERRUPT-CATEGORY>CAT-1</INTERRUPT-CATEGORY>
                                  <INTERRUPT-SOURCE>SRC_ASCLINmRX</INTERRUPT-SOURCE>
                              </BSW-INTERRUPT-ENTITY>
                              <BSW-INTERRUPT-ENTITY>
                                  <SHORT-NAME>Lin_IsrTransmit</SHORT-NAME>
                                  <MINIMUM-START-INTERVAL>0.0</MINIMUM-START-INTERVAL>
                                  <IMPLEMENTED-ENTRY-REF DEST="BSW-MODULE-ENTRY"
                                  >/AUTOSAR_Lin/BswModuleEntrys/Lin_17_AscLin_IsrTransmit</IMPLEMENTED-ENTRY-REF>
                                  <INTERRUPT-CATEGORY>CAT-1</INTERRUPT-CATEGORY>
                                  <INTERRUPT-SOURCE>SRC_ASCLINmTX</INTERRUPT-SOURCE>
                              </BSW-INTERRUPT-ENTITY>
                              <BSW-INTERRUPT-ENTITY>
                                  <SHORT-NAME>Lin_IsrError</SHORT-NAME>
                                  <MINIMUM-START-INTERVAL>0.0</MINIMUM-START-INTERVAL>
                                  <IMPLEMENTED-ENTRY-REF DEST="BSW-MODULE-ENTRY"
                                  >/AUTOSAR_Lin/BswModuleEntrys/Lin_17_AscLin_IsrError</IMPLEMENTED-ENTRY-REF>
                                  <INTERRUPT-CATEGORY>CAT-1</INTERRUPT-CATEGORY>
                                  <INTERRUPT-SOURCE>SRC_ASCLINmEX</INTERRUPT-SOURCE>
                              </BSW-INTERRUPT-ENTITY>
                            </ENTITYS>
                          </BSW-INTERNAL-BEHAVIOR>
                        </INTERNAL-BEHAVIORS>
                    </BSW-MODULE-DESCRIPTION>
                </ELEMENTS>
            </AR-PACKAGE>
            <AR-PACKAGE>
                <SHORT-NAME>BswModuleEntrys</SHORT-NAME>
                <CATEGORY>STANDARD</CATEGORY>
                <ELEMENTS>
                    <BSW-MODULE-ENTRY>
                        <SHORT-NAME>Lin_17_AscLin_IsrReceive</SHORT-NAME>
                        <SERVICE-ID>0x0</SERVICE-ID>
                        <IS-REENTRANT>true</IS-REENTRANT>
                        <IS-SYNCHRONOUS>true</IS-SYNCHRONOUS>
                        <CALL-TYPE>INTERRUPT</CALL-TYPE>
                        <EXECUTION-CONTEXT>INTERRUPT-CAT-1</EXECUTION-CONTEXT>
                        <SW-SERVICE-IMPL-POLICY>STANDARD</SW-SERVICE-IMPL-POLICY>
                        <ARGUMENTS>
                          <SW-SERVICE-ARG>
                            <SHORT-NAME>HwUnit</SHORT-NAME>
                            <SW-DATA-DEF-PROPS>
                              <SW-DATA-DEF-PROPS-VARIANTS>
                                <SW-DATA-DEF-PROPS-CONDITIONAL>
                                <BASE-TYPE-REF DEST="SW-BASE-TYPE"
                                >/AUTOSAR_Platform/BaseTypes/uint8</BASE-TYPE-REF>
                                </SW-DATA-DEF-PROPS-CONDITIONAL>
                              </SW-DATA-DEF-PROPS-VARIANTS>
                            </SW-DATA-DEF-PROPS>
                          </SW-SERVICE-ARG>
                        </ARGUMENTS>
                    </BSW-MODULE-ENTRY>
                    <BSW-MODULE-ENTRY>
                        <SHORT-NAME>Lin_17_AscLin_IsrTransmit</SHORT-NAME>
                        <SERVICE-ID>0x0</SERVICE-ID>
                        <IS-REENTRANT>true</IS-REENTRANT>
                        <IS-SYNCHRONOUS>true</IS-SYNCHRONOUS>
                        <CALL-TYPE>INTERRUPT</CALL-TYPE>
                        <EXECUTION-CONTEXT>INTERRUPT-CAT-1</EXECUTION-CONTEXT>
                        <SW-SERVICE-IMPL-POLICY>STANDARD</SW-SERVICE-IMPL-POLICY>
                        <ARGUMENTS>
                          <SW-SERVICE-ARG>
                            <SHORT-NAME>HwUnit</SHORT-NAME>
                            <SW-DATA-DEF-PROPS>
                              <SW-DATA-DEF-PROPS-VARIANTS>
                                <SW-DATA-DEF-PROPS-CONDITIONAL>
                                <BASE-TYPE-REF DEST="SW-BASE-TYPE"
                                >/AUTOSAR_Platform/BaseTypes/uint8</BASE-TYPE-REF>
                                </SW-DATA-DEF-PROPS-CONDITIONAL>
                              </SW-DATA-DEF-PROPS-VARIANTS>
                            </SW-DATA-DEF-PROPS>
                          </SW-SERVICE-ARG>
                        </ARGUMENTS>
                    </BSW-MODULE-ENTRY>
                    <BSW-MODULE-ENTRY>
                        <SHORT-NAME>Lin_17_AscLin_IsrError</SHORT-NAME>
                        <SERVICE-ID>0x0</SERVICE-ID>
                        <IS-REENTRANT>true</IS-REENTRANT>
                        <IS-SYNCHRONOUS>true</IS-SYNCHRONOUS>
                        <CALL-TYPE>INTERRUPT</CALL-TYPE>
                        <EXECUTION-CONTEXT>INTERRUPT-CAT-1</EXECUTION-CONTEXT>
                        <SW-SERVICE-IMPL-POLICY>STANDARD</SW-SERVICE-IMPL-POLICY>
                        <ARGUMENTS>
                          <SW-SERVICE-ARG>
                            <SHORT-NAME>HwUnit</SHORT-NAME>
                            <SW-DATA-DEF-PROPS>
                              <SW-DATA-DEF-PROPS-VARIANTS>
                                <SW-DATA-DEF-PROPS-CONDITIONAL>
                                <BASE-TYPE-REF DEST="SW-BASE-TYPE"
                                >/AUTOSAR_Platform/BaseTypes/uint8</BASE-TYPE-REF>
                                </SW-DATA-DEF-PROPS-CONDITIONAL>
                              </SW-DATA-DEF-PROPS-VARIANTS>
                            </SW-DATA-DEF-PROPS>
                          </SW-SERVICE-ARG>
                        </ARGUMENTS>
                    </BSW-MODULE-ENTRY>
                </ELEMENTS>
            </AR-PACKAGE>
            <AR-PACKAGE>
                <SHORT-NAME>Implementations</SHORT-NAME>
                <ELEMENTS>
                    <BSW-IMPLEMENTATION>
                        <SHORT-NAME>Lin</SHORT-NAME>
                        <CODE-DESCRIPTORS>
                            <CODE>
                                <SHORT-NAME>Files</SHORT-NAME>
                                <ARTIFACT-DESCRIPTORS>
                                    <AUTOSAR-ENGINEERING-OBJECT>
                                        <SHORT-LABEL>ssc::src::Lin_17_AscLin.c</SHORT-LABEL>
                                        <CATEGORY>SWSRC</CATEGORY>
                                    </AUTOSAR-ENGINEERING-OBJECT>
                                    <AUTOSAR-ENGINEERING-OBJECT>
                                        <SHORT-LABEL>ssc::inc::Lin_17_AscLin.h</SHORT-LABEL>
                                        <CATEGORY>SWHDR</CATEGORY>
                                    </AUTOSAR-ENGINEERING-OBJECT>
                                    <AUTOSAR-ENGINEERING-OBJECT>
                                        <SHORT-LABEL>mak::lin_17_asclin_infineon_tricore::ssc::mak::infineon_lin_17_asclin_cfg.mak</SHORT-LABEL>
                                        <CATEGORY>SWMAKE</CATEGORY>
                                    </AUTOSAR-ENGINEERING-OBJECT>
                                    <AUTOSAR-ENGINEERING-OBJECT>
                                        <SHORT-LABEL>mak::lin_17_asclin_infineon_tricore::ssc::mak::infineon_lin_17_asclin_check.mak</SHORT-LABEL>
                                        <CATEGORY>SWMAKE</CATEGORY>
                                    </AUTOSAR-ENGINEERING-OBJECT>
                                    <AUTOSAR-ENGINEERING-OBJECT>
                                        <SHORT-LABEL>mak::lin_17_asclin_infineon_tricore::ssc::mak::infineon_lin_17_asclin_defs.mak</SHORT-LABEL>
                                        <CATEGORY>SWMAKE</CATEGORY>
                                    </AUTOSAR-ENGINEERING-OBJECT>
                                    <AUTOSAR-ENGINEERING-OBJECT>
                                        <SHORT-LABEL>mak::lin_17_asclin_infineon_tricore::ssc::mak::infineon_lin_17_asclin_rules.mak</SHORT-LABEL>
                                        <CATEGORY>SWMAKE</CATEGORY>
                                    </AUTOSAR-ENGINEERING-OBJECT>
                                </ARTIFACT-DESCRIPTORS>
                            </CODE>
                        </CODE-DESCRIPTORS>
                        <COMPILERS>
                            <COMPILER>
                                <SHORT-NAME>TASKING_COMPILE</SHORT-NAME>
                                <NAME>TASKING</NAME>
                                <OPTIONS>&quot;--core=tc1.6.x -t -Wa-gAHLs --emit-locals=-equ,-symbols -Wa-Ogs -Wa--error-limit=42 --iso=90 --eabi-compliant --integer-enumeration --language=-comments,-gcc,+volatile,-strings --switch=auto --align=0 --default-near-size=0 --default-a0-size=0 --default-a1-size=0 -O2ROP --tradeoff=4 -g --source -D_TASKING_C_TRICORE_=1 $(CALLFUNCTION)&quot;</OPTIONS>
                                <VENDOR>Tasking</VENDOR>
                                <VERSION>Tasking</VERSION>
                            </COMPILER>
                            <COMPILER>
                                <SHORT-NAME>DIAB_COMPILE</SHORT-NAME>
                                <NAME>DIAB</NAME>
                                <OPTIONS>&quot;-Xdialect-c89 -Xno-common -Xalign-functions=4 -Xfp-fast -Xsection-split=1 -Xkeep-assembly-file=2 -g3 -ew4084 -ei4177,4550,4549,4068,5388 -XO -D_DIABDATA_C_TRICORE_=1 $(CALLFUNCTION)&quot;</OPTIONS>
                                <VENDOR>WindRiver</VENDOR>
                                <VERSION>WindRiver</VERSION>
                            </COMPILER>
                            <COMPILER>
                                <SHORT-NAME>GNU_COMPILE</SHORT-NAME>
                                <NAME>GNU</NAME>
                                <OPTIONS>&quot;-save-temps=obj -std=iso9899:1990 -ansi -fno-asm -ffreestanding -Wundef -Wp,-std=iso9899:1990 -fno-short-enums -fpeel-loops -falign-functions=4 -frecord-gcc-switches -fsection-anchors -funsigned-bitfields -ffunction-sections -nostartfiles -O3 -g3 -W -Wall -Wuninitialized -DTRIBOARD_TC27XX -mcpu=tc27xx -D_GNU_C_TRICORE_=1 $(CALLFUNCTION)&quot;</OPTIONS>
                                <VENDOR>Hightec</VENDOR>
                                <VERSION>Hightec</VERSION>
                            </COMPILER>
                        </COMPILERS>
                        <GENERATED-ARTIFACTS>
                            <DEPENDENCY-ON-ARTIFACT>
                                <SHORT-NAME>Lin_Cfg_h</SHORT-NAME>
                                <ARTIFACT-DESCRIPTOR>
                                    <SHORT-LABEL>generate::inc::Lin_17_AscLin_Cfg.h</SHORT-LABEL>
                                    <CATEGORY>SWHDR</CATEGORY>
                                </ARTIFACT-DESCRIPTOR>
                                <USAGES>
                                    <USAGE>CODEGENERATION</USAGE>
                                </USAGES>
                            </DEPENDENCY-ON-ARTIFACT>
                            <DEPENDENCY-ON-ARTIFACT>
                                <SHORT-NAME>Lin_PBcfg_c</SHORT-NAME>
                                <ARTIFACT-DESCRIPTOR>
                                    <SHORT-LABEL>generate::src::Lin_17_AscLin_PBcfg.c</SHORT-LABEL>
                                    <CATEGORY>SWSRC</CATEGORY>
                                </ARTIFACT-DESCRIPTOR>
                                <USAGES>
                                    <USAGE>CODEGENERATION</USAGE>
                                </USAGES>
                            </DEPENDENCY-ON-ARTIFACT>
                            <DEPENDENCY-ON-ARTIFACT>
                                <SHORT-NAME>Lin_xdm</SHORT-NAME>
                                <ARTIFACT-DESCRIPTOR>
                                    <SHORT-LABEL>Config::Lin_17_AscLin.xdm</SHORT-LABEL>
                                    <CATEGORY>SWTOOL</CATEGORY>
                                </ARTIFACT-DESCRIPTOR>
                                <USAGES>
                                    <USAGE>CODEGENERATION</USAGE>
                                </USAGES>
                            </DEPENDENCY-ON-ARTIFACT>
                            <DEPENDENCY-ON-ARTIFACT>
                                <SHORT-NAME>plugin_xml</SHORT-NAME>
                                <ARTIFACT-DESCRIPTOR>
                                    <SHORT-LABEL>Lin_17_AscLin_Aurix::plugin.xml</SHORT-LABEL>
                                    <CATEGORY>SWTOOL</CATEGORY>
                                </ARTIFACT-DESCRIPTOR>
                                <USAGES>
                                    <USAGE>CODEGENERATION</USAGE>
                                </USAGES>
                            </DEPENDENCY-ON-ARTIFACT>
                            <DEPENDENCY-ON-ARTIFACT>
                                <SHORT-NAME>Lin_bmd</SHORT-NAME>
                                <ARTIFACT-DESCRIPTOR>
                                    <SHORT-LABEL>autosar::Lin_17_AscLin.bmd</SHORT-LABEL>
                                    <CATEGORY>SWTOOL</CATEGORY>
                                </ARTIFACT-DESCRIPTOR>
                                <USAGES>
                                    <USAGE>CODEGENERATION</USAGE>
                                </USAGES>
                            </DEPENDENCY-ON-ARTIFACT>
                        </GENERATED-ARTIFACTS>
                        <LINKERS>
                            <LINKER>
                                <SHORT-NAME>TASKING_LINK</SHORT-NAME>
                                <NAME>TASKING</NAME>
                                <OPTIONS>&quot;-t -d_mcal_pjt.lsl -D__CPU__=tc27x -Cmpe:vtc -OtXYCL --error-limit=42 -M -mcrfiklsmnoduq -lc -lfp -lrt&quot;</OPTIONS>
                                <VENDOR>Tasking</VENDOR>
                                <VERSION>Tasking</VERSION>
                            </LINKER>
                            <LINKER>
                                <SHORT-NAME>DIAB_LINK</SHORT-NAME>
                                <NAME>DIAB</NAME>
                                <OPTIONS>&quot;-m6 &quot;_mcal_pjt.dld&quot; -lc&quot;</OPTIONS>
                                <VENDOR>WindRiver</VENDOR>
                                <VERSION>WindRiver</VERSION>
                            </LINKER>
                            <LINKER>
                                <SHORT-NAME>GNU_LINK</SHORT-NAME>
                                <NAME>GNU</NAME>
                                <OPTIONS>&quot;-T&quot;_mcal_pjt.ld&quot; -nostartfiles -Wl,--allow-multiple-definition -Wl,--cref -Wl,--oformat=elf32-tricore -mcpu=tc27xx -Wl,--mem-holes -Wl,--extmap=&quot;a&quot;&quot;</OPTIONS>
                                <VENDOR>Hightec</VENDOR>
                                <VERSION>Hightec</VERSION>
                            </LINKER>
                        </LINKERS>
                        <PROGRAMMING-LANGUAGE>C</PROGRAMMING-LANGUAGE>
                        <RESOURCE-CONSUMPTION>
                            <SHORT-NAME>ResourceConsumption</SHORT-NAME>
                            <MEMORY-SECTIONS>
                                <MEMORY-SECTION>
                                    <SHORT-NAME>LIN_17_ASCLIN_CODE</SHORT-NAME>
                                    <ALIGNMENT>UNSPECIFIED</ALIGNMENT>
                                    <SIZE>3298</SIZE>
                                    <SW-ADDRMETHOD-REF DEST="SW-ADDR-METHOD">/AUTOSAR_MemMap/SwAddrMethods/CODE</SW-ADDRMETHOD-REF>
                                </MEMORY-SECTION>
                                <MEMORY-SECTION>
                                    <SHORT-NAME>LIN_17_ASCLIN_VAR_8BIT</SHORT-NAME>
                                    <ALIGNMENT>8</ALIGNMENT>
                                    <SIZE>41</SIZE>
                                    <SW-ADDRMETHOD-REF DEST="SW-ADDR-METHOD">/AUTOSAR_MemMap/SwAddrMethods/VAR</SW-ADDRMETHOD-REF>
                                </MEMORY-SECTION>
                                <MEMORY-SECTION>
                                    <SHORT-NAME>LIN_17_ASCLIN_VAR_16BIT</SHORT-NAME>
                                    <ALIGNMENT>16</ALIGNMENT>
                                    <SIZE>8</SIZE>
                                    <SW-ADDRMETHOD-REF DEST="SW-ADDR-METHOD">/AUTOSAR_MemMap/SwAddrMethods/VAR</SW-ADDRMETHOD-REF>
                                </MEMORY-SECTION>
                                <MEMORY-SECTION>
                                    <SHORT-NAME>LIN_17_ASCLIN_VAR_32BIT</SHORT-NAME>
                                    <ALIGNMENT>32</ALIGNMENT>
                                    <SIZE>18</SIZE>
                                    <SW-ADDRMETHOD-REF DEST="SW-ADDR-METHOD">/AUTOSAR_MemMap/SwAddrMethods/VAR</SW-ADDRMETHOD-REF>
                                </MEMORY-SECTION>
                                <MEMORY-SECTION>
                                    <SHORT-NAME>LIN_17_ASCLIN_VAR_UNSPECIFIED</SHORT-NAME>
                                    <ALIGNMENT>UNSPECIFIED</ALIGNMENT>
                                    <SIZE>60</SIZE>
                                    <SW-ADDRMETHOD-REF DEST="SW-ADDR-METHOD">/AUTOSAR_MemMap/SwAddrMethods/VAR</SW-ADDRMETHOD-REF>
                                </MEMORY-SECTION>
                                <MEMORY-SECTION>
                                    <SHORT-NAME>LIN_17_ASCLIN_CONST_32BIT</SHORT-NAME>
                                    <ALIGNMENT>32</ALIGNMENT>
                                    <SIZE>18</SIZE>
                                    <SW-ADDRMETHOD-REF DEST="SW-ADDR-METHOD">/AUTOSAR_MemMap/SwAddrMethods/CONST</SW-ADDRMETHOD-REF>
                                </MEMORY-SECTION>
                            </MEMORY-SECTIONS>
                            <SECTION-NAME-PREFIXS>
                                <SECTION-NAME-PREFIX>
                                    <SHORT-NAME>LIN_17_ASCLIN</SHORT-NAME>
                                    <SYMBOL>LIN_17_ASCLIN</SYMBOL>
                                </SECTION-NAME-PREFIX>
                            </SECTION-NAME-PREFIXS>
                        </RESOURCE-CONSUMPTION>
                        <SW-VERSION>0.0.0</SW-VERSION>
                        <VENDOR-ID>17</VENDOR-ID>
                        <AR-RELEASE-VERSION>4.0.3</AR-RELEASE-VERSION>
                        <BEHAVIOR-REF DEST="BSW-INTERNAL-BEHAVIOR">/AUTOSAR_Lin/BswModuleDescriptions/Lin/LinBehavior</BEHAVIOR-REF>
                        <VENDOR-API-INFIX>AscLin</VENDOR-API-INFIX>
                        <VENDOR-SPECIFIC-MODULE-DEF-REFS>
                                <VENDOR-SPECIFIC-MODULE-DEF-REF DEST="ECUC-MODULE-DEF">/AURIX/Lin</VENDOR-SPECIFIC-MODULE-DEF-REF>
                        </VENDOR-SPECIFIC-MODULE-DEF-REFS>
                    </BSW-IMPLEMENTATION>
                </ELEMENTS>
            </AR-PACKAGE>
        </AR-PACKAGES>
    </AR-PACKAGE>
</AR-PACKAGES>
</AUTOSAR>
