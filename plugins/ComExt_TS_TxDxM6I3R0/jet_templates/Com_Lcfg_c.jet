<%@ jet package="dreisoft.tresos.com.generated.jet" class="Com_Lcfg_c"
        imports="dreisoft.tresos.generator.ng.api.ant.JavaGenContext
            dreisoft.tresos.com.generator.Com_ConfigLayoutType
            java.util.HashSet
            java.util.LinkedHashSet
            java.io.IOException
            dreisoft.tresos.com.generated.operationstatus.GeneratorOperationStatus" %>
<%
    JavaGenContext context = null;
    try {
        context = JavaGenContext.get( argument, stringBuffer );
    } catch( IOException e ) {
        e.printStackTrace();
        return "";
    }

    boolean comGenerationSuccessful = !context.gen.isVerifying();

    // get post-build data model
    Com_ConfigLayoutType comPBCfgDataModel = (Com_ConfigLayoutType) context.gen.getVariable("comPBCfgDataModel", Com_ConfigLayoutType.class, null);

    String sequenceSeperator;

    HashSet<String> callOutSet = new LinkedHashSet<String>();
%>

<%@ include file="AutoCoreHeader.jet" %>

/*==================[inclusions]============================================*/

#include <Com_Lcfg.h>
#include <Com_Lcfg_Static.h>
#include <TSAutosar.h>

/* start data section declaration */
#define COM_START_SEC_VAR_NOINIT_UNSPECIFIED
#include <MemMap.h>

/**
 * Internal memory statically allocated upon link-time. Size depends on
 * post build configuration. Memory requirements of post build configuration
 * must be smaller than COM_RAM_SIZE_MAX in order to avoid buffer
 * overflows. Memory is aligned to the the most stringent alignment requirement
 * of any simple data type available on the respective platform, e.g. uint32.
 */
STATIC TS_DefMaxAlignedByteArray(Com_DataMem, COM, VAR_NOINIT, COM_DATA_MEM_SIZE);

/* stop data section declaration */
#define COM_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#include <MemMap.h>



/* start data section declaration */
#define COM_START_SEC_CONST_UNSPECIFIED
#include <MemMap.h>

/**
 * Pointer to statically allocated RAM.
 */
CONSTP2VAR(uint8, COM_CONST, COM_VAR_NOINIT) Com_gDataMemPtr =
  (P2VAR(uint8, COM_CONST, COM_VAR_NOINIT))
  ((P2VAR(void, COM_CONST, COM_VAR_NOINIT)) Com_DataMem);

/* stop data section declaration */
#define COM_STOP_SEC_CONST_UNSPECIFIED
#include <MemMap.h>



#define COM_START_SEC_APPL_CODE
#include <MemMap.h>

/* send callouts */
<%
    for ( String callout : comPBCfgDataModel.getTxIPDUCallOuts() ) {
        if (callOutSet.contains(callout)) {
%>
/* extern FUNC(boolean, COM_APPL_CODE) <%= callout %>(PduIdType ID, P2VAR(uint8,
    AUTOMATIC, COM_VAR_NOINIT) ipduD); (extern declaration already generated) */
<%
        }
        else {
%>
extern FUNC(boolean, COM_APPL_CODE) <%= callout %>(PduIdType ID, P2VAR(uint8,
    AUTOMATIC, COM_VAR_NOINIT) ipduD);
<%
            callOutSet.add(callout);
        }
    }
%>

/* receive callouts */
<%
    for ( String callout : comPBCfgDataModel.getRxIPDUCallOuts() ) {
        if (callOutSet.contains(callout)) {
%>
/* extern FUNC(boolean, COM_APPL_CODE) <%= callout %>(PduIdType ID, P2CONST(uint8,
    AUTOMATIC, COM_APPL_DATA) ipduD); (extern declaration already generated) */
<%
        }
        else {
%>
extern FUNC(boolean, COM_APPL_CODE) <%= callout %>(PduIdType ID, P2CONST(uint8,
    AUTOMATIC, COM_APPL_DATA) ipduD);
<%
            callOutSet.add(callout);
        }
    }
%>

/* TX acknowledge callbacks */
<%
    for (String notification : comPBCfgDataModel.getSigTxAckNotifications()) {
        if (callOutSet.contains(notification)) {
%>
/* extern FUNC(void, COM_APPL_CODE) <%= notification %>(void); (extern declaration already generated)*/
<%
        }
        else {
%>
extern FUNC(void, COM_APPL_CODE) <%= notification %>(void);
<%
            callOutSet.add(notification);
        }
    }
%>

/* TX timeout callbacks */
<%
    for (String notification : comPBCfgDataModel.getSigTxTOutNotifications()) {
        if (callOutSet.contains(notification)) {
%>
/* extern FUNC(void, COM_APPL_CODE) <%= notification %>(void); (extern declaration already generated)*/
<%
        }
        else {
%>
extern FUNC(void, COM_APPL_CODE) <%= notification %>(void);
<%
            callOutSet.add(notification);
        }
    }
%>

/* TX error callbacks */
<%
    for (String notification : comPBCfgDataModel.getSigTxErrNotifications()) {
        if (callOutSet.contains(notification)) {
%>
/* extern FUNC(void, COM_APPL_CODE) <%= notification %>(void); (extern declaration already generated)*/
<%
        }
        else {
%>
extern FUNC(void, COM_APPL_CODE) <%= notification %>(void);
<%
            callOutSet.add(notification);
        }
    }
%>

/* RX timeout callbacks */
<%
    for (String notification : comPBCfgDataModel.getSigRxTOutNotifications()) {
        if (callOutSet.contains(notification)) {
%>
/* extern FUNC(void, COM_APPL_CODE) <%= notification %>(void); (extern declaration already generated)*/
<%
        }
        else {
%>
extern FUNC(void, COM_APPL_CODE) <%= notification %>(void);
<%
            callOutSet.add(notification);
        }
    }
%>

/* RX acknowledge callbacks */
<%
    for (String notification : comPBCfgDataModel.getSigRxAckNotifications()) {
        if (callOutSet.contains(notification)) {
%>
/* extern FUNC(void, COM_APPL_CODE) <%= notification %>(void); (extern declaration already generated)*/
<%
        }
        else {
%>
extern FUNC(void, COM_APPL_CODE) <%= notification %>(void);
<%
            callOutSet.add(notification);
        }
    }
%>

#define COM_STOP_SEC_APPL_CODE
#include <MemMap.h>

#define COM_START_CONFIG_DATA_UNSPECIFIED
#include <MemMap.h>


#if (COM_CALL_OUT_FUNC_PTR_ARRAY_SIZE_MAX != COM_INDEX_NONE)
#if (COM_TX_CALL_OUT_FUNC_PTR_ARRAY_SIZE > 0)
/* send callouts */
CONSTP2VAR(Com_TxCalloutType, AUTOMATIC, COM_APPL_CODE) Com_TxCallouts[COM_TX_CALL_OUT_FUNC_PTR_ARRAY_SIZE] =
{
<%
    sequenceSeperator = " &";
    for ( String callout : comPBCfgDataModel.getTxIPDUCallOuts() ) {
%>
    <%= sequenceSeperator + callout %>
<%
        sequenceSeperator = ",&";
    }
%>
};
#else /* (COM_CALL_OUT_FUNC_PTR_ARRAY_SIZE > 0) */
CONSTP2VAR(Com_TxCalloutType, AUTOMATIC, COM_APPL_CODE) Com_TxCallouts[1] =
{
    NULL_PTR
};
#endif /* (COM_CALL_OUT_FUNC_PTR_ARRAY_SIZE > 0) */


#if (COM_RX_CALL_OUT_FUNC_PTR_ARRAY_SIZE > 0)
/* receive callouts */
CONSTP2VAR(Com_RxCalloutType, AUTOMATIC, COM_APPL_CODE) Com_RxCallouts[COM_RX_CALL_OUT_FUNC_PTR_ARRAY_SIZE] =
{
<%
    sequenceSeperator = " &";
    for ( String callout : comPBCfgDataModel.getRxIPDUCallOuts() ) {
%>
    <%= sequenceSeperator + callout %>
<%
        sequenceSeperator = ",&";
    }
%>
};
#else /* (COM_RX_CALL_OUT_FUNC_PTR_ARRAY_SIZE > 0) */
CONSTP2VAR(Com_RxCalloutType, AUTOMATIC, COM_APPL_CODE) Com_RxCallouts[1] =
{
    NULL_PTR
};
#endif /* (COM_RX_CALL_OUT_FUNC_PTR_ARRAY_SIZE > 0) */
#endif /* (COM_CALL_OUT_FUNC_PTR_ARRAY_SIZE_MAX != COM_INDEX_NONE) */


#if (COM_CBK_TX_ACK_PTR_ARRAY_SIZE_MAX != COM_INDEX_NONE)
#if (COM_CBK_TX_ACK_PTR_ARRAY_SIZE > 0)
CONSTP2VAR(Com_CbkTxAck_Type, AUTOMATIC, COM_RTE_CODE) Com_CbkTxAck_Array[COM_CBK_TX_ACK_PTR_ARRAY_SIZE] =
{
<%
    sequenceSeperator = " &";
    for (String notification : comPBCfgDataModel.getSigTxAckNotifications()) {
%>
    <%= sequenceSeperator + notification %>
<%
        sequenceSeperator = ",&";
    }
%>
};
#else /* (COM_CBK_TX_ACK_PTR_ARRAY_SIZE > 0) */
CONSTP2VAR(Com_CbkTxAck_Type, AUTOMATIC, COM_RTE_CODE) Com_CbkTxAck_Array[1] =
{
    NULL_PTR
};
#endif /* (COM_CBK_TX_ACK_PTR_ARRAY_SIZE > 0) */
#endif /* (COM_CBK_TX_ACK_PTR_ARRAY_SIZE_MAX != COM_INDEX_NONE) */


#if (COM_CBK_TX_T_OUT_ARRAY_SIZE_MAX != COM_INDEX_NONE)
#if (COM_CBK_TX_T_OUT_ARRAY_SIZE > 0)
CONSTP2VAR(Com_CbkTxTOut_Type, AUTOMATIC, COM_RTE_CODE) Com_CbkTxTOut_Array[COM_CBK_TX_T_OUT_ARRAY_SIZE] =
{
<%
    sequenceSeperator = " &";
    for ( String notification : comPBCfgDataModel.getSigTxTOutNotifications() ) {
%>
    <%= sequenceSeperator + notification %>
<%
        sequenceSeperator = ",&";
    }
%>
};
#else /* (COM_CBK_TX_T_OUT_ARRAY_SIZE > 0) */
CONSTP2VAR(Com_CbkTxTOut_Type, AUTOMATIC, COM_RTE_CODE) Com_CbkTxTOut_Array[1] =
{
    NULL_PTR
};
#endif /* (COM_CBK_TX_T_OUT_ARRAY_SIZE > 0) */
#endif /* (COM_CBK_TX_T_OUT_ARRAY_SIZE_MAX != COM_INDEX_NONE) */


#if (COM_CBK_TX_ERR_PTR_ARRAY_SIZE_MAX != COM_INDEX_NONE)
#if (COM_CBK_TX_ERR_PTR_ARRAY_SIZE > 0)
CONSTP2VAR(Com_CbkTxErr_Type, AUTOMATIC, COM_RTE_CODE) Com_CbkTxErr_Array[COM_CBK_TX_ERR_PTR_ARRAY_SIZE] =
{
<%
    sequenceSeperator = " &";
    for ( String notification : comPBCfgDataModel.getSigTxErrNotifications() ) {
%>
    <%= sequenceSeperator + notification %>
<%
        sequenceSeperator = ",&";
    }
%>
};
#else /* (COM_CBK_TX_ERR_PTR_ARRAY_SIZE > 0) */
CONSTP2VAR(Com_CbkTxErr_Type, AUTOMATIC, COM_RTE_CODE) Com_CbkTxErr_Array[1] =
{
    NULL_PTR
};
#endif /* (COM_CBK_TX_ERR_PTR_ARRAY_SIZE > 0) */
#endif /* (COM_CBK_TX_ERR_PTR_ARRAY_SIZE_MAX != COM_INDEX_NONE) */


#if (COM_CBK_RX_ACK_PTR_ARRAY_SIZE_MAX != COM_INDEX_NONE)
#if (COM_CBK_RX_ACK_PTR_ARRAY_SIZE > 0)
CONSTP2VAR(Com_CbkRxAck_Type, AUTOMATIC, COM_RTE_CODE) Com_CbkRxAck_Array[COM_CBK_RX_ACK_PTR_ARRAY_SIZE] =
{
<%
    sequenceSeperator = " &";
    for (String notification : comPBCfgDataModel.getSigRxAckNotifications()) {
%>
    <%= sequenceSeperator + notification %>
<%
        sequenceSeperator = ",&";
    }
%>
};
#else /* (COM_CBK_RX_ACK_PTR_ARRAY_SIZE > 0)) */
CONSTP2VAR(Com_CbkRxAck_Type, AUTOMATIC, COM_RTE_CODE) Com_CbkRxAck_Array[1] =
{
    NULL_PTR
};
#endif /* (COM_CBK_RX_ACK_PTR_ARRAY_SIZE > 0)) */
#endif /* (COM_CBK_RX_ACK_PTR_ARRAY_SIZE_MAX != COM_INDEX_NONE) */


#if (COM_CBK_RX_T_OUT_ARRAY_SIZE_MAX != COM_INDEX_NONE)
#if (COM_CBK_RX_T_OUT_ARRAY_SIZE > 0)
CONSTP2VAR(Com_CbkRxTOut_Type, AUTOMATIC, COM_RTE_CODE) Com_CbkRxTOut_Array[COM_CBK_RX_T_OUT_ARRAY_SIZE] =
{
<%
    sequenceSeperator = " &";
    for ( String notification : comPBCfgDataModel.getSigRxTOutNotifications() ) {
%>
    <%= sequenceSeperator + notification %>
<%
        sequenceSeperator = ",&";
    }
%>
};
#else /* (COM_CBK_RX_T_OUT_ARRAY_SIZE > 0) */
CONSTP2VAR(Com_CbkRxTOut_Type, AUTOMATIC, COM_RTE_CODE) Com_CbkRxTOut_Array[1] =
{
    NULL_PTR
};
#endif /* (COM_CBK_RX_T_OUT_ARRAY_SIZE > 0) */
#endif /* (COM_CBK_RX_T_OUT_ARRAY_SIZE_MAX != COM_INDEX_NONE) */


#define COM_STOP_CONFIG_DATA_UNSPECIFIED
#include <MemMap.h>


/* start data section declaration */
#define COM_START_SEC_CONST_32
#include <MemMap.h>

/**
 * Variable holding link-time configuration
 */
CONST(uint32, COM_CONST) Com_LcfgSignature = COM_LCFG_SIGNATURE;

/* stop data section declaration */
#define COM_STOP_SEC_CONST_32
#include <MemMap.h>

/** @} doxygen end group definition */
/*==================[end of file]===========================================*/
<%
if (comGenerationSuccessful == true)
{
    context.gen.addMessage( GeneratorOperationStatus.GENERATED_FILE( "Com_Lcfg.c" ) );
}
else {
    return "";
}
%>
