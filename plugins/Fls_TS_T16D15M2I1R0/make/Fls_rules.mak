# \file
#
# \brief AUTOSAR Fls
#
# This file contains the implementation of the AUTOSAR
# module Fls.
#
# \author Elektrobit Automotive GmbH, 91058 Erlangen, Germany
#
# Copyright 2005 - 2013 Elektrobit Automotive GmbH
# All rights exclusively reserved for Elektrobit Automotive GmbH,
# unless expressly agreed to otherwise.

#################################################################
# REGISTRY

LIBRARIES_TO_BUILD     += Fls_src

Fls_src_FILES       += $(Fls_CORE_PATH)\src\Fls_17_Pmu.c
Fls_src_FILES       += $(Fls_CORE_PATH)\src\Fls_17_Pmu_ac.c
Fls_src_FILES       += $(Fls_CORE_PATH)\src\Fls_17_Pmu_Ver.c
Fls_src_FILES       += $(Fls_CORE_PATH)\src\Fls_17_Pmu_Irq.c
Fls_src_FILES       += $(Fls_OUTPUT_PATH)\src\Fls_17_Pmu_PBCfg.c

ifeq ($(TS_BUILD_POST_BUILD_BINARY),TRUE)
# If the post build binary shall be built do not compile any files other then the postbuild files.
Fls_src_FILES :=
endif
