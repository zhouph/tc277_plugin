#if !defined( FLS_H )
#define FLS_H

#include <Fls_17_Pmu.h> 

#define FLS_AR_RELEASE_MAJOR_VERSION FLS_17_PMU_AR_RELEASE_MAJOR_VERSION
#define FLS_AR_RELEASE_MINOR_VERSION FLS_17_PMU_AR_RELEASE_MINOR_VERSION

/* Global Type Definitions */
typedef Fls_17_Pmu_ConfigType                Fls_ConfigType;

/* Global Function Declarations */
#define Fls_Init                             Fls_17_Pmu_Init
#define Fls_GetVersionInfo                   Fls_17_Pmu_GetVersionInfo
#define Fls_Erase                            Fls_17_Pmu_Erase
#define Fls_Write                            Fls_17_Pmu_Write
#define Fls_Cancel                           Fls_17_Pmu_Cancel
#define Fls_GetStatus                        Fls_17_Pmu_GetStatus
#define Fls_GetJobResult                     Fls_17_Pmu_GetJobResult
#define Fls_Read                             Fls_17_Pmu_Read
#define Fls_Compare                          Fls_17_Pmu_Compare
#define Fls_SetMode                          Fls_17_Pmu_SetMode
#define Fls_MainFunction                     Fls_17_Pmu_MainFunction

#endif