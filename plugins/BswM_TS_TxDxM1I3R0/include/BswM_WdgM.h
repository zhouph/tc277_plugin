/**
 * \file
 *
 * \brief AUTOSAR BswM
 *
 * This file contains the implementation of the AUTOSAR
 * module BswM.
 *
 * \author Elektrobit Automotive GmbH, 91058 Erlangen, Germany
 *
 * Copyright 2005 - 2013 Elektrobit Automotive GmbH
 * All rights exclusively reserved for Elektrobit Automotive GmbH,
 * unless expressly agreed to otherwise.
 */
#if (!defined BSWM_WDGM_H)
#define BSWM_WDGM_H

/* !LINKSTO     BswM0026,1
 * !description Definition of the BswM_WdgM.h for the WdgM.
 */

#include <BswM_Cfg.h>   /* Required for macro resolution */

#if(BSWM_WDGM_API_ENABLED == STD_ON)

/*==================[includes]===============================================*/

#include <BswM.h>             /* Module public API         */
#include <Os.h>               /* WdgM API                  */

/*==================[macros]=================================================*/

/*==================[type definitions]=======================================*/

/*==================[external function declarations]=========================*/

#define BSWM_START_SEC_CODE
#include <MemMap.h>


/** \brief Requests a partition reset (Called by WdgM).
 **
 ** This function is called by WdgM to request a partition reset .
 **
 ** Precondition: None
 **
 ** \param[in]    Application
 ** \param[inout] None.
 ** \param[out]   None.
 **
 ** \ServiceID{0x11}
 ** \Reentrancy{Reentrant}
 ** \Synchronicity{Synchronous} */
extern FUNC(void, BSWM_CODE) BswM_WdgM_RequestPartitionReset
(
  ApplicationType Application
);


#define BSWM_STOP_SEC_CODE
#include <MemMap.h>


/*==================[internal function declarations]=========================*/

/*==================[external constants]=====================================*/

/*==================[internal constants]=====================================*/

/*==================[external data]==========================================*/

/*==================[internal data]==========================================*/

/*==================[external function definitions]==========================*/

/*==================[internal function definitions]==========================*/
#endif /* (BSWM_WDGM_API_ENABLED == STD_ON) */

#endif /* if !defined( BSWM_WDGM_H ) */
/*==================[end of file]============================================*/
