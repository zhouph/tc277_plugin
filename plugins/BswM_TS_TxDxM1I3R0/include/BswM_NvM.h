/**
 * \file
 *
 * \brief AUTOSAR BswM
 *
 * This file contains the implementation of the AUTOSAR
 * module BswM.
 *
 * \author Elektrobit Automotive GmbH, 91058 Erlangen, Germany
 *
 * Copyright 2005 - 2013 Elektrobit Automotive GmbH
 * All rights exclusively reserved for Elektrobit Automotive GmbH,
 * unless expressly agreed to otherwise.
 */
#if (!defined BSWM_NVM_H)
#define BSWM_NVM_H

/* !LINKSTO     BswM0026,1
 * !description Definition of the BswM_NvM.h for the NvM.
 */

#include <BswM_Cfg.h>   /* Required for macro resolution */

#if (BSWM_NVM_API_ENABLED == STD_ON)
/*==================[includes]===============================================*/

#include <BswM.h>        /* Module public API         */
#include <NvM_Types.h>   /* NvM API                   */
#include <BswM_Int.h>    /* Module internal API       */

/*==================[macros]=================================================*/

/* Magic numbers not exported by NvM */
#define BSWM_NVM_READ_ALL_API_ID                   0x0CU
#define BSWM_NVM_WRITE_ALL_API_ID                  0x0DU

/*==================[type definitions]=======================================*/

/*==================[external function declarations]=========================*/

#define BSWM_START_SEC_CODE
#include <MemMap.h>

/** \brief Indicates the current block mode of an NvM block (Called by NvM).
 **
 ** This function is called by NvM to indicate the current block mode of an NvM block.
 **
 ** Precondition: None
 **
 ** \param[in]    Block            - The Block that the new NvM Mode corresponds to.
 **               CurrentBlockMode - The current block mode of the NvM block.
 ** \param[inout] None.
 ** \param[out]   None.
 **
 ** \ServiceID{0x16}
 ** \Reentrancy{Reentrant}
 ** \Synchronicity{Synchronous} */
extern FUNC(void, BSWM_CODE) BswM_NvM_CurrentBlockMode
(
  NvM_BlockIdType Block,
  NvM_Mode CurrentBlockMode
);
/** \brief Indicates the current state of a multi block job (Called by NvM).
 **
 ** This function is called by NvM to inform the BswM about the current state
 ** of a multi block job
 **
 ** Precondition: None
 **
 ** \param[in]    ServiceId        - Indicates whether the callback refers to
 **                                  multi block services NvM_ReadAll or NvM_WriteAll.
 **               CurrentJobMode   - Current state of the multi block job indicated
 **                                  by parameter ServiceId.
 ** \param[inout] None.
 ** \param[out]   None.
 **
 ** \ServiceID{0x17}
 ** \Reentrancy{Reentrant}
 ** \Synchronicity{Synchronous} */
extern FUNC(void, BSWM_CODE) BswM_NvM_CurrentJobMode
(
uint8 ServiceId,
NvM_RequestResultType CurrentJobMode
);

#define BSWM_STOP_SEC_CODE
#include <MemMap.h>

/*==================[internal function declarations]=========================*/

/*==================[external constants]=====================================*/

/*==================[internal constants]=====================================*/

/*==================[external data]==========================================*/

/*==================[internal data]==========================================*/

/*==================[external function definitions]==========================*/

/*==================[internal function definitions]==========================*/
#endif /* (BSWM_NVM_API_ENABLED == STD_ON) */

#endif /* if !defined( BSWM_NVM_H ) */
/*==================[end of file]============================================*/
