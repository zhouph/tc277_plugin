/**
 * \file
 *
 * \brief AUTOSAR BswM
 *
 * This file contains the implementation of the AUTOSAR
 * module BswM.
 *
 * \author Elektrobit Automotive GmbH, 91058 Erlangen, Germany
 *
 * Copyright 2005 - 2013 Elektrobit Automotive GmbH
 * All rights exclusively reserved for Elektrobit Automotive GmbH,
 * unless expressly agreed to otherwise.
 */
#if (!defined BSWM_SD_H)
#define BSWM_SD_H

#include <BswM_Cfg.h>    /* Needed for macro resolution */

#if(BSWM_SD_API_ENABLED == STD_ON)
/*==================[includes]===============================================*/

#include <BswM.h>   /* Module public API         */
#include <Sd.h>     /* Sd API                    */

/*==================[macros]=================================================*/

/*==================[type definitions]=======================================*/

/*==================[external function declarations]=========================*/

#define BSWM_START_SEC_CODE
#include <MemMap.h>

/** \brief Function called by Service Discovery to indicate current state of
 **        the Client Service (available/down).
 **
 ** This function is called by Sd to indicate the current state of the
 ** client service with SdClientServiceHandleId.
 **
 ** Precondition: None
 **
 ** \param[in] SdClientServiceHandleId - HandleId to identify the ClientService.
 ** \param[in] CurrentClientState      - Current state of the ClientService.
 ** \param[inout] None.
 ** \param[out]   None.
 **
 ** \ServiceID{0x1F}
 ** \Reentrancy{Reentrant}
 ** \Synchronicity{Asynchronous} */
extern FUNC(void, BSWM_CODE) BswM_Sd_ClientServiceCurrentState
(
  uint16 SdClientServiceHandleId,
  Sd_ClientServiceCurrentStateType CurrentClientState
);

/** \brief Function called by Service Discovery to indicate current status of
 **        the Consumed Eventgroup (available/down).
 **
 ** This function is called by Sd to indicate the current state of the
 ** consumed event group service with SdConsumedEventGroupHandleId.
 **
 ** Precondition: None
 **
 ** \param[in] SdConsumedEventGroupHandleId - HandleId to identify the ConsumedEventgroup.
 ** \param[in] ConsumedEventGroupState      - Status of the Consumed Eventgroup.
 ** \param[inout] None.
 ** \param[out]   None.
 **
 ** \ServiceID{0x21}
 ** \Reentrancy{Reentrant}
 ** \Synchronicity{Asynchronous} */
extern FUNC(void, BSWM_CODE) BswM_Sd_ConsumedEventGroupCurrentState
(
  uint16 SdConsumedEventGroupHandleId,
  Sd_ConsumedEventGroupCurrentStateType ConsumedEventGroupState
);


/** \brief Function called by Service Discovery to indicate current status of
 **        the EventHandler (requested/released)
 **
 ** This function is called by Sd to indicate the current state of the
 ** event handler service with SdEventHandlerHandleId.
 **
 ** Precondition: None
 **
 ** \param[in] SdEventHandlerHandleId - HandleId to identify the EventHandler.
 ** \param[in] EventHandlerStatus     - Status of the EventHandler.
 ** \param[inout] None.
 ** \param[out]   None.
 **
 ** \ServiceID{0x20}
 ** \Reentrancy{Reentrant}
 ** \Synchronicity{Asynchronous} */
extern FUNC(void, BSWM_CODE) BswM_Sd_EventHandlerCurrentState
(
  uint16 SdEventHandlerHandleId,
  Sd_EventHandlerCurrentStateType EventHandlerStatus
);

#define BSWM_STOP_SEC_CODE
#include <MemMap.h>

/*==================[internal function declarations]=========================*/

/*==================[external constants]=====================================*/

/*==================[internal constants]=====================================*/

/*==================[external data]==========================================*/

/*==================[internal data]==========================================*/

/*==================[external function definitions]==========================*/

/*==================[internal function definitions]==========================*/
#endif /* (BSWM_SD_API_ENABLED == STD_ON) */

#endif /* if !defined( BSWM_SD_H ) */
/*==================[end of file]============================================*/
