/**
 * \file
 *
 * \brief AUTOSAR BswM
 *
 * This file contains the implementation of the AUTOSAR
 * module BswM.
 *
 * \author Elektrobit Automotive GmbH, 91058 Erlangen, Germany
 *
 * Copyright 2005 - 2013 Elektrobit Automotive GmbH
 * All rights exclusively reserved for Elektrobit Automotive GmbH,
 * unless expressly agreed to otherwise.
 */
#if (!defined BSWM_USER_CALLOUT_H)
#define BSWM_USER_CALLOUT_H

/* !LINKSTO     BswM0026,1
 * !description Definition of the User_Callout.h for other BSW modules.
 */

#endif /* if !defined( BSWM_USER_CALLOUT_H ) */
/*==================[end of file]============================================*/
