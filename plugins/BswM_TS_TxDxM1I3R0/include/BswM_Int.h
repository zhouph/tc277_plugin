/**
 * \file
 *
 * \brief AUTOSAR BswM
 *
 * This file contains the implementation of the AUTOSAR
 * module BswM.
 *
 * \author Elektrobit Automotive GmbH, 91058 Erlangen, Germany
 *
 * Copyright 2005 - 2013 Elektrobit Automotive GmbH
 * All rights exclusively reserved for Elektrobit Automotive GmbH,
 * unless expressly agreed to otherwise.
 */
#if (!defined BSWM_INT_H)
#define BSWM_INT_H

/*==================[inclusions]============================================*/

#include <BswM_Cfg.h>            /* BswM configuration header */

/*==================[type definitions]=======================================*/

/** \brief Type for Nvm Mode as this type is undefined in Nvm module **/
typedef uint8 NvM_Mode;

/*==================[macros]=================================================*/

#if (BSWM_DEV_ERROR_DETECT == STD_ON)
#if (defined BSWM_DET_REPORT_ERROR)
#error BSWM_DET_REPORT_ERROR already defined
#endif /* if (defined BSWM_DET_REPORT_ERROR) */

/** \brief Report BswM development error */
#define BSWM_DET_REPORT_ERROR(ApiId,ErrorId)                            \
        ((void)Det_ReportError(BSWM_MODULE_ID, BSWM_INSTANCE_ID, (ApiId), (ErrorId)))

#endif /* if (BSWM_DEV_ERROR_DETECT == STD_ON) */

#if (defined BSWM_NVM_MODE_UNDEFINED)
#error BSWM_NVM_MODE_UNDEFINED already defined
#endif
/** \brief Nvm Current block mode.
 **
 ** Definition of value for CurrentBlockMode */
#define BSWM_NVM_MODE_UNDEFINED       0x06  /* TO BE REMOVED */

#if (defined BSWM_NETWORK_HANDLE_MAX)
#error BSWM_NETWORK_HANDLE_MAX already defined
#endif
/** \brief Network handle Id
 **
 ** Definition of Network handle Id. */
#define BSWM_NETWORK_HANDLE_MAX        255U

#if (defined BSWM_E_PARAM_INVALID)
#error BSWM_E_PARAM_INVALID already defined
#endif
/** \brief Invalid parameter error
 **
 ** DET error for invalid parameter. */
#define BSWM_E_PARAM_INVALID 128U

#if (defined BSWM_INSTANCE_ID)
#error BSWM_INSTANCE_ID already defined
#endif
/** \brief BswM Instance ID
 **
 ** Instance ID of the BSWM for DET errors (there only is one BswM) */
#define BSWM_INSTANCE_ID 128U


/*==================[internal function declarations]=========================*/

/*==================[external constants]=====================================*/

/*==================[internal constants]=====================================*/

/*==================[external data]==========================================*/

/*==================[internal data]==========================================*/

/*==================[external function definitions]==========================*/

/*==================[internal function definitions]==========================*/

#endif /* if !defined( BSWM_INT_H ) */
/*==================[end of file]============================================*/


