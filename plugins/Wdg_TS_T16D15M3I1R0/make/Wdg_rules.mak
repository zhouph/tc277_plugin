# \file
#
# \brief AUTOSAR Wdg
#
# This file contains the implementation of the AUTOSAR
# module Wdg.
#
# \author Elektrobit Automotive GmbH, 91058 Erlangen, Germany
#
# Copyright 2005 - 2013 Elektrobit Automotive GmbH
# All rights exclusively reserved for Elektrobit Automotive GmbH,
# unless expressly agreed to otherwise.

#################################################################
# REGISTRY

LIBRARIES_TO_BUILD     += Wdg_src

Wdg_src_FILES       += $(Wdg_CORE_PATH)\src\Wdg_17_Scu.c
Wdg_src_FILES       += $(Wdg_CORE_PATH)\src\Wdg_17_Scu_Safe.c
Wdg_src_FILES       += $(Wdg_OUTPUT_PATH)\src\Wdg_17_Scu_PBCfg.c

ifeq ($(TS_BUILD_POST_BUILD_BINARY),TRUE)
# If the post build binary shall be built do not compile any files other then the postbuild files.
Wdg_src_FILES :=
endif
