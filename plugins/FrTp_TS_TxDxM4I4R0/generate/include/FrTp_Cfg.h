/**
 * \file
 *
 * \brief AUTOSAR FrTp
 *
 * This file contains the implementation of the AUTOSAR
 * module FrTp.
 *
 * \author Elektrobit Automotive GmbH, 91058 Erlangen, Germany
 *
 * Copyright 2005 - 2013 Elektrobit Automotive GmbH
 * All rights exclusively reserved for Elektrobit Automotive GmbH,
 * unless expressly agreed to otherwise.
 */

[!INCLUDE "FrTp_Variables.m"!]
[!CODE!]

#ifndef _FRTP_CFG_H_
#define _FRTP_CFG_H_

/******************************************************************************
**                      Include Section                                      **
******************************************************************************/

#include <Std_Types.h>  /* standard types from AUTOSAR */

/******************************************************************************
**                      Global Macros                                        **
******************************************************************************/

/* standard SWS pre-compile time configuration parameters */
#define FRTP_DEV_ERROR_DETECT [!IF "FrTpGeneral/FrTpDevErrorDetect = 'true'"!]STD_ON[!ELSE!]STD_OFF[!ENDIF!]
#define FRTP_VERSION_INFO_API [!IF "FrTpGeneral/FrTpVersionInfoApi = 'true'"!]STD_ON[!ELSE!]STD_OFF[!ENDIF!]
#define FRTP_PBCFGM_SUPPORT_ENABLED [!IF "node:contains(node:refs(as:modconf('PbcfgM')/PbcfgMBswModules/*/PbcfgMBswModuleRef), as:modconf('FrTp')) = 'true'"!]STD_ON[!ELSE!]STD_OFF[!ENDIF!]

#define FRTP_HAVE_ACKRT [!IF "FrTpGeneral/FrTpAckRt = 'true'"!]STD_ON[!ELSE!]STD_OFF[!ENDIF!]
#define FRTP_CHAN_NUM [!"num:integer(FrTpGeneral/FrTpChanNum)"!]U
#define FRTP_HAVE_TC [!IF "FrTpGeneral/FrTpTransmitCancellation = 'true'"!]STD_ON[!ELSE!]STD_OFF[!ENDIF!]
#define FRTP_UNKNOWN_MSG_LENGTH [!IF "FrTpGeneral/FrTpUnknownMsgLength = 'true'"!]STD_ON[!ELSE!]STD_OFF[!ENDIF!]

/* vendor specific pre-compile time configuration parameters */
#define FRTP_FULLDUPLEX_ENABLE [!IF "FrTpGeneral/FrTpFullDuplexEnable = 'true'"!]STD_ON[!ELSE!]STD_OFF[!ENDIF!]

#define FRTP_CON_NUM [!"$NumberOfConnections"!]U /* number of configured connections */
#define FRTP_TXPDU_NUM [!"FrTpGeneral/FrTpTxPduNum"!]U /* number of configured TX PDUs */

/* number FrTp_MainFunction() invocations per Flexray communication cycle */
#define FRTP_MAINFUNCTIONS_PER_FLEXRAYCYCLE [!"FrTpGeneral/VendorSpecific/FrTpMainfunctionsPerCommunicationCycle"!]U 

#define FRTP_CFG_SIGNATURE [!"asc:getConfigSignature(as:modconf('FrTp')[1]//*[not(child::*) and (node:configclass() = 'PreCompile') ])"!]U /* Compile time verification value */

#define FRTP_PUBLIC_INFO_SIGNATURE [!"asc:getConfigSignature(node:difference(as:modconf('FrTp')[1]/CommonPublishedInformation//*[not(child::*) and (node:configclass() = 'PublishedInformation') ], as:modconf('FrTp')[1]/CommonPublishedInformation/Release))"!]U /* PublicInfoSignature */

/* ----- Pre-processor switch to enable/diable relocateable postbuild config ----- */
#if (FRTP_PBCFGM_SUPPORT_ENABLED == STD_ON)
#define FRTP_RELOCATABLE_CFG_ENABLE [!IF "as:modconf('PbcfgM')/PbcfgMGeneral/PbcfgMRelocatableCfgEnable = 'true'"!]STD_ON[!ELSE!]STD_OFF[!ENDIF!]
#else
#define FRTP_RELOCATABLE_CFG_ENABLE [!IF "FrTpGeneral/VendorSpecific/FrTpRelocatablePbcfgEnable = 'true'"!]STD_ON[!ELSE!]STD_OFF[!ENDIF!]
#endif

/* ----- Pre-processor switch to enable/disable copy of STF to local buffer ----- */
#define FRTP_COPY_STF_LOCALBUFFER [!IF "FrTpGeneral/VendorSpecific/FrTpCopyToLocalBuffer = 'true'"!]STD_ON[!ELSE!]STD_OFF[!ENDIF!]

/* ----- Precompile macro to specify the max size of the local buffer ----- */
#if (FRTP_COPY_STF_LOCALBUFFER == STD_ON)
#define FRTP_MAXSIZE_LOCALBUFFER  [!"FrTpGeneral/VendorSpecific/FrTpLocalBufferSize"!]U
#endif
/******************************** defensive programming ************************************/




/*------------------[Defensive programming]---------------------------------*/
[!SELECT "FrTpDefensiveProgramming"!][!//

#if (defined FRTP_DEFENSIVE_PROGRAMMING_ENABLED)
#error FRTP_DEFENSIVE_PROGRAMMING_ENABLED is already defined
#endif
/** \brief Defensive programming usage
 **
 ** En- or disables the usage of the defensive programming */
#define FRTP_DEFENSIVE_PROGRAMMING_ENABLED   [!//
[!IF "(../FrTpGeneral/FrTpDevErrorDetect  = 'true') and (FrTpDefProgEnabled = 'true')"!]STD_ON[!ELSE!]STD_OFF[!ENDIF!]

#if (defined FRTP_PRECONDITION_ASSERT_ENABLED)
#error FRTP_PRECONDITION_ASSERT_ENABLED is already defined
#endif
/** \brief Precondition assertion usage
 **
 ** En- or disables the usage of precondition assertion checks */
#define FRTP_PRECONDITION_ASSERT_ENABLED     [!//
[!IF "(../FrTpGeneral/FrTpDevErrorDetect  = 'true') and (FrTpDefProgEnabled = 'true') and (FrTpPrecondAssertEnabled = 'true')"!]STD_ON[!ELSE!]STD_OFF[!ENDIF!]

#if (defined FRTP_POSTCONDITION_ASSERT_ENABLED)
#error FRTP_POSTCONDITION_ASSERT_ENABLED is already defined
#endif
/** \brief Postcondition assertion usage
 **
 ** En- or disables the usage of postcondition assertion checks */
#define FRTP_POSTCONDITION_ASSERT_ENABLED    [!//
[!IF "(../FrTpGeneral/FrTpDevErrorDetect  = 'true') and (FrTpDefProgEnabled = 'true') and (FrTpPostcondAssertEnabled = 'true')"!]STD_ON[!ELSE!]STD_OFF[!ENDIF!]

#if (defined FRTP_UNREACHABLE_CODE_ASSERT_ENABLED)
#error FRTP_UNREACHABLE_CODE_ASSERT_ENABLED is already defined
#endif
/** \brief Unreachable code assertion usage
 **
 ** En- or disables the usage of unreachable code assertion checks */
#define FRTP_UNREACHABLE_CODE_ASSERT_ENABLED [!//
[!IF "(../FrTpGeneral/FrTpDevErrorDetect  = 'true') and (FrTpDefProgEnabled = 'true') and (FrTpUnreachAssertEnabled = 'true')"!]STD_ON[!ELSE!]STD_OFF[!ENDIF!]

#if (defined FRTP_INVARIANT_ASSERT_ENABLED)
#error FRTP_INVARIANT_ASSERT_ENABLED is already defined
#endif
/** \brief Invariant assertion usage
 **
 ** En- or disables the usage of invariant assertion checks */
#define FRTP_INVARIANT_ASSERT_ENABLED        [!//
[!IF "(../FrTpGeneral/FrTpDevErrorDetect  = 'true') and (FrTpDefProgEnabled = 'true') and (FrTpInvariantAssertEnabled = 'true')"!]STD_ON[!ELSE!]STD_OFF[!ENDIF!]

#if (defined FRTP_STATIC_ASSERT_ENABLED)
#error FRTP_STATIC_ASSERT_ENABLED is already defined
#endif
/** \brief Static assertion usage
 **
 ** En- or disables the usage of static assertion checks */
#define FRTP_STATIC_ASSERT_ENABLED           [!//
[!IF "(../FrTpGeneral/FrTpDevErrorDetect  = 'true') and (FrTpDefProgEnabled = 'true') and (FrTpStaticAssertEnabled = 'true')"!]STD_ON[!ELSE!]STD_OFF[!ENDIF!]

[!ENDSELECT!][!//


#endif /* _FRTP_CFG_H_ */
[!ENDCODE!][!//
