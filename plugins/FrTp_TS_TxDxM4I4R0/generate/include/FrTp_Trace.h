/**
 * \file
 *
 * \brief AUTOSAR FrTp
 *
 * This file contains the implementation of the AUTOSAR
 * module FrTp.
 *
 * \author Elektrobit Automotive GmbH, 91058 Erlangen, Germany
 *
 * Copyright 2005 - 2013 Elektrobit Automotive GmbH
 * All rights exclusively reserved for Elektrobit Automotive GmbH,
 * unless expressly agreed to otherwise.
 */
#if (!defined FRTP_TRACE_H)
#define FRTP_TRACE_H
/*==================[inclusions]============================================*/

[!IF "node:exists(as:modconf('Dbg'))"!]
#include <Dbg.h>
[!ENDIF!]

/*==================[macros]================================================*/

#ifndef DBG_FRTP_HANDLETIMERS_ENTRY
/** \brief Entry point of function FrTp_HandleTimers() */
#define DBG_FRTP_HANDLETIMERS_ENTRY(a)
#endif

#ifndef DBG_FRTP_HANDLETIMERS_EXIT
/** \brief Exit point of function FrTp_HandleTimers() */
#define DBG_FRTP_HANDLETIMERS_EXIT(a)
#endif

#ifndef DBG_FRTP_HANDLETXTIMER1_ENTRY
/** \brief Entry point of function FrTp_HandleTxTimer1() */
#define DBG_FRTP_HANDLETXTIMER1_ENTRY(a)
#endif

#ifndef DBG_FRTP_HANDLETXTIMER1_EXIT
/** \brief Exit point of function FrTp_HandleTxTimer1() */
#define DBG_FRTP_HANDLETXTIMER1_EXIT(a)
#endif

#ifndef DBG_FRTP_HANDLETXTIMER2_ENTRY
/** \brief Entry point of function FrTp_HandleTxTimer2() */
#define DBG_FRTP_HANDLETXTIMER2_ENTRY(a)
#endif

#ifndef DBG_FRTP_HANDLETXTIMER2_EXIT
/** \brief Exit point of function FrTp_HandleTxTimer2() */
#define DBG_FRTP_HANDLETXTIMER2_EXIT(a)
#endif

#ifndef DBG_FRTP_HANDLERXTIMER1_ENTRY
/** \brief Entry point of function FrTp_HandleRxTimer1() */
#define DBG_FRTP_HANDLERXTIMER1_ENTRY(a)
#endif

#ifndef DBG_FRTP_HANDLERXTIMER1_EXIT
/** \brief Exit point of function FrTp_HandleRxTimer1() */
#define DBG_FRTP_HANDLERXTIMER1_EXIT(a)
#endif

#ifndef DBG_FRTP_HANDLERXTIMER2_ENTRY
/** \brief Entry point of function FrTp_HandleRxTimer2() */
#define DBG_FRTP_HANDLERXTIMER2_ENTRY(a)
#endif

#ifndef DBG_FRTP_HANDLERXTIMER2_EXIT
/** \brief Exit point of function FrTp_HandleRxTimer2() */
#define DBG_FRTP_HANDLERXTIMER2_EXIT(a)
#endif

#ifndef DBG_FRTP_HANDLERXTIMER3_ENTRY
/** \brief Entry point of function FrTp_HandleRxTimer3() */
#define DBG_FRTP_HANDLERXTIMER3_ENTRY(a)
#endif

#ifndef DBG_FRTP_HANDLERXTIMER3_EXIT
/** \brief Exit point of function FrTp_HandleRxTimer3() */
#define DBG_FRTP_HANDLERXTIMER3_EXIT(a)
#endif

#ifndef DBG_FRTP_FINDTXCHANNEL_ENTRY
/** \brief Entry point of function FrTp_FindTxChannel() */
#define DBG_FRTP_FINDTXCHANNEL_ENTRY(a)
#endif

#ifndef DBG_FRTP_FINDTXCHANNEL_EXIT
/** \brief Exit point of function FrTp_FindTxChannel() */
#define DBG_FRTP_FINDTXCHANNEL_EXIT(a,b)
#endif

#ifndef DBG_FRTP_RESETCHANNEL_ENTRY
/** \brief Entry point of function FrTp_ResetChannel() */
#define DBG_FRTP_RESETCHANNEL_ENTRY(a)
#endif

#ifndef DBG_FRTP_RESETCHANNEL_EXIT
/** \brief Exit point of function FrTp_ResetChannel() */
#define DBG_FRTP_RESETCHANNEL_EXIT(a)
#endif

#ifndef DBG_FRTP_ISVALIDCONFIG_ENTRY
/** \brief Entry point of function FrTp_IsValidConfig() */
#define DBG_FRTP_ISVALIDCONFIG_ENTRY(a)
#endif

#ifndef DBG_FRTP_ISVALIDCONFIG_EXIT
/** \brief Exit point of function FrTp_IsValidConfig() */
#define DBG_FRTP_ISVALIDCONFIG_EXIT(a,b)
#endif

#ifndef DBG_FRTP_INIT_ENTRY
/** \brief Entry point of function FrTp_Init() */
#define DBG_FRTP_INIT_ENTRY(a)
#endif

#ifndef DBG_FRTP_INIT_EXIT
/** \brief Exit point of function FrTp_Init() */
#define DBG_FRTP_INIT_EXIT(a)
#endif

#ifndef DBG_FRTP_TRANSMIT_ENTRY
/** \brief Entry point of function FrTp_Transmit() */
#define DBG_FRTP_TRANSMIT_ENTRY(a,b)
#endif

#ifndef DBG_FRTP_TRANSMIT_EXIT
/** \brief Exit point of function FrTp_Transmit() */
#define DBG_FRTP_TRANSMIT_EXIT(a,b,c)
#endif

#ifndef DBG_FRTP_CANCELTRANSMIT_ENTRY
/** \brief Entry point of function FrTp_CancelTransmit() */
#define DBG_FRTP_CANCELTRANSMIT_ENTRY(a)
#endif

#ifndef DBG_FRTP_CANCELTRANSMIT_EXIT
/** \brief Exit point of function FrTp_CancelTransmit() */
#define DBG_FRTP_CANCELTRANSMIT_EXIT(a,b)
#endif

#ifndef DBG_FRTP_CANCELRECEIVE_ENTRY
/** \brief Entry point of function FrTp_CancelReceive() */
#define DBG_FRTP_CANCELRECEIVE_ENTRY(a)
#endif

#ifndef DBG_FRTP_CANCELRECEIVE_EXIT
/** \brief Exit point of function FrTp_CancelReceive() */
#define DBG_FRTP_CANCELRECEIVE_EXIT(a,b)
#endif

#ifndef DBG_FRTP_CHANGEPARAMETER_ENTRY
/** \brief Entry point of function FrTp_ChangeParameter() */
#define DBG_FRTP_CHANGEPARAMETER_ENTRY(a,b,c)
#endif

#ifndef DBG_FRTP_CHANGEPARAMETER_EXIT
/** \brief Exit point of function FrTp_ChangeParameter() */
#define DBG_FRTP_CHANGEPARAMETER_EXIT(a,b,c,d)
#endif

#ifndef DBG_FRTP_MAINFUNCTION_ENTRY
/** \brief Entry point of function FrTp_MainFunction() */
#define DBG_FRTP_MAINFUNCTION_ENTRY()
#endif

#ifndef DBG_FRTP_MAINFUNCTION_EXIT
/** \brief Exit point of function FrTp_MainFunction() */
#define DBG_FRTP_MAINFUNCTION_EXIT()
#endif

#ifndef DBG_FRTP_GETFREETXPDU_ENTRY
/** \brief Entry point of function FrTp_GetFreeTxPdu() */
#define DBG_FRTP_GETFREETXPDU_ENTRY(a,b)
#endif

#ifndef DBG_FRTP_GETFREETXPDU_EXIT
/** \brief Exit point of function FrTp_GetFreeTxPdu() */
#define DBG_FRTP_GETFREETXPDU_EXIT(a,b,c)
#endif

#ifndef DBG_FRTP_GETVERSIONINFO_ENTRY
/** \brief Entry point of function FrTp_GetVersionInfo() */
#define DBG_FRTP_GETVERSIONINFO_ENTRY(a)
#endif

#ifndef DBG_FRTP_GETVERSIONINFO_EXIT
/** \brief Exit point of function FrTp_GetVersionInfo() */
#define DBG_FRTP_GETVERSIONINFO_EXIT(a)
#endif

#ifndef DBG_FRTP_FINDRXCHANNEL_ENTRY
/** \brief Entry point of function FrTp_FindRxChannel() */
#define DBG_FRTP_FINDRXCHANNEL_ENTRY(a,b)
#endif

#ifndef DBG_FRTP_FINDRXCHANNEL_EXIT
/** \brief Exit point of function FrTp_FindRxChannel() */
#define DBG_FRTP_FINDRXCHANNEL_EXIT(a,b,c)
#endif

#ifndef DBG_FRTP_FINDCONNECTION_ENTRY
/** \brief Entry point of function FrTp_FindConnection() */
#define DBG_FRTP_FINDCONNECTION_ENTRY(a,b)
#endif

#ifndef DBG_FRTP_FINDCONNECTION_EXIT
/** \brief Exit point of function FrTp_FindConnection() */
#define DBG_FRTP_FINDCONNECTION_EXIT(a,b,c)
#endif

#ifndef DBG_FRTP_PROCESSRXFRAME_ENTRY
/** \brief Entry point of function FrTp_ProcessRxFrame() */
#define DBG_FRTP_PROCESSRXFRAME_ENTRY(a,b,c)
#endif

#ifndef DBG_FRTP_PROCESSRXFRAME_EXIT
/** \brief Exit point of function FrTp_ProcessRxFrame() */
#define DBG_FRTP_PROCESSRXFRAME_EXIT(a,b,c)
#endif

#ifndef DBG_FRTP_TRIGGERTRANSMIT_ENTRY
/** \brief Entry point of function FrTp_TriggerTransmit() */
#define DBG_FRTP_TRIGGERTRANSMIT_ENTRY(a,b)
#endif

#ifndef DBG_FRTP_TRIGGERTRANSMIT_EXIT
/** \brief Exit point of function FrTp_TriggerTransmit() */
#define DBG_FRTP_TRIGGERTRANSMIT_EXIT(a,b,c)
#endif

#ifndef DBG_FRTP_RXINDICATION_ENTRY
/** \brief Entry point of function FrTp_RxIndication() */
#define DBG_FRTP_RXINDICATION_ENTRY(a,b)
#endif

#ifndef DBG_FRTP_RXINDICATION_EXIT
/** \brief Exit point of function FrTp_RxIndication() */
#define DBG_FRTP_RXINDICATION_EXIT(a,b)
#endif

#ifndef DBG_FRTP_TXCONFIRMATION_ENTRY
/** \brief Entry point of function FrTp_TxConfirmation() */
#define DBG_FRTP_TXCONFIRMATION_ENTRY(a)
#endif

#ifndef DBG_FRTP_TXCONFIRMATION_EXIT
/** \brief Exit point of function FrTp_TxConfirmation() */
#define DBG_FRTP_TXCONFIRMATION_EXIT(a)
#endif

#ifndef DBG_FRTP_RXSM_TRIGGERTRANSMIT_ENTRY
/** \brief Entry point of function FrTp_RxSm_TriggerTransmit() */
#define DBG_FRTP_RXSM_TRIGGERTRANSMIT_ENTRY(a,b)
#endif

#ifndef DBG_FRTP_RXSM_TRIGGERTRANSMIT_EXIT
/** \brief Exit point of function FrTp_RxSm_TriggerTransmit() */
#define DBG_FRTP_RXSM_TRIGGERTRANSMIT_EXIT(a,b,c)
#endif

#ifndef DBG_FRTP_RXSM_TRANSMITCONFIRMATION_ENTRY
/** \brief Entry point of function FrTp_RxSm_TransmitConfirmation() */
#define DBG_FRTP_RXSM_TRANSMITCONFIRMATION_ENTRY(a,b)
#endif

#ifndef DBG_FRTP_RXSM_TRANSMITCONFIRMATION_EXIT
/** \brief Exit point of function FrTp_RxSm_TransmitConfirmation() */
#define DBG_FRTP_RXSM_TRANSMITCONFIRMATION_EXIT(a,b)
#endif

#ifndef DBG_FRTP_RXSM_TRANSMITSYNCPOINT_ENTRY
/** \brief Entry point of function FrTp_RxSm_TransmitSyncPoint() */
#define DBG_FRTP_RXSM_TRANSMITSYNCPOINT_ENTRY(a)
#endif

#ifndef DBG_FRTP_RXSM_TRANSMITSYNCPOINT_EXIT
/** \brief Exit point of function FrTp_RxSm_TransmitSyncPoint() */
#define DBG_FRTP_RXSM_TRANSMITSYNCPOINT_EXIT(a)
#endif

#ifndef DBG_FRTP_RXSM_INDICATEANDABORT_ENTRY
/** \brief Entry point of function FrTp_RxSm_IndicateAndAbort() */
#define DBG_FRTP_RXSM_INDICATEANDABORT_ENTRY(a,b)
#endif

#ifndef DBG_FRTP_RXSM_INDICATEANDABORT_EXIT
/** \brief Exit point of function FrTp_RxSm_IndicateAndAbort() */
#define DBG_FRTP_RXSM_INDICATEANDABORT_EXIT(a,b)
#endif

#ifndef DBG_FRTP_RXSM_RXDATA_HANDLESTARTFRAME_ENTRY
/** \brief Entry point of function FrTp_RxSm_RxData_HandleStartFrame() */
#define DBG_FRTP_RXSM_RXDATA_HANDLESTARTFRAME_ENTRY(a,b,c)
#endif

#ifndef DBG_FRTP_RXSM_RXDATA_HANDLESTARTFRAME_EXIT
/** \brief Exit point of function FrTp_RxSm_RxData_HandleStartFrame() */
#define DBG_FRTP_RXSM_RXDATA_HANDLESTARTFRAME_EXIT(a,b,c)
#endif

#ifndef DBG_FRTP_RXSM_RXDATA_HANDLESFBUFOK_ENTRY
/** \brief Entry point of function FrTp_RxSm_RxData_HandleSFBufOk() */
#define DBG_FRTP_RXSM_RXDATA_HANDLESFBUFOK_ENTRY(a,b,c)
#endif

#ifndef DBG_FRTP_RXSM_RXDATA_HANDLESFBUFOK_EXIT
/** \brief Exit point of function FrTp_RxSm_RxData_HandleSFBufOk() */
#define DBG_FRTP_RXSM_RXDATA_HANDLESFBUFOK_EXIT(a,b,c)
#endif

#ifndef DBG_FRTP_RXSM_RXDATA_HANDLESFBUFBUSY_ENTRY
/** \brief Entry point of function FrTp_RxSm_RxData_HandleSFBufBusy() */
#define DBG_FRTP_RXSM_RXDATA_HANDLESFBUFBUSY_ENTRY(a,b,c)
#endif

#ifndef DBG_FRTP_RXSM_RXDATA_HANDLESFBUFBUSY_EXIT
/** \brief Exit point of function FrTp_RxSm_RxData_HandleSFBufBusy() */
#define DBG_FRTP_RXSM_RXDATA_HANDLESFBUFBUSY_EXIT(a,b,c)
#endif

#ifndef DBG_FRTP_RXSM_RXDATA_HANDLESFBUFNOK_ENTRY
/** \brief Entry point of function FrTp_RxSm_RxData_HandleSFBufNOk() */
#define DBG_FRTP_RXSM_RXDATA_HANDLESFBUFNOK_ENTRY(a,b,c,d,e)
#endif

#ifndef DBG_FRTP_RXSM_RXDATA_HANDLESFBUFNOK_EXIT
/** \brief Exit point of function FrTp_RxSm_RxData_HandleSFBufNOk() */
#define DBG_FRTP_RXSM_RXDATA_HANDLESFBUFNOK_EXIT(a,b,c,d,e)
#endif

#ifndef DBG_FRTP_SENDFC_CONTINUE_ENTRY
/** \brief Entry point of function FrTp_SendFc_Continue() */
#define DBG_FRTP_SENDFC_CONTINUE_ENTRY(a,b,c)
#endif

#ifndef DBG_FRTP_SENDFC_CONTINUE_EXIT
/** \brief Exit point of function FrTp_SendFc_Continue() */
#define DBG_FRTP_SENDFC_CONTINUE_EXIT(a,b,c)
#endif

#ifndef DBG_FRTP_SENDFC_RETRY_ENTRY
/** \brief Entry point of function FrTp_SendFc_Retry() */
#define DBG_FRTP_SENDFC_RETRY_ENTRY(a,b,c,d)
#endif

#ifndef DBG_FRTP_SENDFC_RETRY_EXIT
/** \brief Exit point of function FrTp_SendFc_Retry() */
#define DBG_FRTP_SENDFC_RETRY_EXIT(a,b,c,d)
#endif

#ifndef DBG_FRTP_SENDFC_OVFLW_ENTRY
/** \brief Entry point of function FrTp_SendFc_Ovflw() */
#define DBG_FRTP_SENDFC_OVFLW_ENTRY(a,b,c,d)
#endif

#ifndef DBG_FRTP_SENDFC_OVFLW_EXIT
/** \brief Exit point of function FrTp_SendFc_Ovflw() */
#define DBG_FRTP_SENDFC_OVFLW_EXIT(a,b,c,d)
#endif

#ifndef DBG_FRTP_SENDFC_ABORT_ENTRY
/** \brief Entry point of function FrTp_SendFc_Abort() */
#define DBG_FRTP_SENDFC_ABORT_ENTRY(a,b,c,d,e)
#endif

#ifndef DBG_FRTP_SENDFC_ABORT_EXIT
/** \brief Exit point of function FrTp_SendFc_Abort() */
#define DBG_FRTP_SENDFC_ABORT_EXIT(a,b,c,d,e)
#endif

#ifndef DBG_FRTP_COPYTOLOCALBUFFER_STF_ENTRY
/** \brief Entry point of function FrTp_CopyToLocalBuffer_STF() */
#define DBG_FRTP_COPYTOLOCALBUFFER_STF_ENTRY(a,b,c)
#endif

#ifndef DBG_FRTP_COPYTOLOCALBUFFER_STF_EXIT
/** \brief Exit point of function FrTp_CopyToLocalBuffer_STF() */
#define DBG_FRTP_COPYTOLOCALBUFFER_STF_EXIT(a,b,c)
#endif

#ifndef DBG_FRTP_RXSM_RXDATA_HANDLECONFRAME_ENTRY
/** \brief Entry point of function FrTp_RxSm_RxData_HandleConFrame() */
#define DBG_FRTP_RXSM_RXDATA_HANDLECONFRAME_ENTRY(a,b,c,d)
#endif

#ifndef DBG_FRTP_RXSM_RXDATA_HANDLECONFRAME_EXIT
/** \brief Exit point of function FrTp_RxSm_RxData_HandleConFrame() */
#define DBG_FRTP_RXSM_RXDATA_HANDLECONFRAME_EXIT(a,b,c,d)
#endif

#ifndef DBG_FRTP_RXSM_RXDATA_HANDLELASTFRAME_ENTRY
/** \brief Entry point of function FrTp_RxSm_RxData_HandleLastFrame() */
#define DBG_FRTP_RXSM_RXDATA_HANDLELASTFRAME_ENTRY(a,b)
#endif

#ifndef DBG_FRTP_RXSM_RXDATA_HANDLELASTFRAME_EXIT
/** \brief Exit point of function FrTp_RxSm_RxData_HandleLastFrame() */
#define DBG_FRTP_RXSM_RXDATA_HANDLELASTFRAME_EXIT(a,b)
#endif

#ifndef DBG_FRTP_RXSM_RXDATA_ENTRY
/** \brief Entry point of function FrTp_RxSm_RxData() */
#define DBG_FRTP_RXSM_RXDATA_ENTRY(a,b,c,d)
#endif

#ifndef DBG_FRTP_RXSM_RXDATA_EXIT
/** \brief Exit point of function FrTp_RxSm_RxData() */
#define DBG_FRTP_RXSM_RXDATA_EXIT(a,b,c,d)
#endif

#ifndef DBG_FRTP_RESETRXCHANNEL_ENTRY
/** \brief Entry point of function FrTp_ResetRxChannel() */
#define DBG_FRTP_RESETRXCHANNEL_ENTRY(a)
#endif

#ifndef DBG_FRTP_RESETRXCHANNEL_EXIT
/** \brief Exit point of function FrTp_ResetRxChannel() */
#define DBG_FRTP_RESETRXCHANNEL_EXIT(a)
#endif

#ifndef DBG_FRTP_TXSM_REQUESTTRANSMISSION_ENTRY
/** \brief Entry point of function FrTp_TxSm_RequestTransmission() */
#define DBG_FRTP_TXSM_REQUESTTRANSMISSION_ENTRY(a,b)
#endif

#ifndef DBG_FRTP_TXSM_REQUESTTRANSMISSION_EXIT
/** \brief Exit point of function FrTp_TxSm_RequestTransmission() */
#define DBG_FRTP_TXSM_REQUESTTRANSMISSION_EXIT(a,b,c)
#endif

#ifndef DBG_FRTP_TXSM_TRANSMITSYNCPOINT_ENTRY
/** \brief Entry point of function FrTp_TxSm_TransmitSyncPoint() */
#define DBG_FRTP_TXSM_TRANSMITSYNCPOINT_ENTRY(a,b)
#endif

#ifndef DBG_FRTP_TXSM_TRANSMITSYNCPOINT_EXIT
/** \brief Exit point of function FrTp_TxSm_TransmitSyncPoint() */
#define DBG_FRTP_TXSM_TRANSMITSYNCPOINT_EXIT(a,b,c)
#endif

#ifndef DBG_FRTP_TXSM_TRANSMITSYNCPOINTSF_ENTRY
/** \brief Entry point of function FrTp_TxSm_TransmitSyncPointSF() */
#define DBG_FRTP_TXSM_TRANSMITSYNCPOINTSF_ENTRY(a)
#endif

#ifndef DBG_FRTP_TXSM_TRANSMITSYNCPOINTSF_EXIT
/** \brief Exit point of function FrTp_TxSm_TransmitSyncPointSF() */
#define DBG_FRTP_TXSM_TRANSMITSYNCPOINTSF_EXIT(a,b)
#endif

#ifndef DBG_FRTP_TXSM_TRANSMITSYNCPOINTCFLF_ENTRY
/** \brief Entry point of function FrTp_TxSm_TransmitSyncPointCFLF() */
#define DBG_FRTP_TXSM_TRANSMITSYNCPOINTCFLF_ENTRY(a)
#endif

#ifndef DBG_FRTP_TXSM_TRANSMITSYNCPOINTCFLF_EXIT
/** \brief Exit point of function FrTp_TxSm_TransmitSyncPointCFLF() */
#define DBG_FRTP_TXSM_TRANSMITSYNCPOINTCFLF_EXIT(a,b)
#endif

#ifndef DBG_FRTP_TXSM_TRIGGERTRANSMIT_ENTRY
/** \brief Entry point of function FrTp_TxSm_TriggerTransmit() */
#define DBG_FRTP_TXSM_TRIGGERTRANSMIT_ENTRY(a,b,c)
#endif

#ifndef DBG_FRTP_TXSM_TRIGGERTRANSMIT_EXIT
/** \brief Exit point of function FrTp_TxSm_TriggerTransmit() */
#define DBG_FRTP_TXSM_TRIGGERTRANSMIT_EXIT(a,b,c,d)
#endif

#ifndef DBG_FRTP_TXSM_TRANSMITCONFIRMATION_ENTRY
/** \brief Entry point of function FrTp_TxSm_TransmitConfirmation() */
#define DBG_FRTP_TXSM_TRANSMITCONFIRMATION_ENTRY(a,b)
#endif

#ifndef DBG_FRTP_TXSM_TRANSMITCONFIRMATION_EXIT
/** \brief Exit point of function FrTp_TxSm_TransmitConfirmation() */
#define DBG_FRTP_TXSM_TRANSMITCONFIRMATION_EXIT(a,b)
#endif

#ifndef DBG_FRTP_TXSM_CTS_ENTRY
/** \brief Entry point of function FrTp_TxSm_CTS() */
#define DBG_FRTP_TXSM_CTS_ENTRY(a,b,c)
#endif

#ifndef DBG_FRTP_TXSM_CTS_EXIT
/** \brief Exit point of function FrTp_TxSm_CTS() */
#define DBG_FRTP_TXSM_CTS_EXIT(a,b,c)
#endif

#ifndef DBG_FRTP_TXSM_ACK_ENTRY
/** \brief Entry point of function FrTp_TxSm_ACK() */
#define DBG_FRTP_TXSM_ACK_ENTRY(a)
#endif

#ifndef DBG_FRTP_TXSM_ACK_EXIT
/** \brief Exit point of function FrTp_TxSm_ACK() */
#define DBG_FRTP_TXSM_ACK_EXIT(a)
#endif

#ifndef DBG_FRTP_TXSM_RETRY_ENTRY
/** \brief Entry point of function FrTp_TxSm_RETRY() */
#define DBG_FRTP_TXSM_RETRY_ENTRY(a,b)
#endif

#ifndef DBG_FRTP_TXSM_RETRY_EXIT
/** \brief Exit point of function FrTp_TxSm_RETRY() */
#define DBG_FRTP_TXSM_RETRY_EXIT(a,b)
#endif

#ifndef DBG_FRTP_TXSM_WT_ENTRY
/** \brief Entry point of function FrTp_TxSm_WT() */
#define DBG_FRTP_TXSM_WT_ENTRY(a)
#endif

#ifndef DBG_FRTP_TXSM_WT_EXIT
/** \brief Exit point of function FrTp_TxSm_WT() */
#define DBG_FRTP_TXSM_WT_EXIT(a)
#endif

#ifndef DBG_FRTP_TXSM_OVFLW_ENTRY
/** \brief Entry point of function FrTp_TxSm_OVFLW() */
#define DBG_FRTP_TXSM_OVFLW_ENTRY(a)
#endif

#ifndef DBG_FRTP_TXSM_OVFLW_EXIT
/** \brief Exit point of function FrTp_TxSm_OVFLW() */
#define DBG_FRTP_TXSM_OVFLW_EXIT(a)
#endif

#ifndef DBG_FRTP_TXSM_INDICATEANDABORT_ENTRY
/** \brief Entry point of function FrTp_TxSm_IndicateAndAbort() */
#define DBG_FRTP_TXSM_INDICATEANDABORT_ENTRY(a,b)
#endif

#ifndef DBG_FRTP_TXSM_INDICATEANDABORT_EXIT
/** \brief Exit point of function FrTp_TxSm_IndicateAndAbort() */
#define DBG_FRTP_TXSM_INDICATEANDABORT_EXIT(a,b)
#endif

#ifndef DBG_FRTP_RESETTXCHANNEL_ENTRY
/** \brief Entry point of function FrTp_ResetTxChannel() */
#define DBG_FRTP_RESETTXCHANNEL_ENTRY(a)
#endif

#ifndef DBG_FRTP_RESETTXCHANNEL_EXIT
/** \brief Exit point of function FrTp_ResetTxChannel() */
#define DBG_FRTP_RESETTXCHANNEL_EXIT(a)
#endif

#ifndef DBG_FRTP_TXSM_HANDLETRIGGERTXFRAME_ENTRY
/** \brief Entry point of function FrTp_TxSm_HandleTriggerTxFrame() */
#define DBG_FRTP_TXSM_HANDLETRIGGERTXFRAME_ENTRY(a,b,c,d)
#endif

#ifndef DBG_FRTP_TXSM_HANDLETRIGGERTXFRAME_EXIT
/** \brief Exit point of function FrTp_TxSm_HandleTriggerTxFrame() */
#define DBG_FRTP_TXSM_HANDLETRIGGERTXFRAME_EXIT(a,b,c,d)
#endif

#ifndef DBG_FRTP_TXSM_TXBUFFERHANDLING_ENTRY
/** \brief Entry point of function FrTp_TxSm_TxBufferHandling() */
#define DBG_FRTP_TXSM_TXBUFFERHANDLING_ENTRY(a,b,c)
#endif

#ifndef DBG_FRTP_TXSM_TXBUFFERHANDLING_EXIT
/** \brief Exit point of function FrTp_TxSm_TxBufferHandling() */
#define DBG_FRTP_TXSM_TXBUFFERHANDLING_EXIT(a,b,c,d)
#endif

#ifndef DBG_FRTP_FRIFCANCELTRANSMIT_ENTRY
/** \brief Entry point of function FrTp_FrIfCancelTransmit() */
#define DBG_FRTP_FRIFCANCELTRANSMIT_ENTRY(a)
#endif

#ifndef DBG_FRTP_FRIFCANCELTRANSMIT_EXIT
/** \brief Exit point of function FrTp_FrIfCancelTransmit() */
#define DBG_FRTP_FRIFCANCELTRANSMIT_EXIT(a,b)
#endif

/*==================[type definitions]======================================*/

/*==================[external function declarations]========================*/

/*==================[external constants]====================================*/

/*==================[external data]=========================================*/

#endif /* (!defined FRTP_TRACE_H) */
/*==================[end of file]===========================================*/
