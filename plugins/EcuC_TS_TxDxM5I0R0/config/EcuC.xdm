<?xml version='1.0'?>
<datamodel version="3.0" 
           xmlns="http://www.tresos.de/_projects/DataModel2/08/root.xsd" 
           xmlns:a="http://www.tresos.de/_projects/DataModel2/08/attribute.xsd" 
           xmlns:v="http://www.tresos.de/_projects/DataModel2/06/schema.xsd" 
           xmlns:d="http://www.tresos.de/_projects/DataModel2/06/data.xsd">

  <d:ctr type="AUTOSAR" factory="autosar" 
         xmlns:ad="http://www.tresos.de/_projects/DataModel2/08/admindata.xsd" 
         xmlns:icc="http://www.tresos.de/_projects/DataModel2/08/implconfigclass.xsd" 
         xmlns:mt="http://www.tresos.de/_projects/DataModel2/11/multitest.xsd" >
    <d:lst type="TOP-LEVEL-PACKAGES">
      <d:ctr name="TS_TxDxM5I0R0" type="AR-PACKAGE">
        <d:lst type="ELEMENTS">
          <d:chc name="EcuC" type="AR-ELEMENT" value="MODULE-DEF">
            <v:ctr type="MODULE-DEF">
              <a:a name="ADMIN-DATA" type="ADMIN-DATA">
                <ad:ADMIN-DATA>
                  <ad:LANGUAGE>EN</ad:LANGUAGE>
                  <ad:DOC-REVISIONS>
                    <ad:DOC-REVISION>
                      <ad:REVISION-LABEL>4.2.0</ad:REVISION-LABEL>
                      <ad:ISSUED-BY>AUTOSAR</ad:ISSUED-BY>
                      <ad:DATE>2011-11-09T11:36:22Z</ad:DATE>
                    </ad:DOC-REVISION>
                    <ad:DOC-REVISION>
                      <ad:REVISION-LABEL>5.0.5</ad:REVISION-LABEL>
                      <ad:ISSUED-BY>Elektrobit Automotive GmbH</ad:ISSUED-BY>
                      <ad:DATE>2013-02-28T23:59:59Z</ad:DATE>
                    </ad:DOC-REVISION>
                  </ad:DOC-REVISIONS>
                </ad:ADMIN-DATA>
              </a:a>
              <a:a name="DESC">
                <a:v>&lt;html&gt;
                        Virtual module to collect ECU Configuration specific / global configuration information.
                      &lt;/html&gt;</a:v>
              </a:a>
              <a:a name="LOWER-MULTIPLICITY" value="0"/>
              <a:a name="RELEASE" value="asc:4.0"/>
              <a:a name="UPPER-MULTIPLICITY" value="1"/>
              <a:a name="UUID" value="ECUC:f72efedf-ee71-4a1b-b4d6-b77adfbf4cf2"/>
              <v:ctr name="CommonPublishedInformation" type="IDENTIFIABLE">
                <a:a name="DESC">
                  <a:v>EN:
                    &lt;html&gt;
                      Common container, aggregated by all modules. It contains published information about vendor and versions.
                  &lt;/html&gt;</a:v>
                </a:a>
                <a:a name="LABEL" value="Common Published Information"/>
                <a:a name="UUID" 
                     value="ECUC:a00be3e0-8783-9123-2d52-1eb616737ca6"/>
                <v:var name="ArMajorVersion" type="INTEGER_LABEL">
                  <a:a name="DESC">
                    <a:v>EN:
                      &lt;html&gt;
                        Major version number of AUTOSAR specification on which the appropriate implementation is based on.
                    &lt;/html&gt;</a:v>
                  </a:a>
                  <a:a name="IMPLEMENTATIONCONFIGCLASS" 
                       value="PublishedInformation"/>
                  <a:a name="LABEL" value="AUTOSAR Major Version"/>
                  <a:a name="ORIGIN" value="Elektrobit Automotive GmbH"/>
                  <a:a name="SYMBOLICNAMEVALUE" value="false"/>
                  <a:a name="UUID" 
                       value="ECUC:948f3f2e-f129-4bc5-b4e1-7a7bdb8599e1"/>
                  <a:da name="DEFAULT" value="3"/>
                </v:var>
                <v:var name="ArMinorVersion" type="INTEGER_LABEL">
                  <a:a name="DESC">
                    <a:v>EN:
                      &lt;html&gt;
                        Minor version number of AUTOSAR specification on which the appropriate implementation is based on.
                    &lt;/html&gt;</a:v>
                  </a:a>
                  <a:a name="IMPLEMENTATIONCONFIGCLASS" 
                       value="PublishedInformation"/>
                  <a:a name="LABEL" value="AUTOSAR Minor Version"/>
                  <a:a name="ORIGIN" value="Elektrobit Automotive GmbH"/>
                  <a:a name="SYMBOLICNAMEVALUE" value="false"/>
                  <a:a name="UUID" 
                       value="ECUC:2893b920-59d5-4ac2-b2c1-e23742e66d70"/>
                  <a:da name="DEFAULT" value="2"/>
                </v:var>
                <v:var name="ArPatchVersion" type="INTEGER_LABEL">
                  <a:a name="DESC">
                    <a:v>EN:
                      &lt;html&gt;
                        Patch level version number of AUTOSAR specification on which the appropriate implementation is based on.
                    &lt;/html&gt;</a:v>
                  </a:a>
                  <a:a name="IMPLEMENTATIONCONFIGCLASS" 
                       value="PublishedInformation"/>
                  <a:a name="LABEL" value="AUTOSAR Patch Version"/>
                  <a:a name="ORIGIN" value="Elektrobit Automotive GmbH"/>
                  <a:a name="SYMBOLICNAMEVALUE" value="false"/>
                  <a:a name="UUID" 
                       value="ECUC:6428eb9b-8790-488a-b9a3-0fba52d0f59e"/>
                  <a:da name="DEFAULT" value="0"/>
                </v:var>
                <v:var name="SwMajorVersion" type="INTEGER_LABEL">
                  <a:a name="DESC">
                    <a:v>EN:
                      &lt;html&gt;
                        Major version number of the vendor specific implementation of the module.
                    &lt;/html&gt;</a:v>
                  </a:a>
                  <a:a name="IMPLEMENTATIONCONFIGCLASS" 
                       value="PublishedInformation"/>
                  <a:a name="LABEL" value="Software Major Version"/>
                  <a:a name="ORIGIN" value="Elektrobit Automotive GmbH"/>
                  <a:a name="SYMBOLICNAMEVALUE" value="false"/>
                  <a:a name="UUID" 
                       value="ECUC:605b18ae-3f9a-41d4-9225-67c9c5f6fc34"/>
                  <a:da name="DEFAULT" value="5"/>
                </v:var>
                <v:var name="SwMinorVersion" type="INTEGER_LABEL">
                  <a:a name="DESC">
                    <a:v>EN:
                      &lt;html&gt;
                        Minor version number of the vendor specific implementation of the module. The numbering is vendor specific.
                    &lt;/html&gt;</a:v>
                  </a:a>
                  <a:a name="IMPLEMENTATIONCONFIGCLASS" 
                       value="PublishedInformation"/>
                  <a:a name="LABEL" value="Software Minor Version"/>
                  <a:a name="ORIGIN" value="Elektrobit Automotive GmbH"/>
                  <a:a name="SYMBOLICNAMEVALUE" value="false"/>
                  <a:a name="UUID" value="a7fe44dc-ea25-4b8d-8fad-9fbcae86d56f"/>
                  <a:da name="DEFAULT" value="0"/>
                </v:var>
                <v:var name="SwPatchVersion" type="INTEGER_LABEL">
                  <a:a name="DESC">
                    <a:v>EN:
                      &lt;html&gt;
                        Patch level version number of the vendor specific implementation of the module. The numbering is vendor specific.
                    &lt;/html&gt;</a:v>
                  </a:a>
                  <a:a name="IMPLEMENTATIONCONFIGCLASS" 
                       value="PublishedInformation"/>
                  <a:a name="LABEL" value="Software Patch Version"/>
                  <a:a name="ORIGIN" value="Elektrobit Automotive GmbH"/>
                  <a:a name="SYMBOLICNAMEVALUE" value="false"/>
                  <a:a name="UUID" value="a318d2d9-0e75-49da-ac43-e7e4e682e2f9"/>
                  <a:da name="DEFAULT" value="5"/>
                </v:var>
                <v:var name="ModuleId" type="INTEGER_LABEL">
                  <a:a name="DESC">
                    <a:v>EN:
                      &lt;html&gt;
                        Module ID of this module from Module List
                    &lt;/html&gt;</a:v>
                  </a:a>
                  <a:a name="IMPLEMENTATIONCONFIGCLASS" 
                       value="PublishedInformation"/>
                  <a:a name="LABEL" value="Numeric Module ID"/>
                  <a:a name="ORIGIN" value="Elektrobit Automotive GmbH"/>
                  <a:a name="SYMBOLICNAMEVALUE" value="false"/>
                  <a:a name="UUID" 
                       value="ECUC:78bc8362-080f-4253-b3da-804ab69a7154"/>
                  <a:da name="DEFAULT" value="10"/>
                </v:var>
                <v:var name="VendorId" type="INTEGER_LABEL">
                  <a:a name="DESC">
                    <a:v>EN:
                      &lt;html&gt;
                        Vendor ID of the dedicated implementation of this module according to the AUTOSAR vendor list
                    &lt;/html&gt;</a:v>
                  </a:a>
                  <a:a name="IMPLEMENTATIONCONFIGCLASS" 
                       value="PublishedInformation"/>
                  <a:a name="LABEL" value="Vendor ID"/>
                  <a:a name="ORIGIN" value="Elektrobit Automotive GmbH"/>
                  <a:a name="SYMBOLICNAMEVALUE" value="false"/>
                  <a:a name="UUID" 
                       value="ECUC:01b0f467-6943-4558-b4f2-3fa1fad28449"/>
                  <a:da name="DEFAULT" value="1"/>
                </v:var>
                <v:var name="VendorApiInfix" type="STRING_LABEL">
                  <a:a name="IMPLEMENTATIONCONFIGCLASS" 
                       value="PublishedInformation"/>
                  <a:a name="LABEL" value="Vendor API Infix"/>
                  <a:a name="OPTIONAL" value="true"/>
                  <a:a name="ORIGIN" value="Elektrobit Automotive GmbH"/>
                  <a:a name="SYMBOLICNAMEVALUE" value="false"/>
                  <a:a name="UUID" 
                       value="ECUC:1c68a547-f24e-4a4e-9540-69fbd466ec89"/>
                  <a:a name="VISIBLE" value="false"/>
                  <a:da name="DEFAULT" value=""/>
                  <a:da name="ENABLE" value="false"/>
                </v:var>
                <v:var name="Release" type="STRING_LABEL">
                  <a:a name="IMPLEMENTATIONCONFIGCLASS" 
                       value="PublishedInformation"/>
                  <a:a name="LABEL" value="Release Information"/>
                  <a:a name="ORIGIN" value="Elektrobit Automotive GmbH"/>
                  <a:a name="SYMBOLICNAMEVALUE" value="false"/>
                  <a:a name="UUID" 
                       value="ECUC:1c68a547-f24e-4a4e-9540-69fbd533ec89"/>
                  <a:da name="DEFAULT" value=""/>
                </v:var>
              </v:ctr>
              <v:var name="IMPLEMENTATION_CONFIG_VARIANT" type="ENUMERATION">
                <a:a name="LABEL" value="Config Variant"/>
                <a:da name="DEFAULT" value="VariantPostBuild"/>
                <a:da name="EDITABLE" value="false"/>
                <a:da name="RANGE" value="VariantPostBuild"/>
              </v:var>
              <v:ctr name="EcucPartitionCollection" type="IDENTIFIABLE">
                <a:a name="DESC">
                  <a:v>&lt;html&gt;
                        Collection of Partitions defined for this ECU.
                      &lt;/html&gt;</a:v>
                </a:a>
                <a:a name="OPTIONAL" value="true"/>
                <a:a name="TAB" value="EcucPartition"/>
                <a:a name="UUID" 
                     value="ECUC:ed28c0d5-4e0c-4826-89fa-449203a3ce8e"/>
                <a:da name="ENABLE" value="false"/>
                <v:lst name="EcucPartition" type="MAP">
                  <v:ctr name="EcucPartition" type="IDENTIFIABLE">
                    <a:a name="DESC">
                      <a:v>&lt;html&gt;
                        Definition of one Partition on this ECU. One Partition will be implemented using one Os-Application.
                      &lt;/html&gt;</a:v>
                    </a:a>
                    <a:a name="POSTBUILDCHANGEABLE" value="false"/>
                    <a:a name="UUID" 
                         value="ECUC:c11de67e-41cd-4da2-8cfc-01c5e34209be"/>
                    <v:var name="EcucPartitionBswModuleExecution" 
                           type="BOOLEAN">
                      <a:a name="DESC">
                        <a:v>&lt;html&gt;
                        Denotes that this partition will execute all BSW Modules. BSW Modules can only be executed in one partition.
                      &lt;/html&gt;</a:v>
                      </a:a>
                      <a:a name="IMPLEMENTATIONCONFIGCLASS" 
                           type="IMPLEMENTATIONCONFIGCLASS">
                        <icc:v class="PreCompile">VariantPostBuild</icc:v>
                      </a:a>
                      <a:a name="ORIGIN" value="AUTOSAR_ECUC"/>
                      <a:a name="SYMBOLICNAMEVALUE" value="false"/>
                      <a:a name="UUID" 
                           value="ECUC:72f87183-bf87-4f64-a990-38b29817aa2c"/>
                    </v:var>
                    <v:var name="PartitionCanBeRestarted" type="BOOLEAN">
                      <a:a name="DESC">
                        <a:v>&lt;html&gt;
                        Specifies the requirement whether the Partition can be restarted. If set to true all software executing in this partition shall be capable of handling a restart.
                      &lt;/html&gt;</a:v>
                      </a:a>
                      <a:a name="IMPLEMENTATIONCONFIGCLASS" 
                           type="IMPLEMENTATIONCONFIGCLASS">
                        <icc:v class="PreCompile">VariantPostBuild</icc:v>
                      </a:a>
                      <a:a name="ORIGIN" value="AUTOSAR_ECUC"/>
                      <a:a name="SYMBOLICNAMEVALUE" value="false"/>
                      <a:a name="UUID" 
                           value="ECUC:4fbca4af-d17a-463a-9b70-fad218ab42a9"/>
                    </v:var>
                    <v:lst name="EcucPartitionSoftwareComponentInstanceRef" 
                           type="MAP">
                      <v:ctr name="EcucPartitionSoftwareComponentInstanceRef" 
                             type="INSTANCE">
                        <a:a name="DESC">
                          <a:v>&lt;html&gt;
                        References the SW Component instances from the Ecu Extract that shall be executed in this partition.
                      &lt;/html&gt;</a:v>
                        </a:a>
                        <a:a name="IMPLEMENTATIONCONFIGCLASS" 
                             type="IMPLEMENTATIONCONFIGCLASS">
                          <icc:v class="PreCompile">VariantPostBuild</icc:v>
                        </a:a>
                        <a:a name="ORIGIN" value="AUTOSAR_ECUC"/>
                        <a:a name="UUID" 
                             value="ECUC:fcd3eca6-d43f-42f7-86a1-80043415256b"/>
                        <v:ref name="TARGET" type="REFERENCE">
                          <a:a name="ORIGIN" value="AUTOSAR_ECUC"/>
                          <a:da name="REF" value="ASTyped:SwComponentPrototype"/>
                        </v:ref>
                        <v:lst name="CONTEXT">
                          <v:ref name="CONTEXT" type="REFERENCE">
                            <a:a name="ORIGIN" value="AUTOSAR_ECUC"/>
                            <a:da name="RANGE" type="IRefCtxt" 
                                  expr="ROOT-SW-COMPOSITION-PROTOTYPE"/>
                          </v:ref>
                        </v:lst>
                      </v:ctr>
                    </v:lst>
                  </v:ctr>
                </v:lst>
              </v:ctr>
              <v:ctr name="EcucPduCollection" type="IDENTIFIABLE">
                <a:a name="DESC">
                  <a:v>&lt;html&gt;
                        Collection of all Pdu objects flowing through the Com-Stack.
                      &lt;/html&gt;</a:v>
                </a:a>
                <a:a name="OPTIONAL" value="true"/>
                <a:a name="TAB" value="Pdu"/>
                <a:a name="UUID" 
                     value="ECUC:cece0b62-6981-43fb-a5b7-d573fdfb2344"/>
                <a:da name="ENABLE" value="false"/>
                <v:var name="PduIdTypeEnum" type="ENUMERATION">
                  <a:a name="DESC">
                    <a:v>&lt;html&gt;
                        The PduIdType is used within the entire AUTOSAR Com Stack except for bus drivers. The size of this global type depends on the maximum number of PDUs used within one software module. If no software module deals with more PDUs that 256, this type can be set to uint8. If at least one software module handles more than 256 PDUs, this type must be set to uint16. See AUTOSAR_SWS_CommunicationStackTypes for more details.
                      &lt;/html&gt;</a:v>
                  </a:a>
                  <a:a name="IMPLEMENTATIONCONFIGCLASS" 
                       type="IMPLEMENTATIONCONFIGCLASS">
                    <icc:v class="PreCompile">VariantPostBuild</icc:v>
                  </a:a>
                  <a:a name="ORIGIN" value="AUTOSAR_ECUC"/>
                  <a:a name="SYMBOLICNAMEVALUE" value="false"/>
                  <a:a name="UUID" 
                       value="ECUC:58955203-89bd-4dcf-b3d4-c5e83c87080b"/>
                  <a:da name="DEFAULT" value="UINT16"/>
                  <a:da name="EDITABLE" value="false"/>
                  <a:da name="INVALID" type="XPath" expr=". != &apos;UINT16&apos;" 
                        true="Only data type uint16 is supported"/>
                  <a:da name="RANGE">
                    <a:v>UINT16</a:v>
                    <a:v>UINT8</a:v>
                  </a:da>
                </v:var>
                <v:var name="PduLengthTypeEnum" type="ENUMERATION">
                  <a:a name="DESC">
                    <a:v>&lt;html&gt;
                        The PduLengthType is used within the entire AUTOSAR Com Stack except for bus drivers. The size of this global type depends on the maximum length of PDUs to be sent by an ECU. If no segmentation is used the length depends on the maximum payload size of a frame of the underlying communication system (for FlexRay maximum size is 255 bytes, therefore uint8). If segementation is used it depends on the maximum length of a  segmeneted N-PDU (in general uint16 is used). See AUTOSAR_SWS_CommunicationStackTypes for more details.
                      &lt;/html&gt;</a:v>
                  </a:a>
                  <a:a name="IMPLEMENTATIONCONFIGCLASS" 
                       type="IMPLEMENTATIONCONFIGCLASS">
                    <icc:v class="PreCompile">VariantPostBuild</icc:v>
                  </a:a>
                  <a:a name="ORIGIN" value="AUTOSAR_ECUC"/>
                  <a:a name="SYMBOLICNAMEVALUE" value="false"/>
                  <a:a name="UUID" 
                       value="ECUC:3ffce745-6c26-4c88-a57f-cd785b21e467"/>
                  <a:da name="DEFAULT" value="UINT16"/>
                  <a:da name="EDITABLE" value="false"/>
                  <a:da name="INVALID" type="XPath">
                    <a:tst 
                           expr="                               (count(../Pdu/*) &gt; 0)                           and (node:empty(../Pdu/*/PduLength) != &apos;true&apos;)                           and (   ((num:max(../Pdu/*/PduLength) &gt; 65535) and (. != &apos;UINT32&apos;))                                or ((num:max(../Pdu/*/PduLength) &gt; 255)   and (.  = &apos;UINT8&apos;)))" 
                           true="Value of PduLengthTypeEnum is too small to represent PduLength                                  of greatest PduLength."/>
                    <a:tst expr=". != &apos;UINT16&apos;" 
                           true="Only data type uint16 is supported"/>
                  </a:da>
                  <a:da name="RANGE">
                    <a:v>UINT16</a:v>
                    <a:v>UINT32</a:v>
                    <a:v>UINT8</a:v>
                  </a:da>
                </v:var>
                <v:lst name="Pdu" type="MAP">
                  <v:ctr name="Pdu" type="IDENTIFIABLE">
                    <a:a name="DESC">
                      <a:v>&lt;html&gt;
                        One Pdu flowing through the COM-Stack. This Pdu is used by all Com-Stack modules to agree on referencing the same Pdu.
                      &lt;/html&gt;</a:v>
                    </a:a>
                    <a:a name="POSTBUILDCHANGEABLE" value="true"/>
                    <a:a name="UUID" 
                         value="ECUC:422a7abf-add2-418a-81a6-af5cdeae1930"/>
                    <v:var name="PduLength" type="INTEGER">
                      <a:a name="DESC">
                        <a:v>&lt;html&gt;
                        Length of the Pdu in bytes.  It should be noted that in former AUTOSAR releases (Rel 2.1, Rel 3.0, Rel 3.1, Rel 4.0 Rev. 1) this parameter was defined in bits.
                      &lt;/html&gt;</a:v>
                      </a:a>
                      <a:a name="IMPLEMENTATIONCONFIGCLASS" 
                           type="IMPLEMENTATIONCONFIGCLASS">
                        <icc:v class="PostBuild">VariantPostBuild</icc:v>
                      </a:a>
                      <a:a name="ORIGIN" value="AUTOSAR_ECUC"/>
                      <a:a name="SYMBOLICNAMEVALUE" value="false"/>
                      <a:a name="UUID" 
                           value="ECUC:3996a201-ffdf-43d1-a5e8-f88592a78a35"/>
                      <a:da name="INVALID" type="Range">
                        <a:tst expr="&lt;=4095"/>
                        <a:tst expr="&gt;=0"/>
                      </a:da>
                    </v:var>
                    <v:ref name="SysTPduToFrameMappingRef" 
                           type="FOREIGN-REFERENCE">
                      <a:a name="DESC">
                        <a:v>&lt;html&gt;
                        Optional reference to the PduToFrameMapping from the SystemTemplate which this Pdu represents.
                      &lt;/html&gt;</a:v>
                      </a:a>
                      <a:a name="DESTINATION-TYPE" 
                           value="ASTyped:PDU-TO-FRAME-MAPPING"/>
                      <a:a name="IMPLEMENTATIONCONFIGCLASS" 
                           type="IMPLEMENTATIONCONFIGCLASS">
                        <icc:v class="PreCompile">VariantPostBuild</icc:v>
                      </a:a>
                      <a:a name="OPTIONAL" value="true"/>
                      <a:a name="ORIGIN" value="AUTOSAR_ECUC"/>
                      <a:a name="UUID" 
                           value="ECUC:44ff5259-e0d4-4c83-bba5-1d113db79352"/>
                      <a:da name="ENABLE" value="false"/>
                      <a:da name="REF" value="ASTyped:PduToFrameMapping"/>
                    </v:ref>
                  </v:ctr>
                </v:lst>
              </v:ctr>
              <v:ctr name="EcucVariationResolver" type="IDENTIFIABLE">
                <a:a name="DESC">
                  <a:v>&lt;html&gt;
                        Collection of PredefinedVariant elements containing definition of values for SwSystemconst which shall be applied when resolving the variability during ECU Configuration.
                      &lt;/html&gt;</a:v>
                </a:a>
                <a:a name="OPTIONAL" value="true"/>
                <a:a name="TAB" value="PredefinedVariantRef"/>
                <a:a name="UUID" 
                     value="ECUC:973d00a5-5e98-4b38-8b7b-ddd518d40317"/>
                <a:da name="ENABLE" value="false"/>
                <v:lst name="PredefinedVariantRef">
                  <a:da name="MIN" value="1"/>
                  <v:ref name="PredefinedVariantRef" type="FOREIGN-REFERENCE">
                    <a:a name="DESTINATION-TYPE" 
                         value="ASTyped:PREDEFINED-VARIANT"/>
                    <a:a name="IMPLEMENTATIONCONFIGCLASS" 
                         type="IMPLEMENTATIONCONFIGCLASS">
                      <icc:v class="PreCompile">VariantPostBuild</icc:v>
                    </a:a>
                    <a:a name="ORIGIN" value="AUTOSAR_ECUC"/>
                    <a:a name="UUID" 
                         value="ECUC:84013509-59a1-46d4-ba0d-5bcfb76b941f"/>
                    <a:da name="REF" value="ASTyped:PredefinedVariant"/>
                  </v:ref>
                </v:lst>
              </v:ctr>
            </v:ctr>
          </d:chc>
        </d:lst>
      </d:ctr>
    </d:lst>
  </d:ctr>

</datamodel>
