# \file
#
# \brief AUTOSAR FiM
#
# This file contains the implementation of the AUTOSAR
# module FiM.
#
# \author Elektrobit Automotive GmbH, 91058 Erlangen, Germany
#
# Copyright 2005 - 2013 Elektrobit Automotive GmbH
# All rights exclusively reserved for Elektrobit Automotive GmbH,
# unless expressly agreed to otherwise.

#################################################################
# DEFINITIONS

FiM_CORE_PATH         := $(SSC_ROOT)\FiM_$(FiM_VARIANT)

FiM_OUTPUT_PATH       := $(AUTOSAR_BASE_OUTPUT_PATH)

#################################################################
# REGISTRY
SSC_PLUGINS           += FiM
CC_INCLUDE_PATH       +=    \
   $(FiM_CORE_PATH)\include \
   $(FiM_OUTPUT_PATH)\include
