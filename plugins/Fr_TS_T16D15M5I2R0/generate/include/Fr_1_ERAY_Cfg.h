/**
 * \file
 *
 * \brief AUTOSAR Fr
 *
 * This file contains the implementation of the AUTOSAR
 * module Fr.
 *
 * \author Elektrobit Automotive GmbH, 91058 Erlangen, Germany
 *
 * Copyright 2005 - 2013 Elektrobit Automotive GmbH
 * All rights exclusively reserved for Elektrobit Automotive GmbH,
 * unless expressly agreed to otherwise.
 */
[!CODE!]
/* !LINKSTO Fr.ASR40.FR074,1 */

#if !defined _FR_1_ERAY_CFG_H_
#define _FR_1_ERAY_CFG_H_

/*
 * Include Section
 */

#include <Std_Types.h>      /* definitions of STD_ON and STD_OFF */
#include <TSAutosar.h>      /* definitions of TS_PROD_ERR_* */
/*
 * Global Macros
 */
[!SELECT "FrGeneral"!]
/* standard SWS pre-compile time configuration parameters */
#define FR_1_ERAY_DEV_ERROR_DETECT [!IF "FrDevErrorDetect = 'true'"!]STD_ON[!ELSE!]STD_OFF[!ENDIF!]
#define FR_1_ERAY_VERSION_INFO_API [!IF "FrVersionInfoApi = 'true'"!]STD_ON[!ELSE!]STD_OFF[!ENDIF!]
    
[!ENDSELECT!]

/** \brief Switch for DEM to DET reporting for production error FRIF_PROD_ERR_HANDLING_JLE_SYNC */
#define FR_1_ERAY_PROD_ERR_HANDLING_CTRL_TEST   [!//
[!IF "ReportToDem/FrDemCtrlTestResultReportToDem = 'DEM'"!][!//
TS_PROD_ERR_REP_TO_DEM
[!ELSEIF "ReportToDem/FrDemCtrlTestResultReportToDem = 'DET'"!][!//
TS_PROD_ERR_REP_TO_DET
[!ELSE!][!//
TS_PROD_ERR_DISABLE
[!ENDIF!][!//

[!IF "ReportToDem/FrDemCtrlTestResultReportToDem = 'DET'"!][!//
/** \brief Det error ID, if DEM to DET reporting is enabled for FRIF_PROD_ERR_HANDLING_JLE_SYNC */
#define FR_1_ERAY_E_DEMTODET_CTRL_TEST          [!"ReportToDem/FrDemCtrlTestResultReportToDemDetErrorId"!]U
[!ENDIF!][!//

[!SELECT "FrGeneral/VendorSpecific"!]
/* Eb specific pre-compile time configuration parameters */
#define FR_1_ERAY_SINGLE_CTRL_OPT_ENABLE [!IF "SingleCtrlOptEnable = 'true'"!]STD_ON[!ELSE!]STD_OFF[!ENDIF!]
#define FR_1_ERAY_FRIF_INTEGRATION_ENABLE [!IF "FrIfIntegrationEnable = 'true'"!]STD_ON[!ELSE!]STD_OFF[!ENDIF!]
#define FR_1_ERAY_REPORT_TO_DET_ENABLE [!IF "ReportToDetEnable = 'true'"!]STD_ON[!ELSE!]STD_OFF[!ENDIF!]
#define FR_1_ERAY_RELATIVE_TIMER_ENABLE [!IF "RelativeTimerApiEnable = 'true'"!]STD_ON[!ELSE!]STD_OFF[!ENDIF!]
#define FR_1_ERAY_NMVECTOR_ENABLE [!IF "GetNmVectorApiEnable = 'true'"!]STD_ON[!ELSE!]STD_OFF[!ENDIF!]
#define FR_1_ERAY_SETEXTSYNC_API_ENABLE [!IF "SetExtSyncApiEnable = 'true'"!]STD_ON[!ELSE!]STD_OFF[!ENDIF!]
#define FR_1_ERAY_MTS_API_ENABLE [!IF "MtsApiEnable = 'true'"!]STD_ON[!ELSE!]STD_OFF[!ENDIF!]
#define FR_1_ERAY_WAKEUP_API_ENABLE [!IF "WakeupApiEnable = 'true'"!]STD_ON[!ELSE!]STD_OFF[!ENDIF!]
#define FR_1_ERAY_PREPARELPDU_API_ENABLE [!IF "PrepareLpduApiEnable = 'true'"!]STD_ON[!ELSE!]STD_OFF[!ENDIF!]
#define FR_1_ERAY_GETIRQSTATUS_API_ENABLE [!IF "GetIrqStatusApiEnable = 'true'"!]STD_ON[!ELSE!]STD_OFF[!ENDIF!]
#define FR_1_ERAY_DISABLEIRQ_API_ENABLE [!IF "DisableIrqApiEnable = 'true'"!]STD_ON[!ELSE!]STD_OFF[!ENDIF!]
#define FR_1_ERAY_GETCHANNELSTATUS_API_ENABLE [!IF "GetChannelStatusApiEnable = 'true'"!]STD_ON[!ELSE!]STD_OFF[!ENDIF!]
#define FR_1_ERAY_GETCONTROLLERERRORSTATUS_API_ENABLE [!IF "GetControllerErrStatusApiEnable = 'true'"!]STD_ON[!ELSE!]STD_OFF[!ENDIF!]
#define FR_1_ERAY_ALLSLOTS_API_ENABLE [!IF "AllSlotsApiEnable = 'true'"!]STD_ON[!ELSE!]STD_OFF[!ENDIF!]
#define FR_1_ERAY_RECONFIGLPDU_API_ENABLE [!IF "ReconfigLPduApiEnable = 'true'"!]STD_ON[!ELSE!]STD_OFF[!ENDIF!]
#define FR_1_ERAY_DISABLELPDU_API_ENABLE [!IF "DisableLPduApiEnable = 'true'"!]STD_ON[!ELSE!]STD_OFF[!ENDIF!]
#define FR_1_ERAY_ALLOWCOLDSTART_API_ENABLE [!IF "AllowColdstartApiEnable = 'true'"!]STD_ON[!ELSE!]STD_OFF[!ENDIF!]
#define FR_1_ERAY_GETRXWAKEUPSTATUS_API_ENABLE [!IF "GetWakeupRxStatusApiEnable = 'true'"!]STD_ON[!ELSE!]STD_OFF[!ENDIF!]
#define FR_1_ERAY_EXT_IRQ_SERVICES_API_ENABLE [!IF "ExtIRQServicesApiEnable = 'true'"!]STD_ON[!ELSE!]STD_OFF[!ENDIF!]
#define FR_1_ERAY_GETCLOCKCORRECTION_API_ENABLE [!IF "GetClockCorrectionApiEnable = 'true'"!]STD_ON[!ELSE!]STD_OFF[!ENDIF!]
#define FR_1_ERAY_GETSYNCFRAMELIST_API_ENABLE [!IF "GetSyncFrameListApiEnable = 'true'"!]STD_ON[!ELSE!]STD_OFF[!ENDIF!]
#define FR_1_ERAY_DYNAMIC_PAYLOAD_LENGTH_ENABLE [!IF "DynamicPayloadLengthEnable = 'true'"!]STD_ON[!ELSE!]STD_OFF[!ENDIF!]

#define FR_1_ERAY_PAYLOAD_PADDING_ENABLE [!IF "PayloadPaddingEnable = 'true'"!]STD_ON[!ELSE!]STD_OFF[!ENDIF!]
#define FR_1_ERAY_PAYLOAD_PADDING_U8_VALUE [!"PayloadPaddingU8Value"!]U

#define FR_1_ERAY_TIMEOUT_LOOP_LIMIT [!"HardwareSettings/HardwareAccessTimeoutLoopLimit"!]U

#define FR_1_ERAY_PBCFGM_SUPPORT_ENABLED [!IF "node:contains(node:refs(as:modconf('PbcfgM')/PbcfgMBswModules/*/PbcfgMBswModuleRef), as:modconf('Fr')) = 'true'"!]STD_ON[!ELSE!]STD_OFF[!ENDIF!]
[!IF "node:contains(node:refs(as:modconf('PbcfgM')/PbcfgMBswModules/*/PbcfgMBswModuleRef), as:modconf('Fr')) = 'true'"!]
#define FR_1_ERAY_RELOCATABLE_CFG_ENABLE [!IF "as:modconf('PbcfgM')/PbcfgMGeneral/PbcfgMRelocatableCfgEnable = 'true'"!]STD_ON[!ELSE!]STD_OFF[!ENDIF!]
[!ELSE!]
#define FR_1_ERAY_RELOCATABLE_CFG_ENABLE [!IF "FrRelocatablePbcfgEnable = 'true'"!]STD_ON[!ELSE!]STD_OFF[!ENDIF!]
[!ENDIF!]

/** \brief Link time verification value */
#define FR_1_ERAY_PUBLIC_INFO_SIGNATURE [!"asc:getConfigSignature(node:difference(as:modconf('Fr')[1]/CommonPublishedInformation//*[not(child::*) and (node:configclass() = 'PublishedInformation')], as:modconf('Fr')[1]/CommonPublishedInformation/Release))"!]U

/** \brief Compile time verification value */
#define FR_1_ERAY_CFG_SIGNATURE [!"asc:getConfigSignature(as:modconf('Fr')[1]//*[not(child::*) and (node:configclass() = 'PreCompile') ])"!]U

#endif /* _FR_1_ERAY_CFG_H_ */

/*==================[end of file]============================================*/
[!ENDSELECT!]
[!ENDCODE!][!//

