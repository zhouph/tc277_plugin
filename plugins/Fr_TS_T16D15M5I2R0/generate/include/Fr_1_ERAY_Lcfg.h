/**
 * \file
 *
 * \brief AUTOSAR Fr
 *
 * This file contains the implementation of the AUTOSAR
 * module Fr.
 *
 * \author Elektrobit Automotive GmbH, 91058 Erlangen, Germany
 *
 * Copyright 2005 - 2013 Elektrobit Automotive GmbH
 * All rights exclusively reserved for Elektrobit Automotive GmbH,
 * unless expressly agreed to otherwise.
 */

/* !LINKSTO Fr.ASR40.FR074,1 */

#if (!defined FR_1_ERAY_LCFG_H)
#define FR_1_ERAY_LCFG_H

[!AUTOSPACING!]
/*==================[includes]==============================================*/

#include <TSAutosar.h>              /* global configuration */

/*==================[macros]================================================*/

/*==================[type definitions]======================================*/

/*==================[external function declarations]========================*/

/*==================[internal function declarations]========================*/

/*==================[external constants]====================================*/

/*==================[internal constants]====================================*/

/*==================[external data]=========================================*/

/*==================[internal data]=========================================*/

/* start data section declaration */
#define FR_1_ERAY_START_SEC_CONST_32
#include <MemMap.h> /* !LINKSTO Fr.ASR40.FR112,1 */

/* value used to validate post build configuration against link time configuration */
extern CONST(uint32, FR_1_ERAY_CONST) Fr_1_ERAY_LcfgSignature;

/* stop data section declaration */
#define FR_1_ERAY_STOP_SEC_CONST_32
#include <MemMap.h> /* !LINKSTO Fr.ASR40.FR112,1 */

/*==================[external function definitions]=========================*/

/*==================[internal function definitions]=========================*/

#endif /* if !defined( FR_1_ERAY_LCFG_H ) */
/*==================[end of file]===========================================*/
