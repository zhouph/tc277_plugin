# \file
#
# \brief AUTOSAR Fr
#
# This file contains the implementation of the AUTOSAR
# module Fr.
#
# \author Elektrobit Automotive GmbH, 91058 Erlangen, Germany
#
# Copyright 2005 - 2013 Elektrobit Automotive GmbH
# All rights exclusively reserved for Elektrobit Automotive GmbH,
# unless expressly agreed to otherwise.

#################################################################
# DEFINITIONS
Fr_CORE_PATH := $(SSC_ROOT)\Fr_$(Fr_VARIANT)

Fr_OUTPUT_PATH := $(AUTOSAR_BASE_OUTPUT_PATH)

Fr_GEN_FILES =                                  \
    $(Fr_OUTPUT_PATH)\include\Fr_1_ERAY_Cfg.h   \
    $(Fr_OUTPUT_PATH)\include\Fr_1_ERAY_PBcfg.h \
    $(Fr_OUTPUT_PATH)\src\Fr_1_ERAY_PBcfg.c     \

TRESOS_GEN_FILES     += $(Fr_GEN_FILES)

#################################################################
# REGISTRY
SSC_PLUGINS         += Fr
Fr_DEPENDENT_PLUGINS := base_make tresos
Fr_VERSION           := 3.00.00
Fr_DESCRIPTION       := Fr Basic Software Makefile PlugIn for Autosar

CC_INCLUDE_PATH +=                                   \
    $(Fr_CORE_PATH)\include                          \
    $(Fr_CORE_PATH)\src                              \
    $(Fr_CORE_PATH)\src\$(TARGET)\$(DERIVATE)        \
    $(Fr_CORE_PATH)\src\FCAL                         \
    $(Fr_CORE_PATH)\src\MAL                          \
    $(Fr_CORE_PATH)\src\MLIB                         \

ASM_INCLUDE_PATH +=
CPP_INCLUDE_PATH +=
