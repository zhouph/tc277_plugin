/**
 * \file
 *
 * \brief AUTOSAR Fr
 *
 * This file contains the implementation of the AUTOSAR
 * module Fr.
 *
 * \author Elektrobit Automotive GmbH, 91058 Erlangen, Germany
 *
 * Copyright 2005 - 2013 Elektrobit Automotive GmbH
 * All rights exclusively reserved for Elektrobit Automotive GmbH,
 * unless expressly agreed to otherwise.
 */

#if !defined _FR_1_ERAY_MLIB_SWAP_H_
#define _FR_1_ERAY_MLIB_SWAP_H_


/*
 * swap the two bytes of a 16-bit value
 */
STATIC INLINE U16 MLIB_SWAP16(U16 a);
STATIC INLINE U16 MLIB_SWAP16(U16 a)
{
    return (U16)(((U16)((U16)(a<<8U)&(U16)0xff00U)))|((U16)(((U16)(a>>8U)&(U16)0x00ffU)));
}

/*
 * swap all 4 bytes of a 32-bit value (ABCD -> DCBA)
 */
STATIC INLINE U32 MLIB_SWAP32(U32 a);
STATIC INLINE U32 MLIB_SWAP32(U32 a)
{
    return  (U32)(((U32)((U32)(a<<24U)&(U32)0xff000000UL))
           |((U32)((U32)(a<< 8U)&(U32)0x00ff0000UL))
           |((U32)((U32)(a>> 8U)&(U32)0x0000ff00UL))
           |((U32)((U32)(a>>24U)&(U32)0x000000ffUL)));
}

/*
 * swap the upper and lower halfwords of a 32-bit value (ABCD -> CDAB)
 */
STATIC INLINE U32 MLIB_SWAP32_HALFWORD(U32 a);
STATIC INLINE U32 MLIB_SWAP32_HALFWORD(U32 a)
{
    return  (U32)(((U32)((U32)(a<<16U)&(U32)0xffff0000UL))
           |((U32)((U32)(a>>16U)&(U32)0x0000ffffUL)));
}

/*
 * swap the 2 bytes of the upper halfword and the two bytes of the
 * lower halfword (ABCD -> BADC)
 */
STATIC INLINE U32 MLIB_SWAP32_PARTLY(U32 a);
STATIC INLINE U32 MLIB_SWAP32_PARTLY(U32 a)
{
    return  (U32)(((U32)((U32)(a<< 8U)&(U32)0xff000000UL))
           |((U32)((U32)(a>> 8U)&(U32)0x00ff0000UL))
           |((U32)((U32)(a<< 8U)&(U32)0x0000ff00UL))
           |((U32)((U32)(a>> 8U)&(U32)0x000000ffUL)));
}


#endif /* _FR_1_ERAY_MLIB_SWAP_H_ */

