/**
 * \file
 *
 * \brief AUTOSAR Fr
 *
 * This file contains the implementation of the AUTOSAR
 * module Fr.
 *
 * \author Elektrobit Automotive GmbH, 91058 Erlangen, Germany
 *
 * Copyright 2005 - 2013 Elektrobit Automotive GmbH
 * All rights exclusively reserved for Elektrobit Automotive GmbH,
 * unless expressly agreed to otherwise.
 */

/* !LINKSTO Fr.ASR40.FR074,1 */

/*
 * Misra-C:2004 Deviations:
 *
 * MISRA-1) Deviated Rule: 10.3 (required)
 * Complex expression of underlying type 'unsigned short' may only be cast to narrower integer 
 * type of same signedness, however the destination type is 'unsigned int'.
 *
 * Reason:
 * Hardware access macros make use of complex expressions and perform unnecessary casts, as the 
 * actual target type may be target dependent and thus changes. 
 *
 */

 /******************************************************************************
 Include Section
******************************************************************************/

#include <Fr_1_ERAY_Priv.h> /* !LINKSTO Fr.ASR40.FR462_1,1 */

/******************************************************************************
 Local Macros
******************************************************************************/
/******************************************************************************
 Local Data Types
******************************************************************************/
/******************************************************************************
 Local Data
******************************************************************************/
/******************************************************************************
 Local Functions
******************************************************************************/
/* check whether the API is enabled or not */
#if (FR_1_ERAY_GETSYNCFRAMELIST_API_ENABLE == STD_ON)

/* start code section declaration */
#if (TS_MERGED_COMPILE == STD_OFF)
#define FR_1_ERAY_START_SEC_CODE
#include <MemMap.h> /* !LINKSTO Fr.ASR40.FR112,1 */
#endif /* TS_MERGED_COMPILE */

STATIC INLINE FUNC(uint8,FR_1_ERAY_CODE) Fr_1_ERAY_UpdateListAEven
    (
        uint8 iEvenA,
        uint8 Fr_ListSize,
        FCAL_ERAY_RegSegHType RegESIDn_H0,
        P2VAR(uint16,AUTOMATIC,FR_1_ERAY_APPL_DATA) Fr_ChannelAEvenListPtr
   );

STATIC INLINE FUNC(uint8,FR_1_ERAY_CODE) Fr_1_ERAY_UpdateListAEven
    (
        uint8 iEvenA,
        uint8 Fr_ListSize,
        FCAL_ERAY_RegSegHType RegESIDn_H0,
        P2VAR(uint16,AUTOMATIC,FR_1_ERAY_APPL_DATA) Fr_ChannelAEvenListPtr
   )
{
    /* test if the frame was received in channel A in even cycle */
    if(FCAL_ERAY_TEST_BIT(RegESIDn_H0,ESIDn,RXEA,H0) && (iEvenA < Fr_ListSize))
    {
        /* enter frame ID into output array */
        Fr_ChannelAEvenListPtr[iEvenA] = FCAL_ERAY_GET_BIT(RegESIDn_H0,ESIDn,EID,H0);

        /* increment output array idnex */
        iEvenA++;
    }
    
    return iEvenA;
}

STATIC INLINE FUNC(uint8,FR_1_ERAY_CODE) Fr_1_ERAY_UpdateListBEven
    (
        uint8 iEvenB,
        uint8 Fr_ListSize,
        FCAL_ERAY_RegSegHType RegESIDn_H0,
        P2VAR(uint16,AUTOMATIC,FR_1_ERAY_APPL_DATA) Fr_ChannelBEvenListPtr
   );

STATIC INLINE FUNC(uint8,FR_1_ERAY_CODE) Fr_1_ERAY_UpdateListBEven
    (
        uint8 iEvenB,
        uint8 Fr_ListSize,
        FCAL_ERAY_RegSegHType RegESIDn_H0,
        P2VAR(uint16,AUTOMATIC,FR_1_ERAY_APPL_DATA) Fr_ChannelBEvenListPtr
   )
{
    /* test if the frame was received in channel B in even cycle */
    if(FCAL_ERAY_TEST_BIT(RegESIDn_H0,ESIDn,RXEB,H0) && (iEvenB < Fr_ListSize))
    {
        /* enter frame ID into output array */
        Fr_ChannelBEvenListPtr[iEvenB] =
            FCAL_ERAY_GET_BIT(RegESIDn_H0,ESIDn,EID,H0);

        /* increment output array idnex */
        iEvenB++;
    }
    
    return iEvenB;
}

STATIC INLINE FUNC(uint8,FR_1_ERAY_CODE) Fr_1_ERAY_UpdateListAOdd
    (
        uint8 iOddA,
        uint8 Fr_ListSize,
        FCAL_ERAY_RegSegHType RegOSIDn_H0,
        P2VAR(uint16,AUTOMATIC,FR_1_ERAY_APPL_DATA) Fr_ChannelAOddListPtr
   );

STATIC INLINE FUNC(uint8,FR_1_ERAY_CODE) Fr_1_ERAY_UpdateListAOdd
    (
        uint8 iOddA,
        uint8 Fr_ListSize,
        FCAL_ERAY_RegSegHType RegOSIDn_H0,
        P2VAR(uint16,AUTOMATIC,FR_1_ERAY_APPL_DATA) Fr_ChannelAOddListPtr
   )
{
    if(FCAL_ERAY_TEST_BIT(RegOSIDn_H0,OSIDn,RXOA,H0) && (iOddA < Fr_ListSize))
    {
        /* enter frame ID into output array */
        Fr_ChannelAOddListPtr[iOddA] =
            FCAL_ERAY_GET_BIT(RegOSIDn_H0,OSIDn,OID,H0);

        /* increment output array idnex */
        iOddA++;
    }
    
    return iOddA;
}

STATIC INLINE FUNC(uint8,FR_1_ERAY_CODE) Fr_1_ERAY_UpdateListBOdd
    (
        uint8 iOddB,
        uint8 Fr_ListSize,
        FCAL_ERAY_RegSegHType RegOSIDn_H0,
        P2VAR(uint16,AUTOMATIC,FR_1_ERAY_APPL_DATA) Fr_ChannelBOddListPtr
   );

STATIC INLINE FUNC(uint8,FR_1_ERAY_CODE) Fr_1_ERAY_UpdateListBOdd
    (
        uint8 iOddB,
        uint8 Fr_ListSize,
        FCAL_ERAY_RegSegHType RegOSIDn_H0,
        P2VAR(uint16,AUTOMATIC,FR_1_ERAY_APPL_DATA) Fr_ChannelBOddListPtr
   )
{
    /* test if the frame was received in channel B in odd cycle */
    if(FCAL_ERAY_TEST_BIT(RegOSIDn_H0,OSIDn,RXOB,H0) && (iOddB < Fr_ListSize))
    {
        /* enter frame ID into output array */
        Fr_ChannelBOddListPtr[iOddB] =
            FCAL_ERAY_GET_BIT(RegOSIDn_H0,OSIDn,OID,H0);

        /* increment output array idnex */
        iOddB++;
    }
    
    return iOddB;
}
/**
 * \brief   Returns a list of sync frames.
 *
 * This service writes a list of sync frames observed in the last even/odd communication cycle couple
 * into *Fr_ChannelAEvenListPtr, *Fr_ChannelBEvenListPtr, *Fr_ChannelAOddListPtr and
 * *Fr_ChannelBOddListPtr. The maximum number of sync frames written is determined by Fr_ListSize.
 * Unused array elements are written with value 0, determining that all seen syncframes are reported.
 *
 * \param[in] Fr_CtrlIdx                 FlexRay controller index.
 * \param[in] Fr_ListSize                Size of list passed to the output pointers.
 * \param[out] Fr_ChannelAEvenListPtr    Address to write the list of even sync frames of channel A.
 * \param[out] Fr_ChannelBEvenListPtr    Address to write the list of even sync frames of channel B.
 * \param[out] Fr_ChannelAOddListPtr     Address to write the list of odd sync frames of channel A.
 * \param[out] Fr_ChannelBOddListPtr     Address to write the list of odd sync frames of channel B.
 *
 */
STATIC INLINE FUNC(void,FR_1_ERAY_CODE) Fr_1_ERAY_GetSyncFrameList_Internal
    (
        VAR(uint8,AUTOMATIC) Fr_CtrlIdx,
        VAR(uint8,AUTOMATIC) Fr_ListSize,
        P2VAR(uint16,AUTOMATIC,FR_1_ERAY_APPL_DATA) Fr_ChannelAEvenListPtr,
        P2VAR(uint16,AUTOMATIC,FR_1_ERAY_APPL_DATA) Fr_ChannelBEvenListPtr,
        P2VAR(uint16,AUTOMATIC,FR_1_ERAY_APPL_DATA) Fr_ChannelAOddListPtr,
        P2VAR(uint16,AUTOMATIC,FR_1_ERAY_APPL_DATA) Fr_ChannelBOddListPtr
    );

STATIC INLINE FUNC(void,FR_1_ERAY_CODE) Fr_1_ERAY_GetSyncFrameList_Internal
    (
        VAR(uint8,AUTOMATIC) Fr_CtrlIdx,
        VAR(uint8,AUTOMATIC) Fr_ListSize,
        P2VAR(uint16,AUTOMATIC,FR_1_ERAY_APPL_DATA) Fr_ChannelAEvenListPtr,
        P2VAR(uint16,AUTOMATIC,FR_1_ERAY_APPL_DATA) Fr_ChannelBEvenListPtr,
        P2VAR(uint16,AUTOMATIC,FR_1_ERAY_APPL_DATA) Fr_ChannelAOddListPtr,
        P2VAR(uint16,AUTOMATIC,FR_1_ERAY_APPL_DATA) Fr_ChannelBOddListPtr
    )
{
    /* define FlexRay CC access handle */
    FCAL_ERAY_DefineCCHandle();
    
    /* initialize controller handle */
    FCAL_ERAY_InitCCHandle(FR_1_ERAY_CTRLIDX);

    TS_PARAM_UNUSED(Fr_CtrlIdx);
    
    /* implemented functionality */
    {
        /* indices for writing into output arrays */
        VAR(uint8,AUTOMATIC) iEvenA = 0U;
        VAR(uint8,AUTOMATIC) iEvenB = 0U;
        VAR(uint8,AUTOMATIC) iOddA = 0U;
        VAR(uint8,AUTOMATIC) iOddB = 0U;

        /* main iterator variable, looping through every output array element */
        VAR(uint8_least,AUTOMATIC) i;

        /* copy tables to output parameters */
        for(i = 0U; i < (uint8_least)15U; i++)
        {
            /* read even sync ID list register */
            /* Deviation MISRA-1 */
            CONST(FCAL_ERAY_RegSegHType,AUTOMATIC) RegESIDn_H0 =
                FCAL_ERAY_GET_IDX(
                    FCAL_ERAY_GetCCHandle(),
                    ESIDn,
                    H0,
                    i
                    );

            /* read odd sync ID list register */
            /* Deviation MISRA-1 */
            CONST(FCAL_ERAY_RegSegHType,AUTOMATIC) RegOSIDn_H0 =
                FCAL_ERAY_GET_IDX(
                    FCAL_ERAY_GetCCHandle(),
                    OSIDn,
                    H0,
                    i
                    );

            if(i < Fr_ListSize)
            {
                /* first of all, reset output arrays - (no sync frame received) */
                Fr_ChannelAEvenListPtr[i] = 0U;
                Fr_ChannelBEvenListPtr[i] = 0U;
                Fr_ChannelAOddListPtr[i] = 0U;
                Fr_ChannelBOddListPtr[i] = 0U;
            }

            iEvenA = Fr_1_ERAY_UpdateListAEven(iEvenA,Fr_ListSize,RegESIDn_H0,Fr_ChannelAEvenListPtr);
            iEvenB = Fr_1_ERAY_UpdateListBEven(iEvenB,Fr_ListSize,RegESIDn_H0,Fr_ChannelBEvenListPtr);
            iOddA = Fr_1_ERAY_UpdateListAOdd(iOddA,Fr_ListSize,RegOSIDn_H0,Fr_ChannelAOddListPtr);
            iOddB = Fr_1_ERAY_UpdateListBOdd(iOddB,Fr_ListSize,RegOSIDn_H0,Fr_ChannelBOddListPtr);
            

            if((((iEvenA >= Fr_ListSize) && (iEvenB >= Fr_ListSize)) &&
                 (iOddA >= Fr_ListSize)) && (iOddB >= Fr_ListSize))
            {
                break;
            }
        }
    }
}
/******************************************************************************
 Global Functions
******************************************************************************/


FUNC(Std_ReturnType,FR_1_ERAY_CODE) Fr_1_ERAY_GetSyncFrameList
    (
        VAR(uint8,AUTOMATIC) Fr_CtrlIdx,
        VAR(uint8,AUTOMATIC) Fr_ListSize,
        P2VAR(uint16,AUTOMATIC,FR_1_ERAY_APPL_DATA) Fr_ChannelAEvenListPtr,
        P2VAR(uint16,AUTOMATIC,FR_1_ERAY_APPL_DATA) Fr_ChannelBEvenListPtr,
        P2VAR(uint16,AUTOMATIC,FR_1_ERAY_APPL_DATA) Fr_ChannelAOddListPtr,
        P2VAR(uint16,AUTOMATIC,FR_1_ERAY_APPL_DATA) Fr_ChannelBOddListPtr
    )
{
    Std_ReturnType retval = E_OK;
    
    /* define FlexRay CC access handle */
    FCAL_ERAY_DefineCCHandle();

    /* check if development error detection is enabled */
#if (FR_1_ERAY_DEV_ERROR_DETECT == STD_ON)

    /* check for successfully initialized module */
    /* Report to DET and return Error in case module was not initialized before */
    if (Fr_1_ERAY_InitStatus == FR_1_ERAY_UNINIT)
    {
        FR_1_ERAY_DET_REPORTERROR(FR_1_ERAY_E_NOT_INITIALIZED, FR_1_ERAY_GETSYNCFRAMELIST_SERVICE_ID);
        /* Return function status NOT OK */
        retval = E_NOT_OK;
    }

#if (FR_1_ERAY_SINGLE_CTRL_OPT_ENABLE == STD_ON)

    /* check that controller index is 0 */
    else if (Fr_CtrlIdx != (VAR(uint8,AUTOMATIC)) 0U)
    {
        /* Report to DET */
        FR_1_ERAY_DET_REPORTERROR(FR_1_ERAY_E_INV_CTRL_IDX, FR_1_ERAY_GETSYNCFRAMELIST_SERVICE_ID);

        /* Return function status NOT OK */
        retval = E_NOT_OK;
    }

#endif  /* FR_1_ERAY_SINGLE_CTRL_OPT_ENABLE */

    /* check if controller index is within supported bounds */
    else if ((FR_1_ERAY_CTRLIDX >= FCAL_ERAY_GetNumCC()) ||
        (FCAL_ERAY_IsCCHandleValid(FR_1_ERAY_CTRLIDX) == FALSE))
    {
        /* Report to DET */
        FR_1_ERAY_DET_REPORTERROR(FR_1_ERAY_E_INV_CTRL_IDX, FR_1_ERAY_GETSYNCFRAMELIST_SERVICE_ID);

        /* Return function status NOT OK */
        retval = E_NOT_OK;
    }

    /* check that Fr_ListSize has a valid value */
    else if(Fr_ListSize > 15U)
    {
        /* Report to DET */
        FR_1_ERAY_DET_REPORTERROR(FR_1_ERAY_E_INV_CONFIG, FR_1_ERAY_GETSYNCFRAMELIST_SERVICE_ID);

        /* Return function status NOT OK */
        retval = E_NOT_OK;
    }

    /* check pointer for being valid */
    else if(Fr_ChannelAEvenListPtr == NULL_PTR)
    {
        /* Report to DET */
        FR_1_ERAY_DET_REPORTERROR(FR_1_ERAY_E_INV_POINTER, FR_1_ERAY_GETSYNCFRAMELIST_SERVICE_ID);

        /* Return function status NOT OK */
        retval = E_NOT_OK;
    }

    /* check pointer for being valid */
    else if(Fr_ChannelBEvenListPtr == NULL_PTR)
    {
        /* Report to DET */
        FR_1_ERAY_DET_REPORTERROR(FR_1_ERAY_E_INV_POINTER, FR_1_ERAY_GETSYNCFRAMELIST_SERVICE_ID);

        /* Return function status NOT OK */
        retval = E_NOT_OK;
    }

    /* check pointer for being valid */
    else if(Fr_ChannelAOddListPtr == NULL_PTR)
    {
        /* Report to DET */
        FR_1_ERAY_DET_REPORTERROR(FR_1_ERAY_E_INV_POINTER, FR_1_ERAY_GETSYNCFRAMELIST_SERVICE_ID);

        /* Return function status NOT OK */
        retval = E_NOT_OK;
    }

    /* check pointer for being valid */
    else if(Fr_ChannelBOddListPtr == NULL_PTR)
    {
        /* Report to DET */
        FR_1_ERAY_DET_REPORTERROR(FR_1_ERAY_E_INV_POINTER, FR_1_ERAY_GETSYNCFRAMELIST_SERVICE_ID);

        /* Return function status NOT OK */
        retval = E_NOT_OK;
    }
    else


#else /* FR_1_ERAY_DEV_ERROR_DETECT */

    TS_PARAM_UNUSED(Fr_CtrlIdx);

#endif  /* FR_1_ERAY_DEV_ERROR_DETECT */
    {
        /* initialize controller handle */
        FCAL_ERAY_InitCCHandle(FR_1_ERAY_CTRLIDX);

        /* Check whether access to FlexRay CC is functional */
        if (FCAL_ERAY_IsCCAccessValid(FR_1_ERAY_CTRLIDX) == FALSE)
        {
            /* report hardware error */
            FR_1_ERAY_REPORT_CTRL_TEST_STATUS_FAILED(FR_1_ERAY_GETSYNCFRAMELIST_SERVICE_ID);

            /* Return function status NOT OK */
            retval = E_NOT_OK;
        }
        else
        {
            Fr_1_ERAY_GetSyncFrameList_Internal(
              Fr_CtrlIdx,
              Fr_ListSize,
              Fr_ChannelAEvenListPtr,
              Fr_ChannelBEvenListPtr,
              Fr_ChannelAOddListPtr,
              Fr_ChannelBOddListPtr
              );
        }
    }
    return retval;
}

/* stop code section declaration */
#if (TS_MERGED_COMPILE == STD_OFF)
#define FR_1_ERAY_STOP_SEC_CODE
#include <MemMap.h> /* !LINKSTO Fr.ASR40.FR112,1 */
#endif /* TS_MERGED_COMPILE */

#endif  /* FR_1_ERAY_GETSYNCFRAMELIST_API_ENABLE */

