/**
 * \file
 *
 * \brief AUTOSAR Fr
 *
 * This file contains the implementation of the AUTOSAR
 * module Fr.
 *
 * \author Elektrobit Automotive GmbH, 91058 Erlangen, Germany
 *
 * Copyright 2005 - 2013 Elektrobit Automotive GmbH
 * All rights exclusively reserved for Elektrobit Automotive GmbH,
 * unless expressly agreed to otherwise.
 */



/*
 * Misra-C:2004 Deviations:
 *
 * MISRA-1) Deviated Rule: 14.1 (required)
 * There shall be no unreachable code.
 *
 * Reason:
 * Whether this code-block is used depends on the particular implementation of
 * the macro which depends on the hardware the driver is ported to.
 *
 * MISRA-3) Deviated Rule: 19.7 (advisory)
 * A function shall be used in preference to a function-like macro.
 *
 * Reason:
 * since this is a function like macro, contained expression might expand to constant expressions
 * if constant arguments are passed. In order to maintain modularisiation it shall be allowed to pass
 * constant arguments to this macro.
 *
 */

/* !LINKSTO Fr.ASR40.FR074,1 */

/******************************************************************************
 Include Section
******************************************************************************/

#include <Fr_1_ERAY_Priv.h> /* !LINKSTO Fr.ASR40.FR462_1,1 */

/******************************************************************************
 Local Macros
******************************************************************************/
/******************************************************************************
 Local Data Types
******************************************************************************/
/******************************************************************************
 Local Data
******************************************************************************/
/******************************************************************************
 Local Functions
******************************************************************************/
/* start code section declaration */
#if (TS_MERGED_COMPILE == STD_OFF)
#define FR_1_ERAY_START_SEC_CODE
#include <MemMap.h> /* !LINKSTO Fr.ASR40.FR112,1 */
#endif /* TS_MERGED_COMPILE */


/**
 * \brief   Initializes all FlexRay CC buffers contained in configuration.
 *
 * \param Fr_CtrlIdx (in)            Index of FlexRay controller to initialize.
 *
 * \note    This function can be called in POC-state 'ready' only.
 *
 * \retval  E_OK               Function serviced successfully.
 * \retval  E_NOT_OK           Function execution failed.
 */
STATIC INLINE FUNC(Std_ReturnType,FR_1_ERAY_CODE) Fr_1_ERAY_ConfigAllBuffers
    (
        VAR(uint8,AUTOMATIC) Fr_CtrlIdx
    );


/**
 * \brief   Waits while the FlexRay CC Input Shadow Buffer is busy
 *
 * \param Fr_CtrlIdx (in)            Index of FlexRay controller.
 *
 * \retval  E_OK               Function serviced successfully.
 * \retval  E_NOT_OK           Function execution failed (timeout while waiting).
 */
STATIC FUNC(Std_ReturnType,FR_1_ERAY_CODE) Fr_1_ERAY_WaitWhileIBFShadowBusy
    (
        uint8 Fr_CtrlIdx
    );


/**
 * \brief   Configures all buffers of FlexRay CC
 *
 * \param Fr_CtrlIdx (in)            Index of FlexRay controller.
 *
 * \retval  E_OK               Function serviced successfully.
 * \retval  E_NOT_OK           Function execution failed.
 */
STATIC INLINE FUNC(Std_ReturnType,FR_1_ERAY_CODE) Fr_1_ERAY_ConfigAllBuffers
    (
        VAR(uint8,AUTOMATIC) Fr_CtrlIdx
    )
{
    Std_ReturnType retval = E_OK;
    
    /* define FlexRay CC access handle */
    FCAL_ERAY_DefineCCHandle();

/* if single controller optimization is enabled - parameter is not used */
#if (FR_1_ERAY_SINGLE_CTRL_OPT_ENABLE == STD_ON)

    TS_PARAM_UNUSED(Fr_CtrlIdx);

#endif /* FR_1_ERAY_SINGLE_CTRL_OPT_ENABLE */

    /* initialize controller handle */
    FCAL_ERAY_InitCCHandle(FR_1_ERAY_CTRLIDX);

    /* implemented functionality */
    {
        CONSTP2CONST(Fr_1_ERAY_CtrlCfgType,AUTOMATIC,FR_1_ERAY_APPL_CONST) pCtrlCfg = 
             &FR_1_ERAY_GET_CONFIG_ADDR
            (
                Fr_1_ERAY_CtrlCfgType,
                gFr_1_ERAY_ConfigPtr->pCtrlCfg
            )[FR_1_ERAY_CTRLIDX];
            
        /* check for key-slot initialization */
        {

            CONST(uint32,AUTOMATIC) nKeySlotWRHS1 =
                pCtrlCfg->nKeySlot_WRHS1;

            /* check if key-slot must be configured extra */
            if(nKeySlotWRHS1 != 0x0U)
            {
                /* configure key slot */
                /* key slot is always buffer 0 */
                /* PLC is set to 0 */
                /* Data Pointer is set to 0 (is never used, since PLC = 0) */
                /* Deviation MISRA-3, MISRA-1 <+4> */
                retval = Fr_1_ERAY_ConfigBuffer(FR_1_ERAY_CTRLIDX,
                                       0U,
                                       nKeySlotWRHS1,
                                       pCtrlCfg->nKeySlot_CRC,
                                       0U,
                                       FR_1_ERAY_CONTROLLERINIT_SERVICE_ID);

            }
        }

        /* now configure all other LPdus */
        if(retval == E_OK)
        {
            /* lopp counter */
            VAR(uint16_least,AUTOMATIC) Fr_LPduIdx;

            /* loop over all configured LPdus */
            for(Fr_LPduIdx = 0;
                (Fr_LPduIdx < pCtrlCfg->nNumLPdus) && (retval == E_OK);
                Fr_LPduIdx++)
            {

                /* get buffer configuration data structure */
                CONSTP2CONST(Fr_1_ERAY_BufferCfgType,AUTOMATIC,FR_1_ERAY_APPL_CONST) pLPdu =
                    &FR_1_ERAY_GET_CONFIG_ADDR
                    (
                        Fr_1_ERAY_BufferCfgType,
                        pCtrlCfg->pLPduCfg
                    )[Fr_LPduIdx];

                /* get config parameter nFIDExt2 into local variable (since used several times) */
                CONST(uint16,AUTOMATIC) nFIDExt2 = pLPdu->nFID_Ext2;

                /* variables holding the buffer configuration register values */
                /* ERAY WRHS1 register variable */
                VAR(uint32,AUTOMATIC) RegWRHS1_W0;

                /* ERAY WRHS2 register variable */
                VAR(uint32,AUTOMATIC) RegWRHS2_W0;

                /* ERAY WRHS3 register variable */
                VAR(uint32,AUTOMATIC) RegWRHS3_W0;

                /* check whether this buffer shall be initialized or not */
                if(FR_1_ERAY_GET_NOINIT(nFIDExt2) == 0x0U)
                {
                    /* configure this buffer only if NoInit-bit is not set */

                    /* is this a normal buffer (1:1 mapping to LPdu) or a dynamically reconfigurable buffer? */
                    if(FR_1_ERAY_GET_LPDUMODE(nFIDExt2) == FR_1_ERAY_LPDUMODE_STATIC_NORMAL)
                    {
                        /* yes it is - load init values from config */
                        CONST(uint16,AUTOMATIC) nCRCExt1 = pLPdu->nCRC_Ext1;

                        /* build registers out of configuration values */
                        RegWRHS1_W0 = FR_1_ERAY_GET_WRHS1(nCRCExt1,nFIDExt2,pLPdu->nCycle);
                        RegWRHS2_W0 = FR_1_ERAY_GET_WRHS2(nCRCExt1,(((uint32)pLPdu->nPayloadLength + 1U) / 2U));
                        RegWRHS3_W0 = FR_1_ERAY_GET_WRHS3(pLPdu->nDataPointer);
                    }
                    else
                    {
                        /* no it isn't - disable buffer, since it must be reconfigured anyway before usage */
                        RegWRHS1_W0 = 0U;
                        RegWRHS2_W0 = 0U;
                        RegWRHS3_W0 = 0U;
                    }

                    /* finally, initialize the message buffer */
                    retval = Fr_1_ERAY_ConfigBuffer(FR_1_ERAY_CTRLIDX,
                                           pLPdu->nBufferCfgIdx,
                                           RegWRHS1_W0,
                                           RegWRHS2_W0,
                                           RegWRHS3_W0,
                                           FR_1_ERAY_CONTROLLERINIT_SERVICE_ID);

                }

            }
        }
    }

    /* Return function status OK */
    return retval;
}


STATIC FUNC(Std_ReturnType,FR_1_ERAY_CODE) Fr_1_ERAY_WaitWhileIBFShadowBusy
    (
        uint8 Fr_CtrlIdx
    )
{
    Std_ReturnType retval = E_OK;
    
    /* define FlexRay CC access handle */
    FCAL_ERAY_DefineCCHandle();

    /* depending on hardware port, Fr_CtrlIdx is not used within this function */
    TS_PARAM_UNUSED(Fr_CtrlIdx);

    /* initialize controller handle */
    FCAL_ERAY_InitCCHandle(FR_1_ERAY_CTRLIDX);

    {
        CONST(FCAL_ERAY_RegSegWType,AUTOMATIC) RegOBCR_B1 =
            FCAL_ERAY_GET(
                FCAL_ERAY_GetCCHandle(),
                OBCR,
                B1
                );
        VAR(FCAL_ERAY_RegSegBType,AUTOMATIC) RegIBCR_B3 = 0U;
        volatile VAR(uint16_least,AUTOMATIC) nTimeout =
            (uint16_least) FR_1_ERAY_BUFFER_TRANSFER_TIMEOUT;

        /* check output buffer */
        if (FCAL_ERAY_TEST_BIT(RegOBCR_B1,OBCR,OBSYS,B1))
        {
            /* report hardware error */
            FR_1_ERAY_REPORT_CTRL_TEST_STATUS_FAILED(FR_1_ERAY_CONTROLLERINIT_SERVICE_ID);

            retval = E_NOT_OK;
        }
        else
        {

            /* check input buffer */
            do
            {
                if (nTimeout == (uint16_least) 0U)
                {
                    /* report hardware error */
                    FR_1_ERAY_REPORT_CTRL_TEST_STATUS_FAILED(FR_1_ERAY_CONTROLLERINIT_SERVICE_ID);
                    retval = E_NOT_OK;
                }
                else
                {
                    RegIBCR_B3 = FCAL_ERAY_GET(
                        FCAL_ERAY_GetCCHandle(),
                        IBCR,
                        B3
                        );
                    nTimeout--;
                }
            } while ((FCAL_ERAY_TEST_BIT(RegIBCR_B3,IBCR,IBSYS,B3)) && (retval == E_OK));
        }
    }

    return retval;

}


STATIC INLINE FUNC(void,FR_1_ERAY_CODE) Fr_1_ERAY_ReadyUnlock
    (
        VAR(uint8,AUTOMATIC) Fr_CtrlIdx
    );

STATIC INLINE FUNC(void,FR_1_ERAY_CODE) Fr_1_ERAY_ReadyUnlock
    (
        VAR(uint8,AUTOMATIC) Fr_CtrlIdx
    )
{
        /* define FlexRay CC access handle */
    FCAL_ERAY_DefineCCHandle();

    /* initialize controller handle */
    FCAL_ERAY_InitCCHandle(FR_1_ERAY_CTRLIDX);
    
    TS_PARAM_UNUSED(Fr_CtrlIdx);

    /* unlock CC to change to ready state */
    /* unlock step 1 */
    FCAL_ERAY_SETDC(
        FCAL_ERAY_GetCCHandle(),
        LCK,
        B0,
        FCAL_ERAY_ALIGN_VALUE(0xCEU,B0)
        );

    /* unlock step 2 */
    FCAL_ERAY_SETDC(
        FCAL_ERAY_GetCCHandle(),
        LCK,
        B0,
        FCAL_ERAY_ALIGN_VALUE(0x31U,B0)
        );
}

STATIC INLINE FUNC(Std_ReturnType,FR_1_ERAY_CODE) Fr_1_ERAY_InitProtocolRegisters
    (
        VAR(uint8,AUTOMATIC) Fr_CtrlIdx
    );

STATIC INLINE FUNC(Std_ReturnType,FR_1_ERAY_CODE) Fr_1_ERAY_InitProtocolRegisters
    (
        VAR(uint8,AUTOMATIC) Fr_CtrlIdx
    )
{
    Std_ReturnType retval = E_OK;
    
        /* define FlexRay CC access handle */
    FCAL_ERAY_DefineCCHandle();

    /* initialize controller handle */
    FCAL_ERAY_InitCCHandle(FR_1_ERAY_CTRLIDX);
    
    TS_PARAM_UNUSED(Fr_CtrlIdx);
    
    {
        /* get pointer to controller configuration */
        CONSTP2CONST(Fr_1_ERAY_CtrlCfgType,AUTOMATIC,FR_1_ERAY_APPL_CONST) pCtrlCfg = 
             &FR_1_ERAY_GET_CONFIG_ADDR
            (
                Fr_1_ERAY_CtrlCfgType,
                gFr_1_ERAY_ConfigPtr->pCtrlCfg
            )[FR_1_ERAY_CTRLIDX];

        /* get CHI-Value-Array pointer */
        P2CONST(uint32,AUTOMATIC,FR_1_ERAY_APPL_CONST) pCHIVal =
            FR_1_ERAY_GET_CONFIG_ADDR
            (
                uint32,
                pCtrlCfg->pCHIValue
            );

        /* get CHI-Offset-Array pointer */
        P2CONST(uint16,AUTOMATIC,FR_1_ERAY_APPL_CONST) pCHIOff =
            FR_1_ERAY_GET_CONFIG_ADDR
            (
                uint16,
                pCtrlCfg->pCHIOffset
            );

        /* register index variable */
        VAR(uint8_least,AUTOMATIC) iCHI;

        /* loop over all register pairs */
        for(iCHI = 0; iCHI < pCtrlCfg->nCHICfgSize; iCHI++)
        {
            /*
             * write chi value into FlexRay CC
             * disabling and acknowledge of interrupts is ensured by
             * respective configuration register values
             */
            FCAL_ERAY_SET_U32(FCAL_ERAY_GetCCHandle(), *pCHIOff, *pCHIVal);
            pCHIOff = &pCHIOff[1];
            pCHIVal = &pCHIVal[1];
        }

        /* set porting specific register values */

        /* Set register EILS */
        FCAL_ERAY_SET(
            FCAL_ERAY_GetCCHandle(),
            EILS,
            W0,
            (FCAL_ERAY_RegSegBType)FR_1_ERAY_REG_EILS_VALUE);

        /* Set register SILS */
        FCAL_ERAY_SET(
            FCAL_ERAY_GetCCHandle(),
            SILS,
            W0,
            (FCAL_ERAY_RegSegBType)FR_1_ERAY_REG_SILS_VALUE);

        /* Set register ILE */
        FCAL_ERAY_SET(
            FCAL_ERAY_GetCCHandle(),
            ILE,
            W0,
            (FCAL_ERAY_RegSegBType)FR_1_ERAY_REG_ILE_VALUE);


        /* check for busy bit and wait if CMD is busy (with timeout) */
        retval = Fr_1_ERAY_WaitWhileCmdBusyStandard(FR_1_ERAY_CTRLIDX, FR_1_ERAY_CONTROLLERINIT_SERVICE_ID);
    }
    return retval;
}

STATIC INLINE FUNC(Std_ReturnType,FR_1_ERAY_CODE) Fr_1_ERAY_ControllerInit_Internal
    (
        VAR(uint8,AUTOMATIC) Fr_CtrlIdx
    );

STATIC INLINE FUNC(Std_ReturnType,FR_1_ERAY_CODE) Fr_1_ERAY_ControllerInit_Internal
    (
        VAR(uint8,AUTOMATIC) Fr_CtrlIdx
    )
{
    Std_ReturnType retval = E_OK;
    
        /* define FlexRay CC access handle */
    FCAL_ERAY_DefineCCHandle();

    /* initialize controller handle */
    FCAL_ERAY_InitCCHandle(FR_1_ERAY_CTRLIDX);
    
    TS_PARAM_UNUSED(Fr_CtrlIdx);

    {    
        /* check for busy bit and wait if CMD is busy (with timeout)  */
        /* since this is the first API function invoked - perform a longer */
        /* timeout in case a reset was performed before */
        /* therefore no optimization is possible here */
        if(Fr_1_ERAY_WaitWhileCmdBusyReset(FR_1_ERAY_CTRLIDX, FR_1_ERAY_CONTROLLERINIT_SERVICE_ID) != E_OK)
        {
            retval = E_NOT_OK;
        }
        else if(Fr_1_ERAY_CHICommand(FR_1_ERAY_CTRLIDX, FCAL_ERAY_SUCC1_CMD_FREEZE, FR_1_ERAY_CONTROLLERINIT_SERVICE_ID) != E_OK)
        {
            retval = E_NOT_OK;
        }        
    /* check for busy bit only if optimization is disabled */
#if (FR_1_ERAY_CMD_BUSY_CHECK_NONIMMEDIATE_OPT_ENABLE == STD_OFF)

        /* check for busy bit and wait if CMD is busy (with timeout) */
        else if(Fr_1_ERAY_WaitWhileCmdBusyStandard(FR_1_ERAY_CTRLIDX, FR_1_ERAY_CONTROLLERINIT_SERVICE_ID) != E_OK)
        {
            retval = E_NOT_OK;
        }

#endif /* FR_1_ERAY_BUSY_NONIMMEDIATE_OPTIMIZATION */

        /* set CC into default config state */
        else if(Fr_1_ERAY_CHICommand(FR_1_ERAY_CTRLIDX, FCAL_ERAY_SUCC1_CMD_CONFIG, FR_1_ERAY_CONTROLLERINIT_SERVICE_ID) != E_OK)
        {
            retval = E_NOT_OK;
        }

        else if(Fr_1_ERAY_WaitWhileCmdBusyStandard(FR_1_ERAY_CTRLIDX, FR_1_ERAY_CONTROLLERINIT_SERVICE_ID) != E_OK)
        {
            retval = E_NOT_OK;
        }

        /*
         * set CC into config state, write CONFIG command to SUCC1->CMD bit
         * field while conserving all other bits
         */
        else if(Fr_1_ERAY_CHICommand(FR_1_ERAY_CTRLIDX, FCAL_ERAY_SUCC1_CMD_CONFIG, FR_1_ERAY_CONTROLLERINIT_SERVICE_ID) != E_OK)
        {
            retval = E_NOT_OK;
        }

        /* now the ERAY is in config state */

        /*
         * before asserting CLEAR_RAMS, it should be checked that no
         * transfer from/to message RAM is ongoing.
         * for the input buffer, this is done with busy waiting (with timeout)
         * until the IBSYS bit is cleared.
         */
        else if(Fr_1_ERAY_WaitWhileIBFShadowBusy(FR_1_ERAY_CTRLIDX) != E_OK)
        {
            retval = E_NOT_OK;
        }

        /* check for busy bit and wait if CMD is busy (with timeout) */
        else if(Fr_1_ERAY_WaitWhileCmdBusyStandard(FR_1_ERAY_CTRLIDX, FR_1_ERAY_CONTROLLERINIT_SERVICE_ID) != E_OK)
        {
            retval = E_NOT_OK;
        }

        /*
         * clear message RAM to ensure that no transmission requests and no
         * reception indications are pending, write CLEAR_RAMS command to
         * SUCC1->CMD bit field while conserving all other bits
         */
        else if(Fr_1_ERAY_CHICommand(FR_1_ERAY_CTRLIDX, FCAL_ERAY_SUCC1_CMD_CLEAR_RAMS, FR_1_ERAY_CONTROLLERINIT_SERVICE_ID) != E_OK)
        {
            retval = E_NOT_OK;
        }
        else if(Fr_1_ERAY_InitProtocolRegisters(FR_1_ERAY_CTRLIDX) != E_OK)
        {
            retval = E_NOT_OK;
        }
        else if(Fr_1_ERAY_ConfigAllBuffers(FR_1_ERAY_CTRLIDX) != E_OK)
        {
            retval = E_NOT_OK;
        }
        else
        {
            Fr_1_ERAY_ReadyUnlock(FR_1_ERAY_CTRLIDX);
    
        /* check for busy bit only if optimization is disabled */
    #if (FR_1_ERAY_CMD_BUSY_CHECK_NONIMMEDIATE_OPT_ENABLE == STD_OFF)
        
            /* check for busy bit and wait if CMD is busy (with timeout) */
            if(Fr_1_ERAY_WaitWhileCmdBusyStandard(FR_1_ERAY_CTRLIDX, FR_1_ERAY_CONTROLLERINIT_SERVICE_ID) != E_OK)
            {
              retval = E_NOT_OK;
            }
            else
    #endif /* FR_1_ERAY_BUSY_NONIMMEDIATE_OPTIMIZATION */
    
            {
                retval = Fr_1_ERAY_CHICommandPreserveOtherBits(FR_1_ERAY_CTRLIDX, FCAL_ERAY_SUCC1_CMD_READY, FR_1_ERAY_CONTROLLERINIT_SERVICE_ID);
            }
        }
    }
    
    return retval;
}

/******************************************************************************
 Global Data
******************************************************************************/
/******************************************************************************
 Global Functions
******************************************************************************/

TS_MOD_PRIV_DEFN FUNC(Std_ReturnType,FR_1_ERAY_CODE) Fr_1_ERAY_CHICommandPreserveOtherBits
    (
        VAR(uint8,AUTOMATIC) Fr_CtrlIdx,
        VAR(uint8,AUTOMATIC) Fr_CHICmd,
        VAR(uint8,AUTOMATIC) Fr_ServiceId
    )
{
    Std_ReturnType retval = E_OK;
    
    /* define FlexRay CC access handle */
    FCAL_ERAY_DefineCCHandle();

    /* depending on hardware port, Fr_CtrlIdx is not used within this function */
    TS_PARAM_UNUSED(Fr_CtrlIdx);
    TS_PARAM_UNUSED(Fr_ServiceId);
    
    /* initialize controller handle */
    FCAL_ERAY_InitCCHandle(FR_1_ERAY_CTRLIDX);

    /* write READY command to SUCC1->CMD bit while conserving all other bits */
    FCAL_ERAY_SET(
        FCAL_ERAY_GetCCHandle(),
        SUCC1,
        B0,
        Fr_CHICmd
        );

    /* check for accepted command */
#if (FR_1_ERAY_CMD_ACCEPTED_CHECK_ENABLE == STD_ON)

    {
        CONST(FCAL_ERAY_RegSegBType,AUTOMATIC) RegSUCC1_B0 =
            FCAL_ERAY_GET(
                FCAL_ERAY_GetCCHandle(),
                SUCC1,
                B0
                );

        if (FCAL_ERAY_GET_BIT(RegSUCC1_B0,SUCC1,CMD,B0) == 0x0)
        {
            FR_1_ERAY_REPORT_CTRL_TEST_STATUS_FAILED(Fr_ServiceId);
            retval = E_NOT_OK;
        }
    }

#endif /* FR_1_ERAY_CMD_ACCEPTED_CHECK_ENABLE */

    return retval;  
}


TS_MOD_PRIV_DEFN FUNC(Std_ReturnType,FR_1_ERAY_CODE) Fr_1_ERAY_CHICommand
    (
        VAR(uint8,AUTOMATIC) Fr_CtrlIdx,
        VAR(uint8,AUTOMATIC) Fr_CHICmd,
        VAR(uint8,AUTOMATIC) Fr_ServiceId
    )
{
    Std_ReturnType retval = E_OK;
    /* define FlexRay CC access handle */
    FCAL_ERAY_DefineCCHandle();

    /* depending on hardware port, Fr_CtrlIdx is not used within this function */
    TS_PARAM_UNUSED(Fr_CtrlIdx);
    TS_PARAM_UNUSED(Fr_ServiceId);

    /* initialize controller handle */
    FCAL_ERAY_InitCCHandle(FR_1_ERAY_CTRLIDX);

    /* write READY command to SUCC1->CMD bit while conserving all other bits */
    FCAL_ERAY_SETDC(
        FCAL_ERAY_GetCCHandle(),
        SUCC1,
        B0,
        Fr_CHICmd
        );

    /* check for accepted command */
#if (FR_1_ERAY_CMD_ACCEPTED_CHECK_ENABLE == STD_ON)

    {
        CONST(FCAL_ERAY_RegSegBType,AUTOMATIC) RegSUCC1_B0 =
            FCAL_ERAY_GET(
                FCAL_ERAY_GetCCHandle(),
                SUCC1,
                B0
                );

        if (FCAL_ERAY_GET_BIT(RegSUCC1_B0,SUCC1,CMD,B0) == 0x0)
        {
            FR_1_ERAY_REPORT_CTRL_TEST_STATUS_FAILED(Fr_ServiceId);
            retval = E_NOT_OK;
        }
    }

#endif /* FR_1_ERAY_CMD_ACCEPTED_CHECK_ENABLE */

    return retval;  
}

#if (FR_1_ERAY_CMD_BUSY_CHECK_TIMEOUT > 0U) || (FR_1_ERAY_CMD_BUSY_CHECK_RESET_TIMEOUT > 0U)

TS_MOD_PRIV_DEFN FUNC(Std_ReturnType,FR_1_ERAY_CODE) Fr_1_ERAY_WaitWhileCmdBusy
    (
        uint8 Fr_CtrlIdx,
        uint16 Fr_Timeout,
        uint8 Fr_ApiId
    )
{
    Std_ReturnType retval = E_OK;
    /* define FlexRay CC access handle */
    FCAL_ERAY_DefineCCHandle();

    /* depending on hardware port, Fr_CtrlIdx is not used within this function */
    TS_PARAM_UNUSED(Fr_CtrlIdx);
    TS_PARAM_UNUSED(Fr_ApiId);

    /* initialize controller handle */
    FCAL_ERAY_InitCCHandle(FR_1_ERAY_CTRLIDX);

    {
        VAR(FCAL_ERAY_RegSegBType,AUTOMATIC) RegSUCC1_B0 = 0U;
        volatile VAR(uint16_least,AUTOMATIC) nTimeout =
            (uint16_least) Fr_Timeout;
        do
        {
            if (nTimeout == (uint16_least) 0U)
            {
                FR_1_ERAY_REPORT_CTRL_TEST_STATUS_FAILED(Fr_ApiId);
                retval = E_NOT_OK;
            }
            else
            {
                RegSUCC1_B0 = FCAL_ERAY_GET(
                    FCAL_ERAY_GetCCHandle(),
                    SUCC1,
                    B0
                    );
                nTimeout--;
            }
        } while ((FCAL_ERAY_TEST_BIT(RegSUCC1_B0,SUCC1,PBSY,B0)) && (retval == E_OK));
    }

    return retval;
}

#endif /* (FR_1_ERAY_CMD_BUSY_CHECK_TIMEOUT > 0U) || (FR_1_ERAY_CMD_BUSY_CHECK_RESET_TIMEOUT > 0U) */


#if (FR_1_ERAY_BUFFER_TRANSFER_TIMEOUT > 0U)

TS_MOD_PRIV_DEFN FUNC(Std_ReturnType,FR_1_ERAY_CODE) Fr_1_ERAY_WaitWhileIBFBusy
    (
        uint8 Fr_CtrlIdx,
        uint8 Fr_ApiId
    )
{
    Std_ReturnType retval = E_OK;
    /* define FlexRay CC access handle */
    FCAL_ERAY_DefineCCHandle();

    /* depending on hardware port, Fr_CtrlIdx is not used within this function */
    TS_PARAM_UNUSED(Fr_CtrlIdx);
    TS_PARAM_UNUSED(Fr_ApiId);

    /* initialize controller handle */
    FCAL_ERAY_InitCCHandle(FR_1_ERAY_CTRLIDX);

    {
        VAR(FCAL_ERAY_RegSegBType,AUTOMATIC) RegIBCR_B1 = 0U;
        volatile VAR(uint16_least,AUTOMATIC) nTimeout =
            (uint16_least) FR_1_ERAY_BUFFER_TRANSFER_TIMEOUT;
        do
        {
            if (nTimeout == (uint16_least) 0U)
            {
                FR_1_ERAY_REPORT_CTRL_TEST_STATUS_FAILED(Fr_ApiId);
                retval = E_NOT_OK;
            }
            else
            {
                RegIBCR_B1 = FCAL_ERAY_GET(
                    FCAL_ERAY_GetCCHandle(),
                    IBCR,
                    B1
                    );
                nTimeout--;
            }
        } while ((FCAL_ERAY_TEST_BIT(RegIBCR_B1,IBCR,IBSYH,B1)) && (retval == E_OK));
    }

    return retval;

}


TS_MOD_PRIV_DEFN FUNC(Std_ReturnType,FR_1_ERAY_CODE) Fr_1_ERAY_WaitWhileOBFBusy
    (
        uint8 Fr_CtrlIdx,
        uint8 Fr_ApiId
    )
{
    Std_ReturnType retval = E_OK;
    /* define FlexRay CC access handle */
    FCAL_ERAY_DefineCCHandle();

    /* depending on hardware port, Fr_CtrlIdx is not used within this function */
    TS_PARAM_UNUSED(Fr_CtrlIdx);
    TS_PARAM_UNUSED(Fr_ApiId);

    /* initialize controller handle */
    FCAL_ERAY_InitCCHandle(FR_1_ERAY_CTRLIDX);

    {
        VAR(FCAL_ERAY_RegSegBType,AUTOMATIC) RegOBCR_B1 = 0U;
        volatile VAR(uint16_least,AUTOMATIC) nTimeout =
            (uint16_least) FR_1_ERAY_BUFFER_TRANSFER_TIMEOUT;
        do
        {
            if (nTimeout == (uint16_least) 0U)
            {
                FR_1_ERAY_REPORT_CTRL_TEST_STATUS_FAILED(Fr_ApiId);
                retval = E_NOT_OK;
            }
            else
            {
                RegOBCR_B1 = FCAL_ERAY_GET(
                    FCAL_ERAY_GetCCHandle(),
                    OBCR,
                    B1
                    );
                nTimeout--;
            }
        } while ((FCAL_ERAY_TEST_BIT(RegOBCR_B1,OBCR,OBSYS,B1)) && (retval == E_OK));
    }

    return retval;

}
#endif /* #if (FR_1_ERAY_BUFFER_TRANSFER_TIMEOUT > 0U) */


TS_MOD_PRIV_DEFN FUNC(Std_ReturnType,FR_1_ERAY_CODE) Fr_1_ERAY_ConfigBuffer
    (
        uint8  Fr_CtrlIdx,
        uint8  nBufIdx,
        uint32 nWRHS1,
        uint32 nWRHS2,
        uint32 nWRHS3,
        uint8 ApiId
    )
{
    Std_ReturnType retval = E_OK;
    /* define FlexRay CC access handle */
    FCAL_ERAY_DefineCCHandle();

    /* depending on hardware port, Fr_CtrlIdx is not used within this function */
    TS_PARAM_UNUSED(Fr_CtrlIdx);
    TS_PARAM_UNUSED(ApiId);

    /* initialize controller handle */
    FCAL_ERAY_InitCCHandle(FR_1_ERAY_CTRLIDX);

    /* check for ready input buffer */
    if(Fr_1_ERAY_WaitWhileIBFBusy(FR_1_ERAY_CTRLIDX,ApiId) != E_OK)
    {
        retval = E_NOT_OK;
    }
    else
    {

        /* write register WRHS1 to ERAY */
        FCAL_ERAY_SET(
            FCAL_ERAY_GetCCHandle(),
            WRHS1,
            W0,
            nWRHS1
            );

        /* write register WRHS2 to ERAY */
        FCAL_ERAY_SET(
            FCAL_ERAY_GetCCHandle(),
            WRHS2,
            W0,
            nWRHS2
            );

        /* write register WRHS3 to ERAY */
        FCAL_ERAY_SET(
            FCAL_ERAY_GetCCHandle(),
            WRHS3,
            W0,
            nWRHS3
            );

        /* set "load header section" command */
        FCAL_ERAY_SETDC(
            FCAL_ERAY_GetCCHandle(),
            IBCM,
            B0,
            FCAL_ERAY_ALIGN_VALUE(FCAL_ERAY_IBCM_LHSH_W0_MASK,B0)
            );

        /* start buffer transfer */
        FCAL_ERAY_SETDC(
            FCAL_ERAY_GetCCHandle(),
            IBCR,
            B0,
            FCAL_ERAY_ALIGN_VALUE(nBufIdx,B0)
            );
    }
    return retval;
}


/**
 * \brief   Initializes the FlexRay CC low level parameters and all buffers according to
 *          the configuration passed at Fr_1_ERAY_Init().
 *
 * \param Fr_CtrlIdx (in)            Index of FlexRay controller to initialize.
 *
 * \note    This function can be called in any POC-state.
 * \note    This function leaves the FlexRay CC in POC-state "ready".
 *
 * \retval  E_OK               Function serviced successfully.
 * \retval  E_NOT_OK           Function execution failed.
 */

FUNC(Std_ReturnType,FR_1_ERAY_CODE) Fr_1_ERAY_ControllerInit
    (
        VAR(uint8,AUTOMATIC) Fr_CtrlIdx
    )
{
    Std_ReturnType retval = E_OK;
    /* define FlexRay CC access handle */
    FCAL_ERAY_DefineCCHandle();

/* check if development error detection is enabled */
#if (FR_1_ERAY_DEV_ERROR_DETECT == STD_ON)

    /* check for successfully initialized module */
    /* Report to DET and return Error in case module was not initialized before */
    if (Fr_1_ERAY_InitStatus == FR_1_ERAY_UNINIT)
    {
        FR_1_ERAY_DET_REPORTERROR(FR_1_ERAY_E_NOT_INITIALIZED, FR_1_ERAY_CONTROLLERINIT_SERVICE_ID);
        /* Return function status NOT OK */
        retval = E_NOT_OK;
    }    

#if (FR_1_ERAY_SINGLE_CTRL_OPT_ENABLE == STD_ON)

    /* check that controller index is 0 */
    else if (Fr_CtrlIdx != (VAR(uint8,AUTOMATIC)) 0U)
    {
        /* Report to DET */
        FR_1_ERAY_DET_REPORTERROR(FR_1_ERAY_E_INV_CTRL_IDX, FR_1_ERAY_CONTROLLERINIT_SERVICE_ID);

        /* Return function status NOT OK */
        retval = E_NOT_OK;
    }
    
#endif  /* FR_1_ERAY_SINGLE_CTRL_OPT_ENABLE */

    /* check if controller index is within supported bounds */
    else if((FR_1_ERAY_CTRLIDX >= FCAL_ERAY_GetNumCC()) ||
       (FCAL_ERAY_IsCCHandleValid(FR_1_ERAY_CTRLIDX) == FALSE))
    {

        /* Report to DET */
        FR_1_ERAY_DET_REPORTERROR(FR_1_ERAY_E_INV_CTRL_IDX, FR_1_ERAY_CONTROLLERINIT_SERVICE_ID);

        /* Return function status NOT OK */
        retval = E_NOT_OK;
    }

    /* check whether configuration has an entry for this FlexRay CC Idx */
    else if(FR_1_ERAY_CTRLIDX >= gFr_1_ERAY_ConfigPtr->nNumCtrl)
    {

        /* Report to DET */
        FR_1_ERAY_DET_REPORTERROR(FR_1_ERAY_E_INV_CTRL_IDX, FR_1_ERAY_CONTROLLERINIT_SERVICE_ID);

        /* Return function status NOT OK */
        retval = E_NOT_OK;

    }
    else
    
#else /* FR_1_ERAY_DEV_ERROR_DETECT */

    TS_PARAM_UNUSED(Fr_CtrlIdx);

#endif  /* FR_1_ERAY_DEV_ERROR_DETECT */
    {
        /* initialize controller handle */
        FCAL_ERAY_InitCCHandle(FR_1_ERAY_CTRLIDX);

        /* Check whether access to FlexRay CC is functional */
        if(FCAL_ERAY_IsCCAccessValid(FR_1_ERAY_CTRLIDX) == FALSE)
        {
            /* report hardware error */
            FR_1_ERAY_REPORT_CTRL_TEST_STATUS_FAILED(FR_1_ERAY_CONTROLLERINIT_SERVICE_ID);

            /* Return function status NOT OK */
            retval = E_NOT_OK;
        }
        else
        {
            retval = Fr_1_ERAY_ControllerInit_Internal(FR_1_ERAY_CTRLIDX);
        }
    }
    
    return retval;
}


/* stop code section declaration */
#if (TS_MERGED_COMPILE == STD_OFF)
#define FR_1_ERAY_STOP_SEC_CODE
#include <MemMap.h> /* !LINKSTO Fr.ASR40.FR112,1 */
#endif /* TS_MERGED_COMPILE */

