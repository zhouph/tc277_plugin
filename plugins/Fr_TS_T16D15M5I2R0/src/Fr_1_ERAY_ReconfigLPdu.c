/**
 * \file
 *
 * \brief AUTOSAR Fr
 *
 * This file contains the implementation of the AUTOSAR
 * module Fr.
 *
 * \author Elektrobit Automotive GmbH, 91058 Erlangen, Germany
 *
 * Copyright 2005 - 2013 Elektrobit Automotive GmbH
 * All rights exclusively reserved for Elektrobit Automotive GmbH,
 * unless expressly agreed to otherwise.
 */


/* !LINKSTO Fr.ASR40.FR074,1 */
/******************************************************************************
 Include Section
******************************************************************************/

#include <Fr_1_ERAY_Priv.h> /* !LINKSTO Fr.ASR40.FR462_1,1 */
/******************************************************************************
 Local Macros
******************************************************************************/
/******************************************************************************
 Local Data Types
******************************************************************************/
/******************************************************************************
 Local Data
******************************************************************************/
/******************************************************************************
 Local Functions
******************************************************************************/
/* check whether the API is enabled or not */
#if (FR_1_ERAY_RECONFIGLPDU_API_ENABLE == STD_ON)

/* start code section declaration */
#if (TS_MERGED_COMPILE == STD_OFF)
#define FR_1_ERAY_START_SEC_CODE
#include <MemMap.h> /* !LINKSTO Fr.ASR40.FR112,1 */
#endif /* TS_MERGED_COMPILE */

/* check if development error detection is enabled */
#if (FR_1_ERAY_DEV_ERROR_DETECT == STD_ON)

STATIC INLINE FUNC(Std_ReturnType,FR_1_ERAY_CODE) Fr_1_ERAY_CheckCycleFilter
  (
      VAR(uint8,AUTOMATIC) Fr_CycleRepetition,
      VAR(uint8,AUTOMATIC) Fr_CycleOffset
  );
  
STATIC INLINE FUNC(Std_ReturnType,FR_1_ERAY_CODE) Fr_1_ERAY_CheckCycleFilter
  (
      VAR(uint8,AUTOMATIC) Fr_CycleRepetition,
      VAR(uint8,AUTOMATIC) Fr_CycleOffset
  )
{
    Std_ReturnType retval = E_OK;

    /* check for valid repetition cycle value */
    switch(Fr_CycleRepetition)
    {
        case 1: /* intended fall through */
        case 2: /* intended fall through */
        case 4: /* intended fall through */
        case 8: /* intended fall through */
        case 16: /* intended fall through */
        case 32: /* intended fall through */
        case 64: /* intended fall through */
        {
            if(Fr_CycleOffset >= Fr_CycleRepetition)
            {
                /* Return function status NOT OK */
                retval = E_NOT_OK;
            }
            break;
        }
        default:
        {
            /* Return function status NOT OK */
            retval = E_NOT_OK;
            break;
        }
    }
        
    return retval;
}
#endif /* (FR_1_ERAY_DEV_ERROR_DETECT == STD_ON) */
    

/**
 * \brief   Dynamically reconfigures a LPdu.
 *
 * This service reconfigures the LPdu to FlexRay FrameTriggering assignment at execution time.
 * This service doesn't restrict the configuration within the general valid limits. However it does
 * not ensure that missconfigurations are avoided (e.g. assignment of multiple LPdus to a
 * single FrameTriggering). The behvaviour might be implementation dependent in such cases.
 * The flags SUP, SYNC, PPI are always configured to 0.
 *
 * If the access to the FlexRay controller hardware fails, DEM_EVENT_STATUS_FAILED is reported for
 * event ID FR_1_ERAY_E_ACCESS to DEM and E_NOT_OK is returned.
 *
 * \param[in] Fr_CtrlIdx         FlexRay controller index.
 * \param[in] Fr_LPduIdx         LPdu index.
 * \param[in] Fr_FrameId         FlexRay frame ID.
 * \param[in] Fr_ChnlIdx         FlexRay Channel.
 * \param[in] Fr_CycleRepetition Cycle Repetition pattern.
 * \param[in] Fr_CycleOffset       Base Cycle pattern
 * \param[in] Fr_PayloadLength   Payload length in units of bytes.
 * \param[in] Fr_HeaderCRC       FlexRay Header CRC.
 *
 * \return     E_OK: Service execution was successful.
 *         E_NOT_OK: Service execution failed.
 *
 */
STATIC INLINE FUNC(Std_ReturnType,FR_1_ERAY_CODE) Fr_1_ERAY_ReconfigLPdu_Internal
    (
        VAR(uint8,AUTOMATIC) Fr_CtrlIdx,
        VAR(uint16,AUTOMATIC) Fr_LPduIdx,
        VAR(uint16,AUTOMATIC) Fr_FrameId,
        VAR(Fr_ChannelType,AUTOMATIC) Fr_ChnlIdx,
        VAR(uint8,AUTOMATIC) Fr_CycleRepetition,
        VAR(uint8,AUTOMATIC) Fr_CycleOffset,
        VAR(uint8,AUTOMATIC) Fr_PayloadLength,
        VAR(uint16,AUTOMATIC) Fr_HeaderCRC
    );

STATIC INLINE FUNC(Std_ReturnType,FR_1_ERAY_CODE) Fr_1_ERAY_ReconfigLPdu_Internal
    (
        VAR(uint8,AUTOMATIC) Fr_CtrlIdx,
        VAR(uint16,AUTOMATIC) Fr_LPduIdx,
        VAR(uint16,AUTOMATIC) Fr_FrameId,
        VAR(Fr_ChannelType,AUTOMATIC) Fr_ChnlIdx,
        VAR(uint8,AUTOMATIC) Fr_CycleRepetition,
        VAR(uint8,AUTOMATIC) Fr_CycleOffset,
        VAR(uint8,AUTOMATIC) Fr_PayloadLength,
        VAR(uint16,AUTOMATIC) Fr_HeaderCRC
    )
{
    Std_ReturnType retval = E_OK;
    
    /* define FlexRay CC access handle */
    FCAL_ERAY_DefineCCHandle();

    /* initialize controller handle */
    FCAL_ERAY_InitCCHandle(FR_1_ERAY_CTRLIDX);

    TS_PARAM_UNUSED(Fr_CtrlIdx);

    /* implementation of funcionality */
    {
        /* get pointer to controller configuration */
        CONSTP2CONST(Fr_1_ERAY_CtrlCfgType,AUTOMATIC,FR_1_ERAY_APPL_CONST) pCtrlCfg = 
             &FR_1_ERAY_GET_CONFIG_ADDR
            (
                Fr_1_ERAY_CtrlCfgType,
                gFr_1_ERAY_ConfigPtr->pCtrlCfg
            )[FR_1_ERAY_CTRLIDX];
            
        /* get buffer configuration data structure */
        CONSTP2CONST(Fr_1_ERAY_BufferCfgType,AUTOMATIC,FR_1_ERAY_APPL_CONST) pLPdu =
            &FR_1_ERAY_GET_CONFIG_ADDR
            (
                Fr_1_ERAY_BufferCfgType,
                pCtrlCfg->pLPduCfg
            )[Fr_LPduIdx];


        /* get config parameter nFIDExt2 into local variable (since used several times) */
        CONST(uint16_least,AUTOMATIC) nFIDExt2 = pLPdu->nFID_Ext2;

        if(pLPdu->nBufferCfgIdx != FR_1_ERAY_INVALID_MSGBUFFER_INDEX)
        {
/* check if development error detection is enabled */
#if (FR_1_ERAY_DEV_ERROR_DETECT == STD_ON)

            /* check whether this is an dynamically reconfigurable LPDU */
            if(FR_1_ERAY_GET_LPDUMODE(nFIDExt2) != FR_1_ERAY_LPDUMODE_DYNAMIC_RECONFIG)
            {
                /* Report to DET */
                FR_1_ERAY_DET_REPORTERROR(FR_1_ERAY_E_INV_LPDU_IDX, FR_1_ERAY_RECONFIGLPDU_SERVICE_ID);

                /* return error status */
                retval = E_NOT_OK;

            }
            else if(Fr_PayloadLength > pLPdu->nPayloadLength)
            {
                /* Report to DET */
                FR_1_ERAY_DET_REPORTERROR(FR_1_ERAY_E_INV_LENGTH, FR_1_ERAY_RECONFIGLPDU_SERVICE_ID);

                /* return error status */
                retval = E_NOT_OK;
            }
            else
#endif  /* FR_1_ERAY_DEV_ERROR_DETECT */

            {
                /* read config value from configuration */
                CONST(uint16_least,AUTOMATIC) nCRCExt1 = pLPdu->nCRC_Ext1;

                /* get physical buffer index */
                CONST(uint8,AUTOMATIC) PhysBufIdx = pLPdu->nBufferCfgIdx;

                /* variables holding the buffer configuration register values */
                /* ERAY WRHS1 register variable */
                CONST(uint32,AUTOMATIC) RegWRHS1_W0 =
                    (((((uint32)nCRCExt1)<<12U)&((uint32)0x0C000000UL))|
                     ((((uint32)nFIDExt2)<<16U)&((uint32)0x30000000UL))|
                     (((uint32)0x01000000UL<<Fr_ChnlIdx))|
                     ((((uint32)Fr_CycleRepetition|(uint32)Fr_CycleOffset))<<16U)|
                     ((uint32)(Fr_FrameId)));


                /* ERAY WRHS2 register variable */
                /* don't consider CRC now */
                CONST(uint32,AUTOMATIC) RegWRHS2_W0 =
                    FR_1_ERAY_GET_WRHS2(Fr_HeaderCRC,(((uint32)Fr_PayloadLength + 1U) / 2U));

                /* ERAY WRHS3 register variable */
                CONST(uint32,AUTOMATIC) RegWRHS3_W0 =
                    FR_1_ERAY_GET_WRHS3(pLPdu->nDataPointer);


                /* finally, initialize the message buffer */
                retval = Fr_1_ERAY_ConfigBuffer( FR_1_ERAY_CTRLIDX,
                                                 PhysBufIdx,
                                                 RegWRHS1_W0,
                                                 RegWRHS2_W0,
                                                 RegWRHS3_W0,
                                                 FR_1_ERAY_RECONFIGLPDU_SERVICE_ID);

            }
        }
    }
    return retval;
}

/******************************************************************************
 Global Data
******************************************************************************/
/******************************************************************************
 Global Functions
******************************************************************************/



/**
 * \brief   Reconfigs a LPdu.
 *
 * \param Fr_CtrlIdx (in)       FlexRay controller index.
 * \param Fr_LPduIdx (in)       LPdu index.
 * \param Fr_FrameId (in)       FlexRay frame ID.
 * \param Fr_ChnlIdx (in)       FlexRay Channel.
 * \param Fr_CycleRepetition (in) Cycle repetition value.
 * \param Fr_CycleOffset (in)     Base cycle value.
 * \param Fr_PayloadLength (in) Payload length in units of bytes.
 * \param Fr_HeaderCRC (in)     FlexRay Header CRC.
 *
 * \retval  E_OK               Function serviced successfully.
 * \retval  E_NOT_OK           Function execution failed.
 *
 */
FUNC(Std_ReturnType,FR_1_ERAY_CODE) Fr_1_ERAY_ReconfigLPdu
    (
        VAR(uint8,AUTOMATIC) Fr_CtrlIdx,
        VAR(uint16,AUTOMATIC) Fr_LPduIdx,
        VAR(uint16,AUTOMATIC) Fr_FrameId,
        VAR(Fr_ChannelType,AUTOMATIC) Fr_ChnlIdx,
        VAR(uint8,AUTOMATIC) Fr_CycleRepetition,
        VAR(uint8,AUTOMATIC) Fr_CycleOffset,
        VAR(uint8,AUTOMATIC) Fr_PayloadLength,
        VAR(uint16,AUTOMATIC) Fr_HeaderCRC
    )
{
    Std_ReturnType retval = E_NOT_OK;
    
    /* define FlexRay CC access handle */
    FCAL_ERAY_DefineCCHandle();

/* check if development error detection is enabled */
#if (FR_1_ERAY_DEV_ERROR_DETECT == STD_ON)

    /* check for successfully initialized module */
    /* Report to DET and return Error in case module was not initialized before */
    if (Fr_1_ERAY_InitStatus == FR_1_ERAY_UNINIT)
    {
        FR_1_ERAY_DET_REPORTERROR(FR_1_ERAY_E_NOT_INITIALIZED, FR_1_ERAY_RECONFIGLPDU_SERVICE_ID);
    }

#if (FR_1_ERAY_SINGLE_CTRL_OPT_ENABLE == STD_ON)

    /* check that controller index is 0 */
    else if (Fr_CtrlIdx != (VAR(uint8,AUTOMATIC)) 0U)
    {
        /* Report to DET */
        FR_1_ERAY_DET_REPORTERROR(FR_1_ERAY_E_INV_CTRL_IDX, FR_1_ERAY_RECONFIGLPDU_SERVICE_ID);
    }

#endif  /* FR_1_ERAY_SINGLE_CTRL_OPT_ENABLE */

    /* check if controller index is within supported bounds */
    else if((FR_1_ERAY_CTRLIDX >= FCAL_ERAY_GetNumCC()) ||
       (FCAL_ERAY_IsCCHandleValid(FR_1_ERAY_CTRLIDX) == FALSE))
    {
        /* Report to DET */
        FR_1_ERAY_DET_REPORTERROR(FR_1_ERAY_E_INV_CTRL_IDX, FR_1_ERAY_RECONFIGLPDU_SERVICE_ID);
    }

    /* check whether configuration has an entry for this FlexRay CC Idx */
    else if(FR_1_ERAY_CTRLIDX >= gFr_1_ERAY_ConfigPtr->nNumCtrl)
    {
        /* Report to DET */
        FR_1_ERAY_DET_REPORTERROR(FR_1_ERAY_E_INV_CTRL_IDX, FR_1_ERAY_RECONFIGLPDU_SERVICE_ID);
    }

    /* check that the virtual buffer index is within bounds */
    else if(Fr_LPduIdx >= ((&FR_1_ERAY_GET_CONFIG_ADDR(Fr_1_ERAY_CtrlCfgType,
                                                  gFr_1_ERAY_ConfigPtr->pCtrlCfg)
                                                  [FR_1_ERAY_CTRLIDX])->nNumLPdus))
    {
        /* Report to DET */
        FR_1_ERAY_DET_REPORTERROR(FR_1_ERAY_E_INV_LPDU_IDX, FR_1_ERAY_RECONFIGLPDU_SERVICE_ID);
    }

    /* check if frame ID is out of FlexRay range */
    else if(Fr_FrameId > 0x7FFU)
    {
        /* Report to DET */
        FR_1_ERAY_DET_REPORTERROR(FR_1_ERAY_E_INV_CONFIG, FR_1_ERAY_RECONFIGLPDU_SERVICE_ID);
    }

    /* check whether the channel passed is valid */
    else if(((Fr_ChnlIdx != FR_CHANNEL_A) && (Fr_ChnlIdx != FR_CHANNEL_B)) && (Fr_ChnlIdx != FR_CHANNEL_AB))
    {
        /* Report to DET */
        FR_1_ERAY_DET_REPORTERROR(FR_1_ERAY_E_INV_CHNL_IDX, FR_1_ERAY_RECONFIGLPDU_SERVICE_ID);
    }
    
    else if(Fr_1_ERAY_CheckCycleFilter(Fr_CycleRepetition,Fr_CycleOffset) != E_OK)
    {   
        /* Report to DET */
        FR_1_ERAY_DET_REPORTERROR(FR_1_ERAY_E_INV_CYCLE, FR_1_ERAY_RECONFIGLPDU_SERVICE_ID);
    }
    
    /* check for valid header CRC range */
    else if(Fr_HeaderCRC > 0x7FFU)
    {
        /* Report to DET */
        FR_1_ERAY_DET_REPORTERROR(FR_1_ERAY_E_INV_HEADERCRC, FR_1_ERAY_RECONFIGLPDU_SERVICE_ID);
    }
    
    else
#else /* FR_1_ERAY_DEV_ERROR_DETECT */
    TS_PARAM_UNUSED(Fr_CtrlIdx);
#endif  /* FR_1_ERAY_DEV_ERROR_DETECT */
    {
        /* initialize controller handle */
        FCAL_ERAY_InitCCHandle(FR_1_ERAY_CTRLIDX);

        /* Check whether access to FlexRay CC is functional */
        if(FCAL_ERAY_IsCCAccessValid(FR_1_ERAY_CTRLIDX) == FALSE)
        {
            /* report hardware error */
            FR_1_ERAY_REPORT_CTRL_TEST_STATUS_FAILED(FR_1_ERAY_RECONFIGLPDU_SERVICE_ID);
        }
        else
        {
            retval = Fr_1_ERAY_ReconfigLPdu_Internal(
                                                      Fr_CtrlIdx,
                                                      Fr_LPduIdx,
                                                      Fr_FrameId,
                                                      Fr_ChnlIdx,
                                                      Fr_CycleRepetition,
                                                      Fr_CycleOffset,
                                                      Fr_PayloadLength,
                                                      Fr_HeaderCRC
                                                    );
        }
    }
    return retval;
}

/* stop code section declaration */
#if (TS_MERGED_COMPILE == STD_OFF)
#define FR_1_ERAY_STOP_SEC_CODE
#include <MemMap.h> /* !LINKSTO Fr.ASR40.FR112,1 */
#endif /* TS_MERGED_COMPILE */

#endif /* FR_1_ERAY_RECONFIGLPDU_API_ENABLE */

