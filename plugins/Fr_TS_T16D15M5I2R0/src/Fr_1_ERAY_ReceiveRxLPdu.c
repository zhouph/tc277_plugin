/**
 * \file
 *
 * \brief AUTOSAR Fr
 *
 * This file contains the implementation of the AUTOSAR
 * module Fr.
 *
 * \author Elektrobit Automotive GmbH, 91058 Erlangen, Germany
 *
 * Copyright 2005 - 2013 Elektrobit Automotive GmbH
 * All rights exclusively reserved for Elektrobit Automotive GmbH,
 * unless expressly agreed to otherwise.
 */

/* !LINKSTO Fr.ASR40.FR074,1 */

/*
 * Misra-C:2004 Deviations:
 *
 * MISRA-1) Deviated Rule: 10.3 (required)
 * Complex expression of underlying type 'unsigned short' may only be cast to narrower integer 
 * type of same signedness, however the destination type is 'unsigned int'.
 *
 * Reason:
 * Hardware access macros make use of complex expressions and perform unnecessary casts, as the 
 * actual target type may be target dependent and thus changes. 
 *
 */

 /******************************************************************************
 Include Section
******************************************************************************/

#include <Fr_1_ERAY_Priv.h> /* !LINKSTO Fr.ASR40.FR462_1,1 */

/******************************************************************************
 Local Macros
******************************************************************************/
/******************************************************************************
 Local Data Types
******************************************************************************/
/******************************************************************************
 Local Data
******************************************************************************/
/******************************************************************************
 Global Data
******************************************************************************/
/******************************************************************************
 Local Functions
******************************************************************************/

/* start code section declaration */
#if (TS_MERGED_COMPILE == STD_OFF)
#define FR_1_ERAY_START_SEC_CODE
#include <MemMap.h> /* !LINKSTO Fr.ASR40.FR112,1 */
#endif /* TS_MERGED_COMPILE */

/**
 * \brief   returns if the received message header matches expectations.
 *
 * \param Fr_CtrlIdx (in)            Index of FlexRay controller.
 * \param pLPdu (in)                 Pointer to LPdu-related config data.
 *
 * \retval  TRUE                    Received message header matches expected LPdu.
 * \retval  FALSE                   Received message header does not match expected LPdu.
 */
STATIC INLINE FUNC(boolean,FR_1_ERAY_CODE) Fr_1_ERAY_IsMessageHeaderValid
    (
        VAR(uint8,AUTOMATIC) Fr_CtrlIdx,
        P2CONST(Fr_1_ERAY_BufferCfgType,AUTOMATIC,FR_1_ERAY_APPL_CONST) pLPdu
    );

STATIC INLINE FUNC(boolean,FR_1_ERAY_CODE) Fr_1_ERAY_IsMessageHeaderValid
    (
        VAR(uint8,AUTOMATIC) Fr_CtrlIdx,
        P2CONST(Fr_1_ERAY_BufferCfgType,AUTOMATIC,FR_1_ERAY_APPL_CONST) pLPdu
    )
{
    boolean retval = TRUE;
    
    /* define FlexRay CC access handle */
    FCAL_ERAY_DefineCCHandle();

    /* initialize controller handle */
    FCAL_ERAY_InitCCHandle(FR_1_ERAY_CTRLIDX);

    TS_PARAM_UNUSED(Fr_CtrlIdx);

/* if statically scheduled frames are enabled -
 * we must check whether the message buffer was correctly configured
 */
#if (FR_1_ERAY_PREPARELPDU_API_ENABLE == STD_ON)
    {
        /* get config parameter nFIDExt2 into local variable (since used several times) */
        CONST(uint16_least,AUTOMATIC) nFIDExt2 = pLPdu->nFID_Ext2;
        
        /* is this a dynamic payload length buffer (1:1 mapping to LPdu) ? */
        if(FR_1_ERAY_GET_LPDUMODE(nFIDExt2) == FR_1_ERAY_LPDUMODE_STATIC_RECONFIG)
        {
            /* create Reference register value */
            CONST(FCAL_ERAY_RegSegWType,AUTOMATIC) ReferenceWRHS1_W0 =
                FR_1_ERAY_GET_WRHS1(pLPdu->nCRC_Ext1,nFIDExt2,pLPdu->nCycle);

            /* ERAY register RDHS1 */
            CONST(FCAL_ERAY_RegSegWType,AUTOMATIC) RegRDHS1_W0 =
                FCAL_ERAY_GET(
                    FCAL_ERAY_GetCCHandle(),
                    RDHS1,
                    W0
                    );

            /* Compare real register with reference value */
            if(ReferenceWRHS1_W0 != RegRDHS1_W0)
            {
                /* this is a different slot - discard data */
                retval = FALSE;
            }
        }
    }
    
#else /* (FR_1_ERAY_PREPARELPDU_API_ENABLE == STD_ON) */

    TS_PARAM_UNUSED(pLPdu);

#endif /* (FR_1_ERAY_PREPARELPDU_API_ENABLE == STD_ON) */
    return retval;
}

/**
 * \brief   returns if the received message cycle matches expectations.
 *
 * \param Fr_CtrlIdx (in)            Index of FlexRay controller.
 * \param pLPdu (in)                 Pointer to LPdu-related config data.
 *
 * \retval  TRUE                    Received message cycle matches expected LPdu.
 * \retval  FALSE                   Received message cycle does not match expected LPdu.
 */
STATIC INLINE FUNC(boolean,FR_1_ERAY_CODE) Fr_1_ERAY_IsMessageCycleValid
    (
        VAR(uint8,AUTOMATIC) Fr_CtrlIdx,
        P2CONST(Fr_1_ERAY_BufferCfgType,AUTOMATIC,FR_1_ERAY_APPL_CONST) pLPdu
    );

STATIC INLINE FUNC(boolean,FR_1_ERAY_CODE) Fr_1_ERAY_IsMessageCycleValid
    (
        VAR(uint8,AUTOMATIC) Fr_CtrlIdx,
        P2CONST(Fr_1_ERAY_BufferCfgType,AUTOMATIC,FR_1_ERAY_APPL_CONST) pLPdu
    )
{
    boolean retval = TRUE;

    if(FR_1_ERAY_GET_LPDUMODE(pLPdu->nFID_Ext2) != FR_1_ERAY_LPDUMODE_DYNAMIC_RECONFIG)
    {
        /* ERAY register RDHS3 */
        CONST(FCAL_ERAY_RegSegBType,AUTOMATIC) RegRDHS3_B2 =
            FCAL_ERAY_GET(
                FCAL_ERAY_GetCCHandle(),
                RDHS3,
                B2
                );

        /* load the cycle filter information into a local variable */
        /* Bits 0-5: BaseCycle */
        /* Bits 5-7: CycleRepetition Exponent */
        /* Note on Bit 5: belongs to BaseCycle only if CycleRepetion Exponent is 6 (doesn't use bit 5) */
        CONST(uint8_least,AUTOMATIC) LPduCycleFilter = pLPdu->LPduCycleFilter;

        /* extract temporary CycleRepetitionExponent cycle LPDu cycle filter information */
        CONST(uint8,AUTOMATIC) CycleRepetitionExponentTmp =
            (uint8)((LPduCycleFilter>>5U)&0x07U);

        /* limit CycleRepetitionExponent to value of 6 */
        CONST(uint8,AUTOMATIC) CycleRepetitionExponent =
            (CycleRepetitionExponentTmp >= 6U) ? 6U : CycleRepetitionExponentTmp;

        /* get receive cycle information */
        CONST(uint8,AUTOMATIC) RxCycle =
            FCAL_ERAY_GET_BIT(RegRDHS3_B2,RDHS3,RCC,B2);

        /* calculate CycleRepetitionMask from exponent value */
        CONST(uint8,AUTOMATIC) CycleRepetitionMask =
            (((uint8)(((uint8)0x01U)<<CycleRepetitionExponent))-1U);

        /* calculate BaseCycle of LPdu from LPduCycleFilter */
        CONST(uint8,AUTOMATIC) BaseCycle =
            (uint8)((CycleRepetitionMask >= 63U) ? (LPduCycleFilter&0x3FU) : (LPduCycleFilter&0x1FU));

        TS_PARAM_UNUSED(Fr_CtrlIdx);

        /* received data from unexpected cycle */
        if((RxCycle&CycleRepetitionMask) != BaseCycle)
        {
            /* this is a different cycle - discard data */
            retval = FALSE;
        }
    }
    return retval;
}

/**
 * \brief   Transfers a message buffer to the output buffer.
 *
 * \param Fr_CtrlIdx (in)            Index of FlexRay controller.
 * \param Fr_BufIdx (in)             Index of message buffer.
 *
 * \retval  TRUE                    Message transfer OK.
 * \retval  FALSE                   Message transfer Failed.
 */
STATIC INLINE FUNC(boolean,FR_1_ERAY_CODE) Fr_1_ERAY_TransferToOutputBuffer
    (
        VAR(uint8,AUTOMATIC) Fr_CtrlIdx,
        VAR(uint8,AUTOMATIC) Fr_BufIdx
    );

STATIC INLINE FUNC(boolean,FR_1_ERAY_CODE) Fr_1_ERAY_TransferToOutputBuffer
    (
        VAR(uint8,AUTOMATIC) Fr_CtrlIdx,
        VAR(uint8,AUTOMATIC) Fr_BufIdx
    )
{
    boolean retval = TRUE;

    /* define FlexRay CC access handle */
    FCAL_ERAY_DefineCCHandle();

    /* initialize controller handle */
    FCAL_ERAY_InitCCHandle(FR_1_ERAY_CTRLIDX);
    
    TS_PARAM_UNUSED(Fr_CtrlIdx);
    
    /* set command - read header and data section */
    FCAL_ERAY_SETDC(
        FCAL_ERAY_GetCCHandle(),
        OBCM,
        B0,
        FCAL_ERAY_ALIGN_VALUE(FCAL_ERAY_OBCM_RHSS_W0_MASK|FCAL_ERAY_OBCM_RDSS_W0_MASK,B0)
        );

    /* check for completed previous buffer transfer */
    {
        /* OBCR register variable */
        CONST(FCAL_ERAY_RegSegWType,AUTOMATIC) RegOBCR_B1 =
            FCAL_ERAY_GET(
                FCAL_ERAY_GetCCHandle(),
                OBCR,
                B1
                );

        if (FCAL_ERAY_TEST_BIT(RegOBCR_B1,OBCR,OBSYS,B1))
        {
            /* report hardware error */
            FR_1_ERAY_REPORT_CTRL_TEST_STATUS_FAILED(FR_1_ERAY_RECEIVERXLPDU_SERVICE_ID);

            retval = FALSE;
        }
        else
        {
            /* start buffer data transfer */
            /* Deviation MISRA-1 */
            FCAL_ERAY_SETDC(
                FCAL_ERAY_GetCCHandle(),
                OBCR,
                H0,
                FCAL_ERAY_ALIGN_VALUE((((VAR(uint16,AUTOMATIC))FCAL_ERAY_OBCR_REQ_W0_MASK)|((VAR(uint16,AUTOMATIC))Fr_BufIdx)),H0)
                );

            /* wait until buffer transfer is finished */
            if(Fr_1_ERAY_WaitWhileOBFBusy(FR_1_ERAY_CTRLIDX,FR_1_ERAY_RECEIVERXLPDU_SERVICE_ID) != E_OK)
            {
                retval = FALSE;
            }
            else
            {
                /* switch buffer content from shadow to host */
                FCAL_ERAY_SETDC(
                    FCAL_ERAY_GetCCHandle(),
                    OBCR,
                    B1,
                    FCAL_ERAY_ALIGN_VALUE(FCAL_ERAY_OBCR_VIEW_W0_MASK,B1)
                    );
            }
        }
    }
    return retval;
}

/**
 * \brief   Indicates if there is new data available in a Rx-message buffer.
 *
 * \param Fr_CtrlIdx (in)            Index of FlexRay controller.
 * \param Fr_BufIdx (in)             Index of message buffer.
 *
 * \retval  TRUE                    New message received.
 * \retval  FALSE                   No new message received.
 */
STATIC INLINE FUNC(boolean,FR_1_ERAY_CODE) Fr_1_ERAY_IsRxDataReceived
    (
        VAR(uint8,AUTOMATIC) Fr_CtrlIdx,
        VAR(uint8,AUTOMATIC) Fr_BufIdx
    );
    
STATIC INLINE FUNC(boolean,FR_1_ERAY_CODE) Fr_1_ERAY_IsRxDataReceived
    (
        VAR(uint8,AUTOMATIC) Fr_CtrlIdx,
        VAR(uint8,AUTOMATIC) Fr_BufIdx
    )
{
    boolean retval = FALSE;

    /* define FlexRay CC access handle */
    FCAL_ERAY_DefineCCHandle();

    /* initialize controller handle */
    FCAL_ERAY_InitCCHandle(FR_1_ERAY_CTRLIDX);
    
    TS_PARAM_UNUSED(Fr_CtrlIdx);

    /* this is a dummy buffer - immediately with FALSE - no data received */
    if(Fr_BufIdx != FR_1_ERAY_INVALID_MSGBUFFER_INDEX)
    {
        /* translate buffer index into NDAT register */
        CONST(uint16,AUTOMATIC) NDATRegOffset = (((uint16)Fr_BufIdx)>>3)&((uint16)0xFCU);

        /* translate buffer index into NDAT register mask */
        CONST(FCAL_ERAY_RegSegWType,AUTOMATIC) NDATBufMask =
            (FCAL_ERAY_RegSegWType)(0x01UL<<((Fr_BufIdx&0x1FU)));

        /* read TX-request register */
        CONST(FCAL_ERAY_RegSegWType,AUTOMATIC) RegNDAT_W0 =
            FCAL_ERAY_GET_U32(
                FCAL_ERAY_GetCCHandle(),
                (((VAR(uint32,AUTOMATIC))FCAL_ERAY_NDATn_W0_OFFSET) + ((VAR(uint32,AUTOMATIC))NDATRegOffset))
                );

        if((RegNDAT_W0 & NDATBufMask) != (FCAL_ERAY_RegSegWType)0x0UL)
        {
            retval = TRUE;
        }
    }
    return retval;
}

/**
 * \brief   Receives a LPdu.
 *
 * This service reads the payload of a LPdu out of a FlexRay CC receive buffer.
 * The service ensures that the byte order on the network is equal to the byte order
 * in memory (lower address - first, higher address - later).
 * Only valid, non-null frames are received.
 *
 * \param[in] Fr_CtrlIdx                FlexRay controller index.
 * \param[in] Fr_LPduIdx                LPdu index of PDU used for LSdu reception.
 * \param[out] Fr_LSduPtr               Address payload data should be written to.
 * \param[out] Fr_LPduStatusPtr         Address the receive status should be written to.
 * \param[out] Fr_LSduLengthPtr         Address the actually received payload data length
 *                                      (in units of bytes) should be written to.
 *
 * \return     E_OK: Service execution was successful.
 *         E_NOT_OK: Service execution failed.
 */
STATIC INLINE FUNC(Std_ReturnType,FR_1_ERAY_CODE) Fr_1_ERAY_ReceiveRxLPdu_Internal
(
    VAR(uint8,AUTOMATIC) Fr_CtrlIdx,
    VAR(uint16,AUTOMATIC) Fr_LPduIdx,
    P2VAR(uint8,AUTOMATIC,FR_1_ERAY_APPL_DATA) Fr_LSduPtr,
    P2VAR(Fr_RxLPduStatusType,AUTOMATIC,FR_1_ERAY_APPL_DATA) Fr_LPduStatusPtr,
    P2VAR(uint8,AUTOMATIC,FR_1_ERAY_APPL_DATA) Fr_LSduLengthPtr
);

STATIC INLINE FUNC(Std_ReturnType,FR_1_ERAY_CODE) Fr_1_ERAY_ReceiveRxLPdu_Internal
    (
        VAR(uint8,AUTOMATIC) Fr_CtrlIdx,
        VAR(uint16,AUTOMATIC) Fr_LPduIdx,
        P2VAR(uint8,AUTOMATIC,FR_1_ERAY_APPL_DATA) Fr_LSduPtr,
        P2VAR(Fr_RxLPduStatusType,AUTOMATIC,FR_1_ERAY_APPL_DATA) Fr_LPduStatusPtr,
        P2VAR(uint8,AUTOMATIC,FR_1_ERAY_APPL_DATA) Fr_LSduLengthPtr
    )
{
    
    Std_ReturnType retval = E_OK;
    
    /* define FlexRay CC access handle */
    FCAL_ERAY_DefineCCHandle();

    /* initialize controller handle */
    FCAL_ERAY_InitCCHandle(FR_1_ERAY_CTRLIDX);

    TS_PARAM_UNUSED(Fr_CtrlIdx);

    /* implementation of funcionality */
    {
        /* get pointer to controller configuration */
        CONSTP2CONST(Fr_1_ERAY_CtrlCfgType,AUTOMATIC,FR_1_ERAY_APPL_CONST) pCtrlCfg = 
             &FR_1_ERAY_GET_CONFIG_ADDR
            (
                Fr_1_ERAY_CtrlCfgType,
                gFr_1_ERAY_ConfigPtr->pCtrlCfg
            )[FR_1_ERAY_CTRLIDX];
            
        /* get buffer configuration data structure */
        CONSTP2CONST(Fr_1_ERAY_BufferCfgType,AUTOMATIC,FR_1_ERAY_APPL_CONST) pLPdu =
            &FR_1_ERAY_GET_CONFIG_ADDR
            (
                Fr_1_ERAY_BufferCfgType,
                pCtrlCfg->pLPduCfg
            )[Fr_LPduIdx];

        /* get physical buffer index */
        CONST(uint8,AUTOMATIC) PhyBufIDx = pLPdu->nBufferCfgIdx;

        /* check for new data */
        if(Fr_1_ERAY_IsRxDataReceived(FR_1_ERAY_CTRLIDX,PhyBufIDx)) 
        {
            if(Fr_1_ERAY_TransferToOutputBuffer(FR_1_ERAY_CTRLIDX,PhyBufIDx))
            {
                if(Fr_1_ERAY_IsMessageHeaderValid(FR_1_ERAY_CTRLIDX,pLPdu))
                {
                    if(Fr_1_ERAY_IsMessageCycleValid(FR_1_ERAY_CTRLIDX,pLPdu))
                    {
                        /* message copy operation */
                        /* read RDHS2 for obtaining actually received payload length */
                        CONST(FCAL_ERAY_RegSegBType,AUTOMATIC) RegRDHS2_B3 =
                            FCAL_ERAY_GET(
                                FCAL_ERAY_GetCCHandle(),
                                RDHS2,
                                B3
                                );

                        /* get actually received payload length in unit of bytes */
                        CONST(uint8,AUTOMATIC) nRxLengthBytes =
                            (VAR(uint8,AUTOMATIC))(FCAL_ERAY_GET_BIT(RegRDHS2_B3,RDHS2,PLR,B3)<<1U);

                        /* get number of bytes to copy */
                        CONST(uint8,AUTOMATIC) nConfPLLengthBytes = pLPdu->nPayloadLength;

                        /* evaluate number of bytes to copy to upper layer */
                        CONST(uint8,AUTOMATIC) nCopyBytes =
                            (nRxLengthBytes > nConfPLLengthBytes) ?
                                nConfPLLengthBytes : nRxLengthBytes;

    /* if boundary violation bug workaround is required -
     * return data only if no boundary violation occured
     */
    #if (FR_1_ERAY_BVIOL_BUG_WORKAROUND_ENABLE == STD_ON)

                        /* read MBS for obtaining slot status */
                        CONST(FCAL_ERAY_RegSegBType,AUTOMATIC) RegMBS_B0 =
                            FCAL_ERAY_GET(
                                FCAL_ERAY_GetCCHandle(),
                                MBS,
                                B0
                                );

                        /* check for boundary violation */
                        if((FCAL_ERAY_GET_BIT(RegMBS_B0, MBS, SVOA, B0) != 0U) ||
                            (FCAL_ERAY_GET_BIT(RegMBS_B0, MBS, SVOB, B0) != 0U))
                        {
                            /* no new data available */

                            /* no new frame received */
                            *Fr_LPduStatusPtr = FR_NOT_RECEIVED;

                            /* 0 bytes received */
                            *Fr_LSduLengthPtr = 0x0U;
                        }
                        else

    #endif /* FR_1_ERAY_BVIOL_BUG_WORKAROUND_ENABLE */
                        {
                            /* copy payload data */
                            /* using do { }while(0); within macro for keeping it a single statement */
                            FCAL_ERAY_CTRL_TO_MEM_NBO_ALIGN8(
                                FCAL_ERAY_GetCCHandle(),
                                FCAL_ERAY_RDDSn_W0_OFFSET,
                                nCopyBytes,
                                Fr_LSduPtr,
                                APPL_DATA
                                );

                            /* copy number of actually copied bytes to argument pointer */
                            *Fr_LSduLengthPtr = nCopyBytes;

                            /* copy received status to argument pointer */
                            *Fr_LPduStatusPtr = FR_RECEIVED;
                        }
                    }
                    else
                    {
                        /* no new frame received */
                        *Fr_LPduStatusPtr = FR_NOT_RECEIVED;

                        /* 0 bytes received */
                        *Fr_LSduLengthPtr = 0x0U;
                    }
                }
                else
                {
                  retval = E_NOT_OK;
                }
            }
            else
            {
                retval = E_NOT_OK;
            }
        }
        else
        {
            /* no new frame received */
            *Fr_LPduStatusPtr = FR_NOT_RECEIVED;

            /* 0 bytes received */
            *Fr_LSduLengthPtr = 0x0U;
        }

    }
    return retval;
}

/******************************************************************************
 Global Functions
******************************************************************************/

/**
 * \brief   Receives a LPdu.
 *
 * \param Fr_CtrlIdx (in)               FlexRay controller index.
 * \param Fr_LPduIdx (in)               LPdu index of PDU used for LSdu reception.
 * \param Fr_LSduPtr (out)              Address payload data should be written to.
 * \param Fr_LPduStatusPtr (out)        Address the receive status should be written to.
 * \param Fr_LSduLengthPtr (out)        Address the actually received payload data length
 *                                      (in units of bytes) should be written to.
 *
 * \retval  E_OK               Function serviced successfully.
 * \retval  E_NOT_OK           Function execution failed.
 *
 */
FUNC(Std_ReturnType,FR_1_ERAY_CODE) Fr_1_ERAY_ReceiveRxLPdu
    (
        VAR(uint8,AUTOMATIC) Fr_CtrlIdx,
        VAR(uint16,AUTOMATIC) Fr_LPduIdx,
        P2VAR(uint8,AUTOMATIC,FR_1_ERAY_APPL_DATA) Fr_LSduPtr,
        P2VAR(Fr_RxLPduStatusType,AUTOMATIC,FR_1_ERAY_APPL_DATA) Fr_LPduStatusPtr,
        P2VAR(uint8,AUTOMATIC,FR_1_ERAY_APPL_DATA) Fr_LSduLengthPtr
    )
{
    Std_ReturnType retval = E_OK;

    /* define FlexRay CC access handle */
    FCAL_ERAY_DefineCCHandle();

#if (FR_1_ERAY_DEV_ERROR_DETECT == STD_ON)

    /* check for successfully initialized module */
    /* Report to DET and return Error in case module was not initialized before */
    if (Fr_1_ERAY_InitStatus == FR_1_ERAY_UNINIT)
    {
        FR_1_ERAY_DET_REPORTERROR(FR_1_ERAY_E_NOT_INITIALIZED, FR_1_ERAY_RECEIVERXLPDU_SERVICE_ID);
        /* Return function status NOT OK */
        retval = E_NOT_OK;
    }

#if (FR_1_ERAY_SINGLE_CTRL_OPT_ENABLE == STD_ON)

    /* check that controller index is 0 */
    else if (Fr_CtrlIdx != (VAR(uint8,AUTOMATIC)) 0U)
    {
        /* Report to DET */
        FR_1_ERAY_DET_REPORTERROR(FR_1_ERAY_E_INV_CTRL_IDX, FR_1_ERAY_RECEIVERXLPDU_SERVICE_ID);

        /* Return function status NOT OK */
        retval = E_NOT_OK;
    }

#endif  /* FR_1_ERAY_SINGLE_CTRL_OPT_ENABLE */

    /* check if controller index is within supported bounds */
    else if((FR_1_ERAY_CTRLIDX >= FCAL_ERAY_GetNumCC()) ||
       (FCAL_ERAY_IsCCHandleValid(FR_1_ERAY_CTRLIDX) == FALSE))
    {

        /* Report to DET */
        FR_1_ERAY_DET_REPORTERROR(FR_1_ERAY_E_INV_CTRL_IDX, FR_1_ERAY_RECEIVERXLPDU_SERVICE_ID);

        /* Return function status NOT OK */
        retval = E_NOT_OK;
    }

    /* check whether configuration has an entry for this FlexRay CC Idx */
    else if(FR_1_ERAY_CTRLIDX >= gFr_1_ERAY_ConfigPtr->nNumCtrl)
    {

        /* Report to DET */
        FR_1_ERAY_DET_REPORTERROR(FR_1_ERAY_E_INV_CTRL_IDX, FR_1_ERAY_RECEIVERXLPDU_SERVICE_ID);

        /* Return function status NOT OK */
        retval = E_NOT_OK;

    }

    /* check that the virtual buffer index is within bounds */
    else if(Fr_LPduIdx >= ((&FR_1_ERAY_GET_CONFIG_ADDR(Fr_1_ERAY_CtrlCfgType,
                                                  gFr_1_ERAY_ConfigPtr->pCtrlCfg)
                                                  [FR_1_ERAY_CTRLIDX])->nNumLPdus))
    {
        /* Report to DET */
        FR_1_ERAY_DET_REPORTERROR(FR_1_ERAY_E_INV_LPDU_IDX, FR_1_ERAY_RECEIVERXLPDU_SERVICE_ID);

        /* Return negative status code */
        retval = E_NOT_OK;
    }

    /* check pointer for beeing valid */
    else if(Fr_LSduPtr == NULL_PTR)
    {
        /* Report to DET */
        FR_1_ERAY_DET_REPORTERROR(FR_1_ERAY_E_INV_POINTER, FR_1_ERAY_RECEIVERXLPDU_SERVICE_ID);

        /* return error code */
        retval = E_NOT_OK;
    }

    /* check pointer for beeing valid */
    else if(Fr_LPduStatusPtr == NULL_PTR)
    {
        /* Report to DET */
        FR_1_ERAY_DET_REPORTERROR(FR_1_ERAY_E_INV_POINTER, FR_1_ERAY_RECEIVERXLPDU_SERVICE_ID);

        /* return error code */
        retval = E_NOT_OK;
    }

    /* check pointer for beeing valid */
    else if(Fr_LSduLengthPtr == NULL_PTR)
    {
        /* Report to DET */
        FR_1_ERAY_DET_REPORTERROR(FR_1_ERAY_E_INV_POINTER, FR_1_ERAY_RECEIVERXLPDU_SERVICE_ID);

        /* return error code */
        retval = E_NOT_OK;
    }
    else

#else /* FR_1_ERAY_DEV_ERROR_DETECT */

    TS_PARAM_UNUSED(Fr_CtrlIdx);

#endif  /* FR_1_ERAY_DEV_ERROR_DETECT */
    {
        /* initialize controller handle */
        FCAL_ERAY_InitCCHandle(FR_1_ERAY_CTRLIDX);

        /* Check whether access to FlexRay CC is functional */
        if(FCAL_ERAY_IsCCAccessValid(FR_1_ERAY_CTRLIDX) == FALSE)
        {
            /* report hardware error */
            FR_1_ERAY_REPORT_CTRL_TEST_STATUS_FAILED(FR_1_ERAY_RECEIVERXLPDU_SERVICE_ID);

            /* Return function status NOT OK */
            retval = E_NOT_OK;
        }
        else
        {
            retval = Fr_1_ERAY_ReceiveRxLPdu_Internal(FR_1_ERAY_CTRLIDX,Fr_LPduIdx,Fr_LSduPtr,Fr_LPduStatusPtr,Fr_LSduLengthPtr);
        }
    }
    /* Return function status OK */
    return retval;
}


/* stop code section declaration */
#if (TS_MERGED_COMPILE == STD_OFF)
#define FR_1_ERAY_STOP_SEC_CODE
#include <MemMap.h> /* !LINKSTO Fr.ASR40.FR112,1 */
#endif /* TS_MERGED_COMPILE */


