/**
 * \file
 *
 * \brief AUTOSAR Fr
 *
 * This file contains the implementation of the AUTOSAR
 * module Fr.
 *
 * \author Elektrobit Automotive GmbH, 91058 Erlangen, Germany
 *
 * Copyright 2005 - 2013 Elektrobit Automotive GmbH
 * All rights exclusively reserved for Elektrobit Automotive GmbH,
 * unless expressly agreed to otherwise.
 */


/* !LINKSTO Fr.ASR40.FR074,1 */

/*------------------[AUTOSAR vendor identification check]-------------------*/

#if (!defined FR_1_ERAY_VENDOR_ID) /* configuration check */
#error FR_1_ERAY_VENDOR_ID must be defined
#endif

#if (FR_1_ERAY_VENDOR_ID != 1U) /* vendor check */
#error FR_1_ERAY_VENDOR_ID has wrong vendor id
#endif

/*------------------[AUTOSAR release version identification check]----------*/

#if (!defined FR_1_ERAY_AR_RELEASE_MAJOR_VERSION) /* configuration check */
#error FR_1_ERAY_AR_RELEASE_MAJOR_VERSION must be defined
#endif

/* major version check */
#if (FR_1_ERAY_AR_RELEASE_MAJOR_VERSION != 4U)
#error FR_1_ERAY_AR_RELEASE_MAJOR_VERSION wrong (!= 4U)
#endif

#if (!defined FR_1_ERAY_AR_RELEASE_MINOR_VERSION) /* configuration check */
#error FR_1_ERAY_AR_RELEASE_MINOR_VERSION must be defined
#endif

/* minor version check */
#if (FR_1_ERAY_AR_RELEASE_MINOR_VERSION != 0U )
#error FR_1_ERAY_AR_RELEASE_MINOR_VERSION wrong (!= 0U)
#endif

#if (!defined FR_1_ERAY_AR_RELEASE_REVISION_VERSION) /* configuration check */
#error FR_1_ERAY_AR_RELEASE_REVISION_VERSION must be defined
#endif

/* revision version check */
#if (FR_1_ERAY_AR_RELEASE_REVISION_VERSION != 3U )
#error FR_1_ERAY_AR_RELEASE_REVISION_VERSION wrong (!= 3U)
#endif

/*------------------[AUTOSAR module version identification check]-----------*/

#if (!defined FR_1_ERAY_SW_MAJOR_VERSION) /* configuration check */
#error FR_1_ERAY_SW_MAJOR_VERSION must be defined
#endif

/* major version check */
#if (FR_1_ERAY_SW_MAJOR_VERSION != 5U)
#error FR_1_ERAY_SW_MAJOR_VERSION wrong (!= 5U)
#endif

#if (!defined FR_1_ERAY_SW_MINOR_VERSION) /* configuration check */
#error FR_1_ERAY_SW_MINOR_VERSION must be defined
#endif

/* minor version check */
#if (FR_1_ERAY_SW_MINOR_VERSION < 2U)
#error FR_1_ERAY_SW_MINOR_VERSION wrong (< 2U)
#endif

#if (!defined FR_1_ERAY_SW_PATCH_VERSION) /* configuration check */
#error FR_1_ERAY_SW_PATCH_VERSION must be defined
#endif

/* patch version check */
#if (FR_1_ERAY_SW_PATCH_VERSION < 5U)
#error FR_1_ERAY_SW_PATCH_VERSION wrong (< 5U)
#endif
