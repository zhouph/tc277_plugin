/**
 * \file
 *
 * \brief AUTOSAR Fr
 *
 * This file contains the implementation of the AUTOSAR
 * module Fr.
 *
 * \author Elektrobit Automotive GmbH, 91058 Erlangen, Germany
 *
 * Copyright 2005 - 2013 Elektrobit Automotive GmbH
 * All rights exclusively reserved for Elektrobit Automotive GmbH,
 * unless expressly agreed to otherwise.
 */

#if !defined _FR_1_ERAY_FCAL_SYM_H_
#define _FR_1_ERAY_FCAL_SYM_H_

#include <Fr_1_ERAY_MLIB_symreg_access.h>

/***************************** access macros ******************************/

/* map ERAY macros to generic macros while resolving arguments */
/* hardware access macros */
/* get register macro */
#define FCAL_ERAY_GET(h,reg,rseg) \
                MLIB_SYMREG_GET(FCAL_ERAY,h,reg,rseg)

/* set register macro */
#define FCAL_ERAY_SET(h,reg,rseg,val) \
                MLIB_SYMREG_SET(FCAL_ERAY,h,reg,rseg,rseg,val)

/* set register don't care macro */
#define FCAL_ERAY_SETDC(h,reg,rseg,val) \
                MLIB_SYMREG_SETDC(FCAL_ERAY,h,reg,rseg,rseg,val)


/* Get register by index macro */
#define FCAL_ERAY_GET_IDX(h,reg,rseg,idx) \
    MLIB_SYMREG_GET_IDX(FCAL_ERAY,h,reg,rseg,idx)

/* bit operation macros */
/* testbit macro */
#define FCAL_ERAY_TEST_BIT(variable,reg,bit,rseg) \
            MLIB_SYMREG_TEST_BIT(FCAL_ERAY,variable,reg,bit,rseg)


/* get bit(field) macro */
#define FCAL_ERAY_GET_BIT(variable,reg,bit,rseg) \
            MLIB_SYMREG_GET_BIT(FCAL_ERAY,variable,reg,bit,rseg)


/* set bit(field) macro */
#define FCAL_ERAY_SET_BIT(variable,reg,bit,rseg,value) \
            MLIB_SYMREG_SET_BIT(FCAL_ERAY,variable,reg,bit,rseg,value)

/* aligtn register value macro */
#define FCAL_ERAY_ALIGN_VALUE(value,rseg)   \
            MLIB_SYMREG_ALIGN_VALUE(value,rseg)

/* map native access macros independent of the access file */
/* U8 byte segmment accesses are always performed as native U8 operations */
#define FCAL_ERAY_SET_U8_B0(a,b,c) FCAL_ERAY_SET_U8(a,b,c)
#define FCAL_ERAY_SET_U8_B1(a,b,c) FCAL_ERAY_SET_U8(a,b,c)
#define FCAL_ERAY_SET_U8_B2(a,b,c) FCAL_ERAY_SET_U8(a,b,c)
#define FCAL_ERAY_SET_U8_B3(a,b,c) FCAL_ERAY_SET_U8(a,b,c)

/* U16 halfword segmment accesses are always performed as native U16 access operations */
#define FCAL_ERAY_SET_U16_H0(a,b,c) FCAL_ERAY_SET_U16(a,b,c)
#define FCAL_ERAY_SET_U16_H1(a,b,c) FCAL_ERAY_SET_U16(a,b,c)

/* U32 word segmment accesses are always performed as native U32 access operations */
#define FCAL_ERAY_SET_U32_W0(a,b,c) FCAL_ERAY_SET_U32(a,b,c)

/* U8 byte segmment accesses are always performed as native U8 operations */
#define FCAL_ERAY_SETDC_B0(a,b,c) FCAL_ERAY_SET_U8(a,b,c)
#define FCAL_ERAY_SETDC_B1(a,b,c) FCAL_ERAY_SET_U8(a,b,c)
#define FCAL_ERAY_SETDC_B2(a,b,c) FCAL_ERAY_SET_U8(a,b,c)
#define FCAL_ERAY_SETDC_B3(a,b,c) FCAL_ERAY_SET_U8(a,b,c)

/* U16 halfword segmment accesses are always performed as native U16 access operations */
#define FCAL_ERAY_SETDC_H0(a,b,c) FCAL_ERAY_SET_U16(a,b,c)
#define FCAL_ERAY_SETDC_H1(a,b,c) FCAL_ERAY_SET_U16(a,b,c)

/* U32 word segmment accesses are always performed as native U32 access operations */
#define FCAL_ERAY_SETDC_W0(a,b,c) FCAL_ERAY_SET_U32(a,b,c)

/* map ERAY register types to generic register types */
typedef MLIB_SYMREG_RegSegBType FCAL_ERAY_RegSegBType;
typedef MLIB_SYMREG_RegSegHType FCAL_ERAY_RegSegHType;
typedef MLIB_SYMREG_RegSegWType FCAL_ERAY_RegSegWType;

#endif  /* _FR_1_ERAY_FCAL_SYM_H_ */

