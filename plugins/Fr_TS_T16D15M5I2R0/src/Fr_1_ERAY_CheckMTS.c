/**
 * \file
 *
 * \brief AUTOSAR Fr
 *
 * This file contains the implementation of the AUTOSAR
 * module Fr.
 *
 * \author Elektrobit Automotive GmbH, 91058 Erlangen, Germany
 *
 * Copyright 2005 - 2013 Elektrobit Automotive GmbH
 * All rights exclusively reserved for Elektrobit Automotive GmbH,
 * unless expressly agreed to otherwise.
 */

/* !LINKSTO Fr.ASR40.FR074,1 */
/******************************************************************************
 Include Section
******************************************************************************/

#include <Fr_1_ERAY_Priv.h> /* !LINKSTO Fr.ASR40.FR462_1,1 */

/******************************************************************************
 Local Macros
******************************************************************************/
/******************************************************************************
 Local Data Types
******************************************************************************/
/******************************************************************************
 Local Data
******************************************************************************/

/* check whether this API service is enabled or not */
#if (FR_1_ERAY_MTS_API_ENABLE == STD_ON)

/* start constant section declaration */
#define FR_1_ERAY_START_SEC_CONST_UNSPECIFIED
#include <MemMap.h> /* !LINKSTO Fr.ASR40.FR112,1 */

/* translation table that calculates a virtual key into a the output value */
STATIC CONST(Fr_MTSStatusType,FR_1_ERAY_CONST) aKeyToValConv[] =
{
    FR_MTS_NOT_RCV,                 /* no MTS received, no errors detected */
    FR_MTS_NOT_RCV_BVIO,            /* boundary violation detected */
    FR_MTS_NOT_RCV_SYNERR,          /* syntax error detected */
    FR_MTS_NOT_RCV_SYNERR_BVIO,     /* syntax error and boundary violation detected */
    FR_MTS_RCV,                     /* MTS received */
    FR_MTS_RCV_BVIO,                /* MTS received, boundary violation detected */
    FR_MTS_RCV_SYNERR,              /* MTS received, syntax error detected */
    FR_MTS_RCV_SYNERR_BVIO,         /* MTS received, syntax error and boundary violation detected */
};

/* stop constant section declaration */
#define FR_1_ERAY_STOP_SEC_CONST_UNSPECIFIED
#include <MemMap.h> /* !LINKSTO Fr.ASR40.FR112,1 */

/******************************************************************************
 Local Functions
******************************************************************************/
/******************************************************************************
 Global Functions
******************************************************************************/


/* start code section declaration */
#define FR_1_ERAY_START_SEC_CODE
#include <MemMap.h> /* !LINKSTO Fr.ASR40.FR112,1 */


/**
 * \brief   Reports the MTS receive status.
 *
 * \param Fr_CtrlIdx (in)       FlexRay controller index.
 * \param Fr_ChnlIdx (in)       Channel the MTS should be received on.
 * \param Fr_MTSStatusPtr (out) Address the MTS status is written to.
 *
 * \retval  E_OK               Function serviced successfully.
 * \retval  E_NOT_OK           Function execution failed.
 *
 * \note    The function must be called in CC POC-state "normal-active/passive".
 *
 */
FUNC(Std_ReturnType,FR_1_ERAY_CODE) Fr_1_ERAY_CheckMTS
    (
        VAR(uint8,AUTOMATIC) Fr_CtrlIdx,
        VAR(Fr_ChannelType,AUTOMATIC) Fr_ChnlIdx,
        P2VAR(Fr_MTSStatusType,AUTOMATIC,FR_1_ERAY_APPL_DATA) Fr_MTSStatusPtr
    )
{
    Std_ReturnType retval = E_OK;
    /* define FlexRay CC access handle */
    FCAL_ERAY_DefineCCHandle();

/* check if development error detection is enabled */
#if (FR_1_ERAY_DEV_ERROR_DETECT == STD_ON)

    /* check for successfully initialized module */
    /* Report to DET and return Error in case module was not initialized before */
    if (Fr_1_ERAY_InitStatus == FR_1_ERAY_UNINIT)
    {
        FR_1_ERAY_DET_REPORTERROR(FR_1_ERAY_E_NOT_INITIALIZED, FR_1_ERAY_CHECKMTS_SERVICE_ID);
        /* Return function status NOT OK */
        retval = E_NOT_OK;
    }

#if (FR_1_ERAY_SINGLE_CTRL_OPT_ENABLE == STD_ON)

    /* check that controller index is 0 */
    else if (Fr_CtrlIdx != (VAR(uint8,AUTOMATIC)) 0U)
    {
        /* Report to DET */
        FR_1_ERAY_DET_REPORTERROR(FR_1_ERAY_E_INV_CTRL_IDX, FR_1_ERAY_CHECKMTS_SERVICE_ID);

        /* Return function status NOT OK */
        retval = E_NOT_OK;
    }
#endif  /* FR_1_ERAY_SINGLE_CTRL_OPT_ENABLE */

    /* check if controller index is within supported bounds */
    else if((FR_1_ERAY_CTRLIDX >= FCAL_ERAY_GetNumCC()) ||
       (FCAL_ERAY_IsCCHandleValid(FR_1_ERAY_CTRLIDX) == FALSE))
    {

        /* Report to DET */
        FR_1_ERAY_DET_REPORTERROR(FR_1_ERAY_E_INV_CTRL_IDX, FR_1_ERAY_CHECKMTS_SERVICE_ID);

        /* Return function status NOT OK */
        retval = E_NOT_OK;
    }

    /* Check for valid channel argument */
    else if((Fr_ChnlIdx != FR_CHANNEL_A) && (Fr_ChnlIdx != FR_CHANNEL_B))
    {
        /* Report to DET */
        FR_1_ERAY_DET_REPORTERROR(FR_1_ERAY_E_INV_CHNL_IDX, FR_1_ERAY_CHECKMTS_SERVICE_ID);

        /* Return function status NOT OK */
        retval = E_NOT_OK;
    }

    /* check if pointer passed at API is valid */
    else if(Fr_MTSStatusPtr == NULL_PTR)
    {
        /* Report to DET */
        FR_1_ERAY_DET_REPORTERROR(FR_1_ERAY_E_INV_POINTER, FR_1_ERAY_CHECKMTS_SERVICE_ID);

        /* Return function status NOT OK */
        retval = E_NOT_OK;
    }
    else
#else /* FR_1_ERAY_DEV_ERROR_DETECT */

    TS_PARAM_UNUSED(Fr_CtrlIdx);

#endif  /* FR_1_ERAY_DEV_ERROR_DETECT */

    {
        /* initialize controller handle */
        FCAL_ERAY_InitCCHandle(FR_1_ERAY_CTRLIDX);

        /* Check whether access to FlexRay CC is functional */
        if(FCAL_ERAY_IsCCAccessValid(FR_1_ERAY_CTRLIDX) == FALSE)
        {
            /* report hardware error */
            FR_1_ERAY_REPORT_CTRL_TEST_STATUS_FAILED(FR_1_ERAY_CHECKMTS_SERVICE_ID);

            /* Return function status NOT OK */
            retval = E_NOT_OK;
        }

        else
        {   /* check MTS status */
            CONST(FCAL_ERAY_RegSegBType,AUTOMATIC) RegSWNIT_B0 =
                FCAL_ERAY_GET(
                    FCAL_ERAY_GetCCHandle(),
                    SWNIT,
                    B0);

           /* check status of channel A */
            if(Fr_ChnlIdx == FR_CHANNEL_A)
            {
                uint16 tmpKey;

                /* encode MTS-syntax error into key */
                tmpKey = (uint16)((FCAL_ERAY_RegSegBType)(FCAL_ERAY_GET_BIT(RegSWNIT_B0,SWNIT,SESA,B0) << 0x1U));

                /* encode MTS-boundary violation into key */
                tmpKey |= (uint16)(FCAL_ERAY_GET_BIT(RegSWNIT_B0,SWNIT,SBSA,B0));


                /* encode MTS receive status into key */
                tmpKey |= (uint16)((FCAL_ERAY_RegSegBType)(FCAL_ERAY_GET_BIT(RegSWNIT_B0,SWNIT,MTSA,B0) << 0x2U));


                /* translate key to status value */
                *Fr_MTSStatusPtr = aKeyToValConv[tmpKey];

            }
            else /* check status of channel B */
            {
                uint16 tmpKey;

                /* encode MTS-syntax error into key */
                tmpKey = (uint16) ((FCAL_ERAY_RegSegBType)(FCAL_ERAY_GET_BIT(RegSWNIT_B0,SWNIT,SESB,B0) << 0x1U));

                /* encode MTS-boundary violation into key */
                tmpKey |= (uint16) (FCAL_ERAY_GET_BIT(RegSWNIT_B0,SWNIT,SBSB,B0));

                /* encode MTS receive status into key */
                tmpKey |= (uint16)((FCAL_ERAY_RegSegBType)(FCAL_ERAY_GET_BIT(RegSWNIT_B0,SWNIT,MTSB,B0) << 0x2U));

                /* translate key to status value */
                *Fr_MTSStatusPtr = aKeyToValConv[tmpKey];
            }
        }
    }
    return retval;
}

/* stop code section declaration */
#define FR_1_ERAY_STOP_SEC_CODE
#include <MemMap.h> /* !LINKSTO Fr.ASR40.FR112,1 */

#endif /* FR_1_ERAY_MTS_API_ENABLE */
