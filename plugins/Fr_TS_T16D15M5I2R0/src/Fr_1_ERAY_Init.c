/**
 * \file
 *
 * \brief AUTOSAR Fr
 *
 * This file contains the implementation of the AUTOSAR
 * module Fr.
 *
 * \author Elektrobit Automotive GmbH, 91058 Erlangen, Germany
 *
 * Copyright 2005 - 2013 Elektrobit Automotive GmbH
 * All rights exclusively reserved for Elektrobit Automotive GmbH,
 * unless expressly agreed to otherwise.
 */

/* !LINKSTO Fr.ASR40.FR074,1 */
/******************************************************************************
 Include Section
******************************************************************************/

#include <Fr_1_ERAY_Priv.h> /* !LINKSTO Fr.ASR40.FR462_1,1 */
#include <Fr_1_ERAY_Lcfg.h>
#include <TSPBConfig_Signature.h>

/******************************************************************************
 Local Macros
******************************************************************************/
/******************************************************************************
 Local Data Types
******************************************************************************/

/******************************************************************************
 Local Data
******************************************************************************/
/******************************************************************************
 Local Functions
******************************************************************************/
/******************************************************************************
 Global Data
******************************************************************************/

#if (FR_1_ERAY_DEV_ERROR_DETECT == STD_ON)

#define FR_1_ERAY_START_SEC_VAR_FAST_INIT_8
#include <MemMap.h> /* !LINKSTO Fr.ASR40.FR112,1 */

/**
 * global variable for the actual initialization status of the Fr_1_ERAY
 */
VAR(Fr_1_ERAY_InitStatusType, FR_1_ERAY_VAR_FAST) Fr_1_ERAY_InitStatus =
    FR_1_ERAY_UNINIT;

#define FR_1_ERAY_STOP_SEC_VAR_FAST_INIT_8
#include <MemMap.h> /* !LINKSTO Fr.ASR40.FR112,1 */

#endif /* FR_1_ERAY_DEV_ERROR_DETECT */

#define FR_1_ERAY_START_SEC_VAR_FAST_INIT_UNSPECIFIED
#include <MemMap.h> /* !LINKSTO Fr.ASR40.FR112,1 */

/**
 * global variable for the pointer to teh config of Fr_1_ERAY
 */
P2CONST(Fr_1_ERAY_ConfigType, FR_1_ERAY_VAR_FAST, FR_1_ERAY_APPL_CONST) gFr_1_ERAY_ConfigPtr;

#define FR_1_ERAY_STOP_SEC_VAR_FAST_INIT_UNSPECIFIED
#include <MemMap.h> /* !LINKSTO Fr.ASR40.FR112,1 */

/******************************************************************************
 Global Function Declarations
******************************************************************************/

/******************************************************************************
 Global Function Defintions
******************************************************************************/

/* start code section declaration */
#define FR_1_ERAY_START_SEC_CODE
#include <MemMap.h> /* !LINKSTO Fr.ASR40.FR112,1 */

/** \brief Validate configuration
 **
 ** Checks if the post build configuration fits to the link time configuration part.
 **
 ** \param[in] ConfigPtr Pointer where the post-build-time configuration is stored
 ** \return E_OK if the given module configurations is valid otherwise E_NOT_OK.
 **
 **/

FUNC( Std_ReturnType, FR_1_ERAY_CODE) Fr_1_ERAY_IsValidConfig
(
        P2CONST(void, AUTOMATIC, FR_1_ERAY_APPL_CONST) voidConfigPtr
)
{
    /* Assume an invalid configuration */
    Std_ReturnType RetVal = E_NOT_OK;
    P2CONST(Fr_1_ERAY_ConfigType, AUTOMATIC, FR_1_ERAY_APPL_CONST) ConfigPtr = voidConfigPtr;

    if (ConfigPtr != NULL_PTR)
    {
        /* Check if the configuration fits to the platform */
        if (TS_PlatformSigIsValid(ConfigPtr->PlatformSignature) == TRUE )
        {
            /* Validate the post build configuration against the compile time configuration */
            if (FR_1_ERAY_PUBLIC_INFO_SIGNATURE == ConfigPtr->PublicInfoSignature)
            {
                /* Validate the post build configuration against the compile time configuration */
                if (FR_1_ERAY_CFG_SIGNATURE == ConfigPtr->CfgSignature)
                {
                    /* Validate the post build configuration against the link time configuration */
                    if (Fr_1_ERAY_LcfgSignature == ConfigPtr->LcfgSignature)
                    {
                        /* Indicate that the configuration is valid */
                        RetVal = E_OK;
                    }
                }
            }
        }
    }
    return RetVal;
}

/**
 * \brief   Initializes the Fr_1_ERAY module and registers the post-build-time configuration data
 *          passed as argument
 *
 * \param Fr_ConfigPtr (in)    Pointer to configuration structure that holds the
 *                             Fr_1_ERAY module post-build-time configuration data.
 *
 * \note    This function leaves all FlexRay CC's in 'halt' POC-state.
 *
 */
FUNC(void,FR_1_ERAY_CODE) Fr_1_ERAY_Init
    (
        P2CONST(Fr_1_ERAY_ConfigType,AUTOMATIC,FR_1_ERAY_APPL_CONST) Fr_ConfigPtr
    )
{
    P2CONST(Fr_1_ERAY_ConfigType,AUTOMATIC,FR_1_ERAY_APPL_CONST) TmpCfgPtr = Fr_ConfigPtr;

    /* Save the configuration pointer */
#if (FR_1_ERAY_PBCFGM_SUPPORT_ENABLED == STD_ON)
    /* If the initialization function is called with a null pointer get the configuration from the
     * post build configuration manager */
    if (TmpCfgPtr == NULL_PTR)
    {
        PbcfgM_ModuleConfigPtrType ModuleConfig = NULL_PTR;
        if (E_OK == PbcfgM_GetConfig(
              FR_1_ERAY_MODULE_ID,
              0U,
              &ModuleConfig))
        {
            TmpCfgPtr = (P2CONST(Fr_1_ERAY_ConfigType, AUTOMATIC, FR_1_ERAY_APPL_CONST)) ModuleConfig;
        }
    }
#endif /* FR_1_ERAY_PBCFGM_SUPPORT_ENABLED == STD_ON */

/* check if development error detection is enabled */
#if (FR_1_ERAY_DEV_ERROR_DETECT == STD_ON)

    /* check whether parameter LocalConfigPtr is a NULL_PTR */
    if(TmpCfgPtr == NULL_PTR)
    {
        /* Report to DET */
        FR_1_ERAY_DET_REPORTERROR(FR_1_ERAY_E_INV_POINTER, FR_1_ERAY_INIT_SERVICE_ID);
    }
    else
#endif /* FR_1_ERAY_DEV_ERROR_DETECT */
    {
      /* check that configuration pointer is valid */
      if ( E_OK != Fr_1_ERAY_IsValidConfig(TmpCfgPtr))
      {
#if (FR_1_ERAY_DEV_ERROR_DETECT == STD_ON)      
          /* Report to DET */
          FR_1_ERAY_DET_REPORTERROR(FR_1_ERAY_E_INV_CONFIG, FR_1_ERAY_INIT_SERVICE_ID);
#endif /* FR_1_ERAY_DEV_ERROR_DETECT */          
          return;
      }

        /* initialize low level access driver */
        FCAL_ERAY_AccessDriverInit();

        /* save configuration pointer */
        gFr_1_ERAY_ConfigPtr = TmpCfgPtr;

/* check if development error detection is enabled */
#if (FR_1_ERAY_DEV_ERROR_DETECT == STD_ON)

        /* module was successfully initialized - set marker */
        Fr_1_ERAY_InitStatus = FR_1_ERAY_INIT;

#endif /* FR_1_ERAY_DEV_ERROR_DETECT */
    }
}

/* stop code section declaration */
#define FR_1_ERAY_STOP_SEC_CODE
#include <MemMap.h> /* !LINKSTO Fr.ASR40.FR112,1 */

