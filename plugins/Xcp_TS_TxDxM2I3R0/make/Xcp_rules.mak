# \file
#
# \brief AUTOSAR Xcp
#
# This file contains the implementation of the AUTOSAR
# module Xcp.
#
# \author Elektrobit Automotive GmbH, 91058 Erlangen, Germany
#
# Copyright 2005 - 2013 Elektrobit Automotive GmbH
# All rights exclusively reserved for Elektrobit Automotive GmbH,
# unless expressly agreed to otherwise.

#################################################################
# DEFINITIONS

#################################################################
# REGISTRY

Xcp_src_FILES      := $(Xcp_CORE_PATH)\src\Xcp.c \
                      $(Xcp_CORE_PATH)\src\Xcp_CommandProcessor.c \
                      $(Xcp_CORE_PATH)\src\Xcp_EventProcessor.c \
                      $(Xcp_CORE_PATH)\src\Xcp_EventReceiver.c \
                      $(Xcp_CORE_PATH)\src\Xcp_ReceiveProcessor.c \
                      $(Xcp_CORE_PATH)\src\Xcp_StimProcessor.c \
                      $(Xcp_CORE_PATH)\src\Xcp_TransmitProcessor.c \
                      $(Xcp_OUTPUT_PATH)\src\Xcp_Cfg.c

LIBRARIES_TO_BUILD += Xcp_src

#################################################################
# DEPENDENCIES (only for assembler files)
#

#################################################################
# RULES




