#if (!defined XCP_INT_H)
#define XCP_INT_H

/**
 * \file
 *
 * \brief AUTOSAR Xcp
 *
 * This file contains the implementation of the AUTOSAR
 * module Xcp.
 *
 * \author Elektrobit Automotive GmbH, 91058 Erlangen, Germany
 *
 * Copyright 2005 - 2013 Elektrobit Automotive GmbH
 * All rights exclusively reserved for Elektrobit Automotive GmbH,
 * unless expressly agreed to otherwise.
 */

/*==================[inclusions]============================================*/
#include <ComStack_Types.h>     /* Com stack standard types */
#include <Xcp_Cfg.h>            /* XCP configuration header */
#include <Xcp_Int_Cfg.h>        /* Internal configuration defines and declarations */
#include <Xcp_DefProg_Cfg.h>   /* Configuration defines related to defensive programming */

#if (XCP_DEV_ERROR_DETECT == STD_ON)
#include <Det.h>               /* Det API */
#endif

/*==================[macros]================================================*/




/*------------------------[Defensive programming]----------------------------*/

#if (defined XCP_PRECONDITION_ASSERT)
#error XCP_PRECONDITION_ASSERT is already defined
#endif
#if (XCP_PRECONDITION_ASSERT_ENABLED == STD_ON)
/** \brief Report an assertion violation to Det
 **
 ** \param[in] Condition Condition which is violated
 ** \param[in] ApiId Service ID of the API function */
#define XCP_PRECONDITION_ASSERT(Condition, ApiId) \
  DET_PRECONDITION_ASSERT((Condition), XCP_MODULE_ID, XCP_INSTANCE_ID, (ApiId))
#else
#define XCP_PRECONDITION_ASSERT(Condition, ApiId)
#endif

#if (defined XCP_POSTCONDITION_ASSERT)
#error XCP_POSTCONDITION_ASSERT is already defined
#endif
#if (XCP_POSTCONDITION_ASSERT_ENABLED == STD_ON)
/** \brief Report an assertion violation to Det
 **
 ** \param[in] Condition Condition which is violated
 ** \param[in] ApiId Service ID of the API function */
#define XCP_POSTCONDITION_ASSERT(Condition, ApiId) \
  DET_POSTCONDITION_ASSERT((Condition), XCP_MODULE_ID, XCP_INSTANCE_ID, (ApiId))
#else
#define XCP_POSTCONDITION_ASSERT(Condition, ApiId)
#endif

#if (defined XCP_INVARIANT_ASSERT)
#error XCP_INVARIANT_ASSERT is already defined
#endif
#if (XCP_INVARIANT_ASSERT_ENABLED == STD_ON)
/** \brief Report an assertion violation to Det
 **
 ** \param[in] Condition Condition which is violated
 ** \param[in] ApiId Service ID of the API function */
#define XCP_INVARIANT_ASSERT(Condition, ApiId) \
  DET_INVARIANT_ASSERT((Condition), XCP_MODULE_ID, XCP_INSTANCE_ID, (ApiId))
#else
#define XCP_INVARIANT_ASSERT(Condition, ApiId)
#endif

#if (defined XCP_STATIC_ASSERT)
# error XCP_STATIC_ASSERT is already defined
#endif
#if (XCP_STATIC_ASSERT_ENABLED == STD_ON)
/** \brief Report an static assertion violation to Det
 **
 ** \param[in] Condition Condition which is violated */
# define XCP_STATIC_ASSERT(expr) DET_STATIC_ASSERT(expr)
#else
# define XCP_STATIC_ASSERT(expr)
#endif

#if (defined XCP_UNREACHABLE_CODE_ASSERT)
#error XCP_UNREACHABLE_CODE_ASSERT is already defined
#endif
#if (XCP_UNREACHABLE_CODE_ASSERT_ENABLED == STD_ON)
/** \brief Report an unreachable code assertion violation to Det
 **
 ** \param[in] ApiId Service ID of the API function */
#define XCP_UNREACHABLE_CODE_ASSERT(ApiId) \
  DET_UNREACHABLE_CODE_ASSERT(XCP_MODULE_ID, XCP_INSTANCE_ID, (ApiId))
#else
#define XCP_UNREACHABLE_CODE_ASSERT(ApiId)
#endif

#if (defined XCP_INTERNAL_API_ID)
#error XCP_INTERNAL_API_ID is already defined
#endif
/** \brief API ID of module internal functions to be used in assertions */
#define XCP_INTERNAL_API_ID DET_INTERNAL_API_ID


/*------------------[Bit Masks]----------------------------------------------*/

#if (defined XCP_BYTE_ORDER)
#error XCP_BYTE_ORDER already defined
#endif
/** \brief Definition of value indicating the byte order used for transferring
 ** multi-byte parameters in an XCP Packet. */
#if (CPU_BYTE_ORDER == LOW_BYTE_FIRST)
#define XCP_BYTE_ORDER               XCP_LITTLE_ENDIAN_MASK
#else
#define XCP_BYTE_ORDER               XCP_BIG_ENDIAN_MASK
#endif

#if ((XCP_DAQ_CONFIG_TYPE == XCP_DAQ_STATIC_MASK) || (XCP_MIN_DAQ != 0U))

#if (defined XCP_MASK_DAQ_DIRECTION_STIM)
#error XCP_MASK_DAQ_DIRECTION_STIM already defined
#endif
/** \brief Mask to check whether DAQ direction is STIM */
#define XCP_MASK_DAQ_DIRECTION_STIM                      0x08U

#if (defined XCP_MASK_DAQ_DIRECTION_DAQ)
#error XCP_MASK_DAQ_DIRECTION_DAQ already defined
#endif
/** \brief Mask to check whether DAQ direction is DAQ */
#define XCP_MASK_DAQ_DIRECTION_DAQ                       0x04U

#endif /* ((XCP_DAQ_CONFIG_TYPE == XCP_DAQ_STATIC_MASK) || (XCP_MIN_DAQ != 0)) */

#if (defined XCP_RESOURCE_SUPPORTED)
#error XCP_RESOURCE_SUPPORTED already defined
#endif
/** \brief Definition of supported resources by XCP module. */
#define XCP_RESOURCE_SUPPORTED     \
( XCP_RESOURCE_CAL_PAG | ( XCP_RESOURCE_DAQ | (XCP_RESOURCE_STIM | XCP_RESOURCE_PGM) ) )

#if (defined XCP_COMM_MODE_BASIC)
#error XCP_COMM_MODE_BASIC already defined
#endif
/** \brief Definition of value indicating Basic communication mode parameters of Xcp. */
#define XCP_COMM_MODE_BASIC            \
( XCP_BYTE_ORDER | ( XCP_ADDRESS_GRANULARITY | XCP_SLAVE_BLOCK_MODE ) )

#if (defined XCP_COMM_MODE_OPTIONAL)
#error XCP_COMM_MODE_OPTIONAL already defined
#endif
/** \brief Definition of value indicating communication mode information. */
#define XCP_COMM_MODE_OPTIONAL      XCP_MASTER_BLOCK_MODE

#if (defined XCP_COMM_MODE_PGM)
#error XCP_COMM_MODE_PGM already defined
#endif
/** \brief COMM_MODE_PGM for command PROGRAM_START. */
#define XCP_COMM_MODE_PGM           (XCP_MASTER_BLOCK_MODE_PGM | XCP_SLAVE_BLOCK_MODE)

#if (defined XCP_TIMESTAMP_MODE)
#error XCP_TIMESTAMP_MODE already defined
#endif
/** \brief Definition of value indicating Timestamp unit and size. */
#define XCP_TIMESTAMP_MODE          \
( XCP_TIMESTAMP_TYPE | ( XCP_TIMESTAMP_FIXED | XCP_TIMESTAMP_UNIT ) )

#if (defined XCP_RESUME_SUPPORTED)
#error XCP_RESUME_SUPPORTED already defined
#endif
/** \brief Definition of value indicating Resume Mode is supported. */
#if (XCP_STORE_DAQ_SUPPORTED == STD_ON)
#define XCP_RESUME_SUPPORTED           XCP_RESUME_SUPPORTED_MASK
#else
#define XCP_RESUME_SUPPORTED           0x00U
#endif

#if (defined XCP_DAQ_PROPERTIES)
#error XCP_DAQ_PROPERTIES already defined
#endif
/** \brief Definition of value indicating the general properties of DAQ
 ** processor. */
/* Uncomment the properties of DAQ Processor when Supported.*/
#define XCP_DAQ_PROPERTIES                     \
( XCP_DAQ_CONFIG_TYPE | ( XCP_PRESCALER_SUPPORTED | ( XCP_RESUME_SUPPORTED | \
  ( XCP_TIMESTAMP_SUPPORTED | XCP_OVERLOAD_INDICATION_TYPE ) ) ) )

#if (defined XCP_DAQ_KEY_BYTE)
#error XCP_DAQ_KEY_BYTE already defined
#endif
/** \brief Definition of value indicating the Daq List general properties . */
#define XCP_DAQ_KEY_BYTE                    \
( XCP_OPTIMISATION_TYPE | ( XCP_ADDRESS_EXTENSION_TYPE | XCP_IDENTIFICATION_FIELD_TYPE ) )


#if (defined XCP_MAX_CTO_ABS)
#error XCP_MAX_CTO_ABS already defined
#endif
/** \brief The absolute maximum CTO size defined by the maximum between MAX_CTO and MAX_CTO_PGM */
#if (XCP_PGM_SUPPORTED == STD_ON)
#if (XCP_MAX_CTO >= XCP_MAX_CTO_PGM)
#define XCP_MAX_CTO_ABS          XCP_MAX_CTO
#else
#define XCP_MAX_CTO_ABS          XCP_MAX_CTO_PGM
#endif
#else /* programming not supported */
#define XCP_MAX_CTO_ABS            XCP_MAX_CTO
#endif /* XCP_PGM_SUPPORTED == STD_ON */
/*------------------[Constants]----------------------------------------------*/

#if (CPU_BYTE_ORDER == LOW_BYTE_FIRST)

/** \brief byte index of least significant byte in a uint16 value for little
 * endian architectures */
#define XCP_UINT16_LSB_IDX 0U
/** \brief byte index of most significant byte in a uint16 value for little
 * endian architectures */
#define XCP_UINT16_MSB_IDX 1U

/** \brief byte index of least significant byte in a uint32 value for little
 * endian architectures */
#define XCP_UINT32_SB0_IDX 0U
/** \brief byte index of 2nd least significant byte in a uint32 value for
 * little endian architectures */
#define XCP_UINT32_SB1_IDX 1U
/** \brief byte index of 3rd least significant byte in a uint32 value for
 * little endian architectures */
#define XCP_UINT32_SB2_IDX 2U
/** \brief byte index of 4th least significant byte in a uint32 value for
 * little endian architectures */
#define XCP_UINT32_SB3_IDX 3U

#else /* CPU_BYTE_ORDER == HIGH_BYTE_FIRST */

/** \brief byte index of least significant byte in a uint16 value for big
 * endian architectures */
#define XCP_UINT16_LSB_IDX 1U
/** \brief byte index of most significant byte in a uint16 value for big
 * endian architectures */
#define XCP_UINT16_MSB_IDX 0U

/** \brief byte index of least significant byte in a uint32 value for big
 * endian architectures */
#define XCP_UINT32_SB0_IDX 3U
/** \brief byte index of 2nd least significant byte in a uint32 value for big
 * endian architectures */
#define XCP_UINT32_SB1_IDX 2U
/** \brief byte index of 3rd least significant byte in a uint32 value for big
 * endian architectures */
#define XCP_UINT32_SB2_IDX 1U
/** \brief byte index of 4th least significant byte in a uint32 value for big
 * endian architectures */
#define XCP_UINT32_SB3_IDX 0U

#endif /* CPU_BYTE_ORDER == LOW_BYTE_FIRST */

#if (defined XCP_CMD_PROCESSOR_QUEUE_SIZE)
#error XCP_CMD_PROCESSOR_QUEUE_SIZE already defined
#endif
/** \brief Size of the Command Queue.
 *
 * In Standard communication mode the size of command queue shall be 0x01U.
 * In Block communication mode the size of command queue shall be
 * MAX_BS/MAX_BS_PGM which ever is larger.  In Interleaved communication mode
 * the size of command queue shall be QUEUE_SIZE/QUEUE_SIZE_PGM which ever is
 * larger. */
#if((XCP_MASTER_BLOCK_MODE_SUPPORTED == STD_ON) || \
    (XCP_MASTER_BLOCK_MODE_PGM_SUPPORTED == STD_ON))
/** \brief In Block communication mode the size of command queue shall be MAX_BS */
#if (XCP_MAX_BS > XCP_MAX_BS_PGM)
#define XCP_CMD_PROCESSOR_QUEUE_SIZE      XCP_MAX_BS
#else
#define XCP_CMD_PROCESSOR_QUEUE_SIZE      XCP_MAX_BS_PGM
#endif
#else /* no block mode */
/** \brief In Standard communication mode the size of command queue shall be 1. */
#define XCP_CMD_PROCESSOR_QUEUE_SIZE      1U
#endif /* XCP_MASTER_BLOCK_MODE_SUPPORTED == STD_ON || XCP_MASTER_BLOCK_MODE_PGM_SUPPORTED == STD_ON */

#if (defined XCP_TX_TRIGGER_ENABLED)
#error XCP_TX_TRIGGER_ENABLED already defined
#endif
/** \brief Define feature flag macro which tells if TriggerTransmit function will be
 *  called by lower layer */
#if (XCP_ON_FLEXRAY_ENABLED == STD_ON)
#define XCP_TX_TRIGGER_ENABLED STD_ON
#else
#define XCP_TX_TRIGGER_ENABLED STD_OFF
#endif /* XCP_ON_FLEXRAY_ENABLED == STD_ON */

#if (defined XCP_SEED_KEY_MAX_LENGTH)
#error XCP_SEED_KEY_MAX_LENGTH already defined
#endif
/** \brief Definition of maximum supported length for seed and key. */
#define XCP_SEED_KEY_MAX_LENGTH                          (XCP_MAX_CTO - 0x02U)

#if (XCP_ON_CAN_ENABLED == STD_ON)

#if (defined XCP_ASCII_X)
#error XCP_ASCII_X already defined
#endif
/** \brief Definition of value indicating ASCII value for character X */
#define XCP_ASCII_X                                      0x58U

#if (defined XCP_ASCII_C)
#error XCP_ASCII_C already defined
#endif
/** \brief Definition of value indicating ASCII value for character C */
#define XCP_ASCII_C                                      0x43U

#if (defined XCP_ASCII_P)
#error XCP_ASCII_P already defined
#endif
/** \brief Definition of value indicating ASCII value for character P */
#define XCP_ASCII_P                                      0x50U

#if (defined XCP_ASCII_INVERSE_X)
#error XCP_ASCII_INVERSE_X already defined
#endif
/** \brief Definition of value indicating inverse of ASCII value for character X */
#define XCP_ASCII_INVERSE_X                              0xA7U

#if (defined XCP_ASCII_INVERSE_C)
#error XCP_ASCII_INVERSE_C already defined
#endif
/** \brief Definition of value indicating inverse of ASCII value for character C */
#define XCP_ASCII_INVERSE_C                              0xBCU

#if (defined XCP_ASCII_INVERSE_P)
#error XCP_ASCII_INVERSE_P already defined
#endif
/** \brief Definition of value indicating inverse of ASCII value for character P */
#define XCP_ASCII_INVERSE_P                              0xAFU

#if (defined XCP_GET_SLAVE_ID_OFFSET_X)
#error XCP_GET_SLAVE_ID_OFFSET_X already defined
#endif
/** \brief Definition of value indicating offset to ASCII value for character X */
#define XCP_GET_SLAVE_ID_OFFSET_X                        0x00U

#if (defined XCP_GET_SLAVE_ID_OFFSET_C)
#error XCP_GET_SLAVE_ID_OFFSET_C already defined
#endif
/** \brief Definition of value indicating offset to ASCII value for character C */
#define XCP_GET_SLAVE_ID_OFFSET_C                        0x01U

#if (defined XCP_GET_SLAVE_ID_OFFSET_P)
#error XCP_GET_SLAVE_ID_OFFSET_P already defined
#endif
/** \brief Definition of value indicating offset to ASCII value for character P */
#define XCP_GET_SLAVE_ID_OFFSET_P                        0x02U

#if (defined XCP_GET_SLAVE_ID_OFFSET_MODE)
#error XCP_GET_SLAVE_ID_OFFSET_MODE already defined
#endif
/** \brief Definition of value indicating offset to mode parameter in command GET_SLAVE_ID */
#define XCP_GET_SLAVE_ID_OFFSET_MODE                     0x03U

#if (defined XCP_GET_SLAVE_ID_BY_ECHO)
#error XCP_GET_SLAVE_ID_BY_ECHO already defined
#endif
/** \brief Definition of command GET_SLAVE_ID mode parameter value indicating mode
 ** "confirm by echo". */
#define XCP_GET_SLAVE_ID_BY_ECHO                         0x00U

#if (defined XCP_GET_SLAVE_ID_BY_INVERSE_ECHO)
#error XCP_GET_SLAVE_ID_BY_INVERSE_ECHO already defined
#endif
/** \brief Definition of command GET_SLAVE_ID mode parameter value indicating mode
 ** "confirm by inverse echo". */
#define XCP_GET_SLAVE_ID_BY_INVERSE_ECHO                 0x01U

#if (defined XCP_SUBCMD_GET_SLAVE_ID_RES_LENGTH)
#error XCP_SUBCMD_GET_SLAVE_ID_RES_LENGTH already defined
#endif
/** \brief Length for positive response to the command GET_SLAVE_ID. */
#define XCP_SUBCMD_GET_SLAVE_ID_RES_LENGTH               0x08U

#if (defined XCP_SUBCMD_GET_DAQ_ID_RES_LENGTH)
#error XCP_SUBCMD_GET_DAQ_ID_RES_LENGTH already defined
#endif
/** \brief Length for positive response to the command GET_DAQ_ID. */
#define XCP_SUBCMD_GET_DAQ_ID_RES_LENGTH                 0x08U

#if (defined XCP_CAN_ID_FIXED )
#error XCP_CAN_ID_FIXED  already defined
#endif
/** \brief Definition of value to indicate if CAN_ID is FIXED */
#define XCP_CAN_ID_FIXED                                 0x01U

#if (defined XCP_CAN_ID_CONFIGURABLE )
#error XCP_CAN_ID_CONFIGURABLE  already defined
#endif
/** \brief Definition of value to indicate if CAN_ID is CONFIGURABLE */
#define XCP_CAN_ID_CONFIGURABLE                          0x00U

#endif /* XCP_ON_CAN_ENABLED = STD_ON */

#if (defined XCP_CMDPROCESSOR_IDLE)
#error XCP_CMDPROCESSOR_IDLE already defined
#endif
/** \brief Value indicating that command processor is currently idle */
#define XCP_CMDPROCESSOR_IDLE                           0U

#if (defined XCP_CMDPROCESSOR_BUSY)
#error XCP_CMDPROCESSOR_BUSY already defined
#endif
/** \brief Value indicating that command processor is currently busy */
#define XCP_CMDPROCESSOR_BUSY                           1U

#if (defined XCP_ABSOLUTE_IF_MASK)
#error XCP_ABSOLUTE_IF_MASK already defined
#endif
/** \brief Mask indicating Identification field type with Absolute ODT number. */
#define XCP_ABSOLUTE_IF_MASK                               0x00U

#if (defined XCP_RELATIVE_BYTE_IF_MASK)
#error XCP_RELATIVE_BYTE_IF_MASK already defined
#endif
/** \brief Mask indicating Identification field type with Relative ODT number
 ** and 1 byte absolute DAQ list number. */
#define XCP_RELATIVE_BYTE_IF_MASK                          0x40U

#if (defined XCP_RELATIVE_WORD_IF_MASK)
#error XCP_RELATIVE_WORD_IF_MASK already defined
#endif
/** \brief Mask indicating Identification field type with Relative ODT number
 ** and 2 byte absolute DAQ list number. */
#define XCP_RELATIVE_WORD_IF_MASK                          0x80U

#if (defined XCP_RELATIVE_WORD_ALIGNED_IF_MASK)
#error XCP_RELATIVE_WORD_ALIGNED_IF_MASK already defined
#endif
/** \brief Mask indicating Identification field type with Relative ODT number,
 ** and word aligned absolute DAQ list number. */
#define XCP_RELATIVE_WORD_ALIGNED_IF_MASK                  0xC0U

#if (defined XCP_PID_LENGTH)
#error XCP_PID_LENGTH already defined
#endif
/** \brief Length of the Identification field in an incoming DTO
 */
#if (XCP_IDENTIFICATION_FIELD_TYPE == XCP_ABSOLUTE_IF_MASK)
#define XCP_PID_LENGTH    1U
#elif (XCP_IDENTIFICATION_FIELD_TYPE == XCP_RELATIVE_BYTE_IF_MASK)
#define XCP_PID_LENGTH    2U
#elif (XCP_IDENTIFICATION_FIELD_TYPE == XCP_RELATIVE_WORD_IF_MASK)
#define XCP_PID_LENGTH    3U
#else /* (XCP_IDENTIFICATION_FIELD_TYPE == XCP_RELATIVE_WORD_ALIGNED_IF_MASK) */
#define XCP_PID_LENGTH    4U
#endif

#if (defined XCP_TS_LENGTH)
#error XCP_TS_LENGTH already defined
#endif
/** \brief Length of the Timestamp field in an incoming DTO */
#if (XCP_TIMESTAMP_TYPE == XCP_NO_TIME_STAMP_TS_MASK)
#define XCP_TS_LENGTH 0U
#elif (XCP_TIMESTAMP_TYPE == XCP_ONE_BYTE_TS_MASK)
#define XCP_TS_LENGTH 1U
#elif (XCP_TIMESTAMP_TYPE == XCP_TWO_BYTE_TS_MASK)
#define XCP_TS_LENGTH 2U
#elif (XCP_TIMESTAMP_TYPE == XCP_FOUR_BYTE_TS_MASK)
#define XCP_TS_LENGTH 4U
#endif

#if (defined XCP_DTO_HEADER_LENGTH)
#error XCP_DTO_HEADER_LENGTH already defined
#endif
/** \brief Length of the Identification + Timestamp field in an incoming
 * DTO */
#define XCP_DTO_HEADER_LENGTH (XCP_PID_LENGTH + XCP_TS_LENGTH)

#if (defined XCP_CTO_QUEUE_SIZE)
#error XCP_CTO_QUEUE_SIZE already defined
#endif
/** \brief Size of CTO Queue. Size of CTO Queue is taken as size required for
 * responses to at least two commands. */
#define XCP_CTO_QUEUE_SIZE ((XCP_MAX_CTO + 1U) * 2U)

#if (defined XCP_MAX_UPLOAD_BYTES_ONE_CTO)
  #error XCP_MAX_UPLOAD_BYTES_ONE_CTO already defined
#endif
/** \brief  Maximum bytes that can be uploaded from the slave in one CTO response */
#define XCP_MAX_UPLOAD_BYTES_ONE_CTO    (XCP_MAX_CTO - XCP_ADDRESS_GRANULARITY_SIZE)

/** \brief  Maximum elements that can be downloaded to the slave in one command
 * (DOWNLOAD or DOWNLOAD_NEXT)*/
#if (XCP_ADDRESS_GRANULARITY_SIZE == XCP_BYTE_AG_SIZE)
#define XCP_MAX_DOWNLOAD_ELEMNTS_ONE_CMD   ((XCP_MAX_CTO - 2U) / XCP_ADDRESS_GRANULARITY_SIZE)
#else /* XCP_ADDRESS_GRANULARITY_SIZE = 2 or 4 bytes */
#define XCP_MAX_DOWNLOAD_ELEMNTS_ONE_CMD   ((XCP_MAX_CTO - XCP_ADDRESS_GRANULARITY_SIZE) / \
                                             XCP_ADDRESS_GRANULARITY_SIZE)
#endif /* XCP_ADDRESS_GRANULARITY_SIZE == XCP_BYTE_AG_SIZE */

#if (XCP_MASTER_BLOCK_MODE_SUPPORTED == STD_ON)
/** \brief  Maximum elements that can be downloaded to the slave in one download sequence */
  #if ((XCP_MAX_BS * XCP_MAX_DOWNLOAD_ELEMNTS_ONE_CMD) < XCP_ABS_MAX_DOWNLOAD_ELEMENTS)
  #define XCP_MAX_DOWNLOAD_ELEMENTS        (XCP_MAX_BS * XCP_MAX_DOWNLOAD_ELEMNTS_ONE_CMD)
  #else /* we can't have more than 255 elements downloaded in one download sequence */
  #define XCP_MAX_DOWNLOAD_ELEMENTS        XCP_ABS_MAX_DOWNLOAD_ELEMENTS
  #endif /* (XCP_MAX_BS * XCP_MAX_DOWNLOAD_ELEMNTS_ONE_CMD) < 255U */
#else /* (XCP_MASTER_BLOCK_MODE_SUPPORTED != STD_ON) */
  #define XCP_MAX_DOWNLOAD_ELEMENTS        XCP_MAX_DOWNLOAD_ELEMNTS_ONE_CMD
#endif /* XCP_MASTER_BLOCK_MODE_SUPPORTED == STD_ON */


#if (defined XCP_MAX_DOWNLOAD_BYTES)
#error XCP_MAX_DOWNLOAD_BYTES already defined
#endif
/** \brief  Maximum bytes that can be downloaded to the slave in one download sequence */
#define XCP_MAX_DOWNLOAD_BYTES             (XCP_MAX_DOWNLOAD_ELEMENTS * XCP_ADDRESS_GRANULARITY_SIZE)


#if (XCP_PGM_SUPPORTED == STD_ON)
/** \brief  Maximum elements that can be programmed to the slave in one command
 * (PROGRAM or PROGRAM_NEXT) - but not PROGRAM_MAX */
#if (defined XCP_MAX_PROGRAM_ELEMNTS_ONE_CMD)
#error XCP_MAX_PROGRAM_ELEMNTS_ONE_CMD already defined
#endif
#if (XCP_ADDRESS_GRANULARITY_SIZE == XCP_BYTE_AG_SIZE)
#define XCP_MAX_PROGRAM_ELEMNTS_ONE_CMD   ((XCP_MAX_CTO_PGM - 2U) / XCP_ADDRESS_GRANULARITY_SIZE)
#else /* XCP_ADDRESS_GRANULARITY_SIZE = 2 or 4 bytes */
#define XCP_MAX_PROGRAM_ELEMNTS_ONE_CMD   ((XCP_MAX_CTO_PGM - XCP_ADDRESS_GRANULARITY_SIZE) / \
                                             XCP_ADDRESS_GRANULARITY_SIZE)
#endif /* XCP_ADDRESS_GRANULARITY_SIZE == XCP_BYTE_AG_SIZE */

#if (defined XCP_MAX_PROGRAM_ELEMENTS)
#error XCP_MAX_PROGRAM_ELEMENTS already defined
#endif
#if (XCP_MASTER_BLOCK_MODE_PGM_SUPPORTED == STD_ON)
/** \brief  Maximum elements that can be programmed to the slave in one programming sequence */
  #if ((XCP_MAX_BS_PGM * XCP_MAX_PROGRAM_ELEMNTS_ONE_CMD) < XCP_ABS_MAX_PROGRAM_ELEMENTS)
  #define XCP_MAX_PROGRAM_ELEMENTS        (XCP_MAX_BS_PGM * XCP_MAX_PROGRAM_ELEMNTS_ONE_CMD)
  #else /* we can't have more than 255 elements programmed in one programming sequence */
  #define XCP_MAX_PROGRAM_ELEMENTS        XCP_ABS_MAX_PROGRAM_ELEMENTS
  #endif /* (XCP_MAX_BS * XCP_MAX_PROGRAM_ELEMNTS_ONE_CMD) < 255U */
#else /* (XCP_MASTER_BLOCK_MODE_PGM_SUPPORTED != STD_ON) */
  #define XCP_MAX_PROGRAM_ELEMENTS        XCP_MAX_PROGRAM_ELEMNTS_ONE_CMD
#endif /* XCP_MASTER_BLOCK_MODE_PGM_SUPPORTED == STD_ON */

#if (defined XCP_MAX_PROGRAM_BYTES)
#error XCP_MAX_PROGRAM_BYTES already defined
#endif
/** \brief  Maximum bytes that can be programmed to the slave in one programming sequence */
#define XCP_MAX_PROGRAM_BYTES             (XCP_MAX_PROGRAM_ELEMENTS * XCP_ADDRESS_GRANULARITY_SIZE)
#else /* PGM resource is not available */
#if (defined XCP_MAX_PROGRAM_BYTES)
#error XCP_MAX_PROGRAM_BYTES already defined
#endif
/** \brief  Maximum bytes that can be programmed to the slave in one programming sequence */
#define XCP_MAX_PROGRAM_BYTES             0U
#endif /* XCP_PGM_SUPPORTED == STD_ON */

#if (defined XCP_MAX_WRITE_BYTES)
#error XCP_MAX_WRITE_BYTES already defined
#endif
/** \brief  The maximum between XCP_MAX_PROGRAM_BYTES and XCP_MAX_DOWNLOAD_BYTES */
#if (XCP_MAX_PROGRAM_BYTES > XCP_MAX_DOWNLOAD_BYTES)
#define XCP_MAX_MEMORY_WRITE_BYTES   XCP_MAX_PROGRAM_BYTES
#else
#define XCP_MAX_MEMORY_WRITE_BYTES   XCP_MAX_DOWNLOAD_BYTES
#endif

/** \brief Will return the start index of data to be downloaded from the CTO command.
 * Definition is according to the ASAM specs */
#if (XCP_ADDRESS_GRANULARITY_SIZE == XCP_BYTE_AG_SIZE)
#define XCP_GET_START_DATA_INDEX_FROM_CTO()      2U
#else /* AG = 2 or 4 bytes */
#define XCP_GET_START_DATA_INDEX_FROM_CTO()      XCP_ADDRESS_GRANULARITY_SIZE
#endif

#if (defined XCP_DRIVER_VERSION)
#error XCP_DRIVER_VERSION already defined
#endif
/** \brief The version that must be returned to the XCP master upon GET_COMM_MODE_INFO command
 * Constructed according to the ASAM specs */
#define XCP_DRIVER_VERSION  (((uint8)(XCP_SW_MAJOR_VERSION << 4U)) | (XCP_SW_MINOR_VERSION & 0x0FU));

#if (XCP_EVENT_PACKET_ENABLED == STD_ON)

#if (defined XCP_EV_STIM_TIMEOUT_INFO_DAQLIST)
#error XCP_EV_STIM_TIMEOUT_INFO_DAQLIST already defined
#endif
/** \brief EV_STIM_TIMEOUT event reports with a DAQList info. */
#define XCP_EV_STIM_TIMEOUT_INFO_DAQLIST                  0x01U

#if (defined XCP_EV_STIM_TIMEOUT_INFO_EVENT)
#error XCP_EV_STIM_TIMEOUT_INFO_EVENT already defined
#endif
/** \brief EV_STIM_TIMEOUT event reports with a Event channel number as info. */
#define XCP_EV_STIM_TIMEOUT_INFO_EVENT                    0x00U

#if (defined XCP_EV_PID)
#error XCP_EV_PID already defined
#endif
/** \brief PID for Event packet CTO. */
#define XCP_EV_PID                                        0xFDU

#endif /* XCP_EVENT_PACKET_ENABLED == STD_ON */

#if (defined XCP_GET_NO_OF_CONFIGURED_DAQ_LISTS)
#error XCP_GET_NO_OF_CONFIGURED_DAQ_LISTS already defined
#endif
/** \brief The number of configured DAQ lists.
 ** \param[in] RetType Cast type used to fix MISRA-C 10.1 violation */
#if (XCP_DAQ_CONFIG_TYPE == XCP_DAQ_STATIC_MASK)
#define XCP_GET_NO_OF_CONFIGURED_DAQ_LISTS(RetType)    ((RetType)XCP_MAX_DAQ)
#else
#define XCP_GET_NO_OF_CONFIGURED_DAQ_LISTS(RetType)    ((RetType)XCP_MIN_DAQ + (RetType)Xcp_GetNoOfDynamicDaqLists())
#endif

#if (XCP_MAX_DAQ != 0U)

#if (defined XCP_MAX_ODT_ENTRY_SIZE_ABS)
#error XCP_MAX_ODT_ENTRY_SIZE_ABS already defined
#endif
/** \brief The maximum possible size for an ODT entry.
 * This value shall be used when it is not known for certain at the check point the direction of
 * the DAQ list */
#if (XCP_MAX_ODT_ENTRY_SIZE_DAQ > XCP_MAX_ODT_ENTRY_SIZE_STIM)
#define XCP_MAX_ODT_ENTRY_SIZE_ABS    XCP_MAX_ODT_ENTRY_SIZE_DAQ
#else
#define XCP_MAX_ODT_ENTRY_SIZE_ABS    XCP_MAX_ODT_ENTRY_SIZE_STIM
#endif

#endif /* XCP_MAX_DAQ != 0U */

/*------------------[Function like macros]----------------------------------------------*/

#if (XCP_DAQ_CONFIG_TYPE == XCP_DAQ_STATIC_MASK)
/** \brief Gets the DAQ list pointer from the corresponding static structure */
#define XCP_GET_DAQ_LIST_PTR(DaqListNumber)  &(Xcp_DaqLists.Xcp_NonDynamicDaqLists.Xcp_Daq[(DaqListNumber)])

#else /* DAQ lists are dynamically configured */
#define XCP_GET_DAQ_LIST_PTR(DaqListNumber) Xcp_GetDaqListPtr(DaqListNumber)
#endif /* XCP_DAQ_CONFIG_TYPE == XCP_DAQ_STATIC_MASK */

/** \brief Divide the given size by the size of a DWORD (4 bytes) */
#define GET_BYTE_SIZE_IN_DWORDS(size) ((size)/sizeof(uint32))

/** \brief Returns the FIRST_PID in case the DTO Packets have an
 * Identification Field Type "absolute ODT number". */
#if (XCP_IDENTIFICATION_FIELD_TYPE == XCP_ABSOLUTE_IF_MASK)
#if (defined XCP_GET_FIRST_PID)
#error XCP_GET_FIRST_PID already defined
#endif
#define XCP_GET_FIRST_PID(ListParamPtr) ((ListParamPtr)->FirstPID)
#endif

#if ((XCP_DAQ_CONFIG_TYPE == XCP_DAQ_STATIC_MASK) || (XCP_MIN_DAQ != 0U))

#if (defined XCP_GET_DAQLIST_PROPERTIES)
#error XCP_GET_DAQLIST_PROPERTIES already defined
#endif
/** \brief Returns the properties of the specified DAQ List.   */
#define XCP_GET_DAQLIST_PROPERTIES(DaqCfgPtr) ((DaqCfgPtr)->Properties)

#if (defined XCP_GET_DAQLIST_MAX_ODT)
#error XCP_GET_DAQLIST_MAX_ODT already defined
#endif
/** \brief Returns the number of ODTs in the specified DAQ List.   */
#define XCP_GET_DAQLIST_MAX_ODT(DaqCfgPtr) ((DaqCfgPtr)->MaxOdt)

#if (defined XCP_GET_DAQLIST_MAX_ODT_ENTRIES)
#error XCP_GET_DAQLIST_MAX_ODT_ENTRIES already defined
#endif
/** \brief Returns the maximum number ODT entries in the specified DAQ List. */
#define XCP_GET_DAQLIST_MAX_ODT_ENTRIES(DaqCfgPtr) ((DaqCfgPtr)->MaxOdtEntries)

#if (defined XCP_IS_DAQ_DIRECTION)
#error XCP_IS_DAQ_DIRECTION already defined
#endif
/** \brief Checks whether the DAQ configuration supports specified direction */
#define XCP_IS_DAQ_DIRECTION(ListNumber, Direction) \
  (((Xcp_DaqListsDefault.Xcp_Daq[(ListNumber)].Properties & (Direction)) == (Direction))? TRUE:FALSE)

#if (defined XCP_IS_DAQ_DIRECTION_DAQ)
#error XCP_IS_DAQ_DIRECTION_DAQ already defined
#endif
/** \brief Checks whether the DAQ direction is configured as DAQ only  */
#define XCP_IS_DAQ_DIRECTION_DAQ(ListNumber) \
  ((XCP_DAQ_LIST_DIRECTION_DAQ == \
  (Xcp_DaqListsDefault.Xcp_Daq[(ListNumber)].Properties & XCP_DAQ_LIST_DIRECTION_MASK))? TRUE:FALSE)

#if (defined XCP_IS_DAQ_DIRECTION_STIM)
#error XCP_IS_DAQ_DIRECTION_STIM already defined
#endif
/** \brief Checks whether the DAQ direction is configured as STIM only  */
#define XCP_IS_DAQ_DIRECTION_STIM(ListNumber) \
  ((XCP_DAQ_LIST_DIRECTION_STIM == \
  (Xcp_DaqListsDefault.Xcp_Daq[(ListNumber)].Properties & XCP_DAQ_LIST_DIRECTION_MASK))? TRUE:FALSE)

#if (defined XCP_IS_DIRECTION_BOTH)
#error XCP_IS_DIRECTION_BOTH already defined
#endif
/** \brief Checks whether the direction of the DAQ is configured as BOTH */
#define XCP_IS_DIRECTION_BOTH(ListNumber) \
  ((XCP_DAQ_LIST_DIRECTION_DAQ_STIM ==\
  (Xcp_DaqListsDefault.Xcp_Daq[(ListNumber)].Properties & XCP_DAQ_LIST_DIRECTION_MASK))? TRUE:FALSE)

#if (defined XCP_IS_DAQLIST_EVENT_CHANNEL_FIXED)
#error XCP_IS_DAQLIST_EVENT_CHANNEL_FIXED already defined
#endif
/** \brief Checks whether the DAQ List event channel number is fixed */
#define XCP_IS_DAQLIST_EVENT_CHANNEL_FIXED(DaqCfgPtr) \
      ((XCP_MASK_DAQLIST_EVENT_FIXED == \
             ((DaqCfgPtr)->Properties & XCP_MASK_DAQLIST_EVENT_FIXED))?TRUE:FALSE)

#if (defined XCP_GET_DAQLIST_FIXED_EVENT_CHANNEL)
#error XCP_GET_DAQLIST_FIXED_EVENT_CHANNEL already defined
#endif
/** \brief Returns number of the fixed event channel to be used for the
 *   specified DAQ list.
 */
#define XCP_GET_DAQLIST_FIXED_EVENT_CHANNEL(DaqCfgPtr) \
  (((XCP_IS_DAQLIST_EVENT_CHANNEL_FIXED(DaqCfgPtr))== TRUE) ? \
  ((DaqCfgPtr))->EventId : XCP_INVALID_EVENT_CHANNEL)

#endif /* ((XCP_DAQ_CONFIG_TYPE == XCP_DAQ_STATIC_MASK) || (XCP_MIN_DAQ != 0)) */

#if (XCP_EVENT_PACKET_ENABLED == STD_ON)
#if (defined XCP_IS_EVENT_CTO_QUEUE_EMPTY)
#error XCP_IS_EVENT_CTO_QUEUE_EMPTY already defined
#endif
/** \brief Check whether command response CTO Queue is empty. */
#define XCP_IS_EVENT_CTO_QUEUE_EMPTY()\
                  ((0U == Xcp_EventCTOIndex)? TRUE:FALSE)

#if (defined XCP_GET_NEXT_EVENT_CTO_SIZE)
#error XCP_GET_NEXT_EVENT_CTO_SIZE already defined
#endif
/** \brief Returns the size of next command response Event CTO in Event CTO queue */
#define XCP_GET_NEXT_EVENT_CTO_SIZE() (Xcp_EventCTOQueue[0U].Length)
#endif

#if (defined XCP_GET_SESSION_CONFIGID)
#error XCP_GET_SESSION_CONFIGID already defined
#endif
/** \brief Returns the configuration ID. */
#if (XCP_STORE_DAQ_SUPPORTED == STD_ON)
#define XCP_GET_SESSION_CONFIGID()             Xcp_DaqLists.Xcp_StoreDaq.ConfigurationSessionID
#else
#define XCP_GET_SESSION_CONFIGID()             0U
#endif /* XCP_STORE_DAQ_SUPPORTED == STD_ON */

#if (XCP_DEV_ERROR_DETECT == STD_ON)

#if (defined XCP_DET_REPORT_ERROR)
#error XCP_DET_REPORT_ERROR already defined
#endif
/** \brief Report Xcp development error */
#define XCP_DET_REPORT_ERROR(ApiId,ErrorId)                            \
        ((void)Det_ReportError(XCP_MODULE_ID, XCP_INSTANCE_ID, (ApiId), (ErrorId)))

#endif /* if (XCP_DEV_ERROR_DETECT == STD_ON) */

#if (defined XCP_UINT16_FROM_PID)
  #error XCP_UINT16_FROM_PID already defined
#endif
/** \brief Create a uint16 value from the first two elements of uint8 array */
#define XCP_UINT16_FROM_PID(ptr) ((uint16)\
  ((uint16)(((uint16)(ptr)[XCP_UINT16_MSB_IDX]) << 8U) | (uint16)(ptr)[XCP_UINT16_LSB_IDX]))

#if (XCP_MAX_EVENT_CHANNEL != 0U)

#if (defined XCP_GET_EVENT_NO_OF_DAQLIST)
#error XCP_GET_EVENT_NO_OF_DAQLIST already defined
#endif
/** \brief Returns number of DAQ Lists currently associated with event channel.
*/
#define XCP_GET_EVENT_NO_OF_DAQLIST(ChannelNumber) \
                                    (Xcp_Event[(ChannelNumber)].DaqIdListCount)

#if (defined XCP_GET_EVENT_CHANNEL_CONSISTENCY)
#error XCP_GET_EVENT_CHANNEL_CONSISTENCY already defined
#endif
/** \brief Returns the consistency of the specified event channel. */
#define XCP_GET_EVENT_CHANNEL_CONSISTENCY(ChannelNumber) \
                 (Xcp_EventInfo[(ChannelNumber)].Flag & XCP_EVENT_CHANNEL_CONSISTENCY_MASK)

#endif /* XCP_MAX_EVENT_CHANNEL != 0U */

#if (XCP_MAX_DAQ != 0U)

#if (defined XCP_IS_DTO_QUEUE_EMPTY)
#error XCP_IS_DTO_QUEUE_EMPTY already defined
#endif
/** \brief Check whether command response DTO Queue is empty. */
#define XCP_IS_DTO_QUEUE_EMPTY()\
                ((0U == Xcp_DtoQueue.NoOfBytes)? TRUE:FALSE)

#if (defined XCP_IS_DAQLIST_FULLY_CONFIGURED)
#error XCP_IS_DAQLIST_FULLY_CONFIGURED already defined
#endif
/** \brief Check whether DAQ List is already configured with SET_DAQ_LIST_MODE
 *   command.
 */
#define XCP_IS_DAQLIST_FULLY_CONFIGURED(DaqListPtr) \
 (((uint8)((DaqListPtr)->Flags & (uint8)(XCP_MASK_DAQLIST_CONFIGURED)) == 0U)?\
        FALSE:TRUE )

#endif /* XCP_MAX_DAQ != 0U */

/*==================[type definitions]======================================*/

#if (XCP_EVENT_PACKET_ENABLED == STD_ON)
/* XCP event packet codes */
typedef enum
{
  XCP_EV_RESUME_MODE = 0x00,        /* Slave starting in RESUME mode */
  XCP_EV_CLEAR_DAQ  = 0x01,         /* The DAQ configuration in non-volatile
                                        memory has been cleared. */
  XCP_EV_STORE_DAQ  = 0x02,         /* The DAQ configuration has been stored
                                        into non-volatile memory.  */
  XCP_EV_STORE_CAL  = 0x03,         /* The calibration data has been stored
                                        into non-volatile memory.  */
  XCP_EV_CMD_PENDING = 0x05,        /* Slave requesting to restart time-out.  */
  XCP_EV_DAQ_OVERLOAD = 0x06,       /* DAQ processor overload. */
  XCP_EV_SESSION_TERMINATED = 0x07, /* Session terminated by slave device. */
  XCP_EV_TIME_SYNC = 0x08,          /* Transfer of externally triggered timestamp . */
  XCP_EV_STIM_TIMEOUT = 0x09,       /* Indication of a STIM timeout. */
  XCP_EV_SLEEP = 0x0A,              /* Slave entering SLEEP mode. */
  XCP_EV_WAKE_UP = 0x0B,            /* Slave leaving SLEEP mode. */
  XCP_EV_USER = 0x0E,               /* User-defined event. */
  XCP_EV_TRANSPORT = 0x0F           /* Transport layer specific event. */
}Xcp_EventPacketType;

/* EVENT_CTO information used while updating the EVENT_CTO queue. */
typedef struct
{
  uint8 EVENT_CTO[XCP_MAX_CTO];/* Array of EVENT_CTO  */
  uint8 Length;                /* Length of Event_CTO  */
} Xcp_EventCTOType;
#endif /* XCP_EVENT_PACKET_ENABLED == STD_ON */

#if (XCP_MAX_DAQ != 0U)

/* DTO information used while updating the DTO queue.*/
typedef struct
{
  uint16 Length;          /* The Length of the DTO */
  uint8 DTO[XCP_MAX_DTO]; /* The DTO */
} Xcp_DTOType;

/* DTO queue Type. */
typedef struct
{
  uint16 NoOfBytes;  /* Number of bytes used in DTO Queue */
  uint16 Head;       /* DTO queue header */
  uint16 Tail;       /* DTO queue tail */
  uint8 Queue[XCP_DTO_QUEUE_SIZE]; /* DTO queue */
} Xcp_DTOQueueType;

/* Xcp Event. */
typedef struct
{
  uint32 TimeCycleCounter;     /* Needed for Cyclic Event Channels ( TimeCycle > 0U ) */
                               /* Event Sampling Period / Cycle Counter * Xcp Main Function Period */
  P2VAR(Xcp_DaqIdType, TYPEDEF, XCP_VAR) DaqIdList; /* Pointer to the list of DAQ IDs
                                                                of this event */
  uint8 NextDaqIndex;         /* Next DAQ index to be sampled (applicable for DAQ/ODT consistency) */
  uint8 NextOdtIndex;          /* Next ODT to be sampled (applicable for ODT consistency) */
  uint8 DaqIdListCount;        /* The number of DAQ IDs of this event */
  boolean EventIsSet;          /* Flag indicating whether event is set or not */
} Xcp_EventType;

/* Event information in ROM. */
typedef struct
{
  uint32 CycleCounterMax;/* Event Channel Time Cycle Counter Max */
                         /* For cyclic events it is calculated:
                              ( TimeCycle * TimeUnit_in seconds ) / XcpMainFuncPeriod ). */
                         /* For normal events it is initialized to 1U */
  P2CONST(uint8, TYPEDEF, XCP_CONST) EventName; /* The name of the event */
  P2CONST(Xcp_DaqIdType, TYPEDEF, XCP_CONST) DaqIdList; /* Pointer to the list of DAQ IDs
                                                                of this event */
  uint8 DaqIdListCount;  /* The number of DAQ IDs of this event */
  uint8 EventNameLength; /* The length of the event name */
  uint8 Flag;            /* Consistency and STIM/DAQ, both direction combine
                                                    configured for this event */
  uint8 MaxDaqList;      /* Maximum number of DAQ supported for this event */
  uint8 Priority;        /* The priority of this event */
  uint8 TimeCycle;       /* Event Sampling Period / Cycle */
  uint8 TimeUnit;        /* Event Channel Time Cycle Unit */

} Xcp_EventInfoType;

/* Information related to each event in the queue
*/
typedef struct
{
  uint16 ID; /* The ID of the event */
  uint16 NextDaqIndex; /* Next DAQ index to be sampled (applicable for DAQ,
                          ODT consistency) */
  uint8 NextOdtIndex; /* Next ODT to be sampled
                         (applicable for ODT consistency) */
  uint8 Priority; /* The priority of the event */
} Xcp_EventProcessingInfoType;

/* STIM DTO Type */
typedef struct
{
  Xcp_DaqIdType DaqID;                      /* ID of the DAQ List */
  uint8 OdtIndex;                           /* Index in ODT List */
  uint8 Data[XCP_MAX_DTO - XCP_PID_LENGTH]; /* DTO queue */
} Xcp_StimDtoType;

#endif /* XCP_MAX_DAQ != 0U */

/* CTO information used while updating the CTO queue. */
typedef struct
{
  uint8 Length;               /* The Length of the CTO */
  uint8 CTO[XCP_MAX_CTO_ABS]; /* The CTO */
} Xcp_CTOType;

/* Command queue Type. */
typedef struct
{
  uint8 NoOfCmds; /* Number of commands in queue */
  uint8 Head;     /* Command queue header */
  uint8 Tail;     /* Command queue tail */
  Xcp_CTOType Queue[XCP_CMD_PROCESSOR_QUEUE_SIZE]; /* Command queue */
} Xcp_CMDQueueType;

/* Number of bytes used in CTO Queue. Can hold MAX_CTO + the length information */
#if (XCP_MAX_CTO_ABS == 0xFF)
  typedef uint16 Xcp_CtoNoOfBytesType;
#else
  typedef uint8 Xcp_CtoNoOfBytesType;
#endif

/* CTO queue Type. */
typedef struct
{
  Xcp_CtoNoOfBytesType NoOfBytes;     /* Number of bytes in CTO Queue */
  Xcp_CtoNoOfBytesType ReservedBytes; /* Number of reserved bytes in CTO Queue */
  Xcp_CtoNoOfBytesType Head;          /* CTO queue header */
  Xcp_CtoNoOfBytesType Tail;          /* CTO queue tail */
  uint8 Queue[XCP_CTO_QUEUE_SIZE]; /* CTO queue */
} Xcp_CTOQueueType;

/** \brief Command processor's states */
typedef uint8 Xcp_CmdProcessorStateType;

/** \brief Status information of the slave.
 * All current status information of the slave device: this includes the status of the resource
 * protection, pending store requests, general status of data acquisition and stimulation */
typedef struct
{
#if (XCP_RESOURCE_DAQ == XCP_RESOURCE_DAQ_MASK)
  uint16 NoOfSelectedDaq; /** Number of DAQ lists in selected mode */
  uint16 NoOfRunningDaq;  /** Number of DAQ lists in running mode */
#endif
  uint8 SessionStatus;    /** Bit7: Slave is in RESUME mode or not.
                           ** Bit6: Whether a DAQ list is currently running
                           ** Bit5: RESERVED
                           ** Bit3: Request to clear DAQ configuration pending.
                           ** Bit2: Request to store DAQ list pending.
                           ** Bit0: Request to store CALibration data pending.
                           **/
  uint8 ProtectionStatus; /** Bit4: CAL/PAG commands are protected or not.
                           ** Bit3: DAQ list commands are protected or not.
                           ** Bit2: STIM list commands are protected or not.
                           ** Bit0: Programming commands are protected or not.
                           **/
} Xcp_StatusType;


/*==================[external function declarations]========================*/

#define XCP_START_SEC_CODE
#include <MemMap.h>

#if (XCP_MAX_DAQ != 0U)

/** \brief Function to Reset the DAQ Lists and Events
 **
 ** This function resets Running, Selected and Configured
 ** status of all DAQ Lists in XCP slave after a disconnect. */
extern FUNC(void, XCP_CODE) Xcp_ResetEventProcessor(void);

/** \brief Function to Reset a DAQ List
 **
 ** This function resets the configured status, running status
 ** and selected status of a DAQ List in XCP Slave
 **
 ** \param[in] DaqListNumber  The DAQ List number to be reset
 */
extern FUNC(void, XCP_CODE) Xcp_ResetDaqListData(Xcp_DaqIdType DaqListNumber);

/** \brief Function to process the highest priority set event
 **
 ** This function implements the Event processor state machine.
 ** This function shall be invoked cyclically. */
extern FUNC(void, XCP_CODE) Xcp_ProcessEvents(void);

/** \brief Function to dequeue DTO packets to transmit buffer.
 **
 ** Use this function to dequeue DTO packets to specified
 ** transmit buffer.
 **
 ** \param[in]      SpaceAvailable Available space in transmit buffer
 ** \param[inout]   TxLengthPtr Pointer to the transmit data length buffer to which
 **                 copied number of bytes will be updated.
 ** \param[out]     TxBufferPtr Pointer to the transmit buffer to which DTO shall be copied.
 **
 ** \return DTO copy status
 ** \retval XCP_E_OK     One DTO has been copied to TxBufferPtr
 ** \retval XCP_E_NOT_OK Space not enough to copy another DTO or
 **                      No DTO available to transmit */
extern FUNC(Xcp_ErrorType, XCP_CODE) Xcp_GetDTO
(
  P2VAR(uint8, AUTOMATIC, XCP_APPL_DATA)TxBufferPtr,
  P2VAR(PduLengthType, AUTOMATIC, XCP_APPL_DATA)TxLengthPtr,
  const PduLengthType SpaceAvailable
);

/** \brief Function to flush all pending response DTOs
 **
 ** Use this function to flush all pending response DTOs.
 ** Queue head, tail and number of elements will be reset
 ** to 0x00U. */
extern FUNC(void, XCP_CODE) Xcp_FlushDTOQueue(void);

/** \brief Function to find the DaqId associated with a DTO in DTO Queue
 **
 ** This function returns the DaqId associated with a DTO in DTO Queue
 ** when the pointer to the DTO entry is passed as the parameter.
 ** The Daq Id is found based on the identification type used.
 **
 ** \param[inout] DtoPtr  The pointer to the DTO entry in DTO Queue
 ** \param[out] DaqID     The Daq Id associated with the DTO
 ** \param[out] OdtIndex  The ODT Index associated with the DTO
 */
extern FUNC(void, XCP_CODE) Xcp_GetDaqIDFromDto
(
  P2CONST(uint8, AUTOMATIC, XCP_APPL_DATA) DtoPtr,
  P2VAR(Xcp_DaqIdType, AUTOMATIC, AUTOMATIC) DaqID,
  P2VAR(uint8, AUTOMATIC, AUTOMATIC) OdtIndex
);

/** \brief Function to clear all stopped DTOs from Queue.
 **
 ** Use this function to clear all pending DTOs associated with
 ** inactive DAQ Lists so that the trasmission of the DTOs
 ** is stopped.
 **/
extern FUNC(void, XCP_CODE) Xcp_ClearStoppedDtos(void);

/** \brief Function to reset all event channels runtime information
 **
 ** Use this function to reset all event channels: event channel is not set, next DAQ and ODT index
 ** to be processed is 0  */
extern FUNC(void, XCP_CODE) Xcp_ResetEventChannels(void);

#endif /* #if (XCP_MAX_DAQ != 0U) */

#if ((XCP_MASTER_BLOCK_MODE_SUPPORTED == STD_ON) || \
     (XCP_MASTER_BLOCK_MODE_PGM_SUPPORTED == STD_ON))

/** \brief Function to return whether a master block mode is currently in progress
 **
 ** \return TRUE if master block mode is currently in progress, FALSE otherwise
 **  */
extern FUNC(boolean, XCP_CODE) Xcp_IsMasterBlockModeInProgress(void);

#endif /* (XCP_MASTER_BLOCK_MODE_SUPPORTED == STD_ON) ||
          (XCP_MASTER_BLOCK_MODE_PGM_SUPPORTED == STD_ON) */


#if (XCP_MASTER_BLOCK_MODE_PGM_SUPPORTED == STD_ON)

/** \brief Function to return whether a programming is currently in block mode
 **
 ** \return TRUE if programming is in block mode, FALSE otherwise
 **  */
extern FUNC(boolean, XCP_CODE) Xcp_IsProgrammingBlockMode(void);

/** \brief Function to mark the beginning of an block mode programming
 **
 ** This means, that from now on, only PROGRAM_NEXT commands are accepted.
 **  */
FUNC(void, XCP_CODE) Xcp_SetProgrammingBlockMode(void);

#endif /* XCP_MASTER_BLOCK_MODE_SUPPORTED == STD_ON */


#if (XCP_MASTER_BLOCK_MODE_SUPPORTED == STD_ON)

/** \brief Function to return whether a download in block mode is currently active
 **
 ** Use this function to check if the block mode transfer for DOWNLOAD is currently active. Use this
 ** to validate incoming commands.
 **
 ** \retval TRUE   Download is in block mode
 ** \retval FALSE  Download is not in block mode
 ** \return TRUE if download is in block mode transfer, FALSE otherwise
 **  */
extern FUNC(boolean, XCP_CODE) Xcp_IsDownloadInBlockMode(void);

/** \brief Function to set the download in block mode flag
 **
 ** The function will set the download in block mode flag - meaning that we just received a DOWNLOAD
 ** command and we can accept further DOWNLOAD_NEXT commands
 **  */
extern FUNC(void, XCP_CODE) Xcp_SetDownloadInBlockMode(void);

#endif /* XCP_MASTER_BLOCK_MODE_SUPPORTED == STD_ON */

#if (XCP_EVENT_PACKET_ENABLED == STD_ON)
/** \brief Function dequeue Event CTO packets to transmit buffer.
 **
 ** Use this function to dequeue Event CTO packets to specified
 ** transmit buffer.
 **
 ** \param[in]      SpaceAvailable Available space in transmit buffer.
 ** \param[inout]   TxLengthPtr Pointer to the transmit data length buffer to which
 **                 copied number of bytes will be updated.
 ** \param[out]     TxBufferPtr Pointer to the transmit buffer to which CTO will be copied.
 **
 ** \return Event CTO copy status
 ** \retval XCP_E_OK     One Event CTO has been copied to TxBufferPtr
 ** \retval XCP_E_NOT_OK Space not enough to copy another Event CTO or
 **                      No Event CTO available to transmit */
extern FUNC(Xcp_ErrorType, XCP_CODE) Xcp_GetEventCTO
(
  P2VAR(uint8, AUTOMATIC, XCP_APPL_DATA)TxBufferPtr,
  P2VAR(PduLengthType, AUTOMATIC, XCP_APPL_DATA)TxLengthPtr,
  const PduLengthType SpaceAvailable
);

/** \brief Function to flush all pending event CTOs
 **
 ** Use this function flush all pending event CTOs.
 ** Flags for all event codes will be cleared. */
extern FUNC(void, XCP_CODE) Xcp_FlushEventCTOQueue(void);

#endif /* XCP_EVENT_PACKET_ENABLED == STD_ON */

#if (XCP_ON_FLEXRAY_ENABLED == STD_ON)

/** \brief Function to get the data to be passed through TriggerTransmit
 ** function
 **
 ** Use this function to get the data to be passed through TriggerTransmit
 ** function.
 **
 ** \param[out] PduPtr The address of the buffer in which data needs to be
 ** copied */
extern FUNC(void, XCP_CODE)Xcp_ProvideTxData
(
  P2VAR(PduInfoType, AUTOMATIC, XCP_APPL_DATA)PduPtr
);

/** \brief Function to determine whether XCP transmit processor is waiting for
 ** a transmit trigger.
 **
 ** This functions checks whether the transmit processor state machine is
 ** expecting transmit trigger invocation for a previous transmit request. Use
 ** this function to ward off any unexpected trigger requests from the lower
 ** layer.
 **
 ** \return State machine status
 ** \retval TRUE    Transmit processor is expecting a transmit trigger.
 ** \retval FALSE   Transmit processor is not expecting a transmit trigger. */
extern FUNC(boolean, XCP_CODE) Xcp_IsWaitingForTrigger(void);

#endif /* XCP_ON_FLEXRAY_ENABLED == STD_ON */

#if (XCP_RESOURCE_STIM == XCP_RESOURCE_STIM_MASK)

/** \brief Function to insert an incoming DTO packet into the STIM Buffer
 **
 ** This function is used to check an incoming DTO packet and insert
 ** it into the STIM Buffer.
 **
 ** \param[in] SduLength  DTO length to be processed.
 ** \param[in] SduDataPtr  DTO packet data to be processed. */
extern FUNC(void, XCP_CODE) Xcp_InsertSTIM
(
  PduLengthType SduLength,
  P2CONST(uint8, AUTOMATIC, XCP_APPL_DATA) SduDataPtr
);

/** \brief Function to process the stimulation of data
 **
 ** This function is used to stimulate the data stored in the STIM Buffer
 ** for one or more ODTs of one or more DAQ Lists based on consistency
 **
 ** \param[in] EventID      Event ID to process
 ** \param[in] EvDaqIndex   Index of the DAQ in the DAQ List
 ** \param[in] OdtIndex     The ODT Index */
extern FUNC(void, XCP_CODE) Xcp_StimulateData
(
  uint16 EventID,
  uint8 EvDaqIndex,
  uint8 OdtIndex
);

/** \brief Function to check if all ODTs for a DAQ are present
 ** in the STIM Buffer.
 **
 ** This function is used to search the STIM buffer to check whether the data
 ** for all ODTs of a DAQ is present in the buffer.
 **
 ** \param[in] DaqID        The Daq Id
 **
 ** \return Query status
 ** \retval E_OK      All ODTs are in the STIM buffer.
 ** \retval E_NOT_OK  Not all ODTs are in the STIM buffer. */
extern FUNC(Std_ReturnType, XCP_CODE) Xcp_SearchDaqInStimBuffer(Xcp_DaqIdType DaqID);

/** \brief Function to check if an ODT is present in the STIM Buffer.
 **
 ** This function is used to search the STIM buffer to check whether the data
 ** for an ODT is present in the buffer.
 **
 ** \param[in] DaqID        The Daq Id
 ** \param[in] OdtIndex     The ODT Index
 ** \param[out] Position    Position of the ODT in the Buffer
 **
 ** \return Query status
 ** \retval E_OK      The ODT is present in the STIM buffer.
 ** \retval E_NOT_OK  The ODT is not present in the STIM buffer. */
extern FUNC(Std_ReturnType, XCP_CODE) Xcp_SearchOdtInStimBuffer
(
  Xcp_DaqIdType DaqID,
  uint8 OdtIndex,
  P2VAR(uint16, AUTOMATIC, XCP_VAR) Position
);

/** \brief Function to flush all pending DTO entries from STIM buffer
 **
 ** This function is used to remove all entries from the STIM buffer The
 ** number of elements will be reset to zero. */
extern FUNC(void, XCP_CODE) Xcp_FlushStimBuffer(void);

#endif /* XCP_RESOURCE_STIM == XCP_RESOURCE_STIM_MASK */

#if (XCP_STORE_DAQ_SUPPORTED == STD_ON)
/**
 ** \brief Function to prepare the selected stored DAQ lists
 **  The function will determine which DAQ lists were selected to be stored in NV memory and will
 **  make them ready to be sampled.
 **  */
extern FUNC(void, XCP_CODE) Xcp_PrepareStoredDaqLists(void);

/**
 ** \brief Function to trigger the resume mode.
 **  The function will determine which DAQ lists need to be started in resume mode, prepare them for
 **  immediate transmission and queue their associated events.
 **  */
extern FUNC(void, XCP_CODE) Xcp_PrepareStoredDaqListsResume(void);

#endif /* XCP_STORE_DAQ_SUPPORTED == STD_ON */

#if (XCP_DAQ_CONFIG_TYPE == XCP_DAQ_DYNAMIC_MASK)

/**
 ** \brief Function to return a pointer to a DAQ list from the corresponding area where it is stored.
 **
 **  This function will return a pointer to the DAQ list corresponding to the received DAQ list number
 **  from two possible areas, depending on the configuration type:
 **   - if it is predefined, from the Xcp_DaqLists.Xcp_NonDynamicDaqLists structure
 **   - if it is dynamic, from the area allocated for dynamic DAQ lists
 **
 **  If the configuration type is static, this function will be available as a macro, as then there
 **  would be only possible area from where to fetch the DAQ list.
 **
 **  \param[in] DaqListNumber   The DAQ list number based upon the DAQ list is retrieved.
 **
 ** \return A pointer to the DAQ list found.
 **  */
extern FUNC(P2VAR(Xcp_DaqType, AUTOMATIC, XCP_VAR), XCP_CODE) Xcp_GetDaqListPtr
(
  Xcp_DaqIdType DaqListNumber
);

/**
 ** \brief Function to return the number of dynamically configured DAQ lists.
 **
 ** If the dynamic DAQ list configuration is invalid (e.g. ERR_MEMORY_OVERFLOW was thrown during
 ** the configuration), this function will return 0.
 **
 ** \return The number of dynamically configured DAQ lists.
 **  */
extern FUNC(Xcp_DaqIdType, XCP_CODE) Xcp_GetNoOfDynamicDaqLists( void );

#endif /* XCP_DAQ_CONFIG_TYPE == XCP_DAQ_DYNAMIC_MASK */

#if (XCP_MAX_CYCLIC_EVENT_CHANNEL > 0U)
/** \brief Sets all the cyclic events
 **/
extern FUNC(void, XCP_CODE) Xcp_SetCyclicEvents( void );

#endif /* XCP_MAX_CYCLIC_EVENT_CHANNEL > 0U */

#if (XCP_ON_ETHERNET_ENABLED == STD_ON)
/** \brief Initializes the Ethernet frame counter to 0. */
extern FUNC(void, XCP_CODE) Xcp_InitEthernetFrameCtr(void);

#endif /* XCP_ON_ETHERNET_ENABLED == STD_ON */

/**
 ** \brief Function to perform an XCP disconnect.
 **  The function will terminate the current XCP session and
 **  reset internal data structures and sub processors.
 **  */
extern FUNC(void, XCP_CODE) Xcp_Disconnect(void);

/** \brief Function to get the command processor current state
 **
 ** \return The state of the command processor
 ** \retval XCP_CMDPROCESSOR_IDLE      The command processor's state is IDLE
 ** \retval XCP_CMDPROCESSOR_BUSY      The command processor's state is BUSY
 **
 */
extern FUNC(Xcp_CmdProcessorStateType, XCP_CODE) Xcp_GetCommandProcessorState(void);

#define XCP_STOP_SEC_CODE
#include <MemMap.h>

/*==================[internal function declarations]========================*/

/*==================[external constants]====================================*/

#define XCP_START_SEC_CONST_UNSPECIFIED
#include <MemMap.h>

#if (XCP_MAX_EVENT_CHANNEL != 0U)
/** \brief Event Information */
extern CONST(Xcp_EventInfoType, XCP_CONST) Xcp_EventInfo[XCP_MAX_EVENT_CHANNEL];
#endif

#if ((XCP_MAX_DAQ != 0U) && (XCP_MAX_DAQ > XCP_DAQ_COUNT)) /* If we have static or predefined DAQ lists */
extern CONST(Xcp_NonDynamicDaqListsType, XCP_CONST) Xcp_DaqListsDefault;
#endif

#define XCP_STOP_SEC_CONST_UNSPECIFIED
#include <MemMap.h>

/*==================[internal constants]====================================*/

/*==================[external data]=========================================*/

#define XCP_START_SEC_VAR_UNSPECIFIED
#include <MemMap.h>

#if (XCP_MAX_EVENT_CHANNEL != 0U)
/** \brief Event Details */
extern VAR(Xcp_EventType, XCP_VAR)
  Xcp_Event[XCP_MAX_EVENT_CHANNEL];
#endif

#define XCP_STOP_SEC_VAR_UNSPECIFIED
#include <MemMap.h>


#define XCP_START_SEC_VAR_NOINIT_UNSPECIFIED
#include <MemMap.h>

#if (XCP_MAX_DAQ != 0U)
/** \brief Queue for holding the DTOs to be transmitted. */
extern VAR(Xcp_DTOQueueType,  XCP_VAR_NOINIT) Xcp_DtoQueue;
#endif

#if (XCP_RESOURCE_STIM == XCP_RESOURCE_STIM_MASK)
/** \brief Buffer for holding all incoming STIM data */
extern VAR(Xcp_StimDtoType,  XCP_VAR_NOINIT) Xcp_StimDtoBuffer[XCP_STIM_BUFFER_SIZE];
#endif

/** \brief All current status information of the slave device. */
extern VAR(Xcp_StatusType,  XCP_VAR_NOINIT) Xcp_Status;

#define XCP_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#include <MemMap.h>

#define XCP_START_SEC_VAR_NOINIT_8BIT
#include <MemMap.h>

#if (XCP_EVENT_PACKET_ENABLED == STD_ON)
/** \brief Definition of Event_CTO queue.
 *
 * The Event_CTO queue is implemented as byte array. */
extern VAR(Xcp_EventCTOType, XCP_VAR_NOINIT) Xcp_EventCTOQueue[XCP_EV_CTO_QUEUE_SIZE];
#endif /* XCP_EVENT_PACKET_ENABLED == STD_ON */

#if (XCP_STORE_DAQ_SUPPORTED == STD_ON)

/** \brief The flag that determines whether we are in resume mode and we are waiting for the
 * event packet EV_RESUME_MODE to be transmitted. */
extern VAR(boolean, XCP_VAR_NOINIT) Xcp_EvResumeModeTransmitPending;

#endif /* XCP_STORE_DAQ_SUPPORTED == STD_ON */

#define XCP_STOP_SEC_VAR_NOINIT_8BIT
#include <MemMap.h>


#define XCP_START_SEC_VAR_16BIT
#include <MemMap.h>

/* If event packet transmission enabled */
#if (XCP_EVENT_PACKET_ENABLED == STD_ON)
/** \brief variable to hold the current index of Event CTO Queue. */
extern VAR(uint16, XCP_VAR)Xcp_EventCTOIndex;
#endif

#if (XCP_RESOURCE_STIM == XCP_RESOURCE_STIM_MASK)
/** \brief Number of entries in the STIM buffer */
extern VAR(uint16, XCP_VAR) Xcp_StimDtoCount;
#endif

#define XCP_STOP_SEC_VAR_16BIT
#include <MemMap.h>

/*==================[internal data]=========================================*/

/*==================[external function definitions]=========================*/

/*==================[internal function definitions]=========================*/

/*==================[end of file]===========================================*/

#endif /* if !defined( XCP_INT_H ) */
