/**
 * \file
 *
 * \brief AUTOSAR Xcp
 *
 * This file contains the implementation of the AUTOSAR
 * module Xcp.
 *
 * \author Elektrobit Automotive GmbH, 91058 Erlangen, Germany
 *
 * Copyright 2005 - 2013 Elektrobit Automotive GmbH
 * All rights exclusively reserved for Elektrobit Automotive GmbH,
 * unless expressly agreed to otherwise.
 */


/*==================[inclusions]============================================*/

#include <Xcp_Trace.h>
#include <Std_Types.h>        /* AUTOSAR standard types */
#include <TSAutosar.h>        /* EB specific standard types */

#include <Xcp.h>              /* Module public API */
#include <Xcp_Int.h>          /* Internal types */
#include <TSMem.h>            /* EB memory functions */
#include <Xcp_Cbk.h>          /* Callbacks and callouts of Xcp */

#if (XCP_RESOURCE_CAL_PAG == XCP_RESOURCE_CAL_PAG_MASK)
#include <Xcp_UserCallouts.h> /* User Callout Function declarations */
#endif

#include <SchM_Xcp.h>           /* Needed for exclusive area definition */

/*==================[macros]================================================*/

/*------------------[AUTOSAR vendor identification check]-------------------*/

#if (!defined XCP_VENDOR_ID) /* configuration check */
#error XCP_VENDOR_ID must be defined
#endif

#if (XCP_VENDOR_ID != 1U) /* vendor check */
#error XCP_VENDOR_ID has wrong vendor id
#endif

/*------------------[AUTOSAR release version identification check]----------*/

#if (!defined XCP_AR_RELEASE_MAJOR_VERSION) /* configuration check */
#error XCP_AR_RELEASE_MAJOR_VERSION must be defined
#endif

/* major version check */
#if (XCP_AR_RELEASE_MAJOR_VERSION != 4U)
#error XCP_AR_RELEASE_MAJOR_VERSION wrong (!= 4U)
#endif

#if (!defined XCP_AR_RELEASE_MINOR_VERSION) /* configuration check */
#error XCP_AR_RELEASE_MINOR_VERSION must be defined
#endif

/* minor version check */
#if (XCP_AR_RELEASE_MINOR_VERSION != 0U )
#error XCP_AR_RELEASE_MINOR_VERSION wrong (!= 0U)
#endif

#if (!defined XCP_AR_RELEASE_REVISION_VERSION) /* configuration check */
#error XCP_AR_RELEASE_REVISION_VERSION must be defined
#endif

/* revision version check */
#if (XCP_AR_RELEASE_REVISION_VERSION != 3U )
#error XCP_AR_RELEASE_REVISION_VERSION wrong (!= 3U)
#endif

/*------------------[AUTOSAR module version identification check]-----------*/

#if (!defined XCP_SW_MAJOR_VERSION) /* configuration check */
#error XCP_SW_MAJOR_VERSION must be defined
#endif

/* major version check */
#if (XCP_SW_MAJOR_VERSION != 2U)
#error XCP_SW_MAJOR_VERSION wrong (!= 2U)
#endif

#if (!defined XCP_SW_MINOR_VERSION) /* configuration check */
#error XCP_SW_MINOR_VERSION must be defined
#endif

/* minor version check */
#if (XCP_SW_MINOR_VERSION < 3U)
#error XCP_SW_MINOR_VERSION wrong (< 3U)
#endif

#if (!defined XCP_SW_PATCH_VERSION) /* configuration check */
#error XCP_SW_PATCH_VERSION must be defined
#endif

/* patch version check */
#if (XCP_SW_PATCH_VERSION < 0U)
#error XCP_SW_PATCH_VERSION wrong (< 0U)
#endif

/*==================[type definitions]======================================*/

/*==================[external function declarations]========================*/

/*==================[internal function declarations]========================*/

#define XCP_START_SEC_CODE
#include <MemMap.h>
/** \brief XCP PDU transmit confirmation.
 **
 ** This function is a common function used by all TX confirmations for different
 ** communication buses when an AUTOSAR XCP PDU has been transmitted.
 **
 ** \param[in]  XcpTxPduId  PDU-ID that has been transmitted.
 **
 ** \Reentrancy{non-Reentrant}
 ** \Synchronicity{Synchronous}
 ** */
STATIC FUNC(void, XCP_CODE) Xcp_CommonTxConfirmation(PduIdType XcpTxPduId);

/** \brief Function to handle the XCP PDU when it is received.
 **
 ** This function is a common function used by all Rx indications for different
 ** communication buses when an AUTOSAR XCP PDU has been received.
 **
 ** \param[in]  XcpRxPduId  PDU-ID that has been received.
 ** \param[in]  XcpRxPduPtr  Contains the length (SduLength) of the received I-PDU and a pointer to
 **                          a buffer (SduDataPtr) containing the I-PDU.
 **
 ** \Reentrancy{non-Reentrant}
 ** \Synchronicity{Synchronous}
 ** */
STATIC FUNC(void, XCP_CODE) Xcp_CommonRxIndication
(
  PduIdType XcpRxPduId,
  P2VAR(PduInfoType, AUTOMATIC, XCP_APPL_DATA) XcpRxPduPtr
);

#if (XCP_MAX_DAQ != 0U)
/** \brief Function used to initialize all DAQ lists
 **
 ** The function will initialize defaults data from ROM. The initialization is done only
 ** if the data was not initialized from the NV memory by the NvM_ReadAll() service (if DAQ storage
 ** is supported and requested).
 **
 ** \return Result of the operation.
 ** \retval E_OK if the DAQ lists were initialized with no error
 ** \retval E_NOT_OK if the DET error XCP_E_INVALID_NVM_BLOCK_LENGTH is thrown.
 **
 ** */
STATIC FUNC(Std_ReturnType, XCP_CODE) Xcp_InitializeDaqLists(void);
#endif

#define XCP_STOP_SEC_CODE
#include <MemMap.h>

/*==================[external constants]====================================*/

/*==================[internal constants]====================================*/

/*==================[external data]=========================================*/

#define XCP_START_SEC_VAR_UNSPECIFIED
#include <MemMap.h>

/* Connection status of XCP module */
VAR(Xcp_StateType, XCP_VAR) Xcp_State = XCP_STATE_DISCONNECTED;

#define XCP_STOP_SEC_VAR_UNSPECIFIED
#include <MemMap.h>


#define XCP_START_SEC_VAR_16BIT
#include <MemMap.h>

#if (XCP_RESOURCE_STIM == XCP_RESOURCE_STIM_MASK)
/* Number of entries in the STIM buffer */
VAR(uint16, XCP_VAR) Xcp_StimDtoCount = 0U;
#endif

#define XCP_STOP_SEC_VAR_16BIT
#include <MemMap.h>


#define XCP_START_SEC_VAR_8BIT
#include <MemMap.h>

/* Initialization status of the XCP module */
VAR(boolean, XCP_VAR) Xcp_Initialized = FALSE;

#define XCP_STOP_SEC_VAR_8BIT
#include <MemMap.h>


/*==================[NOINIT external data]=========================================*/

#define XCP_START_SEC_VAR_NOINIT_UNSPECIFIED
#include <MemMap.h>

#if (XCP_MAX_DAQ != 0U)
/* Queue for holding the DTOs to be transmitted */
VAR(Xcp_DTOQueueType, XCP_VAR_NOINIT) Xcp_DtoQueue;
#endif

#if (XCP_RESOURCE_STIM == XCP_RESOURCE_STIM_MASK)
/* Buffer for holding all incoming STIM data */
VAR(Xcp_StimDtoType, XCP_VAR_NOINIT) Xcp_StimDtoBuffer[XCP_STIM_BUFFER_SIZE];
#endif

#define XCP_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#include <MemMap.h>



#define XCP_START_SEC_VAR_NOINIT_8BIT
#include <MemMap.h>

/* status flag for Tx confirmation callback */
VAR(boolean, XCP_VAR_NOINIT) Xcp_TxConfCallbackReceived;


#if (XCP_TX_TRIGGER_ENABLED == STD_ON)
/* status flag for Trigger callback */
VAR(boolean, XCP_VAR_NOINIT) Xcp_TriggerCallbackReceived;
#endif

#define XCP_STOP_SEC_VAR_NOINIT_8BIT
#include <MemMap.h>

/*==================[internal data]=========================================*/

#if (XCP_ON_ETHERNET_ENABLED == STD_ON)
#if (XCP_SOAD_SOCKET_PROTOCOL_UDP == STD_OFF)
#define XCP_START_SEC_VAR_8BIT
#include <MemMap.h>

/** \brief Flag used to indicate the closing of an Ethernet TCP socket connection
 ** to the Xcp_MainFunction() */
STATIC VAR(boolean, XCP_VAR_POWER_ON_INIT) Xcp_EthernetConnectionClosed = FALSE;

#define XCP_STOP_SEC_VAR_8BIT
#include <MemMap.h>

#endif /* XCP_SOAD_SOCKET_PROTOCOL_UDP == STD_OFF */
#endif /* XCP_ON_ETHERNET_ENABLED == STD_ON */

/*==================[external function definitions]=========================*/

#define XCP_START_SEC_CODE
#include <MemMap.h>

/*------------------[Xcp_GetVersionInfo]-------------------------------------*/
#if (XCP_VERSION_INFO_API == STD_ON)

FUNC(void, XCP_CODE) Xcp_GetVersionInfo
(
  P2VAR(Std_VersionInfoType, AUTOMATIC, XCP_APPL_DATA) VersionInfoPtr
)
{
  DBG_XCP_GETVERSIONINFO_ENTRY(VersionInfoPtr);

#if (XCP_DEV_ERROR_DETECT == STD_ON)
  if (NULL_PTR == VersionInfoPtr)
  {
    XCP_DET_REPORT_ERROR(XCP_SID_GET_VERSION_INFO, XCP_E_INV_POINTER);
  }
  else
#endif
  {
    VersionInfoPtr->vendorID         = XCP_VENDOR_ID;
    VersionInfoPtr->moduleID         = XCP_MODULE_ID;
    VersionInfoPtr->sw_major_version = XCP_SW_MAJOR_VERSION;
    VersionInfoPtr->sw_minor_version = XCP_SW_MINOR_VERSION;
    VersionInfoPtr->sw_patch_version = XCP_SW_PATCH_VERSION;
  }

  DBG_XCP_GETVERSIONINFO_EXIT(VersionInfoPtr);
}

#endif

/*------------------[Xcp_Init]-----------------------------------------------*/
FUNC(void,XCP_CODE) Xcp_Init
(
  P2CONST(Xcp_ConfigType, AUTOMATIC, XCP_APPL_CONST) Xcp_ConfigPtr
)
{
  DBG_XCP_INIT_ENTRY(Xcp_ConfigPtr);

  /* Present implementation doesn't have any post build parameters */
  TS_PARAM_UNUSED(Xcp_ConfigPtr);

  /* initialize flag for received Tx confirmation */
  Xcp_TxConfCallbackReceived = FALSE;
#if (XCP_TX_TRIGGER_ENABLED == STD_ON)
  /* initialize flag for received trigger transmit callback */
  Xcp_TriggerCallbackReceived = FALSE;
#endif

#if (XCP_ON_ETHERNET_ENABLED == STD_ON)

  /* Initialize frame transmit counter of XCP frame header */
  Xcp_InitEthernetFrameCtr();

#if (XCP_SOAD_AUTO_OPEN_SO_CON == STD_ON)
  /* Open SoAd socket connection(s) for XCP */
  if (SoAd_OpenSoCon( XCP_SOAD_SOCKET_RX_ID ) == E_OK)
  {
    /* OK */
  }
  /* CHECK: NOPARSE */
  else
  {
    /* SoAd_OpenSoCon() cannot return E_NOT_OK if the configuration is set correctly. */
    XCP_UNREACHABLE_CODE_ASSERT(XCP_SID_INIT);
  }
  /* CHECK: PARSE */
#if (XCP_SOAD_SOCKET_PROTOCOL_UDP == STD_ON)
  if (SoAd_OpenSoCon( XCP_SOAD_SOCKET_TX_ID ) == E_OK)
  {
    /* OK */
  }
  /* CHECK: NOPARSE */
  else
  {
    /* SoAd_OpenSoCon() cannot return E_NOT_OK if the configuration is set correctly */
    XCP_UNREACHABLE_CODE_ASSERT(XCP_SID_INIT);
  }
  /* CHECK: PARSE */
#endif /* XCP_SOAD_SOCKET_PROTOCOL_UDP == STD_ON */
#endif /* XCP_SOAD_AUTO_OPEN_SO_CON == STD_ON */

#endif /* XCP_ON_ETHERNET_ENABLED == STD_ON */

  /* Flush command queue */
  Xcp_FlushCMDQueue();
  /* Flush CTO Queue */
  Xcp_FlushCTOQueue();
/* If event packet transmission enabled */
#if (XCP_EVENT_PACKET_ENABLED == STD_ON)
  /* Flush XCP events CTOs */
  Xcp_FlushEventCTOQueue();
#endif

  /* Initialize command processor */
  Xcp_InitCommandProcessor();
  /* Initialize transmit processor */
  Xcp_InitTransmitProcessor();

  /* If CAL/PAG resource is supported */
#if (XCP_RESOURCE_CAL_PAG == XCP_RESOURCE_CAL_PAG_MASK)
  /* Initialize Calibration Segments and Pages */
  Xcp_ApplCalPagInit();
#endif /* (XCP_RESOURCE_CAL_PAG == XCP_RESOURCE_CAL_PAG_MASK) */

  /* If we have DAQ lists configured */
  #if (XCP_MAX_DAQ != 0U)

  /* If STIM resource is supported */
#if (XCP_RESOURCE_STIM == XCP_RESOURCE_STIM_MASK)
  /* Flush STIM Buffer */
  Xcp_FlushStimBuffer();
#endif /* (XCP_RESOURCE_STIM == XCP_RESOURCE_STIM_MASK) */

  /* Reset all event channels runtime information */
  Xcp_ResetEventChannels();
  /* Flush DTO Queue */
  Xcp_FlushDTOQueue();

#if (XCP_STORE_DAQ_SUPPORTED == STD_ON)
  /* Initialize the flag that determines whether we are in resume mode and we are waiting for the
   * event packet EV_RESUME_MODE to be transmitted. */
  Xcp_EvResumeModeTransmitPending = FALSE;
#endif

  /* Initialize DAQ lists */
  if (Xcp_InitializeDaqLists() == E_OK)
#endif /* XCP_MAX_DAQ != 0U */
  {
    /* Tag module as Initialized */
    Xcp_Initialized = TRUE;
  }

  DBG_XCP_INIT_EXIT(Xcp_ConfigPtr);
}

/*------------------[Xcp_MainFunction]---------------------------------------*/
FUNC(void,XCP_CODE) Xcp_MainFunction(void)
{
  DBG_XCP_MAINFUNCTION_ENTRY();
		
  /* Check whether XCP is initialized */
  if (Xcp_Initialized == TRUE)
  {
#if (XCP_ON_ETHERNET_ENABLED == STD_ON)
#if (XCP_SOAD_SOCKET_PROTOCOL_UDP == STD_OFF)

    /* If Ethernet socket connection has been closed */
    if (Xcp_EthernetConnectionClosed == TRUE)
    {
      /* Enter critical section */
      SchM_Enter_Xcp_SCHM_XCP_EXCLUSIVE_AREA_XCP_INTERNALS();

      /* Perform an XCP disconnect */
      Xcp_Disconnect();

      /* Exit critical section */
      SchM_Exit_Xcp_SCHM_XCP_EXCLUSIVE_AREA_XCP_INTERNALS();

      /* Reset flag */
      Xcp_EthernetConnectionClosed = FALSE;
    }

#endif /* XCP_SOAD_SOCKET_PROTOCOL_UDP == STD_OFF */
#endif /* XCP_ON_ETHERNET_ENABLED */

    /* Check if we have any async event flags set*/
    Xcp_HandleAsyncEventFlags();

    /* Execute Command processor */
    Xcp_ProcessCommandQueue(XCP_RUN_FROM_MAIN_FUNC_CONTEXT);

#if ((XCP_RESOURCE_DAQ == XCP_RESOURCE_DAQ_MASK) || (XCP_RESOURCE_STIM == XCP_RESOURCE_STIM_MASK))

#if (XCP_STORE_DAQ_SUPPORTED == STD_ON)
    /* Process the events only when the EV_RESUME_MODE is not pending for transmission so that the
     * data sampling to start only from the point where EV_RESUME_MODE is sent during resume mode */
    if (Xcp_EvResumeModeTransmitPending == FALSE)
#endif /* XCP_STORE_DAQ_SUPPORTED == STD_ON */
    {
#if (XCP_MAX_CYCLIC_EVENT_CHANNEL > 0U)
      /* Set all the Cyclic Events */
      Xcp_SetCyclicEvents();
#endif
      /* Execute Event processor */
      Xcp_ProcessEvents();
    }

#endif /* XCP_RESOURCE_DAQ == XCP_RESOURCE_DAQ_MASK || XCP_RESOURCE_STIM == XCP_RESOURCE_STIM_MASK */
    /* Execute Transmit processor */
    Xcp_ProcessTransmit();
  }

  DBG_XCP_MAINFUNCTION_EXIT();
}


#if (XCP_ON_CAN_ENABLED == STD_ON)

/*------------------[Xcp_CanIfRxIndication]----------------------------------*/

FUNC(void,XCP_CODE) Xcp_CanIfRxIndication
(
  PduIdType XcpRxPduId,
  P2VAR(PduInfoType, AUTOMATIC, XCP_APPL_DATA) XcpRxPduPtr
)
{
  DBG_XCP_CANIFRXINDICATION_ENTRY(XcpRxPduId,XcpRxPduPtr);

  Xcp_CommonRxIndication(XcpRxPduId, XcpRxPduPtr);

  DBG_XCP_CANIFRXINDICATION_EXIT(XcpRxPduId,XcpRxPduPtr);
}

/*------------------[Xcp_CanIfTxConfirmation]--------------------------------*/

FUNC(void, XCP_CODE) Xcp_CanIfTxConfirmation
(
  PduIdType XcpTxPduId
)
{
  DBG_XCP_CANIFTXCONFIRMATION_ENTRY(XcpTxPduId);

  Xcp_CommonTxConfirmation(XcpTxPduId);

  DBG_XCP_CANIFTXCONFIRMATION_EXIT(XcpTxPduId);
}

#endif /* XCP_ON_CAN_ENABLED == STD_ON */


#if (XCP_ON_ETHERNET_ENABLED == STD_ON)

/*------------------[Xcp_SoAdIfRxIndication]---------------------------------*/

FUNC(void,XCP_CODE) Xcp_SoAdIfRxIndication
(
  PduIdType XcpRxPduId,
  P2VAR(PduInfoType, AUTOMATIC, XCP_APPL_DATA) XcpRxPduPtr
)
{
  DBG_XCP_SOADIFRXINDICATION_ENTRY(XcpRxPduId,XcpRxPduPtr);

  Xcp_CommonRxIndication(XcpRxPduId, XcpRxPduPtr);

  DBG_XCP_SOADIFRXINDICATION_EXIT(XcpRxPduId,XcpRxPduPtr);
}

/*------------------[Xcp_SoAdIfTxConfirmation]-------------------------------*/

FUNC(void,XCP_CODE) Xcp_SoAdIfTxConfirmation
(
  PduIdType XcpTxPduId
)
{
  DBG_XCP_SOADIFTXCONFIRMATION_ENTRY(XcpTxPduId);

  Xcp_CommonTxConfirmation(XcpTxPduId);

  DBG_XCP_SOADIFTXCONFIRMATION_EXIT(XcpTxPduId);
}


#if (XCP_SOAD_SOCKET_PROTOCOL_UDP == STD_OFF)
/*------------------[Xcp_SoAdSoConModeChg]-----------------------------------*/

FUNC(void,XCP_CODE) Xcp_SoAdSoConModeChg
(
  SoAd_SoConIdType SoConId,
  SoAd_SoConModeType Mode
)
{
  DBG_XCP_SOADSOCONMODECHG_ENTRY(SoConId,Mode);
#if (XCP_DEV_ERROR_DETECT == STD_ON)
  /* Check whether XCP is initialized */
  if (Xcp_Initialized == FALSE)
  {
    XCP_DET_REPORT_ERROR(XCP_SID_SOAD_SO_CON_MODE_CHG, XCP_E_NOT_INITIALIZED);
  }
  else
#endif
  {
    /* If in XCP connected state */
    if (Xcp_State == XCP_STATE_CONNECTED)
    {
      /* If socket connection is configured for XCP */
      if (SoConId == XCP_SOAD_SOCKET_RX_ID) /* TCP uses the same socket to reception and transmission */
      {
        /* If XCP socket connection is not ONLINE */
        if (Mode != SOAD_SOCON_ONLINE)
        {
          /* Set flag */
          Xcp_EthernetConnectionClosed = TRUE;
        }
      }
    }
  }

  DBG_XCP_SOADSOCONMODECHG_EXIT(SoConId,Mode);
}


#endif /* XCP_SOAD_SOCKET_PROTOCOL_UDP == STD_OFF */
#endif /* XCP_ON_ETHERNET_ENABLED == STD_ON */


#if (XCP_ON_FLEXRAY_ENABLED == STD_ON)

/*------------------[Xcp_FrIfRxIndication]-----------------------------------*/

FUNC(void,XCP_CODE) Xcp_FrIfRxIndication
(
  PduIdType XcpRxPduId,
  P2VAR(PduInfoType, AUTOMATIC, XCP_APPL_DATA) XcpRxPduPtr
)
{
  DBG_XCP_FRIFRXINDICATION_ENTRY(XcpRxPduId,XcpRxPduPtr);

  Xcp_CommonRxIndication(XcpRxPduId, XcpRxPduPtr);

  DBG_XCP_FRIFRXINDICATION_EXIT(XcpRxPduId,XcpRxPduPtr);
}

/*------------------[Xcp_FrIfTxConfirmation]---------------------------------*/

FUNC(void,XCP_CODE) Xcp_FrIfTxConfirmation
(
  PduIdType XcpTxPduId
)
{
  DBG_XCP_FRIFTXCONFIRMATION_ENTRY(XcpTxPduId);

  Xcp_CommonTxConfirmation(XcpTxPduId);

  DBG_XCP_FRIFTXCONFIRMATION_EXIT(XcpTxPduId);
}

/*------------------[Xcp_FrIfTriggerTransmit]--------------------------------*/

FUNC(Std_ReturnType,XCP_CODE) Xcp_FrIfTriggerTransmit
(
  PduIdType XcpTxPduId,
  P2VAR(PduInfoType, AUTOMATIC, XCP_APPL_DATA) PduInfoPtr
)
{
  Std_ReturnType RetVal = E_NOT_OK;

  DBG_XCP_FRIFTRIGGERTRANSMIT_ENTRY(XcpTxPduId,PduInfoPtr);

#if (XCP_DEV_ERROR_DETECT == STD_ON)
  if (FALSE == Xcp_Initialized)
  {
    XCP_DET_REPORT_ERROR(XCP_SID_FRIF_TRIGGER_TRANSMIT, XCP_E_NOT_INITIALIZED);
  }
  else if ((PduInfoPtr == NULL_PTR) || (PduInfoPtr->SduDataPtr == NULL_PTR))
  {
    XCP_DET_REPORT_ERROR(XCP_SID_FRIF_TRIGGER_TRANSMIT, XCP_E_INV_POINTER);
  }
  else if (XcpTxPduId != XCP_TX_PDU_ID)
  {
    XCP_DET_REPORT_ERROR(XCP_SID_FRIF_TRIGGER_TRANSMIT, XCP_E_INVALID_PDUID);
  }
  else
#endif
  {
    TS_PARAM_UNUSED(XcpTxPduId);
    /* Check whether XCP is expecting a trigger */
    if(TRUE == Xcp_IsWaitingForTrigger())
    {
      /* Get data to be transmitted */
      Xcp_ProvideTxData(PduInfoPtr);
      /* Indicate that copy has been successful */
      RetVal = E_OK;
      /* Trigger transition XCP_TXWAIT_CONFIRMATION */
      Xcp_TriggerCallbackReceived = TRUE;
    }
  }


  DBG_XCP_FRIFTRIGGERTRANSMIT_EXIT(RetVal,XcpTxPduId,PduInfoPtr);
  return RetVal;
}

#endif /* XCP_ON_FLEXRAY_ENABLED == STD_ON */


/*==================[internal function definitions]=========================*/

STATIC FUNC(void,XCP_CODE) Xcp_CommonRxIndication
(
  PduIdType XcpRxPduId,
  P2VAR(PduInfoType, AUTOMATIC, XCP_APPL_DATA) XcpRxPduPtr
)
{
  DBG_XCP_COMMONRXINDICATION_ENTRY(XcpRxPduId,XcpRxPduPtr);

#if (XCP_DEV_ERROR_DETECT == STD_ON)
  if (Xcp_Initialized == FALSE)
  {
    XCP_DET_REPORT_ERROR(XCP_SID_IF_RX_INDICATION, XCP_E_NOT_INITIALIZED);
  }
  else if ((XcpRxPduPtr == NULL_PTR)|| (XcpRxPduPtr->SduDataPtr == NULL_PTR))
  {
    XCP_DET_REPORT_ERROR(XCP_SID_IF_RX_INDICATION, XCP_E_NULL_POINTER);
  }
  else if ( XCP_IS_RX_PDU_ID_VALID(XcpRxPduId) == FALSE )
  {
    XCP_DET_REPORT_ERROR(XCP_SID_IF_RX_INDICATION, XCP_E_INVALID_PDUID);
  }
  else
#endif /* XCP_DEV_ERROR_DETECT == STD_ON */
  if ( XcpRxPduPtr->SduLength < (XCP_FRAME_HEADER_LENGTH + 1U) )
  {
    /* The CTO data packet is not valid, send an ERR_CMD_SYNTAX response packet.
     * The Pid cannot be fetched from the message, therefore set a valid, placeholder, PID that must
     * be responded only if the Xcp is in connected state */
    SchM_Enter_Xcp_SCHM_XCP_EXCLUSIVE_AREA_XCP_INTERNALS();

    Xcp_SendErrorCmdSyntaxPacket(XCP_CMD_SYNCH_PID);

    SchM_Exit_Xcp_SCHM_XCP_EXCLUSIVE_AREA_XCP_INTERNALS();
  }
  else
#if (XCP_ON_FLEXRAY_ENABLED == STD_ON)
  if ( XcpRxPduPtr->SduDataPtr[0U] != XCP_FLEXRAY_NAX )
  {
    /* Ignore messages that are meant for a different slave, i.e. have the wrong NAX. */
  }
  else
#endif  /* XCP_ON_FLEXRAY_ENABLED == STD_ON */
  {
    /* Local variable to hold the SDU length */
    PduLengthType SduLength = XcpRxPduPtr->SduLength;

#if (XCP_ON_FLEXRAY_ENABLED == STD_ON)
    /* if FlexRay is enabled (meaning that a tail is appended to packets with odd length) we might
     * encounter the situation where
     *  - the command length is equal to the MAX_CTO
     *  - the whole packet has an odd length.
     *
     * If such a case occurs a fill tail byte will be appended due to communication restriction,
     * therefore we must adjust the received length in order to pass the validity checks. */

    /* Which MAX_CTO to check against, MAX_CTO or MAX_CTO_PGM */
    uint8 MaxCtoValue = XCP_MAX_CTO;

#if (XCP_PGM_SUPPORTED == STD_ON)
    /* Local variable to hold the data packet PID (the first byte) */
    uint8 Pid =  XcpRxPduPtr->SduDataPtr[XCP_FRAME_HEADER_LENGTH + XCP_RES_PID_INDEX];
    /* If the PID corresponds to CMD packet */
    if (XCP_PID_IS_CMD(Pid))
    {
      /* If PID corresponds to a programming command */
      if ( ( Pid >= XCP_CMD_FIRST_PROGRAMMING_PID ) &&
           ( Pid <= XCP_CMD_LAST_PROGRAMMING_PID )
         )
      {
        /* set to check against MAX_CTO_PGM instead of MAX_CTO */
        MaxCtoValue = XCP_MAX_CTO_PGM;
      }
    }
#endif /* XCP_PGM_SUPPORTED == STD_ON */

  /* Check that the MAX_CTO(_PGM) + FlexRay header length is odd */
  if (((MaxCtoValue + XCP_FRAME_HEADER_LENGTH) % 2U) != 0U)
  {
    /* If the received length is higher that MAX_CTO or MAX_CTO_PGM with just 1 byte
     * (considering the header also) */
    if (SduLength == (PduLengthType)MaxCtoValue + (PduLengthType)XCP_FRAME_HEADER_LENGTH +
                     (PduLengthType)1U )
    {
      /* This means the SduLength should have an odd number of bytes, but the FlexRay communication
       * added an extra tail byte to achieve an even number of bytes.  */
      SduLength--;
    }
  }

#endif /* XCP_ON_FLEXRAY_ENABLED == STD_ON */

    /* Call the Rx indication function to insert the received COMMAND/STIM in
       corresponding queue to start processing. Cut away any header that might have been necessary
       by the underlying communication bus (e.g. FlexRay) */
    Xcp_RxIndication( SduLength - XCP_FRAME_HEADER_LENGTH ,
                    &(XcpRxPduPtr->SduDataPtr[XCP_FRAME_HEADER_LENGTH])
                  );
    TS_PARAM_UNUSED(XcpRxPduId);
  }

  /* We must clear the EV_RESUME_MODE transmit pending flag as a reception means that the
   * communication is working so there won't be any reason for the EV_RESUME_MODE transmit request
   * to not be accepted */
#if (XCP_STORE_DAQ_SUPPORTED == STD_ON)
  if (Xcp_EvResumeModeTransmitPending == TRUE)
  {
    Xcp_EvResumeModeTransmitPending = FALSE;
  }
#endif

  DBG_XCP_COMMONRXINDICATION_EXIT(XcpRxPduId,XcpRxPduPtr);

}

STATIC FUNC(void, XCP_CODE) Xcp_CommonTxConfirmation(PduIdType XcpTxPduId)
{
  DBG_XCP_COMMONTXCONFIRMATION_ENTRY(XcpTxPduId);
#if (XCP_DEV_ERROR_DETECT == STD_ON)
  /* Check whether XCP is initialized */
  if (Xcp_Initialized == FALSE)
  {
    XCP_DET_REPORT_ERROR(XCP_SID_IF_TX_CONFIRMATION, XCP_E_NOT_INITIALIZED);
  }
  /* Check whether XCP TxPduId is valid */
  else if (XcpTxPduId != XCP_TX_PDU_ID)
  {
    XCP_DET_REPORT_ERROR(XCP_SID_IF_TX_CONFIRMATION, XCP_E_INVALID_PDUID);
  }
  else
#endif
  {
    TS_PARAM_UNUSED(XcpTxPduId);
    /* If XCP was waiting for a confirmation */
    if (Xcp_IsWaitingForConfirmation() == TRUE)
    {
      /* set flag for received Tx confirmation */
      Xcp_TxConfCallbackReceived = TRUE;

      if (Xcp_GetCommandProcessorState() == XCP_CMDPROCESSOR_BUSY)
      {
        /* Re-run the command processing */
        Xcp_ProcessCommandQueue(XCP_RUN_FROM_TX_CONF_CONTEXT);
      }
      /* Initiate next transmission if any packet is needed to be sent */
      Xcp_SendNextPacketFromConf();
    }
  }

  DBG_XCP_COMMONTXCONFIRMATION_EXIT(XcpTxPduId);
}

#if (XCP_MAX_DAQ != 0U)
STATIC FUNC(Std_ReturnType, XCP_CODE) Xcp_InitializeDaqLists(void)
{
  /* The return value of this function*/
  Std_ReturnType RetValue = E_OK;
  /* Variable to indicate if the DAQ lists were initialized from NV memory */
  boolean DaqListsInitializedFromNV = FALSE;

#if (XCP_STORE_DAQ_SUPPORTED == STD_ON)
  /* The result of the NvM_ReadAll() service for the XCP NVM block descriptor */
  NvM_RequestResultType BlockResult;
#if (XCP_DEV_ERROR_DETECT == STD_ON)
  /* Variable to store the size of the DAQ lists runtime information */
  uint32 SizeOfDaqLists = (uint32)sizeof(Xcp_DaqLists);

  DBG_XCP_INITIALIZEDAQLISTS_ENTRY();

  if (SizeOfDaqLists != XCP_STORE_DAQ_NVM_BLOCK_LENGTH)
  {
    /* Throw error */
    XCP_DET_REPORT_ERROR(XCP_SID_INIT, XCP_E_INVALID_NVM_BLOCK_LENGTH);
    RetValue = E_NOT_OK;
  }
  else
#endif /* XCP_DEV_ERROR_DETECT == STD_ON */
  {
    /* Read the error status */
    if (NvM_GetErrorStatus(XCP_STORE_DAQ_NVM_BLOCK_ID, &BlockResult) == E_OK)
    {
      /* We consider the DAQ lists initialized only if NvM read successfully the DAQ lists from the NV
       * memory and the start up mode is STORE_DAQ (with or without resume mode). In case of CLEAR_DAQ,
       * we let the initialization routine to initialize the DAQ lists with default values. */
      if ((BlockResult == NVM_REQ_OK) &&
         ((Xcp_DaqLists.Xcp_StoreDaq.Mode == XCP_MASK_STORE_DAQ_REQ_RESUME) ||
          (Xcp_DaqLists.Xcp_StoreDaq.Mode == XCP_MASK_STORE_DAQ_REQ_NORESUME)))
      {
        DaqListsInitializedFromNV = TRUE;
      }
    }
  }
#endif /* XCP_STORE_DAQ_SUPPORTED == STD_ON */

  /* If the DAQ lists could not be loaded from the NV memory or the DAQ lists were cleared from NV,
   * or the storage of DAQ lists is not supported we need to restore/initialize the DAQ lists with
   * default data */
  if (DaqListsInitializedFromNV == FALSE)
  {
#if (XCP_STORE_DAQ_SUPPORTED == STD_ON)
    /* Iterator */
    uint16 i;
    /* Clear the store DAQ administrative structure as it might have been erroneously filled by the
     * NvM stack */
    Xcp_DaqLists.Xcp_StoreDaq.ConfigurationSessionID = 0U;
#if (XCP_DAQ_CONFIG_TYPE == XCP_DAQ_DYNAMIC_MASK)
    Xcp_DaqLists.Xcp_StoreDaq.NoOfConfiguredDynamicDaqLists = 0U;
#endif
    Xcp_DaqLists.Xcp_StoreDaq.Mode = 0U;
    for (i = 0U; i< XCP_NUMBER_OF_BYTES_SELECTED_DAQ; i++)
    {
      Xcp_DaqLists.Xcp_StoreDaq.SelectedDAQ[i] = 0U;
    }
#endif

#if ((XCP_MAX_DAQ != 0U) && (XCP_MAX_DAQ > XCP_DAQ_COUNT)) /* If we have static or predefined DAQ lists */
    /* Initialize the DAQ lists with data from ROM */
    TS_MemCpy(&(Xcp_DaqLists.Xcp_NonDynamicDaqLists),&Xcp_DaqListsDefault, (uint16)(sizeof(Xcp_DaqListsDefault)));
#endif

#if (XCP_DAQ_CONFIG_TYPE == XCP_DAQ_DYNAMIC_MASK)
    /* Reset the entire dynamic memory area to 0 */
    TS_MemBZero(Xcp_DaqLists.Xcp_DynamicArea, XCP_DYNAMIC_AREA_SIZE);
#endif
  }
#if (XCP_STORE_DAQ_SUPPORTED == STD_ON)
  else /* Daq lists were initialized from NV memory */
  {
    /* If the desired start mode is resume we enable the immediate transfer start */
    if (Xcp_DaqLists.Xcp_StoreDaq.Mode == XCP_MASK_STORE_DAQ_REQ_RESUME)
    {
      Xcp_PrepareStoredDaqListsResume();
    }
    else
    {
      /* If the desired start mode is not resume, just prepare the DAQ lists accordingly */
      Xcp_PrepareStoredDaqLists();
    }
  }
#endif

  DBG_XCP_INITIALIZEDAQLISTS_EXIT(RetValue);
  return RetValue;
}
#endif /* XCP_MAX_DAQ != 0U */

#define XCP_STOP_SEC_CODE
#include <MemMap.h>

/*==================[end of file]===========================================*/
