/**
 * \file
 *
 * \brief AUTOSAR Xcp
 *
 * This file contains the implementation of the AUTOSAR
 * module Xcp.
 *
 * \author Elektrobit Automotive GmbH, 91058 Erlangen, Germany
 *
 * Copyright 2005 - 2013 Elektrobit Automotive GmbH
 * All rights exclusively reserved for Elektrobit Automotive GmbH,
 * unless expressly agreed to otherwise.
 */

/*==================[inclusions]============================================*/

#include <Xcp_Trace.h>
#include <Std_Types.h>         /* AUTOSAR standard types */
#include <TSAutosar.h>         /* EB specific standard types */
#include <SchM_Xcp.h>          /* Needed for exclusive area definition */

#include <Xcp.h>               /* Module public API */
#include <Xcp_Int.h>           /* Internal defines and declarations */

/*==================[macros]================================================*/

/*==================[type definitions]======================================*/

/*==================[external function declarations]========================*/

/*==================[internal function declarations]========================*/
#if (XCP_MAX_DAQ != 0U)

#define XCP_START_SEC_CODE
#include <MemMap.h>

/** \brief Function to check whether any DAQ List of an event is active.
 **
 ** Use this function to check whether any DAQ List corresponding to the Event Id is active.
 **
 ** \pre This function is called within a critical section.
 **
 ** \param[in]  EventId  XCP DAQ Event Id.
 ** \return whether the event has a daq list active
 ** \retval E_OK     At least one DAQ List corresponding to the event is running
 ** \retval E_NOT_OK No DAQ List is running
 **/
STATIC FUNC(Std_ReturnType, XCP_CODE) Xcp_CheckDaqListActive
(
  uint16 EventId
);

/** \brief Function to mark an event as being set
 **
 ** Use this function to mark an event as being set. This implies that it will begin
 ** sampling/stimulating its associated DAQ lists.
 **
 ** \param[in]  EventId  XCP Event Id.
 ** \return success of operation
 ** \retval XCP_OK       The event has been marked as set
 ** \retval XCP_OVERLOAD The event wasn't marked as set as it was previously set - OVERLOAD situation.
 **/
STATIC FUNC(Xcp_ReturnType, XCP_CODE) Xcp_MarkEventAsSet
(
  uint16 EventId
);

#if (XCP_MAX_CYCLIC_EVENT_CHANNEL > 0U)
/** \brief Function to Set One Cyclic Event
 **
 ** \param[in]  EventId  XCP Event Id.
 **/
STATIC FUNC(void, XCP_CODE) Xcp_SetOneCyclicEvent
(
  uint16 EventId
);
#endif

#define XCP_STOP_SEC_CODE
#include <MemMap.h>

#endif
/*==================[external constants]====================================*/

/*==================[internal constants]====================================*/

/*==================[external data]=========================================*/

/*==================[internal data]=========================================*/

/*==================[external function definitions]=========================*/

#define XCP_START_SEC_CODE
#include <MemMap.h>

/*------------------[Xcp_SetEvent]---------------------------------*/
FUNC(Xcp_ReturnType, XCP_CODE) Xcp_SetEvent
(
  uint16 EventId
)
{
  Xcp_ReturnType RetVal = XCP_NOT_OK;

  DBG_XCP_SETEVENT_ENTRY(EventId);

  /* Abort if the module is not initialized */
  if( Xcp_Initialized == FALSE )
  {
    RetVal = XCP_NOT_INITIALIZED;
  }
  else
  {
#if (XCP_DEV_ERROR_DETECT == STD_ON)
#if (XCP_MAX_EVENT_CHANNEL != 0U)
    /* If the received event id is invalid or cyclic */
    if( ( EventId >= XCP_MAX_EVENT_CHANNEL ) || ( Xcp_EventInfo[EventId].TimeCycle != 0U ) )
#endif /* XCP_MAX_EVENT_CHANNEL != 0U */
    {
      XCP_DET_REPORT_ERROR(XCP_SID_SET_EVENT, XCP_E_INVALID_EVENT);
    }
#if (XCP_MAX_EVENT_CHANNEL != 0U)
    else
#endif /* XCP_MAX_EVENT_CHANNEL != 0U */
#endif /* XCP_DEV_ERROR_DETECT == STD_ON */
#if (XCP_MAX_EVENT_CHANNEL != 0U)
    {
      SchM_Enter_Xcp_SCHM_XCP_EXCLUSIVE_AREA_XCP_INTERNALS();

      /* The event needs to be processed only if, XCP is not in disconnected state and
         at least one DAQ list is configured in the event */
      if (Xcp_State != XCP_STATE_DISCONNECTED)
      {
        /* If any (currently running) DAQ List is associated with the event */
        if( Xcp_CheckDaqListActive(EventId) == E_OK )
        {
          /* Mark the event as set */
          RetVal = Xcp_MarkEventAsSet( EventId );
        }
        else
        {
          RetVal = XCP_NO_ACTIVE_LIST;
        }
      }
      else
      {
        RetVal = XCP_NOT_CONNECTED;
      }

      SchM_Exit_Xcp_SCHM_XCP_EXCLUSIVE_AREA_XCP_INTERNALS();
    }
#else /* XCP_MAX_EVENT_CHANNEL != 0U */
    {
      TS_PARAM_UNUSED(EventId);
    }
#endif /* XCP_MAX_EVENT_CHANNEL != 0U */
  }

  DBG_XCP_SETEVENT_EXIT(RetVal,EventId);
  return RetVal;
}


/*==================[internal function definitions]=========================*/

/*------------------[Xcp_SetCyclicEvents]---------------------------------*/
#if (XCP_MAX_CYCLIC_EVENT_CHANNEL > 0U)
FUNC(void, XCP_CODE) Xcp_SetCyclicEvents(void)
{
  DBG_XCP_SETCYCLICEVENTS_ENTRY();

  /* Check if Slave is not disconnected */
  if (Xcp_State != XCP_STATE_DISCONNECTED)
  {
    /* Local variable to hold Event Channel number */
    uint16 EventChannelNumber;

    /* Loop for all the Cyclic Event Channels */
    for( EventChannelNumber = 0U;
         EventChannelNumber < XCP_MAX_CYCLIC_EVENT_CHANNEL;
         EventChannelNumber++ )
    {
      /* The first events are the cyclic ones */
      /* Set the Cyclic Event Channel */
      Xcp_SetOneCyclicEvent( EventChannelNumber );
    }

  } /* ( Xcp_CheckConnected() == E_OK ) */

  DBG_XCP_SETCYCLICEVENTS_EXIT();
}

/*------------------[Xcp_SetOneCyclicEvent]---------------------------------*/
STATIC FUNC(void, XCP_CODE) Xcp_SetOneCyclicEvent
(
  uint16 EventId
)
{
  DBG_XCP_SETONECYCLICEVENT_ENTRY(EventId);

  /* Check whether any DAQ List of this event is currently running */
  if ( Xcp_CheckDaqListActive(EventId) == E_OK )
  {
    /* Decrement the Cyclic Event Counter */
    Xcp_Event[EventId].TimeCycleCounter--;

    /* Check the Cyclic Event Counter */
    if( Xcp_Event[EventId].TimeCycleCounter == 0U )
    {
      /* Cyclic Event Counter = 0U. Event cyclic time has elapsed. */
      /* Reinitialize the Cycle Event Counter to its Maximum value */
      Xcp_Event[EventId].TimeCycleCounter = Xcp_EventInfo[EventId].CycleCounterMax;

      /* Mark the cyclic event as set. No meaningful way to react on error situations here,
       * therefore ignoring the return value. */
      (void)Xcp_MarkEventAsSet( EventId );
    } /* ( Xcp_Event[EventId].TimeCycleCounter == 0U ) */

  } /* ( E_OK == Xcp_CheckDaqListActive(EventId) ) */

  DBG_XCP_SETONECYCLICEVENT_EXIT(EventId);
}
#endif /* (XCP_MAX_CYCLIC_EVENT_CHANNEL > 0U) */

/*------------------[Xcp_CheckDaqListActive]--------------------------------*/
#if (XCP_MAX_DAQ != 0U)

STATIC FUNC(Std_ReturnType, XCP_CODE) Xcp_CheckDaqListActive
(
  uint16 EventId
)
{
  /* Set as no DAQ List is running */
  Std_ReturnType RetVal = E_NOT_OK;

  uint8 i = 0;
  uint8 DaqListCount;
  /* Variable to hold address of a DAQ List */
  P2VAR(Xcp_DaqType, AUTOMATIC, XCP_APPL_DATA) DaqListPtr;

  DBG_XCP_CHECKDAQLISTACTIVE_ENTRY(EventId);

  /* Total DAQ Lists associated with the particular event taken */
  DaqListCount = Xcp_Event[EventId].DaqIdListCount;

  /* Check whether any DAQ List is currently running */
  if ((Xcp_Status.SessionStatus & XCP_MASK_DAQ_RUNNING ) != 0U )
  {
    /* Loop through all DAQ Lists corresponding to the Event */
    while(i < DaqListCount)
    {
      /* Retrieve DAQ List pointer */
      DaqListPtr = XCP_GET_DAQ_LIST_PTR(Xcp_Event[EventId].DaqIdList[i]);

      /* Check whether DAQ List is currently running */
      if(XCP_IS_DAQ_LIST_RUNNING(DaqListPtr) == TRUE)
      {
        RetVal = E_OK;
        break;
      }
      /* Select next DAQ List */
      i++;
    }
  }

  DBG_XCP_CHECKDAQLISTACTIVE_EXIT(RetVal, EventId);
  return RetVal;
}

/*------------------[Xcp_MarkEventAsSet]--------------------------------*/

STATIC FUNC(Xcp_ReturnType, XCP_CODE) Xcp_MarkEventAsSet
(
  uint16 EventId
)
{
  /* The function return value */
  Xcp_ReturnType RetVal = XCP_OK;

  DBG_XCP_MARKEVENTASSET_ENTRY(EventId);

  /* Check if the event was not previously set */
  if (Xcp_Event[EventId].EventIsSet == FALSE)
  {
    /* Update the event as being set */
    Xcp_Event[EventId].EventIsSet = TRUE;
    /* Reset the DAQ and ODT indexes */
    Xcp_Event[EventId].NextDaqIndex = 0U;
    Xcp_Event[EventId].NextOdtIndex = 0U;
  }
  else
  {
    /* Check if Event Packet is Enabled */
#if (XCP_EVENT_PACKET_ENABLED == STD_ON)
    if(Xcp_EventCTOIndex < XCP_EV_CTO_QUEUE_SIZE)
    {
      /* Queue the Event Packet EV_DAQ_OVERLOAD */
      Xcp_EventCTOQueue[Xcp_EventCTOIndex].EVENT_CTO[0U] = XCP_EV_PID;
      Xcp_EventCTOQueue[Xcp_EventCTOIndex].EVENT_CTO[1U] = XCP_EV_DAQ_OVERLOAD;
      Xcp_EventCTOQueue[Xcp_EventCTOIndex].Length = 0x02U;
      Xcp_EventCTOIndex++;
    }
#endif
    RetVal = XCP_OVERLOAD;
  }

  DBG_XCP_MARKEVENTASSET_EXIT(RetVal, EventId);
  return RetVal;
}

#endif /* (XCP_MAX_DAQ != 0U) */

#define XCP_STOP_SEC_CODE
#include <MemMap.h>

/*==================[end of file]===========================================*/
