/**
 * \file
 *
 * \brief AUTOSAR Xcp
 *
 * This file contains the implementation of the AUTOSAR
 * module Xcp.
 *
 * \author Elektrobit Automotive GmbH, 91058 Erlangen, Germany
 *
 * Copyright 2005 - 2013 Elektrobit Automotive GmbH
 * All rights exclusively reserved for Elektrobit Automotive GmbH,
 * unless expressly agreed to otherwise.
 */

/*     MISRA-C:2004 Deviation List
 *
 *     MISRA-1) Deviated Rule: 12.5 (required)
 *    "The operands of a logical '&&' or '||' shall be 'primary-expressions'."
 *
 *    Reason: There seems to be no issue. The operands are only
 *    primary-expressions.
 *
 */

/*==================[inclusions]============================================*/

#include <Xcp_Trace.h>
#include <Std_Types.h>         /* AUTOSAR standard types */
#include <TSAutosar.h>         /* EB specific standard types */
#include <TSMem.h>             /* EB memory functions */
#include <SchM_Xcp.h>          /* Needed for exclusive area definition */

#include <Xcp.h>               /* Module public API */
#include <Xcp_Int.h>           /* Internal defines and declarations */

#if(STD_ON == XCP_ON_CAN_ENABLED)
# include <CanIf.h>             /* CAN interface APIs  */
#endif
#if(STD_ON == XCP_ON_ETHERNET_ENABLED)
# include <SoAd.h>              /* Ethernet interface APIs */
#endif
#if(STD_ON == XCP_ON_FLEXRAY_ENABLED)
# include <FrIf.h>             /* FlexRay interface APIs  */
#endif
#if (XCP_PROD_ERR_HANDLING_RETRY_FAILED == TS_PROD_ERR_REP_TO_DEM)
# include <Dem.h>               /* Dem event IDs */
#endif

/*==================[macros]================================================*/

#if (defined XCP_SINGLE_PACKET_MESSAGE)
# error XCP_SINGLE_PACKET_MESSAGE already defined
#endif
/** \brief Enable or disable transmission of single packet in Xcp message. */
#define XCP_SINGLE_PACKET_MESSAGE                    STD_ON

#if (defined XCP_TP_INTERFACE_TRANSMIT)
# error XCP_TP_INTERFACE_TRANSMIT already defined
#endif
#if (STD_ON == XCP_ON_CAN_ENABLED)
/** \brief CanIf transmit function call */
# define XCP_TP_INTERFACE_TRANSMIT(TxPduId, TxPduPtr) (CanIf_Transmit(TxPduId,TxPduPtr))
#elif (STD_ON == XCP_ON_ETHERNET_ENABLED)
/** \brief SoAdIf transmit function call */
# define XCP_TP_INTERFACE_TRANSMIT(TxPduId, TxPduPtr) (SoAd_IfTransmit(TxPduId,TxPduPtr))
#elif (STD_ON == XCP_ON_FLEXRAY_ENABLED)
/** \brief FrIf transmit function call */
# define XCP_TP_INTERFACE_TRANSMIT(TxPduId, TxPduPtr) (FrIf_Transmit(TxPduId,TxPduPtr))
#else
# define XCP_TP_INTERFACE_TRANSMIT(TxPduId, TxPduPtr) E_NOT_OK
#endif

#if (defined XCP_TX_BUFFER_SIZE)
# error XCP_TX_BUFFER_SIZE already defined
#endif
/** \brief Definition of XCP transmit buffer size. */
#if (    (XCP_ON_FLEXRAY_ENABLED == STD_ON) \
      && (((XCP_FRAME_HEADER_LENGTH + XCP_MAX(XCP_MAX_CTO,XCP_MAX_DTO)) % 2U) == 1U))
  #if (XCP_MAX_CTO > XCP_MAX_DTO)
  #define XCP_TX_BUFFER_SIZE      (XCP_FRAME_HEADER_LENGTH + XCP_MAX_CTO + 1U)
  #else
  #define XCP_TX_BUFFER_SIZE      (XCP_FRAME_HEADER_LENGTH + XCP_MAX_DTO + 1U)
  #endif
#else
  #if (XCP_MAX_CTO > XCP_MAX_DTO)
  #define XCP_TX_BUFFER_SIZE      (XCP_FRAME_HEADER_LENGTH + XCP_MAX_CTO)
  #else
  #define XCP_TX_BUFFER_SIZE      (XCP_FRAME_HEADER_LENGTH + XCP_MAX_DTO)
  #endif
#endif

#if (defined XCP_TX_TRANSITION_IDLE)
# error XCP_TX_TRANSITION_IDLE already defined
#endif
/** \brief Definition of transmit processor state machine state transitions:
 * XCP_TX_TRANSITION_IDLE */
#define XCP_TX_TRANSITION_IDLE                       0x01U

#if (defined XCP_TXWAIT_TRIGGER)
# error XCP_TXWAIT_TRIGGER already defined
#endif
/** \brief Definition of transmit processor state machine state transitions:
 * TXWAIT_TRIGGER */
#define XCP_TXWAIT_TRIGGER                           0x02U

#if (defined XCP_TXWAIT_CONFIRMATION)
# error XCP_TXWAIT_CONFIRMATION already defined
#endif
/** \brief Definition of transmit processor state machine state transitions:
 * TXWAIT_CONFIRMATION */
#define XCP_TXWAIT_CONFIRMATION                      0x04U

#if (defined XCP_TXWAIT_RETRY)
# error XCP_TXWAIT_RETRY already defined
#endif
/** \brief Definition of transmit processor state machine state transitions:
 * TXWAIT_RETRY */
#define XCP_TXWAIT_RETRY                             0x08U

#if (defined XCP_TXRETRY_ABORT)
# error XCP_TXRETRY_ABORT already defined
#endif
/** \brief Definition of transmit processor state machine state transitions:
 * TXRETRY_ABORT */
#define XCP_TXRETRY_ABORT                            0x10U

/*==================[type definitions]======================================*/

/** \brief Definition of Tx processor statemachine states */
typedef enum
{
    XCP_TX_IDLE,
/* If TriggerTransmit function will be called by lower layer */
#if (XCP_TX_TRIGGER_ENABLED == STD_ON)
    XCP_TX_TRIGGER,
#endif
    XCP_TX_CONFIRMATION,
#if (XCP_TX_RETRY_COUNT > 0) || (XCP_STORE_DAQ_SUPPORTED == STD_ON)
    XCP_TX_RETRY,
#endif
    XCP_TX_MAXSTATE
} Xcp_TxStateType;

/** \brief Definition of Tx processor statemachine state handling function
 * type */
typedef P2FUNC(void, XCP_CONST, TxStateHandlerFuncPtr)(void);

/** \brief Definition of Bus Timeout Counter data type */
typedef struct{
  Xcp_BusTimeoutCounterValueType Value; /* Counter's current value */
  boolean IsActive;                     /* Counter's active/inactive property */
}Xcp_BusTimeoutCounterType;
/*==================[external function declarations]========================*/

/*==================[internal function declarations]========================*/
#define XCP_START_SEC_CODE
#include <MemMap.h>

/** \brief Function to process transmit statemachine idle state.
 **
 ** State change will be executed if there is any transition trigger to
 ** TX_TRIGGER TX_CONFIRMATION or TX_RETRY states.  When there is no
 ** transition trigger, the TX_IDLE state will be executed. */
STATIC FUNC(void, XCP_CODE) Xcp_TxProcessStateIdle(void);

/* If TriggerTransmit function will be called by lower layer */
#if (XCP_TX_TRIGGER_ENABLED == STD_ON)
/** \brief Function to process transmit statemachine wait for trigger state.
 **
 ** State change will be executed if there is any transition trigger to
 ** TX_CONFIRMATION. When there is no transition trigger, the TX_TRIGGER state
 ** will be executed. This function will be available only if the transport
 ** interface requires <module>TriggerTransmit(). */
STATIC FUNC(void, XCP_CODE) Xcp_TxProcessStateTrigger(void);
#endif

/** \brief Function to process transmit statemachine wait for confirmation
 ** state.
 **
 ** State change will be executed if there is any transition trigger to
 ** TX_IDLE.When there is no transition trigger, the TX_CONFIRMATION state
 ** will be executed. */
STATIC FUNC(void, XCP_CODE) Xcp_TxProcessStateConfirmation(void);


#if (XCP_TX_RETRY_COUNT > 0) || (XCP_STORE_DAQ_SUPPORTED == STD_ON)
/** \brief Function to process transmit statemachine retry state.
 **
 ** State change will be executed if there is any transition trigger to
 ** TX_IDLE, TX_TRIGGER or TX_CONFIRMATION.When there is no transition trigger
 ** the TX_RETRY state will be executed. */
STATIC FUNC(void, XCP_CODE) Xcp_TxProcessStateRetry(void);

/** \brief Function to retry transmission of Tx PDU.
 **
 ** This function repeats the transmission of TX PDU until retry count expires
 ** or until transmission succeeds, which ever occurs first. */
STATIC FUNC(void, XCP_CODE) Xcp_TxRetry(void);

#endif /* (XCP_TX_RETRY_COUNT > 0) || (XCP_STORE_DAQ_SUPPORTED == STD_ON) */

/** \brief Function to transmit a Tx PDU.
 **
 ** This function copies CTOs and DTOs from COmmand processor and Event
 ** processor respectively to the Tx buffer and tries to transmit the Tx PDU
 ** through Xcp_TransmitMessage(). */
STATIC FUNC(void, XCP_CODE) Xcp_Transmit(void);

/** \brief Function to transmit a Tx PDU.
 **
 ** This function transmits the Tx PDU. */
STATIC FUNC(Std_ReturnType, XCP_CODE) Xcp_TransmitMessage(void);

/** \brief Function will report error and abort the current transmission context
 **
 ** This function will report an error to DEM or DET, depending on the configuration, and
 ** flush the corresponding queue from which the failed message originates.
 **  */
STATIC FUNC(void, XCP_CODE)  Xcp_AbortMessageTransmission(void);

#if ((XCP_ON_ETHERNET_ENABLED == STD_ON) || (XCP_ON_FLEXRAY_ENABLED == STD_ON))
/** \brief Add the frame header and tail to the PDU to be sent
 **
 ** \param[inout] TxPduPtr Pointer to the TxPdu, to which the header and tail
 **                        shall be added.
 */
STATIC FUNC(void, XCP_CODE) Xcp_AddFrameHeaderAndTail
(
  P2VAR(PduInfoType, AUTOMATIC, XCP_APPL_DATA) TxPduPtr
);
#endif /* XCP_ON_ETHERNET_ENABLED == STD_ON || XCP_ON_FLEXRAY_ENABLED == STD_ON */

/** \brief Process the Bus Timeout monitor */
STATIC FUNC(void, XCP_CODE) Xcp_ProcessBusTimeout(void);

#define XCP_STOP_SEC_CODE
#include <MemMap.h>

/*==================[external constants]====================================*/

/*==================[internal constants]====================================*/

#define XCP_START_SEC_CONST_UNSPECIFIED
#include <MemMap.h>

/** \brief Transmit state machine state handling functions array */
STATIC CONST(TxStateHandlerFuncPtr, XCP_CONST) Xcp_TxState[XCP_TX_MAXSTATE] =
{
  &Xcp_TxProcessStateIdle,          /* state: TX_IDLE */

/* If TriggerTransmit functionality is available */
#if (XCP_TX_TRIGGER_ENABLED == STD_ON)
  &Xcp_TxProcessStateTrigger,       /* state: TX_TRIGGER */
#endif

  &Xcp_TxProcessStateConfirmation,  /* state: TX_CONFIRMATION */
#if (XCP_TX_RETRY_COUNT > 0) || (XCP_STORE_DAQ_SUPPORTED == STD_ON)
  &Xcp_TxProcessStateRetry          /* state: TX_RETRY */
#endif
};

#define XCP_STOP_SEC_CONST_UNSPECIFIED
#include <MemMap.h>

/*==================[external data]=========================================*/

/*==================[internal data]=========================================*/

#define XCP_START_SEC_VAR_NOINIT_8BIT
#include <MemMap.h>

/** \brief Xcp transmit buffer. */
STATIC VAR(uint8, XCP_VAR_NOINIT) Xcp_TxBuffer[XCP_TX_BUFFER_SIZE];

#if (XCP_TX_RETRY_COUNT > 0) || (XCP_STORE_DAQ_SUPPORTED == STD_ON)
/** \brief Transmit Retry counter. */
STATIC VAR(uint8, XCP_VAR_NOINIT) Xcp_RetryCount;
#endif

/** \brief Last triggered transition in Tx processor state machine. */
STATIC VAR(uint8, XCP_VAR_NOINIT) Xcp_Transition;

/** \brief Holds the currently processed state transition */
STATIC VAR(uint8, XCP_VAR_NOINIT) Xcp_ProcessedTransition;

#define XCP_STOP_SEC_VAR_NOINIT_8BIT
#include <MemMap.h>


#define XCP_START_SEC_VAR_NOINIT_UNSPECIFIED
#include <MemMap.h>

/** \brief Transmit data pdu (Transmit data and length). */
STATIC VAR(PduInfoType, XCP_VAR_NOINIT) Xcp_TxPdu;

/** \brief Current state of the Tx processor state machine. */
STATIC VAR(Xcp_TxStateType, XCP_VAR_NOINIT) Xcp_Presentstate;

/** \brief Bus Timeout Counter's data. */
STATIC VAR(Xcp_BusTimeoutCounterType, XCP_VAR_NOINIT) Xcp_BusTimeoutCounter;

#define XCP_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#include <MemMap.h>

#if (XCP_ON_ETHERNET_ENABLED == STD_ON)
#define XCP_START_SEC_VAR_NOINIT_16BIT
#include <MemMap.h>

/** \brief Ethernet header frame transmit counter */
STATIC VAR(uint16, XCP_VAR_NOINIT) Xcp_FrameTransmitCounter;

#define XCP_STOP_SEC_VAR_NOINIT_16BIT
#include <MemMap.h>
#endif /* XCP_ON_ETHERNET_ENABLED == STD_ON */

/*==================[external function definitions]=========================*/

#define XCP_START_SEC_CODE
#include <MemMap.h>

#if (XCP_ON_ETHERNET_ENABLED == STD_ON)
/*-----------------------------[Xcp_InitEthernetFrameCtr]------------------------*/
FUNC(void, XCP_CODE) Xcp_InitEthernetFrameCtr(void)
{
  DBG_XCP_INITETHERNETFRAMECTR_ENTRY();

  Xcp_FrameTransmitCounter = 0U;

  DBG_XCP_INITETHERNETFRAMECTR_EXIT();
}

#if (XCP_SOAD_SOCKET_PROTOCOL_UDP == STD_ON)

#endif /* XCP_SOAD_SOCKET_PROTOCOL_UDP == STD_ON */
#endif /* XCP_ON_ETHERNET_ENABLED == STD_ON */

/*-----------------------------[Xcp_ProcessTransmit]------------------------*/

FUNC(void, XCP_CODE) Xcp_ProcessTransmit(void)
{
  /* Holds the pending state transitions before state machine processing */
  uint8 OldTransitions;

  DBG_XCP_PROCESSTRANSMIT_ENTRY();

  /* Process the Bus Timeout functionality */
  Xcp_ProcessBusTimeout();

  /* Process the state machine until a state transitions or state change has
   * occurred */
  do
  {
    /* Get the pending transitions before state machine processing */
    OldTransitions = Xcp_Transition;

    /* clear processed transition */
    Xcp_ProcessedTransition = (uint8) XCP_TX_TRANSITION_DEFAULT;

    /* Execute Tx states until all transitions are handled */
    Xcp_TxState[Xcp_Presentstate]();

    /* Clear the currently processed transition */
    Xcp_Transition &= (uint8)(~Xcp_ProcessedTransition);
  } while (Xcp_Transition != OldTransitions);

  DBG_XCP_PROCESSTRANSMIT_EXIT();
}

/*-----------------------------[Xcp_InitTransmitProcessor]------------------*/

FUNC(void, XCP_CODE) Xcp_InitTransmitProcessor(void)
{
  DBG_XCP_INITTRANSMITPROCESSOR_ENTRY();

  /* Initialize transition in Tx processor state machine. */
  Xcp_Transition = XCP_TX_TRANSITION_DEFAULT;

  /* Initialize transmit data pdu (Transmit data and length). */
#if (XCP_ON_FLEXRAY_ENABLED == STD_ON)
  Xcp_TxBuffer[0] = XCP_FLEXRAY_NAX;
#if (XCP_FRAME_HEADER_LENGTH > 1U)
  {
    uint8_least i;
    for( i = 1U; i < XCP_FRAME_HEADER_LENGTH; i++ )
    {
      Xcp_TxBuffer[i] = XCP_RESERVED_BYTE;
    }
  }
#endif
#endif
  Xcp_TxPdu.SduDataPtr = Xcp_TxBuffer;
  Xcp_TxPdu.SduLength  = 0U;

  /* De-activate the Bus Timeout Counter each time the TX processor is re-initialized */
  Xcp_BusTimeoutCounter.IsActive = FALSE;

  /* Initialize current state of the Tx processor state machine. */
  Xcp_Presentstate = XCP_TX_IDLE;

  DBG_XCP_INITTRANSMITPROCESSOR_EXIT();
}

/* If TriggerTransmit function will be called by lower layer */
#if (XCP_TX_TRIGGER_ENABLED == STD_ON)

/*-----------------------------[Xcp_ProvideTxData]--------------------------*/

FUNC(void, XCP_CODE) Xcp_ProvideTxData
(
  P2VAR(PduInfoType, AUTOMATIC, XCP_APPL_DATA) PduPtr
)
{
  DBG_XCP_PROVIDETXDATA_ENTRY(PduPtr);

  /* Copy data to be transmitted */
  TS_MemCpy(PduPtr->SduDataPtr, Xcp_TxPdu.SduDataPtr, Xcp_TxPdu.SduLength);
  /* Update number of bytes copied */
  PduPtr->SduLength = Xcp_TxPdu.SduLength;

  DBG_XCP_PROVIDETXDATA_EXIT(PduPtr);
}
#endif

/*-----------------------------[Xcp_SendNextPacketFromConf]-----------------*/

FUNC(void, XCP_CODE) Xcp_SendNextPacketFromConf(void)
{
  DBG_XCP_SENDNEXTPACKETFROMCONF_ENTRY();

  /* Transmit any available packet */
  Xcp_Transmit();

  /* Transition TXWAIT_CONFIRMATION */
  if((Xcp_Transition & XCP_TXWAIT_CONFIRMATION) == XCP_TXWAIT_CONFIRMATION)
  {
    /* Change the state to TX_CONFIRMATION */
    Xcp_Presentstate = XCP_TX_CONFIRMATION;
    /* Store the processed transition */
    Xcp_ProcessedTransition = XCP_TXWAIT_CONFIRMATION;
    /* Do entry actions for TX_CONFIRMATION */
    /* Reset transmit data length */
    Xcp_TxPdu.SduLength = 0U;
  }
/* If TriggerTransmit function will be called by lower layer */
#if (XCP_TX_TRIGGER_ENABLED == STD_ON)
  /* Transition TXWAIT_TRIGGER */
  else if((Xcp_Transition & XCP_TXWAIT_TRIGGER) == XCP_TXWAIT_TRIGGER)
  {
    /* Change the state to TX_TRIGGER */
    Xcp_Presentstate = XCP_TX_TRIGGER;
    /* Store the processed transition */
    Xcp_ProcessedTransition = XCP_TXWAIT_TRIGGER;
  }
#endif
/* If a retry mechanism is available */
#if (XCP_TX_RETRY_COUNT > 0) || (XCP_STORE_DAQ_SUPPORTED == STD_ON)
  /* Handle Transitions triggered from Xcp_Transmit() */
  /* Transition TXWAIT_RETRY*/
  else if((Xcp_Transition & XCP_TXWAIT_RETRY) == XCP_TXWAIT_RETRY)
  {
    /* Change the state to TX_RETRY */
    Xcp_Presentstate = XCP_TX_RETRY;
    /* Do entry actions for TX_RETRY */
    /* Set retry count */
#if (XCP_STORE_DAQ_SUPPORTED == STD_ON)
    /* If the EV_RESUME_MODE is pending to be transmitted, set the retry count to 1 in order to
     * activate the retry mechanism for this special case. We set it to 1 as, when a message is
     * received, we clear the EV_RESUME_MODE pending flag and, therefore, we will retry for one more
     * time the message transmission.*/
    if (Xcp_EvResumeModeTransmitPending == TRUE)
    {
      Xcp_RetryCount = 1U;
    }
    else
#endif /* XCP_STORE_DAQ_SUPPORTED == STD_ON */
    {
      /* Initialize with the pre-configured value */
      Xcp_RetryCount = XCP_TX_RETRY_COUNT;
    }
    /* Store the processed transition */
    Xcp_ProcessedTransition = XCP_TXWAIT_RETRY;
  }
#endif /* (XCP_TX_RETRY_COUNT > 0) || (XCP_STORE_DAQ_SUPPORTED == STD_ON) */
  else
  {
    /* do nothing */
  }

  DBG_XCP_SENDNEXTPACKETFROMCONF_EXIT();
}

/*-----------------------------[Xcp_IsWaitingForConfirmation]---------------*/

FUNC(boolean, XCP_CODE) Xcp_IsWaitingForConfirmation(void)
{
  /* Check whether XCP transmit processor state machine is waiting for a
   * confirmation
   *
   * 1. When transition is set to wait for confirmation state, after a
   * transmit in Idle state.
   *
   * 2. When transition is set to wait for confirmation state, after a
   * transmit in Retry state.
   *
   * 3. When transition is set to wait for confirmation state, after receiving
   * transmit trigger in wait for trigger state.
   *
   * 4. When in the wait for confirmation state, after processing the
   * transition from Idle, Retry or wait for trigger states. */

  boolean RetValue;

  DBG_XCP_ISWAITINGFORCONFIRMATION_ENTRY();

#if (XCP_TX_TRIGGER_ENABLED == STD_OFF)
  RetValue =  ( ((Xcp_Transition & XCP_TXWAIT_CONFIRMATION) == XCP_TXWAIT_CONFIRMATION) ||
               ((uint8)Xcp_Presentstate == XCP_TX_CONFIRMATION) ) ? TRUE : FALSE;
#else
  RetValue =  ( ( ((Xcp_Transition & XCP_TXWAIT_CONFIRMATION) == XCP_TXWAIT_CONFIRMATION) ||
                 ((uint8)Xcp_Presentstate == XCP_TX_CONFIRMATION) ) ||
                ( Xcp_TriggerCallbackReceived == (boolean)TRUE ) ) ? TRUE : FALSE;
#endif

  DBG_XCP_ISWAITINGFORCONFIRMATION_EXIT(RetValue);
 return RetValue;
}

/* If TriggerTransmit function will be called by lower layer */
#if (XCP_TX_TRIGGER_ENABLED == STD_ON)

/*-----------------------------[Xcp_IsWaitingForTrigger]--------------------*/

FUNC(boolean, XCP_CODE) Xcp_IsWaitingForTrigger(void)
{
  /* Check whether XCP transmit processor state machine is waiting for a trigger
   *
   * 1. When transition is set to wait for trigger state, after a transmit in
   * Idle state.
   *
   * 2. When transition is set to wait for trigger state, after a transmit in
   * Retry state.
   *
   * 3. When in the wait for trigger state, after processing the transition
   * from Idle or Retry states. */

  boolean RetValue;

  DBG_XCP_ISWAITINGFORTRIGGER_ENTRY();

  RetValue = (boolean)
             ( ( ( ( Xcp_Transition & XCP_TXWAIT_TRIGGER ) == XCP_TXWAIT_TRIGGER ) ||
                 (   Xcp_Presentstate == XCP_TX_TRIGGER ) ) ? TRUE : FALSE );

  DBG_XCP_ISWAITINGFORTRIGGER_EXIT(RetValue);
  return (RetValue);
}

#endif

/*==================[internal function definitions]=========================*/

/*-----------------------------[Xcp_TxProcessStateIdle]---------------------*/

STATIC FUNC(void, XCP_CODE) Xcp_TxProcessStateIdle(void)
{
  DBG_XCP_TXPROCESSSTATEIDLE_ENTRY();

  Xcp_SendNextPacketFromConf();

  DBG_XCP_TXPROCESSSTATEIDLE_EXIT();
}

/*-----------------------------[Xcp_TxProcessStateTrigger]------------------*/
/* If TriggerTransmit function will be called by lower layer */
#if (XCP_TX_TRIGGER_ENABLED == STD_ON)
STATIC FUNC(void, XCP_CODE) Xcp_TxProcessStateTrigger(void)
{
  DBG_XCP_TXPROCESSSTATETRIGGER_ENTRY();

  /* If there is a trigger for transition to TX_CONFIRMATION state */
  if (Xcp_TriggerCallbackReceived == TRUE)
  {
    /* Change the state to TX_CONFIRMATION */
    Xcp_Presentstate = XCP_TX_CONFIRMATION;
    /* Store the transition */
    Xcp_Transition |= (uint8) XCP_TXWAIT_CONFIRMATION;
    /* Clear trigger call back flag.*/
    Xcp_TriggerCallbackReceived = FALSE;
    /* Reset transmit data length */
    Xcp_TxPdu.SduLength = 0U;
  }

  DBG_XCP_TXPROCESSSTATETRIGGER_EXIT();
}
#endif

/*-----------------------------[Xcp_TxProcessStateConfirmation]-------------*/

STATIC FUNC(void, XCP_CODE) Xcp_TxProcessStateConfirmation(void)
{
  DBG_XCP_TXPROCESSSTATECONFIRMATION_ENTRY();

  /* look for callback flag from Tx confirmation function */
  if (Xcp_TxConfCallbackReceived == TRUE)
  {
    /* De-activate the Bus Timeout Counter before changing state to TX Idle */
    Xcp_BusTimeoutCounter.IsActive = FALSE;

    /* Change the state to TX_IDLE */
    Xcp_Presentstate = XCP_TX_IDLE;
    /* Store the transition */
    Xcp_Transition = (uint8) XCP_TX_TRANSITION_IDLE;

    /* clear Tx confirmation callback flag for indication */
    Xcp_TxConfCallbackReceived = FALSE;
  }

  DBG_XCP_TXPROCESSSTATECONFIRMATION_EXIT();
}

#if (XCP_TX_RETRY_COUNT > 0) || (XCP_STORE_DAQ_SUPPORTED == STD_ON)
/*-----------------------------[Xcp_TxProcessStateRetry]--------------------*/

STATIC FUNC(void, XCP_CODE) Xcp_TxProcessStateRetry(void)
{
  DBG_XCP_TXPROCESSSTATERETRY_ENTRY();

  /* Retry transmission */
  Xcp_TxRetry();

  /* Handle Transitions triggered from Xcp_TxRetry() */
  /* Transition TXRETRY_ABORT*/
  if ((uint8)XCP_TXRETRY_ABORT == (Xcp_Transition & (uint8)XCP_TXRETRY_ABORT))
  {
    /* The number of retries had expired. Report error and abort the current message transmission.*/
    Xcp_AbortMessageTransmission();

    /* Signal that the transition is processed */
    Xcp_ProcessedTransition = (uint8) XCP_TXRETRY_ABORT;
  }

/* If TriggerTransmit function will be called by lower layer */

#if (XCP_TX_TRIGGER_ENABLED == STD_ON)
  /* Transition TXWAIT_TRIGGER */
  else if((uint8)XCP_TXWAIT_TRIGGER == (Xcp_Transition & (uint8)XCP_TXWAIT_TRIGGER))
  {
   /* Change the state to TX_TRIGGER */
    Xcp_Presentstate = XCP_TX_TRIGGER;
    /* Store the processed transition */
    Xcp_ProcessedTransition = (uint8) XCP_TXWAIT_TRIGGER;
  }
#endif /* XCP_TX_TRIGGER_ENABLED == STD_ON */

  /* Transition TXWAIT_CONFIRMATION*/
  else if((uint8) XCP_TXWAIT_CONFIRMATION == (Xcp_Transition & (uint8)XCP_TXWAIT_CONFIRMATION))
  {
    /* Change the state to TX_CONFIRMATION */
    Xcp_Presentstate = XCP_TX_CONFIRMATION;
    /* Store the processed transition */
    Xcp_ProcessedTransition = (uint8) XCP_TXWAIT_CONFIRMATION;
    /* Do entry actions for TX_CONFIRMATION */
    /* Reset transmit data length */
    Xcp_TxPdu.SduLength = 0U;
  }
  else
  {
    /* do nothing */
  }

  DBG_XCP_TXPROCESSSTATERETRY_EXIT();
}

/*-----------------------------[Xcp_TxRetry]-----------------------------*/

STATIC FUNC(void, XCP_CODE) Xcp_TxRetry(void)
{
  /* Local variable to collect return value of lower layer
  transmit function */
  Std_ReturnType RetVal;

  DBG_XCP_TXRETRY_ENTRY();

#if (XCP_STORE_DAQ_SUPPORTED == STD_ON)
  /* If the EV_RESUME_MODE is pending to be transmitted, do not adjust the retry count, as
   * we must retry until a message was received or the event was transmitted */
  if (Xcp_EvResumeModeTransmitPending == FALSE)
#endif
  {
    /* Decrement retry count */
    Xcp_RetryCount--;
  }

  /* Clear all erroneous transitions if any */
  Xcp_Transition = XCP_TX_TRANSITION_DEFAULT;

  RetVal = Xcp_TransmitMessage();

  if (RetVal != E_OK)
  {
    /* If transmit request failed , Check whether retries are over */
    if (Xcp_RetryCount == 0U)
    {
      /* Emit to TXRETRY_ABORT */
      Xcp_Transition |= (uint8)XCP_TXRETRY_ABORT;
    }
  }

  DBG_XCP_TXRETRY_EXIT();
}

#endif /* (XCP_TX_RETRY_COUNT > 0) || (XCP_STORE_DAQ_SUPPORTED == STD_ON) */

/*-----------------------------[Xcp_Transmit]-----------------------------*/

STATIC FUNC(void, XCP_CODE) Xcp_Transmit(void)
{
  /* Local variable to collect command processor component return  */
  Xcp_ErrorType RetVal;

  DBG_XCP_TRANSMIT_ENTRY();

  /* Initialize transmit data length */
  Xcp_TxPdu.SduLength = 0U;

#if (STD_ON == XCP_SINGLE_PACKET_MESSAGE)
#if (XCP_EVENT_PACKET_ENABLED == STD_ON)
  /* Get the Event CTO data if available */
  RetVal = Xcp_GetEventCTO( &(Xcp_TxPdu.SduDataPtr[XCP_FRAME_HEADER_LENGTH]),
                            &Xcp_TxPdu.SduLength,
                            (PduLengthType)(XCP_TX_BUFFER_SIZE - XCP_FRAME_HEADER_LENGTH)
                          );
  /* If Event CTO not available */
  if (XCP_E_NOT_OK == RetVal)
  {
#endif
    /*Get Response CTO data if available*/
    RetVal = Xcp_GetCTO( &(Xcp_TxPdu.SduDataPtr[XCP_FRAME_HEADER_LENGTH]),
                         &Xcp_TxPdu.SduLength,
                         (PduLengthType)(XCP_TX_BUFFER_SIZE - XCP_FRAME_HEADER_LENGTH)
                       );

    /* If DAQ is available */
#if (XCP_RESOURCE_DAQ == XCP_RESOURCE_DAQ_MASK)
    /* If there is no CTO to transmit */
    if (XCP_E_NOT_OK == RetVal)
    {
        /* Copy pending DTO from DTO queue */
        (void)Xcp_GetDTO( &(Xcp_TxPdu.SduDataPtr[XCP_FRAME_HEADER_LENGTH]),
                          &Xcp_TxPdu.SduLength,
                          (const PduLengthType)(XCP_TX_BUFFER_SIZE - XCP_FRAME_HEADER_LENGTH)
                        );
    }
#else
  TS_PARAM_UNUSED(RetVal);
#endif /* #if (XCP_RESOURCE_DAQ == XCP_RESOURCE_DAQ_MASK) */

#if (XCP_EVENT_PACKET_ENABLED == STD_ON)
  }
#endif

#endif /*(STD_OFF == XCP_SINGLE_PACKET_MESSAGE)*/

  /* Clear all erroneous transitions if any */
  Xcp_Transition = XCP_TX_TRANSITION_DEFAULT;

  /* Check if there is anything to transmit */
  if (0U < Xcp_TxPdu.SduLength)
  {
#if ((XCP_ON_ETHERNET_ENABLED == STD_ON) || (XCP_ON_FLEXRAY_ENABLED == STD_ON))
    /* Add frame header and tail */
    Xcp_AddFrameHeaderAndTail( &Xcp_TxPdu );
#endif

    /* Try to transmit the message */
    if (Xcp_TransmitMessage() != E_OK )
    {
      /* Check whether we should retry or abort the message */
#if (XCP_TX_RETRY_COUNT > 0)
      /* A general retry mechanism is available, retry */
      Xcp_Transition |= (uint8)XCP_TXWAIT_RETRY;
#elif (XCP_STORE_DAQ_SUPPORTED == STD_ON)
      /* A general retry mechanism is not available, but it might be that we need to retry
       * the EV_RESUME_MODE event packet */
      if (Xcp_EvResumeModeTransmitPending == TRUE)
      {
        /* Retry */
        Xcp_Transition |= (uint8)XCP_TXWAIT_RETRY;
      }
      else
      {
        /* Not the EV_RESUME_MODE packet, abort message as general retries are not supported*/
        Xcp_AbortMessageTransmission();
      }
#else /* a retry mechanism is not supported */
      Xcp_AbortMessageTransmission();
#endif
    }
  }

  DBG_XCP_TRANSMIT_EXIT();
}

/*-----------------------------[Xcp_TransmitMessage]------------------------*/

STATIC FUNC(Std_ReturnType, XCP_CODE)  Xcp_TransmitMessage(void)
{
  /* Local variable to collect return value of lower layer transmit
   * function */
  Std_ReturnType RetVal;

  DBG_XCP_TRANSMITMESSAGE_ENTRY();

  /* prepare next transition to avoid  TxConfirmation callback deadlock */
  /* If TriggerTransmit function will be called by lower layer */
#if (XCP_TX_TRIGGER_ENABLED == STD_ON)
  /* Emit TXWAIT_TRIGGER */
  Xcp_Transition |= (uint8)XCP_TXWAIT_TRIGGER;
  /* clear callback flag for trigger transmit */
  Xcp_TriggerCallbackReceived = FALSE;
#else
  /* Emit TXWAIT_CONFIRMATION */
  Xcp_Transition |= (uint8)XCP_TXWAIT_CONFIRMATION;
#endif
  /* clear callback flags for TX confirmation */
  Xcp_TxConfCallbackReceived = FALSE;

  /* Call the configured lower layer module transmit function */
  RetVal = XCP_TP_INTERFACE_TRANSMIT(XCP_RES_TX_PDU_ID, &Xcp_TxPdu);

  /* If transmit request was not successful*/
  if (RetVal != E_OK)
  {
    /* De-activate the Bus Timeout Counter in case the transmission was not accepted by the
     * underlying communication(meaning: when the TX state machine goes to Retry or
     * Idle state) */
    Xcp_BusTimeoutCounter.IsActive = FALSE;

#if (XCP_TX_TRIGGER_ENABLED == STD_ON)
    /* reset state bit from earlier setting */
    Xcp_Transition &= (uint8)(~(uint8)XCP_TXWAIT_TRIGGER);
#else
    /* reset state bit from earlier setting */
    Xcp_Transition &= (uint8)(~(uint8)XCP_TXWAIT_CONFIRMATION);
#endif
  }
  else
  {
    /* Activate the Bus Timeout Counter each time the transmission is accepted by the
     * underlying communication. */
    Xcp_BusTimeoutCounter.IsActive = TRUE;
    /* Reset the Bus Timeout Counter value each time the transmission is accepted by the
     * underlying communication. */
    Xcp_BusTimeoutCounter.Value = 0U;
#if (XCP_STORE_DAQ_SUPPORTED == STD_ON)
  /* Transmit is successful, check if the event packet EV_RESUME_MODE needed to be transmitted */
    if (Xcp_EvResumeModeTransmitPending == TRUE)
    {
      /* Clear the flag. The transmit was accepted.*/
      Xcp_EvResumeModeTransmitPending = FALSE;
    }
#endif /* XCP_STORE_DAQ_SUPPORTED == STD_ON */
  }

  DBG_XCP_TRANSMITMESSAGE_EXIT(RetVal);

  return RetVal;
}

/*-----------------------------[Xcp_AbortMessageTransmission]------------------------*/

STATIC FUNC(void, XCP_CODE)  Xcp_AbortMessageTransmission(void)
{
  DBG_XCP_ABORTMESSAGETRANSMISSION_ENTRY();

  /* If error needs to be reported to DEM */
#if (XCP_PROD_ERR_HANDLING_RETRY_FAILED == TS_PROD_ERR_REP_TO_DEM)
  /* Report DEM */
  Dem_ReportErrorStatus( XCP_E_RETRY_FAILED,DEM_EVENT_STATUS_FAILED);
  /* If error needs to be reported to DET */
#elif (XCP_PROD_ERR_HANDLING_RETRY_FAILED == TS_PROD_ERR_REP_TO_DET)
  /* Report DET */
  XCP_DET_REPORT_ERROR(XCP_SID_MAIN_FUNCTION, XCP_E_DEMTODET_RETRY_FAILED);
#endif /* XCP_PROD_ERR_HANDLING_RETRY_FAILED == TS_PROD_ERR_REP_TO_DEM */

  /* Reset transmit data size */
  Xcp_TxPdu.SduLength = 0U;
  /* Change the state to TX_IDLE */
  Xcp_Presentstate = XCP_TX_IDLE;

  DBG_XCP_ABORTMESSAGETRANSMISSION_EXIT();
}

/*-----------------------------[Xcp_AddFrameHeaderAndTail]------------------*/

#if ((XCP_ON_ETHERNET_ENABLED == STD_ON) || (XCP_ON_FLEXRAY_ENABLED == STD_ON))
STATIC FUNC(void, XCP_CODE) Xcp_AddFrameHeaderAndTail
(
  P2VAR(PduInfoType, AUTOMATIC, XCP_APPL_DATA) TxPduPtr
)
{

  DBG_XCP_ADDFRAMEHEADERANDTAIL_ENTRY(TxPduPtr);

#if (XCP_ON_ETHERNET_ENABLED == STD_ON)
  /* Increment Ethernet header frame transmit counter */
  Xcp_FrameTransmitCounter++;

  /* Ethernet XCP header (4 bytes) */
  TxPduPtr->SduDataPtr[0] = (uint8)(TxPduPtr->SduLength & 0xFFU);
  /* SduLength must be a 16bit value. We are not casting it explicitly to uint16 in order to
   * detect early if it has a different size than required by means of warnings/misra checks. */
  TxPduPtr->SduDataPtr[1] = (uint8)((uint8)(TxPduPtr->SduLength >> 8U) & 0xFFU);
  TxPduPtr->SduDataPtr[2] = (uint8)(Xcp_FrameTransmitCounter & 0xFFU);
  TxPduPtr->SduDataPtr[3] = (uint8)((uint8)(Xcp_FrameTransmitCounter >> 8U) & 0xFFU);
  TxPduPtr->SduLength += XCP_FRAME_HEADER_LENGTH;
#elif (XCP_ON_FLEXRAY_ENABLED == STD_ON)
  /* FlexRay XCP header */
  /* No content change after initialization in Xcp_InitTransmitProcessor()
     necessary as we don't support multiple frames and sequence correction
     and this function is only called for the static Tx buffer. */
  TxPduPtr->SduLength += XCP_FRAME_HEADER_LENGTH;

  /* FlexRay XCP tail: FlexRay messages must have an even length if measured
     in bytes. */
  if( (TxPduPtr->SduLength & 1U) == 1U)
  {
    TxPduPtr->SduDataPtr[TxPduPtr->SduLength] = XCP_RESERVED_BYTE;
    (TxPduPtr->SduLength)++;
  }
#endif

  DBG_XCP_ADDFRAMEHEADERANDTAIL_EXIT(TxPduPtr);
}
#endif /* XCP_ON_ETHERNET_ENABLED == STD_ON || XCP_ON_FLEXRAY_ENABLED == STD_ON */

STATIC FUNC(void, XCP_CODE) Xcp_ProcessBusTimeout(void)
{

  DBG_XCP_PROCESSBUSTIMEOUT_ENTRY();
  
  if(Xcp_BusTimeoutCounter.IsActive)
  {
    if(Xcp_BusTimeoutCounter.Value < XCP_TX_BUS_TO_COUNTER_MAX_VAL)
    {
      /* increment the Bus Timeout Counter's value until reaches the maximum configuration value */
      Xcp_BusTimeoutCounter.Value++;
    }
    else
    {
      /* Enter critical section */
      SchM_Enter_Xcp_SCHM_XCP_EXCLUSIVE_AREA_XCP_INTERNALS();

      /* Disconnect the slave in case the Bus Timeout Counter's maximum configuration value was
       * reached within the previous Xcp_MainFunction call */
      Xcp_Disconnect();

      /* Exit critical section */
      SchM_Exit_Xcp_SCHM_XCP_EXCLUSIVE_AREA_XCP_INTERNALS();
    }
  }
  else
  {
    /* nothing to do in this case*/
  }
  
  DBG_XCP_PROCESSBUSTIMEOUT_EXIT();
}

#define XCP_STOP_SEC_CODE
#include <MemMap.h>
/*==================[end of file]===========================================*/
