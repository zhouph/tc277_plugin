[!/**
 * \file
 *
 * \brief AUTOSAR Xcp
 *
 * This file contains the implementation of the AUTOSAR
 * module Xcp.
 *
 * \author Elektrobit Automotive GmbH, 91058 Erlangen, Germany
 *
 * Copyright 2005 - 2013 Elektrobit Automotive GmbH
 * All rights exclusively reserved for Elektrobit Automotive GmbH,
 * unless expressly agreed to otherwise.
 */!][!//
[!/*
*** multiple inclusion protection ***
*/!][!IF "not(var:defined('XCP_VARS_M'))"!][!/*
*/!][!VAR "XCP_VARS_M"="'true'"!][!/*

*** Number of predefined DAQ lists ***
*/!][!IF "XcpGeneral/XcpDaqSupported = 'true'"!][!/*
  */!][!VAR "XcpMinDaq" = "num:i(XcpGeneral/XcpMinDaq)"!][!/*
*/!][!ELSE!][!/*
  */!][!VAR "XcpMinDaq" = "0"!][!/*
*/!][!ENDIF!][!/*


*** Variable to check that master block mode for programming is supported ***
*/!][!VAR "XcpMasterBlockModePgmSupported" = "'false'"!][!/*
*/!][!IF "(XcpGeneral/XcpPgmSupported = 'true') and node:exists(XcpGeneral/XcpMasterBlockModePgmSupported) and (XcpGeneral/XcpMasterBlockModePgmSupported = 'true')"!][!/*
  */!][!VAR "XcpMasterBlockModePgmSupported" = "'true'"!][!/*
*/!][!ENDIF!][!/*

*** Variable to hold whether direction STIM is supported ***
*/!][!VAR "XcpStimSupported" = "'false'"!][!/*
*/!][!IF "(XcpGeneral/XcpDaqSupported = 'true') and node:exists(XcpGeneral/XcpStimSupported) and (XcpGeneral/XcpStimSupported = 'true')"!][!/*
  */!][!VAR "XcpStimSupported" = "'true'"!][!/*
*/!][!ENDIF!][!/*

*** Length of the identification field (PID) ***
*/!][!VAR "XcpPidSize" = "0"!][!/*
*/!][!IF "XcpGeneral/XcpIdentificationFieldType = 'ABSOLUTE'"!][!/*
  */!][!VAR "XcpPidSize" = "1"!][!/*
*/!][!ELSEIF "XcpGeneral/XcpIdentificationFieldType = 'RELATIVE_BYTE'"!][!/*
  */!][!VAR "XcpPidSize" = "2"!][!/*
*/!][!ELSEIF "XcpGeneral/XcpIdentificationFieldType = 'RELATIVE_WORD'"!][!/*
  */!][!VAR "XcpPidSize" = "3"!][!/*
*/!][!ELSE!][!/*
  */!][!VAR "XcpPidSize" = "4"!][!/*
*/!][!ENDIF!][!/*

*** XcpFrameHeaderLength: Length of a possible XCP frame header (0 for CAN, 1, 2 or 4 for FlexRay, 4 for Ethernet) ***
*/!][!VAR "XcpFrameHeaderLength" = "0"!][!/*
*/!][!IF "XcpGeneral/XcpOnEthernetEnabled = 'true'"!][!/*
  */!][!VAR "XcpFrameHeaderLength" = "4"!][!/*
*/!][!ELSEIF "XcpGeneral/XcpOnFlexRayEnabled = 'true'"!][!/*
  */!][!IF "XcpGeneral/XcpFlxHeaderAlignment = 'PACKET_ALIGNMENT_8'"!][!/*
    */!][!VAR "XcpFrameHeaderLength" = "1"!][!/*
  */!][!ELSEIF "XcpGeneral/XcpFlxHeaderAlignment = 'PACKET_ALIGNMENT_16'"!][!/*
    */!][!VAR "XcpFrameHeaderLength" = "2"!][!/*
  */!][!ELSE!][!/*
    */!][!VAR "XcpFrameHeaderLength" = "4"!][!/*
  */!][!ENDIF!][!/*
*/!][!ENDIF!][!/*

*/!][!VAR "XcpDaqConfigType" = "XcpGeneral/XcpDaqConfigType"!][!/*

*** Minimum size of an  ODT Entry. Must be updated when Xcp Supports Granularity other than BYTE.  ***
*/!][!VAR "XcpMinODTEntrySize" = "1"!][!/*

*** Length of the identification field (PID) ***
*/!][!VAR "XcpPidSize" = "0"!][!/*
*/!][!IF "XcpGeneral/XcpIdentificationFieldType = 'ABSOLUTE'"!][!/*
  */!][!VAR "XcpPidSize" = "1"!][!/*
*/!][!ELSEIF "XcpGeneral/XcpIdentificationFieldType = 'RELATIVE_BYTE'"!][!/*
  */!][!VAR "XcpPidSize" = "2"!][!/*
*/!][!ELSEIF "XcpGeneral/XcpIdentificationFieldType = 'RELATIVE_WORD'"!][!/*
  */!][!VAR "XcpPidSize" = "3"!][!/*
*/!][!ELSE!][!/*
  */!][!VAR "XcpPidSize" = "4"!][!/*
*/!][!ENDIF!][!/*

*** Number of predefined DAQ lists (MIN_DAQ) ***
*/!][!IF "XcpGeneral/XcpDaqSupported = 'true'"!][!/*
  */!][!VAR "XcpMinDaq" = "num:i(XcpGeneral/XcpMinDaq)"!][!/*
*/!][!ELSE!][!/*
  */!][!VAR "XcpMinDaq" = "0"!][!/*
*/!][!ENDIF!][!/*

*** Number of predefined ODTs ***
  */!][!VAR "XcpNoOfPreODTs" = "num:i(count(XcpConfig/*[1]/XcpDaqList/*[XcpDaqListNumber < $XcpMinDaq]/XcpOdt/*))"!][!/*

*** Number of predefined ODT Entries ***
  */!][!VAR "XcpNoOfPreODTEntries" = "num:i(count(XcpConfig/*[1]/XcpDaqList/*[XcpDaqListNumber < $XcpMinDaq]/XcpOdt/*/XcpOdtEntry/*))"!][!/*

*** Number of Dynamic Daq Lists ***
*/!][!IF "XcpGeneral/XcpDaqSupported = 'true' and $XcpDaqConfigType = 'DAQ_DYNAMIC'"!][!/*
  */!][!VAR "XcpDaqCount" = "num:i(XcpGeneral/XcpDaqCount)"!][!/*
*/!][!ELSE!][!/*
  */!][!VAR "XcpDaqCount" = "0"!][!/*
*/!][!ENDIF!][!/*

*** Total number of DAQ lists ***
*/!][!IF "XcpGeneral/XcpDaqSupported = 'true'"!][!/*
 */!][!IF "($XcpDaqConfigType = 'DAQ_STATIC')"!][!/*
   */!][!VAR "XcpMaxDaq" = "num:integer(count(XcpConfig/*[1]/XcpDaqList/*))"!][!/*
 */!][!ELSE!][!/*
   */!][!VAR "XcpMaxDaq" = "$XcpDaqCount + $XcpMinDaq"!][!/*
 */!][!ENDIF!][!/*
*/!][!ELSE!][!/*
 */!][!VAR "XcpMaxDaq" = "0"!][!/*
*/!][!ENDIF!][!/*

*** Total number of predefined/static DAQ lists ***
*/!][!IF "XcpGeneral/XcpDaqSupported = 'true'"!][!/*
 */!][!IF "($XcpDaqConfigType = 'DAQ_STATIC')"!][!/*
   */!][!VAR "XcpNoOfNonDynamicDaqLists" = "$XcpMaxDaq"!][!/*
 */!][!ELSE!][!/*
   */!][!VAR "XcpNoOfNonDynamicDaqLists" = "$XcpMinDaq"!][!/*
 */!][!ENDIF!][!/*
*/!][!ELSE!][!/*
 */!][!VAR "XcpNoOfNonDynamicDaqLists" = "0"!][!/*
*/!][!ENDIF!][!/*

*** Number of ODTs allowed for a Dynamic Daq List ***
*/!][!IF "XcpGeneral/XcpDaqSupported = 'true' and $XcpDaqConfigType = 'DAQ_DYNAMIC'"!][!/*
  */!][!VAR "XcpOdtCount" = "num:i(XcpGeneral/XcpOdtCount)"!][!/*
*/!][!ELSE!][!/*
  */!][!VAR "XcpOdtCount" = "0"!][!/*
*/!][!ENDIF!][!/*

*** Number of static ODTs ***
*/!][!VAR "XcpNoOfStaODTs" = "0"!][!/*
*/!][!IF "XcpGeneral/XcpDaqSupported = 'true'"!][!/*
  */!][!IF "$XcpDaqConfigType = 'DAQ_STATIC'"!][!/*
    */!][!IF "(($XcpMaxDaq - $XcpMinDaq) >0)"!][!/*
      */!][!LOOP "XcpConfig/*[1]/XcpDaqList/*[XcpDaqListNumber >= $XcpMinDaq]"!][!/*
        */!][!VAR "XcpNoOfStaODTs" ="num:i($XcpNoOfStaODTs + XcpMaxOdt)"!][!/*
     */!][!ENDLOOP!][!/*
    */!][!ENDIF!][!/*
  */!][!ENDIF!][!/*
*/!][!ENDIF!][!/*

*** Total number of possible ODTs ***
*/!][!VAR "XcpTotalNumberOfOdts" = "0"!][!/*
*/!][!IF "XcpGeneral/XcpDaqSupported = 'true'"!][!/*
  */!][!IF "$XcpDaqConfigType = 'DAQ_STATIC'"!][!/*
    */!][!VAR "XcpTotalNumberOfOdts" = "num:i($XcpNoOfPreODTs + $XcpNoOfStaODTs)"!][!/*
  */!][!ELSE!][!/*
    */!][!VAR "XcpTotalNumberOfOdts" = "num:i(($XcpDaqCount * $XcpOdtCount) + $XcpNoOfPreODTs)"!][!/*
  */!][!ENDIF!][!/*
*/!][!ENDIF!][!/*

*** Total number of predefined and/or static ODTs ***
*/!][!IF "XcpGeneral/XcpDaqSupported = 'true'"!][!/*
 */!][!IF "($XcpDaqConfigType = 'DAQ_STATIC')"!][!/*
   */!][!VAR "XcpNoOfNonDynamicODTs" = "$XcpTotalNumberOfOdts"!][!/*
 */!][!ELSE!][!/*
   */!][!VAR "XcpNoOfNonDynamicODTs" = "$XcpNoOfPreODTs"!][!/*
 */!][!ENDIF!][!/*
*/!][!ELSE!][!/*
 */!][!VAR "XcpNoOfNonDynamicODTs" = "0"!][!/*
*/!][!ENDIF!][!/*

*** Number of ODT Entries allowed for a dynamic ODT ***
*/!][!IF "XcpGeneral/XcpDaqSupported = 'true' and $XcpDaqConfigType = 'DAQ_DYNAMIC'"!][!/*
  */!][!VAR "XcpOdtEntriesCount" = "num:i(XcpGeneral/XcpOdtEntriesCount)"!][!/*
*/!][!ELSE!][!/*
  */!][!VAR "XcpOdtEntriesCount" = "0"!][!/*
*/!][!ENDIF!][!/*


*** Total number of Event channels ***
*/!][!VAR "XcpMaxEventChannel" = "count(XcpConfig/*[1]/XcpEventChannel/*)"!][!/*

*** Boolean to check if timestamp is configured or not ***
*/!][!VAR "XcpTimestampIsEnabled" = "(XcpGeneral/XcpTimestampType != 'NO_TIME_STAMP') and (XcpGeneral/XcpDaqSupported = 'true')"!][!/*

*** Variable to check if storage of DAQ list in NV memory is supported ***
*/!][!VAR "StoreDaq" = "'false'"!][!/*
*/!][!IF "node:exists(XcpGeneral/XcpStoreDaqSupported)"!][!/*
  */!][!VAR "StoreDaq" = "XcpGeneral/XcpStoreDaqSupported"!][!/*
*/!][!ENDIF!][!/*

*** The timestamp size ***
*/!][!VAR "XcpTimestampSize" = "num:i((number(XcpGeneral/XcpTimestampType ='NO_TIME_STAMP') * 0) + (number(XcpGeneral/XcpTimestampType ='ONE_BYTE') * 1) + (number(XcpGeneral/XcpTimestampType ='TWO_BYTE') * 2) + (number(XcpGeneral/XcpTimestampType ='FOUR_BYTE') * 4))"!][!/*

*** Calculate the free ODT entry space for DAQ lists with timestamp enabled ***
*/!][!VAR "XcpAbsoluteMaxOdtEntrySize" = "num:i((XcpGeneral/XcpMaxDto - ($XcpTimestampSize + $XcpPidSize)) div $XcpMinODTEntrySize)"!][!/*

*** Number of static ODT Entries  ***
*/!][!VAR "XcpNoOfStaODTEntries" = "0"!][!/*
*/!][!IF "XcpGeneral/XcpDaqSupported = 'true'"!][!/*
  */!][!IF "$XcpDaqConfigType = 'DAQ_STATIC'"!][!/*
    */!][!IF "(($XcpMaxDaq - $XcpMinDaq) >0)"!][!/*
      */!][!LOOP "XcpConfig/*[1]/XcpDaqList/*[XcpDaqListNumber >= $XcpMinDaq]"!][!/*
        */!][!IF "(XcpMaxOdtEntries >= $XcpAbsoluteMaxOdtEntrySize) and ($XcpTimestampIsEnabled = 'true')"!][!/*
          */!][!VAR "XcpNoOfStaODTEntries" ="num:i($XcpNoOfStaODTEntries + (XcpMaxOdt * XcpMaxOdtEntries)) - $XcpTimestampSize"!][!/*
        */!][!ELSE!][!/*
          */!][!VAR "XcpNoOfStaODTEntries" ="num:i($XcpNoOfStaODTEntries + (XcpMaxOdt * XcpMaxOdtEntries))"!][!/*
        */!][!ENDIF!][!/*
     */!][!ENDLOOP!][!/*
    */!][!ENDIF!][!/*
  */!][!ENDIF!][!/*
*/!][!ENDIF!][!/*

*** Total number of predefined and/or static ODT Entries ***
*/!][!IF "XcpGeneral/XcpDaqSupported = 'true'"!][!/*
 */!][!IF "($XcpDaqConfigType = 'DAQ_STATIC')"!][!/*
   */!][!VAR "XcpNoOfNonDynamicODTEntries" = "num:i($XcpNoOfPreODTEntries + $XcpNoOfStaODTEntries)"!][!/*
 */!][!ELSE!][!/*
   */!][!VAR "XcpNoOfNonDynamicODTEntries" = "$XcpNoOfPreODTEntries"!][!/*
 */!][!ENDIF!][!/*
*/!][!ELSE!][!/*
 */!][!VAR "XcpNoOfNonDynamicODTEntries" = "0"!][!/*
*/!][!ENDIF!][!/*

*** Variable to calculate the number of bits for the Bus Timeout Counter's data type ***
*/!][!IF "num:i(round(num:f(num:div(XcpGeneral/XcpTxBusTimeout, XcpGeneral/XcpMainFunctionPeriod)))) < bit:shl(1,8)"!][!/*
  */!][!VAR "BusToTypeSize" = "num:i(8)"!][!/*
*/!][!ELSEIF "num:i(round(num:f(num:div(XcpGeneral/XcpTxBusTimeout, XcpGeneral/XcpMainFunctionPeriod)))) < bit:shl(1,16)"!][!/*
  */!][!VAR "BusToTypeSize" = "num:i(16)"!][!/*
*/!][!ELSE!][!/*
  */!][!VAR "BusToTypeSize" = "num:i(32)"!][!/*
*/!][!ENDIF!][!/*

*** End of file ***
*/!][!ENDIF!][!/*
*/!][!//
