/**
 * \file
 *
 * \brief AUTOSAR Xcp
 *
 * This file contains the implementation of the AUTOSAR
 * module Xcp.
 *
 * \author Elektrobit Automotive GmbH, 91058 Erlangen, Germany
 *
 * Copyright 2005 - 2013 Elektrobit Automotive GmbH
 * All rights exclusively reserved for Elektrobit Automotive GmbH,
 * unless expressly agreed to otherwise.
 */
[!INCLUDE "Xcp_Vars.m"!][!//
#if (!defined XCP_CFG_H)
#define XCP_CFG_H

/* This file contains the generated Xcp module configuration. */

[!AUTOSPACING!]
[!//
[!VAR "Spaces31"  = "'                               '"!]
[!//

[!IF "text:tolower(XcpGeneral/XcpDaqSupported) = 'true'"!]
  [!VAR "TimestampSize" = "num:integer((number(XcpGeneral/XcpTimestampType ='NO_TIME_STAMP') * 0) + (number(XcpGeneral/XcpTimestampType ='ONE_BYTE') * 1) + (number(XcpGeneral/XcpTimestampType ='TWO_BYTE') * 2) + (number(XcpGeneral/XcpTimestampType ='FOUR_BYTE') * 4))"!]
  [!VAR "IdFieldSize" = "num:integer((number(XcpGeneral/XcpIdentificationFieldType ='ABSOLUTE') * 1) + (number(XcpGeneral/XcpIdentificationFieldType ='RELATIVE_BYTE') * 2) + (number(XcpGeneral/XcpIdentificationFieldType ='RELATIVE_WORD') * 3) + (number(XcpGeneral/XcpIdentificationFieldType ='RELATIVE_WORD_ALIGNED') * 4))"!]
  [!VAR "AddressGranularity" = "num:integer("1")"!]
  [!VAR "MaxOdtEntrySize" = "num:integer(XcpGeneral/XcpOdtEntrySizeDaq)"!]
  [!IF "$MaxOdtEntrySize < num:integer(XcpGeneral/XcpOdtEntrySizeStim)"!]
    [!VAR "MaxOdtEntrySize" = "num:integer(XcpGeneral/XcpOdtEntrySizeStim)"!]
  [!ENDIF!]
  [!VAR "MaxOdtEntriesPerOdt" = "0"!]
  [!VAR "TempMax" = "0"!]
  [!LOOP "XcpConfig/*[1]/XcpDaqList/*"!]
    [!IF "./XcpDaqListNumber < $XcpMinDaq"!]
      [!LOOP "./XcpOdt/*"!]
        [!VAR "TempMax" = "count(./XcpOdtEntry/*)"!]
        [!IF "$MaxOdtEntriesPerOdt < $TempMax"!]
          [!VAR "MaxOdtEntriesPerOdt" = "$TempMax"!]
        [!ENDIF!]
      [!ENDLOOP!]
    [!ELSE!]
      [!IF "$MaxOdtEntriesPerOdt < ./XcpMaxOdtEntries"!]
        [!VAR "MaxOdtEntriesPerOdt" = "./XcpMaxOdtEntries"!]
      [!ENDIF!]
    [!ENDIF!]
  [!ENDLOOP!]
  [!IF "XcpGeneral/XcpDaqConfigType = 'DAQ_DYNAMIC'"!]
    [!IF "$MaxOdtEntriesPerOdt < XcpGeneral/XcpOdtEntriesCount"!]
      [!VAR "MaxOdtEntriesPerOdt" = "XcpGeneral/XcpOdtEntriesCount"!]
    [!ENDIF!]
  [!ENDIF!]
  [!IF "XcpGeneral/XcpMaxDto > num:i($TimestampSize + $IdFieldSize + ($MaxOdtEntrySize * $MaxOdtEntriesPerOdt * $AddressGranularity))"!]
    [!WARNING!]XcpMaxDto is bigger than the maximum possible DTO packet length which results in a waste of memory. The maximum length for a DTO packet is (sizeof identification field type + sizeof timestamp field + (maximum number of ODT entries in an ODT * maximum size of a ODT entry * address granularity)).[!ENDWARNING!]
  [!ENDIF!]
[!ENDIF!]

[!IF "text:tolower(XcpGeneral/XcpOnEthernetEnabled) = 'true'"!]
[!IF "node:exists(XcpConfig/*[1]/XcpPdu/*[@name='XcpRxPdu'][1])"!]
  [!VAR "RxPduRef" = "XcpConfig/*[1]/XcpPdu/*[@name='XcpRxPdu'][1]/XcpRxPduRef"!]
  [!LOOP "as:modconf('SoAd')[1]/SoAdConfig/*/SoAdSocketRoute/*"!]
    [!IF "node:path(node:ref(SoAdSocketRouteDest/*[1]/SoAdRxPduRef))= node:path(node:ref($RxPduRef))"!]
      [!IF "node:refvalid(./SoAdRxSocketConnectionRef)"!]
        [!VAR "SoAdSocketProtocolUdp" = "node:ref(./SoAdRxSocketConnectionRef)/../../SoAdSocketProtocol = 'SoAdSocketUdp'"!]
      [!ENDIF!]
    [!ENDIF!]
  [!ENDLOOP!]
[!ENDIF!]
[!ENDIF!]

[!VAR "DynamicAreaSize" = "0"!]
[!IF "(text:tolower(XcpGeneral/XcpDaqSupported) = 'true') and ($XcpDaqConfigType = 'DAQ_DYNAMIC')"!]
  [!IF "num:integer(XcpGeneral/XcpDynamicAreaSize mod 4) = 0"!]
[!VAR "DynamicAreaSize" = "num:integer(XcpGeneral/XcpDynamicAreaSize)"!]
  [!ELSE!]
[!VAR "DynamicAreaSize" = "num:integer(XcpGeneral/XcpDynamicAreaSize + (4 - (XcpGeneral/XcpDynamicAreaSize mod 4)))"!]
  [!ENDIF!]
[!ENDIF!]

/*==================[includes]===============================================*/

#include <Std_Types.h>    /* AUTOSAR standard types */
#include <Xcp_Types.h>    /* Xcp types to be published */
#include <TSAutosar.h>    /* EB specific standard types */

/*==================[macros]=================================================*/

#if (defined XCP_VERSION_INFO_API)
#error XCP_VERSION_INFO_API already defined
#endif
/** \brief Switch, indicating whether API version information is
 ** activated or deactivated  for XCP */
#define XCP_VERSION_INFO_API         [!//
[!IF "text:tolower(XcpGeneral/XcpVersionInfoApi) = 'true'"!]STD_ON[!ELSE!]STD_OFF[!ENDIF!]


#if (defined XCP_ON_CAN_ENABLED)
#error XCP_ON_CAN_ENABLED already defined
#endif
/** \brief Switch, indicating whether XCPonCAN functionality is available */
#define XCP_ON_CAN_ENABLED           [!//
[!IF "text:tolower(XcpGeneral/XcpOnCanEnabled) = 'true'"!]STD_ON[!ELSE!]STD_OFF[!ENDIF!]


#if (defined XCP_ON_FLEXRAY_ENABLED)
#error XCP_ON_FLEXRAY_ENABLED already defined
#endif
/** \brief Switch, indicating whether XCPonFlexRay functionality is available */
#define XCP_ON_FLEXRAY_ENABLED       [!//
[!IF "text:tolower(XcpGeneral/XcpOnFlexRayEnabled) = 'true'"!]STD_ON[!ELSE!]STD_OFF[!ENDIF!]

#if (defined XCP_ON_ETHERNET_ENABLED)
#error XCP_ON_ETHERNET_ENABLED already defined
#endif
/** \brief Switch, indicating whether XCPonEthernet functionality is available */
#define XCP_ON_ETHERNET_ENABLED      [!//
[!IF "text:tolower(XcpGeneral/XcpOnEthernetEnabled) = 'true'"!]STD_ON[!ELSE!]STD_OFF[!ENDIF!]

[!IF "text:tolower(XcpGeneral/XcpOnEthernetEnabled) = 'true'"!]
#if (defined XCP_SOAD_SOCKET_PROTOCOL_UDP)
#error XCP_SOAD_SOCKET_PROTOCOL_UDP already defined
#endif
/** \brief Switch, indicating whether SoAd is configured using UDP protocol.
 ** If UDP is used the socket must be closed if it is not used anymore.
 ** Otherwise the socket would be locked to the initial source port,
 ** preventing further requests from other ports.
 **/
#define XCP_SOAD_SOCKET_PROTOCOL_UDP          [!//
[!IF "$SoAdSocketProtocolUdp"!]STD_ON[!ELSE!]STD_OFF[!ENDIF!]
[!ENDIF!]


[!IF "ReportToDem/XcpRetryFailedReportToDem = 'DET'"!][!//
#if (defined XCP_E_DEMTODET_RETRY_FAILED)
#error XCP_E_DEMTODET_RETRY_FAILED already defined
#endif
/* DET error Id for Xcp Retry Failed */
#define XCP_E_DEMTODET_RETRY_FAILED  [!//
[!"ReportToDem/XcpRetryFailedDemDetErrorId"!]U
[!ENDIF!][!//

/*------------------[symbolic name values]----------------------------------*/
[!LOOP "as:modconf('Xcp')[1]/XcpConfig/*[1]/XcpPdu/*[@name='XcpTxPdu']"!]

#if (defined XcpConf_XcpPdu_[!"name(.)"!])
#error XcpConf_XcpPdu_[!"name(.)"!] already defined
#endif
/** \brief Symbolic name value of the Handle ID for receiving a TxConfirmation or a TriggerTransmit
 **
 ** This Handle ID must be used by the underlying communication layer after the Xcp requested a
 ** transmission.
 **/
#define XcpConf_XcpPdu_[!"name(.)"!]        [!"XcpTxPduId"!]U

#if (!defined XCP_DONT_PROVIDE_LEGACY_SYMBOLIC_NAMES)
#if (defined [!"name(.)"!])
#error [!"name(.)"!] is already defined
#endif
/** \brief Export symbolic name value without prefix (AUTOSAR version <= 3.1 rev4) */
#define [!"name(.)"!]        [!"XcpTxPduId"!]U

#if (defined Xcp_[!"name(.)"!])
#error Xcp_[!"name(.)"!] is already defined
#endif
/** \brief Export symbolic name value with module abbreviation as prefix only (3.1 rev4 < AUTOSAR version <= AUTOSAR 4.0 rev2) */
#define Xcp_[!"name(.)"!]        [!"XcpTxPduId"!]U
#endif /* XCP_DONT_PROVIDE_LEGACY_SYMBOLIC_NAMES */
[!ENDLOOP!]

[!LOOP "as:modconf('Xcp')[1]/XcpConfig/*[1]/XcpPdu/*[@name='XcpRxPdu']"!]

#if (defined XcpConf_XcpPdu_[!"name(.)"!])
#error XcpConf_XcpPdu_[!"name(.)"!] already defined
#endif
/** \brief Symbolic name value for the Handle IDs used to receive packets from the master. */
#define XcpConf_XcpPdu_[!"name(.)"!]        [!"XcpRxPduId"!]U

#if (!defined XCP_DONT_PROVIDE_LEGACY_SYMBOLIC_NAMES)
#if (defined [!"name(.)"!])
#error [!"name(.)"!] is already defined
#endif
/** \brief Export symbolic name value without prefix (AUTOSAR version <= 3.1 rev4) */
#define [!"name(.)"!]        [!"XcpRxPduId"!]U

#if (defined Xcp_[!"name(.)"!])
#error Xcp_[!"name(.)"!] is already defined
#endif
/** \brief Export symbolic name value with module abbreviation as prefix only (3.1 rev4 < AUTOSAR version <= AUTOSAR 4.0 rev2) */
#define Xcp_[!"name(.)"!]        [!"XcpRxPduId"!]U
#endif /* XCP_DONT_PROVIDE_LEGACY_SYMBOLIC_NAMES */
[!ENDLOOP!]

[!LOOP "XcpConfig/*[1]/XcpEventChannel/*"!]

#if (defined XcpConf_XcpEventChannel_[!"name(.)"!])
#error XcpConf_XcpEventChannel_[!"name(.)"!] already defined
#endif
/** \brief The Index number of the event channel*/
#define XcpConf_XcpEventChannel_[!"name(.)"!]         [!"XcpEventChannelNumber"!]U

#if (!defined XCP_DONT_PROVIDE_LEGACY_SYMBOLIC_NAMES)
#if (defined [!"name(.)"!])
#error [!"name(.)"!] is already defined
#endif
/** \brief Export symbolic name value without prefix (AUTOSAR version <= 3.1 rev4) */
#define [!"name(.)"!]        [!"XcpEventChannelNumber"!]U

#if (defined Xcp_[!"name(.)"!])
#error Xcp_[!"name(.)"!] is already defined
#endif
/** \brief Export symbolic name value with module abbreviation as prefix only (3.1 rev4 < AUTOSAR version <= AUTOSAR 4.0 rev2) */
#define Xcp_[!"name(.)"!]        [!"XcpEventChannelNumber"!]U
#endif /* XCP_DONT_PROVIDE_LEGACY_SYMBOLIC_NAMES */
[!ENDLOOP!]

[!LOOP "XcpConfig/*/XcpDaqList/*"!]

#if (defined XcpConf_XcpDaqList_[!"name(.)"!])
#error XcpConf_XcpDaqList_[!"name(.)"!] already defined
#endif
/** \brief The Index number of the static/predefined DAQ List*/
#define XcpConf_XcpDaqList_[!"name(.)"!]         [!"XcpDaqListNumber"!]U

#if (!defined XCP_DONT_PROVIDE_LEGACY_SYMBOLIC_NAMES)
#if (defined [!"name(.)"!])
#error [!"name(.)"!] is already defined
#endif
/** \brief Export symbolic name value without prefix (AUTOSAR version <= 3.1 rev4) */
#define [!"name(.)"!]        [!"XcpDaqListNumber"!]U

#if (defined Xcp_[!"name(.)"!])
#error Xcp_[!"name(.)"!] is already defined
#endif
/** \brief Export symbolic name value with module abbreviation as prefix only (3.1 rev4 < AUTOSAR version <= AUTOSAR 4.0 rev2) */
#define Xcp_[!"name(.)"!]        [!"XcpDaqListNumber"!]U
#endif /* XCP_DONT_PROVIDE_LEGACY_SYMBOLIC_NAMES */
[!ENDLOOP!]


[!LOOP "XcpConfig/*/XcpDaqList/*[XcpDaqListNumber < as:modconf('Xcp')[1]/XcpGeneral/XcpMinDaq]/XcpOdt/*"!]

#if (defined XcpConf_[!"../../@name"!]_[!"name(.)"!])
#error XcpConf_[!"../../@name"!]_[!"name(.)"!] already defined
#endif
/** \brief Index number of ODT [!"XcpOdtNumber"!] within the predefined DAQ list [!"../../XcpDaqListNumber"!] */
#define XcpConf_[!"../../@name"!]_[!"name(.)"!]         [!"XcpOdtNumber"!]U

#if (!defined XCP_DONT_PROVIDE_LEGACY_SYMBOLIC_NAMES)
#if (defined [!"../../@name"!]_[!"name(.)"!])
#error [!"../../@name"!]_[!"name(.)"!] is already defined
#endif
/** \brief Export symbolic name value without prefix (AUTOSAR version <= 3.1 rev4) */
#define [!"../../@name"!]_[!"name(.)"!]        [!"XcpOdtNumber"!]U

#if (defined Xcp_[!"../../@name"!]_[!"name(.)"!])
#error Xcp_[!"../../@name"!]_[!"name(.)"!] is already defined
#endif
/** \brief Export symbolic name value with module abbreviation as prefix only (3.1 rev4 < AUTOSAR version <= AUTOSAR 4.0 rev2) */
#define Xcp_[!"../../@name"!]_[!"name(.)"!]        [!"XcpOdtNumber"!]U
#endif /* XCP_DONT_PROVIDE_LEGACY_SYMBOLIC_NAMES */
[!ENDLOOP!]

#if (defined XCP_MAX_DAQ)
#error XCP_MAX_DAQ already defined
#endif
/** \brief The maximum number of DAQ lists on the XCP slave. */
#define XCP_MAX_DAQ                  [!//
[!"num:integer($XcpMaxDaq)"!]U

#if (defined XCP_NO_OF_NON_DYNAMIC_DAQ_LISTS)
#error XCP_NO_OF_NON_DYNAMIC_DAQ_LISTS already defined
#endif
/** \brief The number of predefined/static DAQ lists */
#define XCP_NO_OF_NON_DYNAMIC_DAQ_LISTS             [!//
[!"num:integer($XcpNoOfNonDynamicDaqLists)"!]U

#if (defined XCP_NO_OF_NON_DYNAMIC_ODTS)
#error XCP_NO_OF_NON_DYNAMIC_ODTS already defined
#endif
/** \brief The total number of predefined/static ODTs */
#define XCP_NO_OF_NON_DYNAMIC_ODTS                   [!//
[!"num:integer($XcpNoOfNonDynamicODTs)"!]U

#if (defined XCP_NO_OF_NON_DYNAMIC_ODTENTRIES)
#error XCP_NO_OF_NON_DYNAMIC_ODTENTRIES already defined
#endif
/** \brief The total number of predefined/static ODT entries */
#define XCP_NO_OF_NON_DYNAMIC_ODTENTRIES             [!//
[!"num:integer($XcpNoOfNonDynamicODTEntries)"!]U

#if (defined XCP_MAX_ODT)
#error XCP_MAX_ODT already defined
#endif
/** \brief Total number of ODTs possible in the slave.
 **
 **/
#define XCP_MAX_ODT                                        [!"num:integer($XcpTotalNumberOfOdts)"!]U

#if (defined XCP_DYNAMIC_AREA_SIZE)
#error XCP_DYNAMIC_AREA_SIZE already defined
#endif
/** \brief The size in bytes of the memory area used for dynamic DAQ list configuration.
 **
 **/
#define XCP_DYNAMIC_AREA_SIZE                             [!"num:integer($DynamicAreaSize)"!]U


#if (defined XCP_NUMBER_OF_BITS_IN_BYTE)
#error XCP_NUMBER_OF_BITS_IN_BYTE already defined
#endif
/** \brief The number of bits in a byte */
#define XCP_NUMBER_OF_BITS_IN_BYTE        8U

[!IF "text:tolower($StoreDaq) = 'true'"!]

#if (defined XCP_NUMBER_OF_BYTES_SELECTED_DAQ)
#error XCP_NUMBER_OF_BYTES_SELECTED_DAQ already defined
#endif
/** \brief The number of bytes needed to hold the information of which DAQ lists is selected to
 * start immediately after a DAQ store in NV was completed successfully. */
#define XCP_NUMBER_OF_BYTES_SELECTED_DAQ  (((XCP_MAX_DAQ-1U)/XCP_NUMBER_OF_BITS_IN_BYTE) + 1U)

#if (defined XCP_NVM_DAQ_LISTS_RAM_ADDRESS)
#error XCP_NVM_DAQ_LISTS_RAM_ADDRESS already defined
#endif
/** \brief Symbolic name for the DAQ lists runtime structure used to configure the NvM block */
#define XCP_NVM_DAQ_LISTS_RAM_ADDRESS     &Xcp_DaqLists

[!ENDIF!] [!/* $StoreDaq = 'true' */!]


#if (defined XCP_STORE_DAQ_SUPPORTED)
#error XCP_STORE_DAQ_SUPPORTED already defined
#endif
/** \brief Switch, indicating whether all DAQ lists can be stored in the NV memory. **/
#define XCP_STORE_DAQ_SUPPORTED         [!//
[!IF "text:tolower($StoreDaq) = 'true'"!]STD_ON[!ELSE!]STD_OFF[!ENDIF!]


#if (defined XCP_CAL_PAG_STORE_SUPPORTED)
#error XCP_CAL_PAG_STORE_SUPPORTED already defined
#endif
/** \brief Switch, indicating whether calibration data storing to NV memory functionality is available.
 **/
#define XCP_CAL_PAG_STORE_SUPPORTED          [!//
STD_[!IF "(node:exists(XcpGeneral/XcpCalPagStoreSupported) = 'true') and (text:tolower(XcpGeneral/XcpCalPagStoreSupported) = 'true')"!]ON[!ELSE!]OFF[!ENDIF!]

/*==================[type definitions]=======================================*/

[!IF "$XcpTimestampIsEnabled = 'true'"!]
/** \brief Timestamp Type definition
 ** */
[!IF "XcpGeneral/XcpTimestampType = 'ONE_BYTE'"!]
typedef uint8  Xcp_TimestampType;
[!ENDIF!]
[!IF "XcpGeneral/XcpTimestampType = 'TWO_BYTE'"!]
typedef uint16 Xcp_TimestampType;
[!ENDIF!]
[!IF "XcpGeneral/XcpTimestampType = 'FOUR_BYTE'"!]
typedef uint32 Xcp_TimestampType;
[!ENDIF!]

[!ENDIF!] [!/* XcpGeneral/XcpTimestampType != 'NO_TIME_STAMP and XcpGeneral/XcpDaqSupported = 'true' */!]

[!IF "$XcpMaxDaq > 0"!]

/** \brief DAQ Id type */
[!IF "XcpGeneral/XcpIdentificationFieldType = 'RELATIVE_BYTE'"!]
typedef uint8 Xcp_DaqIdType;
[!ELSE!]
typedef uint16 Xcp_DaqIdType;
[!ENDIF!]


/** \brief ODT Entry Type. */
typedef struct
{
  /* The address location of the ODT entry */
  P2VAR(uint8, TYPEDEF, XCP_APPL_DATA) Address;
  uint8 Length;     /* Size of the element in AG */
  uint8 BitOffset;  /* Position of the Bit to be considered */
  uint8 AddrExtn;   /* Address extension of the ODT */
} Xcp_OdtEntryType;

/** \brief ODT Table Type. */
typedef struct
{
  P2VAR(Xcp_OdtEntryType, TYPEDEF, XCP_VAR) OdtEntry; /* Pointer to OdtEntry
                                                         array or Object Descriptor table */
  uint8 NrOfOdtEntries;                               /* Number of elements in the ODT */
} Xcp_OdtType;

/** \brief DAQ List properties. */
typedef struct
{
  P2VAR(Xcp_OdtType, TYPEDEF, XCP_VAR) OdtList; /* Pointer to the ODT Array or DAQ List */
  uint16 EventId;      /* Event channel number for this DAQ */
  uint8 Mode;          /* Current  mode information of the DAQ List.
                              Bit7: Resume status,
                              Bit6: Running status,
                              Bit5: PID_OFF,
                              Bit4: Time Stamp,
                              Bit2: Selected,
                              Bit1: Direction,
                              Bit0: Alternating */
  uint8 Priority;      /* Priority of this DAQ */
[!IF "XcpGeneral/XcpIdentificationFieldType = 'ABSOLUTE'"!]
  uint8 FirstPID;      /* The first PID only, when using absolute ODT number */
[!ENDIF!]
  uint8 MaxOdt;        /* Number of ODTs in this DAQ list */
  uint8 MaxOdtEntries; /* Maximum number of ODT entries into an Object Descriptor Table
                          of this DAQ list. */
  uint8 Prescaler;     /* Preset value of the prescaler */
  uint8 PrescalerCnt;  /* Current value of the prescaler */
  uint8 Properties;   /*  Configuration properties of the DAQ List:
                              Bit7: RESERVED,
                              Bit6: RESERVED
                              Bit5: RESERVED,
                              Bit4: RESERVED,
                              Bit3: STIM direction allowed,
                              Bit2: DAQ direction allowed,
                              Bit1: EventFixed,
                              Bit0: Predefined */
  uint8 Flags;         /* Additional configuration of the DAQ List:
                              Bit7: RESERVED,
                              Bit6: RESERVED
                              Bit5: RESERVED,
                              Bit4: RESERVED,
                              Bit3: RESERVED,
                              Bit2: RESERVED,
                              Bit1: Configured,
                              Bit0: Ready to sample */
} Xcp_DaqType;

[!IF "text:tolower($StoreDaq) = 'true'"!]
/** \brief The administration structure saved in the NV upon a store request */
typedef struct
{
  uint16 ConfigurationSessionID; /* Session Configuration ID */
[!IF "$XcpDaqConfigType = 'DAQ_DYNAMIC'"!]
  Xcp_DaqIdType NoOfConfiguredDynamicDaqLists; /* How many Dynamic DAQ lists were allocated during a
                                                  runtime configuration */
[!ENDIF!]
  uint8 Mode;                    /* The requested mode:
                                    Bit1: STORE_DAQ_REQ_NORESUME
                                    Bit2: STORE_DAQ_REQ_RESUME
                                    Bit3: CLEAR_DAQ_REQ
                                  */
  uint8 SelectedDAQ[XCP_NUMBER_OF_BYTES_SELECTED_DAQ];    /* Each bit in this array corresponds to a
                                                          DAQ list. If the bit is set it means that
                                                          the DAQ list was selected to be stored */
} Xcp_StoreDAQType;
[!ENDIF!]  [!/* $StoreDaq  = 'true' */!]

[!IF "$XcpMaxDaq > $XcpDaqCount"!] [!/* We have static or predefined DAQ lists */!]
/** \brief Static/predefined DAQ list related information grouped into a contiguous area. */
typedef struct
{
  Xcp_DaqType Xcp_Daq[XCP_NO_OF_NON_DYNAMIC_DAQ_LISTS];    /* All DAQ lists wich are not dynamically
                                                              allocated */
  Xcp_OdtType Xcp_Odt[XCP_NO_OF_NON_DYNAMIC_ODTS];         /* All ODTs wich are not dynamically
                                                              allocated */
  Xcp_OdtEntryType Xcp_OdtEntry[XCP_NO_OF_NON_DYNAMIC_ODTENTRIES]; /* All ODT entries wich are not
                                                                      dynamically allocated */

} Xcp_NonDynamicDaqListsType;
[!ENDIF!]

/** \brief All DAQ list related information grouped into a contiguous area
 ** Grouping them enables the DAQ lists storage in NV memory using a single NvM block. */
typedef struct
{
[!IF "$XcpDaqConfigType = 'DAQ_DYNAMIC'"!]
  /* Xcp_DynamicArea array must be the first variable of this structure due to alignment
   restrictions - the start address will be converted into a Xcp_DaqType structure. */
  uint8 Xcp_DynamicArea[XCP_DYNAMIC_AREA_SIZE];
[!ENDIF!]
  /* This dummy variable is added so that the entire structure is aligned to 4 bytes in order to
  prevent inconsistencies between what sizeof(Xcp_DaqLists) is providing and the actual size of
  Xcp_DaqLists in the map file after the linkage. Without this, the members of this structure are
  aligned to 2 bytes by the compiler, but the linker might align the entire structure to 4 bytes -
  thus adding extra bytes that the sizeof() will be unaware of.*/
  uint32 DummyVar;
[!IF "text:tolower($StoreDaq) = 'true'"!]
  Xcp_StoreDAQType Xcp_StoreDaq;
[!ENDIF!]
[!IF "$XcpMaxDaq > $XcpDaqCount"!]
  Xcp_NonDynamicDaqListsType Xcp_NonDynamicDaqLists;
[!ENDIF!]
} Xcp_AllDaqListsType;

[!ENDIF!]   [!/* MAX_DAQ > 0 */!]

/*==================[external function declarations]=========================*/

/*==================[internal function declarations]=========================*/

/*==================[external constants]=====================================*/

#define XCP_START_SEC_CONST_UNSPECIFIED
#include <MemMap.h>

/** \brief Configuration placeholder for Xcp_Init() call. */
extern CONST( Xcp_ConfigType, XCP_CONST ) [!"node:name(XcpConfig/*[1])"!];

#define XCP_STOP_SEC_CONST_UNSPECIFIED
#include <MemMap.h>

/*==================[internal constants]=====================================*/

/*==================[external data]==========================================*/

[!IF "$XcpMaxDaq > 0"!]

#define XCP_START_SEC_VAR_SAVED_ZONE_UNSPECIFIED
#include <MemMap.h>

/** \brief All DAQ list related information grouped together. */
extern VAR(Xcp_AllDaqListsType, XCP_VAR_NOINIT) Xcp_DaqLists;

#define XCP_STOP_SEC_VAR_SAVED_ZONE_UNSPECIFIED
#include <MemMap.h>

[!ENDIF!]   [!/* MAX_DAQ > 0 */!]

/*==================[internal data]==========================================*/

/*==================[external function definitions]==========================*/

/*==================[internal function definitions]==========================*/

#endif /* if !defined( XCP_CFG_H ) */
/*==================[end of file]============================================*/
