/**
 * \file
 *
 * \brief AUTOSAR Xcp
 *
 * This file contains the implementation of the AUTOSAR
 * module Xcp.
 *
 * \author Elektrobit Automotive GmbH, 91058 Erlangen, Germany
 *
 * Copyright 2005 - 2013 Elektrobit Automotive GmbH
 * All rights exclusively reserved for Elektrobit Automotive GmbH,
 * unless expressly agreed to otherwise.
 */

[!INCLUDE "include/Xcp_Vars.m"!][!//
/*==================[inclusions]============================================*/

#include <Std_Types.h>         /* AUTOSAR standard types */
#include <Xcp.h>               /* Module public API */
#include <Xcp_Int.h>           /* Module internal interface */
[!IF "normalize-space(XcpGeneral/XcpUserHeader) != ''"!][!//
#include <[!"normalize-space(XcpGeneral/XcpUserHeader)"!]> /* User-defined symbols */
[!ENDIF!][!//

[!AUTOSPACING!][!//

/*==================[macros]================================================*/

/*==================[type definitions]======================================*/

/*==================[external function declarations]========================*/

/*==================[internal function declarations]========================*/

/*==================[internal constants]====================================*/

[!VAR "IdentificationFieldType" = "XcpGeneral/XcpIdentificationFieldType"!]
[!VAR "XcpOdtEntriesCount" = "num:i(XcpGeneral/XcpOdtEntriesCount)"!]
[!VAR "MaxDto" = "num:i(XcpGeneral/XcpMaxDto)"!]

[!SELECT "XcpConfig/*[1]"!][!//

[!IF "count(XcpEventChannel/*/XcpEventChannelTriggeredDaqListRef/*) > 0"!]

#define XCP_START_SEC_CONST_16BIT
#include <MemMap.h>

/** \brief List of DAQ IDs in the order of events */
STATIC CONST(Xcp_DaqIdType, XCP_CONST) Xcp_DaqIdListConst[XCP_NUM_EVENT_CHANNEL_PRECFG] =
{
[!LOOP "node:order(XcpEventChannel/*, 'XcpEventChannelNumber')"!]
  /* Event channel: [!"XcpEventChannelNumber"!] */
  [!LOOP "node:order(XcpEventChannelTriggeredDaqListRef/*, 'as:ref(.)/XcpDaqListPriority')"!]
  [!"as:ref(.)/XcpDaqListNumber"!]U,
  [!ENDLOOP!]
[!ENDLOOP!]
};

#define XCP_STOP_SEC_CONST_16BIT
#include <MemMap.h>

[!ENDIF!]

#define XCP_START_SEC_CONST_8BIT
#include <MemMap.h>

[!LOOP "node:order(XcpEventChannel/*, 'XcpEventChannelNumber')"!]
/*  Event name of Event channel number : [!"XcpEventChannelNumber"!]  */
STATIC CONST(uint8, XCP_CONST) Xcp_EventName_[!"XcpEventChannelNumber"!][[!"num:i(string-length(name(.)))"!]] = "[!"name(.)"!]";

[!ENDLOOP!]

#define XCP_STOP_SEC_CONST_8BIT
#include <MemMap.h>

/*==================[external constants]====================================*/

#define XCP_START_SEC_CONST_UNSPECIFIED
#include <MemMap.h>

/** \brief Configuration placeholder for Xcp_Init() call. */
CONST( Xcp_ConfigType, XCP_CONST ) [!"node:name(.)"!] = { 0U };

#define XCP_STOP_SEC_CONST_UNSPECIFIED
#include <MemMap.h>


[!IF "$XcpMaxEventChannel > 0"!]

[!/* Local variables: time unit constants */!]
[!VAR "Event_Channel_Time_Unit_1ns"   = "0.000000001"!]     [!/* 1ns   = 0.000000001s */!]
[!VAR "Event_Channel_Time_Unit_10ns"  = "0.00000001"!]      [!/* 10ns  = 0.00000001s */!]
[!VAR "Event_Channel_Time_Unit_100ns" = "0.0000001"!]       [!/* 100ns = 0.0000001s */!]
[!VAR "Event_Channel_Time_Unit_1us"   = "0.000001"!]        [!/* 1us   = 0.000001s */!]
[!VAR "Event_Channel_Time_Unit_10us"  = "0.00001"!]         [!/* 10us  = 0.00001s */!]
[!VAR "Event_Channel_Time_Unit_100us" = "0.0001"!]          [!/* 100us = 0.0001s */!]
[!VAR "Event_Channel_Time_Unit_1ms"   = "0.001"!]           [!/* 1ms   = 0.001s */!]
[!VAR "Event_Channel_Time_Unit_10ms"  = "0.01"!]            [!/* 10ms  = 0.01s */!]
[!VAR "Event_Channel_Time_Unit_100ms" = "0.1"!]             [!/* 100ms = 0.1s */!]
[!VAR "Event_Channel_Time_Unit_1s"    = "1.0"!]             [!/* 1s    = 1s */!]
[!VAR "Event_Channel_Time_Unit_1ps"   = "0.000000000001"!]  [!/* 1ps   = 0.000000000001s */!]
[!VAR "Event_Channel_Time_Unit_10ps"  = "0.00000000001"!]   [!/* 10ps  = 0.00000000001s */!]
[!VAR "Event_Channel_Time_Unit_100ps" = "0.0000000001"!]    [!/* 100ps = 0.0000000001s */!]

[!/* Local variable: XcpMainFunctionPeriod*/!]
[!VAR "XcpMainFuncPeriod" = "../../XcpGeneral/XcpMainFunctionPeriod"!]

[!/* Macro: EVENT_CHANNEL_TIME_CYCLE_COUNTER_MAX*/!]
[!MACRO "EVENT_CHANNEL_TIME_CYCLE_COUNTER_MAX", "time_unit" = "'TIMESTAMP_UNIT_1MS'"!][!/*
  */!][!IF "$time_unit = 'TIMESTAMP_UNIT_1NS'"!][!/*
  */!]    [!"num:i(round(XcpEventChannelTimeCycle * $Event_Channel_Time_Unit_1ns div $XcpMainFuncPeriod))"!][!/*
  */!][!ELSEIF "$time_unit = 'TIMESTAMP_UNIT_10NS'"!][!/*
  */!]    [!"num:i(round(XcpEventChannelTimeCycle * $Event_Channel_Time_Unit_10ns div $XcpMainFuncPeriod))"!][!/*
  */!][!ELSEIF "$time_unit = 'TIMESTAMP_UNIT_100NS'"!][!/*
  */!]    [!"num:i(round(XcpEventChannelTimeCycle * $Event_Channel_Time_Unit_100ns div $XcpMainFuncPeriod))"!][!/*
  */!][!ELSEIF "$time_unit = 'TIMESTAMP_UNIT_1US'"!][!/*
  */!]    [!"num:i(round(XcpEventChannelTimeCycle * $Event_Channel_Time_Unit_1us div $XcpMainFuncPeriod))"!][!/*
  */!][!ELSEIF "$time_unit = 'TIMESTAMP_UNIT_10US'"!][!/*
  */!]    [!"num:i(round(XcpEventChannelTimeCycle * $Event_Channel_Time_Unit_10us div $XcpMainFuncPeriod))"!][!/*
  */!][!ELSEIF "$time_unit = 'TIMESTAMP_UNIT_100US'"!][!/*
  */!]    [!"num:i(round(XcpEventChannelTimeCycle * $Event_Channel_Time_Unit_100us div $XcpMainFuncPeriod))"!][!/*
  */!][!ELSEIF "$time_unit = 'TIMESTAMP_UNIT_1MS'"!][!/*
  */!]    [!"num:i(round(XcpEventChannelTimeCycle * $Event_Channel_Time_Unit_1ms div $XcpMainFuncPeriod))"!][!/*
  */!][!ELSEIF "$time_unit = 'TIMESTAMP_UNIT_10MS'"!][!/*
  */!]    [!"num:i(round(XcpEventChannelTimeCycle * $Event_Channel_Time_Unit_10ms div $XcpMainFuncPeriod))"!][!/*
  */!][!ELSEIF "$time_unit = 'TIMESTAMP_UNIT_100MS'"!][!/*
  */!]    [!"num:i(round(XcpEventChannelTimeCycle * $Event_Channel_Time_Unit_100ms div $XcpMainFuncPeriod))"!][!/*
  */!][!ELSEIF "$time_unit = 'TIMESTAMP_UNIT_1S'"!][!/*
  */!]    [!"num:i(round(XcpEventChannelTimeCycle * $Event_Channel_Time_Unit_1s div $XcpMainFuncPeriod))"!][!/*
  */!][!ELSEIF "$time_unit = 'TIMESTAMP_UNIT_1PS'"!][!/*
  */!]    [!"num:i(round(XcpEventChannelTimeCycle * $Event_Channel_Time_Unit_1ps div $XcpMainFuncPeriod))"!][!/*
  */!][!ELSEIF "$time_unit = 'TIMESTAMP_UNIT_10PS'"!][!/*
  */!]    [!"num:i(round(XcpEventChannelTimeCycle * $Event_Channel_Time_Unit_10ps div $XcpMainFuncPeriod))"!][!/*
  */!][!ELSEIF "$time_unit = 'TIMESTAMP_UNIT_100PS'"!][!/*
  */!]    [!"num:i(round(XcpEventChannelTimeCycle * $Event_Channel_Time_Unit_100ps div $XcpMainFuncPeriod))"!][!/*
  */!][!ELSE!][!/*
  */!]    [!ERROR!]The XcpEventChannelTimeUnit has an out of range value.[!ENDERROR!][!/*
*/!][!ENDIF!][!/*
*/!][!ENDMACRO!]


#define XCP_START_SEC_CONST_UNSPECIFIED
#include <MemMap.h>

[!VAR "Counter" = "0"!]
/** \brief Configured event properties used to restore after a session is started */
CONST(Xcp_EventInfoType, XCP_CONST) Xcp_EventInfo[XCP_MAX_EVENT_CHANNEL] =
{
[!LOOP "node:order(XcpEventChannel/*, 'XcpEventChannelNumber')"!]
  {
    /* The event channel cycle counter maximum value */
    [!IF "XcpEventChannelTimeCycle > 0"!]
    [!CALL "EVENT_CHANNEL_TIME_CYCLE_COUNTER_MAX", "time_unit" = "XcpEventChannelTimeUnit"!]U,
    [!ELSE!][!/*
    */!]    0U,
    [!ENDIF!]
    /* The name of the event */
    Xcp_EventName_[!"XcpEventChannelNumber"!],
  [!IF "count(XcpEventChannelTriggeredDaqListRef/*) > 0"!]
    /* Pointer to the list of DAQ IDs of this event */
    &Xcp_DaqIdListConst[[!"num:i($Counter)"!]],
    /* The number of DAQ Lists of this event */
    [!"num:i(count(XcpEventChannelTriggeredDaqListRef/*))"!],
  [!ELSE!]
    /* Pointer to the list of DAQ IDs of this event */
    NULL_PTR,
    /* The number of DAQ Lists of this event */
    0U,
  [!ENDIF!]
    /* The length of the event name */
    [!"num:i(string-length(name(.)))"!]U,
    /* Consistency and STIM/DAQ, both direction combine configured for this event */
    XCP_EVENT_CHANNEL_DIRECTION_[!"XcpEventChannelType"!] | XCP_EVENT_CHANNEL_CONSISTENCY_[!"XcpEventChannelConsistency"!],
    /* Maximum number of DAQ supported for this event */
    [!"XcpEventChannelMaxDaqList"!]U,
    /* The priority of this event */
    [!"XcpEventChannelPriority"!]U,
    /* The sampling period of this event */
    [!"XcpEventChannelTimeCycle"!]U,
    [!IF "XcpEventChannelTimeCycle > 0"!]
    /* The unit of the event channel time cycle */
    XCP_[!"XcpEventChannelTimeUnit"!]_MASK,
    [!ELSE!][!/*
    */!]    /* The unit of the event channel time cycle */
    XCP_TIME_UNIT_DONT_CARE_MASK, /* DON'T CARE value because XcpEventChannelTimeCycle = 0 */
    [!ENDIF!]
  },
[!VAR "Counter" = "$Counter + count(XcpEventChannelTriggeredDaqListRef/*)"!]
[!ENDLOOP!]
};

#define XCP_STOP_SEC_CONST_UNSPECIFIED
#include <MemMap.h>

[!ENDIF!]

[!IF "$XcpMaxDaq > 0 and $XcpMaxDaq > $XcpDaqCount"!][!/* We have static or predefined DAQ lists */!]
#define XCP_START_SEC_CONST_UNSPECIFIED
#include <MemMap.h>

[!VAR "CounterODT" = "0"!]
[!VAR "XcpDaqListName" = "0"!]
[!VAR "CounterODTEntry" = "0"!]


/** \brief All Non-dynamic DAQ lists together with ODTs and ODT entries
 ** This information is used to restore all non-dynamic DAQ lists with default values
 ** for initialization/recovery purposes.
 */
CONST(Xcp_NonDynamicDaqListsType, XCP_CONST) Xcp_DaqListsDefault =
{
  /* ------------- Xcp_Daq list configuration section: ---------------------------------------- */
  {
  [!LOOP "node:order(XcpDaqList/*, 'XcpDaqListNumber')"!]
    [!IF "XcpDaqListNumber < $XcpMinDaq"!]
      [!IF "XcpDaqListNumber = 0"!]
    /* ---- DAQ List property structure for all predefined DAQ Lists. ---- */
      [!ENDIF!]
    [!ELSE!]
      [!IF "XcpDaqListNumber = $XcpMinDaq"!]
    /* ---- DAQ List property structure for all Static DAQ Lists. -------- */
      [!ENDIF!]
    [!ENDIF!]
    /* DaqList configuration for the DaqListId = [!"num:i(XcpDaqListNumber)"!] */
    {
      /* Pointer to the ODT Array or DAQ List */
      &Xcp_DaqLists.Xcp_NonDynamicDaqLists.Xcp_Odt[[!"num:i($CounterODT)"!]],
      /* Event channel number for this DAQ */
      [!VAR "XcpDaqListName" = "name(.)"!][!//
      [!VAR "XcpEventPresent" = "0"!]
      [!VAR "XcpDirectionSet" = "0"!]
      [!LOOP "../../XcpEventChannel/*/XcpEventChannelTriggeredDaqListRef/*[name(as:ref(.)) = $XcpDaqListName]"!]
      [!"../../XcpEventChannelNumber"!]U,
      [!VAR "XcpEventPresent" = "1"!]
      [!ENDLOOP!]
      [!IF "$XcpEventPresent = 0"!]
      XCP_INVALID_EVENT_CHANNEL,
      [!ENDIF!]
      /* Current  mode information of the DAQ List */
      [!IF "node:exists(XcpDaqListType) and (XcpDaqListType = 'DAQ' or XcpDaqListType = 'STIM')"!]
      XCP_MASK_DAQLIST_DIRECTION_[!"XcpDaqListType"!],
      [!VAR "XcpDirectionSet" = "1"!]
      [!ELSE!]
      0U,
      [!ENDIF!]
      /* Priority of this DAQ */
      [!IF "(node:exists(./XcpDaqListPriority) = 'true')"!]
      [!"./XcpDaqListPriority "!]U,
      [!ELSE!]
      0U,
      [!ENDIF!]
      [!IF "$IdentificationFieldType = 'ABSOLUTE'"!]
      /* The first PID only, when using absolute ODT number */
      [!"num:i($CounterODT)"!]U,
      [!ENDIF!]
      /* Number of ODTs in this DAQ list*/
      [!IF "XcpDaqListNumber <  $XcpMinDaq"!]
      [!"num:i(count(./XcpOdt/*))"!]U,
      [!ELSE!]
      [!"./XcpMaxOdt"!]U,
      [!ENDIF!]
      /* Maximum number of ODT entries in an ODT for this DAQ list */
      [!IF "XcpDaqListNumber <  $XcpMinDaq"!]
      [!VAR "ODTEntryCntMax" = "0"!]
      [!VAR "ODTEntryCnt" = "0"!]
      [!LOOP "./XcpOdt/*"!]
        [!VAR "ODTEntryCnt" = "count(./XcpOdtEntry/*)"!]
        [!IF "$ODTEntryCntMax < $ODTEntryCnt"!]
          [!VAR "ODTEntryCntMax" = "$ODTEntryCnt"!]
        [!ENDIF!]
      [!ENDLOOP!]
      [!"num:i($ODTEntryCntMax)"!]U,
      [!ELSE!]
      [!"./XcpMaxOdtEntries"!]U,
      [!ENDIF!]
      /* Preset value of the prescaler */
      [!IF "(node:exists(./XcpDaqListPrescaler) = 'true')"!]
      [!"./XcpDaqListPrescaler "!]U,
      [!ELSE!]
      1U,
      [!ENDIF!]
      /* Current value of the prescaler */
      0U,
      /* Configuration properties of the DAQ List */
      [!IF "(XcpDaqListNumber < $XcpMinDaq)"!]
      XCP_DAQ_LIST_DIRECTION_[!"XcpDaqListType"!][!IF "text:tolower(./XcpDaqEventFixed) = 'true'"!] | XCP_MASK_DAQLIST_EVENT_FIXED[!ENDIF!] [!IF "XcpDaqListNumber < $XcpMinDaq"!] | XCP_DAQ_LIST_PREDEFINED[!ENDIF!],
      [!ELSE!]
        [!IF "($XcpStimSupported = 'true')"!]
      XCP_DAQ_LIST_DIRECTION_DAQ_STIM,
        [!ELSE!]
      XCP_DAQ_LIST_DIRECTION_DAQ,
        [!ENDIF!]
      [!ENDIF!]
      /* Additional configuration of the DAQ List */
      [!IF "(XcpDaqListNumber < $XcpMinDaq) and ($XcpEventPresent = 1) and ($XcpDirectionSet = 1)"!]
      XCP_MASK_DAQLIST_READ_READY | XCP_MASK_DAQLIST_CONFIGURED,
      [!ELSE!]
      0U,
      [!ENDIF!]
    },
  [!IF "XcpDaqListNumber <  $XcpMinDaq"!]
    [!VAR "CounterODT" = "$CounterODT + count(XcpOdt/*)"!]
  [!ELSE!]
    [!VAR "CounterODT" = "$CounterODT + num:integer(XcpMaxOdt)"!]
  [!ENDIF!]
  [!ENDLOOP!]
  },
  /* --------------------- Xcp_Odt configuration section: ------------------------------------- */
  {
  [!LOOP "node:order(XcpDaqList/*, 'XcpDaqListNumber')"!]
    [!IF "(XcpDaqListNumber < $XcpMinDaq)"!]
      [!IF "XcpDaqListNumber = 0"!]
    /* ----- ODT configuration section for predefined ODTs ------- */
      [!ENDIF!]
    /* --- ODT configuration list for the DaqListId = [!"num:i(XcpDaqListNumber)"!] --- */
      [!LOOP "node:order(XcpOdt/*, 'XcpOdtNumber')"!]
    /* ODT configuration for the OdtID = [!"num:i(XcpOdtNumber)"!][!IF "($XcpTimestampIsEnabled = 'true') and (XcpOdtNumber = 0)"!] (the resulting DTO for this ODT contains [!"num:i($XcpTimestampSize)"!] byte[!IF "$XcpTimestampSize > 1"!]s[!ENDIF!][!CR!]
     *                                      with the time stamp data) [!ENDIF!]*/
    {
      /* Pointer to OdtEntry array or Object Descriptor table */
      &(Xcp_DaqLists.Xcp_NonDynamicDaqLists.Xcp_OdtEntry)[[!"num:i($CounterODTEntry)"!]],
      /* Number of elements in the ODT */
      [!"num:i(count(XcpOdtEntry/*))"!]U
    },
      [!VAR "CounterODTEntry" = "$CounterODTEntry + count(XcpOdtEntry/*)"!]
      [!ENDLOOP!]
    [!ELSE!]
      [!IF "XcpDaqListNumber = $XcpMinDaq"!]
    /* ----- ODT configuration section for static ODTs ---------- */
      [!ENDIF!]
    /* --- ODT configuration list for the DaqListId = [!"num:i(XcpDaqListNumber)"!] --- */
      [!FOR "x" = "1" TO "num:i(XcpMaxOdt)"!]
    /* ODT configuration for the OdtID = [!"num:i($x - 1)"!][!IF "($XcpTimestampIsEnabled = 'true') and ($x = 1)"!] (the resulting DTO for this ODT contains [!"num:i($XcpTimestampSize)"!] byte[!IF "$XcpTimestampSize > 1"!]s[!ENDIF!][!CR!]
     *                                      with the time stamp data) [!ENDIF!]*/
       [!IF "(XcpMaxOdtEntries > $XcpAbsoluteMaxOdtEntrySize) and ($XcpTimestampIsEnabled = 'true') and ($x = 1)"!]
         [!VAR "XcpRelativeOdtEntrySize" = "$XcpAbsoluteMaxOdtEntrySize"!]
       [!ELSE!]
         [!VAR "XcpRelativeOdtEntrySize" = "XcpMaxOdtEntries"!]
       [!ENDIF!]
    {
      /* Pointer to OdtEntry array or Object Descriptor table */
      &(Xcp_DaqLists.Xcp_NonDynamicDaqLists.Xcp_OdtEntry)[[!"num:i($CounterODTEntry)"!]],
      /* Number of elements in the ODT */
      [!"$XcpRelativeOdtEntrySize"!]U
    },
      [!VAR "CounterODTEntry" = "$CounterODTEntry + $XcpRelativeOdtEntrySize"!]
      [!ENDFOR!]
    [!ENDIF!]
  [!ENDLOOP!]
  },
  /* ------------------ Xcp_OdtEntry configuration section: ----------------------------------- */
  {
  [!LOOP "node:order(XcpDaqList/*, 'XcpDaqListNumber')"!]
    [!VAR "SelectedDaqListId" = "num:i(XcpDaqListNumber)"!]
    [!IF "(XcpDaqListNumber < $XcpMinDaq)"!]
      [!IF "XcpDaqListNumber = 0"!]
    /* ------ ODT Entries for all predefined DAQ lists ------- */
      [!ENDIF!]
      [!LOOP "node:order(XcpOdt/*, 'XcpOdtNumber')"!]
        [!VAR "SelectedOdtId" = "num:i(XcpOdtNumber)"!]
    /* -- ODT Entries List for DaqListId = [!"$SelectedDaqListId"!] and OdtId = [!"num:i(XcpOdtNumber)"!] -- */
        [!LOOP "node:order(XcpOdtEntry/*, 'XcpOdtEntryNumber')"!]
    /* ODT Entry configuration for the OdtEntryId = [!"num:i(XcpOdtEntryNumber)"!] */
    {
      /* The address location of the ODT entry */
      (P2VAR(uint8, XCP_CONST, XCP_APPL_DATA))[!"XcpOdtEntryAddress"!],
      /* Size of the element in AG */
      [!"XcpOdtEntryLength"!]U,
      /* Position of the Bit to be considered */
      0U,
      /* Address extension of the ODT */
      0U,
    },
        [!ENDLOOP!]
      [!ENDLOOP!]
    [!ELSE!]
      [!IF "XcpDaqListNumber = $XcpMinDaq"!]
    /* -- ODT Entries for all static configurable DAQ lists -- */
      [!ENDIF!]
      [!FOR "x" = "1" TO "num:i(XcpMaxOdt)"!]
        [!IF "(XcpMaxOdtEntries > $XcpAbsoluteMaxOdtEntrySize) and ($XcpTimestampIsEnabled = 'true') and ($x = 1)"!]
          [!VAR "XcpRelativeOdtEntrySize" = "$XcpAbsoluteMaxOdtEntrySize"!]
        [!ELSE!]
          [!VAR "XcpRelativeOdtEntrySize" = "XcpMaxOdtEntries"!]
        [!ENDIF!]
    /* -- ODT Entries List for DaqListId = [!"num:i(XcpDaqListNumber)"!] and OdtId = [!"num:i($x - 1)"!] -- */
        [!FOR "y" = "1" TO "$XcpRelativeOdtEntrySize"!]
    /* ODT Entry configuration for the OdtEntryId = [!"num:i($y - 1)"!] */
    {
      /* The address location of the ODT entry */
      NULL_PTR,
      /* Size of the element in AG */
      0U,
      /* Position of the Bit to be considered */
      0xFFU,
      /* Address extension of the ODT */
      0U,
    },
        [!ENDFOR!]
      [!ENDFOR!]
    [!ENDIF!]
  [!ENDLOOP!]
  },
};

#define XCP_STOP_SEC_CONST_UNSPECIFIED
#include <MemMap.h>
[!ENDIF!] [!/* We have static or predefined DAQ lists */!]

/*==================[internal data]=========================================*/


[!IF "($XcpMaxEventChannel > 0) and (num:i(sum(XcpEventChannel/*/XcpEventChannelMaxDaqList)) > 0)"!]

#define XCP_START_SEC_VAR_16BIT
#include <MemMap.h>

/** \brief List of DAQ IDs in the order of events */
STATIC VAR(Xcp_DaqIdType, XCP_VAR) Xcp_DaqIdList[XCP_EVENT_CHANNEL_TOTAL_DAQ] =
{
[!LOOP "node:order(XcpEventChannel/*, 'XcpEventChannelNumber')"!]
  [!IF "XcpEventChannelMaxDaqList > 0"!]
    [!LOOP "XcpEventChannelTriggeredDaqListRef/*"!]
  [!"as:ref(.)/XcpDaqListNumber"!]U,
    [!ENDLOOP!]
    [!IF "count(./XcpEventChannelTriggeredDaqListRef/*) < XcpEventChannelMaxDaqList"!]
      [!FOR "x" = "1" TO "num:i(XcpEventChannelMaxDaqList - count(./XcpEventChannelTriggeredDaqListRef/*))"!]
  XCP_DUMMY_DAQ_LIST,
      [!ENDFOR!]
   [!ENDIF!]
  [!ENDIF!]
[!ENDLOOP!]
};

#define XCP_STOP_SEC_VAR_16BIT
#include <MemMap.h>
[!ENDIF!]

/*==================[external data]=========================================*/

[!IF "$XcpMaxDaq > 0"!]

#define XCP_START_SEC_VAR_SAVED_ZONE_UNSPECIFIED
#include <MemMap.h>

/** \brief All runtime DAQ lists together with ODTs and ODT entries and admin data*/
VAR(Xcp_AllDaqListsType, XCP_VAR_NOINIT) Xcp_DaqLists;

#define XCP_STOP_SEC_VAR_SAVED_ZONE_UNSPECIFIED
#include <MemMap.h>

#define XCP_START_SEC_VAR_UNSPECIFIED
#include <MemMap.h>

[!IF "$XcpMaxEventChannel > 0"!]
[!VAR "Counter" = "0"!]
/** \brief Event */
VAR(Xcp_EventType, XCP_VAR) Xcp_Event[XCP_MAX_EVENT_CHANNEL] =
{
[!LOOP "node:order(XcpEventChannel/*, 'XcpEventChannelNumber')"!]
  {
    /* Time Cycle Counter */
    1U,
  [!IF "XcpEventChannelMaxDaqList > 0"!]
    /* Pointer to the list of DAQ IDs of this event */
    &Xcp_DaqIdList[[!"num:i($Counter)"!]],
  [!ELSE!]
    /* Pointer to the list of DAQ IDs of this event */
    NULL_PTR,
  [!ENDIF!]
    /* Next DAQ index to be sampled */
    0U,
    /* Next ODT to be sampled */
    0U,
  [!IF "XcpEventChannelMaxDaqList > 0"!]
    /* The number of DAQ Lists of this event */
    [!"num:i(count(./XcpEventChannelTriggeredDaqListRef/*))"!],
  [!ELSE!]
     /* The number of DAQ Lists of this event */
     0U,
  [!ENDIF!]
    /* Flag signaling whether this event was set (cyclically or via Xcp_SetEvent()) */
    FALSE,
  },
[!VAR "Counter" = "$Counter + XcpEventChannelMaxDaqList"!]
[!ENDLOOP!]
};
[!ENDIF!]

#define XCP_STOP_SEC_VAR_UNSPECIFIED
#include <MemMap.h>
[!ENDIF!]

/*==================[external function definitions]=========================*/

/*==================[internal function definitions]=========================*/
[!ENDSELECT!][!//
/*==================[end of file]===========================================*/
