/**
 * \file
 *
 * \brief AUTOSAR PbcfgM
 *
 * This file contains the implementation of the AUTOSAR
 * module PbcfgM.
 *
 * \author Elektrobit Automotive GmbH, 91058 Erlangen, Germany
 *
 * Copyright 2005 - 2013 Elektrobit Automotive GmbH
 * All rights exclusively reserved for Elektrobit Automotive GmbH,
 * unless expressly agreed to otherwise.
 */
#if (!defined PBCFGM_TRACE_H)
#define PBCFGM_TRACE_H
/*==================[inclusions]============================================*/

[!IF "node:exists(as:modconf('Dbg'))"!]
#include <Dbg.h>
[!ENDIF!]

/*==================[macros]================================================*/

#ifndef DBG_PBCFGM_INIT_ENTRY
/** \brief Entry point of function PbcfgM_Init() */
#define DBG_PBCFGM_INIT_ENTRY(a)
#endif

#ifndef DBG_PBCFGM_INITSTATUS
/** \brief Change of PbcfgM_InitStatus */
#define DBG_PBCFGM_INITSTATUS(a,b)
#endif

#ifndef DBG_PBCFGM_INIT_EXIT
/** \brief Exit point of function PbcfgM_Init() */
#define DBG_PBCFGM_INIT_EXIT(a)
#endif

#ifndef DBG_PBCFGM_GETCONFIG_ENTRY
/** \brief Entry point of function PbcfgM_GetConfig() */
#define DBG_PBCFGM_GETCONFIG_ENTRY(a,b,c)
#endif

#ifndef DBG_PBCFGM_GETCONFIG_EXIT
/** \brief Exit point of function PbcfgM_GetConfig() */
#define DBG_PBCFGM_GETCONFIG_EXIT(a,b,c,d)
#endif

#ifndef DBG_PBCFGM_ISVALIDCONFIG_ENTRY
/** \brief Entry point of function PbcfgM_IsValidConfig() */
#define DBG_PBCFGM_ISVALIDCONFIG_ENTRY(a)
#endif

#ifndef DBG_PBCFGM_ISVALIDCONFIG_EXIT
/** \brief Exit point of function PbcfgM_IsValidConfig() */
#define DBG_PBCFGM_ISVALIDCONFIG_EXIT(a,b)
#endif

#ifndef DBG_PBCFGM_ARETHEMODULECONFIGSVALID_ENTRY
/** \brief Entry point of function PbcfgM_AreTheModuleConfigsValid() */
#define DBG_PBCFGM_ARETHEMODULECONFIGSVALID_ENTRY(a)
#endif

#ifndef DBG_PBCFGM_ARETHEMODULECONFIGSVALID_EXIT
/** \brief Exit point of function PbcfgM_AreTheModuleConfigsValid() */
#define DBG_PBCFGM_ARETHEMODULECONFIGSVALID_EXIT(a,b)
#endif

/*==================[type definitions]======================================*/

/*==================[external function declarations]========================*/

/*==================[external constants]====================================*/

/*==================[external data]=========================================*/

#endif /* (!defined PBCFGM_TRACE_H) */
/*==================[end of file]===========================================*/
