#if !defined( PWM_H )
#define PWM_H

#include <Pwm_17_Gtm.h> 

#define PWM_AR_RELEASE_MAJOR_VERSION PWM_17_GTM_AR_RELEASE_MAJOR_VERSION
#define PWM_AR_RELEASE_MINOR_VERSION PWM_17_GTM_AR_RELEASE_MINOR_VERSION

/* Global Type Definitions */
#if (PWM_NOTIFICATION_SUPPORTED == STD_ON)
typedef Pwm_17_Gtm_EdgeNotificationType   Pwm_EdgeNotificationType;
#endif /* PWM_NOTIFICATION_SUPPORTED */
typedef Pwm_17_Gtm_ChannelType            Pwm_ChannelType;
typedef Pwm_17_Gtm_PeriodType             Pwm_PeriodType;
typedef Pwm_17_Gtm_OutputStateType        Pwm_OutputStateType;
typedef Pwm_17_Gtm_ChannelClassType       Pwm_ChannelClassType;
typedef Pwm_17_Gtm_ConfigType             Pwm_ConfigType;


/* Global Function Declarations */
#define Pwm_Init                 Pwm_17_Gtm_Init
#define Pwm_DeInit               Pwm_17_Gtm_DeInit
#define Pwm_SetDutyCycle         Pwm_17_Gtm_SetDutyCycle
#define Pwm_SetPeriodAndDuty     Pwm_17_Gtm_SetPeriodAndDuty
#define Pwm_SetOutputToIdle      Pwm_17_Gtm_SetOutputToIdle
#define Pwm_GetOutputState       Pwm_17_Gtm_GetOutputState
#define Pwm_DisableNotification  Pwm_17_Gtm_DisableNotification
#define Pwm_EnableNotification   Pwm_17_Gtm_EnableNotification
#define Pwm_GetVersionInfo       Pwm_17_Gtm_GetVersionInfo

#endif  