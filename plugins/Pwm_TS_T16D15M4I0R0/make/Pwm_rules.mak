# \file
#
# \brief AUTOSAR Pwm
#
# This file contains the implementation of the AUTOSAR
# module Pwm.
#
# \author Elektrobit Automotive GmbH, 91058 Erlangen, Germany
#
# Copyright 2005 - 2013 Elektrobit Automotive GmbH
# All rights exclusively reserved for Elektrobit Automotive GmbH,
# unless expressly agreed to otherwise.

#################################################################
# REGISTRY

LIBRARIES_TO_BUILD     += Pwm_src

Pwm_src_FILES       += $(Pwm_CORE_PATH)\src\Pwm_17_Gtm.c
Pwm_src_FILES       += $(Pwm_CORE_PATH)\src\Pwm_17_Gtm_Ver.c
Pwm_src_FILES       += $(Pwm_OUTPUT_PATH)\src\Pwm_17_Gtm_PBCfg.c

ifeq ($(TS_BUILD_POST_BUILD_BINARY),TRUE)
# If the post build binary shall be built do not compile any files other then the postbuild files.
Pwm_src_FILES :=
endif
