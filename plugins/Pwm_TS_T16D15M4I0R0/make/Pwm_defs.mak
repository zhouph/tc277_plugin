# \file
#
# \brief AUTOSAR Pwm
#
# This file contains the implementation of the AUTOSAR
# module Pwm.
#
# \author Elektrobit Automotive GmbH, 91058 Erlangen, Germany
#
# Copyright 2005 - 2013 Elektrobit Automotive GmbH
# All rights exclusively reserved for Elektrobit Automotive GmbH,
# unless expressly agreed to otherwise.

#################################################################
# DEFINITIONS

Pwm_CORE_PATH      := $(SSC_ROOT)\Pwm_$(Pwm_VARIANT)
Pwm_OUTPUT_PATH    := $(AUTOSAR_BASE_OUTPUT_PATH)

Pwm_GEN_FILES      += $(Pwm_OUTPUT_PATH)\include\Pwm_17_Gtm_Cfg.h
Pwm_GEN_FILES      += $(Pwm_OUTPUT_PATH)\src\Pwm_17_Gtm_PBCfg.c

TRESOS_GEN_FILES   += $(Pwm_GEN_FILES)

CC_INCLUDE_PATH    += $(Pwm_CORE_PATH)\include
CC_INCLUDE_PATH    += $(Pwm_OUTPUT_PATH)\include

