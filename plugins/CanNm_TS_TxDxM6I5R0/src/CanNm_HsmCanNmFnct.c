/**
 * \file
 *
 * \brief AUTOSAR CanNm
 *
 * This file contains the implementation of the AUTOSAR
 * module CanNm.
 *
 * \author Elektrobit Automotive GmbH, 91058 Erlangen, Germany
 *
 * Copyright 2005 - 2013 Elektrobit Automotive GmbH
 * All rights exclusively reserved for Elektrobit Automotive GmbH,
 * unless expressly agreed to otherwise.
 */

/* Implementation of the state handler functions of the state machine
 * CanNm.
 *
 * This file contains the implementation of the state functions.  It
 * is generated but must be edited to implement the real actions.  If the
 * state machine model is updated and the code generator regenerates the
 * files the user must manually merge the changes with any existing hand
 * written code.
 */

/*  MISRA-C:2004 Deviation List
 *
 *  MISRA-1) Deviated Rule: 19.10 (required)
 *    "Parameter instance shall be enclosed in parentheses."
 *
 *     Reason:
 *     It is used in function parameter declarations and definitions
 *     or as structure member.
 */

/* CHECK: RULE 301 OFF (this file is partly generated, some lines may be
 * longer then 100 characters) */

/*==================[inclusions]============================================*/

#include <CanNm_Trace.h>
#include <Std_Types.h>                  /* AUTOSAR standard types */
#include <TSMem.h>
#include <CanNm_Hsm.h>                  /* state machine driver interface */
#include <CanNm_HsmCanNm.h>             /* public statechart model definitions */
#include <CanNm_HsmCanNmInt.h>          /* internal statechart model definitions */
#include <CanNm_Int.h>                  /* Module intenal definitions */

/* !LINKSTO CANNM312,1 */
#include <CanIf.h>                      /* CanIf API, CanIf_Transmit() */
/* !LINKSTO CANNM307,1 */
#include <Nm_Cbk.h>                     /* Declaration of Nm callback APIs */
#include <SchM_CanNm.h>                 /* SchM-header for CanNm */

#if (CANNM_DEV_ERROR_DETECT == STD_ON)
/* !LINKSTO CANNM308,1 */
#include <Det.h>                        /* Det API */
#endif

#if (CANNM_PN_ENABLED == STD_ON)
/* CANNM446 */
#include <CanSM_TxTimeoutException.h>   /* Callback function CanSM_TxTimeoutException() */
#endif

/*==================[macros]================================================*/

#if (CANNM_HSM_INST_MULTI_ENABLED == STD_ON)
/* Deviation MISRA-1 */
#define CANNM_PL_TIMER(a, b)          a, b
#else
/* Deviation MISRA-1 */
#define CANNM_PL_TIMER(a, b)          b
#endif

/* CANNM073 */
/** \brief Start Tx Timeout Timer.
 ** \param instIdx index of state machine instance to work on */
#define CANNM_TX_TIMER_START(instIdx,TimeoutTime)       \
  do                                                    \
  {                                                     \
    CANNM_CHANNEL_STATUS(instIdx).TimeoutTimer          \
      = (TimeoutTime);   \
  } while (0)

/** \brief Stop Tx Timeout Timer.
 ** \param instIdx index of state machine instance to work on */
#define CANNM_TX_TIMER_STOP(instIdx)                    \
  do                                                    \
  {                                                     \
    CANNM_CHANNEL_STATUS(instIdx).TimeoutTimer = 0U;    \
  } while (0)


/*==================[type definitions]======================================*/

/*==================[external function declarations]========================*/

/*==================[internal function declarations]========================*/
#define CANNM_START_SEC_CODE
#include <MemMap.h>

/** \brief Initialize Internal variables of respective channel.
 ** \param instIdx index of state machine instance to work on */
STATIC FUNC(void, CANNM_CODE) CanNm_InitIntVar(
  CANNM_PDL_SF(const uint8 instIdx));

/** \brief Start Nm Timeout Timer.
 ** \param instIdx index of state machine instance to work on */
STATIC FUNC(void, CANNM_CODE) CanNm_NmTimerStart(
  CANNM_PL_TIMER(const uint8 instIdx, CanNm_TimeType CanNmTime));

/** \brief Stop Nm Timeout Timer.
 ** \param instIdx index of state machine instance to work on */
STATIC FUNC(void, CANNM_CODE) CanNm_NmTimerStop(
  CANNM_PDL_SF(const uint8 instIdx));

/** \brief Start Universal Timer.
 ** \param instIdx index of state machine instance to work on
 ** \param UniTimer Universal Timer value */
STATIC FUNC(void, CANNM_CODE) CanNm_UniTimerStart(
  CANNM_PL_TIMER(const uint8 instIdx, CanNm_TimeType UniTimer));

/** \brief Stop Universal Timer.
 ** \param instIdx index of state machine instance to work on */
STATIC FUNC(void, CANNM_CODE) CanNm_UniTimerStop(
  CANNM_PDL_SF(const uint8 instIdx));

STATIC FUNC(void, CANNM_CODE) CanNm_RmsTimerStart(
  CANNM_PL_TIMER(const uint8 instIdx, CanNm_TimeType RmsTime));


#if (CANNM_PASSIVE_MODE_ENABLED == STD_OFF)
/** \brief Start CanNm Message Cycle Timer.
 ** \param instIdx index of state machine instance to work on
 ** \param MsgCycleTimer CanNm Message Cycle Timer value */
STATIC FUNC(void, CANNM_CODE) CanNm_MsgCycleTimerStart(
  CANNM_PL_TIMER(const uint8 instIdx, CanNm_TimeType MsgCycleTimer));

/** \brief Stop CanNm Message Cycle Timer.
 ** \param instIdx index of state machine instance to work on */
STATIC FUNC(void, CANNM_CODE) CanNm_MsgCycleTimerStop(
  CANNM_PDL_SF(const uint8 instIdx));
#endif

/** \brief Handle the common actions on event Rx Indication in
 **        all sub-states in Network Mode
 ** \param instIdx index of state machine instance to work on */
STATIC FUNC(void, CANNM_CODE) CanNm_HandleRxIndicationCommon(
  CANNM_PDL_SF(const uint8 instIdx));

/** \brief Handle the action on event Rx Indication in
 **        Normal Operation Remote Active state
 ** \param instIdx index of state machine instance to work on */
STATIC FUNC(void, CANNM_CODE) CanNm_HandleRxIndication_NOState_RA(
  CANNM_PDL_SF(const uint8 instIdx));

/** \brief Handle the action on event Rx Indication in
 **        Normal Operation Remote Sleep state
 ** \param instIdx index of state machine instance to work on */
STATIC FUNC(void, CANNM_CODE) CanNm_HandleRxIndication_NOState(
  CANNM_PDL_SF(const uint8 instIdx));

#if ((CANNM_PASSIVE_MODE_ENABLED == STD_OFF) && (CANNM_IMMEDIATE_TXCONF_ENABLED == STD_OFF))
/** \brief Handle the action on event Tx Confirmation
 ** \param instIdx index of state machine instance to work on */
STATIC FUNC(void, CANNM_CODE) CanNm_HandleTxConfirmation(
  CANNM_PDL_SF(const uint8 instIdx));
#endif

#if ((CANNM_COM_CONTROL_ENABLED == STD_ON) && (CANNM_PASSIVE_MODE_ENABLED == STD_OFF))
  /** \brief Handle the action on event communication ability.
 ** \param instIdx index of state machine instance to work on */
STATIC FUNC(void, CANNM_CODE) CanNm_HandleComControl(
 CANNM_PDL_SF(const uint8 instIdx));
#endif

/** \brief Transmit Nm message.
 ** \param instIdx index of state machine instance to work on */
#if (CANNM_PASSIVE_MODE_ENABLED == STD_OFF)
FUNC(void, CANNM_CODE) CanNm_HandleTransmit(
  CANNM_PDL_SF(const uint8 instIdx));
#endif
#define CANNM_STOP_SEC_CODE
#include <MemMap.h>

/*==================[external constants]====================================*/

/*==================[internal constants]====================================*/

/*==================[external data]=========================================*/

/*==================[internal data]=========================================*/

/*==================[external function definitions]=========================*/

#define CANNM_START_SEC_CODE
#include <MemMap.h>

/* ************************ state functions ******************************* */

/* ************************************************************************
 * State: TOP
 * Parent state: none
 * Init substate: BusSleepMode
 * Transitions originating from this state:
 */

FUNC(void, CANNM_CODE) CanNm_HsmCanNmSfTOPEntry(
  CANNM_PDL_SF(const uint8 instIdx))
{
#if (CANNM_HSM_INST_MULTI_ENABLED == STD_ON)
  DBG_CANNM_HSMCANNMSFTOPENTRY_ENTRY(CANNM_INST(instIdx));
#else
  DBG_CANNM_HSMCANNMSFTOPENTRY_ENTRY();
#endif
  /* entry action 'InitIntVar();TxTimerStop(); NmTimerStop(); UniTimerStop(); MsgCycleTimerStop();' */
  CanNm_InitIntVar(CANNM_PL_SF(instIdx));
#if ((CANNM_PASSIVE_MODE_ENABLED == STD_OFF)            \
  && (CANNM_IMMEDIATE_TXCONF_ENABLED == STD_OFF))
  CANNM_TX_TIMER_STOP(instIdx);
#endif
  CanNm_NmTimerStop(CANNM_PL_SF(instIdx));
  CanNm_UniTimerStop(CANNM_PL_SF(instIdx));
#if (CANNM_PASSIVE_MODE_ENABLED == STD_OFF)
  CanNm_MsgCycleTimerStop(CANNM_PL_SF(instIdx));
#endif
#if (CANNM_HSM_INST_MULTI_ENABLED == STD_ON)
  DBG_CANNM_HSMCANNMSFTOPENTRY_EXIT(CANNM_INST(instIdx));
#else
  DBG_CANNM_HSMCANNMSFTOPENTRY_EXIT();
#endif
}

/* ************************************************************************
 * State: BusSleepMode
 * Parent state: TOP
 * Init substate: none, this is a leaf state
 * Transitions originating from this state:
 * 1) RX_INDICATION[]/Nm_NetworkStartIndication(); #ifdef (...) Nm_PduRxIndication; #endif #if(Det == STD_ON);DET_REPORT_ERROR();#endif --> CANNM127 CANNM037 CANNM336 CANNM337
 * 2) BusSleepMode -> NetworkMode: NETWORK_START[]/
 * 3) BusSleepMode -> NetworkMode: NET_REQ_STATUS_CHANGED[NetworkRequested==TRUE]/
 */

FUNC(void, CANNM_CODE) CanNm_HsmCanNmSfBusSleepModeEntry(
  CANNM_PDL_SF(const uint8 instIdx))
{
#if (CANNM_HSM_INST_MULTI_ENABLED == STD_ON)
  DBG_CANNM_HSMCANNMSFBUSSLEEPMODEENTRY_ENTRY(CANNM_INST(instIdx));
#else
  DBG_CANNM_HSMCANNMSFBUSSLEEPMODEENTRY_ENTRY();
#endif
  /* entry action '#ifdef (SCI) Nm_StateChangeNotification(ActState, BusSleepState);CurState = BusSleepState; Nm_BusSleepMode(); --> CANNM126' */

#if (CANNM_STATE_CHANGE_IND_ENABLED == STD_ON)
  Nm_StateChangeNotification(
    CANNM_CHANNEL_CONFIG(instIdx).nmChannelId,
    CANNM_CHANNEL_STATUS(instIdx).CurState,
    NM_STATE_BUS_SLEEP);
#endif
  if (CANNM_CHANNEL_STATUS(instIdx).CurState == NM_STATE_PREPARE_BUS_SLEEP)
  {
    Nm_BusSleepMode(CANNM_CHANNEL_CONFIG(instIdx).nmChannelId); /* CANNM126 */
  }
  CANNM_CHANNEL_STATUS(instIdx).CurState = NM_STATE_BUS_SLEEP;
#if (CANNM_PASSIVE_MODE_ENABLED == STD_OFF)
  CANNM_CHANNEL_STATUS(instIdx).NmTimerExpired = FALSE;
  CANNM_CHANNEL_STATUS(instIdx).FirstCanIfTransmitOk = FALSE;
#endif  
#if (CANNM_HSM_INST_MULTI_ENABLED == STD_ON)
  DBG_CANNM_HSMCANNMSFBUSSLEEPMODEENTRY_EXIT(CANNM_INST(instIdx));
#else
  DBG_CANNM_HSMCANNMSFBUSSLEEPMODEENTRY_EXIT();
#endif
}
FUNC(void, CANNM_CODE) CanNm_HsmCanNmSfBusSleepModeAction1(
  CANNM_PDL_SF(const uint8 instIdx))
{
#if (CANNM_HSM_INST_MULTI_ENABLED == STD_ON)
  DBG_CANNM_HSMCANNMSFBUSSLEEPMODEACTION1_ENTRY(CANNM_INST(instIdx));
#else
  DBG_CANNM_HSMCANNMSFBUSSLEEPMODEACTION1_ENTRY();
#endif
  /* action 'Nm_NetworkStartIndication(); #ifdef (...) Nm_PduRxIndication; #endif #if(Det == STD_ON);DET_REPORT_ERROR();#endif --> CANNM127 CANNM037 CANNM336 CANNM337'
   * for RX_INDICATION[]/...
   * internal transition */

  /* CANNM127 */
  Nm_NetworkStartIndication(CANNM_CHANNEL_CONFIG(instIdx).nmChannelId);

#if (CANNM_PDU_RX_INDICATION_ENABLED == STD_ON)
  /* CANNM037 */
  Nm_PduRxIndication(CANNM_CHANNEL_CONFIG(instIdx).nmChannelId);
#endif
#if (CANNM_DEV_ERROR_DETECT == STD_ON)
  /* CANNM336 */
  (void)Det_ReportError
  (CANNM_MODULE_ID, CANNM_INSTANCE_ID,
   CANNM_SERVID_RXINDICATION, CANNM_E_NET_START_IND);
#endif
#if (CANNM_HSM_INST_MULTI_ENABLED == STD_ON)
  DBG_CANNM_HSMCANNMSFBUSSLEEPMODEACTION1_EXIT(CANNM_INST(instIdx));
#else
  DBG_CANNM_HSMCANNMSFBUSSLEEPMODEACTION1_EXIT();
#endif
}
FUNC(boolean, CANNM_CODE) CanNm_HsmCanNmSfBusSleepModeGuard3(
  CANNM_PDL_SF(const uint8 instIdx))
{
  boolean ret;
#if (CANNM_HSM_INST_MULTI_ENABLED == STD_ON)
  DBG_CANNM_HSMCANNMSFBUSSLEEPMODEGUARD3_ENTRY(CANNM_INST(instIdx));
#else
  DBG_CANNM_HSMCANNMSFBUSSLEEPMODEGUARD3_ENTRY();
#endif
  /* guard condition 'NetworkRequested==TRUE'
   * for NET_REQ_STATUS_CHANGED[...]/
   * external transition to state NetworkMode */
  ret = (boolean)(((CANNM_CHANNEL_STATUS(instIdx).ChanStatus &
                     CANNM_NETWORK_REQUESTED) != 0U) ? TRUE : FALSE);
#if (CANNM_HSM_INST_MULTI_ENABLED == STD_ON)
  DBG_CANNM_HSMCANNMSFBUSSLEEPMODEGUARD3_EXIT(ret, CANNM_INST(instIdx));
#else
  DBG_CANNM_HSMCANNMSFBUSSLEEPMODEGUARD3_EXIT(ret);
#endif
  return ret;
}

/* ************************************************************************
 * State: NetworkMode
 * Parent state: TOP
 * Init substate: SendingSubMode
 * Transitions originating from this state:
 * 1) COM_CONTROL[]/#if (CANNM_COM_CONTROL_ENABLED == STD_ON)
 *    HandleComControl()
 * 2) RX_INDICATION[]/HandleRxIndicationCommon()
 * 3) TX_CONFIRMATION[]/HandleTxConfirmation();
 */

FUNC(void, CANNM_CODE) CanNm_HsmCanNmSfNetworkModeEntry(
  CANNM_PDL_SF(const uint8 instIdx))
{
#if (CANNM_HSM_INST_MULTI_ENABLED == STD_ON)
  DBG_CANNM_HSMCANNMSFNETWORKMODEENTRY_ENTRY(CANNM_INST(instIdx));
#else
  DBG_CANNM_HSMCANNMSFNETWORKMODEENTRY_ENTRY();
#endif
  /* entry action '#if (!(PASSIVE_MODE == true)) && (ChanStatus & CANNM_NETWORK_REQUESTED) &&(IMM_TRAN_ENBLD)
   * {Chanstatus &= IMM_TRANS_ACTIVE} */
  /* entry action 'NmTimerStart(CANNM_TIMEOUT_TIME) --> CANNM096; Nm_NetworkMode(); -->CANNM97; if(ChanStatus & NETWORK_REQUESTED) SetActiveWakeupBit() -->CANNM401; */
  /* CANNM096 */
  CanNm_NmTimerStart(CANNM_PL_TIMER(instIdx, CANNM_CHANNEL_CONFIG(instIdx).CanNmTime));
  Nm_NetworkMode(CANNM_CHANNEL_CONFIG(instIdx).nmChannelId); /* CANNM97 */
#if (CANNM_ACTIVE_WAKEUP_BIT_ENABLED == STD_ON)
  if((CANNM_CHANNEL_STATUS(instIdx).ChanStatus & CANNM_NETWORK_REQUESTED) != 0U)
  {
    CANNM_CHANNEL_CONFIG(instIdx).TxPduPtr[CANNM_CHANNEL_CONFIG(instIdx).CbvPos] |=
      CANNM_CBV_ACTIVEWAKEUPBIT;
  }
#endif
#if ((CANNM_IMMEDIATE_TRANSMISSION == STD_ON) && (CANNM_PASSIVE_MODE_ENABLED == STD_OFF))
  if((0U != (CANNM_CHANNEL_STATUS(instIdx).ChanStatus & CANNM_NETWORK_REQUESTED))
    && (CANNM_CHANNEL_CONFIG(instIdx).ImmediateNmTransmissions > 0U))
  {
    /* Enter critical section to protect from concurrent access
     * to different bits in 'ChanStatus' in different APIs.
     */
    SchM_Enter_CanNm_SCHM_CANNM_EXCLUSIVE_AREA_0();
    CANNM_CHANNEL_STATUS(instIdx).ChanStatus |= CANNM_IMMEDIATE_TRANSMISSION_ACTIVE;
    SchM_Exit_CanNm_SCHM_CANNM_EXCLUSIVE_AREA_0();
  }
#elif (CANNM_HSM_INST_MULTI_ENABLED == STD_ON)
  TS_PARAM_UNUSED(instIdx);
#endif
#if (CANNM_HSM_INST_MULTI_ENABLED == STD_ON)
  DBG_CANNM_HSMCANNMSFNETWORKMODEENTRY_EXIT(CANNM_INST(instIdx));
#else
  DBG_CANNM_HSMCANNMSFNETWORKMODEENTRY_EXIT();
#endif
}
FUNC(void, CANNM_CODE) CanNm_HsmCanNmSfNetworkModeExit(
   CANNM_PDL_SF(const uint8 instIdx))
{
#if (CANNM_HSM_INST_MULTI_ENABLED == STD_ON)
  DBG_CANNM_HSMCANNMSFNETWORKMODEEXIT_ENTRY(CANNM_INST(instIdx));
#else
  DBG_CANNM_HSMCANNMSFNETWORKMODEEXIT_ENTRY();
#endif
  /* exit action 'ClearActiveWakeUpBit() -->CANNM402;' */
#if (CANNM_ACTIVE_WAKEUP_BIT_ENABLED == STD_ON)
  CANNM_CHANNEL_CONFIG(instIdx).TxPduPtr[CANNM_CHANNEL_CONFIG(instIdx).CbvPos] &=
    (uint8)(~CANNM_CBV_ACTIVEWAKEUPBIT);
#elif (CANNM_HSM_INST_MULTI_ENABLED == STD_ON)
  TS_PARAM_UNUSED(instIdx);
#endif
#if (CANNM_HSM_INST_MULTI_ENABLED == STD_ON)
  DBG_CANNM_HSMCANNMSFNETWORKMODEEXIT_EXIT(CANNM_INST(instIdx));
#else
  DBG_CANNM_HSMCANNMSFNETWORKMODEEXIT_EXIT();
#endif
}
FUNC(void, CANNM_CODE) CanNm_HsmCanNmSfNetworkModeAction1(
  CANNM_PDL_SF(const uint8 instIdx))
{
#if (CANNM_HSM_INST_MULTI_ENABLED == STD_ON)
  DBG_CANNM_HSMCANNMSFNETWORKMODEACTION1_ENTRY(CANNM_INST(instIdx));
#else
  DBG_CANNM_HSMCANNMSFNETWORKMODEACTION1_ENTRY();
#endif
  /* action '#if (CANNM_COM_CONTROL_ENABLED == STD_ON) HandleComControl()'
   * for COM_CONTROL[]/...
   * internal transition */
#if ((CANNM_COM_CONTROL_ENABLED == STD_ON) && (CANNM_PASSIVE_MODE_ENABLED == STD_OFF))
  CanNm_HandleComControl(CANNM_PL_SF(instIdx));
#elif (CANNM_HSM_INST_MULTI_ENABLED == STD_ON)
  TS_PARAM_UNUSED(instIdx);
#endif
#if (CANNM_HSM_INST_MULTI_ENABLED == STD_ON)
  DBG_CANNM_HSMCANNMSFNETWORKMODEACTION1_EXIT(CANNM_INST(instIdx));
#else
  DBG_CANNM_HSMCANNMSFNETWORKMODEACTION1_EXIT();
#endif
}
FUNC(void, CANNM_CODE) CanNm_HsmCanNmSfNetworkModeAction2(
  CANNM_PDL_SF(const uint8 instIdx))
{
#if (CANNM_HSM_INST_MULTI_ENABLED == STD_ON)
  DBG_CANNM_HSMCANNMSFNETWORKMODEACTION2_ENTRY(CANNM_INST(instIdx));
#else
  DBG_CANNM_HSMCANNMSFNETWORKMODEACTION2_ENTRY();
#endif
  /* action 'HandleRxIndicationCommon()'
   * for RX_INDICATION[]/...
   * internal transition */
  CanNm_HandleRxIndicationCommon(CANNM_PL_SF(instIdx));
#if (CANNM_HSM_INST_MULTI_ENABLED == STD_ON)
  DBG_CANNM_HSMCANNMSFNETWORKMODEACTION2_EXIT(CANNM_INST(instIdx));
#else
  DBG_CANNM_HSMCANNMSFNETWORKMODEACTION2_EXIT();
#endif
}
FUNC(void, CANNM_CODE) CanNm_HsmCanNmSfNetworkModeAction3(
  CANNM_PDL_SF(const uint8 instIdx))
{
#if (CANNM_HSM_INST_MULTI_ENABLED == STD_ON)
  DBG_CANNM_HSMCANNMSFNETWORKMODEACTION3_ENTRY(CANNM_INST(instIdx));
#else
  DBG_CANNM_HSMCANNMSFNETWORKMODEACTION3_ENTRY();
#endif
  /* action 'HandleTxConfirmation();'
   * for TX_CONFIRMATION[]/...
   * internal transition */
#if ((CANNM_PASSIVE_MODE_ENABLED == STD_OFF) && (CANNM_IMMEDIATE_TXCONF_ENABLED == STD_OFF))
  CanNm_HandleTxConfirmation(CANNM_PL_SF(instIdx));
#elif (CANNM_HSM_INST_MULTI_ENABLED == STD_ON)
  TS_PARAM_UNUSED(instIdx);
#endif

#if (CANNM_HSM_INST_MULTI_ENABLED == STD_ON)
  DBG_CANNM_HSMCANNMSFNETWORKMODEACTION3_EXIT(CANNM_INST(instIdx));
#else
  DBG_CANNM_HSMCANNMSFNETWORKMODEACTION3_EXIT();
#endif
}
FUNC(boolean, CANNM_CODE) CanNm_HsmCanNmSfNetworkModeGuard4(
  CANNM_PDL_SF(const uint8 instIdx))
{
  boolean ret;
#if (CANNM_HSM_INST_MULTI_ENABLED == STD_ON)
  DBG_CANNM_HSMCANNMSFNETWORKMODEGUARD4_ENTRY(CANNM_INST(instIdx));
#else
  DBG_CANNM_HSMCANNMSFNETWORKMODEGUARD4_ENTRY();
#endif
  /* guard condition 'PnHandleMultipleNetworkRequests==TRUE && NetworkRequested==TRUE'
   * for NET_REQ_STATUS_CHANGED[...]/
   * external transition to state NetworkMode */
  if ((CANNM_CHANNEL_CONFIG(instIdx).PnHandleMultipleNetworkRequests == TRUE)
      && ((CANNM_CHANNEL_STATUS(instIdx).ChanStatus & CANNM_NETWORK_REQUESTED) != 0U))
  {
    ret = TRUE;
  }
  else
  {
    ret = FALSE;
  }
#if (CANNM_HSM_INST_MULTI_ENABLED == STD_ON)
  DBG_CANNM_HSMCANNMSFNETWORKMODEGUARD4_EXIT(ret, CANNM_INST(instIdx));
#else
  DBG_CANNM_HSMCANNMSFNETWORKMODEGUARD4_EXIT(ret);
#endif
  return ret;
}

/* ************************************************************************
 * State: ReadySleepState
 * Parent state: NetworkMode
 * Init substate: ReadySleepRemoteActivity
 * Transitions originating from this state:
 * 1) ReadySleepState -> PrepareBusSleepMode: NM_TIMEOUT[NmTimer == 0]/
 */

FUNC(void, CANNM_CODE) CanNm_HsmCanNmSfReadySleepStateEntry(
  CANNM_PDL_SF(const uint8 instIdx))
{
#if (CANNM_HSM_INST_MULTI_ENABLED == STD_ON)
  DBG_CANNM_HSMCANNMSFREADYSLEEPSTATEENTRY_ENTRY(CANNM_INST(instIdx));
#else
  DBG_CANNM_HSMCANNMSFREADYSLEEPSTATEENTRY_ENTRY();
#endif
  /* entry action '#ifdef (SCI) Nm_StateChangeNotification(LastState, ActState) --> CANNM166;CurState = ReadySleepState;' */
#if (CANNM_STATE_CHANGE_IND_ENABLED == STD_ON)
  /* CANNM166 */
  Nm_StateChangeNotification(
    CANNM_CHANNEL_CONFIG(instIdx).nmChannelId,
    CANNM_CHANNEL_STATUS(instIdx).CurState,
    NM_STATE_READY_SLEEP);
#endif

  CANNM_CHANNEL_STATUS(instIdx).CurState = NM_STATE_READY_SLEEP;
#if (CANNM_HSM_INST_MULTI_ENABLED == STD_ON)
  DBG_CANNM_HSMCANNMSFREADYSLEEPSTATEENTRY_EXIT(CANNM_INST(instIdx));
#else
  DBG_CANNM_HSMCANNMSFREADYSLEEPSTATEENTRY_EXIT();
#endif
}
FUNC(boolean, CANNM_CODE) CanNm_HsmCanNmSfReadySleepStateGuard1(
  CANNM_PDL_SF(const uint8 instIdx))
{
  boolean ret;
#if (CANNM_HSM_INST_MULTI_ENABLED == STD_ON)
  DBG_CANNM_HSMCANNMSFREADYSLEEPSTATEGUARD1_ENTRY(CANNM_INST(instIdx));
#else
  DBG_CANNM_HSMCANNMSFREADYSLEEPSTATEGUARD1_ENTRY();
#endif
  /* guard condition 'NmTimer == 0'
   * for NM_TIMEOUT[...]/
   * external transition to state PrepareBusSleepMode */
  ret = (boolean)((CANNM_CHANNEL_STATUS(instIdx).CanNmTimer == 0U) ? TRUE : FALSE);
#if (CANNM_HSM_INST_MULTI_ENABLED == STD_ON)
  DBG_CANNM_HSMCANNMSFREADYSLEEPSTATEGUARD1_EXIT(ret, CANNM_INST(instIdx));
#else
  DBG_CANNM_HSMCANNMSFREADYSLEEPSTATEGUARD1_EXIT(ret);
#endif
  return ret;
}

/* ************************************************************************
 * State: ReadySleepRemoteActivity
 * Parent state: ReadySleepState
 * Init substate: none, this is a leaf state
 * Transitions originating from this state:
 * 1) ReadySleepRemoteActivity -> NormalOperationRemoteActivity: NET_REQ_STATUS_CHANGED[PnHandleMultipleNetworkRequests==FALSE && NetworkRequested==TRUE]/
 * 2) ReadySleepRemoteActivity -> RepeatMessageState: REPEAT_MESSAGE_REASON[]/
 */

FUNC(boolean, CANNM_CODE) CanNm_HsmCanNmSfReadySleepRemoteActivityGuard1(
  CANNM_PDL_SF(const uint8 instIdx))
{
  boolean ret;
#if (CANNM_HSM_INST_MULTI_ENABLED == STD_ON)
  DBG_CANNM_HSMCANNMSFREADYSLEEPREMOTEACTIVITYGUARD1_ENTRY(CANNM_INST(instIdx));
#else
  DBG_CANNM_HSMCANNMSFREADYSLEEPREMOTEACTIVITYGUARD1_ENTRY();
#endif
  /* guard condition 'PnHandleMultipleNetworkRequests==FALSE && NetworkRequested==TRUE'
   * for NET_REQ_STATUS_CHANGED[...]/
   * external transition to state NormalOperationRemoteActivity */
  if ((CANNM_CHANNEL_CONFIG(instIdx).PnHandleMultipleNetworkRequests == FALSE)
      && ((CANNM_CHANNEL_STATUS(instIdx).ChanStatus & CANNM_NETWORK_REQUESTED) != 0U))
  {
    ret = TRUE;
  }
  else
  {
    ret = FALSE;
  }
#if (CANNM_HSM_INST_MULTI_ENABLED == STD_ON)
  DBG_CANNM_HSMCANNMSFREADYSLEEPREMOTEACTIVITYGUARD1_EXIT(ret, CANNM_INST(instIdx));
#else
  DBG_CANNM_HSMCANNMSFREADYSLEEPREMOTEACTIVITYGUARD1_EXIT(ret);
#endif
  return ret;
}

/* ************************************************************************
 * State: ReadySleepRemoteSleep
 * Parent state: ReadySleepState
 * Init substate: none, this is a leaf state
 * Transitions originating from this state:
 * 1) ReadySleepRemoteSleep -> NormalOperationRemoteSleep: NET_REQ_STATUS_CHANGED[PnHandleMultipleNetworkRequests==FALSE && NetworkRequested==TRUE]/
 * 2) ReadySleepRemoteSleep -> RepeatMessageState: REPEAT_MESSAGE_REASON[]/Nm_RemoteSleepCancellation();
 * 3) ReadySleepRemoteSleep -> ReadySleepRemoteActivity: RX_INDICATION[]/Nm_RemoteSleepCancellation(); HandleRxIndicationCommon();
 */

FUNC(boolean, CANNM_CODE) CanNm_HsmCanNmSfReadySleepRemoteSleepGuard1(
  CANNM_PDL_SF(const uint8 instIdx))
{
  boolean ret;
#if (CANNM_HSM_INST_MULTI_ENABLED == STD_ON)
  DBG_CANNM_HSMCANNMSFREADYSLEEPREMOTESLEEPGUARD1_ENTRY(CANNM_INST(instIdx));
#else
  DBG_CANNM_HSMCANNMSFREADYSLEEPREMOTESLEEPGUARD1_ENTRY();
#endif
  /* guard condition 'PnHandleMultipleNetworkRequests==FALSE && NetworkRequested==TRUE'
   * for NET_REQ_STATUS_CHANGED[...]/
   * external transition to state NormalOperationRemoteSleep */
  if ((CANNM_CHANNEL_CONFIG(instIdx).PnHandleMultipleNetworkRequests == FALSE)
     && ((CANNM_CHANNEL_STATUS(instIdx).ChanStatus & CANNM_NETWORK_REQUESTED) != 0U))
  {
    ret = TRUE;
  }
  else
  {
    ret = FALSE;
  }
#if (CANNM_HSM_INST_MULTI_ENABLED == STD_ON)
  DBG_CANNM_HSMCANNMSFREADYSLEEPREMOTESLEEPGUARD1_EXIT(ret, CANNM_INST(instIdx));
#else
  DBG_CANNM_HSMCANNMSFREADYSLEEPREMOTESLEEPGUARD1_EXIT(ret);
#endif
  return ret;
}
FUNC(void, CANNM_CODE) CanNm_HsmCanNmSfReadySleepRemoteSleepAction2(
  CANNM_PDL_SF(const uint8 instIdx))
{
#if (CANNM_HSM_INST_MULTI_ENABLED == STD_ON)
  DBG_CANNM_HSMCANNMSFREADYSLEEPREMOTESLEEPACTION2_ENTRY(CANNM_INST(instIdx));
#else
  DBG_CANNM_HSMCANNMSFREADYSLEEPREMOTESLEEPACTION2_ENTRY();
#endif
  /* action 'Nm_RemoteSleepCancellation();'
   * for REPEAT_MESSAGE_REASON[]/...
   * external transition to state RepeatMessageState */
#if (CANNM_REMOTE_SLEEP_IND_ENABLED == STD_ON)
  Nm_RemoteSleepCancellation(CANNM_CHANNEL_CONFIG(instIdx).nmChannelId);
#elif (CANNM_HSM_INST_MULTI_ENABLED == STD_ON)
  TS_PARAM_UNUSED(instIdx);
#endif
#if (CANNM_HSM_INST_MULTI_ENABLED == STD_ON)
  DBG_CANNM_HSMCANNMSFREADYSLEEPREMOTESLEEPACTION2_EXIT(CANNM_INST(instIdx));
#else
  DBG_CANNM_HSMCANNMSFREADYSLEEPREMOTESLEEPACTION2_EXIT();
#endif
}
FUNC(void, CANNM_CODE) CanNm_HsmCanNmSfReadySleepRemoteSleepAction3(
  CANNM_PDL_SF(const uint8 instIdx))
{
#if (CANNM_HSM_INST_MULTI_ENABLED == STD_ON)
  DBG_CANNM_HSMCANNMSFREADYSLEEPREMOTESLEEPACTION3_ENTRY(CANNM_INST(instIdx));
#else
  DBG_CANNM_HSMCANNMSFREADYSLEEPREMOTESLEEPACTION3_ENTRY();
#endif
  /* action 'Nm_RemoteSleepCancellation(); HandleRxIndicationCommon();'
   * for RX_INDICATION[]/...
   * external transition to state ReadySleepRemoteActivity */
#if (CANNM_REMOTE_SLEEP_IND_ENABLED == STD_ON)
  Nm_RemoteSleepCancellation(CANNM_CHANNEL_CONFIG(instIdx).nmChannelId);
#elif (CANNM_HSM_INST_MULTI_ENABLED == STD_ON)
  TS_PARAM_UNUSED(instIdx);
#endif
  CanNm_HandleRxIndicationCommon(CANNM_PL_SF(instIdx));
#if (CANNM_HSM_INST_MULTI_ENABLED == STD_ON)
  DBG_CANNM_HSMCANNMSFREADYSLEEPREMOTESLEEPACTION3_EXIT(CANNM_INST(instIdx));
#else
  DBG_CANNM_HSMCANNMSFREADYSLEEPREMOTESLEEPACTION3_EXIT();
#endif
}

/* ************************************************************************
 * State: SendingSubMode
 * Parent state: NetworkMode
 * Init substate: RepeatMessageState
 * Transitions originating from this state:
 * 1) TX_TIMEOUT[]/#if ((PASSIVE_MODE == false) && (IMM_TRANCONF_ENBLD == false)) Nm_TxTimeoutException();
 * 2) MSG_CYCLE_TIMEOUT[]/HandleMsgCycleTimeout() --> CANNM032
 * 3) NM_TIMEOUT[]/if (NmTimeout == 0) {HandleNmTimeout();}
 *    #if (CANNM_PN_ENABLED == STD_ON) CanSM_TxTimeoutException();
 */

FUNC(void, CANNM_CODE) CanNm_HsmCanNmSfSendingSubModeEntry(
  CANNM_PDL_SF(const uint8 instIdx))
{
#if (CANNM_HSM_INST_MULTI_ENABLED == STD_ON)
  DBG_CANNM_HSMCANNMSFSENDINGSUBMODEENTRY_ENTRY(CANNM_INST(instIdx));
#else
  DBG_CANNM_HSMCANNMSFSENDINGSUBMODEENTRY_ENTRY();
#endif
  /* entry action '#if (!(PASSIVE_MODE == true)) &&
   * (!(ChanStatus & CANNM_COM_DISABLED))&&(Chanstatus & IMM_TRANS_ACTIVE)
   * HandleImmediateTransmissions() ---> CANNM324
   * #if (!(PASSIVE_MODE == true)) && (!(ChanStatus & CANNM_COM_DISABLED)) &&
   * (!(Chanstatus & IMM_TRANS_ACTIVE))
   * MsgCycleTimerStart(CANNM_MSG_CYCLE_OFFSET) -->CANNM100 CANNM116 CANNM005' */
#if (CANNM_PASSIVE_MODE_ENABLED == STD_OFF)
  if ((CANNM_CHANNEL_STATUS(instIdx).ChanStatus & CANNM_COM_DISABLED) == 0U)
  {
#if (CANNM_IMMEDIATE_TRANSMISSION == STD_ON)
    /* Check if immediate transmission feature is active for this channel. */
    if ((CANNM_CHANNEL_STATUS(instIdx).ChanStatus & CANNM_IMMEDIATE_TRANSMISSION_ACTIVE) != 0U)
    {
      /* Load immediate transmission counter with configured value */
      CANNM_CHANNEL_STATUS(instIdx).ImmediateNmTransmissionCounter =
        CANNM_CHANNEL_CONFIG(instIdx).ImmediateNmTransmissions;
      /* Start transmission immediately */
      CanNm_HandleTransmit(CANNM_PL_SF(instIdx));
    }
    else
#endif
    {
      /* CANNM005 */
    CanNm_MsgCycleTimerStart(
      CANNM_PL_TIMER(instIdx, CANNM_CHANNEL_CONFIG(instIdx).MsgCycleOffset));
    }
  }
#elif (CANNM_HSM_INST_MULTI_ENABLED == STD_ON)
  TS_PARAM_UNUSED(instIdx);
#endif
#if (CANNM_HSM_INST_MULTI_ENABLED == STD_ON)
  DBG_CANNM_HSMCANNMSFSENDINGSUBMODEENTRY_EXIT(CANNM_INST(instIdx));
#else
  DBG_CANNM_HSMCANNMSFSENDINGSUBMODEENTRY_EXIT();
#endif
}
FUNC(void, CANNM_CODE) CanNm_HsmCanNmSfSendingSubModeExit(
   CANNM_PDL_SF(const uint8 instIdx))
{
#if (CANNM_HSM_INST_MULTI_ENABLED == STD_ON)
  DBG_CANNM_HSMCANNMSFSENDINGSUBMODEEXIT_ENTRY(CANNM_INST(instIdx));
#else
  DBG_CANNM_HSMCANNMSFSENDINGSUBMODEEXIT_ENTRY();
#endif
  /* exit action 'MsgCycleTimerStop(); --> CANNM051' */
#if (CANNM_PASSIVE_MODE_ENABLED == STD_OFF)
  CanNm_MsgCycleTimerStop(CANNM_PL_SF(instIdx));
#elif (CANNM_HSM_INST_MULTI_ENABLED == STD_ON)
  TS_PARAM_UNUSED(instIdx);
#endif
#if (CANNM_HSM_INST_MULTI_ENABLED == STD_ON)
  DBG_CANNM_HSMCANNMSFSENDINGSUBMODEEXIT_EXIT(CANNM_INST(instIdx));
#else
  DBG_CANNM_HSMCANNMSFSENDINGSUBMODEEXIT_EXIT();
#endif
}
FUNC(void, CANNM_CODE) CanNm_HsmCanNmSfSendingSubModeAction1(
  CANNM_PDL_SF(const uint8 instIdx))
{
#if (CANNM_HSM_INST_MULTI_ENABLED == STD_ON)
  DBG_CANNM_HSMCANNMSFSENDINGSUBMODEACTION1_ENTRY(CANNM_INST(instIdx));
#else
  DBG_CANNM_HSMCANNMSFSENDINGSUBMODEACTION1_ENTRY();
#endif
  /* action '#if ((PASSIVE_MODE == false) && (IMM_TRANCONF_ENBLD == false)) Nm_TxTimeoutException();
   * #if (CANNM_PN_ENABLED == STD_ON) CanSM_TxTimeoutException();'
   * for TX_TIMEOUT[]/...
   * internal transition */
#if ((CANNM_HSM_INST_MULTI_ENABLED == STD_ON) && \
    ((CANNM_PASSIVE_MODE_ENABLED == STD_ON) || \
     (CANNM_IMMEDIATE_TXCONF_ENABLED == STD_ON)))
  TS_PARAM_UNUSED(instIdx);
#endif

#if ((CANNM_PASSIVE_MODE_ENABLED == STD_OFF) && \
     (CANNM_IMMEDIATE_TXCONF_ENABLED == STD_OFF))
  Nm_TxTimeoutException(CANNM_CHANNEL_CONFIG(instIdx).nmChannelId);
#endif
#if (CANNM_PN_ENABLED == STD_ON)
  if (TRUE == CANNM_CHANNEL_CONFIG(instIdx).PnEnabled)
  {
    CanSM_TxTimeoutException(CANNM_CHANNEL_CONFIG(instIdx).nmChannelId);
  }
#endif

#if (CANNM_HSM_INST_MULTI_ENABLED == STD_ON)
  DBG_CANNM_HSMCANNMSFSENDINGSUBMODEACTION1_EXIT(CANNM_INST(instIdx));
#else
  DBG_CANNM_HSMCANNMSFSENDINGSUBMODEACTION1_EXIT();
#endif
}

FUNC(void, CANNM_CODE) CanNm_HsmCanNmSfSendingSubModeAction2(
  CANNM_PDL_SF(const uint8 instIdx))
{
#if (CANNM_HSM_INST_MULTI_ENABLED == STD_ON)
  DBG_CANNM_HSMCANNMSFSENDINGSUBMODEACTION2_ENTRY(CANNM_INST(instIdx));
#else
  DBG_CANNM_HSMCANNMSFSENDINGSUBMODEACTION2_ENTRY();
#endif
  /* action 'HandleMsgCycleTimeout() --> CANNM032'
   * for MSG_CYCLE_TIMEOUT[]/...
   * internal transition */
  /* if timer was not yet reactivated before */
#if (CANNM_PASSIVE_MODE_ENABLED == STD_OFF)
   /* Do transmission */
   CanNm_HandleTransmit(CANNM_PL_SF(instIdx));
#elif (CANNM_HSM_INST_MULTI_ENABLED == STD_ON)
  TS_PARAM_UNUSED(instIdx);
#endif

#if (CANNM_HSM_INST_MULTI_ENABLED == STD_ON)
  DBG_CANNM_HSMCANNMSFSENDINGSUBMODEACTION2_EXIT(CANNM_INST(instIdx));
#else
  DBG_CANNM_HSMCANNMSFSENDINGSUBMODEACTION2_EXIT();
#endif
}
FUNC(void, CANNM_CODE) CanNm_HsmCanNmSfSendingSubModeAction3(
  CANNM_PDL_SF(const uint8 instIdx))
{
#if (CANNM_HSM_INST_MULTI_ENABLED == STD_ON)
  DBG_CANNM_HSMCANNMSFSENDINGSUBMODEACTION3_ENTRY(CANNM_INST(instIdx));
#else
  DBG_CANNM_HSMCANNMSFSENDINGSUBMODEACTION3_ENTRY();
#endif
  /* action 'if (NmTimeout == 0) {HandleNmTimeout();}'
   * for NM_TIMEOUT[]/...
   * internal transition */
  if (CANNM_CHANNEL_STATUS(instIdx).CanNmTimer == 0U)
  {
    /* CANNM193, CANNM194: When the NM-Timeout Timer expires in the Repeat
     * Message State or in the Normal Operation State, the NM shall report
     * CANNM_E_NETWORK_TIMEOUT to DET */
#if (CANNM_DEV_ERROR_DETECT == STD_ON)
    /* Service ID CANNM_SERVID_MAINFUNCTION_X is used as this function
     * is state machine generated */
    (void)Det_ReportError
    (CANNM_MODULE_ID, CANNM_INSTANCE_ID,
     CANNM_SERVID_MAINFUNCTION_X, CANNM_E_NETWORK_TIMEOUT);
#endif

    /* CANNM101, CANNM117: When the NM-Timeout Timer expires in the Repeat
     * Message State or in the Normal Operation State, the NM-Timeout Timer
     * shall be restarted.
     *
     * CANNM206: The NM-Timeout Timer shall be reset every time it is started
     * or restarted. */
    CanNm_NmTimerStart(CANNM_PL_TIMER(instIdx, CANNM_CHANNEL_CONFIG(instIdx).CanNmTime));
  }
#if (CANNM_HSM_INST_MULTI_ENABLED == STD_ON)
  DBG_CANNM_HSMCANNMSFSENDINGSUBMODEACTION3_EXIT(CANNM_INST(instIdx));
#else
  DBG_CANNM_HSMCANNMSFSENDINGSUBMODEACTION3_EXIT();
#endif
}

/* ************************************************************************
 * State: NormalOperationState
 * Parent state: SendingSubMode
 * Init substate: NormalOperationRemoteActivity
 * Transitions originating from this state:
 */

FUNC(void, CANNM_CODE) CanNm_HsmCanNmSfNormalOperationStateEntry(
  CANNM_PDL_SF(const uint8 instIdx))
{
#if (CANNM_HSM_INST_MULTI_ENABLED == STD_ON)
  DBG_CANNM_HSMCANNMSFNORMALOPERATIONSTATEENTRY_ENTRY(CANNM_INST(instIdx));
#else
  DBG_CANNM_HSMCANNMSFNORMALOPERATIONSTATEENTRY_ENTRY();
#endif
  /* entry action '#ifdef (SCI) Nm_StateChangeNotification(LastState, ActState);CurState = NormalOperationState;' */
#if (CANNM_STATE_CHANGE_IND_ENABLED == STD_ON)
  Nm_StateChangeNotification(
    CANNM_CHANNEL_CONFIG(instIdx).nmChannelId,
    CANNM_CHANNEL_STATUS(instIdx).CurState, NM_STATE_NORMAL_OPERATION);
#endif

  CANNM_CHANNEL_STATUS(instIdx).CurState = NM_STATE_NORMAL_OPERATION;
#if (CANNM_HSM_INST_MULTI_ENABLED == STD_ON)
  DBG_CANNM_HSMCANNMSFNORMALOPERATIONSTATEENTRY_EXIT(CANNM_INST(instIdx));
#else
  DBG_CANNM_HSMCANNMSFNORMALOPERATIONSTATEENTRY_EXIT();
#endif
}
FUNC(void, CANNM_CODE) CanNm_HsmCanNmSfNormalOperationStateExit(
   CANNM_PDL_SF(const uint8 instIdx))
{
#if (CANNM_HSM_INST_MULTI_ENABLED == STD_ON)
  DBG_CANNM_HSMCANNMSFNORMALOPERATIONSTATEEXIT_ENTRY(CANNM_INST(instIdx));
#else
  DBG_CANNM_HSMCANNMSFNORMALOPERATIONSTATEEXIT_ENTRY();
#endif
  /* exit action 'RsiStatus = RSI_REJECT_QUERY' */
#if (CANNM_REMOTE_SLEEP_IND_ENABLED == STD_ON)
  /* Enter critical section to protect from concurrent access
   * to different bits in 'ChanStatus' in different APIs.
   */
  SchM_Enter_CanNm_SCHM_CANNM_EXCLUSIVE_AREA_0();
  /* set RSI Status to CANNM_RSI_REJECT */
  CANNM_CHANNEL_STATUS(instIdx).ChanStatus &= (uint8)(~CANNM_RSI_MASK);
  CANNM_CHANNEL_STATUS(instIdx).ChanStatus |= CANNM_RSI_REJECT;
  SchM_Exit_CanNm_SCHM_CANNM_EXCLUSIVE_AREA_0();
#elif (CANNM_HSM_INST_MULTI_ENABLED == STD_ON)
  TS_PARAM_UNUSED(instIdx);
#endif
#if (CANNM_HSM_INST_MULTI_ENABLED == STD_ON)
  DBG_CANNM_HSMCANNMSFNORMALOPERATIONSTATEEXIT_EXIT(CANNM_INST(instIdx));
#else
  DBG_CANNM_HSMCANNMSFNORMALOPERATIONSTATEEXIT_EXIT();
#endif
}

/* ************************************************************************
 * State: NormalOperationRemoteActivity
 * Parent state: NormalOperationState
 * Init substate: none, this is a leaf state
 * Transitions originating from this state:
 * 1) RX_INDICATION[]/HandleRxIndication_NOState_RA();
 * 2) NormalOperationRemoteActivity -> ReadySleepRemoteActivity: NET_REQ_STATUS_CHANGED[NetworkRequested==FALSE]/
 * 3) NormalOperationRemoteActivity -> RepeatMessageState: REPEAT_MESSAGE_REASON[]/
 * 4) NormalOperationRemoteActivity -> NormalOperationRemoteSleep: UNI_TIMEOUT[(!(ChanStatus  & CANNM_COM_DISABLED)) && (UniTimer == 0)]/
 */

FUNC(void, CANNM_CODE) CanNm_HsmCanNmSfNormalOperationRemoteActivityEntry(
  CANNM_PDL_SF(const uint8 instIdx))
{
#if (CANNM_HSM_INST_MULTI_ENABLED == STD_ON)
  DBG_CANNM_HSMCANNMSFNORMALOPERATIONREMOTEACTIVITYENTRY_ENTRY(CANNM_INST(instIdx));
#else
  DBG_CANNM_HSMCANNMSFNORMALOPERATIONREMOTEACTIVITYENTRY_ENTRY();
#endif
  /* entry action '#ifdef (RSI) UniTimerStart(CANNM_REMOTE_SLEEP_IND_TIME) -->CANNM150 #ifdef RSI if (RsiStatus == CANNM_RSI_TRUE) {Nm_RemoteSleepCancellation();} RsiStatus = CANNM_RSI_FALSE;' */

#if (CANNM_REMOTE_SLEEP_IND_ENABLED == STD_ON)
  /* CANNM150 */
  CanNm_UniTimerStart(
    CANNM_PL_TIMER(instIdx, CANNM_CHANNEL_CONFIG(instIdx).RSITime));

  if ((CANNM_CHANNEL_STATUS(instIdx).ChanStatus & CANNM_RSI_MASK)
      == CANNM_RSI_TRUE)
  {
    Nm_RemoteSleepCancellation(CANNM_CHANNEL_CONFIG(instIdx).nmChannelId);
  }

  /* Enter critical section to protect from concurrent access
   * to different bits in 'ChanStatus' in different APIs.
   */
  SchM_Enter_CanNm_SCHM_CANNM_EXCLUSIVE_AREA_0();
  /* set RSI Status to CANNM_RSI_FALSE */
  CANNM_CHANNEL_STATUS(instIdx).ChanStatus &= (uint8)(~CANNM_RSI_MASK);
  CANNM_CHANNEL_STATUS(instIdx).ChanStatus |= CANNM_RSI_FALSE;
  SchM_Exit_CanNm_SCHM_CANNM_EXCLUSIVE_AREA_0();
#elif (CANNM_HSM_INST_MULTI_ENABLED == STD_ON)
  TS_PARAM_UNUSED(instIdx);
#endif
#if (CANNM_HSM_INST_MULTI_ENABLED == STD_ON)
  DBG_CANNM_HSMCANNMSFNORMALOPERATIONREMOTEACTIVITYENTRY_EXIT(CANNM_INST(instIdx));
#else
  DBG_CANNM_HSMCANNMSFNORMALOPERATIONREMOTEACTIVITYENTRY_EXIT();
#endif
}
FUNC(void, CANNM_CODE) CanNm_HsmCanNmSfNormalOperationRemoteActivityAction1(
  CANNM_PDL_SF(const uint8 instIdx))
{
#if (CANNM_HSM_INST_MULTI_ENABLED == STD_ON)
  DBG_CANNM_HSMCANNMSFNORMALOPERATIONREMOTEACTIVITYACTION1_ENTRY(CANNM_INST(instIdx));
#else
  DBG_CANNM_HSMCANNMSFNORMALOPERATIONREMOTEACTIVITYACTION1_ENTRY();
#endif
  /* action 'HandleRxIndication_NOState_RA();'
   * for RX_INDICATION[]/...
   * internal transition */
  CanNm_HandleRxIndication_NOState_RA(CANNM_PL_SF(instIdx));
#if (CANNM_HSM_INST_MULTI_ENABLED == STD_ON)
  DBG_CANNM_HSMCANNMSFNORMALOPERATIONREMOTEACTIVITYACTION1_EXIT(CANNM_INST(instIdx));
#else
  DBG_CANNM_HSMCANNMSFNORMALOPERATIONREMOTEACTIVITYACTION1_EXIT();
#endif
}
FUNC(boolean, CANNM_CODE) CanNm_HsmCanNmSfNormalOperationRemoteActivityGuard2(
  CANNM_PDL_SF(const uint8 instIdx))
{
  boolean ret;
#if (CANNM_HSM_INST_MULTI_ENABLED == STD_ON)
  DBG_CANNM_HSMCANNMSFNORMALOPERATIONREMOTEACTIVITYGUARD2_ENTRY(CANNM_INST(instIdx));
#else
  DBG_CANNM_HSMCANNMSFNORMALOPERATIONREMOTEACTIVITYGUARD2_ENTRY();
#endif
  /* guard condition 'NetworkRequested==FALSE'
   * for NET_REQ_STATUS_CHANGED[...]/
   * external transition to state ReadySleepRemoteActivity */
  ret = (boolean)(((CANNM_CHANNEL_STATUS(instIdx).ChanStatus &
                     CANNM_NETWORK_REQUESTED) == 0U) ? TRUE : FALSE);
#if (CANNM_HSM_INST_MULTI_ENABLED == STD_ON)
  DBG_CANNM_HSMCANNMSFNORMALOPERATIONREMOTEACTIVITYGUARD2_EXIT(ret, CANNM_INST(instIdx));
#else
  DBG_CANNM_HSMCANNMSFNORMALOPERATIONREMOTEACTIVITYGUARD2_EXIT(ret);
#endif
  return ret;
}
FUNC(boolean, CANNM_CODE) CanNm_HsmCanNmSfNormalOperationRemoteActivityGuard4(
  CANNM_PDL_SF(const uint8 instIdx))
{
  boolean ret;
#if (CANNM_HSM_INST_MULTI_ENABLED == STD_ON)
  DBG_CANNM_HSMCANNMSFNORMALOPERATIONREMOTEACTIVITYGUARD4_ENTRY(CANNM_INST(instIdx));
#else
  DBG_CANNM_HSMCANNMSFNORMALOPERATIONREMOTEACTIVITYGUARD4_ENTRY();
#endif
  /* guard condition '(!(ChanStatus & CANNM_COM_DISABLED)) && (UniTimer == 0)'
   * for UNI_TIMEOUT[...]/
   * external transition to state NormalOperationRemoteSleep */
  ret = (boolean)
    (((CANNM_CHANNEL_STATUS(instIdx).ChanStatus & CANNM_COM_DISABLED) == 0U) ? TRUE : FALSE);
#if (CANNM_HSM_INST_MULTI_ENABLED == STD_ON)
  DBG_CANNM_HSMCANNMSFNORMALOPERATIONREMOTEACTIVITYGUARD4_EXIT(ret, CANNM_INST(instIdx));
#else
  DBG_CANNM_HSMCANNMSFNORMALOPERATIONREMOTEACTIVITYGUARD4_EXIT(ret);
#endif
  return ret;
}

/* ************************************************************************
 * State: NormalOperationRemoteSleep
 * Parent state: NormalOperationState
 * Init substate: none, this is a leaf state
 * Transitions originating from this state:
 * 1) NormalOperationRemoteSleep -> ReadySleepRemoteSleep: NET_REQ_STATUS_CHANGED[NetworkRequested==FALSE]/
 * 2) NormalOperationRemoteSleep -> RepeatMessageState: REPEAT_MESSAGE_REASON[]/Nm_RemoteSleepCancellation();
 * 3) NormalOperationRemoteSleep -> NormalOperationRemoteActivity: RX_INDICATION[]/HandleRxIndication_NOState() RSI timer is started by entry action /
 */

FUNC(void, CANNM_CODE) CanNm_HsmCanNmSfNormalOperationRemoteSleepEntry(
  CANNM_PDL_SF(const uint8 instIdx))
{
#if (CANNM_HSM_INST_MULTI_ENABLED == STD_ON)
  DBG_CANNM_HSMCANNMSFNORMALOPERATIONREMOTESLEEPENTRY_ENTRY(CANNM_INST(instIdx));
#else
  DBG_CANNM_HSMCANNMSFNORMALOPERATIONREMOTESLEEPENTRY_ENTRY();
#endif
  /* entry action 'Nm_RemoteSleepIndication();RsiStatus = CANNM_RSI_TRUE;' */
#if (CANNM_REMOTE_SLEEP_IND_ENABLED == STD_ON)
  Nm_RemoteSleepIndication(CANNM_CHANNEL_CONFIG(instIdx).nmChannelId);

  /* Enter critical section to protect from concurrent access
   * to different bits in 'ChanStatus' in different APIs.
   */
  SchM_Enter_CanNm_SCHM_CANNM_EXCLUSIVE_AREA_0();
  /* set RSI Status to CANNM_RSI_TRUE */
  CANNM_CHANNEL_STATUS(instIdx).ChanStatus &= (uint8)(~CANNM_RSI_MASK);
  CANNM_CHANNEL_STATUS(instIdx).ChanStatus |= CANNM_RSI_TRUE;
  SchM_Exit_CanNm_SCHM_CANNM_EXCLUSIVE_AREA_0();

#elif (CANNM_HSM_INST_MULTI_ENABLED == STD_ON)
  TS_PARAM_UNUSED(instIdx);
#endif
#if (CANNM_HSM_INST_MULTI_ENABLED == STD_ON)
  DBG_CANNM_HSMCANNMSFNORMALOPERATIONREMOTESLEEPENTRY_EXIT(CANNM_INST(instIdx));
#else
  DBG_CANNM_HSMCANNMSFNORMALOPERATIONREMOTESLEEPENTRY_EXIT();
#endif
}
FUNC(boolean, CANNM_CODE) CanNm_HsmCanNmSfNormalOperationRemoteSleepGuard1(
  CANNM_PDL_SF(const uint8 instIdx))
{
  boolean ret;
#if (CANNM_HSM_INST_MULTI_ENABLED == STD_ON)
  DBG_CANNM_HSMCANNMSFNORMALOPERATIONREMOTESLEEPGUARD1_ENTRY(CANNM_INST(instIdx));
#else
  DBG_CANNM_HSMCANNMSFNORMALOPERATIONREMOTESLEEPGUARD1_ENTRY();
#endif
  /* guard condition 'NetworkRequested==FALSE'
   * for NET_REQ_STATUS_CHANGED[...]/
   * external transition to state ReadySleepRemoteSleep */
  ret = (boolean)(((CANNM_CHANNEL_STATUS(instIdx).ChanStatus &
                     CANNM_NETWORK_REQUESTED) == 0U) ? TRUE : FALSE);
#if (CANNM_HSM_INST_MULTI_ENABLED == STD_ON)
  DBG_CANNM_HSMCANNMSFNORMALOPERATIONREMOTESLEEPGUARD1_EXIT(ret, CANNM_INST(instIdx));
#else
  DBG_CANNM_HSMCANNMSFNORMALOPERATIONREMOTESLEEPGUARD1_EXIT(ret);
#endif
  return ret;
}
FUNC(void, CANNM_CODE) CanNm_HsmCanNmSfNormalOperationRemoteSleepAction2(
  CANNM_PDL_SF(const uint8 instIdx))
{
#if (CANNM_HSM_INST_MULTI_ENABLED == STD_ON)
  DBG_CANNM_HSMCANNMSFNORMALOPERATIONREMOTESLEEPACTION2_ENTRY(CANNM_INST(instIdx));
#else
  DBG_CANNM_HSMCANNMSFNORMALOPERATIONREMOTESLEEPACTION2_ENTRY();
#endif
  /* action 'Nm_RemoteSleepCancellation();'
   * for REPEAT_MESSAGE_REASON[]/...
   * external transition to state RepeatMessageState */
#if (CANNM_REMOTE_SLEEP_IND_ENABLED == STD_ON)
  Nm_RemoteSleepCancellation(CANNM_CHANNEL_CONFIG(instIdx).nmChannelId);
#elif (CANNM_HSM_INST_MULTI_ENABLED == STD_ON)
  TS_PARAM_UNUSED(instIdx);
#endif
#if (CANNM_HSM_INST_MULTI_ENABLED == STD_ON)
  DBG_CANNM_HSMCANNMSFNORMALOPERATIONREMOTESLEEPACTION2_EXIT(CANNM_INST(instIdx));
#else
  DBG_CANNM_HSMCANNMSFNORMALOPERATIONREMOTESLEEPACTION2_EXIT();
#endif
}
FUNC(void, CANNM_CODE) CanNm_HsmCanNmSfNormalOperationRemoteSleepAction3(
  CANNM_PDL_SF(const uint8 instIdx))
{
#if (CANNM_HSM_INST_MULTI_ENABLED == STD_ON)
  DBG_CANNM_HSMCANNMSFNORMALOPERATIONREMOTESLEEPACTION3_ENTRY(CANNM_INST(instIdx));
#else
  DBG_CANNM_HSMCANNMSFNORMALOPERATIONREMOTESLEEPACTION3_ENTRY();
#endif
  /* action 'HandleRxIndication_NOState() RSI timer is started by entry action /'
   * for RX_INDICATION[]/...
   * external transition to state NormalOperationRemoteActivity */
  CanNm_HandleRxIndication_NOState(CANNM_PL_SF(instIdx));
#if (CANNM_HSM_INST_MULTI_ENABLED == STD_ON)
  DBG_CANNM_HSMCANNMSFNORMALOPERATIONREMOTESLEEPACTION3_EXIT(CANNM_INST(instIdx));
#else
  DBG_CANNM_HSMCANNMSFNORMALOPERATIONREMOTESLEEPACTION3_EXIT();
#endif
}

/* ************************************************************************
 * State: RepeatMessageState
 * Parent state: SendingSubMode
 * Init substate: none, this is a leaf state
 * Transitions originating from this state:
 * 1) RepeatMessageState -> ReadySleepState: RMS_TIMEOUT[(NetworkRequested==FALSE) && (RMS timer == 0))]/
 * 2) RepeatMessageState -> NormalOperationState: RMS_TIMEOUT[((NetworkRequested==TRUE) && (RMS timer == 0))]/
 */

FUNC(void, CANNM_CODE) CanNm_HsmCanNmSfRepeatMessageStateEntry(
  CANNM_PDL_SF(const uint8 instIdx))
{
#if (CANNM_HSM_INST_MULTI_ENABLED == STD_ON)
  DBG_CANNM_HSMCANNMSFREPEATMESSAGESTATEENTRY_ENTRY(CANNM_INST(instIdx));
#else
  DBG_CANNM_HSMCANNMSFREPEATMESSAGESTATEENTRY_ENTRY();
#endif
  /* entry action '#ifdef RSI RsiStatus = CANNM_RSI_REJECT#ifdev (SCI) Nm_StateChangeNotification(LastState, ActState);CurState = RepeatMessageState; RMSTimerStart() -->CANNM102;' */

  /* CANNM102 */
  CanNm_RmsTimerStart(
    CANNM_PL_TIMER(instIdx, CANNM_CHANNEL_CONFIG(instIdx).RMSTime));
#if (CANNM_STATE_CHANGE_IND_ENABLED == STD_ON)
  Nm_StateChangeNotification(
    CANNM_CHANNEL_CONFIG(instIdx).nmChannelId,
    CANNM_CHANNEL_STATUS(instIdx).CurState, NM_STATE_REPEAT_MESSAGE);
#endif

#if (CANNM_REMOTE_SLEEP_IND_ENABLED == STD_ON)
  /* Enter critical section to protect from concurrent access
   * to different bits in 'ChanStatus' in different APIs.
   */
  SchM_Enter_CanNm_SCHM_CANNM_EXCLUSIVE_AREA_0();
  /* set RSI Status to CANNM_RSI_REJECT */
  CANNM_CHANNEL_STATUS(instIdx).ChanStatus &= (uint8)(~CANNM_RSI_MASK);
  CANNM_CHANNEL_STATUS(instIdx).ChanStatus |= CANNM_RSI_REJECT;
  SchM_Exit_CanNm_SCHM_CANNM_EXCLUSIVE_AREA_0();
#endif


  CANNM_CHANNEL_STATUS(instIdx).CurState = NM_STATE_REPEAT_MESSAGE;

#if (CANNM_HSM_INST_MULTI_ENABLED == STD_ON)
  DBG_CANNM_HSMCANNMSFREPEATMESSAGESTATEENTRY_EXIT(CANNM_INST(instIdx));
#else
  DBG_CANNM_HSMCANNMSFREPEATMESSAGESTATEENTRY_EXIT();
#endif
}
FUNC(void, CANNM_CODE) CanNm_HsmCanNmSfRepeatMessageStateExit(
   CANNM_PDL_SF(const uint8 instIdx))
{
  /* exit action '#if (NODE_DETECTION_ENABLED == true) <Unset Repeat Message Bit> --> CANNM107' */
#if (CANNM_NODE_DETECTION_ENABLED == STD_ON)
  CONSTP2VAR(uint8, AUTOMATIC, AUTOSAR_COMSTACKDATA) TxPduPtr
    = &CANNM_CHANNEL_CONFIG(instIdx).TxPduPtr[
      CANNM_CHANNEL_CONFIG(instIdx).CbvPos];
#endif

#if (CANNM_HSM_INST_MULTI_ENABLED == STD_ON)
  DBG_CANNM_HSMCANNMSFREPEATMESSAGESTATEEXIT_ENTRY(CANNM_INST(instIdx));
#else
  DBG_CANNM_HSMCANNMSFREPEATMESSAGESTATEEXIT_ENTRY();
#endif

#if (CANNM_NODE_DETECTION_ENABLED == STD_ON)
  /* Clear RepeatMessageBit, CANNM107 */
  TS_AtomicClearBit_8(TxPduPtr, CANNM_CBV_REPEATMESSAGEBIT);

#elif (CANNM_HSM_INST_MULTI_ENABLED == STD_ON)
  TS_PARAM_UNUSED(instIdx);
#endif

#if (CANNM_HSM_INST_MULTI_ENABLED == STD_ON)
  DBG_CANNM_HSMCANNMSFREPEATMESSAGESTATEEXIT_EXIT(CANNM_INST(instIdx));
#else
  DBG_CANNM_HSMCANNMSFREPEATMESSAGESTATEEXIT_EXIT();
#endif
}
FUNC(boolean, CANNM_CODE) CanNm_HsmCanNmSfRepeatMessageStateGuard1(
  CANNM_PDL_SF(const uint8 instIdx))
{
  boolean ret;
#if (CANNM_HSM_INST_MULTI_ENABLED == STD_ON)
  DBG_CANNM_HSMCANNMSFREPEATMESSAGESTATEGUARD1_ENTRY(CANNM_INST(instIdx));
#else
  DBG_CANNM_HSMCANNMSFREPEATMESSAGESTATEGUARD1_ENTRY();
#endif
  /* guard condition '(NetworkRequested==FALSE) && (RMS timer == 0))'
   * for RMS_TIMEOUT[...]/
   * external transition to state ReadySleepState */
  ret = (boolean)( ( ( (CANNM_CHANNEL_STATUS(instIdx).ChanStatus &
                         CANNM_NETWORK_REQUESTED) == 0U) &&
                       (CANNM_CHANNEL_STATUS(instIdx).RmsTimer == 0U)
                   ) ? TRUE : FALSE
                 );
#if (CANNM_HSM_INST_MULTI_ENABLED == STD_ON)
  DBG_CANNM_HSMCANNMSFREPEATMESSAGESTATEGUARD1_EXIT(ret, CANNM_INST(instIdx));
#else
  DBG_CANNM_HSMCANNMSFREPEATMESSAGESTATEGUARD1_EXIT(ret);
#endif
  return ret;
}
FUNC(boolean, CANNM_CODE) CanNm_HsmCanNmSfRepeatMessageStateGuard2(
  CANNM_PDL_SF(const uint8 instIdx))
{
  boolean ret;
#if (CANNM_HSM_INST_MULTI_ENABLED == STD_ON)
  DBG_CANNM_HSMCANNMSFREPEATMESSAGESTATEGUARD2_ENTRY(CANNM_INST(instIdx));
#else
  DBG_CANNM_HSMCANNMSFREPEATMESSAGESTATEGUARD2_ENTRY();
#endif
  /* guard condition '((NetworkRequested==TRUE) && (RMS timer == 0))'
   * for RMS_TIMEOUT[...]/
   * external transition to state NormalOperationState */
  ret = (boolean)( ( ( (CANNM_CHANNEL_STATUS(instIdx).ChanStatus &
                        CANNM_NETWORK_REQUESTED) != 0U) &&
                     (CANNM_CHANNEL_STATUS(instIdx).RmsTimer == 0U)
                   )? TRUE : FALSE
                 );
#if (CANNM_HSM_INST_MULTI_ENABLED == STD_ON)
  DBG_CANNM_HSMCANNMSFREPEATMESSAGESTATEGUARD2_EXIT(ret, CANNM_INST(instIdx));
#else
  DBG_CANNM_HSMCANNMSFREPEATMESSAGESTATEGUARD2_EXIT(ret);
#endif
  return ret;
}

/* ************************************************************************
 * State: PrepareBusSleepMode
 * Parent state: TOP
 * Init substate: none, this is a leaf state
 * Transitions originating from this state:
 * 1) PrepareBusSleepMode -> NetworkMode: NET_REQ_STATUS_CHANGED[NetworkRequested==TRUE]/#if (CANNM_IMM._RESTART == true) <Send NM message> #endif
 * 2) PrepareBusSleepMode -> NetworkMode: RX_INDICATION[]/#ifdef (...) Nm_PduRxIndication(); #endif --> CANNM037
 * 3) PrepareBusSleepMode -> BusSleepMode: UNI_TIMEOUT[]/
 */

FUNC(void, CANNM_CODE) CanNm_HsmCanNmSfPrepareBusSleepModeEntry(
  CANNM_PDL_SF(const uint8 instIdx))
{
#if (CANNM_HSM_INST_MULTI_ENABLED == STD_ON)
  DBG_CANNM_HSMCANNMSFPREPAREBUSSLEEPMODEENTRY_ENTRY(CANNM_INST(instIdx));
#else
  DBG_CANNM_HSMCANNMSFPREPAREBUSSLEEPMODEENTRY_ENTRY();
#endif
  /* entry action '#ifdef (SCI) Nm_StateChangeNotification(LastState, ActState);CurState = PrepareBusSleepMode; Nm_PrepareBusSleepMode(); --> CANNM114UniTimerStart(CANNM_WAIT_BUS_SLEEP_TIME); --> CANNM115' */

  /* CANNM115 */
  CanNm_UniTimerStart(
    CANNM_PL_TIMER(instIdx, CANNM_CHANNEL_CONFIG(instIdx).WBSTime));
#if (CANNM_STATE_CHANGE_IND_ENABLED == STD_ON)
  Nm_StateChangeNotification(
    CANNM_CHANNEL_CONFIG(instIdx).nmChannelId,
    CANNM_CHANNEL_STATUS(instIdx).CurState, NM_STATE_PREPARE_BUS_SLEEP);
#endif
  /* CANNM114 */
  Nm_PrepareBusSleepMode(CANNM_CHANNEL_CONFIG(instIdx).nmChannelId);
  CANNM_CHANNEL_STATUS(instIdx).CurState = NM_STATE_PREPARE_BUS_SLEEP;
#if (CANNM_PASSIVE_MODE_ENABLED == STD_OFF)
  CANNM_CHANNEL_STATUS(instIdx).NmTimerExpired = FALSE;
  CANNM_CHANNEL_STATUS(instIdx).FirstCanIfTransmitOk = FALSE;
#endif
#if (CANNM_HSM_INST_MULTI_ENABLED == STD_ON)
  DBG_CANNM_HSMCANNMSFPREPAREBUSSLEEPMODEENTRY_EXIT(CANNM_INST(instIdx));
#else
  DBG_CANNM_HSMCANNMSFPREPAREBUSSLEEPMODEENTRY_EXIT();
#endif
}
FUNC(boolean, CANNM_CODE) CanNm_HsmCanNmSfPrepareBusSleepModeGuard1(
  CANNM_PDL_SF(const uint8 instIdx))
{
  boolean ret;
#if (CANNM_HSM_INST_MULTI_ENABLED == STD_ON)
  DBG_CANNM_HSMCANNMSFPREPAREBUSSLEEPMODEGUARD1_ENTRY(CANNM_INST(instIdx));
#else
  DBG_CANNM_HSMCANNMSFPREPAREBUSSLEEPMODEGUARD1_ENTRY();
#endif
  /* guard condition 'NetworkRequested==TRUE'
   * for NET_REQ_STATUS_CHANGED[...]/#if (CANNM_IMM._RESTART == true) <Send NM message> #endif
   * external transition to state NetworkMode */
  ret = (boolean)(((CANNM_CHANNEL_STATUS(instIdx).ChanStatus &
                     CANNM_NETWORK_REQUESTED) != 0U) ? TRUE : FALSE);
#if (CANNM_HSM_INST_MULTI_ENABLED == STD_ON)
  DBG_CANNM_HSMCANNMSFPREPAREBUSSLEEPMODEGUARD1_EXIT(ret, CANNM_INST(instIdx));
#else
  DBG_CANNM_HSMCANNMSFPREPAREBUSSLEEPMODEGUARD1_EXIT(ret);
#endif
  return ret;
}
FUNC(void, CANNM_CODE) CanNm_HsmCanNmSfPrepareBusSleepModeAction1(
  CANNM_PDL_SF(const uint8 instIdx))
{
#if (CANNM_IMMEDIATE_RESTART_ENABLED == STD_ON)
  PduInfoType pduInfo;
#endif /* CANNM_IMMEDIATE_RESTART_ENABLED == STD_ON */

#if (CANNM_HSM_INST_MULTI_ENABLED == STD_ON)
  DBG_CANNM_HSMCANNMSFPREPAREBUSSLEEPMODEACTION1_ENTRY(CANNM_INST(instIdx));
#else
  DBG_CANNM_HSMCANNMSFPREPAREBUSSLEEPMODEACTION1_ENTRY();
#endif

  /* action '#if (CANNM_IMM._RESTART == true) <Send NM message> #endif'
   * for NET_REQ_STATUS_CHANGED[NetworkRequested==TRUE]/...
   * external transition to state NetworkMode */

#if (CANNM_IMMEDIATE_RESTART_ENABLED == STD_ON)
  /* prepare pduInfo */
  pduInfo.SduDataPtr = CANNM_CHANNEL_CONFIG(instIdx).TxPduPtr;
  pduInfo.SduLength  = CANNM_CHANNEL_CONFIG(instIdx).PduLength;

#if ( CANNM_COM_USER_DATA_SUPPORT == STD_ON )
  /* CANNM328 */
#if (CANNM_HSM_INST_MULTI_ENABLED == STD_ON)
  CanNm_GetPduUserData(instIdx, &pduInfo);
#else
  CanNm_GetPduUserData(0U, &pduInfo);
#endif

#endif /* ( CANNM_COM_USER_DATA_SUPPORT == STD_ON ) */

  /* CANNM122.
   * Here, the return value of CanIf_Transmit is ignored as the SWS doesn't
   * describe reaction upon failure of CanIf_Transmit()
   */
  (void) CanIf_Transmit(CANNM_CHANNEL_CONFIG(instIdx).TxPduId, &pduInfo);
#elif (CANNM_HSM_INST_MULTI_ENABLED == STD_ON)
  TS_PARAM_UNUSED(instIdx);
#endif
#if (CANNM_HSM_INST_MULTI_ENABLED == STD_ON)
  DBG_CANNM_HSMCANNMSFPREPAREBUSSLEEPMODEACTION1_EXIT(CANNM_INST(instIdx));
#else
  DBG_CANNM_HSMCANNMSFPREPAREBUSSLEEPMODEACTION1_EXIT();
#endif
}
FUNC(void, CANNM_CODE) CanNm_HsmCanNmSfPrepareBusSleepModeAction2(
  CANNM_PDL_SF(const uint8 instIdx))
{
#if (CANNM_HSM_INST_MULTI_ENABLED == STD_ON)
  DBG_CANNM_HSMCANNMSFPREPAREBUSSLEEPMODEACTION2_ENTRY(CANNM_INST(instIdx));
#else
  DBG_CANNM_HSMCANNMSFPREPAREBUSSLEEPMODEACTION2_ENTRY();
#endif
  /* action '#ifdef (...) Nm_PduRxIndication(); #endif --> CANNM037'
   * for RX_INDICATION[]/...
   * external transition to state NetworkMode */
#if (CANNM_PDU_RX_INDICATION_ENABLED == STD_ON)
  /* CANNM037 */
  Nm_PduRxIndication(CANNM_CHANNEL_CONFIG(instIdx).nmChannelId);
#elif (CANNM_HSM_INST_MULTI_ENABLED == STD_ON)
  TS_PARAM_UNUSED(instIdx);
#endif
#if (CANNM_HSM_INST_MULTI_ENABLED == STD_ON)
  DBG_CANNM_HSMCANNMSFPREPAREBUSSLEEPMODEACTION2_EXIT(CANNM_INST(instIdx));
#else
  DBG_CANNM_HSMCANNMSFPREPAREBUSSLEEPMODEACTION2_EXIT();
#endif
}


/*==================[internal function definitions]=========================*/

STATIC FUNC(void, CANNM_CODE) CanNm_InitIntVar(
  CANNM_PDL_SF(const uint8 instIdx))
{
#if (CANNM_HSM_INST_MULTI_ENABLED == STD_ON)
  DBG_CANNM_INITINTVAR_ENTRY(CANNM_INST(instIdx));
#else
  DBG_CANNM_INITINTVAR_ENTRY();
#endif
      /* CANNM143, CANNM023 */
    CANNM_CHANNEL_STATUS(instIdx).ChanStatus    = 0U;
#if (CANNM_PASSIVE_MODE_ENABLED == STD_OFF)
    /* CANNM061, CANNM033*/
    CANNM_CHANNEL_STATUS(instIdx).MsgCycleTimer = 0U;
#if (CANNM_IMMEDIATE_TXCONF_ENABLED == STD_OFF)
    CANNM_CHANNEL_STATUS(instIdx).TimeoutTimer  = 0U;
#endif
    CANNM_CHANNEL_STATUS(instIdx).RmsTimer      = 0U;
#endif

#if (CANNM_PASSIVE_MODE_ENABLED == STD_OFF)
#if (CANNM_USER_DATA_ENABLED == STD_ON)
  /* !LINKSTO CANNM025,1 */
  /* Set user data to 0xFF */
  TS_MemSet(
    &CANNM_CHANNEL_CONFIG(instIdx).TxPduPtr[CANNM_CHANNEL_CONFIG(instIdx).UDFBPos],
    0xFFU, (uint16)CANNM_CHANNEL_CONFIG(instIdx).UserDataLength);

  TS_MemSet(
    &CANNM_CHANNEL_CONFIG(instIdx).RxPduPtr[CANNM_CHANNEL_CONFIG(instIdx).UDFBPos],
    0xFFU, (uint16)CANNM_CHANNEL_CONFIG(instIdx).UserDataLength);
#endif

#if (CANNM_NODE_DETECTION_ENABLED == STD_ON)
  /* CANNM085 */
  /* An X-Path check in the configuration schema already ensures that
   * the position of the CBV is not set to OFF when Node Detection support
   * is enabled. So an additional check is not required here.
   */
  CANNM_CHANNEL_CONFIG(instIdx).TxPduPtr[CANNM_CHANNEL_CONFIG(instIdx).CbvPos] = 0x00U;
#endif

#if (CANNM_NODE_ID_ENABLED == STD_ON)
    if (CANNM_CHANNEL_CONFIG(instIdx).NidPos != CANNM_PDU_OFF)
    {
      /* CANNM074, CANNM013 */
      CANNM_CHANNEL_CONFIG(instIdx).TxPduPtr[CANNM_CHANNEL_CONFIG(instIdx).NidPos]
        = CANNM_CHANNEL_CONFIG(instIdx).NodeId;
    }
#endif

#if (CANNM_PN_ENABLED == STD_ON)
  if(CANNM_CHANNEL_CONFIG(instIdx).PnEnabled == TRUE)
  {
    /* CANNM413, Set PN bit */
    CANNM_CHANNEL_CONFIG(instIdx).TxPduPtr[CANNM_CHANNEL_CONFIG(instIdx).CbvPos] |=
                                                                         CANNM_CBV_PNINFOBITMASK;
  }
#endif
#endif

#if (CANNM_PN_ENABLED == STD_ON)
  /* Disable filtering of partial network info */
  CANNM_CHANNEL_STATUS(instIdx).PnFilterEnabled = FALSE;
#endif

#if (CANNM_PN_EIRA_CALC_ENABLED == STD_ON)
  /* Initialise external requests */
  TS_MemSet(CANNM_CHANNEL_STATUS(instIdx).PnInfoEra, 0U, CANNM_PN_INFO_LENGTH);
#endif
#if (CANNM_HSM_INST_MULTI_ENABLED == STD_ON)
  DBG_CANNM_INITINTVAR_EXIT(CANNM_INST(instIdx));
#else
  DBG_CANNM_INITINTVAR_EXIT();
#endif
}

STATIC FUNC(void, CANNM_CODE) CanNm_NmTimerStart(
  CANNM_PL_TIMER(const uint8 instIdx, CanNm_TimeType CanNmTime))
{
#if (CANNM_HSM_INST_MULTI_ENABLED == STD_ON)
  DBG_CANNM_NMTIMERSTART_ENTRY(CANNM_INST(instIdx), CanNmTime);
#else
  DBG_CANNM_NMTIMERSTART_ENTRY(CanNmTime);
#endif
  /* load timer value */
  /* CANNM206 */
  CANNM_CHANNEL_STATUS(instIdx).CanNmTimer = CanNmTime;
#if (CANNM_HSM_INST_MULTI_ENABLED == STD_ON)
  DBG_CANNM_NMTIMERSTART_EXIT(CANNM_INST(instIdx), CanNmTime);
#else
  DBG_CANNM_NMTIMERSTART_EXIT(CanNmTime);
#endif
}

STATIC FUNC(void, CANNM_CODE) CanNm_NmTimerStop(
  CANNM_PDL_SF(const uint8 instIdx))
{
#if (CANNM_HSM_INST_MULTI_ENABLED == STD_ON)
  DBG_CANNM_NMTIMERSTOP_ENTRY(CANNM_INST(instIdx));
#else
  DBG_CANNM_NMTIMERSTOP_ENTRY();
#endif
  /* Stop the Nm Timer */
  CANNM_CHANNEL_STATUS(instIdx).CanNmTimer = 0U;
#if (CANNM_HSM_INST_MULTI_ENABLED == STD_ON)
  DBG_CANNM_NMTIMERSTOP_EXIT(CANNM_INST(instIdx));
#else
  DBG_CANNM_NMTIMERSTOP_EXIT();
#endif
}

STATIC FUNC(void, CANNM_CODE) CanNm_UniTimerStart(
  CANNM_PL_TIMER(const uint8 instIdx, CanNm_TimeType UniTimer))
{
#if (CANNM_HSM_INST_MULTI_ENABLED == STD_ON)
  DBG_CANNM_UNITIMERSTART_ENTRY(CANNM_INST(instIdx), UniTimer);
#else
  DBG_CANNM_UNITIMERSTART_ENTRY(UniTimer);
#endif
  /* load timer value */
  CANNM_CHANNEL_STATUS(instIdx).UniversalTimer = UniTimer;
#if (CANNM_HSM_INST_MULTI_ENABLED == STD_ON)
  DBG_CANNM_UNITIMERSTART_EXIT(CANNM_INST(instIdx), UniTimer);
#else
  DBG_CANNM_UNITIMERSTART_EXIT(UniTimer);
#endif
}

STATIC FUNC(void, CANNM_CODE) CanNm_UniTimerStop(
  CANNM_PDL_SF(const uint8 instIdx))
{
#if (CANNM_HSM_INST_MULTI_ENABLED == STD_ON)
  DBG_CANNM_UNITIMERSTOP_ENTRY(CANNM_INST(instIdx));
#else
  DBG_CANNM_UNITIMERSTOP_ENTRY();
#endif
  /* Stop the Universal Timer */
  CANNM_CHANNEL_STATUS(instIdx).UniversalTimer = 0U;
#if (CANNM_HSM_INST_MULTI_ENABLED == STD_ON)
  DBG_CANNM_UNITIMERSTOP_EXIT(CANNM_INST(instIdx));
#else
  DBG_CANNM_UNITIMERSTOP_EXIT();
#endif
}

STATIC FUNC(void, CANNM_CODE) CanNm_RmsTimerStart(
  CANNM_PL_TIMER(const uint8 instIdx, CanNm_TimeType RmsTime))
{
#if (CANNM_HSM_INST_MULTI_ENABLED == STD_ON)
  DBG_CANNM_RMSTIMERSTART_ENTRY(CANNM_INST(instIdx), RmsTime);
#else
  DBG_CANNM_RMSTIMERSTART_ENTRY(RmsTime);
#endif
  CANNM_CHANNEL_STATUS(instIdx).RmsTimer = RmsTime;
#if (CANNM_HSM_INST_MULTI_ENABLED == STD_ON)
  DBG_CANNM_RMSTIMERSTART_EXIT(CANNM_INST(instIdx), RmsTime);
#else
  DBG_CANNM_RMSTIMERSTART_EXIT(RmsTime);
#endif
}

#if (CANNM_PASSIVE_MODE_ENABLED == STD_OFF)
STATIC FUNC(void, CANNM_CODE) CanNm_MsgCycleTimerStart(
  CANNM_PL_TIMER(const uint8 instIdx, CanNm_TimeType MsgCycleTimer))
{
#if (CANNM_HSM_INST_MULTI_ENABLED == STD_ON)
  DBG_CANNM_MSGCYCLETIMERSTART_ENTRY(CANNM_INST(instIdx), MsgCycleTimer);
#else
  DBG_CANNM_MSGCYCLETIMERSTART_ENTRY(MsgCycleTimer);
#endif
  /* load timer value */
  CANNM_CHANNEL_STATUS(instIdx).MsgCycleTimer = MsgCycleTimer;

  if (MsgCycleTimer == 0U)
  {
    /* Emit the event MSG_CYCLE_TIMEOUT if timer expires immediately */
    (void)CANNM_HSMEMITTOSELFINST(
      &CanNm_HsmScCanNm, instIdx, CANNM_HSM_CANNM_EV_MSG_CYCLE_TIMEOUT);
  }
#if (CANNM_HSM_INST_MULTI_ENABLED == STD_ON)
  DBG_CANNM_MSGCYCLETIMERSTART_EXIT(CANNM_INST(instIdx), MsgCycleTimer);
#else
  DBG_CANNM_MSGCYCLETIMERSTART_EXIT(MsgCycleTimer);
#endif
}

STATIC FUNC(void, CANNM_CODE) CanNm_MsgCycleTimerStop(
  CANNM_PDL_SF(const uint8 instIdx))
{
#if (CANNM_HSM_INST_MULTI_ENABLED == STD_ON)
  DBG_CANNM_MSGCYCLETIMERSTOP_ENTRY(CANNM_INST(instIdx));
#else
  DBG_CANNM_MSGCYCLETIMERSTOP_ENTRY();
#endif
  /* Stop the Msg Cycle Timer */
  CANNM_CHANNEL_STATUS(instIdx).MsgCycleTimer = 0U;
#if (CANNM_HSM_INST_MULTI_ENABLED == STD_ON)
  DBG_CANNM_MSGCYCLETIMERSTOP_EXIT(CANNM_INST(instIdx));
#else
  DBG_CANNM_MSGCYCLETIMERSTOP_EXIT();
#endif
}
#endif

STATIC FUNC(void, CANNM_CODE) CanNm_HandleRxIndicationCommon(
  CANNM_PDL_SF(const uint8 instIdx))
{
#if (CANNM_HSM_INST_MULTI_ENABLED == STD_ON)
  DBG_CANNM_HANDLERXINDICATIONCOMMON_ENTRY(CANNM_INST(instIdx));
#else
  DBG_CANNM_HANDLERXINDICATIONCOMMON_ENTRY();
#endif
  /* CANNM098 */
  /* Note: CanNmTime-1 is assigned to reduce the extra main cycle delay
   * during the transition from Ready Sleep State to Prepare Bus-Sleep Mode
   */
  CanNm_NmTimerStart(CANNM_PL_TIMER(instIdx, (CANNM_CHANNEL_CONFIG(instIdx).CanNmTime - 1U)));

#if (CANNM_PDU_RX_INDICATION_ENABLED == STD_ON)
  /* CANNM037 */
  Nm_PduRxIndication(CANNM_CHANNEL_CONFIG(instIdx).nmChannelId);
#endif
#if (CANNM_HSM_INST_MULTI_ENABLED == STD_ON)
  DBG_CANNM_HANDLERXINDICATIONCOMMON_EXIT(CANNM_INST(instIdx));
#else
  DBG_CANNM_HANDLERXINDICATIONCOMMON_EXIT();
#endif
}

STATIC FUNC(void, CANNM_CODE) CanNm_HandleRxIndication_NOState_RA(
  CANNM_PDL_SF(const uint8 instIdx))
{
#if (CANNM_HSM_INST_MULTI_ENABLED == STD_ON)
  DBG_CANNM_HANDLERXINDICATION_NOSTATE_RA_ENTRY(CANNM_INST(instIdx));
#else
  DBG_CANNM_HANDLERXINDICATION_NOSTATE_RA_ENTRY();
#endif
#if (CANNM_REMOTE_SLEEP_IND_ENABLED == STD_ON)
  CanNm_UniTimerStart(
    CANNM_PL_TIMER(instIdx, CANNM_CHANNEL_CONFIG(instIdx).RSITime));
#endif
  CanNm_HandleRxIndication_NOState(CANNM_PL_SF(instIdx));
#if (CANNM_HSM_INST_MULTI_ENABLED == STD_ON)
  DBG_CANNM_HANDLERXINDICATION_NOSTATE_RA_EXIT(CANNM_INST(instIdx));
#else
  DBG_CANNM_HANDLERXINDICATION_NOSTATE_RA_EXIT();
#endif
}

STATIC FUNC(void, CANNM_CODE) CanNm_HandleRxIndication_NOState(
  CANNM_PDL_SF(const uint8 instIdx))
{
#if (CANNM_HSM_INST_MULTI_ENABLED == STD_ON)
  DBG_CANNM_HANDLERXINDICATION_NOSTATE_ENTRY(CANNM_INST(instIdx));
#else
  DBG_CANNM_HANDLERXINDICATION_NOSTATE_ENTRY();
#endif
  /* CANNM157 => Activate BLR in Normal Operation state */
#if (CANNM_BUS_LOAD_REDUCTION_ENABLED == STD_ON)
  if (CANNM_CHANNEL_CONFIG_BUSLOADREDACTIVE(instIdx))
  {
    /* CANNM069, CANNM238, CANNM023 */
    CanNm_MsgCycleTimerStart(
      CANNM_PL_TIMER(instIdx, CANNM_CHANNEL_CONFIG(instIdx).ReducedTime));
  }
#endif
  /* Execute the common actions for Rx Indication */
  CanNm_HandleRxIndicationCommon(CANNM_PL_SF(instIdx));
#if (CANNM_HSM_INST_MULTI_ENABLED == STD_ON)
  DBG_CANNM_HANDLERXINDICATION_NOSTATE_EXIT(CANNM_INST(instIdx));
#else
  DBG_CANNM_HANDLERXINDICATION_NOSTATE_EXIT();
#endif
}

#if ((CANNM_PASSIVE_MODE_ENABLED == STD_OFF) && (CANNM_IMMEDIATE_TXCONF_ENABLED == STD_OFF))
STATIC FUNC(void, CANNM_CODE) CanNm_HandleTxConfirmation(
  CANNM_PDL_SF(const uint8 instIdx))
{
#if (CANNM_HSM_INST_MULTI_ENABLED == STD_ON)
  DBG_CANNM_HANDLETXCONFIRMATION_ENTRY(CANNM_INST(instIdx));
#else
  DBG_CANNM_HANDLETXCONFIRMATION_ENTRY();
#endif
  /* CANNM099 */
  /* Note: CanNm_NmTimerStart() occurs only after one main cycle
   * once the TX_CONFIRMATION event is emited. So Nm timer is restarted
   * with CanNmTime-1 to avoid an extra main cycle delay.
   */
  CanNm_NmTimerStart(CANNM_PL_TIMER(instIdx, (CANNM_CHANNEL_CONFIG(instIdx).CanNmTime - 1U)));
  /* CANNM065 */
  CANNM_TX_TIMER_STOP(instIdx);
#if (CANNM_HSM_INST_MULTI_ENABLED == STD_ON)
  DBG_CANNM_HANDLETXCONFIRMATION_EXIT(CANNM_INST(instIdx));
#else
  DBG_CANNM_HANDLETXCONFIRMATION_EXIT();
#endif
}
#endif

#if ((CANNM_COM_CONTROL_ENABLED == STD_ON) && (CANNM_PASSIVE_MODE_ENABLED == STD_OFF))
STATIC FUNC(void, CANNM_CODE)CanNm_HandleComControl(
 CANNM_PDL_SF(const uint8 instIdx))
{
#if (CANNM_HSM_INST_MULTI_ENABLED == STD_ON)
  DBG_CANNM_HANDLECOMCONTROL_ENTRY(CANNM_INST(instIdx));
#else
  DBG_CANNM_HANDLECOMCONTROL_ENTRY();
#endif
  if ((CANNM_CHANNEL_STATUS(instIdx).ChanStatus & CANNM_COM_DISABLED) == 0U)
  {
    /* CANNM176, CANNM178 */
    CanNm_MsgCycleTimerStart(
      CANNM_PL_TIMER(instIdx, CANNM_CHANNEL_CONFIG(instIdx).MsgCycleOffset));
    /* CANNM179 */
    CanNm_NmTimerStart(CANNM_PL_TIMER(instIdx, CANNM_CHANNEL_CONFIG(instIdx).CanNmTime));
    /* CANNM180 */
    CanNm_UniTimerStart(
      CANNM_PL_TIMER(instIdx, CANNM_CHANNEL_CONFIG(instIdx).RSITime));
  }
  else
  {
    /* CANNM173 */
    CanNm_MsgCycleTimerStop(CANNM_PL_SF(instIdx));
    /* CANNM174 */
    CanNm_NmTimerStop(CANNM_PL_SF(instIdx));
    /* CANNM175 */
    CanNm_UniTimerStop(CANNM_PL_SF(instIdx));

    /* Enter critical section to protect from concurrent access
     * to different bits in 'ChanStatus' in different APIs.
     */
    SchM_Enter_CanNm_SCHM_CANNM_EXCLUSIVE_AREA_0();
    /* Disable immediate transmision.
     * Note: Enabling transmission will not resume immediate transmission instead
     * CanNm starts normal periodic transmission.
     */
    CANNM_CHANNEL_STATUS(instIdx).ChanStatus &= ((uint8)(~CANNM_IMMEDIATE_TRANSMISSION_ACTIVE));
    SchM_Exit_CanNm_SCHM_CANNM_EXCLUSIVE_AREA_0();

    CANNM_CHANNEL_STATUS(instIdx).ImmediateNmTransmissionCounter = 0U;
  }
#if (CANNM_HSM_INST_MULTI_ENABLED == STD_ON)
  DBG_CANNM_HANDLECOMCONTROL_EXIT(CANNM_INST(instIdx));
#else
  DBG_CANNM_HANDLECOMCONTROL_EXIT();
#endif
}
#endif

#if (CANNM_PASSIVE_MODE_ENABLED == STD_OFF)
FUNC(void, CANNM_CODE) CanNm_HandleTransmit(
  CANNM_PDL_SF(const uint8 instIdx))
{
  PduInfoType pduInfo;
  Std_ReturnType retVal;
  /* prepare pduInfo */
  pduInfo.SduDataPtr = CANNM_CHANNEL_CONFIG(instIdx).TxPduPtr;
  pduInfo.SduLength  = CANNM_CHANNEL_CONFIG(instIdx).PduLength;

#if (CANNM_HSM_INST_MULTI_ENABLED == STD_ON)
  DBG_CANNM_HANDLETRANSMIT_ENTRY(CANNM_INST(instIdx));
#else
  DBG_CANNM_HANDLETRANSMIT_ENTRY();
#endif

#if ( CANNM_COM_USER_DATA_SUPPORT == STD_ON )
  /* CANNM328 */
#if (CANNM_HSM_INST_MULTI_ENABLED == STD_ON)
  CanNm_GetPduUserData(instIdx, &pduInfo);
#else
  CanNm_GetPduUserData(0U, &pduInfo);
#endif
#endif /* ( CANNM_COM_USER_DATA_SUPPORT == STD_ON ) */

  retVal = CanIf_Transmit(CANNM_CHANNEL_CONFIG(instIdx).TxPduId, &pduInfo);
  /*this is used to verify if current or a previous CanIf_Transmit returned E_OK*/
  if(retVal == E_OK)
  {
    CANNM_CHANNEL_STATUS(instIdx).FirstCanIfTransmitOk = TRUE;
  }
  if( (CANNM_CHANNEL_STATUS(instIdx).FirstCanIfTransmitOk == FALSE) &&
      (CANNM_CHANNEL_STATUS(instIdx).NmTimerExpired == FALSE) &&
      (CANNM_CHANNEL_CONFIG(instIdx).NmRetryFirstMessageRequest == TRUE))
  {
    CanNm_RmsTimerStart(
      CANNM_PL_TIMER(instIdx, CANNM_CHANNEL_CONFIG(instIdx).RMSTime + 1U));
    CanNm_MsgCycleTimerStart(
        CANNM_PL_TIMER(instIdx, 1U));  
  }
  else
  {
#if (CANNM_IMMEDIATE_TXCONF_ENABLED == STD_OFF)
    CANNM_TX_TIMER_START(instIdx, CANNM_CHANNEL_CONFIG(instIdx).MsgTimeoutTime);
#endif

#if (CANNM_IMMEDIATE_TRANSMISSION == STD_ON)
    if ((CANNM_CHANNEL_STATUS(instIdx).ChanStatus & CANNM_IMMEDIATE_TRANSMISSION_ACTIVE) != 0U)
    {
      /* Decrement the counter */
      --CANNM_CHANNEL_STATUS(instIdx).ImmediateNmTransmissionCounter;
      if (CANNM_CHANNEL_STATUS(instIdx).ImmediateNmTransmissionCounter > 0U)
      {
        CanNm_MsgCycleTimerStart(
          CANNM_PL_TIMER(instIdx, CANNM_CHANNEL_CONFIG(instIdx).ImmediateNmCycleTime));
      }
      else
      {
        if(CANNM_CHANNEL_CONFIG(instIdx).MsgCycleOffset > 0)
        {
          /* CANNM335 */
          CanNm_MsgCycleTimerStart(
            CANNM_PL_TIMER(instIdx, CANNM_CHANNEL_CONFIG(instIdx).MsgCycleOffset));
        }
        else
        {
            /* CANNM040 */
            CanNm_MsgCycleTimerStart(
              CANNM_PL_TIMER(instIdx, CANNM_CHANNEL_CONFIG(instIdx).MsgCycleTime));
        }

        /* Enter critical section to protect from concurrent access
        * to different bits in 'ChanStatus' in different APIs.
        */
        SchM_Enter_CanNm_SCHM_CANNM_EXCLUSIVE_AREA_0();
        CANNM_CHANNEL_STATUS(instIdx).ChanStatus &= (uint8)(~CANNM_IMMEDIATE_TRANSMISSION_ACTIVE);
        SchM_Exit_CanNm_SCHM_CANNM_EXCLUSIVE_AREA_0();
      }
    }
    else
#endif
    {
      /* CANNM040 */
        CanNm_MsgCycleTimerStart(
          CANNM_PL_TIMER(instIdx, CANNM_CHANNEL_CONFIG(instIdx).MsgCycleTime));
    }
  }
#if (CANNM_HSM_INST_MULTI_ENABLED == STD_ON)
  DBG_CANNM_HANDLETRANSMIT_EXIT(CANNM_INST(instIdx));
#else
  DBG_CANNM_HANDLETRANSMIT_EXIT();
#endif
}
#endif

#define CANNM_STOP_SEC_CODE
#include <MemMap.h>

/*==================[end of file]===========================================*/
