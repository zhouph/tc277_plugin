/* Os_salsa.h
 *
 * This file maps the required SALSA service names onto existing OS services.
 *
 * The content of this file is not integrated into the normal OS files because
 * the names do not conform to the AUTOSAR nameing scheme. These names were
 * specified in the requirements, so they must be used.
 *
 * CHECK: TABS 4 (see editor commands at end of file)
 *
 * Copyright 1998-2014 Elektrobit Automotive GmbH
 * All rights exclusively reserved for Elektrobit Automotive GmbH,
 * unless expressly agreed to otherwise.
 *
 * $Id: Os_salsa.h 17602 2014-02-03 12:09:35Z tojo2507 $
*/
#ifndef __OS_SALSA_H
#define __OS_SALSA_H

#ifndef OS_ASM

#include <Os_types.h>
#include <Os_cpuload.h>

#define GetCpuLoad()	OS_GetCpuLoad(0)
#define GetMaxCpuLoad()	OS_GetCpuLoad(1)
#define InitCpuLoad()	OS_ResetPeakCpuLoad()

os_uint8_t GetStackUsage(os_taskid_t);
os_uint8_t GetSystemStackUsage(void);

#endif  /* OS_ASM */

#endif  /* __OS_SALSA_H */

/* Editor settings - DO NOT DELETE
 * vi:set ts=4:
*/
