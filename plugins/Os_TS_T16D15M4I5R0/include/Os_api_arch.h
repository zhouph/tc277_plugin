/* Os_api_arch.h
 *
 * This file includes the appropriate architecture API include file depending
 * on the chosen architecture.
 *
 *
 * CHECK: TABS 4 (see editor commands at end of file)
 *
 * Copyright 1998-2014 Elektrobit Automotive GmbH
 * All rights exclusively reserved for Elektrobit Automotive GmbH,
 * unless expressly agreed to otherwise.
 *
 * $Id: Os_api_arch.h 17602 2014-02-03 12:09:35Z tojo2507 $
*/
#ifndef __OS_API_ARCH_H
#define __OS_API_ARCH_H

#include <Os_defs.h>

#if (OS_ARCH==OS_PA)
#include <PA/Os_api_PA.h>
#elif (OS_ARCH==OS_TRICORE)
#include <TRICORE/Os_api_TRICORE.h>
#elif (OS_ARCH==OS_V850)
#include <V850/Os_api_V850.h>
#elif (OS_ARCH==OS_RH850)
#include <RH850/Os_api_RH850.h>
#elif (OS_ARCH==OS_NEWARCH)
#include <NEWARCH/Os_api_NEWARCH.h>
#elif (OS_ARCH==OS_PIKEOS)
#include <PIKEOS/Os_api_PIKEOS.h>
#elif (OS_ARCH==OS_S12X)
#include <S12X/Os_api_S12X.h>
#elif (OS_ARCH==OS_XC2000)
#include <XC2000/Os_api_XC2000.h>
#elif (OS_ARCH==OS_EASYCAN)
#include <EASYCAN/Os_api_EASYCAN.h>
#elif (OS_ARCH==OS_MB91)
#include <MB91/Os_api_MB91.h>
#elif (OS_ARCH==OS_R32C)
#include <R32C/Os_api_R32C.h>
#elif (OS_ARCH==OS_WINDOWS)
#include <WINDOWS/Os_api_WINDOWS.h>
#elif (OS_ARCH==OS_SH2)
#include <SH2/Os_api_SH2.h>
#elif (OS_ARCH==OS_SH4)
#include <SH4/Os_api_SH4.h>
#elif (OS_ARCH==OS_ARM)
#include <ARM/Os_api_ARM.h>
#elif (OS_ARCH==OS_LINUX)
#include <LINUX/Os_api_LINUX.h>
#elif (OS_ARCH==OS_DSPIC33)
#include <DSPIC33/Os_api_DSPIC33.h>
#elif (OS_ARCH==OS_CORTEXM)
#include <CORTEXM/Os_api_CORTEXM.h>
#else
#error "Unsupported OS_ARCH defined. Check your Makefiles!"
#endif


#endif /* __OS_API_ARCH_H */

/* Editor settings - DO NOT DELETE
 * vi:set ts=4:
*/
