/* Os_tool.h
 *
 * This file includes the appropriate ARCH_tool.h include file depending
 * on the chosen architecture.
 *
 * The Makefiles must ensure that the OS_ARCH and OS_CPU macros are
 * defined appropriately on the command line.
 *
 * CHECK: TABS 4 (see editor commands at end of file)
 *
 * Copyright 1998-2014 Elektrobit Automotive GmbH
 * All rights exclusively reserved for Elektrobit Automotive GmbH,
 * unless expressly agreed to otherwise.
 *
 * $Id: Os_tool.h 20522 2015-02-19 16:35:09Z stpo8218 $
*/
#ifndef __OS_TOOL_H
#define __OS_TOOL_H

#include <Os_defs.h>

#if (OS_ARCH==OS_PA)

#include <PA/Os_tool_PA.h>

#elif (OS_ARCH==OS_TRICORE)

#include <TRICORE/Os_tool_TRICORE.h>

#elif (OS_ARCH==OS_V850)

#include <V850/Os_tool_V850.h>

#elif (OS_ARCH==OS_RH850)

#include <RH850/Os_tool_RH850.h>

#elif (OS_ARCH==OS_NEWARCH)

#include <NEWARCH/Os_tool_NEWARCH.h>

#elif (OS_ARCH==OS_PIKEOS)

#include <PIKEOS/Os_tool_PIKEOS.h>

#elif (OS_ARCH==OS_S12X)

#include <S12X/Os_tool_S12X.h>

#elif (OS_ARCH==OS_XC2000)

#include <XC2000/Os_tool_XC2000.h>

#elif (OS_ARCH==OS_EASYCAN)

#include <EASYCAN/Os_tool_EASYCAN.h>

#elif (OS_ARCH==OS_MB91)

#include <MB91/Os_tool_MB91.h>

#elif (OS_ARCH==OS_R32C)

#include <R32C/Os_tool_R32C.h>

#elif (OS_ARCH==OS_WINDOWS)

#include <WINDOWS/Os_tool_WINDOWS.h>

#elif (OS_ARCH==OS_SH2)

#include <SH2/Os_tool_SH2.h>

#elif (OS_ARCH==OS_SH4)

#include <SH4/Os_tool_SH4.h>

#elif (OS_ARCH==OS_ARM)

#include <ARM/Os_tool_ARM.h>

#elif (OS_ARCH==OS_LINUX)

#include <LINUX/Os_tool_LINUX.h>

#elif (OS_ARCH==OS_DSPIC33)

#include <DSPIC33/Os_tool_DSPIC33.h>

#else
#error "Unsupported OS_ARCH defined. Check your Makefiles!"
#endif

#ifdef EB_STATIC_CHECK
/* Need declaration of OS_inKernel, see below. */
#include <Os_userkernel.h>
#endif

#ifndef EB_STATIC_CHECK

/* If the section attribute macro is not defined we define it to be:
 *    __attribute__((section("sect")))    for gnu, RealView and GHS
 *    empty                               for other toolchains
*/
#ifndef OS_SECTION_ATTRIB

#if ((OS_TOOL==OS_htgcc) || (OS_TOOL==OS_gnu) || (OS_TOOL==OS_realview) \
	 || (OS_TOOL==OS_ghs) || (OS_TOOL==OS_mplabx))
#define OS_SECTION_ATTRIB(sect)		__attribute__((section(#sect)))
#else
#define OS_SECTION_ATTRIB(sect)
#endif

#endif /* ! OS_SECTION_ATTRIB */

/* The OS_VAR_UNINITIALIZED_ATTRIB denotes explicitely that a variable
 * is not to be placed in an initialized data section.
 * Required for RealView 4.1 if a section name is specified: In this case
 * RealView will mark this section as ALLOC, CONTENTS, DATA, LOAD, which
 * means that space in the ROM is consumed. Using this attribute will
 * generate an ALLOC-only section.
 */
#ifndef OS_VAR_UNINITIALIZED_ATTRIB

#if (OS_TOOL==OS_realview)
#define OS_VAR_UNINITIALIZED_ATTRIB()	__attribute__((zero_init))
#else
#define OS_VAR_UNINITIALIZED_ATTRIB()
#endif /* (OS_TOOL==OS_realview) */

/* OS_STACK_ATTRIB: Add __attribute__s for stacks. By default, we mark stacks
 * as uninitialized data, but CPU-specific toolchain headers may override this
 * functionality.
 */
#ifndef OS_STACK_ATTRIB
#define OS_STACK_ATTRIB() OS_VAR_UNINITIALIZED_ATTRIB()
#endif

#endif /* ! OS_VAR_UNINITIALIZED_ATTRIB */

#else /* EB_STATIC_CHECK */

#define OS_SECTION_ATTRIB(sect)
#define OS_VAR_UNINITIALIZED_ATTRIB()
#define OS_STACK_ATTRIB() OS_VAR_UNINITIALIZED_ATTRIB()

#endif /* EB_STATIC_CHECK */

/* OS_PARAM_UNUSED(p) - mark parameter p as unused parameter (or variable).
 * Usage: Function-like.
 */
#ifndef OS_PARAM_UNUSED
#error "OS_PARAM_UNUSED must be defined by the toolchain header."
#endif


/* Macros to abstract memory region addresses.
 *
 * A memory region is formed from the following components:
 * - initialized data (VMA) with ROM-image (LMA) of initial values,
 * - uninitialized data (VMA).
 * Both components are optional, however data must always have lower addresses
 * than uninitialized data.
 *
 * The following symbols must be defined for memory regions:
 * __DATA_<region>: Start-VMA of of memory region.
 * __DEND_<region>: End-VMA of memory region.
 * __IDAT_<region>: Start LMA of initialized data portion.
 * __IEND_<region>: End LMA of initialized data portion.
 *
 * All of this is toolchain-specific, hence we want to exclude static C checks.
*/
#ifndef EB_STATIC_CHECK
/* Macro that evaluates to the name of the linker generated symbol referring
 * to the Start-VMA of memory region <region>.
 *
 * Note:  This is the default implementation, it may be changed in the toolchain header!
 */
#ifndef OS_TOOL_MR_START
#define OS_TOOL_MR_START(region) __DATA_ ## region
#endif

/* Macro that evaluates to the name of the linker generated symbol referring
 * to the virtual memory address one byte beyond memory region <region>.
 *
 * Note:  This is the default implementation, it may be changed in the toolchain header!
 */
#ifndef OS_TOOL_MR_END
#define OS_TOOL_MR_END(region) __DEND_ ## region
#endif

/* Macro that evaluates to the name of the linker generated symbol referring
 * to the LMA of the data portion of memory region <region>.
 *
 * Note:  This is the default implementation, it may be changed in the toolchain header!
 */
#ifndef OS_TOOL_MR_LMA_START
#define OS_TOOL_MR_LMA_START(region) __IDAT_ ## region
#endif


/* Macro that evaluates to the name of the linker generated symbol referring
 * to the LMA one byte beyond the initialized data portion of memory region
 * <region>.
 *
 * Note:  This is the default implementation, it may be changed in the toolchain header!
 */
#ifndef OS_TOOL_MR_LMA_END
#define OS_TOOL_MR_LMA_END(region) __IEND_ ## region
#endif

#else /* EB_STATIC_CHECK */
/* Evaluate to a value that the static checker understands.
 * Since the address of the (otherwise) linker-generated symbol is taken,
 * use a variable that is always present instead.
*/
#define OS_TOOL_MR_START(region)		(OS_inKernel)
#define OS_TOOL_MR_END(region)			(OS_inKernel)
#define OS_TOOL_MR_LMA_START(region)	(OS_inKernel)
#define OS_TOOL_MR_LMA_END(region)		(OS_inKernel)
#endif

#endif
/* Editor settings - DO NOT DELETE
 * vi:set ts=4:
*/
