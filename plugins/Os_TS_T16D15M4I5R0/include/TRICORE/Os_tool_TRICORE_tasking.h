/* Os_tool_TRICORE_tasking.h - Tricore macros for Tasking toolchain
 *
 * CHECK: TABS 4 (see editor commands at end of file)
 *
 * Copyright 1998-2014 Elektrobit Automotive GmbH
 * All rights exclusively reserved for Elektrobit Automotive GmbH,
 * unless expressly agreed to otherwise.
 *
 * $Id: Os_tool_TRICORE_tasking.h 18544 2014-07-30 04:50:39Z masa8317 $
*/

/*	This file contains macros (C and assembler) for use with Tricore
 *	and the Tasking compiler.
*/

#ifndef __OS_TOOL_TRICORE_TASKING_H
#define __OS_TOOL_TRICORE_TASKING_H

/* The Tasking compiler implements a read of a volatile variable incorrectly - it gets optimised away.
*/
#ifndef OS_ASM
extern volatile os_uint32_t OS_junk;

#define OS_ReadbackVolatile(x)	(OS_junk = (os_uint32_t)(x))
#endif

#ifndef OS_TRICORE_BF_00
#error "Include error: Silicon bug workarounds have not been defined"
#endif

#if OS_TRICORE_BF_05 && OS_TRICORE_BF_11
#error "BF_05 and BF_11 together are incompatible with the current workaround code."
#endif

/* Including the workaround for BF_11 is optional because it adds CUP cycles to every interrupt.
 *
 * It is included by default because of the danger, but a system designed can exclude
 * it if it is considered safe.
*/
#ifndef OS_INCLUDE_BF11_WORKAROUND
#define OS_INCLUDE_BF11_WORKAROUND	1
#endif

/* See comment in corresponding gnu header file */
#define OS__XSTR(s) OS__STR(s)
#define OS__STR(s) #s

/* OS_PARAM_UNUSED(p) - mark parameter p as unused parameter (or variable).
 * Usage: Function-like.
 *
 * Attention: When changing this implementation, ensure that the
 * resulting binary code does not differ (TRICORE has separate
 * data and address registers, so a non-fully optimizing compiler
 * might add a move from an address to a data register if the
 * parameter in question is a pointer).
 */
#define OS_PARAM_UNUSED(p) do { /* NOTHING */ } while (0)

#ifdef OS_ASM
/* CHECK: SAVE
 * CHECK: RULE 402 OFF - These are not C macros!
*/

/*	For Tasking we have to put a # operator before immediate values
 *	in the assembler. This results in a C preprocesor warning, which
 *	can be turned off for .s files.
*/
#define _IMM(p,x)	p x

/*	The Tasking assembler expects the register bit for instructions such as jz.t
 *	to be specified as (e.g.) d15:1
*/
#define _REGBIT(r,b)	r:b


/*	The Tasking assembler uses @his(x) and @los(x) to extract the
 *	high and low words from a literal.
*/
#define _hiword(x)	@his(x)
#define _loword(x)	@los(x)

/*	Tasking uses .global instead of .globl. .extern is OK
 *	.type is completely different :-(
*/
#define _GTYPE(s,t)
#define _TTYPE(s,t)	s	.type	t
#define	_GLOBAL		.global
#define _EXTERN		.extern

/*	Tasking assembler has no standard .text and .data sections so we have
 *	to explicitly declare them as well as switch to them.
*/
/* CHECK: NOPARSE */
	.sdecl	".text",code
	.sdecl	".data",data
/* CHECK: PARSE */

#define	_TEXT	.sect ".text"
#define _DATA	.sect ".data"

/*	The Tasking assembler uses absolute notation for alignment
*/
#define _align(n,p)		.align n


/* The Tasking assembler has the .half directive for 16-bit words */
#define _HWORD .half

/*	Some native assembler macros to redefine some op-codes
 *	safely. The macros are (mostly) named like the opcodes,
 *	with _ prepended. Simply converting to uppercase has no
 *	effect - opcodes are case-insensitive.
 *	The macro _mfcr simply maps to mfcr at the moment, but
 *	should be used in case there is ever the need to synchronise.
*/
/* CHECK: NOPARSE */
_dsync	.macro
		dsync
#if OS_TRICORE_BF_01==1
		nop
		nop
#endif
	.endm

_mtcr	.macro	creg,reg
		_dsync
		mtcr	creg,reg
		isync
	.endm

_mfcr	.macro	reg,creg
		mfcr	reg,creg
	.endm

_loop	.macro	areg,label
#if OS_TRICORE_BF_02==1
		isync
#endif
		loop	areg,label
	.endm

/*	The _entry macro goes at the top of every function and
 *	ISR.
*/
_entry	.macro
#if OS_TRICORE_BF_05==1
		_dsync
#endif
	.endm

/*	The _short macro goes into very short (no instructions, or
 *	only a single integer instruction) subroutines, to pad with
 *	a NOP of necessary
*/
_short	.macro
#if OS_TRICORE_BF_08==1
#if OS_TRICORE_BF_05==1
		nop
#endif
#endif
	.endm

/*	The _jinop macro goes between a ld.a and a ji instruction
 *	to pad with a nop if necessary.
*/
_jinop	.macro
#if OS_TRICORE_BF_03==1
		nop
#endif
	.endm

/*	The _dvloop macro goes between a dvstep and a loop
 *	to pad with a nop if necessary.
*/
_dvloop	.macro
#if OS_TRICORE_BF_09==1
		nop
#endif
	.endm

/*	The GenIntVector macro generates an interrupt vector with the
 *	specified symbol, BISR-level, isr-id, entry and exit functions.
*/
GenIntVector	.macro	name,bisrlvl,isrid,entry,exit
	.align	32
	.global	name
	.extern	entry
	.extern	exit
#if OS_TRICORE_BF_11 && OS_INCLUDE_BF11_WORKAROUND
	.extern	OS_Bf11Workaround
#endif
name:
#if OS_TRICORE_BF_11 && OS_INCLUDE_BF11_WORKAROUND
	_mfcr	d15,#OS_PCXI
	jnz.t	d15:OS_PCXI_PIE_BIT,name.c
	mov		d15,#isrid
	j		OS_Bf11Workaround
name.c:
#else
	_entry
#endif
	bisr	#bisrlvl
	mov		d4,#isrid
	call	entry
	j		exit
	.endm


/*	The DirectVector macro generates an interrupt vector which jumps directly to the
 *  entry. The entry needs to use the interrupt keyword.
 */
DirectVector	.macro	name,entry
	.align	32
	.global	name
	.extern	entry
name:
	j		entry
.endm

/* CHECK: PARSE */
/* CHECK: RESTORE */
#else

#ifdef __cplusplus
extern "C" {
#endif

#define OS_DEBUG_BREAK()	__asm(" debug")
#define OS_ISYNC()			__asm(" isync")
#define OS_DSYNC_UNSAFE()	__asm(" dsync")

#if OS_TRICORE_BF_01==1
#define OS_DSYNC()			__asm(" dsync \n nop \n nop \n")
#else
#define OS_DSYNC()			__asm(" dsync")
#endif

#define OS_NOP()			__asm(" nop")

#define OS_MFCR(csfr)		__mfcr(csfr)

#if OS_TRICORE_BF_01==1
#define OS_MTCR(csfr, val) \
	do {										\
		register os_int32_t mtcrTmp = (val);	\
		__asm(" dsync \n nop \n nop \n mtcr #"OS__XSTR(csfr)",%0" : : "d" (mtcrTmp));										\
		OS_ISYNC();								\
	} while (0)
#else
#define OS_MTCR(csfr, val) \
	do {																\
		register os_int32_t mtcrTmp = (val);							\
		__asm(" dsync \n mtcr #"OS__XSTR(csfr)",%0" : : "d" (mtcrTmp));	\
		OS_ISYNC();														\
	} while (0)
#endif

inline void *OS_MFRA(void)
{	register void *mfraRes;
	__asm("mov.aa %0,a11" : "=a" (mfraRes));
	return mfraRes;
}

inline void *OS_MFSP(void)
{	register void *mfspRes;
	__asm("mov.aa %0,sp" : "=a" (mfspRes));
	return mfspRes;
}

#define OS_MTSP(val) \
	do {											\
		register void *mtspTmp = (val);				\
		__asm("mov.aa sp,%0" : : "a" (mtspTmp));	\
	} while (0)

#define OS_MTRA(val) \
	do {													\
		register void *mtraTmp = (val);						\
		__asm volatile ("mov.aa a11,%0" : : "a" (mtraTmp));	\
	} while (0)

#define OS_MTD4(val) \
	do {													\
		register os_uint32_t mtd4Tmp = (val);				\
		__asm volatile ("mov d4,%0" : : "d" (mtd4Tmp));	\
	} while (0)

#define OS_DISABLE()	__disable()
#define OS_ENABLE()		__enable()

/* Macro to restore lower context
*/
#define OS_RSLCX()			__asm ("rslcx")

/* Macro to return from a JL instruction. We call it RFJL, but it is
 * really only a JI A11.
*/
#define OS_RFJL()			__asm ("ji a11")

/* Macro to return from interrupt.
*/
#define OS_RFE()			__asm ("rfe")

/* Inline assembler to count leading zeros in a word
*/
inline os_int_t OS_CLZ(os_uint32_t wurd)
{	register os_int_t OS_CLZ_nZeros;
	__asm volatile ("clz %0,%1" : "=d" (OS_CLZ_nZeros) : "d" (wurd));
	return OS_CLZ_nZeros;
}

#ifdef __cplusplus
}
#endif

#endif

#endif
/* Editor settings; DO NOT DELETE
 * vi:set ts=4:
*/
