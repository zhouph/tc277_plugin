/* Os_proto_TRICORE.h
 *
 * This file is the architecture-dependent prototype file for TRICORE
 *
 * The prototypes of kernel functions are in a separate file because they
 * are included in the Os_kernel.h file AFTER the generic types
 * have been defined, whereas the architecture-dependent types file is
 * included BEFORE the generic types are defined.
 *
 * CHECK: TABS 4 (see editor commands at end of file)
 *
 * Copyright 1998-2014 Elektrobit Automotive GmbH
 * All rights exclusively reserved for Elektrobit Automotive GmbH,
 * unless expressly agreed to otherwise.
 *
 * $Id: Os_proto_TRICORE.h 19542 2014-10-29 12:37:38Z thdr9337 $
*/
#ifndef __OS_PROTO_TRICORE_H
#define __OS_PROTO_TRICORE_H

#ifdef __cplusplus
extern "C" {
#endif

#include <Os_timestamp.h>

#ifndef EB_STATIC_CHECK

#if OS_TOOL==OS_tasking
/* Include tool-specific function prototypes. This is mostly for
 * inline functions that use generic data types and therefore cannot be
 * declared in the main toolchain-specific header. The file only exists
 * for tasking because with gnu we use macros
*/
#include <TRICORE/Os_proto_TRICORE_tasking.h>
#endif /* OS_TOOL */

#else /* EB_STATIC_CHECK */

/* dummy prototypes for static checker */
extern os_intstatus_t OS_IntEnable(void);
extern os_intstatus_t OS_IntDisable(void);
extern os_intstatus_t OS_IntDisableConditional(void);
extern os_intstatus_t OS_IntDisableAll(void);
extern void OS_IntRestore(os_intstatus_t);

#endif /* EB_STATIC_CHECK */

/* TRICORE kernel function prototypes
*/
#ifndef OS_ASM

void OS_StartTask(const os_task_t *);

void OS_TricoreCallIsr(const os_isr_t *, os_isrdynamic_t *);

void OS_Trap0Handler(void);
void OS_Trap1Handler(void);
void OS_Trap2Handler(void);
void OS_Trap3Handler(void);
void OS_Trap4Handler(void);
void OS_Trap5Handler(void);
void OS_Trap7Handler(void);

/* Function to initialise the local variables of SetProtection
*/
void OS_SetProtectionInit(void);

#endif

#ifdef __cplusplus
}
#endif

#endif
/* Editor settings - DO NOT DELETE
 * vi:set ts=4:
*/
