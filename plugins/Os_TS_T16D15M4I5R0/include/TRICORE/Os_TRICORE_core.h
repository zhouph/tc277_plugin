/* Os_TRICORE_core.h - Tricore core header
 *
 * This file contains definitions for features that are common across all
 * the Tricore CPUs, including the structure of context-save areas (CSAs)
 * and the addresses of the core special-function registers (CSFRs)
 *
 * CHECK: TABS 4 (see editor commands at end of file)
 *
 * Copyright 1998-2014 Elektrobit Automotive GmbH
 * All rights exclusively reserved for Elektrobit Automotive GmbH,
 * unless expressly agreed to otherwise.
 *
 * $Id: Os_TRICORE_core.h 17655 2014-02-07 08:52:02Z tojo2507 $
*/

#ifndef __OS_TRICORE_CORE_H
#define __OS_TRICORE_CORE_H

#include <Os_types.h>

#ifdef __cplusplus
extern "C" {
#endif

/* We check if the architecture is defined. If not (and for all the older derivates) we set it to 1.3.1
 * Normally the Os_<DERIVATE>.h file should set it. */

#ifndef OS_TRICOREARCH
#define OS_TRICOREARCH      OS_TRICOREARCH_131
#endif


/*! os_tricoremodule_t
 *
 * Data structure for peripheral module header block.
 * Most of the peripheral modules on Tricore contain one of these at the
 * start of their address space, and the module needs initialisation before
 * the device can be used. We can use common code for this.
 *
 * Some peripheral modules don't implement all the registers of the header.
 * Some peripheral modules have register in the 'reserved' location.
 * None of that is of concern to us here.
*/
#ifndef OS_ASM
typedef struct os_tricoremodule_s
{
	os_reg32_t		clc;		/* Clock control register */
	os_reg32_t		reserved;	/* Some modules use this! */
	os_reg32_t		id;			/* Module identification register */
} os_tricoremodule_t;

#endif

/* Bit definitions for CLC */
#define OS_CLC_RMC	OS_U(0x0000ff00)	/* Clock divider (Rate mode control?) */
#define OS_CLC_FSOE	OS_U(0x00000020)	/* Fast shut-off enable */
#define OS_CLC_SBWE	OS_U(0x00000010)	/* Suspend bit write enable */
#define OS_CLC_EDIS	OS_U(0x00000008)	/* External request disable */
#define OS_CLC_SPEN	OS_U(0x00000004)	/* Suspend enable bit */
#define OS_CLC_DISS	OS_U(0x00000002)	/* Disable status bit */
#define OS_CLC_DISR	OS_U(0x00000001)	/* Disable request bit */

/* Bit definitions for ID */
#define OS_ID_MOD	OS_U(0xffff0000)	/* Module identification number */
#define OS_ID_TYPE	OS_U(0x0000ff00)	/* Module type */
#define OS_ID_MOD16	OS_U(0x0000ff00)	/* Module id (used to be 16-bit register) */
#define OS_ID_MOD32	OS_U(0x0000c000)	/* Indicates 32-bit register */
#define OS_ID_REV	OS_U(0x000000ff)	/* Module revision number */


/*	Move an integer value to the appropriate field. */
#define OS_CLC_RMCval(div)	(	(os_uint32_t)							\
								( ((os_uint32_t)(div) & 0xffffu) << 8 )	\
							)

/*	Extract the module ident (shifted to LSB) using appropriate field,
 *	taking account of 16/32 bit register types.
*/
#define Os_ModuleId(v)	( (((v)&OS_ID_MOD16) == OS_ID_MOD32)	\
							? ( ((v)>>16) & OS_U(0xffff) )		\
							: ( ((v)>>8) & OS_U(0xff) )			\
						)

/*! os_tricoresrn_t
 *
 * Data type for interrupt service request node. All generation of
 * interrupts on Tricore is done through one of these SRN registers.
 * We can use common code to program the priority, enable/disable, etc.
*/
#ifndef OS_ASM
typedef os_reg32_t	 os_tricoresrn_t;
#endif

#if (OS_TRICOREARCH == OS_TRICOREARCH_16EP) /* AURIX TC2xx */

#if (OS_CPU == OS_TC277) /* AURIX TC277 */
#define OS_SRN_SETR		OS_U(0x04000000)	/* Request set bit */
#define OS_SRN_CLRR		OS_U(0x02000000)	/* Request clear bit */
#define OS_SRN_SRR		OS_U(0x01000000)	/* Service request bit */
/* The AURIX TC277 has only one DMA engine and three CPUs, thus at most four service providers. */
#define OS_SRN_TOS		OS_U(0x00001800)	/* Type of service */
#define OS_SRN_SRE		OS_U(0x00000400)	/* Service request enable */
#define	OS_SRN_SRPN		OS_U(0x000000ff)	/* Priority */

#ifndef OS_ASM
#define OS_SRN_TOS_CPU	OS_SRN_TOSval(OS_TricoreGetCoreId())	/* Interrupt the CPU */
#define OS_SRN_TOSval(tos)	(((tos) & 0x3u) << 11)
#endif

#else /* AURIX TC2Dx */

#define OS_SRN_SETR		OS_U(0x04000000)	/* Request set bit */
#define OS_SRN_CLRR		OS_U(0x02000000)	/* Request clear bit */
#define OS_SRN_SRR		OS_U(0x01000000)	/* Service request bit */
/* The AURIX TC2Dx chips have twice as many service providers, because of they have more DMA engines. */
#define OS_SRN_TOS		OS_U(0x00003800)	/* Type of service */
#define OS_SRN_SRE		OS_U(0x00000400)	/* Service request enable */
#define	OS_SRN_SRPN		OS_U(0x000000ff)	/* Priority */

#ifndef OS_ASM
#define OS_SRN_TOS_CPU	OS_SRN_TOSval(OS_TricoreGetCoreId())	/* Interrupt the CPU */
#define OS_SRN_TOSval(tos)	(((tos) & 0x7u) << 11)
#endif /* !OS_ASM */
#endif /* OS_CPU == OS_TC277 */

#else

/* Bit fields in the SRN */
#define OS_SRN_SETR		OS_U(0x00008000)	/* Request set bit */
#define OS_SRN_CLRR		OS_U(0x00004000)	/* Request clear bit */
#define OS_SRN_SRR		OS_U(0x00002000)	/* Service request bit */
#define OS_SRN_SRE		OS_U(0x00001000)	/* Service request enable */
#define OS_SRN_TOS		OS_U(0x00000c00)	/* Type of service */
#define	OS_SRN_SRPN		OS_U(0x000000ff)	/* Priority */

#ifndef OS_ASM
#define OS_SRN_TOS_CPU	OS_SRN_TOSval((os_uint32_t) 0u)	/* Interrupt the CPU */
#define OS_SRN_TOSval(tos)	((tos)<<10)
#endif /* !OS_ASM */
#endif /* OS_TRICOREARCH == OS_TRICOREARCH_16EP */

#ifndef OS_ASM

void OS_InitModule(os_tricoremodule_t *, os_uint32_t, os_uint32_t);

#endif

#ifndef OS_ASM

typedef struct os_uppercx_s
{
	os_reg32_t pcxi;	/* link word */
	os_reg32_t psw;		/* processor status word */
	os_reg32_t a10;		/* Stack pointer. */
	os_reg32_t a11;		/* Return address register. */

#if (OS_CPU == OS_RIDERA)
	/* The order changed from rider A to rider B */
	os_reg32_t a12;
	os_reg32_t a13;
	os_reg32_t a14;
	os_reg32_t a15;
	os_reg32_t d8;
	os_reg32_t d9;
	os_reg32_t d10;
	os_reg32_t d11;
#else
	os_reg32_t d8;
	os_reg32_t d9;
	os_reg32_t d10;
	os_reg32_t d11;
	os_reg32_t a12;
	os_reg32_t a13;
	os_reg32_t a14;
	os_reg32_t a15;
#endif
	os_reg32_t d12;
	os_reg32_t d13;
	os_reg32_t d14;
	os_reg32_t d15;
} os_uppercx_t;

typedef struct os_lowercx_s
{
	os_reg32_t pcxi;	/* link word */
	os_reg32_t a11;		/* return address register. */
	os_reg32_t a2;
	os_reg32_t a3;
#if (OS_CPU == OS_RIDERA)
	/* The order changed from rider A to rider B */
	os_reg32_t a4;
	os_reg32_t a5;
	os_reg32_t a6;
	os_reg32_t a7;
	os_reg32_t d0;
	os_reg32_t d1;
	os_reg32_t d2;
	os_reg32_t d3;
#else
	os_reg32_t d0;
	os_reg32_t d1;
	os_reg32_t d2;
	os_reg32_t d3;
	os_reg32_t a4;
	os_reg32_t a5;
	os_reg32_t a6;
	os_reg32_t a7;
#endif
	os_reg32_t d4;
	os_reg32_t d5;
	os_reg32_t d6;
	os_reg32_t d7;
} os_lowercx_t;

#endif

/*	Offsets of saved registers in CSAs.
 *	For use in assembler modules - should never be used in C
*/
#ifdef OS_ASM

#define UPPERCX_PCXI	0x00
#define UPPERCX_PSW		0x04
#define UPPERCX_A10		0x08
#define UPPERCX_A11		0x0c

#if (OS_CPU == OS_RIDERA)
#define UPPERCX_A12		0x10
#define UPPERCX_A13		0x14
#define UPPERCX_A14		0x18
#define UPPERCX_A15		0x1c
#define UPPERCX_D8		0x20
#define UPPERCX_D9		0x24
#define UPPERCX_D10		0x28
#define UPPERCX_D11		0x2c
#else
#define UPPERCX_D8		0x10
#define UPPERCX_D9		0x14
#define UPPERCX_D10		0x18
#define UPPERCX_D11		0x1c
#define UPPERCX_A12		0x20
#define UPPERCX_A13		0x24
#define UPPERCX_A14		0x28
#define UPPERCX_A15		0x2c
#endif
#define UPPERCX_D12		0x30
#define UPPERCX_D13		0x34
#define UPPERCX_D14		0x38
#define UPPERCX_D15		0x3c

#define LOWERCX_PCXI	0x00
#define LOWERCX_A11		0x04
#define LOWERCX_A2		0x08
#define LOWERCX_A3		0x0c
#if (OS_CPU == OS_RIDERA)
#define LOWERCX_A4		0x10
#define LOWERCX_A5		0x14
#define LOWERCX_A6		0x18
#define LOWERCX_A7		0x1c
#define LOWERCX_D0		0x20
#define LOWERCX_D1		0x24
#define LOWERCX_D2		0x28
#define LOWERCX_D3		0x2c
#else
#define LOWERCX_D0		0x10
#define LOWERCX_D1		0x14
#define LOWERCX_D2		0x18
#define LOWERCX_D3		0x1c
#define LOWERCX_A4		0x20
#define LOWERCX_A5		0x24
#define LOWERCX_A6		0x28
#define LOWERCX_A7		0x2c
#endif
#define LOWERCX_D4		0x30
#define LOWERCX_D5		0x34
#define LOWERCX_D6		0x38
#define LOWERCX_D7		0x3c

#endif

/*	CSFR offsets for Tricore Rider B and D. These offsets can be used
 *	with the mtcr/mfcr instructions, or can be added to a base address
 *	to get the absolute address for accesses via the bus.
 *
 *	For Rider B, the registers DCX and DMS are not documented.
 *	For Rider D, the registers DBITEN and GPRWB are not documented.
 *
*/

#if (OS_TRICOREARCH == OS_TRICOREARCH_16EP)
#define	OS_DPR0_L	0xc000	/* Data protection register 0, lower */
#define	OS_DPR0_U	0xc004	/* Data protection register 0, upper */
#define	OS_DPR1_L	0xc008	/* Data protection register 1, lower */
#define	OS_DPR1_U	0xc00c	/* Data protection register 1, upper */
#define	OS_DPR2_L	0xc010	/* Data protection register 2, lower */
#define	OS_DPR2_U	0xc014	/* Data protection register 2, upper */
#define	OS_DPR3_L	0xc018	/* Data protection register 3, lower */
#define	OS_DPR3_U	0xc01c	/* Data protection register 3, upper */
#define	OS_DPR4_L	0xc020	/* Data protection register 0, lower */
#define	OS_DPR4_U	0xc024	/* Data protection register 0, upper */
#define	OS_CPR0_L	0xd000	/* Code protection register 0, lower */
#define	OS_CPR0_U	0xd004	/* Code protection register 0, upper */
#define	OS_CPR1_L	0xd008	/* Code protection register 0, lower */
#define	OS_CPR1_U	0xd00c	/* Code protection register 0, upper */
#define OS_DPRE_0   0xe010  /* Data protection read enable set config register 0 */
#define OS_DPRE_1   0xe014  /* Data protection read enable set config register 1 */
#define OS_DPWE_0   0xe020  /* Data protection write enable set config register 0 */
#define OS_DPWE_1   0xe024  /* Data protection write enable set config register 1 */
#define OS_CPXE_0   0xe000  /* Code protection execute enable set config register 0 */
#define OS_CPXE_1   0xe004  /* Code protection execute enable set config register 1 */
#else
#define	OS_DPR0_0L	0xc000	/* Data protection register 0, set 0, lower */
#define	OS_DPR0_0U	0xc004	/* Data protection register 0, set 0, upper */
#define	OS_DPR0_1L	0xc008	/* Data protection register 1, set 0, lower */
#define	OS_DPR0_1U	0xc00c	/* Data protection register 1, set 0, upper */
#define	OS_DPR0_2L	0xc010	/* Data protection register 2, set 0, lower */
#define	OS_DPR0_2U	0xc014	/* Data protection register 2, set 0, upper */
#define	OS_DPR0_3L	0xc018	/* Data protection register 3, set 0, lower */
#define	OS_DPR0_3U	0xc01c	/* Data protection register 3, set 0, upper */
#define	OS_DPR1_0L	0xc400	/* Data protection register 0, set 1, lower */
#define	OS_DPR1_0U	0xc404	/* Data protection register 0, set 1, upper */
#define	OS_DPR1_1L	0xc408	/* Data protection register 1, set 1, lower */
#define	OS_DPR1_1U	0xc40c	/* Data protection register 1, set 1, upper */
#define	OS_DPR1_2L	0xc410	/* Data protection register 2, set 1, lower */
#define	OS_DPR1_2U	0xc414	/* Data protection register 2, set 1, upper */
#define	OS_DPR1_3L	0xc418	/* Data protection register 3, set 1, lower */
#define	OS_DPR1_3U	0xc41c	/* Data protection register 3, set 1, upper */
#define	OS_CPR0_0L	0xd000	/* Code protection register 0, set 0, lower */
#define	OS_CPR0_0U	0xd004	/* Code protection register 0, set 0, upper */
#define	OS_CPR0_1L	0xd008	/* Code protection register 1, set 0, lower */
#define	OS_CPR0_1U	0xd00c	/* Code protection register 1, set 0, upper */
#define	OS_CPR1_0L	0xd400	/* Code protection register 0, set 1, lower */
#define	OS_CPR1_0U	0xd404	/* Code protection register 0, set 1, upper */
#define	OS_CPR1_1L	0xd408	/* Code protection register 1, set 1, lower */
#define	OS_CPR1_1U	0xd40c	/* Code protection register 1, set 1, upper */
#define	OS_DPM0		0xe000	/* Data prot. mode regs, set 0 */
#define	OS_DPM1		0xe080	/* Data prot. mode regs, set 1 */
#define	OS_CPM0		0xe200	/* Code prot. mode regs, set 0 */
#define	OS_CPM1		0xe280	/* Code prot. mode regs, set 1 */
/* Bit definitions for DPMn.
 * Only the read/write enable bits are defined here.
*/
#define OS_DPM_WE3			OS_U(0x80000000)
#define OS_DPM_RE3			OS_U(0x40000000)
#define OS_DPM_WE2			OS_U(0x00800000)
#define OS_DPM_RE2			OS_U(0x00400000)
#define OS_DPM_WE1			OS_U(0x00008000)
#define OS_DPM_RE1			OS_U(0x00004000)
#define OS_DPM_WE0			OS_U(0x00000080)
#define OS_DPM_RE0			OS_U(0x00000040)
/* Bit definitions for CPMn.
 * Only the execute enable bits are defined here.
*/
#define OS_CPM_XE1			OS_U(0x00008000)
#define OS_CPM_XE0			OS_U(0x00000080)
#endif

#define	OS_DBGSR	0xfd00	/* Debug status register */
#define	OS_GPRWB	0xfd04	/* GPR write back trigger */
#define	OS_EXEVT	0xfd08	/* External break input event specifier */
#define	OS_CREVT	0xfd0c	/* Emulator resource protection event specifier */
#define	OS_SWEVT	0xfd10	/* Software break event specifier */
#define	OS_TR0EVT	0xfd20	/* Trigger event 0 specifier */
#define	OS_TR1EVT	0xfd24	/* Trigger event 1 specifier */

#define OS_DMS		0xfd40	/* Debug monitor start address register */
#define OS_DCX		0xfd44	/* Debug context save area pointer */

#define	OS_PCXI		0xfe00	/* Previous context information register */
#define	OS_PSW		0xfe04	/* Program status word */
#define	OS_PC		0xfe08	/* Program counter */
#define	OS_DBITEN	0xfe0c	/* Register bank dual bit enable register */
#define	OS_SYSCON	0xfe14	/* System configuration register */
#define	OS_BIV		0xfe20	/* Base address of interrupt vector table */
#define	OS_BTV		0xfe24	/* Base address of trap vector table */
#define	OS_ISP		0xfe28	/* Interrupt stack pointer */
#define	OS_ICR		0xfe2c	/* Interrupt unit control register */
#define	OS_FCX		0xfe38	/* Free CSA list head pointer */
#define	OS_LCX		0xfe3c	/* Free CSA list limit pointer */

#if (OS_TRICOREARCH == OS_TRICOREARCH_16EP)
#define OS_CORE_ID  0xfe1c  /* core id register */
#endif

/* Bit definitions for PSW */
#define	OS_PSW_C			OS_U(0x80000000)		/* Carry */
#define	OS_PSW_V			OS_U(0x40000000)		/* Overflow */
#define OS_PSW_SV			OS_U(0x20000000)		/* Sticky overflow */
#define OS_PSW_AV			OS_U(0x10000000)		/* Advanced overflow */
#define OS_PSW_SAV			OS_U(0x08000000)		/* Sticky advanced overflow */
#define OS_PSW_PRS			OS_U(0x00003000)		/* Protection register set */
#define OS_PSW_PRS_0		OS_U(0x00000000)		/*	--- Protection register set 1 */
#define OS_PSW_PRS_1		OS_U(0x00001000)		/*	--- Protection register set 2 */
#define OS_PSW_PRS_2		OS_U(0x00002000)		/*	--- Protection register set 3 */
#define OS_PSW_PRS_3		OS_U(0x00003000)		/*	--- Protection register set 4 */
#define OS_PSW_IO			OS_U(0x00000C00)		/* I/O privelege mode. */
#define OS_PSW_IO_U0		OS_U(0x00000000)		/*	--- User 0 */
#define OS_PSW_IO_U1		OS_U(0x00000400)		/*	--- User 1 */
#define OS_PSW_IO_SU		OS_U(0x00000800)		/*	--- Supervisor */
#define OS_PSW_IO_RES		OS_U(0x00000C00)		/*	--- Reserved */
#define OS_PSW_IS			OS_U(0x00000200)		/* Interrupt stack in use */
#define OS_PSW_GW			OS_U(0x00000100)		/* Global register write perm. */
#define OS_PSW_CDE			OS_U(0x00000080)		/* Call depth count enable */
#define OS_PSW_CDC			OS_U(0x0000007F)		/* Call depth counter */
#define OS_PSW_CDC_DIS		OS_U(0x0000007F)		/*	--- Disable call depth ctr */
#define OS_PSW_CDC_6		OS_U(0x00000000)		/*	--- 6-bit call depth counter */
#define OS_PSW_CDC_5		OS_U(0x00000040)		/*	--- 5-bit call depth counter */
#define OS_PSW_CDC_4		OS_U(0x00000060)		/*	--- 4-bit call depth counter */
#define OS_PSW_CDC_3		OS_U(0x00000070)		/*	--- 3-bit call depth counter */
#define OS_PSW_CDC_2		OS_U(0x00000078)		/*	--- 2-bit call depth counter */
#define OS_PSW_CDC_1		OS_U(0x0000007C)		/*	--- 1-bit call depth counter */
#define OS_PSW_CDC_TRA		OS_U(0x0000007E)		/*	--- Call trace */

/*	Bit definitions for all context pointer registers (PCXI,FCX,LCX)
*/
#define OS_CX_SEG			OS_U(0x000F0000)		/* Segment number of CSA. */
#define OS_CX_OFF			OS_U(0x0000FFFF)		/* Index of CSA. */
#define OS_CX_CX			(OS_CX_SEG|OS_CX_OFF)	/* Entire context */

/*	Additiional bit definitions for PCXI. */


#if (OS_TRICOREARCH == OS_TRICOREARCH_16EP)
#define OS_PCXI_PCPN		OS_U(0xFFC00000)	/* Previous CPU priority. */
#define OS_PCXI_PCPN_BIT	22					/* Start bit of PCPN. */
#define OS_PCXI_PIE			OS_U(0x00200000)	/* Previous interrupt enable. */
#define OS_PCXI_PIE_BIT		21					/* Previous interrupt enable (bit number for j(n)z.t instructions) */
#define OS_PCXI_UL			OS_U(0x00100000)	/* Upper/lower context flag. */
#else
#define OS_PCXI_PCPN		OS_U(0xFF000000)	/* Previous CPU priority. */
#define OS_PCXI_PCPN_BIT	24					/* Start bit of PCPN. */
#define OS_PCXI_PIE			OS_U(0x00800000)	/* Previous interrupt enable. */
#define OS_PCXI_PIE_BIT		23					/* Previous interrupt enable (bit number for j(n)z.t instructions) */
#define OS_PCXI_UL			OS_U(0x00400000)	/* Upper/lower context flag. */
#endif

/* Bit definitions for ICR. */

#if (OS_TRICOREARCH == OS_TRICOREARCH_16EP)
#define OS_ICR_IE			OS_U(0x00008000)	/* Interrupt enable. */
#define	OS_ICR_CONEC		OS_U(0)				/* no longer present in 1.6E/P */
#define OS_ICR_CARBCYC		OS_U(0)				/* no longer present in 1.6E/P */
#else
#define OS_ICR_IE			OS_U(0x00000100)	/* Interrupt enable. */
#define	OS_ICR_CONEC		OS_U(0x04000000)	/* CPU cycles per arb. cycle */
#define OS_ICR_CONEC_1		OS_U(0x04000000)	/*	--- 1 clock cycle */
#define OS_ICR_CONEC_2		OS_U(0x00000000)	/*	--- 2 clock cycles */
#define OS_ICR_CARBCYC		OS_U(0x03000000)	/* No. of arbitration cycles */
#define OS_ICR_CARBCYC_4	OS_U(0x00000000)	/*	--- 4 --> interrupts 1..255 */
#define OS_ICR_CARBCYC_3	OS_U(0x01000000)	/*	--- 3 --> interrupts 1..63 */
#define OS_ICR_CARBCYC_2	OS_U(0x02000000)	/*	--- 2 --> interrupts 1..15 */
#define OS_ICR_CARBCYC_1	OS_U(0x03000000)	/*	--- 1 --> interrupts 1..3 */
#endif

#define OS_ICR_PIPN			OS_U(0x00FF0000)	/* Pending interrupt priority */
#define OS_ICR_CCPN			OS_U(0x000000FF)	/* Current CPU priority number. */

/* OS_ICR_ARBC2CARBC - this macro converts no. of arbitration cycles (1..4) to
 * the CARBCYC bits (above).
*/
#if (OS_TRICOREARCH == OS_TRICOREARCH_16EP)
#define OS_ICR_ARBC2CARBC(x)	OS_U(0) /* no longer present in 1.6E/P */
#else
#define OS_ICR_ARBC2CARBC(x)	((4-(x))<<24)
#endif

#define OS_MAXARBCYCLES		4

/* Bit definitions for SYSCON. */
#define OS_SYSCON_PROTEN	OS_U(0x00000002)		/* Enable memory protection. */
#define OS_SYSCON_ENDINIT	OS_U(0x00000001)		/* End of initialisation. */


#ifndef OS_ASM

void OS_FreeCxList(os_uint32_t);
void OS_FreeCxListTo(os_uint32_t, os_uint32_t);
os_uint32_t OS_Grab1Csa(void);
os_uint32_t OS_Grab2Csa(void);

void OS_SetPr0(os_uint32_t, const os_uint32_t *, os_uint32_t, const os_uint32_t *);
void OS_SetPr1(os_uint32_t, const os_uint32_t *, os_uint32_t, const os_uint32_t *);

os_uint32_t OS_TricoreGetCoreId(void);

#endif

/* OS_AddrFromCx() converts a CSA link word to an address.
 * No test is made for validity!
*/
#define OS_AddrFromCx(cx) \
	((void *)( (((cx) & OS_CX_SEG) << 12) + (((cx) & OS_CX_OFF) << 6) ))

/* OS_CxFromAddr() converts an address to a CSA link word.
 * No test is made for validity!
*/
#define OS_CxFromAddr(a) \
	( (((a) >> 6) & OS_CX_OFF) +  (((a) >> 12) & OS_CX_SEG) )

#ifdef __cplusplus
}
#endif

#endif

/* Editor settings; DO NOT DELETE
 * vi:set ts=4:
*/
