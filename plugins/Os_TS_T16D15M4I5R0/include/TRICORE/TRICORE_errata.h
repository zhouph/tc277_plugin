/* TRICORE_errata.h - documentation of supported errata workarounds.
 *
 * This file is for documentation purposes only. It provides a list of all
 * the relevant errata whose workarounds have been implelmented in the source
 * code.
 *
 * The actual errata workarounds that are necessary for a particular variant
 * are defined in the appropriate derivative header file.
 *
 * CHECK: TABS 4 (see editor commands at end of file)
 *
 * Copyright 1998-2014 Elektrobit Automotive GmbH
 * All rights exclusively reserved for Elektrobit Automotive GmbH,
 * unless expressly agreed to otherwise.
 *
 * $Id: TRICORE_errata.h 17602 2014-02-03 12:09:35Z tojo2507 $
*/
#ifndef TRICORE_ERRATA_H
#define TRICORE_ERRATA_H

#error "This file is for documentation only. It should never be used"

/*	This file documents which errata-sheet workarounds have to be used
 *	for the different Tricore core architectures and CPU variants.
 *
 *	Infineon claim that their errata numbering covers all Tricore
 *	variants, but there's at least one CPU problem number that's
 *	used twice. So we define our own bugfix numbering scheme and
 *	give information to map this onto the Infineon scheme. Our
 *	bug-fixes are called OS_TRICORE_BF_xx (xx = 00, 01, ...)
 *
 *	The following errata have software workarounds:
 *
 *	OS_TRICORE_BF_00
 *		CPU8 (TC1775B, TC10GP)
 *		If an interrupt occurs during any divide instruction, the
 *		return address can be corrupted.  The solution is to disable
 *		interrupts during sequences of DVSTEPs and related instructions.
 *		However, this doesn't help against NMI :-(
 *
 *	OS_TRICORE_BF_01
 *		CPU9 (All cores?)
 *		DSYNC instructions must be followed by 2 NOP instructions.
 *		Do not use the assembler workaround for this problem - it
 *		causes the assembler to blindly insert 2 NOPs after every
 *		DSYNC, which can cause chaos in some places.
 *
 *	OS_TRICORE_BF_02
 *		CPU10 (TC1775B, TC10GP)
 *		The LOOP instruction sometimes jumps to undefined addresses.
 *		The workaround is to insert an ISYNC before the LOOP.
 *
 *	OS_TRICORE_BF_03
 *		CPU11 (RiderD, TC1920A, TC1796)
 *		CPU_TC.048.1 (TC1796)
 *		A JI Ax instruction must not immediately follow a LD.A Ax
 *		instruction. A NOP between the two prevents problems.
 *
 *	OS_TRICORE_BF_04
 *		CPU12 (TC1775B)
 *		CPU_T.048.1 (TC1796)
 *		A JL/CALL etc to a location that immediately uses the link
 *		register could cause the wrong value to be used.
 *
 *	OS_TRICORE_BF_05
 *		CPU13 (RiderD, TC1920A)
 *		CPU14 (RiderD, TC1920A)
 *		A DSYNC instruction is required at the top of every subroutine
 *		and interrupt handler. The DSYNC might need NOPs too - see CPU9.
 *
 *	OS_TRICORE_BF_06
 *		CPU13 (TC1775B)
 *		If the last location of the PMU SRAM contains a
 *		long jump forward, a bus error occurs. Don't use the
 *		last location of the PMU SRAM.
 *
 *	OS_TRICORE_BF_07
 *		CPU14 (TC1775B)
 *		CALLI A11 doesn't work, so don't use it.
 *
 *	OS_TRICORE_BF_08
 *		CPU16 (RiderD, TC1920A)
 *		CPU_T.048.2 (TC1796)
 *		Similar to CPU11. A subroutine must contain at least 2 integer
 *		instructions or at least 1 non-integer instruction.
 *
 *	OS_TRICORE_BF_09
 *		CPU18 (TC1775B)
 *		DVSTEP followed by LOOP doesn't work. Put a NOP in between.
 *
 *	OS_TRICORE_BF_10
 *		STM_TC.002 (TC1796)
 *		Mapping between SRCn and IRn is twisted. SRC0 controls IR1 and v.v.
 *		This is only present on A-step CPUs. To avoid having a new architecture
 *		the problem is auto-detected by reading the STM's revision from its
 *		module ID register.
 *
 *	OS_TRICORE_BF_11
 *		CPU_TC.113 (TC1797,TC1767)
 *		If an interrupt is arbitrated just as a trap occurs, the interrupt can
 *		break into the trap handler before the first instruction, thus delaying
 *		handling of the trap.
*/

/*	A list of all bug-fixes, just for documentation,
 *	and so that it's easier to add new CPU variants. Just
 *	copy the whole list and delete those you don't want!
*/
#define OS_TRICORE_BF_00	1	/* Don't allow DVSTEP to be interrupted */
#define OS_TRICORE_BF_01	1	/* 2 x NOP after DSYNC */
#define OS_TRICORE_BF_02	1	/* ISYNC before LOOP */
#define OS_TRICORE_BF_03	1	/* NOP between LD.A Ax and JI Ax */
#define OS_TRICORE_BF_04	1	/* Don't use A11 1st instr. in function */
#define OS_TRICORE_BF_05	1	/* DSYNC at top of every function/ISR */
#define OS_TRICORE_BF_06	1	/* Don't use last PMU SRAM location */
#define OS_TRICORE_BF_07	1	/* Don't use CALLI A11 */
#define OS_TRICORE_BF_08	1	/* Subroutine must have 2 int or 1 non-int instr.*/
#define OS_TRICORE_BF_09	1	/* Put NOP between DVSTEP and LOOP */
#define OS_TRICORE_BF_10	1	/* STM interrupt mapping twisted */
#define OS_TRICORE_BF_11	1	/* Trap can be interrupted before 1st instruction */

/* The following sets of workarounds are for known derivatives that do not
 * (yet) have derivative description files
*/
#if OS_CPU == OS_PXD4260

#define OS_TRICORE_BF_00	1	/* Don't allow DVSTEP to be interrupted */
#define OS_TRICORE_BF_01	1	/* 2 x NOP after DSYNC */
#define OS_TRICORE_BF_02	1	/* ISYNC before LOOP */
#define OS_TRICORE_BF_03	0	/* NOP between LD.A Ax and JI Ax */
#define OS_TRICORE_BF_04	0	/* Don't use A11 1st instr. in function */
#define OS_TRICORE_BF_05	0	/* DSYNC at top of every function/ISR */
#define OS_TRICORE_BF_06	0	/* Don't use last PMU SRAM location */
#define OS_TRICORE_BF_07	0	/* Don't use CALLI A11 */
#define OS_TRICORE_BF_08	0	/* Subroutine must have 2 int or 1 non-int instr.*/
#define OS_TRICORE_BF_09	0	/* Put NOP between DVSTEP and LOOP */
#define OS_TRICORE_BF_10	0	/* STM interrupt mapping twisted */
#define OS_TRICORE_BF_11	0	/* Trap can be interrupted before 1st instruction */

#endif

#if OS_CPU == OS_RIDERD

#define OS_TRICORE_BF_00	1	/* Don't allow DVSTEP to be interrupted */
#define OS_TRICORE_BF_01	1	/* 2 x NOP after DSYNC */
#define OS_TRICORE_BF_02	1	/* ISYNC before LOOP */
#define OS_TRICORE_BF_03	1	/* NOP between LD.A Ax and JI Ax */
#define OS_TRICORE_BF_04	0	/* Don't use A11 1st instr. in function */
#define OS_TRICORE_BF_05	1	/* DSYNC at top of every function/ISR */
#define OS_TRICORE_BF_06	0	/* Don't use last PMU SRAM location */
#define OS_TRICORE_BF_07	0	/* Don't use CALLI A11 */
#define OS_TRICORE_BF_08	1	/* Subroutine must have 2 int or 1 non-int instr.*/
#define OS_TRICORE_BF_09	0	/* Put NOP between DVSTEP and LOOP */
#define OS_TRICORE_BF_10	0	/* STM interrupt mapping twisted */
#define OS_TRICORE_BF_11	0	/* Trap can be interrupted before 1st instruction */

#endif

#endif
/* Editor settings; DO NOT DELETE
 * vi:set ts=4:
*/
