/* Os_TRICORE_stmconfig.h - Configuration of the STM
 *
 * This file will be included by board.h. The following macros must be defined
 * in order to configure the timers:
 *
 *  OS_STM_PRESCALE	- the desired STM prescaler value (1..7) as programmed into the RMC field in STM_CLC
 *  OS_STM_CMPPOS0	- compare region for STM_T0 (0..24) as programmed into the MSTART0 field in STM_CMCON
 *  OS_STM_CMPPOS1	- compare region for STM_T1 (0..24) as programmed into the MSTART1 field in STM_CMCON
 *
 * CHECK: TABS 4 (see editor commands at end of file)
 *
 * Copyright 1998-2014 Elektrobit Automotive GmbH
 * All rights exclusively reserved for Elektrobit Automotive GmbH,
 * unless expressly agreed to otherwise.
 *
 * $Id: Os_TRICORE_stmconfig.h 19579 2014-10-30 16:43:50Z thdr9337 $
*/

#ifndef __OS_TRICORE_STMCONFIG_H
#define __OS_TRICORE_STMCONFIG_H

/* First, some default values. If nothing else is defined we assume that the
 * STM runs at the module frequency and that the comparators compare with
 * the bottom 32 bits of the STM.
*/
#ifndef OS_STM_PRESCALE
#define OS_STM_PRESCALE	1
#endif


#if !((OS_CPU == OS_TC27XFPGA) || (OS_CPU == OS_TC2D5) || (OS_CPU == OS_TC277))

#ifndef OS_STM_CMPPOS0
#define OS_STM_CMPPOS0		0
#endif

#ifndef OS_STM_CMPPOS1
#define OS_STM_CMPPOS1		0
#endif

#endif


/* Here we define the conversion macros for the individual HW-timers that can
 * be selected in the OIL file. These are defined using the global helper
 * macro, supplying the MSTARTx value for the comparator.
 * If the prescaler macro is not defined, we don't define the conversion macros.
 * If the prescaler is out of range we emit an error message.
 * For timers 1 and 3, if the prescaler is 7 we silently ignore it.
*/
#if (OS_CPU == OS_TC27XFPGA) || (OS_CPU == OS_TC2D5) || (OS_CPU == OS_TC277) || (OS_CPU == OS_TC23XL) \
 || (OS_CPU == OS_TC22XL)

#define OS_NsToTicks_STM0_T0(ns)	(OS_NsToTicks_STM(ns))
#define OS_TicksToNs_STM0_T0(tx)	(OS_TicksToNs_STM(tx))

#define OS_NsToTicks_STM0_T1(ns)	(OS_NsToTicks_STM(ns))
#define OS_TicksToNs_STM0_T1(tx)	(OS_TicksToNs_STM(tx))

#if (OS_CPU != OS_TC23XL) && (OS_CPU != OS_TC22XL)
#define OS_NsToTicks_STM1_T0(ns)	(OS_NsToTicks_STM(ns))
#define OS_TicksToNs_STM1_T0(tx)	(OS_TicksToNs_STM(tx))
#define OS_NsToTicks_STM2_T0(ns)	(OS_NsToTicks_STM(ns))
#define OS_TicksToNs_STM2_T0(tx)	(OS_TicksToNs_STM(tx))

#define OS_NsToTicks_STM1_T1(ns)	(OS_NsToTicks_STM(ns))
#define OS_TicksToNs_STM1_T1(tx)	(OS_TicksToNs_STM(tx))
#define OS_NsToTicks_STM2_T1(ns)	(OS_NsToTicks_STM(ns))
#define OS_TicksToNs_STM2_T1(tx)	(OS_TicksToNs_STM(tx))
#endif

#else

#if (OS_STM_CMPPOS0 >= 0) && (OS_STM_CMPPOS0 <= 24)
#define OS_NsToTicks_STM_T0(ns)	(OS_NsToTicks_STM(ns)/(1 << OS_STM_CMPPOS0))
#define OS_TicksToNs_STM_T0(tx)	(OS_TicksToNs_STM(tx)*(1 << OS_STM_CMPPOS0))
#else
#error "Invalid/unsupported value for OS_STM_CMPPOS0"
#endif

#if (OS_STM_CMPPOS1 >= 0) && (OS_STM_CMPPOS1 <= 24)
#define OS_NsToTicks_STM_T1(ns)	(OS_NsToTicks_STM(ns)/(1 << OS_STM_CMPPOS1))
#define OS_TicksToNs_STM_T1(tx)	(OS_TicksToNs_STM(tx)*(1 << OS_STM_CMPPOS1))
#else
#error "Invalid/unsupported value for OS_STM_CMPPOS1"
#endif
#endif

/*
 * Conversion macros for the Timebase Timer
 */
#define OS_NsToTicks_TbTimer(ns)	OS_NsToTicks_STM(ns)
#define OS_TicksToNs_TbTimer(ticks)	OS_TicksToNs_STM(ticks)

/* This macro converts ticks to nanoseconds and vice versa for the STM's
 * master clock (Fmod divided by RMC)
*/
#if (OS_STM_PRESCALE >= 1) && (OS_STM_PRESCALE <= 7)
#define OS_NsToTicks_STM(ns)	(OS_BoardNsToTicks(ns)/(OS_STM_PRESCALE))
#define OS_TicksToNs_STM(tx)	(OS_BoardTicksToNs(tx)*(OS_STM_PRESCALE))
#else
#error "Invalid/unsupported value for OS_STM_PRESCALE"
#endif

#endif
/* Editor settings: DO NOT DELETE
 * vi: set ts=4:
*/
