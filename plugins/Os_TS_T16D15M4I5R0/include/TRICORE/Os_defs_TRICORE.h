/* Os_defs_TRICORE.h - Tricore definitions for derivative selection etc.
 *
 * CHECK: TABS 4 (see editor commands at end of file)
 *
 * Copyright 1998-2014 Elektrobit Automotive GmbH
 * All rights exclusively reserved for Elektrobit Automotive GmbH,
 * unless expressly agreed to otherwise.
 *
 * $Id: Os_defs_TRICORE.h 19525 2014-10-29 09:12:57Z thdr9337 $
*/

#ifndef __OS_DEFS_TRICORE_H
#define __OS_DEFS_TRICORE_H

/* CPUs. OS_CPU must be one of these.
 *
 * CAVEAT:
 * The existence of a TRICORE derivative in this list does not imply a
 * commitment or intention to support the derivative.
*/
#define OS_RIDERA	0x000

#define OS_TC10GP	0x001
#define OS_PXD4260	0x002	/* "Harrier" */
#define OS_TC11IB	0x003
#define OS_TC1775	0x004
#define OS_TC1765	0x005

#define OS_RIDERD	0x101
#define OS_TC1920	0x102
#define OS_TC1910	0x103
#define OS_TC1912	0x104
#define OS_TC1796	0x105
#define OS_TC1766	0x106
#define OS_TC1797	0x107
#define OS_TC1767	0x108
#define OS_TC1387	0x109
#define OS_TC1782	0x10a
#define OS_TC1798	0x10b
#define OS_TC1791	0x10c
#define OS_TC1793	0x10d

#define OS_TC27XFPGA 0x200
#define OS_TC2D5     0x201
#define OS_TC277	 0x202
#define OS_TC23XL    0x203
#define OS_TC22XL    0x204

/* Architectures of the TriCore familiy */

#define OS_TRICOREARCH_13         0x01
#define OS_TRICOREARCH_131        0x02
#define OS_TRICOREARCH_16         0x03
#define OS_TRICOREARCH_16EP       0x04  /* Aurix */

#endif
/* Editor settings; DO NOT DELETE
 * vi:set ts=4:
*/
