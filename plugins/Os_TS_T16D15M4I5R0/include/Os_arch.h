/* Os_arch.h
 *
 * This file includes the appropriate architecture include file depending
 * on the chosen architecture.
 *
 * See also: Os_defs.h
 *
 * CHECK: TABS 4 (see editor commands at end of file)
 *
 * Copyright 1998-2014 Elektrobit Automotive GmbH
 * All rights exclusively reserved for Elektrobit Automotive GmbH,
 * unless expressly agreed to otherwise.
 *
 * $Id: Os_arch.h 18018 2014-04-22 14:01:46Z stpo8218 $
*/
#ifndef __OS_ARCH_H
#define __OS_ARCH_H

#include <Os_defs.h>

#if (OS_ARCH==OS_PA)
#include <PA/Os_kernel_PA.h>
#elif (OS_ARCH==OS_TRICORE)
#include <TRICORE/Os_kernel_TRICORE.h>
#elif (OS_ARCH==OS_V850)
#include <V850/Os_kernel_V850.h>
#elif (OS_ARCH==OS_RH850)
#include <RH850/Os_kernel_RH850.h>
#elif (OS_ARCH==OS_NEWARCH)
#include <NEWARCH/Os_kernel_NEWARCH.h>
#elif (OS_ARCH==OS_PIKEOS)
#include <PIKEOS/Os_kernel_PIKEOS.h>
#elif (OS_ARCH==OS_S12X)
#include <S12X/Os_kernel_S12X.h>
#elif (OS_ARCH==OS_XC2000)
#include <XC2000/Os_kernel_XC2000.h>
#elif (OS_ARCH==OS_EASYCAN)
#include <EASYCAN/Os_kernel_EASYCAN.h>
#elif (OS_ARCH==OS_MB91)
#include <MB91/Os_kernel_MB91.h>
#elif (OS_ARCH==OS_R32C)
#include <R32C/Os_kernel_R32C.h>
#elif (OS_ARCH==OS_WINDOWS)
#include <WINDOWS/Os_kernel_WINDOWS.h>
#elif (OS_ARCH==OS_SH2)
#include <SH2/Os_kernel_SH2.h>
#elif (OS_ARCH==OS_SH4)
#include <SH4/Os_kernel_SH4.h>
#elif (OS_ARCH==OS_ARM)
#include <ARM/Os_kernel_ARM.h>
#elif (OS_ARCH==OS_LINUX)
#include <LINUX/Os_kernel_LINUX.h>
#elif (OS_ARCH==OS_DSPIC33)
#include <DSPIC33/Os_kernel_DSPIC33.h>
#elif (OS_ARCH==OS_CORTEXM)
#include <CORTEXM/Os_kernel_CORTEXM.h>
#else
#error "Unsupported OS_ARCH defined. Check your Makefiles!"
#endif


/* Some default values for macros that might not be defined for all architectures
*/
#ifndef OS_HAVE_ARCHTASK_T
#define OS_HAVE_ARCHTASK_T 0
#endif

#ifndef OS_ARCHTASK_INIT
#define OS_ARCHTASK_INIT(x)
#endif

#ifndef OS_HAVE_ARCHAPP_T
#define OS_HAVE_ARCHAPP_T 0
#endif

#ifndef OS_ARCHAPP_INIT
#define OS_ARCHAPP_INIT(x)
#endif

#ifndef OS_HAVE_ARCHISRDYN_T
#define OS_HAVE_ARCHISRDYN_T	0
#endif

#ifndef OS_IntRestoreAll
#define OS_IntRestoreAll(is)	OS_IntRestore(is)
#endif

/* OS_USE_IRQ_ATOMIC_INKERNEL: Interrupt vector functions set the inKernel
 * flag before enabling interrupts again. This is needed, in case the
 * the interrupt masking mechanism of an interrupt controller may allow an
 * interrupt that occured between the IRQ entry and setting the lock
 * level to occur right after enabling interrupts.
 */
#ifndef OS_USE_IRQ_ATOMIC_INKERNEL
#define OS_USE_IRQ_ATOMIC_INKERNEL 0
#endif

/* OS_CPU_CAN_ATOMIC_READ_32: Set to 1, if the CPU reads a correctly aligned
 * 32-bit value in a consistent (typically atomic) way by hardware without
 * special precautions (or need for special assembly instructions).
 *
 * Note: This is one for all 32-bit CPUs and should be 0 for all 16-bit CPUs.
 */
#ifndef OS_CPU_CAN_ATOMIC_READ_32
#define OS_CPU_CAN_ATOMIC_READ_32	1
#endif

/*!
 * OS_GetCallerSp
 *
 * This macro returns the caller's stack pointer to be used in the stack check
 * when calling a trusted function. The provided stack pointer is expected to
 * lie between the bounds returned by the OS_GetTrustedFunctionStackBounds
 * macro.
 *
 * This is the common implementation for architectures where trusted functions
 * are always executed on the kernel stack. Since this macro is invoked in
 * kernel context just before calling the trusted function, the current stack
 * pointer is returned.
 *
 * Param spp: type os_address_t*, to store the caller's stack pointer
 */
#ifndef OS_GetCallerSp
#define OS_GetCallerSp(spp) \
	do { \
		*(spp) = (os_address_t) OS_GetCurrentSp(); \
	} while (0)
#endif /* OS_GetCallerSp */

/*!
 * OS_GetTrustedFunctionStackBounds
 *
 * This macro determines the lower and upper bounds for the stack of
 * a trusted function to be executed.
 *
 * There is two standard implementations depending on whether the trusted
 * functions are executed on the stack of the calling task/ISR or on the kernel
 * stack. The former is commonly the case for function call kernels, the latter
 * if the system call interface is used, but some architectures such as TRICORE
 * (always on task/ISR stack) differ.
 *
 * The standard implementation for OS_GetTFStackBounds_Syscall for syscall
 * kernels returns the bounds of the kernel stack.
 * The standard implementation OS_GetTFStackBounds_Fctcall for function call
 * kernels returns the bounds of the stack of the calling task/ISR.
 *
 * If the architecture does not define a custom mapping, a default mapping is
 * established to the standard implementations depending on whether the kernel
 * is a function call or syscall kernel below. Architectures for which this is
 * not appropriate may define OS_GetTrustedFunctionStackBounds to a custom
 * implementation.
 *
 * Param lbp: type os_address_t*, to store the lower bound
 * Param ubp: type os_address_t*, to store the upper bound
*/

#ifndef OS_GetTrustedFunctionStackBounds
#if (OS_KERNEL_TYPE==OS_FUNCTION_CALL)
#define OS_GetTrustedFunctionStackBounds(lbp,ubp) OS_GetTFStackBounds_Fctcall(lbp,ubp)
#else
#define OS_GetTrustedFunctionStackBounds(lbp,ubp) OS_GetTFStackBounds_Syscall(lbp,ubp)
#endif
#endif

#define OS_GetTFStackBounds_Syscall(lbp,ubp) \
	do { \
		*(lbp) = (os_address_t) OS_iStackBase; \
		*(ubp) = *(lbp) + OS_iStackLen; \
	} while (0)

#define OS_GetTFStackBounds_Fctcall(lbp,ubp) \
	do { \
		if ( OS_inFunction == OS_INTASK ) \
		{ \
			*(lbp) = OS_TASK_STACK_START; \
			*(ubp) = OS_TASK_STACK_END; \
		} \
		else \
		{ \
			if ( (OS_GetIsrStackLimit(&OS_isrDynamicBase[OS_isrCurrent]) == OS_NULL) ) \
			{ \
				*(lbp) = (os_address_t)OS_iStackBase; \
				*(ubp) = *(lbp) + OS_iStackLen; \
			} \
			else \
			{ \
				if(OS_STACKGROWS==OS_STACKGROWSDOWN) \
				{ \
					*(ubp) = (os_address_t)OS_GetIsrStackLimit(&OS_isrDynamicBase[OS_isrCurrent]); \
					*(lbp) = *(ubp) - OS_isrTableBase[OS_isrCurrent].stackLen; \
				} \
				else /* OS_STACKGROWSUP */ \
				{ \
					*(lbp) = (os_address_t)OS_GetIsrStackLimit(&OS_isrDynamicBase[OS_isrCurrent]); \
					*(ubp) = *(lbp) + OS_isrTableBase[OS_isrCurrent].stackLen; \
				} \
			} \
		} \
	} while (0)

/* OS_ATOMIC_ASSIGN_32(dest, src): Assign the 32-bit value src to dest so that
 * the value is consistent.
 *
 * Param dest: destination. Must be 32-bit (integer type) and aligned correctly.
 * Param src: source. Must be 32-bit (integer type) and aligned correctly.
*/
#if (OS_CPU_CAN_ATOMIC_READ_32 == 0)

/* On 16-bit CPUs, the assignment typically requires at least two
 * instructions for each 16-bit portion. If we get interrupted in
 * between these, the result is inconsistent.
 * Therefore, we check if the result is identical after the assignment,
 * which means that we didn't get interrupted in between (or at least
 * that the value wasn't updated in between).
 */
#define OS_ATOMIC_ASSIGN_32(dest, src)								\
	do																\
	{																\
		(dest) = (volatile os_uint32_t)(src);						\
	} while ((os_uint32_t)(dest) != (volatile os_uint32_t)(src));

#elif (OS_CPU_CAN_ATOMIC_READ_32 == 1)

/* On 32-bit CPUs (or larger), a 32-bit value can be loaded atomically,
 * at least if it is aligned correctly.
 */
#define OS_ATOMIC_ASSIGN_32(dest, src)						\
	do														\
	{														\
		(dest) = (src); /* always consistent on 32-bit */	\
	} while (0)

#else
#error "Incorrect value of OS_CPU_CAN_ATOMIC_READ_32."
#endif

#endif

/* Editor settings - DO NOT DELETE
 * vi:set ts=4:
*/
