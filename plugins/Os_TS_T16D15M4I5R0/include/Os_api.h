/* Os_api.h
 *
 * This file defines the OS User API.
 *
 * CHECK: TABS 4 (see editor commands at end of file)
 *
 * Copyright 1998-2014 Elektrobit Automotive GmbH
 * All rights exclusively reserved for Elektrobit Automotive GmbH,
 * unless expressly agreed to otherwise.
 *
 * $Id: Os_api.h 18119 2014-05-07 13:01:45Z dh $
*/
#ifndef __OS_API_H
#define __OS_API_H

#include <Os_defs.h>
#include <Os_types.h>

#if (OS_KERNEL_TYPE==OS_FUNCTION_CALL)

#include <Os_api_arch.h>			/* include first to allow overriding */
#include <Os_api_nosyscall.h>
#include <Os_acct_api.h>
#include <Os_proosek.h>
#include <Os_api_simtimer.h>
#include <Os_api_timestamp.h>

#elif (OS_KERNEL_TYPE==OS_MICROKERNEL)

#include <Os_api_microkernel.h>
#include <Os_api_timestamp.h>

#else
#include <Os_api_arch.h>			/* include first to allow overriding */
#include <Os_acct_api.h>
#include <Os_proosek.h>
#include <Os_api_simtimer.h>
#include <Os_api_timestamp.h>

/*!
 * OS system call stubs
 *
 * These are short assembler routines that make the
 * appropriate system call.
*/
#ifndef OS_ASM

/* OSEK/VDX API */
os_result_t OS_UserActivateTask(os_taskid_t);
os_result_t OS_UserTerminateTask(void);
os_result_t OS_UserChainTask(os_taskid_t);
os_result_t OS_UserSchedule(void);
os_result_t OS_UserGetTaskId(os_taskid_t*);
os_result_t OS_UserGetTaskState(os_taskid_t, os_taskstate_t*);
void OS_UserSuspendInterrupts(os_intlocktype_t);
void OS_UserResumeInterrupts(os_intlocktype_t);
os_result_t OS_UserGetResource(os_resourceid_t);
os_result_t OS_UserReleaseResource(os_resourceid_t);
os_result_t OS_UserSetEvent(os_taskid_t, os_eventmask_t);
os_result_t OS_UserClearEvent(os_eventmask_t);
os_result_t OS_UserGetEvent(os_taskid_t, os_eventmask_t*);
os_result_t OS_UserWaitEvent(os_eventmask_t);
os_result_t OS_UserGetAlarmBase(os_alarmid_t, os_alarmbase_t*);
os_result_t OS_UserGetAlarm(os_alarmid_t, os_tick_t*);
os_result_t OS_UserSetRelAlarm(os_alarmid_t, os_tick_t, os_tick_t);
os_result_t OS_UserSetAbsAlarm(os_alarmid_t, os_tick_t, os_tick_t);
os_result_t OS_UserCancelAlarm(os_alarmid_t);
os_appmodeid_t OS_UserGetActiveApplicationMode(void);
void OS_UserStartOs(os_appmodeid_t);
void OS_UserShutdownOs(os_result_t);

/* ProOSEK 4.x API
*/
os_result_t OS_UserIncrementCounter(os_counterid_t);

/* Autosar API
*/
os_result_t OS_UserGetStackInfo(os_taskorisr_t, os_stackinfo_t*);
os_result_t OS_UserStartScheduleTable(os_scheduleid_t, os_tick_t, os_boolean_t);
os_result_t OS_UserStartScheduleTableSynchron(os_scheduleid_t);
os_result_t OS_UserChainScheduleTable(os_scheduleid_t, os_scheduleid_t);
os_result_t OS_UserStopScheduleTable(os_scheduleid_t);
os_result_t OS_UserGetScheduleTableStatus(os_scheduleid_t, os_uint8_t*);
os_result_t OS_UserTerminateApplication(os_restart_t);
os_applicationid_t OS_UserGetApplicationId(void);
os_applicationid_t OS_UserCheckObjectOwnership(os_objecttype_t, os_objectid_t);
os_boolean_t OS_UserCheckObjectAccess(os_applicationid_t, os_objecttype_t, os_objectid_t);
os_isrid_t OS_UserGetIsrId(void);
os_memoryaccess_t OS_UserCheckIsrMemoryAccess(os_isrid_t, void*, os_size_t);
os_memoryaccess_t OS_UserCheckTaskMemoryAccess(os_taskid_t, void*, os_size_t);
os_result_t OS_UserCallTrustedFunction(os_functionid_t, void*);
os_result_t OS_UserSyncScheduleTable(os_scheduleid_t, os_tick_t);
os_result_t OS_UserSetScheduleTableAsync(os_scheduleid_t);
os_result_t OS_UserDisableInterruptSource(os_isrid_t);
os_result_t OS_UserEnableInterruptSource(os_isrid_t);
os_result_t OS_UserGetCounterValue(os_counterid_t, os_tick_t*);
os_result_t OS_UserGetElapsedCounterValue(os_counterid_t, os_tick_t*, os_tick_t*);

os_result_t OS_UserIocWrite(os_uint32_t, const void * const []);
os_result_t OS_UserIocRead(os_uint32_t, void * const []);
os_result_t OS_UserIocSend(os_uint32_t, const void * const []);
os_result_t OS_UserIocReceive(os_uint32_t, void * const []);
os_result_t OS_UserIocEmptyQueue(os_uint32_t id);

#endif  /* OS_ASM */

#endif  /* (OS_KERNEL_TYPE==OS_FUNCTION_CALL) */

/* The OS defines OS_HAS_ATOMIC_MONITOR_SUPPORT as OS_HAS_TRANSACTION_SUPPORT for backward compatibility.
 * This can be removed when all the Platforms have been updated.
*/
#define OS_HAS_ATOMIC_MONITOR_SUPPORT	OS_HAS_TRANSACTION_SUPPORT

/* Fast (but non-conformant) implementations of SuspendOSInterrupts etc.
*/
#ifndef OS_ASM
void OS_FastSuspendOsInterrupts(void);
void OS_FastResumeOsInterrupts(void);
void OS_FastSuspendAllInterrupts(void);
void OS_FastResumeAllInterrupts(void);
#endif  /* OS_ASM */

#ifndef OS_ASM
/* EB-specific system services
*/
#ifndef OS_EXCLUDE_DISABLEINTERRUPTSOURCE
/* !LINKSTO Kernel.Feature.InterruptSource.API.DisableInterruptSource, 1
*/
#define OS_DisableInterruptSource(i)	OS_UserDisableInterruptSource(i)
#define OSServiceID_DisableInterruptSource	OS_SID_DisableInterruptSource
#define OSError_DisableInterruptSource_DisableISR()			((ISRType)(OS_errorStatus.parameter[0]))
#endif

#ifndef OS_EXCLUDE_ENABLEINTERRUPTSOURCE
/* !LINKSTO Kernel.Feature.InterruptSource.API.EnableInterruptSource, 1
*/
#define OS_EnableInterruptSource(i)		OS_UserEnableInterruptSource(i)
#define OSServiceID_EnableInterruptSource	OS_SID_EnableInterruptSource
#define OSError_EnableInterruptSource_EnableISR()			((ISRType)(OS_errorStatus.parameter[0]))
#endif

/* User library routines
*/
os_int_t OS_StackCheck(void);
void OS_GetCurrentStackArea(void **, void **);

/* OS_StartGptTickers
 *
 * This function is defined in src/Os_starttickers.c if there are GPT tickers to start and if
 * automatic startup of the Gpt tickers has been inhibited by defining OS_INHIBIT_GPT_STARTUP=1
 * in board.h
*/
void OS_StartGptTickers(void);

/* Extra APIs to help improve the performance of non- and mixed-premptive systems.
 *
 * !LINKSTO Kernel.Feature.FastSchedule, 1
 * !LINKSTO Kernel.Feature.FastSchedule.ErrorChecking, 1
 * !LINKSTO Kernel.Feature.FastSchedule.ScheduleIfNecessary, 1
 * !LINKSTO Kernel.Feature.FastSchedule.ScheduleIfNecessary.ReturnValue, 1
 * !LINKSTO Kernel.Feature.FastSchedule.ScheduleIfWorthwhile, 1
 * !LINKSTO Kernel.Feature.FastSchedule.ScheduleIfWorthwhile.ReturnValue, 1
*/
os_boolean_t OS_IsScheduleNecessary(void);
os_boolean_t OS_IsScheduleWorthwhile(void);

#define OS_ScheduleIfNecessary()	( OS_IsScheduleNecessary() ? Schedule() : OS_E_OK )
#define OS_ScheduleIfWorthwhile()	( OS_IsScheduleWorthwhile() ? Schedule() : OS_E_OK )

#endif   /* OS_ASM */

/* If we're running on a microkernel that supports the WaitGetClearEvent API, map OS_WaitGetClearEvent to it.
*/
#if OS_KERNEL_TYPE == OS_MICROKERNEL

#ifdef MK_HAS_WAITGETCLEAREVENT

#define OS_HAS_WAITGETCLEAREVENT	MK_HAS_WAITGETCLEAREVENT

#if OS_HAS_WAITGETCLEAREVENT
#define OS_WaitGetClearEvent(e, ep)	MK_WaitGetClearEvent((e), (ep))
#endif

#endif

#endif

#endif /* __OS_API_H */

/* Editor settings - DO NOT DELETE
 * vi:set ts=4:
*/
