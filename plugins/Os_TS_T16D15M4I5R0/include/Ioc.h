/* Ioc.h
 *
 * Common Type definitions for Ioc
 *
 * CHECK: TABS 4 (see editor commands at end of file)
 *
 * Copyright 1998-2014 Elektrobit Automotive GmbH
 * All rights exclusively reserved for Elektrobit Automotive GmbH,
 * unless expressly agreed to otherwise.
 *
 * $Id: Ioc.h 17602 2014-02-03 12:09:35Z tojo2507 $
*/
#ifndef IOC_H
#define IOC_H

#include <Os_defs.h>

#include <Ioc/Ioc.h>
#include <Ioc_gen.h> /* generated header */

#endif
