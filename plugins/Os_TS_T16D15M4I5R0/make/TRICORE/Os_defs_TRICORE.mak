#################################################################
#
# $Id: Os_defs_TRICORE.mak 20029 2014-12-09 08:33:14Z mabr2343 $
#
# This makefile contains derivative-specific definitions for building the OS.
#
# Copyright 1998-2014 Elektrobit Automotive GmbH
# All rights exclusively reserved for Elektrobit Automotive GmbH,
# unless expressly agreed to otherwise.
#
#################################################################

ifneq ($(ASM_FILE_SUFFIX),s)
$(error ASM_FILE_SUFFIX does not match file names - expected 's', got '$(ASM_FILE_SUFFIX)')
endif

OS_TRICORE_PREFIX=$(OS_ARCH_PREFIX)

#########################
# TRICORE src\ DIRECTORY
#
ifeq ($(OS_KERNEL_TYPE),MICROKERNEL)
OS_OBJS_ARCH = 													\
	$(addprefix $(AutosarOS_SRC_PATH)/TRICORE/,					\
		Os_configuration_microkernel_TRICORE.$(CC_FILE_SUFFIX)	\
	)
else
ifneq ($(BUILD_MODE),LIB)
ASM_FILES_TO_BUILD += \
	$(AutosarOS_SRC_PATH)\TRICORE\$(OS_TRICORE_PREFIX)interruptvectors.$(ASM_FILE_SUFFIX)

EXCLUDE_MAKE_DEPEND  += \
	$(AutosarOS_SRC_PATH)\TRICORE\$(OS_TRICORE_PREFIX)interruptvectors.$(ASM_FILE_SUFFIX)
endif

OS_OBJS_ARCH += 												\
	$(addprefix $(AutosarOS_SRC_PATH)/TRICORE/,					\
		Os_configuration_TRICORE.$(CC_FILE_SUFFIX)				\
		$(OS_TRICORE_PREFIX)initsp.$(CC_FILE_SUFFIX)			\
		$(OS_TRICORE_PREFIX)initcsalist.$(CC_FILE_SUFFIX) 		\
		$(OS_TRICORE_PREFIX)startupchecks.$(CC_FILE_SUFFIX)		\
	)
endif

Os_src_FILES += $(OS_OBJS_ARCH)


############################
#  TRICORE COMMON lib
#
ifeq ($(OS_KERNEL_TYPE),MICROKERNEL)
#OS_KLIB_OBJS_ARCH = 											\
#	$(addprefix $(AutosarOS_LIB_SRC_PATH)\TRICORE\,				\
#		$(OS_TRICORE_PREFIX)gettimestamp.$(ASM_FILE_SUFFIX)		\
#	)															\
#	$(OS_KLIB_OBJS_DERIVATE)
OS_KLIB_OBJS_ARCH = 											\
	$(OS_KLIB_OBJS_DERIVATE)
OS_ULIB_OBJS_ARCH =
else
OS_KLIB_OBJS_ARCH = \
	$(addprefix $(AutosarOS_LIB_SRC_PATH)\TRICORE\,			\
		$(OS_TRICORE_PREFIX)bf11workaround.$(ASM_FILE_SUFFIX)			\
		$(OS_TRICORE_PREFIX)callapphook.$(CC_FILE_SUFFIX)				\
		$(OS_TRICORE_PREFIX)callhook.$(CC_FILE_SUFFIX)					\
		$(OS_TRICORE_PREFIX)callisr.$(CC_FILE_SUFFIX)					\
		$(OS_TRICORE_PREFIX)callnontrustedisr.$(CC_FILE_SUFFIX)			\
		$(OS_TRICORE_PREFIX)cat1handler.$(ASM_FILE_SUFFIX)				\
		$(OS_TRICORE_PREFIX)cat1interrupt.$(ASM_FILE_SUFFIX)			\
		$(OS_TRICORE_PREFIX)cat2handler.$(ASM_FILE_SUFFIX)				\
		$(OS_TRICORE_PREFIX)cat2interrupt.$(ASM_FILE_SUFFIX)			\
		$(OS_TRICORE_PREFIX)catkhandler.$(ASM_FILE_SUFFIX)				\
		$(OS_TRICORE_PREFIX)enterprotectedmode.$(CC_FILE_SUFFIX)		\
		$(OS_TRICORE_PREFIX)exceptionvectors.$(ASM_FILE_SUFFIX)			\
		$(OS_TRICORE_PREFIX)exceptionstartup.$(ASM_FILE_SUFFIX)			\
		$(OS_TRICORE_PREFIX)findtasksp.$(CC_FILE_SUFFIX)				\
		$(OS_TRICORE_PREFIX)freecxlist.$(ASM_FILE_SUFFIX)				\
		$(OS_TRICORE_PREFIX)freecxlistto.$(ASM_FILE_SUFFIX)				\
		$(OS_TRICORE_PREFIX)grab1csa.$(ASM_FILE_SUFFIX)					\
		$(OS_TRICORE_PREFIX)grab2csa.$(ASM_FILE_SUFFIX)					\
		$(OS_TRICORE_PREFIX)gettimestamp.$(CC_FILE_SUFFIX)				\
		$(OS_TRICORE_PREFIX)getcoreid.$(ASM_FILE_SUFFIX)				\
		$(OS_TRICORE_PREFIX)killcalledcontext.$(CC_FILE_SUFFIX)			\
		$(OS_TRICORE_PREFIX)idle.$(ASM_FILE_SUFFIX)						\
		$(OS_TRICORE_PREFIX)initmodule.$(CC_FILE_SUFFIX)				\
		$(OS_TRICORE_PREFIX)initarchtricore.$(CC_FILE_SUFFIX)			\
		$(OS_TRICORE_PREFIX)sethookprotection.$(CC_FILE_SUFFIX)			\
		$(OS_TRICORE_PREFIX)setisrprotection.$(CC_FILE_SUFFIX)			\
		$(OS_TRICORE_PREFIX)setprotection.$(CC_FILE_SUFFIX)				\
		$(OS_TRICORE_PREFIX)starttask.$(CC_FILE_SUFFIX)					\
		$(OS_TRICORE_PREFIX)syscall.$(ASM_FILE_SUFFIX)					\
		$(OS_TRICORE_PREFIX)syscallstartup.$(ASM_FILE_SUFFIX)			\
		$(OS_TRICORE_PREFIX)trap0handler.$(CC_FILE_SUFFIX)				\
		$(OS_TRICORE_PREFIX)trap1handler.$(CC_FILE_SUFFIX)				\
		$(OS_TRICORE_PREFIX)trap2handler.$(CC_FILE_SUFFIX)				\
		$(OS_TRICORE_PREFIX)trap3handler.$(CC_FILE_SUFFIX)				\
		$(OS_TRICORE_PREFIX)trap4handler.$(CC_FILE_SUFFIX)				\
		$(OS_TRICORE_PREFIX)trap5handler.$(CC_FILE_SUFFIX)				\
		$(OS_TRICORE_PREFIX)trap7handler$(OS_T7H_VARIANT).$(CC_FILE_SUFFIX)				\
		$(OS_TRICORE_PREFIX)trustedfunctionwrapper.$(ASM_FILE_SUFFIX)	\
		$(OS_TRICORE_PREFIX)unknowninterrupt.$(ASM_FILE_SUFFIX)			\
		$(OS_TRICORE_PREFIX)writeendinit.$(CC_FILE_SUFFIX)				\
	 )											   \
	  $(addprefix $(AutosarOS_LIB_SRC_PATH)\kernel\, \
		$(OS_KERN_PREFIX)triggerinterrupt.$(CC_FILE_SUFFIX)	   \
   )                                              \
		$(OS_KLIB_OBJS_DERIVATE)

# those need to be in the lib for non-Aurix
ifeq ($(filter TC27XFPGA TC2D5 TC277 TC23XL TC22XL,$(OS_CPU)),)
OS_KLIB_OBJS_ARCH += \
	$(addprefix $(AutosarOS_LIB_SRC_PATH)\TRICORE\,			\
		$(OS_TRICORE_PREFIX)setpr0.$(ASM_FILE_SUFFIX)					\
		$(OS_TRICORE_PREFIX)setpr1.$(ASM_FILE_SUFFIX)					\
	 )
endif

OS_ULIB_OBJS_ARCH = \
	$(OS_ULIB_OBJS_ARCH_COMMON)


OS_ELIB_OBJS_ARCH = \
	$(addprefix $(AutosarOS_LIB_SRC_PATH)\TRICORE\,						\
		$(OS_TRICORE_PREFIX)$(OS_ERROR_INFIX)$(OS_HYPHEN)ArchTrapHandler.$(CC_FILE_SUFFIX)	\
	)
endif

ifneq ($(OBJ_OUTPUT_PATH),)
OS_OBJ_DIR = $(OBJ_OUTPUT_PATH)
else
OS_OBJ_DIR = $(OBJ_DIR)
endif
$(OS_OBJ_DIR)\$(OS_TRICORE_PREFIX)interruptvectors.o: $(AutosarOS_OUTPUT_PATH)\include\Os_config.h

include $(AutosarOS_CORE_PATH)/make/$(OS_ARCH)/$(OS_CPU)/Os_defs_$(OS_ARCH)_$(OS_CPU).mak

# Editor settings: DO NOT DELETE
# vi:set ts=4:
