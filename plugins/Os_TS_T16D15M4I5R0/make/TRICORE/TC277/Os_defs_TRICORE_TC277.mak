#################################################################
#
# $Id: Os_defs_TRICORE_TC277.mak 17608 2014-02-03 14:53:52Z tojo2507 $
#
# This makefile contains derivative-specific definitions for building the OS.
#
# Copyright 1998-2014 Elektrobit Automotive GmbH
# All rights exclusively reserved for Elektrobit Automotive GmbH,
# unless expressly agreed to otherwise.
#
#################################################################

OS_T7H_VARIANT=-tc131

OS_KLIB_OBJS_DERIVATE =$(addprefix $(AutosarOS_LIB_SRC_PATH)\TRICORE\,	     \
                   $(OS_TRICORE_PREFIX)timer$(OS_HYPHEN)stm.$(CC_FILE_SUFFIX) \
                   )
