/* TRICORE-callhook.c
 *
 * This file contains the OS_CallHook wrapper for TRICORE. It is in a
 * separate file to prevent the Tasking compiler from inlining it :-(
 *
 * CHECK: TABS 4 (see editor commands at end of file)
 *
 * Copyright 1998-2014 Elektrobit Automotive GmbH
 * All rights exclusively reserved for Elektrobit Automotive GmbH,
 * unless expressly agreed to otherwise.
 *
 * $Id: TRICORE-callhook.c 17629 2014-02-05 10:19:35Z tojo2507 $
*/

#include <Os_kernel.h>

#include <memmap/Os_mm_code_begin.h>

/*!
 * OS_CallHook()
 *
 * This function sets up its own return address and CSA so that an
 * RFE instruction will jump to the first instruction of the hook
 * with the registers in the expected state. In particular, the register
 * PCXI (in the hook function) must be zero so that the hook can get back
 * into the kernel when it returns.
 *
 * This function never returns!
*/
void OS_CallHook
(	const os_appcontext_t *app,
	void *hook,
	os_size_t stackLen,
	os_savedcontext_t *ctxt,
	os_unsigned_t param
)
{
	os_uint32_t pcxi;
	os_uppercx_t *cx;

	/* Ensure that the function call is complete before reading PCXI
	 * This might not be necessary if errata-workaround
	 * OS_TRICORE_BF_05 is necessary, because then there's a DSYNC
	 * in the function entry anyway.
	*/
	OS_CxDsync();

	pcxi = OS_MFCR(OS_PCXI);
	cx = OS_AddrFromCx(pcxi);

	/* eventually align stack */
	OS_ALIGN_STACK(cx->a10);
	ctxt->stackLimit = (os_uint8_t *)cx->a10;

	OS_SetHookProtection(app, ctxt, stackLen);

	/* Load application's PSW. Ensure trace mode is off and that
	 * we use the interrupt stack.
	*/
	cx->psw = app->archapp.psw | OS_PSW_IS | 1u;	/* trace mode -> off */
	cx->a11 = (os_uint32_t)&OS_NullFunction;
	cx->pcxi = 0;
	OS_MTRA(hook);

	/* Frig the PSW so that we can RFE from here instead of RET
	*/
	OS_MTCR(OS_PSW, OS_PSW_CDC_6|OS_PSW_CDE|OS_PSW_GW|OS_PSW_IS|OS_PSW_IO_SU|OS_PSW_PRS_0);

	/* Load the parameter for the hook function. This assumes that the
	 * parameter can only be of integral type, which is true at the time
	 * of writing.
	*/
	OS_MTD4(param);

	OS_RFE();
}

#include <memmap/Os_mm_code_end.h>

/* Editor settings; DO NOT DELETE
 * vi:set ts=4:
*/
