/* TRICORE_timer_stm.c - Tricore STM driver for AutosarOS
 *
 * CHECK: TABS 4 (see editor commands at end of file)
 *
 * Copyright 1998-2014 Elektrobit Automotive GmbH
 * All rights exclusively reserved for Elektrobit Automotive GmbH,
 * unless expressly agreed to otherwise.
 *
 * $Id: TRICORE-timer-stm.c 18079 2014-04-29 12:26:27Z olme8414 $
*/

#include <Os_kernel.h>

#include <TRICORE/Os_TRICORE_timer_stm.h>
#include <TRICORE/Os_TRICORE_stm.h>
#include <TRICORE/Os_TRICORE_cpu.h>
#include <Os_tool.h>

#if !defined(OS_STM_HAS_COMPARE)
#error "OS_STM_HAS_COMPARE is not defined: check the derivative spec file"
#elif OS_STM_HAS_COMPARE==0
#error "STM on this variant has no compare registers. Check your Makefiles!"
#endif

#if !defined(OS_N_STM)
#error "OS_N_STM is not defined: check the derivative spec file"
#endif

/* some fallbacks for the "old" (i.e., pre-Aurix) Tricores */
#if (OS_N_STM == 1) && (!(defined OS_STM0_BASE))
#define OS_STM0_BASE	OS_STM_BASE
#define OS_SRC_STM0SR0	OS_STM_SRC0
#define OS_SRC_STM0SR1	OS_STM_SRC1
#endif

/* If the OS is running above the microkernel, it doesn't have the rights to
 * write the SRC-registers... and doesn't need to. Probably it may not even be
 * necessary in the other case, but this behaviour is kept for backwards
 * compatibility.
*/
#if OS_KERNEL_TYPE == OS_MICROKERNEL
#define OS_STM_WRITE_SRC	0
#else
#define OS_STM_WRITE_SRC	1
#endif

/* stmdescr_t
 *
 * Struct storing the base address of an STM, as well as the base addresses of the
 * SRC-registers in the respective SRNs for both comparators. The latters are not
 * needed if the OS runs on top of the microkernel.
 *
*/
typedef struct stmdescr_s stmdescr_t;
struct stmdescr_s
{
	os_tricorestm_t *base;
#if (OS_STM_WRITE_SRC == 1)
	os_tricoresrn_t *src[2];
#endif
};

#if (OS_STM_WRITE_SRC == 1)
#define OS_STMDESCR_INIT(base, src0, src1) { (os_tricorestm_t *)(base), {(src0), (src1)} }
#else
#define OS_STMDESCR_INIT(base, src0, src1) { (os_tricorestm_t *)(base) }
#endif

/* stmmasks_t
 *
 * Struct storing bitmasks for STM registers where control bits for the different
 * registers are contained in the same register at different positions.
*/
typedef struct stmmasks_s stmmasks_t;
struct stmmasks_s
{
	os_uint32_t setFlag;
	os_uint32_t rstFlag;
	os_uint32_t icrMask;
	os_uint32_t icrCmpXEn;
	os_uint32_t icrCmpXOs;
	os_uint32_t	cmconMask;
	os_uint32_t	cmconValue;
};

#include <memmap/Os_mm_const_begin.h>
static const stmmasks_t stmMasks[2] =
{
	{
		OS_STM_CMP0IRS,
		OS_STM_CMP0IRR,
		OS_STM_CMP0,
		OS_STM_CMP0EN,
		OS_STM_CMP0OS_IR0,
		OS_STM_CMCON0,
		OS_STM_MSIZE0
	},
	{
		OS_STM_CMP1IRS,
		OS_STM_CMP1IRR,
		OS_STM_CMP1,
		OS_STM_CMP1EN,
		OS_STM_CMP1OS_IR1,
		OS_STM_CMCON1,
		OS_STM_MSIZE1
	}
};

static const stmdescr_t stmDescr[OS_N_STM] =
{
	OS_STMDESCR_INIT( OS_STM0_BASE, &OS_SRC_STM0SR0, &OS_SRC_STM0SR1 ),
#if ( OS_N_STM > 1 )
	OS_STMDESCR_INIT( OS_STM1_BASE, &OS_SRC_STM1SR0, &OS_SRC_STM1SR1 ),
#endif
#if ( OS_N_STM > 2 )
	OS_STMDESCR_INIT( OS_STM2_BASE, &OS_SRC_STM2SR0, &OS_SRC_STM2SR1 ),
#endif
};

const os_hwtdrv_t OS_stmDriver =
{
	&OS_StmInit,
	&OS_StmRead,
	&OS_StmStart,
	&OS_StmStop
};
#include <memmap/Os_mm_const_end.h>


/*!
 * OS_StmInit()
 *
 * Initialises the STM specified in the HW timer descriptor.
 *
*/
void OS_StmInit(const os_hwt_t *td)
{
	os_tricorestm_t * const stm = stmDescr[td->major].base;
	stmmasks_t const * const sm = &stmMasks[td->minor];
	os_uint32_t cmpOsFixed = sm->icrCmpXOs;

#if OS_TRICORE_BF_10
	/* On derivatives with the "twisted interrupts" silicon bug (eg TC1796),
	 * later revisions of the chips have an updated timer module with the bug fixed.
	 * So we test at runtime for the faulty revisions and use fix the IRQ-sources if
	 * necessary: for CMP0 (minor number == 0), the OS-bit needs to be set ot IR1, for
	 * CMP1 it needs to be set to IR0.
	*/
	if ( (stm->stm_module_hdr.id & OS_ID_REV) <= OS_STM_REV_BF_10 )
	{
		if ( td->minor == 0 )
		{
			cmpOsFixed = OS_STM_CMP0OS_IR1;
		}
		else
		{
			cmpOsFixed = OS_STM_CMP1OS_IR0;
		}
	}

#endif

	/* Put the comparator into a known state.
	 * This also selects the interrupt SRN, taking BF_10 into account.
	*/
	stm->stm_icr = (stm->stm_icr & ~(sm->icrMask)) | cmpOsFixed;

	/* Clear any pending interrupt
	*/
	stm->stm_isrr = sm->rstFlag;

	/* Configure the compare register.
	*/
	stm->stm_cmcon = (stm->stm_cmcon & ~(sm->cmconMask)) | sm->cmconValue;
}

/*!
 * OS_StmRead()
 *
 * Reads the timer and returns the result. We need to look at the appropriate bits of the timer.
*/
os_timervalue_t OS_StmRead(const os_hwt_t *td)
{
	return stmDescr[td->major].base->stm_tim0;
}

/*!
 * OS_StmStart()
 *
 * Initialises and starts the STM comparator as specified in the HW timer descriptor.
 *
 * The absolute value given by (old + delta) is stored into the comparator. If the
 * value is already in the past, the timer interrupt is triggered. The "in the past"
 * computation uses the given old value as a limit for times that are less than the
 * new timer position but must be considered to be in the future.
*/
os_boolean_t OS_StmStart(const os_hwt_t *td, os_timervalue_t old, os_tick_t delta)
{
	os_tricorestm_t * const stm = stmDescr[td->major].base;
	stmmasks_t const * const sm = &stmMasks[td->minor];
	os_timervalue_t new;

	/* Clear any pending interrupt.
	*/
	stm->stm_isrr = sm->rstFlag;
#if (OS_STM_WRITE_SRC == 1)
	*(stmDescr[td->major].src[td->minor]) |= OS_SRN_CLRR;
#endif

	/* Set the compare register to the absolute value specified.
	 * We don't need to handle wraparound specially, because the
	 * timer is 32-bit, so the wrap point is the same as the integer
	 * overflow point.
	*/
	stm->stm_cmp[td->minor] = (old + delta);

	/* Enable the comparator and the interrupt
	*/
	stm->stm_icr |= sm->icrCmpXEn;
#if (OS_STM_WRITE_SRC == 1)
	*(stmDescr[td->major].src[td->minor]) |= OS_SRN_SRE;

	/* Attempt to flush the CPU pipelines
	 *
	 * According to Infineon it is only necessary to read back the last register that was written.
	*/
	OS_ReadbackVolatile(*(stmDescr[td->major].src[td->minor]));
#else
	OS_ReadbackVolatile(stm->stm_icr);
#endif

	/* Look at the stm value again. If the before and after values straddle the
	 * programmed value, we return TRUE to indicate that the time is in the past.
	 * Overflows here don't matter - all we're interested in are the relative distances.
	*/
	new = stm->stm_tim0;

	return (delta <= (new - old));
}

/*!
 * OS_StmStop()
 *
 * Disables the STM comparator as specified in the HW timer descriptor.
*/
void OS_StmStop(const os_hwt_t *td)
{
	os_tricorestm_t * const stm = stmDescr[td->major].base;
	stmmasks_t const * const sm = &stmMasks[td->minor];

	/* Disable the comparator clear any pending interrupt.
	 *
	 * According to Infineon it is only necessary to read back the last register that was written.
	 * In this case we need to ensure that the comparator is really disabled before clearing the
	 * interrupt flag and enabling the interrupt.
	*/
	stm->stm_icr &= ~(sm->icrCmpXEn);
	OS_ReadbackVolatile(stm->stm_icr);

	stm->stm_isrr = sm->rstFlag;

#if (OS_STM_WRITE_SRC == 1)
	*(stmDescr[td->major].src[td->minor]) |= OS_SRN_CLRR;
	OS_ReadbackVolatile(*(stmDescr[td->major].src[td->minor]));
#else
	OS_ReadbackVolatile(stm->stm_isrr);
#endif

}

/* Editor settings; DO NOT DELETE
 * vi:set ts=4:
*/
