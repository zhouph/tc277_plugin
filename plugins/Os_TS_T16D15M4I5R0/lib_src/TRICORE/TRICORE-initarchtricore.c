/* TRICORE-initarchtricore.c
 *
 * This file contains the OS_InitArchTricore function.
 *
 * CHECK: TABS 4 (see editor commands at end of file)
 *
 * Copyright 1998-2014 Elektrobit Automotive GmbH
 * All rights exclusively reserved for Elektrobit Automotive GmbH,
 * unless expressly agreed to otherwise.
 *
 * $Id: TRICORE-initarchtricore.c 17655 2014-02-07 08:52:02Z tojo2507 $
*/

#include <Os_kernel.h>

#if (OS_TOOL == OS_tasking)
/* This variable is used by assorted kernel and driver routines for flushing the processor
 * pipeline by reading back a hardware register immediately after writing a value to it.
 * It is only used for the Tasking compiler, and is only really necessary for older
 * versions (2.1, 2.2r1?)
 * See also Os_tool_TRICORE_tasking.h
*/
#include <memmap/Os_mm_var32_begin.h>
volatile os_uint32_t OS_junk;
#include <memmap/Os_mm_var32_end.h>

#endif

#include <memmap/Os_mm_code_begin.h>

/*!
 * OS_InitArchTricore()
 *
 * This function performs the parts of the harware initialisation that
 * are architecture-specific, OS-related and independent of the
 * board package.
 *
 * For Tricore the following hardware needs to be initialised:
 *
 * PSW (processor status word)
 * Stack pointer
 * Context save areas
 * PLL (phase-locked loop - sets CPU frequency
 * Timers (including prescalers)
 * PCXI (zero)
 * BTV (trap vector base)
 * BIV (interrupt vector base)
 * ISP (interrupt stack pointer)
 * RAM (global data and bss sections)
 * ICR (arbitration cycles, interrupt level (disabled)
 *
 * With the exception of BTV and BIV, these must all be set to working
 * values before calling StartOS, otherwise StartOS cannot be called.
 * So we program the two vector base registers here. The ICR's arbitration
 * cycles field is also set to match the configured kernel.
 *
 * This also permits the startup code to use a different trap table
 * from the normal OS. AutosarOS 1.5 requires that, should a processor
 * exception occur before StartOS, the handler enters an endless loop.
 * This can be implemented easily by building a special startup trap
 * table and using this during startup. The system call entry in this
 * table must naturally still jump to the system-call handler, otherwise
 * StartOS cannot be called.
*/
void OS_InitArchTricore(void)
{
	/* By the time we get here we are running with all known
	   interrupts disabled (i.e. at the "lock all" level) but
	   the global interrupt enable bit is set (enabled).
	   We leave the level intact but set the disable bit and
	   at the same time program the arbitration cycles.
	   Then, after the vector registers are programmed we
	   simply ENABLE. We leave the ICR_CONEC bit alone too
	   (no. of cpu cycles per arbitration cycle). This can
	   be set as needed for the board.
	*/
	OS_MTCR(OS_ICR,
			( (OS_MFCR(OS_ICR) & (os_uint32_t) ~((os_uint32_t)OS_ICR_IE | OS_ICR_CARBCYC))
			  | OS_ICR_ARBC2CARBC(OS_arbCycles)
			) );

	OS_WriteEndinit(0);

	OS_MTCR(OS_BTV, (os_uint32_t)&OS_trapTable);
	OS_MTCR(OS_BIV, (os_uint32_t)&OS_interruptTable);

	OS_WriteEndinit(1);

	OS_ENABLE();


    /* The following servcie will init OS_globalDpr1[] */
	OS_SetProtectionInit();
}

#include <memmap/Os_mm_code_end.h>

/* Editor settings; DO NOT DELETE
 * vi:set ts=4:
*/
