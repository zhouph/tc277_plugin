/* TRICORE-setpr0.s
 *
 * This file contains the function OS_SetPr0
 *
 * CHECK: TABS 4 (see editor commands at end of file)
 *
 * Copyright 1998-2014 Elektrobit Automotive GmbH
 * All rights exclusively reserved for Elektrobit Automotive GmbH,
 * unless expressly agreed to otherwise.
 *
 * $Id: TRICORE-setpr0.s 17602 2014-02-03 12:09:35Z tojo2507 $
*/
#ifndef OS_ASM
#define OS_ASM
#endif

#include <Os_kernel.h>
#include <Os_tool.h>
#include <TRICORE/Os_TRICORE_core.h>

/*!
 * OS_SetPr0() - sets up protection register set 0 (code and data)
 *
 * This routine transfers a full set of protection register values into
 * code protection register set 0 and data protection register set 0,
 * including the associated protection mode registers.
 *
 * The protection register ranges are first loaded into data registers,
 * thus (hopefully) avoiding the need for dsync/isync before and after
 * each mtcr.
 *
 * The code protection registers and CPM are not written if the cpm
 * value (d4) is zero. This should permit debugging of systems
 * while retaining data protection.
 *
 * The function's prototype is
 * void OS_SetPr0
 * (   unsigned cpm,	/@ in register d4 @/
 *     unsigned *cpr,	/@ in register a4 @/
 *     unsigned dpm,	/@ in register d5 @/
 *     unsigned *dpr	/@ in register a5 @/
 * )
*/
	_TEXT
	_GLOBAL	OS_SetPr0
	_GTYPE(OS_SetPr0,@function)
	_GLOBAL	__callee.OS_SetPr0.da.v.ipip
	_GTYPE(__callee.OS_SetPr0.da.v.ipip,@function)

_TTYPE(OS_SetPr0,FUNC)
_TTYPE(__callee.OS_SetPr0.da.v.ipip,FUNC)
	_entry


	ld.w	d8,[a5]0		/* Load all 8 DPR registers */
	ld.w	d9,[a5]4
	ld.w	d10,[a5]8
	ld.w	d11,[a5]12
	ld.w	d12,[a5]16
	ld.w	d13,[a5]20
	ld.w	d14,[a5]24
	ld.w	d15,[a5]28

	jeq		d4,_IMM(#,0),pr0Partial

	ld.w	d0,[a4]0		/* Load all 4 CPR registers */
	ld.w	d1,[a4]4
	ld.w	d2,[a4]8
	ld.w	d3,[a4]12

	_dsync

	mtcr	_IMM(#,OS_DPR0_0L),d8	/* Program DPR0 */
	mtcr	_IMM(#,OS_DPR0_0U),d9
	mtcr	_IMM(#,OS_DPR0_1L),d10
	mtcr	_IMM(#,OS_DPR0_1U),d11
	mtcr	_IMM(#,OS_DPR0_2L),d12
	mtcr	_IMM(#,OS_DPR0_2U),d13
	mtcr	_IMM(#,OS_DPR0_3L),d14
	mtcr	_IMM(#,OS_DPR0_3U),d15
	mtcr	_IMM(#,OS_DPM0),d5		/* Program DPM0 */

	mtcr	_IMM(#,OS_CPR0_0L),d0	/* Program CPR0 */
	mtcr	_IMM(#,OS_CPR0_0U),d1
	mtcr	_IMM(#,OS_CPR0_1L),d2
	mtcr	_IMM(#,OS_CPR0_1U),d3
	mtcr	_IMM(#,OS_CPM0),d4		/* Program CPM0 */

	isync

	ret

pr0Partial:
	_dsync

	mtcr	_IMM(#,OS_DPR0_0L),d8	/* Program DPR0 */
	mtcr	_IMM(#,OS_DPR0_0U),d9
	mtcr	_IMM(#,OS_DPR0_1L),d10
	mtcr	_IMM(#,OS_DPR0_1U),d11
	mtcr	_IMM(#,OS_DPR0_2L),d12
	mtcr	_IMM(#,OS_DPR0_2U),d13
	mtcr	_IMM(#,OS_DPR0_3L),d14
	mtcr	_IMM(#,OS_DPR0_3U),d15
	mtcr	_IMM(#,OS_DPM0),d5		/* Program DPM0 */

	isync

	ret

/* Editor settings - DO NOT DELETE
 * vi:set ts=4:
*/
