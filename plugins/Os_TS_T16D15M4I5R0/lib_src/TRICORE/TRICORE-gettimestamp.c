/* TRICORE-gettimestamp.c
 *
 * CHECK: TABS 4 (see editor commands at end of file)
 *
 * Copyright 1998-2014 Elektrobit Automotive GmbH
 * All rights exclusively reserved for Elektrobit Automotive GmbH,
 * unless expressly agreed to otherwise.
 *
 * $Id: TRICORE-gettimestamp.c 17602 2014-02-03 12:09:35Z tojo2507 $
*/

#include <Os_kernel.h>
#include <TRICORE/Os_TRICORE_core.h>
#include <TRICORE/Os_TRICORE_stm.h>

#include <memmap/Os_mm_code_begin.h>

/* OS_TricoreGetTimestamp
 *
 * Places the current value of the STM (56/64 bits)into the indicated mk_time_t (64-bit) location
 *
 * Assumptions:
 *  - None. In particular, no assumption is made about interrupts being disabled.
 *
 * Parameters: currentTime points to the mk_time_t output location
 *
 * In this function, the loop terminates when the lower-part of the STM counter is read between two
 * identical values from the upper part.
 *
 * Termination: in theory, the loop might never terminate. However, this would require a very carefully-crafted
 * interrupt, or an interrupt load so high that in practice whatever called this function would never do
 * any processing anyway.
 *
*/
void OS_TricoreGetTimestamp(os_timestamp_t *currentTime)
{
	os_uint32_t high1;
	os_uint32_t high2;
	os_uint32_t low;
	os_tricorestm_t * stm;

#if (OS_TRICOREARCH == OS_TRICOREARCH_16EP)
	stm = OS_timestampStm;
#else
	stm = &OS_stm;
#endif

	do
	{
		high1 = stm->stm_tim6;
		low = stm->stm_tim0;
		high2 = stm->stm_tim6;
	} while ( high1 != high2 );

	currentTime->tsLo = low;
	currentTime->tsHi = high1;
}

#include <memmap/Os_mm_code_end.h>

/* Editor settings; DO NOT DELETE
 * vi:set ts=4:
*/
