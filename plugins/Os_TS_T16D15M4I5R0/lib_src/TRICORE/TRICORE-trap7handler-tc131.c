/* TRICORE-trap7handler.c
 *
 * This file contains the OS_Trap7Handler function.
 *
 * CHECK: TABS 4 (see editor commands at end of file)
 *
 * Copyright 1998-2014 Elektrobit Automotive GmbH
 * All rights exclusively reserved for Elektrobit Automotive GmbH,
 * unless expressly agreed to otherwise.
 *
 * $Id: TRICORE-trap7handler-tc131.c 17602 2014-02-03 12:09:35Z tojo2507 $
*/

#define OS_SID OS_SID_ArchTrapHandler
#define OS_SIF OS_svc_ArchTrapHandler

#include <Os_kernel.h>
#include <TRICORE/Os_TRICORE_core.h>

#include <memmap/Os_mm_code_begin.h>

/*!
 * OS_Trap7Handler()
 *
 * This function handles TRICORE class 7 traps.
 *
 * Class 7 traps are caused by some internal event in the CPU, or an external
 * trigger. We just read the register, clear the traps, and then execute a
 * DEBUG instruction. The user can override this function if necessary.
*/
void OS_Trap7Handler(void)
{
	os_uint32_t trapstat;

	/* Read the trap status registers and clear the traps
	*/
	trapstat = OS_pwr.pwr_trapstat;
	OS_pwr.pwr_trapclr = trapstat;

	OS_DEBUG_BREAK();
}

#include <memmap/Os_mm_code_end.h>

/* Editor settings; DO NOT DELETE
 * vi:set ts=4:
*/
