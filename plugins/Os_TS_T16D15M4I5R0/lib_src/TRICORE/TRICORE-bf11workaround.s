/* TRICORE-bf11workaround.s
 *
 * This file contains the code to work around the TC1797 silicon bug known
 * internally in AutosarOS as "BF_11".
 *
 * The problem is that the processor incorrectly accepts an interrupt during a small
 * interval immediately after an exception occurs. At this time interrupts should be
 * disabled.
 *
 * For most traps types this is not a problem, because the trap is caused by the context
 * that was executing, and delaying handling the trap until we return to that context
 * (possibly much later after several context-switches) causes no problems as long as
 * (a) the potential extra cost in CSAs is allowed for and (b) the OS doesn't assume
 * that the CSA behind the interrupt CSA is a normal code CSA.
 *
 * However, there are a few traps where the cause is system-wide and where the delayed handling
 * is not acceptable:
 *
 *  - NMI            - most of the causes of NMI need handling immediately (application-dependent).
 *  - 3.1 (FCX==LCX) - if this trap occurs and gets interrupted, the ISR and any resulting
 *                     preemptions might use up all the remaining CSAs on the free-list,
 *                     resulting in the unrecoverable 3.4 (FCX==0) trap.
 *
 * This routine is jumped to by the interrupt vector code if PCXI.IE is 0 (indicating that
 * the interrupt was taken while interrupts were disabled). The vector code places the ISR ID
 * in register D15. Here, the lower context is saved, the ISR ID is transferred to the parameter
 * register D4 and the kernel function OS_TriggerInterrupt() is called. After the return,
 * the lower context is restored and an RFE returns to the interrupted trap vector code
 * with interrupts disabled. After the trap has been handled and interrupts re-enabled
 * the retriggered interrupt will occur once more.
 *
 * If the vector is an unknown interrupt, the vector number is passed to this routine in
 * D15 instead of the ISR ID. The current TRICORE generator orders the ISRs (with IDs 0 to (n-1))
 * in vector locations 1 to n (where n is the number of ISRs configured). The highest valid
 * ISR-ID is therefore (n-1) and the lowest vector number for an unknown interrupt is (n+1),
 * therefore a vector number for an unused interrupt can never be a valid ISR-ID.
 * CAVEAT: this only works as long as the generator compresses the vector table. If the feature
 * is ever disabled for some reason, this bug workaround could retrigger an incorrect ISR
 * if an unconfigured interrupt occurs and interrupts a trap.
 *
 * CHECK: TABS 4 (see editor commands at end of file)
 *
 * Copyright 1998-2014 Elektrobit Automotive GmbH
 * All rights exclusively reserved for Elektrobit Automotive GmbH,
 * unless expressly agreed to otherwise.
 *
 * $Id: TRICORE-bf11workaround.s 17602 2014-02-03 12:09:35Z tojo2507 $
*/
#ifndef OS_ASM
#define OS_ASM
#endif

#include <Os_kernel.h>
#include <Os_tool.h>
#include <TRICORE/Os_TRICORE_core.h>

	_GLOBAL		OS_Bf11Workaround
	_EXTERN		OS_TriggerInterrupt

/* OS_Bf11Workaround
 *
 * Retriggers an interrupt then returns to interrupted context.
 *
 * On entry: ISR-ID (or vector number for unknown interrupts) in d15
*/
	_TEXT

OS_Bf11Workaround:
	svlcx
	mov		d4,d15
	call	OS_TriggerInterrupt
	rslcx
	rfe

/* Editor settings - DO NOT DELETE
 * vi:set ts=4:
*/
