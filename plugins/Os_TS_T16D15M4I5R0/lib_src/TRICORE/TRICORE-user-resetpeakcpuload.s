/* TRICORE-user-resetpeakcpuload.s
 *
 * This file contains the system function OS_UserResetPeakCpuLoad()
 * Generated by the makesc.pl script
 *
 * Copyright 1998-2014 Elektrobit Automotive GmbH
 * All rights exclusively reserved for Elektrobit Automotive GmbH,
 * unless expressly agreed to otherwise.
*/
#ifndef OS_ASM
#define OS_ASM
#endif

#include <Os_tool.h>
#include <Os_syscalls.h>

	_TEXT
	_GLOBAL	OS_UserResetPeakCpuLoad
	_GTYPE(OS_UserResetPeakCpuLoad,@function)
	_GLOBAL	__callee.OS_UserResetPeakCpuLoad.da.v.v
	_GTYPE(__callee.OS_UserResetPeakCpuLoad.da.v.v,@function)

/*!
 *	OS_UserResetPeakCpuLoad()
 *
 *	This routine makes the system call to OS_SC_ResetPeakCpuLoad
*/

_TTYPE(OS_UserResetPeakCpuLoad,FUNC)
_TTYPE(__callee.OS_UserResetPeakCpuLoad.da.v.v,FUNC)

	_entry
	syscall	_IMM(#,OS_SC_ResetPeakCpuLoad)
	ret

/* Editor settings - DO NOT DELETE
 * vi:set ts=4:
*/
