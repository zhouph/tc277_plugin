/* TRICORE-callapphook.c
 *
 * This file contains the OS_CallAppHook wrapper for TRICORE.
 *
 * CHECK: TABS 4 (see editor commands at end of file)
 *
 * Copyright 1998-2014 Elektrobit Automotive GmbH
 * All rights exclusively reserved for Elektrobit Automotive GmbH,
 * unless expressly agreed to otherwise.
 *
 * $Id: TRICORE-callapphook.c 17602 2014-02-03 12:09:35Z tojo2507 $
*/

#include <Os_kernel.h>

#include <memmap/Os_mm_code_begin.h>

/*!
 * OS_CallAppHook
 *
 * This wrapper function has the effect of calling an application-specific
 * hook. Such a hook is not called directly, but "started" in a
 * manner similar to a task. When the hook returns, it triggers a PCX==0
 * exception and the trap handler picks up the stored PCXI and RA and returns
 * to the caller of this function.
*/
void OS_CallAppHook
(	const os_appcontext_t *app,
	os_savedcontext_t *ctxt,
	void *hook,
	os_stacklen_t stackLen,
	os_unsigned_t param
)
{
	/* Ensure that the function call is complete before reading PCXI
	 * This might not be necessary if errata-workaround
	 * OS_TRICORE_BF_05 is necessary, because then there's a DSYNC
	 * in the function entry anyway.
	*/
	OS_CxDsync();

	ctxt->pcxi = OS_MFCR(OS_PCXI);
	ctxt->ra = (os_uint32_t)OS_MFRA();

	OS_CallHook(app, hook, stackLen, ctxt, param);
	/* Never returns */

	/* Force a CALL to OS_CallHook() instead of a JUMP
	 * Should never get here.
	*/
	OS_ShutdownNoHooks();
}

#include <memmap/Os_mm_code_end.h>

/* Editor settings; DO NOT DELETE
 * vi:set ts=4:
*/
