/* TRICORE-callnontrustedisr.c
 *
 * This file contains the OS_CallNontrustedIsr interrupt handler wrapper
 * for TRICORE. It is in a separate file to prevent the Tasking compiler
 * from inlining it :-(
 *
 * CHECK: TABS 4 (see editor commands at end of file)
 *
 * Copyright 1998-2014 Elektrobit Automotive GmbH
 * All rights exclusively reserved for Elektrobit Automotive GmbH,
 * unless expressly agreed to otherwise.
 *
 * $Id: TRICORE-callnontrustedisr.c 17637 2014-02-05 15:58:52Z tojo2507 $
*/

#include <Os_kernel.h>

#include <memmap/Os_mm_code_begin.h>

/*!
 * OS_CallNontrustedIsr()
 *
 * This function sets up its own return address and CSA so that an
 * RFE instruction will jump to the first instruction of the ISR
 * with the registers in the expected state. In particular, the register
 * PCXI (in the ISR) must be zero so that the ISR can get back into the kernel.
 * when it returns.
 * The function never returns!
*/
void OS_CallNontrustedIsr
(	const os_isr_t *isr,
	os_isrdynamic_t *id
)
{
	os_uint32_t pcxi;
	os_uppercx_t *cx;
	const os_appcontext_t *a = OS_GET_APP(isr->app);

	/* Ensure that the function call is complete before reading PCXI
	 * This might not be necessary if errata-workaround
	 * OS_TRICORE_BF_05 is necessary, because then there's a DSYNC
	 * in the function entry anyway.
	*/
	OS_CxDsync();

	pcxi = OS_MFCR(OS_PCXI);
	cx = OS_AddrFromCx(pcxi);

	/* Eventually align stack */
	OS_ALIGN_STACK(cx->a10);
	id->c.stackLimit = (os_uint8_t *)cx->a10;

	OS_SETISRPROTECTION(isr, id);

	/* Load application's PSW. Ensure trace mode is off and that
	 * we use the interrupt stack.
	*/
	cx->psw = a->archapp.psw | OS_PSW_IS | 1u;	/* trace mode -> off */
	cx->a11 = (os_uint32_t)&OS_NullFunction;
	cx->pcxi = 0;
	pcxi = (pcxi & (OS_CX_CX|OS_PCXI_UL)) | OS_PCXI_PIE | ((os_uint32_t)isr->arch.runPrio << OS_PCXI_PCPN_BIT);
	OS_MTCR(OS_PCXI,pcxi);
	OS_MTRA(isr->func);
	OS_MTCR(OS_PSW, OS_PSW_CDC_6|OS_PSW_CDE|OS_PSW_GW|OS_PSW_IS|OS_PSW_IO_SU|OS_PSW_PRS_0);
	OS_RFE();
}

#include <memmap/Os_mm_code_end.h>

/* Editor settings; DO NOT DELETE
 * vi:set ts=4:
*/
