/* kern-iocwrite.c
 *
 * This file contains the function OS_IocWrite()
 *
 * CHECK: TABS 4 (see editor commands at end of file)
 *
 * Copyright 1998-2014 Elektrobit Automotive GmbH
 * All rights exclusively reserved for Elektrobit Automotive GmbH,
 * unless expressly agreed to otherwise.
 *
 * $Id: kern-iocemptyqueue.c 17602 2014-02-03 12:09:35Z tojo2507 $
*/
#include <Os_types.h>
#include <Os_kernel.h>
#include <Os_userkernel.h>
#include <Ioc/Ioc.h>
#include <Ioc/Ioc_kern.h>

os_result_t OS_KernIocEmptyQueue(os_uint32_t id)
{
	os_intstatus_t is;
	os_result_t retval = IOC_E_SEG_FAULT;

	/* check parameter */
	if ( id < Ioc_NumOfQueuedData )
	{
		/* lock data structures */
		is = OS_IOC_LOCKDATA(id);

		/* clear queue */
		Ioc_QMeta[id].count = 0;
		Ioc_QMeta[id].tail = 0;
		Ioc_QMeta[id].dataLost = 0;

		/* unlock data structures */
		OS_IOC_UNLOCKDATA(id, is);

		retval = IOC_E_OK;
	}

	return retval;
}


/* Editor settings - DO NOT DELETE
 * vi:set ts=4:
*/
