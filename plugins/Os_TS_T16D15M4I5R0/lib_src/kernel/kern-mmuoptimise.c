/* kern-mmuoptimise.c
 *
 * This file contains the OS_MmuOptimise function
 *
 * This function optimises a given chunk map by sorting it, rounding the chunks
 * out so that they occupy whole pages (of the smallest page size) and then
 * merging adjacent chunks.
 *
 * CHECK: TABS 4 (see editor commands at end of file)
 *
 * Copyright 1998-2014 Elektrobit Automotive GmbH
 * All rights exclusively reserved for Elektrobit Automotive GmbH,
 * unless expressly agreed to otherwise.
 *
 * $Id: kern-mmuoptimise.c 17602 2014-02-03 12:09:35Z tojo2507 $
*/

#include <Os_kernel.h>
#include <Os_mmu.h>

#include <memmap/Os_mm_code_begin.h>

/*!
 * OS_MmuOptimise
 *
 * This function optimises a region map for further processing.
*/
os_int_t OS_MmuOptimise(os_pagemap_t *chunkmap, os_int_t nchunks)
{
	OS_MmuMapSort(chunkmap, nchunks);
	OS_MmuPagify(chunkmap, nchunks);
	return OS_MmuMapMerge(chunkmap, nchunks);
}

#include <memmap/Os_mm_code_end.h>

/* Editor settings; DO NOT DELETE
 * vi:set ts=4:
*/
