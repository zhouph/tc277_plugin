/* kern-setalarm.c
 *
 * This file contains the OS_SetAlarm function.
 *
 * CHECK: TABS 4 (see editor commands at end of file)
 *
 * Copyright 1998-2014 Elektrobit Automotive GmbH
 * All rights exclusively reserved for Elektrobit Automotive GmbH,
 * unless expressly agreed to otherwise.
 *
 * $Id: kern-setalarm.c 17602 2014-02-03 12:09:35Z tojo2507 $
*/

#include <Os_kernel.h>

#include <memmap/Os_mm_code_begin.h>

/*!
 * OS_SetAlarm
 *
 * The alarm is set to expire in the specified number of timer ticks.
 * The alarm's period is also set as specified.
 *
 * If the increment is 0, the OSEK/VDX OS specification says it is up
 * to the implementation what to do. Here, we have chosen to expire the
 * alarm immediately.
 *
 * !LINKSTO Kernel.API.Alarms.SetRelAlarm.API, 1
 * !LINKSTO Kernel.API.Alarms.SetAbsAlarm.API, 1
*/
os_uint8_t OS_SetAlarm
(	os_alarmid_t a,				/* The alarm (index) */
	os_alarmdynamic_t *ad,		/* The alarm (dynamic stuff) */
	const os_counter_t *cs,		/* The counter (static stuff) */
	os_counterdynamic_t *cd,	/* The counter (dynamic stuff) */
	os_tick_t val,				/* The relative or absolute counter value */
	os_tick_t per,				/* The period */
	os_boolean_t rel			/* TRUE if val is a relative value */
)
{
	os_intstatus_t is;
	os_uint8_t result;

	/* For a hardware counter we need the current value from the hardware,
	 * and we need to update the delta of the head alarm in the queue.
	 *
	 * This is done before disabling interrupts. If a timer interrupt
	 * happens here it is safe.
	 *
	 * If the counter is locked the value is already current.
	*/
	if ( OS_CounterIsHw(cs) && (cd->lock == 0) )
	{
		OS_CtrUpdate(cs, cd);
	}

	is = OS_IntDisable();

	/* Remember the current state of the alarm. This is for error handling
	 * in the caller.
	*/
	result = ad->inUse;

	/* If the alarm is still available, enqueue it
	*/
	if ( result == OS_ALARM_IDLE )
	{
		ad->inUse = OS_ALARM_INUSE;
		ad->period = per;

		/* Calculate an appropriate delta
		*/
		if ( rel )
		{
			/* Relative value: the delta is simply the given value
			*/
			ad->delta = val;
		}
		else
		{
			/* Absolute value: compute the delta with wraparound.
			*/
			ad->delta = OS_CounterSub(val, cd->current, cs->maxallowedvalue);
		}

		OS_EnqueueAlarm(cs, cd, a, ad);

		OS_IntRestore(is);

		/* If our alarm is at the beginning of the queue of a hardware counter we need
		 * to adjust the timer's time-to-next-interrupt to reflect this.
		 * (But not if the counter is locked)
		*/
		if ( OS_CounterIsHw(cs) && (cd->lock == 0) && (cd->head == a) )
		{
			OS_CtrUpdate(cs, cd);
		}
	}
	else
	{
		OS_IntRestore(is);
	}

	return result;
}

#include <memmap/Os_mm_code_end.h>

/* Editor settings; DO NOT DELETE
 * vi:set ts=4:
*/
