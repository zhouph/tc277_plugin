/* kern-setschedulealarm.c
 *
 * This file contains the OS_SetScheduleAlarm function.
 *
 * CHECK: TABS 4 (see editor commands at end of file)
 *
 * Copyright 1998-2014 Elektrobit Automotive GmbH
 * All rights exclusively reserved for Elektrobit Automotive GmbH,
 * unless expressly agreed to otherwise.
 *
 * $Id: kern-setschedulealarm.c 17602 2014-02-03 12:09:35Z tojo2507 $
*/

#include <Os_kernel.h>

#include <memmap/Os_mm_code_begin.h>

/*!
 * OS_SetScheduleAlarm
 *
 * The alarm is set to expire in/at the specified number of timer ticks.
*/
void OS_SetScheduleAlarm
(	const os_schedule_t *ss,	/* The schedule table (static stuff) */
	os_scheduledynamic_t *sd,	/* The schedule table (dynamic stuff) */
	os_tick_t val,				/* The relative or absolute counter value */
	os_boolean_t rel			/* TRUE if val is a relative value */
)
{
	const os_alarm_t *as;
	os_alarmdynamic_t *ad;
	const os_counter_t *cs;
	os_counterdynamic_t *cd;
	os_intstatus_t is;

	as = &OS_alarm[ss->alarm];
	ad = &OS_alarmDynamic[ss->alarm];
	cs = &OS_counter[as->counter];
	cd = &OS_counterDynamic[as->counter];

	/* For a hardware counter we need the current value from the hardware,
	 * and we need to update the delta of the head alarm in the queue.
	 *
	 * This is done before disabling interrupts. If a timer interrupt
	 * happens here it is safe.
	 *
	 * If the counter is locked the value is already current.
	*/
	if ( (!rel) && (OS_CounterIsHw(cs)) && (cd->lock == 0) )
	{
		OS_CtrUpdate(cs, cd);
	}

	is = OS_IntDisable();

	ad->inUse = OS_ALARM_INUSE;
	ad->period = 0;

	/* Calculate an appropriate delta
	*/
	if ( rel )
	{
		/* Relative value: the delta is simply the given value
		*/
		ad->delta = val;
	}
	else
	{
		/* Absolute value. Take account of wrap-around.
		*/
		ad->delta = OS_CounterSub(val, cd->current, cs->maxallowedvalue);
	}

	/* Store the counter value at which the alarm will expire
	*/
	sd->ctrAbs = OS_CounterAdd(cd->current, ad->delta, cs->maxallowedvalue);

	OS_EnqueueAlarm(cs, cd, ss->alarm, ad);

	OS_IntRestore(is);

	/* If our alarm is at the beginning of the queue of a hardware counter we need
	 * to adjust the timer's time-to-next-interrupt to reflect this.
	 * (But not if the counter is locked)
	*/
	if ( OS_CounterIsHw(cs) && (cd->lock == 0) && (cd->head == ss->alarm) )
	{
		OS_CtrUpdate(cs, cd);
	}

}

#include <memmap/Os_mm_code_end.h>

/* Editor settings; DO NOT DELETE
 * vi:set ts=4:
*/
