/* kern-timestampupdate.c
 *
 * This file contains the OS_TimestampUpdate function.
 *
 * CHECK: TABS 4 (see editor commands at end of file)
 *
 * Copyright 1998-2014 Elektrobit Automotive GmbH
 * All rights exclusively reserved for Elektrobit Automotive GmbH,
 * unless expressly agreed to otherwise.
 *
 * $Id: kern-timestampupdate.c 18288 2014-06-12 14:16:37Z stpo8218 $
*/

#include <Os_kernel.h>
#include <Os_timestamp.h>

#include <memmap/Os_mm_code_begin.h>

/*!
 * OS_TimestampUpdate
 *
 * This is called when the hardware timer for the timestamp generates an interrupt.
 * It is used when the hardware timer is solely used for timestamp generation.
 *
 * Internal timestamp timers are not expected to generate an interrupt, so
 * this function is empty in this case.
 *
 * This is an internal function that is only ever called from kernel or
 * generated code.
*/
void OS_TimestampUpdate(void)
{
#if OS_USEGENERICTIMESTAMP == 1
	os_intstatus_t is;
	os_timervalue_t current;

	is = OS_IntDisable();

	/* Clear the timer interrupt
	*/
	OS_HwtStop(OS_timeStampTimer);

	/* Read the timer and advance the timestamp
	*/
	current = OS_HwtRead(OS_timeStampTimer);
	OS_AdvanceTimeStamp(current);

	/* Set the next time. We assume that this is always in the future, so the
	 * return value of OS_HwtStart() is not tested.
	*/
	OS_HwtStart(OS_timeStampTimer, current, OS_timeStampTimer->maxDelta);

	OS_IntRestore(is);
#endif
}

#include <memmap/Os_mm_code_end.h>

/* Editor settings; DO NOT DELETE
 * vi:set ts=4:
*/
