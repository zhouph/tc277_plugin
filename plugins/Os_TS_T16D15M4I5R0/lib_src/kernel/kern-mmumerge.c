/* kern-mmumerge.c
 *
 * This file contains the OS_MmuMapMerge function
 *
 * CHECK: TABS 4 (see editor commands at end of file)
 *
 * Copyright 1998-2014 Elektrobit Automotive GmbH
 * All rights exclusively reserved for Elektrobit Automotive GmbH,
 * unless expressly agreed to otherwise.
 *
 * $Id: kern-mmumerge.c 17602 2014-02-03 12:09:35Z tojo2507 $
*/

#include <Os_kernel.h>
#include <Os_mmu.h>

#include <memmap/Os_mm_code_begin.h>

/*!
 * OS_MmuMapMerge
 *
 * This function optimises an existing MMU page map by joining together
 * adjacent regions that have the same flags.
 * It is assumed that the map array is initially sorted into ascending order of base address.
 * It is further assumed that regions with different flags are disjoint.
 *
 * The new number of maps is returned.
 *
 * MISRA note: all array operations are performed using indexes rather than pointers in conformance with Rule 17.4
 * However, the array parameter is declared as a pointer rather than an array, because, although it might appear
 * clearer locally (subjective!), the call to the function with a partial array would be more confusing.
*/
os_int_t OS_MmuMapMerge(os_pagemap_t *map, os_int_t nmaps)
{
	os_int_t curr = 0;
	os_int_t next = 1;
	os_int_t new_nmaps = nmaps;

	while ( next < nmaps )
	{
		if ( ((map[curr].base + map[curr].size) >= map[next].base) &&
			 ( map[curr].flags == map[next].flags ) )
		{
			if ( (map[curr].base + map[curr].size) >= (map[next].base + map[next].size) )
			{
				/* Next region is contained entirely within current regions and so can be discarded.
				*/
			}
			else
			{
				/* Need to resize the current hunk
				*/
				map[curr].size = map[next].base + map[next].size - map[curr].base;
			}

			/* Move to the next hunk and reduce the number of hunks.
			 * The "current" stays where it is.
			*/
			new_nmaps--;
			next++;
		}
		else
		{
			curr++;

			/* Fill in the gaps as we go along
			*/
			if ( curr != next )
			{
				map[curr] = map[next];
			}

			next++;
		}
	}

	return new_nmaps;
}

#include <memmap/Os_mm_code_end.h>

/* Editor settings; DO NOT DELETE
 * vi:set ts=4:
*/
