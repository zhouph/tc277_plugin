/* kern-getapplicationid.c
 *
 * This file contains the OS_KernGetApplicationId function
 *
 * CHECK: TABS 4 (see editor commands at end of file)
 *
 * Copyright 1998-2014 Elektrobit Automotive GmbH
 * All rights exclusively reserved for Elektrobit Automotive GmbH,
 * unless expressly agreed to otherwise.
 *
 * $Id: kern-getapplicationid.c 17602 2014-02-03 12:09:35Z tojo2507 $
*/

#define OS_SID	OS_SID_GetApplicationId
#define OS_SIF	OS_svc_GetApplicationId

#include <Os_kernel.h>

/* Include definitions for tracing */
#include <Os_trace.h>

#include <memmap/Os_mm_code_begin.h>

/*!
 * OS_KernGetApplicationId implements the API GetApplicationID
 *
 * The id of the current application (or OS_NULLAPP if none) is
 * returned.
 *
 * !LINKSTO Kernel.Autosar.OsApplication.QueryCurrent, 1
 * !LINKSTO Kernel.Autosar.API.SystemServices.GetApplicationID, 2
*/
os_applicationid_t OS_KernGetApplicationId(void)
{
	os_uint8_t inFunction;
	os_applicationid_t a = OS_NULLAPP;
	const os_appcontext_t *app = OS_NULL;

	OS_TRACE_GETAPPLICATIONID_ENTRY();

	if ( !OS_CallingContextCheck() )
	{
		if ( OS_ErrorHandlingForVoidApi() )
		{
			/* can't propagate the return value of OS_ERROR -> ignore it */
			(void) OS_ERROR(OS_ERROR_WrongContext, OS_NULL);
		}
	}
	else
	if ( !OS_InterruptEnableCheck(OS_IEC_AUTOSAR) )
	{
		if ( OS_ErrorHandlingForVoidApi() )
		{
			/* can't propagate the return value of OS_ERROR -> ignore it */
			(void) OS_ERROR(OS_ERROR_InterruptDisabled, OS_NULL);
		}
	}
	else
	{
		/* Select the calling context. For error and protection
		 * hooks we need the parent context.
		*/
		if ( (OS_inFunction == OS_INERRORHOOK) || (OS_inFunction == OS_INPROTECTIONHOOK) )
		{
			inFunction = OS_errorStatus.calledFrom;
		}
		else
		{
			inFunction = OS_inFunction;
		}

		switch (inFunction)
		{
		case OS_INTASK:
		case OS_INPRETASKHOOK:
		case OS_INPOSTTASKHOOK:
			/* In these cases, the current app is the current task's
			 * application.
			*/
			if ( OS_taskCurrent != OS_NULL )
			{
				app = OS_GET_APP(OS_taskCurrent->app);
			}
			break;

		case OS_INCAT1:
		case OS_INCAT2:
		case OS_INPREISRHOOK:
		case OS_INPOSTISRHOOK:
			/* In these cases, the current app is the current ISR's
			 * application.
			*/
			if ( OS_isrCurrent < OS_nInterrupts )
			{
				app = OS_GET_APP(OS_isrTableBase[OS_isrCurrent].app);
			}
			break;

		case OS_INERRORHOOK:
		case OS_INSTARTUPHOOK:
		case OS_INSHUTDOWNHOOK:
			/* In these cases, it might be an application-specific hook.
			*/
			app = OS_hookApp;
			break;

		default:
			/* In all other cases (BOOT, INTERNAL, ACB) there's no application,
			 * so we just fall through leaving app at OS_NULL.
			*/
			break;
		}

		/* Now we look up the app's ID
		*/
		if ( !OS_AppIsNull(app) )
		{
			a = app->appId;
		}
	}

	OS_TRACE_GETAPPLICATIONID_EXIT_P(a);
	return a;
}

/* API entries for User's Guide
 * CHECK: NOPARSE

<api API="OS_USER">
  <name>OS_UserGetApplicationId</name>
  <synopsis>Get the current application</synopsis>
  <syntax>
    os_applicationid_t OS_UserGetApplicationId(void)
  </syntax>
  <description>
    <code>OS_UserGetApplicationId()</code> returns the ID of the current
    application. If no category 2 ISR or task is running, or if the current
    ISR or task does not belong to an application, <code>OS_NULLAPP</code>
    is returned instead.
  </description>
  <availability>
    No restrictions.
  </availability>
  <returns>AppId=ID of current application</returns>
  <returns>OS_NULLAPP=No application is running</returns>
</api>

 * CHECK: PARSE
*/

#include <memmap/Os_mm_code_end.h>

/* Editor settings; DO NOT DELETE
 * vi:set ts=4:
*/
