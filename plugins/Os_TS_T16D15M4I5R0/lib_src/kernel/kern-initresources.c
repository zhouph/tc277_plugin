/* kern-initresources.c
 *
 * This file contains the OS_InitResources function.
 *
 * CHECK: TABS 4 (see editor commands at end of file)
 *
 * Copyright 1998-2014 Elektrobit Automotive GmbH
 * All rights exclusively reserved for Elektrobit Automotive GmbH,
 * unless expressly agreed to otherwise.
 *
 * $Id: kern-initresources.c 17602 2014-02-03 12:09:35Z tojo2507 $
*/

/*
 * MISRA-C:2004 Deviation List
 *
 * MISRA-1) Deviated Rule: 17.4 (required)
 * Array indexing shall be the only allowed form of pointer arithmetic.
 *
 * Reason:
 * Pointer arithmetic is used for classical tasks like iterating through
 * an initialization array to make them more readable and maintainable.
*/

#include <Os_kernel.h>

#include <memmap/Os_mm_code_begin.h>

/* OS_InitResources
 *
 * The dynamic data for each resource needs initialising because the default
 * bss value of 0 has an unwanted meaining in the kernel.
*/
void OS_InitResources(void)
{
	os_unsigned_t i = OS_nResources;
	os_resourcedynamic_t *r = OS_resourceDynamic;

	while ( i > 0 )
	{
		r->lastPrio = 0;
		r->takenBy = OS_NULLTASK;
		r->next = OS_NULLRESOURCE;
		/* Deviation MISRA-1 */
		r++;
		i--;
	}
}

#include <memmap/Os_mm_code_end.h>

/* Editor settings; DO NOT DELETE
 * vi:set ts=4:
*/
