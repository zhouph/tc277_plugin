/* kern-startupcheckscounter.c
 *
 * This file contains the OS_StartupChecksCounter function.
 *
 * CHECK: TABS 4 (see editor commands at end of file)
 *
 * Copyright 1998-2014 Elektrobit Automotive GmbH
 * All rights exclusively reserved for Elektrobit Automotive GmbH,
 * unless expressly agreed to otherwise.
 *
 * $Id: kern-startupcheckscounter.c 17602 2014-02-03 12:09:35Z tojo2507 $
*/

#include <Os_kernel.h>
#include <Os_panic.h>

#include <memmap/Os_mm_code_begin.h>

/* OS_StartupChecksCounter
 *
 * This function performs a variety of checks of the counter configuration
*/
os_result_t OS_StartupChecksCounter(void)
{
	os_result_t result = OS_E_OK;
	os_unsigned_t i;
	const os_counter_t *ctr;

	for ( i = 0; i < OS_nCounters; i++ )
	{
		ctr = &OS_counter[i];

		if ( (OS_GET_APP(ctr->app) != OS_NULL) &&
				((OS_GET_APP(ctr->app->permission) & OS_GET_APP(ctr->permissions)) == 0) )
		{
			result = OS_PANIC(OS_PANIC_SCHK_OwningApplicationHasNoPermission);
		}

		if ( ctr->hwt != OS_NULL )
		{
			/* For hardware counters, the counter's range must be at least the timer's range,
			 * because the modulo arithmetic to permit otherwise is not implemented and would be costly.
			 *
			 * !LINKSTO Kernel.Autosar.Counter.Types.HARDWARE.ExtendedValue.Range, 1
			*/
			if ( ctr->maxallowedvalue < ctr->hwt->wrapMask )
			{
				result = OS_PANIC(OS_PANIC_SCHK_CounterRangeLowerThanHardwareTimer);
			}
		}
	}

	return result;
}

#include <memmap/Os_mm_code_end.h>

/* Editor settings; DO NOT DELETE
 * vi:set ts=4:
*/
