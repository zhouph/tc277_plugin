/* kern-startscheduletablesynchron.c
 *
 * This file contains the OS_KernStartScheduleTableSynchron function.
 *
 * CHECK: TABS 4 (see editor commands at end of file)
 *
 * Copyright 1998-2014 Elektrobit Automotive GmbH
 * All rights exclusively reserved for Elektrobit Automotive GmbH,
 * unless expressly agreed to otherwise.
 *
 * $Id: kern-startscheduletablesynchron.c 18431 2014-07-10 05:31:56Z ingi2575 $
*/
/* TOOLDIAG List of possible tool diagnostics
 *
 * TOOLDIAG-1) Possible diagnostic: SetButNeverUsed
 *   Variable is set but never used.
 *
 * Reason: Not an issue, variable will be used if at least one application exists.
 */

#define OS_SID OS_SID_StartScheduleTableSynchron
#define OS_SIF OS_svc_StartScheduleTableSynchron

#include <Os_kernel.h>

/* Include definitions for tracing */
#include <Os_trace.h>

#include <memmap/Os_mm_code_begin.h>

/*!
 * OS_KernStartScheduleTableSynchron
 *
 * The specified schedule table is placed in the "waiting" state
 * and will start only when SyncScheduleTable is called for it.
 *
 * !LINKSTO Kernel.Autosar.API.SystemServices.StartScheduleTableSynchron, 2
*/
os_result_t OS_KernStartScheduleTableSynchron
(	os_scheduleid_t s
)
{
	const os_schedule_t *ss;
	/* Possible diagnostic TOOLDIAG-1 <1> */
	const os_appcontext_t *app;

	os_result_t r = OS_E_OK;
	os_errorresult_t e;

	OS_PARAMETERACCESS_DECL

	OS_SAVE_PARAMETER_N(0,(os_paramtype_t)s);

	OS_TRACE_STARTSCHEDULETABLESYNCHRON_ENTRY(s);

	if ( !OS_CallingContextCheck() )
	{
		r = OS_ERROR(OS_ERROR_WrongContext, OS_GET_PARAMETER_VAR());
	}
	else
	if ( !OS_InterruptEnableCheck(OS_IEC_AUTOSAR) )
	{
		r = OS_ERROR(OS_ERROR_InterruptDisabled, OS_GET_PARAMETER_VAR());
	}
	else
	if ( s >= OS_nSchedules )
	{
		/* !LINKSTO Kernel.Autosar.API.SystemServices.StartScheduleTableSynchron.Invalid, 2
		*/
		r = OS_ERROR(OS_ERROR_InvalidScheduleId, OS_GET_PARAMETER_VAR());
	}
	else
	{
		ss = &OS_scheduleTableBase[s];
		app = OS_CurrentApp();

		if ( !OS_HasPermission(app, ss->permissions) )
		{
			r = OS_ERROR(OS_ERROR_Permission, OS_GET_PARAMETER_VAR());
		}
		else
		if ( (ss->flags & OS_ST_SYNCABLE) == 0 )
		{
			/* !LINKSTO Kernel.Autosar.API.SystemServices.StartScheduleTableSynchron.NonSynch, 1
			 * !LINKSTO Kernel.Autosar.ScheduleTable.Synchronisation.Strategy.IMPLICIT, 1
			 * !LINKSTO Kernel.Autosar.ScheduleTable.Synchronisation.Strategy.IMPLICIT.Start, 1
			*/
			r = OS_ERROR(OS_ERROR_NotSyncable, OS_GET_PARAMETER_VAR());
		}
		else
		{
			/* !LINKSTO Kernel.Autosar.ScheduleTable.Synchronisation.Strategy.EXPLICIT, 1
			*/
			e = OS_DoStartScheduleTableSynchron(s, ss);

			if ( e != OS_ERRORCODE_NOCHECK(OS_ERROR_NoError) )
			{
				r = OS_ERROR_NOCHECK(e, OS_GET_PARAMETER_VAR());
			}
		}
	}

	OS_TRACE_STARTSCHEDULETABLESYNCHRON_EXIT_P(r,s);
	return r;
}

/* API entries for User's Guide
 * CHECK: NOPARSE

<api API="OS_USER">
  <name>OS_UserStartScheduleTableSynchron</name>
  <synopsis>Start a schedule table synchronously</synopsis>
  <syntax>
    os_result_t OS_UserStartScheduleTableSynchron
    (   os_scheduleid_t s     /@ ID of table @/
    )
  </syntax>
  <description>
    <code>OS_UserStartScheduleTableSynchron()</code> places a schedule table
    into the WAITING state so that it will start synchronously when
    global time becomes available.
  </description>
  <availability>
  </availability>
  <returns>OS_E_OK=Success</returns>
</api>

 * CHECK: PARSE
*/

#include <memmap/Os_mm_code_end.h>

/* Editor settings; DO NOT DELETE
 * vi:set ts=4:
*/
