/* kern-stopscheduletable.c
 *
 * This file contains the OS_KernStopScheduleTable function.
 *
 * CHECK: TABS 4 (see editor commands at end of file)
 *
 * Copyright 1998-2014 Elektrobit Automotive GmbH
 * All rights exclusively reserved for Elektrobit Automotive GmbH,
 * unless expressly agreed to otherwise.
 *
 * $Id: kern-stopscheduletable.c 18431 2014-07-10 05:31:56Z ingi2575 $
*/
/* TOOLDIAG List of possible tool diagnostics
 *
 * TOOLDIAG-1) Possible diagnostic: SetButNeverUsed
 *   Variable is set but never used.
 *
 * Reason: Not an issue, variable will be used if at least one application exists.
 */

/*
 * MISRA-C:2004 Deviation List
 *
 * MISRA-1) Deviated Rule: 17.4 (required)
 * Array indexing shall be the only allowed form of pointer arithmetic.
 *
 * Reason:
 * Pointer arithmetic is used for classical tasks like iterating through
 * an configuration array to make them more readable and maintainable.
*/

#define OS_SID OS_SID_StopScheduleTable
#define OS_SIF OS_svc_StopScheduleTable

#include <Os_kernel.h>

/* Include definitions for tracing */
#include <Os_trace.h>

#include <memmap/Os_mm_code_begin.h>

/*!
 * OS_KernStopScheduleTable implements the API StopScheduleTable
 *
 * The specified schedule table is stopped immediately.
 *
 * !LINKSTO Kernel.Autosar.ScheduleTable.Stop, 1
 * !LINKSTO Kernel.Autosar.API.SystemServices.StopScheduleTable, 2
*/
os_result_t OS_KernStopScheduleTable
(	os_scheduleid_t s
)
{
	const os_schedule_t *ss;
	os_scheduledynamic_t *sd;
	/* Possible diagnostic TOOLDIAG-1 <1> */
	const os_appcontext_t *app;
	os_scheduleid_t chain;
	os_scheduleid_t chainer;
	os_schedulestatus_t oldstatus;

	os_result_t r = OS_E_OK;
	os_intstatus_t is;
	OS_PARAMETERACCESS_DECL

	OS_SAVE_PARAMETER_N(0,(os_paramtype_t)s);

	OS_TRACE_STOPSCHEDULETABLE_ENTRY(s);

	if ( !OS_CallingContextCheck() )
	{
		r = OS_ERROR(OS_ERROR_WrongContext, OS_GET_PARAMETER_VAR());
	}
	else
	if ( !OS_InterruptEnableCheck(OS_IEC_AUTOSAR) )
	{
		r = OS_ERROR(OS_ERROR_InterruptDisabled, OS_GET_PARAMETER_VAR());
	}
	else
	if ( s >= OS_nSchedules )
	{
		/* !LINKSTO Kernel.Autosar.API.SystemServices.StopScheduleTable.Invalid, 1
		*/
		r = OS_ERROR(OS_ERROR_InvalidScheduleId, OS_GET_PARAMETER_VAR());
	}
	else
	{
		ss = &OS_scheduleTableBase[s];
		app = OS_CurrentApp();

		if ( !OS_HasPermission(app, ss->permissions) )
		{
			r = OS_ERROR(OS_ERROR_Permission, OS_GET_PARAMETER_VAR());
		}
		else
		{
			sd = &OS_scheduleDynamicBase[s];

			is = OS_IntDisable();

			oldstatus = sd->status & OS_ST_STATE_X;

			if ( oldstatus == OS_ST_STOPPED )
			{
				/* Stopped or quarantined.
				 *
				 * !LINKSTO Kernel.Autosar.API.SystemServices.StopScheduleTable.NotStarted, 2
				*/
				r = OS_ERROR(OS_ERROR_NotRunning, OS_GET_PARAMETER_VAR());
			}
			else
			{
				chain = sd->chain;

				sd->adjRemaining = 0;
				sd->chain = OS_NULLSCHEDULE;
				sd->next = 0;
				sd->status = OS_ST_STOPPED;

				if ( oldstatus == OS_ST_RUNNING )
				{
					/* OS_KillAlarm complains if the alarm wasn't in the queue, but this is
					 * not an error here - so we ignore the return value. */
					(void) OS_KillAlarm(ss->alarm, OS_ALARM_IDLE);
				}
				else
				if ( oldstatus == OS_ST_CHAINED )
				{
					/* If the schedule table is chained to another, we need to find the other
					 * in order to break the link.
					*/
					sd = &OS_scheduleDynamicBase[0];
					chainer = 0;
					while ( (chainer < OS_nSchedules) && (sd != OS_NULL) )
					{
						if ( sd->chain == s )
						{
							/* Found the chainer - break the link and stop the loop
							*/
							sd->chain = OS_NULLSCHEDULE;
							sd = OS_NULL;
						}
						else
						{
							/* Next, please!
							*/
							chainer++;
							/* Deviation MISRA-1 */
							sd++;
						}
					}
				}
				else
				{
					/* MISRA-C */
				}

				/* Now walk the list of chained schedule tables and set
				 * each one to STOPPED.
				 * At the time of writing, Autosar does not permit more than
				 * one schedule table to be chained because the table to
				 * chain *to* must be running. However, this loop copes
				 * with the possibility that this limitation might be removed
				 * in the future.
				 *
				 * !LINKSTO Kernel.Autosar.API.SystemServices.StopScheduleTable.HasChained, 1
				*/
				while ( chain != OS_NULLSCHEDULE )
				{
					sd = &OS_scheduleDynamicBase[chain];
					sd->status = OS_ST_STOPPED;
					chain = sd->chain;
					sd->chain = OS_NULLSCHEDULE;
				}
			}

			OS_IntRestore(is);
		}
	}

	OS_TRACE_STOPSCHEDULETABLE_EXIT_P(r,s);
	return r;
}

/* API entries for User's Guide
 * CHECK: NOPARSE

<api API="OS_USER">
  <name>OS_UserStopScheduleTable</name>
  <synopsis>Stop a schedule table</synopsis>
  <syntax>
    os_result_t OS_UserStopScheduleTable
    (   os_scheduleid_t s    /@ ID of table @/
    )
  </syntax>
  <description>
    <code>OS_UserStopScheduleTable()</code> stops a schedule table
    immediately. If another schedule table has been chained behind
    the specified schedule table, that chained table is also placed
    in the STOPPED state. If the specified schedule table is itself
    in the CHAINED state, the chaining link is broken.
  </description>
  <availability>
  </availability>
  <returns>OS_E_OK=Success</returns>
</api>

 * CHECK: PARSE
*/

#include <memmap/Os_mm_code_end.h>

/* Editor settings; DO NOT DELETE
 * vi:set ts=4:
*/
