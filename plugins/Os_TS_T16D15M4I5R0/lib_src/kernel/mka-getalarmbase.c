/* mka-getalarmbase.c
 *
 * This file contains the GetAlarmBase function.
 *
 * CHECK: TABS 4 (see editor commands at end of file)
 *
 * Copyright 1998-2014 Elektrobit Automotive GmbH
 * All rights exclusively reserved for Elektrobit Automotive GmbH,
 * unless expressly agreed to otherwise.
 *
 * $Id: mka-getalarmbase.c 17602 2014-02-03 12:09:35Z tojo2507 $
*/
#include <Os_kernel.h>
#include <public/Mk_autosar.h>
#include <Os_api_microkernel.h>

/*
 * GetAlarmBase() places the values of the configuration parameters for the specified alarm
 * into the referenced variable.
 *
 * WARNING: this function cannot be ASIL because it calls a QM function in the OS.
*/
StatusType GetAlarmBase(AlarmType alarmId, AlarmBaseRefType albase)
{
	return OS_KernGetAlarmBase(alarmId, albase);
}

/* Editor settings; DO NOT DELETE
 * vi:set ts=4:
*/
