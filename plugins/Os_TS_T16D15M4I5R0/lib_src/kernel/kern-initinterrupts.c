/* kern-initinterrupts.c
 *
 * Copyright 1998-2014 Elektrobit Automotive GmbH
 * All rights exclusively reserved for Elektrobit Automotive GmbH,
 * unless expressly agreed to otherwise.
 *
 * $Id: kern-initinterrupts.c 17602 2014-02-03 12:09:35Z tojo2507 $
*/

/*
 * MISRA-C:2004 Deviation List
 *
 * MISRA-1) Deviated Rule: 17.4 (required)
 * Array indexing shall be the only allowed form of pointer arithmetic.
 *
 * Reason:
 * Pointer arithmetic is used for classical tasks like iterating through
 * an initialization array to make them more readable and maintainable.
*/

#include <Os_kernel.h>

#include <memmap/Os_mm_code_begin.h>

/* OS_InitInterrupts()
 *
 * This function initialises all the interrupt sources configured by the
 * generator.
*/
void OS_InitInterrupts(void)
{
	os_unsigned_t i;
	const os_isr_t *isr = OS_isrTable;
	os_isrdynamic_t *isrd = OS_isrDynamic;

	for ( i = 0; i < OS_nInterrupts; i++ )
	{
		OS_SetupIsr(isr);

		if ( (isr->flags & OS_ISRF_ENABLE) != 0 )
		{
			OS_EnableIsr(isr);
		}
		else
		{
			isrd->statusflags = OS_ISRF_BLOCKED;
		}

		/* Deviation MISRA-1 <+2> */
		isr++;
		isrd++;
	}
}

#include <memmap/Os_mm_code_end.h>

/* Editor settings; DO NOT DELETE
 * vi:set ts=4:
*/
