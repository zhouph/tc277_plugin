/* kern-permittedcontextosek.c
 *
 * This file contains the OS_permittedContextOsek array
 *
 * CHECK: TABS 4 (see editor commands at end of file)
 *
 * Copyright 1998-2014 Elektrobit Automotive GmbH
 * All rights exclusively reserved for Elektrobit Automotive GmbH,
 * unless expressly agreed to otherwise.
 *
 * $Id: kern-permittedcontextosek.c 17602 2014-02-03 12:09:35Z tojo2507 $
*/

#include <Os_kernel.h>

#include <memmap/Os_mm_code_begin.h>

/* OS_permittedContextOsek
 *
 * This table implements the relaxed OSEK/VDX rules for calling context.
 * Using this table the only checks that are made are for errors that
 * would cause problems for the kernel. The calling contexts for
 * Autosar APIs remain restrictive as for Autosar, because selecting this
 * table is only meant for backwards compatibility.
 *
 * The table contains an entry for each system service, identified by its
 * SID (see Os_error.h).
 *
 * A system-service's entry in this table contains a bit for each possible
 * calling context. If the bit is 1, the service is permitted to be called
 * from the context.
*/
const os_callermask_t OS_permittedContextOsek[OS_SID_XXX] =
{
	OS_INTASKMASK|						/* GetApplicationId			*/
	OS_INCAT2MASK|
	OS_INERRORHOOKMASK|
	OS_INPRETASKHOOKMASK|
	OS_INPOSTTASKHOOKMASK|
	OS_INSTARTUPHOOKMASK|
	OS_INSHUTDOWNHOOKMASK|
	OS_INACBMASK|
	OS_INPROTECTIONHOOKMASK,

	OS_INTASKMASK|						/* GetIsrId					*/
	OS_INCAT2MASK|
	OS_INCAT1MASK|
	OS_INACBMASK|
	OS_INERRORHOOKMASK|
	OS_INPROTECTIONHOOKMASK,

	OS_INTASKMASK|						/* CallTrustedFunction		*/
	OS_INCAT2MASK,

	OS_INTASKMASK|						/* CheckIsrMemoryAccess		*/
	OS_INCAT2MASK|
	OS_INERRORHOOKMASK|
	OS_INPROTECTIONHOOKMASK,

	OS_INTASKMASK|						/* CheckTaskMemoryAccess	*/
	OS_INCAT2MASK|
	OS_INERRORHOOKMASK|
	OS_INPROTECTIONHOOKMASK,

	OS_INTASKMASK|						/* CheckObjectAccess		*/
	OS_INCAT2MASK|
	OS_INERRORHOOKMASK|
	OS_INPROTECTIONHOOKMASK,

	OS_INTASKMASK|						/* CheckObjectOwnership		*/
	OS_INCAT2MASK|
	OS_INERRORHOOKMASK|
	OS_INPROTECTIONHOOKMASK,

	OS_INTASKMASK|						/* StartScheduleTableRel	*/
	OS_INCAT2MASK|
	OS_ININTERNALMASK,

	OS_INTASKMASK|						/* StartScheduleTableAbs	*/
	OS_INCAT2MASK|
	OS_ININTERNALMASK,

	OS_INTASKMASK|						/* StopScheduleTable		*/
	OS_INCAT2MASK,

	OS_INTASKMASK|						/* ChainScheduleTable/NextScheduleTable	*/
	OS_INCAT2MASK,

	OS_INTASKMASK|						/* StartScheduleTableSynchron	*/
	OS_INCAT2MASK|
	OS_ININTERNALMASK,

	OS_INTASKMASK|						/* SyncScheduleTable		*/
	OS_INCAT2MASK,

	OS_INTASKMASK|						/* SetScheduleTableAsync	*/
	OS_INCAT2MASK,

	OS_INTASKMASK|						/* GetScheduleTableStatus	*/
	OS_INCAT2MASK,

	OS_INTASKMASK|						/* Advance/IncrementCounter	*/
	OS_INCAT2MASK|
	OS_INACBMASK|
	OS_INSTARTUPHOOKMASK|
	OS_INERRORHOOKMASK|
	OS_INPROTECTIONHOOKMASK|
	OS_ININTERNALMASK,

	OS_INTASKMASK|						/* GetCounterValue	*/
	OS_INCAT2MASK,

	OS_INTASKMASK|						/* GetElapsedCounterValue/GetElapsedValue	*/
	OS_INCAT2MASK,

	OS_INTASKMASK|						/* TerminateApplication		*/
	OS_INCAT2MASK|
	OS_INERRORHOOKMASK,

	OS_INTASKMASK|						/* AllowAccess				*/
	OS_INCAT2MASK,

	OS_INTASKMASK|						/* GetApplicationState		*/
	OS_INCAT2MASK|
	OS_INERRORHOOKMASK|
	OS_INPRETASKHOOKMASK|
	OS_INPOSTTASKHOOKMASK|
	OS_INSTARTUPHOOKMASK|
	OS_INSHUTDOWNHOOKMASK|
	OS_INPROTECTIONHOOKMASK,

	OS_INALLMASK,						/* UnknownSyscall			*/

	OS_INTASKMASK|						/* ActivateTask				*/
	OS_INCAT2MASK|
	OS_INACBMASK|
	OS_INSTARTUPHOOKMASK|
	OS_INERRORHOOKMASK|
	OS_INPROTECTIONHOOKMASK|
	OS_ININTERNALMASK,

	OS_INTASKMASK,						/* TerminateTask			*/

	OS_INTASKMASK,						/* ChainTask				*/

	OS_INTASKMASK,						/* Schedule					*/

	OS_INALLMASK,						/* GetTaskId				*/

	OS_INALLMASK,						/* GetTaskState				*/

	OS_INTASKMASK|						/* SuspendOSInterrupts,SuspendAllInterrupts,DisableAllInterrupts	*/
	OS_INCAT2MASK|
	OS_INCAT1MASK|
	OS_INERRORHOOKMASK|
	OS_INPRETASKHOOKMASK|
	OS_INPOSTTASKHOOKMASK|
	OS_INSTARTUPHOOKMASK|
	OS_INSHUTDOWNHOOKMASK|
	OS_INPROTECTIONHOOKMASK|
	OS_INACBMASK|
	OS_INBOOTMASK,

	OS_INTASKMASK|						/* ResumeOSInterrupts,ResumeAllInterrupts,EnableAllInterrupts		*/
	OS_INCAT2MASK|
	OS_INCAT1MASK|
	OS_INERRORHOOKMASK|
	OS_INPRETASKHOOKMASK|
	OS_INPOSTTASKHOOKMASK|
	OS_INSTARTUPHOOKMASK|
	OS_INSHUTDOWNHOOKMASK|
	OS_INPROTECTIONHOOKMASK|
	OS_INACBMASK|
	OS_INBOOTMASK,

	OS_INTASKMASK|						/* GetResource				*/
	OS_INCAT2MASK,

	OS_INTASKMASK|						/* ReleaseResource			*/
	OS_INCAT2MASK,

	OS_INTASKMASK|						/* SetEvent					*/
	OS_INCAT2MASK|
	OS_INACBMASK|
	OS_INSTARTUPHOOKMASK|
	OS_INERRORHOOKMASK|
	OS_INPROTECTIONHOOKMASK|
	OS_ININTERNALMASK,

	OS_INTASKMASK,						/* ClearEvent				*/

	OS_INALLMASK,						/* GetEvent					*/

	OS_INTASKMASK,						/* WaitEvent				*/

	OS_INALLMASK,						/* GetAlarmBase				*/

	OS_INALLMASK,						/* GetAlarm					*/

	OS_INTASKMASK|						/* SetRelAlarm				*/
	OS_INCAT2MASK|
	OS_INACBMASK|
	OS_INSTARTUPHOOKMASK|
	OS_INERRORHOOKMASK|
	OS_INPROTECTIONHOOKMASK|
	OS_ININTERNALMASK,

	OS_INTASKMASK|						/* SetAbsAlarm				*/
	OS_INCAT2MASK|
	OS_INACBMASK|
	OS_INSTARTUPHOOKMASK|
	OS_INERRORHOOKMASK|
	OS_INPROTECTIONHOOKMASK|
	OS_ININTERNALMASK,

	OS_INTASKMASK|						/* CancelAlarm				*/
	OS_INCAT2MASK|
	OS_INACBMASK|
	OS_INSTARTUPHOOKMASK|
	OS_INERRORHOOKMASK|
	OS_INPROTECTIONHOOKMASK,

	OS_INALLMASK,						/* GetActiveApplicationMode	*/

	OS_INBOOTMASK,						/* StartOs					*/

	OS_INTASKMASK|						/* ShutdownOs				*/
	OS_INCAT2MASK|
	OS_INACBMASK|
	OS_INSTARTUPHOOKMASK|
	OS_INPROTECTIONHOOKMASK|
	OS_INERRORHOOKMASK|
	OS_INSTARTUPHOOKMASK|
	OS_ININTERNALMASK,

	OS_INTASKMASK|						/* GetStackInfo				*/
	OS_INCAT2MASK,

	OS_INTASKMASK|						/* DisableInterruptSource	*/
	OS_INCAT2MASK,

	OS_INTASKMASK|						/* EnableInterruptSource	*/
	OS_INCAT2MASK
};

#include <memmap/Os_mm_code_end.h>

/* Editor settings; DO NOT DELETE
 * vi:set ts=4:
*/
