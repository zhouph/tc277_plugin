/* kern-getalarm.c
 *
 * This file contains the OS_KernGetAlarm function.
 *
 * CHECK: TABS 4 (see editor commands at end of file)
 *
 * Copyright 1998-2014 Elektrobit Automotive GmbH
 * All rights exclusively reserved for Elektrobit Automotive GmbH,
 * unless expressly agreed to otherwise.
 *
 * $Id: kern-getalarm.c 18431 2014-07-10 05:31:56Z ingi2575 $
*/

#define OS_SID	OS_SID_GetAlarm
#define OS_SIF	OS_svc_GetAlarm

#include <Os_kernel.h>
#include <Os_panic.h>

/* Include definitions for tracing */
#include <Os_trace.h>

#include <memmap/Os_mm_code_begin.h>

/*!
 * OS_KernGetAlarm
 *
 * The total number of ticks remaining before the alarm expires
 * is written to the indicated location.
 *
 * !LINKSTO Kernel.API.Alarms.GetAlarm.API, 1
*/
os_result_t OS_KernGetAlarm(os_alarmid_t a, os_tick_t *out)
{
	os_result_t r = OS_E_OK;
	const os_alarm_t *as;
	os_alarmdynamic_t *ad;
	os_counterdynamic_t *cd;
	os_intstatus_t is;
	os_int_t gadRes;
	OS_PARAMETERACCESS_DECL

	OS_SAVE_PARAMETER_N(0,(os_paramtype_t)a);
	OS_SAVE_PARAMETER_N(1,(os_paramtype_t)out);

	OS_TRACE_GETALARM_ENTRY(a);

	if ( !OS_CallingContextCheck() )
	{
		r = OS_ERROR(OS_ERROR_WrongContext, OS_GET_PARAMETER_VAR());
	}
	else
	if ( !OS_InterruptEnableCheck(OS_IEC_AUTOSAR) )
	{
		r = OS_ERROR(OS_ERROR_InterruptDisabled, OS_GET_PARAMETER_VAR());
	}
	else
	if ( OS_ArchCanWrite(out, sizeof(*out)) == 0 )
	{
		r = OS_ERROR(OS_ERROR_WriteProtect, OS_GET_PARAMETER_VAR());
	}
	else
	if ( ! OS_IsValidAlarmId( a ) )
	{
		/* !LINKSTO Kernel.API.Alarms.GetAlarm.UnknownAlarm, 1
		*/
		r = OS_ERROR(OS_ERROR_InvalidAlarmId, OS_GET_PARAMETER_VAR());
	}
	else
	{
		as = &OS_alarmTableBase[a];
		ad = &OS_alarmDynamicBase[a];

		is = OS_IntDisable();

		/* Call internal function to do the job, If it returns
		 * nonzero, something was wrong:
		 *  > 0 --> Alarm not in use
		 *  < 0 --> Corrupt alarm list
		*/
		gadRes = OS_GetAlarmDelta(a, as, ad, out);

		OS_IntRestore(is);

		if ( gadRes < 0 )
		{
			r = OS_PANIC(OS_PANIC_InvalidAlarmState);
		}
		else
		if ( gadRes > 0 )
		{
			/* !LINKSTO Kernel.API.Alarms.GetAlarm.UnusedAlarm, 1
			*/
			r = OS_ERROR(OS_ERROR_AlarmNotInUse, OS_GET_PARAMETER_VAR());
		}
		else
		{
			/* No need to keep interrupts disabled for this:
			 *  - if the counter is locked neither the counter's delta nor the error term will
			 *    change in an ISR.
			 *  - if the counter is not locked the error term will always be zero even if the
			 *    alarm's delta has changed sine we calculated it.
			*/
			cd = &OS_counterDynamicBase[as->counter];

			if ( *out <= cd->error )
			{
				*out = 0;
			}
			else
			{
				*out -= cd->error;
			}
		}
	}

	OS_TRACE_GETALARM_EXIT_P(r,a);
	return r;
}

/* API entries for User's Guide
 * CHECK: NOPARSE

<api API="OS_USER">
  <name>OS_UserGetAlarm</name>
  <synopsis>Get the time remaining on the alarm</synopsis>
  <syntax>
    os_result_t OS_UserGetAlarm
    (   os_alarmid_t a,   /@ ID of the alarm @/
        os_tick_t *out    /@ Where to put the answer @/
    )
  </syntax>
  <description>
    <code>OS_UserGetAlarm()</code> calculates the time remaining before
    the specified alarm expires and places the result in the designated
    <code>out</code> variable. If the alarm is not in use or another
    error is detected, the <code>out</code> variable remains unchanged.
  </description>
  <availability>
  </availability>
  <returns>OS_E_OK=Success</returns>
</api>

 * CHECK: PARSE
*/

#include <memmap/Os_mm_code_end.h>

/* Editor settings; DO NOT DELETE
 * vi:set ts=4:
*/
