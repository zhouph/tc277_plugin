/* kern-shutdown.c
 *
 * This file contains the OS_Shutdown function.
 * The function does the job of shutting down the kernel.
 *
 * CHECK: TABS 4 (see editor commands at end of file)
 *
 * Copyright 1998-2014 Elektrobit Automotive GmbH
 * All rights exclusively reserved for Elektrobit Automotive GmbH,
 * unless expressly agreed to otherwise.
 *
 * $Id: kern-shutdown.c 19903 2014-11-20 15:16:20Z thdr9337 $
*/

/* TOOLDIAG List of possible tool diagnostics
 *
 * TOOLDIAG-1) Possible diagnostic: InfiniteLoop
 *   Possible infinite loop.
 *
 * Reason: Infinite loop is wanted here.
 */

/*
 * MISRA-C:2004 Deviation List
 *
 * MISRA-1) Deviated Rule: 17.4 (required)
 * Array indexing shall be the only allowed form of pointer arithmetic.
 *
 * Reason:
 * Pointer arithmetic is used for classical tasks like iterating through
 * an configuration array to make them more readable and maintainable.
 *
 *
 * MISRA-2) Deviated Rule: 16.2 (required)
 * Functions shall not call themselves, either directly or indirectly.
 *
 * Reason:
 * Centralized error-handling facility (OS_Error, OS_ErrorAction) which
 * provides several different reactions for detected errors, including
 * system shutdown or the call of a hook function. Functions for
 * triggering these reactions (e.g. OS_CallErrorHook, OS_Shutdown)
 * also handle errors using OS_Error. Implementation of OS_Error and
 * OS_ErrorAction ensures that no recursion is possible.
*/

#define OS_SID	OS_SID_ShutdownOs
#define OS_SIF	OS_svc_ShutdownOs

#include <Os_kernel.h>

/* Include definitions for tracing */
#include <Os_trace.h>

#include <memmap/Os_mm_code_begin.h>

/*!
 * OS_Shutdown
 *
 * This function shuts down the OS. Interrupts are disabled at the
 * "disable all" level so that category 1 interrupts are blocked,
 * but any higher priority interrupts remain enabled.
 * This means that a future OSEK/Time subsystem would continue to run.
 *
 * Look! No Post-task hooks!
 * !LINKSTO Kernel.Autosar.ServiceErrors.Miscellaneous.Shutdown.PostTaskHook, 1
*/
/* Deviation MISRA-2 */
void OS_Shutdown(os_result_t code)
{
	OS_inFunction = OS_ININTERNAL;

	/* !LINKSTO Kernel.HookRoutines.PrioISRC2, 1
	 *
	 * In fact, we block Cat1 interrupts too:
	 * !LINKSTO Kernel.Autosar.OSEK.Differences.ShutdownOS, 1
	 *
	 * The call to OS_IntDisableConditional) *after* OS_IntDisableAll() ensures that neither Cat1 nor Execution
	 * Timer interrupts can get through, no matter what their relative levels are. (ASCOS-1475)
	 *
	 * NOTE: This may have to be refined for multicore because the cross-core interrupt - which will be blocked
	 * by OS_IntDisableConditional() - will need to be opened at some stage.
	*/
	(void) OS_IntDisableAll(); /* old level discarded */
	(void) OS_IntDisableConditional(); /* old level discarded */
	OS_SHUTDOWNEXECTIMING();

	/* Call all the application-specific shutdown hooks. This is done
	 * BEFORE the global shutdown hook, according to the Autosar spec.
	 *
	 * !LINKSTO Kernel.Autosar.OsApplication.ApplicationHooks.ShutdownHook.Order, 1
	*/
	if ( OS_appsStarted != 0 )
	{
		/* Deviation MISRA-1 */
		OS_CALLAPPSHUTDOWNHOOKS(code);
	}

	/* !LINKSTO Kernel.API.Hooks.ShutdownHook.API, 1
	 * !LINKSTO Kernel.API.Hooks.ShutdownHook.Shutdown, 1
	 * !LINKSTO Kernel.HookRoutines.PrioISRC2, 1
	 *		Interrupts are disabled here
	*/
	OS_CALLSHUTDOWNHOOK(code);

	OS_ShutdownNoHooks();
}

void OS_ShutdownNoHooks(void)
{
	/* Set inFunction to SHUTDOWN state before spinning forever. This
	 * could be used by (eg) a future mixed OSEK/Time system to determine
	 * the state of the underlying OSEK/OS.
	*/
	OS_inFunction = OS_INSHUTDOWN;

	/* Signal shutdown to tracing. */
	OS_HW_TRACE_TASK(OS_NULLTASK);
	OS_HW_TRACE_ISR(OS_NULLISR);

	/* Endless loop if the shutdown hook ever returns. When called from OS_Shutdown() (above),
	 * "all" interrupts are disabled.
	 *
	 * !LINKSTO Kernel.API.OSControl.ShutdownOS.Shutdown, 1
	 * !LINKSTO Kernel.Autosar.OSEK.Differences.ShutdownOS, 1
	 *
	*/
	/* Possible diagnostic TOOLDIAG-1 <1> */
	for (;;)
	{
		/* wait forever */
	}
}

#include <memmap/Os_mm_code_end.h>

/* Editor settings; DO NOT DELETE
 * vi:set ts=4:
*/
