/* kern-mmuenterprotectedmode.c
 *
 * This file contains the OS_EnterProtectedMode function for processors witn an MMU.
 *
 * CHECK: TABS 4 (see editor commands at end of file)
 *
 * Copyright 1998-2014 Elektrobit Automotive GmbH
 * All rights exclusively reserved for Elektrobit Automotive GmbH,
 * unless expressly agreed to otherwise.
 *
 * $Id: kern-mmuenterprotectedmode.c 19212 2014-10-13 09:13:24Z mist9353 $
*/

#include <Os_kernel.h>
#include <Os_mmu.h>
#include <Os_memalloc.h>

#include <memmap/Os_mm_var16_begin.h>
os_objecttype_t OS_mmuOwnerId;
#include <memmap/Os_mm_var16_end.h>

#include <memmap/Os_mm_var8_begin.h>
os_objecttype_t OS_mmuOwnerType;
#include <memmap/Os_mm_var8_end.h>

#include <memmap/Os_mm_var_begin.h>
os_int_t OS_nFreePageMaps;
os_int_t OS_maxObjectMaps;
os_int_t OS_nTrustedPageMaps;
os_archpagemap_t *OS_trustedPageMaps;
#include <memmap/Os_mm_var_end.h>


#include <memmap/Os_mm_code_begin.h>

/* OS_EnterProtectedMode
 *
 * This function is OS_EnterProtectedMode() for CPUs that have a Memory Management Unit
 *
 * This function switches the CPU from its normal startup mode into "protected" mode by calling the
 * architecture-dependent function OS_InitMmu().
 * It then sets up all the page mappings for non-trusted tasks, ISRs and applications.
 * Initialising the MMU must be done first because this function will typically calculate
 * how many free page mappings are available to be used by the mapping functions that are called
 * later.
 *
 * !LINKSTO Kernel.Autosar.Protection.HardwareProtection, 1
*/
void OS_EnterProtectedMode(void)
{
	os_taskid_t taskid;
	const os_task_t *task;
	os_isrid_t isrid;
	const os_isr_t *isr;
	os_applicationid_t appid;
	const os_appcontext_t *app;
	os_int_t nErrors = 0;

	/* Initialise the memory allocator
	 * This should really be done elsewhere, but since the only use for the memory allocator at the moment
	 * is the MMU memory mapping tables, here is where it shall temporarily reside.
	*/
	OS_InitMem((void *)OS_heapBase, OS_heapSize);

	/* Set the MMU "owner" to nobody and clear the max. object maps variable.
	*/
	OS_mmuOwnerType = OS_OBJ_NONE;
	OS_maxObjectMaps = 0;

	/* Initialise the MMU. This should set up the global pages and the variables by which we
	 * know how many free entries exist and where they are.
	*/
	OS_InitMmu();

	/* Go through all the tasks, ISRs and applications and set up the TLB array for each non-trusted
	 * object.
	*/
	for ( taskid = 0; taskid < OS_nTasks; taskid++ )
	{
		task =  OS_TidToTp(taskid);
		app = OS_GET_APP(task->app);
		if ( !OS_AppIsTrusted(app) )
		{
			/* Task belongs to a non-trusted application. We need to set up its TLB map
			*/
			if ( OS_MmuTaskMapper(taskid) != OS_E_OK )
			{
				nErrors++;
			}
		}
	}

	for ( isrid = 0; isrid < OS_nInterrupts; isrid++ )
	{
		isr =  &OS_isrTableBase[isrid];
		app = OS_GET_APP(isr->app);
		if ( !OS_AppIsTrusted(app) )
		{
			/* ISR belongs to a non-trusted application. We need to set up its TLB map
			*/
			if ( OS_MmuIsrMapper(isrid) != OS_E_OK )
			{
				nErrors++;
			}
		}
	}

	for ( appid = 0; appid < OS_nApps; appid++ )
	{
		app =  &OS_appTableBase[appid];
		if ( ( (app->flags & OS_APP_TRUSTED) == 0 ) &&
			 ( (app->startupHook != OS_NULL) || (app->shutdownHook != OS_NULL) || (app->errorHook != OS_NULL) )	)
		{
			/* Application is non-trusted and has one or more hook functions. We need to set up its TLB map
			*/
			if ( OS_MmuAppMapper(appid) != OS_E_OK )
			{
				nErrors++;
			}
		}
	}

	/* Perform any post-initialisation that is necessary for the MMU.
	*/
	OS_PostInitMmu();

	if ( OS_nTrustedPageMaps != 0 )
	{
		/* Go through all the tasks, ISRs and applications and add the global mappings to every
		 * TRUSTED object.
		*/
		for ( taskid = 0; taskid < OS_nTasks; taskid++ )
		{
			task =  OS_TidToTp(taskid);
			app = OS_GET_APP(task->app);
			if ( OS_AppIsTrusted(app) )
			{
				/* Task belongs to a trusted application. We need to set up its TLB map
				*/
				OS_ArchSetTaskPagemap(task->dynamic, OS_nTrustedPageMaps, OS_trustedPageMaps);
			}
		}

		for ( isrid = 0; isrid < OS_nInterrupts; isrid++ )
		{
			isr =  &OS_isrTableBase[isrid];
			app = OS_GET_APP(isr->app);
			if ( OS_AppIsTrusted(app) )
			{
				/* ISR belongs to a trusted application. We need to set up its TLB map
				*/
				OS_ArchSetIsrPagemap(&OS_isrDynamicBase[isrid], OS_nTrustedPageMaps, OS_trustedPageMaps);
			}
		}

		for ( appid = 0; appid < OS_nApps; appid++ )
		{
			app =  &OS_appTableBase[appid];
			if ( ( (app->flags & OS_APP_TRUSTED) != 0 ) &&
				 ( (app->startupHook != OS_NULL) || (app->shutdownHook != OS_NULL) || (app->errorHook != OS_NULL) )	)
			{
				/* Application is -trusted and has one or more hook functions. We need to set up its TLB map
				*/
				OS_ArchSetAppPagemap(&OS_appDynamicBase[appid], OS_nTrustedPageMaps, OS_trustedPageMaps);
			}
		}
	}

	/* Last chance to trap errors.
	 *
	 * NOTE to potential optimisers: please do not remove this branch and its endless loop.
	 * If all error handling is disabled and errors are ignored we end up here. We must not go
	 * any further than this.
	 * It doesn't make sense to try and report the error here, or even to try and shut down. That has
	 * already been attempted at the source of error and has obviously failed for some reason.
	*/
	if ( nErrors != 0 )
	{
		for (;;)
		{
			/* Wait here until help arrives. */
		}
	}

}

#include <memmap/Os_mm_code_end.h>

/* Editor settings; DO NOT DELETE
 * vi:set ts=4:
*/
