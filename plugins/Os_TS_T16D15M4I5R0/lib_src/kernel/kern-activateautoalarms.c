/* kern-activateautoalarms.c
 *
 * This file contains the OS_ActivateAutoAlarms function.
 *
 * CHECK: TABS 4 (see editor commands at end of file)
 *
 * Copyright 1998-2014 Elektrobit Automotive GmbH
 * All rights exclusively reserved for Elektrobit Automotive GmbH,
 * unless expressly agreed to otherwise.
 *
 * $Id: kern-activateautoalarms.c 17602 2014-02-03 12:09:35Z tojo2507 $
*/

#include <Os_kernel.h>

#include <memmap/Os_mm_code_begin.h>

/* OS_ActivateAutoAlarms()
 *
 * Activate all alarms that are specified for the requested mode
*/
void OS_ActivateAutoAlarms(void)
{
	os_uint16_t idx;
	const os_autoalarm_t *aa;
	os_alarmid_t a;

	idx = OS_startModeAlarms[OS_mode];
	aa = &OS_autoStartAlarms[idx];
	a = aa->alarm;

	while ( a < OS_nAlarms )
	{
		if ( aa->method == OS_STARTMETHOD_ABS )
		{
			/* !LINKSTO Kernel.Autosar.OSEK.Differences.AlarmAutostartAbs, 1
			*/
			/* no way to propagate errors (and, unless there's an error in the
			 * configuration, none are expected anyways) -> ignore return value */
			(void) OS_KernSetAbsAlarm(a, aa->interval, aa->cycle);
		}
		else
		{
			/* An incorrectly generated method will start "relative".
			*/
			/* no way to propagate errors, see above -> ignore return value */
			(void) OS_KernSetRelAlarm(a, aa->interval, aa->cycle);
		}
		idx++;
		aa = &OS_autoStartAlarms[idx];
		a = aa->alarm;
	}
}

#include <memmap/Os_mm_code_end.h>

/* Editor settings; DO NOT DELETE
 * vi:set ts=4:
*/
