/* kern-mmusethookprotection.c
 *
 * This file contains the OS_SetHookProtection function for architectures with an MMU.
 *
 * !LINKSTO Kernel.Autosar.Protection.HardwareProtection, 1
 *
 * CHECK: TABS 4 (see editor commands at end of file)
 *
 * Copyright 1998-2014 Elektrobit Automotive GmbH
 * All rights exclusively reserved for Elektrobit Automotive GmbH,
 * unless expressly agreed to otherwise.
 *
 * $Id: kern-mmusethookprotection.c 17602 2014-02-03 12:09:35Z tojo2507 $
*/

#include <Os_kernel.h>
#include <Os_mmu.h>

#include <memmap/Os_mm_code_begin.h>

/* OS_SetHookProtection
 *
 * This function loads the page table for the given Hook into the MMU
 *
 * If the application already "owns" the MMU nothing is done.
 *
 * !LINKSTO Kernel.Autosar.Protection.HardwareProtection, 1
*/
void OS_SetHookProtection
(	const os_appcontext_t *app,
	os_savedcontext_t *ctxt,
	os_size_t stackLen
)
{
	os_appdynamic_t *appd = &OS_appDynamicBase[app->appId];

	if ( (OS_mmuOwnerType == OS_OBJ_APPLICATION) && (OS_mmuOwnerId == app->appId) )
	{
		/* Application already owns the MMU, so there's nothing to do.
		*/
	}
	else
	if ( OS_AppUsesMmu(appd) )
	{
		OS_mmuOwnerType = OS_OBJ_APPLICATION;
		OS_mmuOwnerId = app->appId;

		OS_FillMmuForApp(appd);
	}
	else
	{
		/* MISRA :-( */
	}
}

#include <memmap/Os_mm_code_end.h>

/* Editor settings; DO NOT DELETE
 * vi:set ts=4:
*/
