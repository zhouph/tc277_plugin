/* kern-activatetask.c
 *
 * This file contains the OS_KernActivateTask function.
 *
 * CHECK: TABS 4 (see editor commands at end of file)
 *
 * Copyright 1998-2014 Elektrobit Automotive GmbH
 * All rights exclusively reserved for Elektrobit Automotive GmbH,
 * unless expressly agreed to otherwise.
 *
 * $Id: kern-activatetask.c 18431 2014-07-10 05:31:56Z ingi2575 $
*/
/* TOOLDIAG List of possible tool diagnostics
 *
 * TOOLDIAG-1) Possible diagnostic: SetButNeverUsed
 *   Variable is set but never used.
 *
 * Reason: Not an issue, variable will be used if at least one application exists.
 */

#define OS_SID OS_SID_ActivateTask
#define OS_SIF OS_svc_ActivateTask

#include <Os_kernel.h>

/* Include definitions for tracing */
#include <Os_trace.h>

#include <memmap/Os_mm_code_begin.h>

/*!
 * OS_KernActivateTask implements the API ActivateTask
 *
 * The specified task is activated. If it is already active, the
 * pending activation counter is incremented (up to the maximum).
 * Otherwise the task state is set to NEW and it is enqueued.
 *
 * Interrupts could be enabled on entry, so this function disables them
 * before loading the task state, because the state could get changed
 * during an ISR. The saved interrupt state is restored before
 * returning.
 *
 * !LINKSTO Kernel.API.TaskManagement.ActivateTask.API, 1
*/
os_result_t OS_KernActivateTask(os_taskid_t t)
{
	const os_task_t *tp;
	/* Possible diagnostic TOOLDIAG-1 <1> */
	const os_appcontext_t *app;
	os_result_t r = OS_E_OK;
	os_errorresult_t e;
	OS_PARAMETERACCESS_DECL

	OS_SAVE_PARAMETER_N(0,(os_paramtype_t)t);

	OS_TRACE_ACTIVATETASK_ENTRY(t);

	if ( !OS_CallingContextCheck() )
	{
		r = OS_ERROR(OS_ERROR_WrongContext, OS_GET_PARAMETER_VAR());
	}
	else
	if ( !OS_InterruptEnableCheck(OS_IEC_OSEKEXTRA) )
	{
		r = OS_ERROR(OS_ERROR_InterruptDisabled, OS_GET_PARAMETER_VAR());
	}
	else
	if ( !OS_IsValidTaskId(t) )
	{
		/* !LINKSTO Kernel.API.TaskManagement.ActivateTask.InvalidTask, 1
		*/
		r = OS_ERROR(OS_ERROR_InvalidTaskId, OS_GET_PARAMETER_VAR());
	}
	else
	{
		tp = OS_TidToTp(t);

		app = OS_CurrentApp();
		if ( !OS_HasPermission(app, tp->permissions) )
		{
			r = OS_ERROR(OS_ERROR_Permission, OS_GET_PARAMETER_VAR());
		}
		else
		{
			e = OS_DoActivateTask(tp);

			if ( e != OS_ERRORCODE_NOCHECK(OS_ERROR_NoError) )
			{
				r = OS_ERROR_NOCHECK(e, OS_GET_PARAMETER_VAR());
			}
		}
	}

	OS_TRACE_ACTIVATETASK_EXIT_P(r,t);
	return r;
}

/* API entries for User's Guide
 * CHECK: NOPARSE

<api API="OS_USER">
  <name>OS_UserActivateTask</name>
  <synopsis>Activate a task</synopsis>
  <syntax>
    os_result_t OS_UserActivateTask
    (   os_taskid_t t    /@ ID of task @/
    )
  </syntax>
  <description>
    <code>OS_UserActivateTask()</code> activates a task. If the specified
    task is currently in the <emphasis>suspended</emphasis> state its new state will be
    <emphasis>ready</emphasis>. If the task is already <emphasis>ready</emphasis> or <emphasis>running</emphasis>
    the activation will be recorded and performed after the task terminates,
    if permitted.
  </description>
  <availability>
  </availability>
  <returns>OS_E_OK=Success</returns>
</api>

 * CHECK: PARSE
*/

#include <memmap/Os_mm_code_end.h>

/* Editor settings; DO NOT DELETE
 * vi:set ts=4:
*/
