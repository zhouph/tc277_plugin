/* kern-restartapplication.c
 *
 * This file contains the OS_RestartApplication function
 *
 * CHECK: TABS 4 (see editor commands at end of file)
 *
 * Copyright 1998-2014 Elektrobit Automotive GmbH
 * All rights exclusively reserved for Elektrobit Automotive GmbH,
 * unless expressly agreed to otherwise.
 *
 * $Id: kern-restartapplication.c 17602 2014-02-03 12:09:35Z tojo2507 $
*/

/*
 * MISRA-C:2004 Deviation List
 *
 * MISRA-1) Deviated Rule: 17.4 (required)
 * Array indexing shall be the only allowed form of pointer arithmetic.
 *
 * Reason:
 * Pointer arithmetic is used for classical tasks like iterating through
 * an array to make them more readable and maintainable.
*/

#include <Os_kernel.h>

#include <memmap/Os_mm_code_begin.h>

/*!
 * OS_RestartApplication restarts the specified application.
 *
 * Restarting an application means:
 *  - setting all alarms, schedule tables and tasks to their respective
 *    "not running" states
 *  - activating the restart task for the application, if there is one.
*/
void OS_RestartApplication(const os_appcontext_t *app)
{
	os_taskid_t taskid;
	os_alarmid_t alarmid;
	os_scheduleid_t scheduleid;
	const os_task_t *task;
	const os_alarm_t *alarm;
	const os_schedule_t *schedule;

	/* Set all alarms to the IDLE state.
	 * Only true OSEK alarms are considered. Others (such as
	 * those belonging to schedule tables) won't be in the
	 * QUARANTINED state.
	*/
	alarm = OS_alarmTableBase;
	for ( alarmid = 0; alarmid < OS_nAlarms; alarmid++ )
	{
		if ( OS_GET_APP(alarm->app) == app )
		{
			(*OS_killAlarmFunc)(alarmid, OS_ALARM_IDLE);
		}
		/* Deviation MISRA-1 */
		alarm++;
	}

	/* Set all schedule tables to the STOPPED state.
	*/
	schedule = OS_scheduleTableBase;
	for ( scheduleid = 0; scheduleid < OS_nSchedules; scheduleid++ )
	{
		if ( OS_GET_APP(schedule->app) == app )
		{
			(*OS_killScheduleFunc)(scheduleid, OS_ST_STOPPED);
		}
		/* Deviation MISRA-1 */
		schedule++;
	}

	/* Set all tasks to the SUSPENDED state.
	*/
	task = OS_taskTableBase;
	for ( taskid = 0; taskid < OS_nTasks; taskid++ )
	{
		if ( OS_GET_APP(task->app) == app )
		{
			(*OS_killTaskFunc)(task, OS_TS_SUSPENDED);
		}
		/* Deviation MISRA-1 */
		task++;
	}

	/* The ISRs remain disabled. They may be reenabled by the restart task.
	*/

	/* Finally, activate the restart task. We don't use OS_KernActivateTask
	 * because the autosar error checking would probably throw out the request.
	 * Plus, we don't want to link in the function if it isn't needed.
	 *
	 * !LINKSTO Kernel.Autosar.Protection.ProtectionHook.RestartApplication, 1
	*/
	if ( (OS_IsValidTaskId(app->restartTask) ) && (OS_actiTaskFunc != OS_NULL) )
	{
		(*OS_actiTaskFunc)(&OS_taskTableBase[app->restartTask]);
	}
}

#include <memmap/Os_mm_code_end.h>

/* Editor settings; DO NOT DELETE
 * vi:set ts=4:
*/
