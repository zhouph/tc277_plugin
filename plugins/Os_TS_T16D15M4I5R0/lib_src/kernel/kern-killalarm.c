/* kern-killalarm.c
 *
 * This file contains the OS_KillAlarm function.
 *
 * CHECK: TABS 4 (see editor commands at end of file)
 *
 * Copyright 1998-2014 Elektrobit Automotive GmbH
 * All rights exclusively reserved for Elektrobit Automotive GmbH,
 * unless expressly agreed to otherwise.
 *
 * $Id: kern-killalarm.c 18431 2014-07-10 05:31:56Z ingi2575 $
*/

#define OS_SID	OS_SID_KillAlarm
#define OS_SIF	OS_svc_KillAlarm

#include <Os_kernel.h>

#include <memmap/Os_mm_code_begin.h>

/*!
 * OS_KernKillAlarm
 *
 * The alarm is cancelled and its state is set to IDLE or QUARANTINED,
 * depending on the 2nd parameter.
 * An alarm never goes from the QUARANTINED to the IDLE state as a
 * result of this call.
*/
os_result_t OS_KillAlarm(os_alarmid_t a, os_uint8_t s)
{
	os_result_t r = OS_E_OK;
	const os_alarm_t *as;
	os_alarmdynamic_t *ad;
	const os_counter_t *cs;
	os_counterdynamic_t *cd;
	os_alarmid_t ap;
	os_alarmdynamic_t *pp;
	os_intstatus_t is;

	as = &OS_alarmTableBase[a];
	ad = &OS_alarmDynamicBase[a];
	cs = &OS_counterTableBase[as->counter];
	cd = &OS_counterDynamicBase[as->counter];

	is = OS_IntDisable();

	if ( ad->inUse == OS_ALARM_INUSE )
	{
		/* Find the predecessor of the alarm in the list.
		*/
		ap = cd->head;
		pp = OS_NULL;

		while ( (ap < OS_totalAlarms) && (ap != a) )
		{
			pp = &OS_alarmDynamicBase[ap];
			ap = pp->next;
		}

		if ( ap == a )
		{
			/* !LINKSTO Kernel.API.Alarms.CancelAlarm.CancelAlarm, 1
			*/
			/* pp now points to the alarm before ours in the counter
			 * queue, or OS_NULL if there isn't one.
			 * Dequeue our alarm.
			*/
			if ( pp == OS_NULL )
			{
				cd->head = ad->next;
			}
			else
			{
				pp->next = ad->next;
			}

			/* If there's an alarm after ours in the queue, we must add
			 * our remaining delta to its delta to maintain its correct
			 * timing
			*/
			if ( ad->next < OS_totalAlarms )
			{
				OS_alarmDynamicBase[ad->next].delta += ad->delta;
			}

			/* If the counter is a hardware timer and our alarm was first in the queue
			 * we should reprogram it with the new delta. We never operate
			 * on a locked counter.
			*/
			if ( (pp == OS_NULL) && OS_CounterIsHw(cs) && (cd->lock == 0) )
			{
				OS_CtrUpdate(cs, cd);
			}
		}
		else
		{
			OS_PARAMETERACCESS_DECL

			OS_SAVE_PARAMETER_N(0,(os_paramtype_t)a);
			OS_SAVE_PARAMETER_N(1,(os_paramtype_t)s);

			r = OS_ERROR(OS_ERROR_AlarmNotInQueue, OS_GET_PARAMETER_VAR());
		}

		ad->inUse = s;
		ad->period = 0;
		ad->delta = 0;
		ad->next = OS_NULLALARM;
	}
	else
	{
		ad->inUse = s;
	}

	OS_IntRestore(is);

	return r;
}

/* API entries for User's Guide
 * CHECK: NOPARSE

<api API="OS_INTERNAL">
  <name>OS_KillAlarm</name>
  <synopsis>Kill an alarm</synopsis>
  <syntax>
    os_result_t OS_KillAlarm
    (   os_alarmid_t a,    /@ ID of the alarm @/
        os_uint8_t s       /@ New state @/
    )
  </syntax>
  <description>
    <code>OS_KillAlarm()</code> resets the expiration time of
    the specified alarm and sets its state as required.
  </description>
  <availability>
  </availability>
  <returns>OS_E_OK=Success</returns>
</api>

 * CHECK: PARSE
*/

#include <memmap/Os_mm_code_end.h>

/* Editor settings; DO NOT DELETE
 * vi:set ts=4:
*/
