/* kern-getscheduletablestatus.c
 *
 * This file contains the OS_KernGetScheduleTableStatus function
 *
 * CHECK: TABS 4 (see editor commands at end of file)
 *
 * Copyright 1998-2014 Elektrobit Automotive GmbH
 * All rights exclusively reserved for Elektrobit Automotive GmbH,
 * unless expressly agreed to otherwise.
 *
 * $Id: kern-getscheduletablestatus.c 18431 2014-07-10 05:31:56Z ingi2575 $
*/

#define OS_SID	OS_SID_GetScheduleTableStatus
#define OS_SIF	OS_svc_GetScheduleTableStatus

#include <Os_kernel.h>

/* Include definitions for tracing */
#include <Os_trace.h>

#include <memmap/Os_mm_code_begin.h>

/*!
 * OS_KernGetScheduleTableStatus implements the API GetScheduleTableStatus
 *
 * The status of the specified schedule table is written to the specified
 * output location.
 *
 * Interrupts are enabled on entry and remain so throughout. The
 * status returned is therefore a snapshot of the schedule table's state,
 * and could have changed by the time the caller actually gets it.
 * However, this could happen anyway.
 *
 * !LINKSTO Kernel.Autosar.API.SystemServices.GetScheduleTableStatus, 2
 * !LINKSTO Kernel.Autosar.ScheduleTable.Synchronisation.QuerySynch, 2
*/
os_result_t OS_KernGetScheduleTableStatus
(	os_scheduleid_t s,
	os_uint8_t *out
)
{
	os_scheduledynamic_t *sd;
	os_result_t r = OS_E_OK;
	OS_PARAMETERACCESS_DECL

	OS_SAVE_PARAMETER_N(0,(os_paramtype_t)s);
	OS_SAVE_PARAMETER_N(1,(os_paramtype_t)out);

	OS_TRACE_GETSCHEDULETABLESTATUS_ENTRY(s);

	if ( !OS_CallingContextCheck() )
	{
		r = OS_ERROR(OS_ERROR_WrongContext, OS_GET_PARAMETER_VAR());
	}
	else
	if ( !OS_InterruptEnableCheck(OS_IEC_AUTOSAR) )
	{
		r = OS_ERROR(OS_ERROR_InterruptDisabled, OS_GET_PARAMETER_VAR());
	}
	else
	if ( OS_ArchCanWrite(out, sizeof(*out)) == 0 )
	{
		r = OS_ERROR(OS_ERROR_WriteProtect, OS_GET_PARAMETER_VAR());
	}
	else
	if ( s >= OS_nSchedules )
	{
		/* !LINKSTO Kernel.Autosar.API.SystemServices.GetScheduleTableStatus.Invalid, 1
		*/
		r = OS_ERROR(OS_ERROR_InvalidScheduleId, OS_GET_PARAMETER_VAR());
	}
	else
	{
		sd = &OS_scheduleDynamicBase[s];

		*out = sd->status;
	}

	OS_TRACE_GETSCHEDULETABLESTATUS_EXIT_P(r,s);
	return r;
}

/* API entries for User's Guide
 * CHECK: NOPARSE

<api API="OS_USER">
  <name>OS_UserGetScheduleTableStatus</name>
  <synopsis>Get a schedule table's status</synopsis>
  <syntax>
    os_result_t OS_UserGetScheduleTableStatus
    (   os_scheduleid_t s   /@ ID of schedule table @/
        os_uint8_t *out     /@ Where to put the result @/
    )
  </syntax>
  <description>
    <code>OS_UserGetScheduleTableStatus()</code> writes the current
    status of the schedule table to the specified location.
  </description>
  <availability>
    No restrictions.
  </availability>
  <returns>Status=Success</returns>
  <returns>OS_E_OK=Success</returns>
</api>

 * CHECK: PARSE
*/

#include <memmap/Os_mm_code_end.h>

/* Editor settings; DO NOT DELETE
 * vi:set ts=4:
*/
