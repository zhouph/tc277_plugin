/* kern-initisrstacks.c
 *
 * This file contains the OS_InitIsrStacks function.
 *
 * CHECK: TABS 4 (see editor commands at end of file)
 *
 * Copyright 1998-2014 Elektrobit Automotive GmbH
 * All rights exclusively reserved for Elektrobit Automotive GmbH,
 * unless expressly agreed to otherwise.
 *
 * $Id: kern-initisrstacks.c 17602 2014-02-03 12:09:35Z tojo2507 $
*/

#include <Os_kernel.h>

#include <memmap/Os_mm_code_begin.h>

/* OS_InitIsrStacks()
 *
 * Set all ISR stacks to 0xeb (or whatever the fill pattern is)
*/
void OS_InitIsrStacks(void)
{
	os_isrid_t i;

	for ( i = 0; i < OS_nInterrupts; i++ )
	{
		const os_isr_t *is = &OS_isrTable[i];
		os_stackelement_t *p;

		/* Depending on the architecture, stacks may reside on the
		 * interrupt/kernel stack. In that case OS_GetIsrStackBase
		 * returns OS_NULL. These stacks are already initialized by
		 * OS_InitKernStack().
		 */
		p = OS_GetIsrStackBase(is);
		if ( p != OS_NULL )
		{
			os_size_t k;
			for ( k = 0; k < (is->stackLen / sizeof(os_stackelement_t)); k++ )
			{
				p[k] = OS_STACKFILL;
			}
		}
	}
}

#include <memmap/Os_mm_code_end.h>

/* Editor settings; DO NOT DELETE
 * vi:set ts=4:
*/
