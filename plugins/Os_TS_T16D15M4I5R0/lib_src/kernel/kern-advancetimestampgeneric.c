/* kern-advancetimestampgeneric.c
 *
 * This file contains the OS_AdvanceTimeStampGeneric() function.
 *
 * CHECK: TABS 4 (see editor commands at end of file)
 *
 * Copyright 1998-2014 Elektrobit Automotive GmbH
 * All rights exclusively reserved for Elektrobit Automotive GmbH,
 * unless expressly agreed to otherwise.
 *
 * $Id: kern-advancetimestampgeneric.c 18663 2014-08-27 11:12:35Z mist8519 $
 */

#include <Os_kernel.h>
#include <Os_timestamp.h>

#include <memmap/Os_mm_code_begin.h>

/* OS_AdvanceTimeStampGeneric
 *
 * This function advances the time stamp value by adding the number of ticks of the hardware
 * timestamp timer that have happened since "last time" to the global timestamp value.
 *
 * NOTE: we do not test the OS_timeStampTimer for OS_NULL here. If the timestamp is not configured,
 * the constant will be OS_NULL, but the test against the actual hardware timer for which the interrupt
 * has occurred (see kern-hwcounterexpire.c and kern-hwtimerexpire.c) will fail, so this function
 * will never be called.
*/
void OS_AdvanceTimeStampGeneric(os_timervalue_t newTmrVal)
{
#if OS_USEGENERICTIMESTAMP
	os_timervalue_t diff;
	os_intstatus_t is = OS_IntDisableAll();

	diff = OS_HwtSub(OS_timeStampTimer, newTmrVal, OS_lastTimeStampTime);
	OS_TimeAdd32(&OS_timeStampValue, OS_timeStampValue, diff);
	OS_lastTimeStampTime = newTmrVal;

	OS_IntRestoreAll(is);
#else
	OS_PARAM_UNUSED(newTmrVal);
#endif
}

#include <memmap/Os_mm_code_end.h>

/* Editor settings; DO NOT DELETE
 * vi:set ts=4:
*/
