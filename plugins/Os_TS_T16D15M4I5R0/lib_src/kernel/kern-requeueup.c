/* kern-requeueup.c
 *
 * This file contains the OS_RequeueUp function.
 *
 * CHECK: TABS 4 (see editor commands at end of file)
 *
 * Copyright 1998-2014 Elektrobit Automotive GmbH
 * All rights exclusively reserved for Elektrobit Automotive GmbH,
 * unless expressly agreed to otherwise.
 *
 * $Id: kern-requeueup.c 17718 2014-02-17 15:31:32Z tojo2507 $
*/

#include <Os_kernel.h>

#include <memmap/Os_mm_code_begin.h>

/*!
 * OS_RequeueUp
 *
 * The specified task activation is moved from its position in the task queue to somewhere
 * further up the queue because it has increased its priority. The task's priority has already
 * been set to its new value.
 *
 * OS_RequeueUp() must be called with interrupts locked
*/
void OS_RequeueUp(os_tasklink_t tAct, os_prio_t prio)
{
	os_tasklink_t qAct = OS_taskActivations[0];
	os_tasklink_t qNew = 0;			/* New predecessor in queue */
	os_tasklink_t qOld;				/* Old predecessor in queue */
	os_tasklink_t qNewNext;			/* New successor in queue */

	/* Run through the queue till we find the new position. Note
	 * that this loop will automatically stop at the current position
	 * because the priority test will return "equal", so there's no
	 * need to explicitly stop at qAct==tAct.
	*/
	while ( (qAct != 0) && (OS_FindPrio(qAct) > prio) )
	{
		qNew = qAct;
		qAct = OS_taskActivations[qAct];
	}

	/* If we leave the above loop with qAct == 0, the current task is not in the queue.
	 * We can't handle that here so we just leave the queue as it is.
	*/
	if ( qAct != 0 )
	{
		/* Remember the current queue position. This will be the new successor in the queue.
		*/
		qNewNext = qAct;

		/* Now we find the current ("old") position in the queue by searching from the current position.
		*/
		qOld = qNew;
		while ( (qAct != tAct) && (qAct != 0) )
		{
			qOld = qAct;
			qAct = OS_taskActivations[qAct];
		}

		if ( qOld == qNew )
		{
			/* The old position and the new position are the same, so no change.
			*/
		}
		else
		{
			if ( qAct == tAct )
			{
				/* Now we remove the activation from its former position
				* (after qOld) and insert it after qNew.
				* The above test is just a sanity test. If qAct != tAct it means the task isn't
				* in the queue.
				*/
				/* Remove from old place in queue
				*/
				OS_taskActivations[qOld] = OS_taskActivations[tAct];

				/* Insert at new place in queue
				*/
				OS_taskActivations[qNew] = tAct;
				OS_taskActivations[tAct] = qNewNext;

				/* Finally, set the new queue head because it might have changed.
				*/
				OS_taskQueueHead = OS_taskPtrs[OS_taskActivations[0]];
			}
		}
	}
}

#include <memmap/Os_mm_code_end.h>

/* Editor settings; DO NOT DELETE
 * vi:set ts=4:
*/
