/* kern-mmubestpagesize.c
 *
 * This file contains the OS_MmuBestPageSize function, which finds the best page-size
 * for a given starting address.
 *
 * CHECK: TABS 4 (see editor commands at end of file)
 *
 * Copyright 1998-2014 Elektrobit Automotive GmbH
 * All rights exclusively reserved for Elektrobit Automotive GmbH,
 * unless expressly agreed to otherwise.
 *
 * $Id: kern-mmubestpagesize.c 17602 2014-02-03 12:09:35Z tojo2507 $
*/

#include <Os_kernel.h>
#include <Os_mmu.h>

#include <memmap/Os_mm_const_begin.h>

static const os_address_t OS_pageZero[]	= OS_PAGEZERO_ARRAY;

#include <memmap/Os_mm_const_end.h>


#include <memmap/Os_mm_code_begin.h>

/*!
 * OS_BestPageSize()
 *
 * This function determines the largest page-size that can be used to map the page address given as a parameter.
*/
os_pagesize_t OS_MmuBestPageSize(os_address_t page)
{
	os_pagesize_t best = OS_PAGESIZE_MAX;
	const os_address_t *p = &OS_pageZero[OS_PAGESIZE_MAX];

	/* This loop always terminates because the first entry of the pageZero array is zero!
	*/
	while ( (page & *p) != 0 )
	{
		p--;
		best--;
	}

	return best;
}

#include <memmap/Os_mm_code_end.h>

/* Editor settings; DO NOT DELETE
 * vi:set ts=4:
*/
