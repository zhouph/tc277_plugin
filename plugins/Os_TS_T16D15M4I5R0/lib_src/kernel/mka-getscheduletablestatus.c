/* mka-getscheduletablestatus.c
 *
 * This file contains the GetScheduleTableStatus function.
 *
 * CHECK: TABS 4 (see editor commands at end of file)
 *
 * Copyright 1998-2014 Elektrobit Automotive GmbH
 * All rights exclusively reserved for Elektrobit Automotive GmbH,
 * unless expressly agreed to otherwise.
 *
 * $Id: mka-getscheduletablestatus.c 17602 2014-02-03 12:09:35Z tojo2507 $
*/
#include <Os_kernel.h>
#include <public/Mk_autosar.h>
#include <Os_api_microkernel.h>

/*
 * GetScheduleTableStatus() places the schedule table status into the referenced variable.
 *
 * WARNING: this function cannot be ASIL because it calls a QM function in the OS.
 *
 * This function is functionally identical to the OS_GetScheduleTableStatus library function
 * used in the standalone OS, except that it calls OS_KernGetScheduleTableStatus() directly
 * instead of over a system call. This behaviour relies on the global readability of
 * OS variables.
*/
StatusType GetScheduleTableStatus(ScheduleTableType scheduleId, ScheduleTableStatusRefType out)
{
	StatusType returnValue;
	os_uint8_t internalStatus;

	returnValue = OS_KernGetScheduleTableStatus(scheduleId, &internalStatus);

	if ( returnValue == OS_E_OK )
	{
		if ( (internalStatus & OS_ST_STATE) == OS_ST_RUNNING )
		{
			if (  (internalStatus & OS_ST_SYNCHRONOUS) == 0 )
			{
				/* !LINKSTO Kernel.Autosar.API.SystemServices.GetScheduleTableStatus.Asynchronous, 1
				*/
				*out = SCHEDULETABLE_RUNNING;
			}
			else
			{
				/* !LINKSTO Kernel.Autosar.API.SystemServices.GetScheduleTableStatus.Synchronous, 1
				*/
				*out = SCHEDULETABLE_RUNNING_AND_SYNCHRONOUS;
			}
		}
		else
		if ( (internalStatus & OS_ST_STATE) == OS_ST_WAITING )
		{
			/* !LINKSTO Kernel.Autosar.API.SystemServices.GetScheduleTableStatus.Waiting, 2
			*/
			*out = SCHEDULETABLE_WAITING;
		}
		else
		if ( (internalStatus & OS_ST_STATE) == OS_ST_CHAINED )
		{
			/* !LINKSTO Kernel.Autosar.API.SystemServices.GetScheduleTableStatus.Next, 1
			*/
			*out = SCHEDULETABLE_NEXT;
		}
		else
		{
			/* !LINKSTO Kernel.Autosar.API.SystemServices.GetScheduleTableStatus.NotStarted, 2
			*/
			*out = SCHEDULETABLE_STOPPED;
		}
	}

	return returnValue;
}

/* Editor settings; DO NOT DELETE
 * vi:set ts=4:
*/
