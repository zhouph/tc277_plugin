/* kern-mmupadramreadonly.c
 *
 * This file contains the OS_MmuPadRamReadonly function
 *
 * This function fills the provided partial chunk map by filling the areas between the chunks
 * with read-only chunks, so that the whole area (of OS_ramSize bytes starting at OS_ramBase) is
 * mapped. The resulting map is not sorted.
 *
 * It is assumed that the array provided is big enough to hold the extra chunks. I.e. if there
 * are N writable chunks the array needs to be dimensioned with N+2 elements.
 *
 * CHECK: TABS 4 (see editor commands at end of file)
 *
 * Copyright 1998-2014 Elektrobit Automotive GmbH
 * All rights exclusively reserved for Elektrobit Automotive GmbH,
 * unless expressly agreed to otherwise.
 *
 * $Id: kern-mmupadramreadonly.c 17602 2014-02-03 12:09:35Z tojo2507 $
*/

#include <Os_kernel.h>
#include <Os_mmu.h>

#include <memmap/Os_mm_code_begin.h>

/*!
 * OS_MmuPadRamReadonly
 *
 * This function fills the gaps in a memory map with read-only chunks
*/
os_int_t OS_MmuPadRamReadonly(os_pagemap_t *chunkmap, os_int_t nchunks)
{
	os_int_t i;
	os_int_t ngaps;
	os_address_t chunkStart;
	os_address_t chunkEnd;
	os_address_t ramEnd;

	if ( OS_ramSize != 0 )
	{
		ramEnd = OS_ramBase + OS_ramSize;
		ngaps = nchunks - 1;

		for ( i = 0; i < ngaps; i++ )
		{
			if ( (chunkmap[i].base + chunkmap[i].size) < chunkmap[i+1].base )
			{
				/* There's a gap between the two chunks
				 * Calculate the chunk start and end (end is really start of next chunk).
				*/
				chunkStart = chunkmap[i].base + chunkmap[i].size;
				chunkEnd = chunkmap[i+1].base;

				/* Constrain the chunk to lie within the defined read-only region
				*/
				if ( chunkStart < OS_ramBase )
				{
					chunkStart = OS_ramBase;
				}
				if ( chunkEnd > ramEnd )
				{
					chunkEnd = ramEnd;
				}

				/* If the modified chunk defines a region of positive size we can add it to our list
				*/
				if ( chunkEnd > chunkStart )
				{
					chunkmap[nchunks].base = chunkStart;
					chunkmap[nchunks].size = chunkEnd - chunkStart;
					chunkmap[nchunks].flags = OS_MEMPERM_UR;
					nchunks++;
				}
			}
		}

		/* Add a chunk before the first existing chunk if necessary
		*/
		chunkEnd = chunkmap[0].base;
		if ( OS_ramBase < chunkEnd )
		{
			if ( chunkEnd > ramEnd )
			{
				chunkEnd = ramEnd;
			}
			/* No need to test for validity - the "OS_ramBase < chunkEnd" test has ensured that.
			*/
			chunkmap[nchunks].base = OS_ramBase;
			chunkmap[nchunks].size = chunkEnd - OS_ramBase;
			chunkmap[nchunks].flags = OS_MEMPERM_UR;
			OS_MmuPagify(&chunkmap[nchunks], 1);
			nchunks++;
		}

		/* Add a chunk after the last existing chunk if necessary
		*/
		chunkStart = chunkmap[ngaps].base + chunkmap[ngaps].size;
		if ( ramEnd > chunkStart )
		{
			if ( chunkStart < OS_ramBase )
			{
				chunkStart = OS_ramBase;
			}
			/* No need to test for validity - the "ramEnd > chunkStart" test has ensured that.
			*/
			chunkmap[nchunks].base = chunkStart;
			chunkmap[nchunks].size = ramEnd - chunkStart;
			chunkmap[nchunks].flags = OS_MEMPERM_UR;
			OS_MmuPagify(&chunkmap[nchunks], 1);
			nchunks++;
		}
	}

	/* Finally, add a read-only area for the constants (usually in ROM)
	*/
	if ( OS_constSize != 0 )
	{
		chunkmap[nchunks].base = OS_constBase;
		chunkmap[nchunks].size = OS_constSize;
		chunkmap[nchunks].flags = OS_MEMPERM_UR;
		OS_MmuPagify(&chunkmap[nchunks], 1);
		nchunks++;
	}

	return nchunks;
}

#include <memmap/Os_mm_code_end.h>

/* Editor settings; DO NOT DELETE
 * vi:set ts=4:
*/
