/* mkl-getelapsedcountervalue.c
 *
 * This file contains the GetElapsedCounterValue function.
 *
 * CHECK: TABS 4 (see editor commands at end of file)
 *
 * Copyright 1998-2014 Elektrobit Automotive GmbH
 * All rights exclusively reserved for Elektrobit Automotive GmbH,
 * unless expressly agreed to otherwise.
 *
 * $Id: mka-getelapsedcountervalue.c 17602 2014-02-03 12:09:35Z tojo2507 $
*/
#include <Os_kernel.h>
#include <public/Mk_autosar.h>

/*
 * GetElapsedCounterValue() places the value of the specified counter into the first referenced variable
 * and the difference between the new value and the old value (modulo MAXALLOWEDVALUE) into the second
 * referenced variable.
 *
 * WARNING: this function cannot be ASIL because it calls a QM function in the OS.
*/
StatusType GetElapsedCounterValue(CounterType counterId, TickRefType value, TickRefType elapsedValue)
{
	TickType tmpTick;
	StatusType ret;

	ret = GetCounterValue(counterId, &tmpTick);

	*elapsedValue = OS_CounterSub(tmpTick, *value, counterId);
	*value = tmpTick;

	return ret;
}

/* Editor settings; DO NOT DELETE
 * vi:set ts=4:
*/
