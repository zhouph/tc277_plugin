/* kern-getmem.c
 *
 * This file contains the OS_GetMem function, which allocates a chunk of memory of a given size
 * from a global pool.
 *
 * CHECK: TABS 4 (see editor commands at end of file)
 *
 * Copyright 1998-2014 Elektrobit Automotive GmbH
 * All rights exclusively reserved for Elektrobit Automotive GmbH,
 * unless expressly agreed to otherwise.
 *
 * $Id: kern-getmem.c 17602 2014-02-03 12:09:35Z tojo2507 $
*/

/*
 * MISRA-C:2004 Deviation List
 *
 * MISRA-1) Deviated Rule: 11.4 (advisory)
 * A cast should not be performed between a pointer to object type and
 * a different pointer to object type.
 *
 * Reason:
 * Code for memory allocation uses opaque structures of a specific size
 * to actually *do* the allocation, but has to use a defined type for
 * managing the "free" list. Implementation ensures that alignment
 * restrictions are adhered to.
*/

#include <Os_kernel.h>
#include <Os_memalloc.h>

#include <memmap/Os_mm_code_begin.h>

/*!
 * OS_GetMem()
 *
 * This function allocates a chunk of memory at least nbytes long from the global pool and returns its address.
 *
 * The allocation algorithm used is "first fit". It is assumed that most memory will be allocated at startup
 * and never freed, so the simplest algorithm is sufficient.
 *
 * Note: this function could never be used by non-trusted applications (even if a system-call interface were to
 * be constructed) because the memory pool lies outside the accessible area for non-trusted applications.
*/
void *OS_GetMem(os_size_t nbytes)
{
	os_memblock_t *p,*q,*r;
	os_memlump_t *lump;
	os_size_t nlumps;
	os_boolean_t looking;
	os_intstatus_t is;

	if ( nbytes == 0 )
	{
		p = OS_NULL;
	}
	else
	{
		nlumps = OS_MemBytesToLumps(nbytes);
		looking = OS_TRUE;

		is = OS_IntDisable();

		q = &OS_memHead;
		p = OS_memHead.next;
		while ( (p != OS_NULL) && looking )
		{
			if ( p->size == nlumps )
			{
				q->next = p->next;
				looking = OS_FALSE;
			}
			else
			if ( p->size > nlumps )
			{
				/* Deviation MISRA-1 <+2> */
				lump = (os_memlump_t *)p;
				r = (os_memblock_t *)&lump[nlumps];
				q->next = r;
				r->next = p->next;
				r->size = p->size - nlumps;
				looking = OS_FALSE;
			}
			else
			{
				q = p;
				p = p->next;
			}
		}

		OS_IntRestore(is);
	}

	return (void *)p;
}

#include <memmap/Os_mm_code_end.h>

/* Editor settings; DO NOT DELETE
 * vi:set ts=4:
*/
