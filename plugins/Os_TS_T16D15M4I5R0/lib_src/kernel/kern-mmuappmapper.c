/* kern-mmuappmapper.c
 *
 * This file contains the OS_MmuAppMapper function
 *
 * The MMU mapper creates a memory map for the MMU for a non-trusted application. The memory map specifies all memory
 * regions that are accessible by the hook functions of a non-trusted application.
 *
 * At the moment, it is assumed that there is a single contiguous region of RAM into which
 * all stacks and data are linked. Since there are up to 3 writable regions, this memory must
 * be partitioned into up to 7 regions. A further region is needed for the read-only constants
 * in ROM, giving 8 in total.
 *
 * CHECK: TABS 4 (see editor commands at end of file)
 *
 * Copyright 1998-2014 Elektrobit Automotive GmbH
 * All rights exclusively reserved for Elektrobit Automotive GmbH,
 * unless expressly agreed to otherwise.
 *
 * $Id: kern-mmuappmapper.c 20296 2015-01-30 13:51:13Z stpo8218 $
*/

#define OS_SID OS_SID_MemoryManagement
#define OS_SIF OS_svc_MemoryManagement

#include <Os_kernel.h>
#include <Os_mmu.h>
#include <Os_panic.h>

#if (OS_KERNEL_TYPE!=OS_SYSTEM_CALL)
#error "This file requires a system-call kernel. Remove it from the non-system-call build."
#endif

#include <memmap/Os_mm_code_begin.h>

/* This sets dimensions on the region map (below)
*/
#define MAX_REGIONS		8

/*!
 * OS_MmuAppMapper
 *
 * This function creates an MMU mapping for the data areas of a given application.
*/
os_result_t OS_MmuAppMapper(os_applicationid_t appid)
{
	os_pagemap_t chunkmap[MAX_REGIONS];
	os_pagemap_t pagemap[OS_MMU_MAX_PAGES];
	os_archpagemap_t *archpagemap;
	os_int_t nchunks = 0;
	os_int_t npages = 0;
	os_result_t result = OS_E_OK;

	const os_appcontext_t *apps = &OS_appTableBase[appid];
	os_appdynamic_t *appd = &OS_appDynamicBase[appid];

	/* First, work out how many memory areas there are, and where they are.
	*/
	if ( OS_eHookStackBase != OS_NULL )
	{
		chunkmap[nchunks].base = (os_address_t)OS_eHookStackBase;
		chunkmap[nchunks].size = OS_eHookStackLen;
		chunkmap[nchunks].flags = OS_MEMPERM_UR | OS_MEMPERM_UW;
		nchunks++;
	}
	if ( OS_sHookStackBase != OS_NULL )
	{
		chunkmap[nchunks].base = (os_address_t)OS_sHookStackBase;
		chunkmap[nchunks].size = OS_sHookStackLen;
		chunkmap[nchunks].flags = OS_MEMPERM_UR | OS_MEMPERM_UW;
		nchunks++;
	}

	if ( apps->dataStart != OS_NULL )
	{
		chunkmap[nchunks].base = (os_address_t)apps->dataStart;
		chunkmap[nchunks].size = apps->dataEnd - apps->dataStart;		/* Assumes ptrs are to byte */
		chunkmap[nchunks].flags = OS_MEMPERM_UR | OS_MEMPERM_UW;
		nchunks++;
	}

	/* Optimise the chunks (sort, pagify and merge) and add the read-only data chunks
	*/
	nchunks = OS_MmuOptimise(chunkmap, nchunks);
	nchunks = OS_MmuPadRamReadonly(chunkmap, nchunks);

	/* Convert the chunks to pages. Returns -1 if there's a problem. This
	 * writes the page map into pagemap[] array.
	*/
	npages = OS_MmuMapPages(chunkmap, nchunks, pagemap, OS_nFreePageMaps);

	if ( npages < 0 )
	{
		result = OS_PANIC(OS_PANIC_InsufficientPageMaps);
	}
	else
	if ( npages > 0 )
	{
		archpagemap = OS_ArchMmuPageMapTranslate(pagemap, npages);

		if ( archpagemap == OS_NULL )
		{
			result = OS_PANIC(OS_PANIC_InsufficientHeap);
		}

		OS_ArchSetAppPagemap(appd, npages, archpagemap);
	}

	return result;
}

#include <memmap/Os_mm_code_end.h>

/* Editor settings; DO NOT DELETE
 * vi:set ts=4:
*/
