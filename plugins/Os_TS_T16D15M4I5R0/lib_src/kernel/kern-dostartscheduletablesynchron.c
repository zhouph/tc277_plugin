/* kern-dostartscheduletablesynchron.c
 *
 * This file contains the OS_DoStartScheduleTableSynchron function.
 *
 * CHECK: TABS 4 (see editor commands at end of file)
 *
 * Copyright 1998-2014 Elektrobit Automotive GmbH
 * All rights exclusively reserved for Elektrobit Automotive GmbH,
 * unless expressly agreed to otherwise.
 *
 * $Id: kern-dostartscheduletablesynchron.c 17602 2014-02-03 12:09:35Z tojo2507 $
*/

#define OS_SID OS_SID_StartScheduleTableSynchron
#define OS_SIF OS_svc_StartScheduleTableSynchron

#include <Os_kernel.h>

#include <memmap/Os_mm_code_begin.h>

/*!
 * OS_DoStartScheduleTableSynchron
 *
 * The specified schedule table is placed in the "waiting" state
 * and will start only when SyncScheduleTable is called for it.
 *
 * !LINKSTO Kernel.Autosar.API.SystemServices.StartScheduleTableSynchron, 2
*/
os_errorresult_t OS_DoStartScheduleTableSynchron
(	os_scheduleid_t s,
	const os_schedule_t *ss
)
{
	os_scheduledynamic_t *sd;
	os_alarmdynamic_t *ad;
	os_errorresult_t result = OS_ERRORCODE_NOCHECK(OS_ERROR_NoError);
	os_intstatus_t is;

	/* !LINKSTO Kernel.Autosar.ScheduleTable.Synchronisation.Strategy.EXPLICIT, 1
	*/
	sd = &OS_scheduleDynamicBase[s];
	ad = &OS_alarmDynamicBase[ss->alarm];

	is = OS_IntDisable();

	if ( sd->status != OS_ST_STOPPED )
	{
		/* !LINKSTO Kernel.Autosar.API.SystemServices.StartScheduleTableSynchron.State, 2
		*/
		result = OS_ERRORCODE_CHECK(OS_ERROR_ScheduleTableNotIdle);
	}
	else
	if ( ad->inUse != OS_ALARM_IDLE )
	{
		/* This should never happen.
		*/
		result = OS_ERRORCODE_CHECK(OS_ERROR_AlarmInUse);
	}
	else
	{
		/* !LINKSTO Kernel.Autosar.API.SystemServices.StartScheduleTableSynchron.NewState, 1
		*/
		sd->adjRemaining = OS_MAXTICK;
		sd->chain = OS_NULLSCHEDULE;
		sd->next = 0;
		sd->status = OS_ST_WAITING;
	}

	OS_IntRestore(is);

	return result;
}

#include <memmap/Os_mm_code_end.h>

/* Editor settings; DO NOT DELETE
 * vi:set ts=4:
*/
