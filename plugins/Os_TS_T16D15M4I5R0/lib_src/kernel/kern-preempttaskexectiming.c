/* kern-preempttaskexectiming.c
 *
 * This file contains the OS_PreemptTaskExecTiming function
 *
 * CHECK: TABS 4 (see editor commands at end of file)
 *
 * Copyright 1998-2014 Elektrobit Automotive GmbH
 * All rights exclusively reserved for Elektrobit Automotive GmbH,
 * unless expressly agreed to otherwise.
 *
 * $Id: kern-preempttaskexectiming.c 17602 2014-02-03 12:09:35Z tojo2507 $
*/

#include <Os_kernel.h>

#include <memmap/Os_mm_code_begin.h>

/*!
 * OS_PreemptTaskExecTiming
 *
 * This function gets the amount of execution time that has passed since
 * the last measurement point, substracts it from the time remaining,
 * and stores the result in the given task's remaining budget.
 *
 * This function is called by the dispatcher when a task is pre-empted, after the state
 * has been set to READY_SYNC or READY_ASYNC. It is also called by the Category 2 wrapper
 * when a task has been interrupted. It is therefore safe to call OS_Error (and
 * possibly kill the task) here.
 *
*/
void OS_PreemptTaskExecTiming(const os_task_t *tp)
{
	os_taskaccounting_t *acc = OS_GET_ACCT(tp->accounting);
	os_tick_t used;

	if ( acc != OS_NULL )
	{
		OS_ResetExecTimingInterrupt();
		used = OS_GetTimeUsed();

		OS_accounting.etUsed += used;
		acc->etUsed = OS_accounting.etUsed;
		acc->etType = OS_accounting.etType;

		if ( ((tp->flags & OS_TF_MEASUREEXEC) != 0) && (OS_accounting.etUsed > acc->etMax) )
		{
			acc->etMax = OS_accounting.etUsed;
		}

		if ( (OS_accounting.etLimit != 0) && (OS_accounting.etUsed >= OS_accounting.etLimit) )
		{
			OS_ExceedExecTime();
		}
	}
}

#include <memmap/Os_mm_code_end.h>

/* Editor settings; DO NOT DELETE
 * vi:set ts=4:
*/
