/* kern-catkentry.c
 *
 * This file contains the CatK interrupt handler wrapper.
 *
 * CHECK: TABS 4 (see editor commands at end of file)
 *
 * Copyright 1998-2014 Elektrobit Automotive GmbH
 * All rights exclusively reserved for Elektrobit Automotive GmbH,
 * unless expressly agreed to otherwise.
 *
 * $Id: kern-catkentry.c 18087 2014-04-29 16:02:15Z stpo8218 $
*/

#include <Os_kernel.h>
#include <Os_cpuload_kernel.h>

#include <memmap/Os_mm_code_begin.h>

/* OS_CatKEntry() - call a Category K (Kernel) ISR.
 *
 * This function calls a category K (Kernel) ISR.
 *
 * The previous value of inKernel is saved, then inKernel is set
 * to 1.
 * Then the ISR function itself is called.
 * Finally, the saved value of inKernel is returned. The real inKernel
 * flag is left at 1 because we might need to reschedule in the exit
 * routine.
 *
 * Interrupts remain at the kernel lock level priority throughout. The run priority
 * is not used for category K interrupts.
*/
os_uint8_t OS_CatKEntry(os_isrid_t iid)
{
	const os_isr_t *isr;
	os_isrfunc_t f;
	os_uint8_t inKernel;

	OS_CPULOAD_GENERICCAT2_ENTRY();

	isr = &OS_isrTable[iid];
	f = isr->func;

	inKernel = OS_inKernel;
	OS_SetIsrinKernel();

	(*f)();

	return inKernel;
}

#include <memmap/Os_mm_code_end.h>

/* Editor settings; DO NOT DELETE
 * vi:set ts=4:
*/
