/* kern-starttaskexectiming.c
 *
 * This file contains the OS_StartTaskExecTiming function.
 *
 * CHECK: TABS 4 (see editor commands at end of file)
 *
 * Copyright 1998-2014 Elektrobit Automotive GmbH
 * All rights exclusively reserved for Elektrobit Automotive GmbH,
 * unless expressly agreed to otherwise.
 *
 * $Id: kern-starttaskexectiming.c 18659 2014-08-27 08:50:00Z mist8519 $
*/

#include <Os_kernel.h>

#include <memmap/Os_mm_code_begin.h>

/*!
 * OS_StartTaskExecTiming
 *
 * This function gets the amount of execution time that has been used
 * by a task and stores it into the global accounting structure. It then
 * sets the limit in the global accounting structure and sets the timer
 * interrupt for the difference.
 * The function is called when a task gains or regains the CPU.
 *
 * !LINKSTO Kernel.Autosar.Protection.TimingProtection.ExecutionTime.Measurement.Task, 1
*/
void OS_StartTaskExecTiming(const os_task_t *tp)
{
	os_taskaccounting_t *acc = OS_GET_ACCT(tp->accounting);
	os_tick_t remaining;

	OS_PARAM_UNUSED(tp);

	if ( acc != OS_NULL )
	{
		OS_accounting.frc = OS_ReadExecTimer();
		OS_accounting.inFunction = OS_INTASK;
		OS_accounting.etType = acc->etType;
		OS_accounting.etUsed = acc->etUsed;
		OS_accounting.etLimit = OS_GET_TP(tp->execBudget);

		if ( OS_accounting.etLimit != 0 )
		{
			if ( OS_accounting.etUsed >= OS_accounting.etLimit )
			{
				/* This should never happen.
				*/
				remaining = 1;
			}
			else
			{
				remaining = OS_accounting.etLimit - OS_accounting.etUsed;
			}
			OS_SetExecTimingInterrupt(OS_accounting.frc, remaining);
		}
	}
}

#include <memmap/Os_mm_code_end.h>

/* Editor settings; DO NOT DELETE
 * vi:set ts=4:
*/
