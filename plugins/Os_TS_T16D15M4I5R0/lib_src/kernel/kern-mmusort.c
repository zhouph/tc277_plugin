/* kern-mmusort.c
 *
 * This file contains the OS_MmuMapSort function
 *
 * CHECK: TABS 4 (see editor commands at end of file)
 *
 * Copyright 1998-2014 Elektrobit Automotive GmbH
 * All rights exclusively reserved for Elektrobit Automotive GmbH,
 * unless expressly agreed to otherwise.
 *
 * $Id: kern-mmusort.c 17602 2014-02-03 12:09:35Z tojo2507 $
*/

#include <Os_kernel.h>
#include <Os_mmu.h>

#include <memmap/Os_mm_code_begin.h>

/*!
 * OS_MmuMapSort
 *
 * This function sorts an existing MMU page map into ascending order of base address.
 * A simple swap algorithm is used because:
 *	- the function is only used at startup
 *	- the number of entries during the sorting phase is small
 *
 * The algorithm operates thusly:
 *	- run through the array; at each element xx:
 *		- run through the array starting with xx and find the smallest.
 *		- if the xx isn't the smallest, swap the smallest and the xx.
 *
 * MISRA note: all array operations are performed using indexes rather than pointers in conformance with Rule 17.4
 * However, the array parameter is declared as a pointer rather than an array, because, although it might appear
 * clearer locally (subjective!), the call to the function with a partial array would be more confusing.
*/
void OS_MmuMapSort(os_pagemap_t *map, os_int_t nmaps)
{
	os_int_t curr;
	os_int_t best;
	os_int_t exam;

	for ( curr = 0; curr < nmaps - 1; curr++ )
	{
		best = curr;
		for ( exam = curr + 1; exam < nmaps; exam++ )
		{
			if ( map[exam].base < map[best].base )
			{
				best = exam;
			}
		}

		if ( best != curr )
		{
			os_pagemap_t tmp = map[best];
			map[best] = map[curr];
			map[curr] = tmp;
		}
	}
}

#include <memmap/Os_mm_code_end.h>

/* Editor settings; DO NOT DELETE
 * vi:set ts=4:
*/
