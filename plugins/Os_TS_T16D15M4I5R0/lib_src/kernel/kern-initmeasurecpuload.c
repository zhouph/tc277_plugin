/* kern-initmeasurecpuload.c
 *
 * This file contains the OS_InitMeasureCpuLoad() function.
 *
 * CHECK: TABS 4 (see editor commands at end of file)
 *
 * Copyright 1998-2014 Elektrobit Automotive GmbH
 * All rights exclusively reserved for Elektrobit Automotive GmbH,
 * unless expressly agreed to otherwise.
 *
 * $Id: kern-initmeasurecpuload.c 17602 2014-02-03 12:09:35Z tojo2507 $
*/

#include <Os_kernel.h>
#include <Os_timestamp.h>
#include <Os_cpuload_kernel.h>

#include <memmap/Os_mm_var_begin.h>
os_cpuload_t OS_cpuLoad;
#include <memmap/Os_mm_var_end.h>

#include <memmap/Os_mm_code_begin.h>

/* OS_InitMeasureCpuLoad
 *
 * This function initialises the CPU load measurement system.
 *
 * It is the "startup" initialisation, it does NOT perform the functionality of the
 * API InitCpuLoad(), which merely resets the peak detector.
 *
 * !LINKSTO  Kernel.Feature.CpuLoadMeasurement.API.InitCpuLoad, 1
*/
void OS_InitMeasureCpuLoad(void)
{
	os_int_t i;

	OS_GetTimeStamp(&OS_cpuLoad.idleExitTime);
	OS_TimeCopy(&OS_cpuLoad.intervalStartTime, OS_cpuLoad.idleExitTime);
	OS_cpuLoad.busyNestingCounter = 1;
	OS_cpuLoad.busyTime = 0;
	OS_cpuLoad.busyTimeSum = 0;
	OS_cpuLoad.busyIndex = OS_cpuLoadCfg.nIntervals;
	OS_cpuLoad.currentLoad = 0;
	OS_cpuLoad.peakLoad = 0;

	for ( i = 0; i < OS_cpuLoadCfg.nIntervals; i++ )
	{
		OS_cpuLoadCfg.busyBuffer[i] = 0;
	}
}

#include <memmap/Os_mm_code_end.h>

/* Editor settings; DO NOT DELETE
 * vi:set ts=4:
*/
