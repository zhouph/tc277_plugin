/* kern-error.c
 *
 * This file contains the OS_Error function.
 *
 * CHECK: TABS 4 (see editor commands at end of file)
 *
 * Copyright 1998-2014 Elektrobit Automotive GmbH
 * All rights exclusively reserved for Elektrobit Automotive GmbH,
 * unless expressly agreed to otherwise.
 *
 * $Id: kern-error.c 18458 2014-07-15 08:40:00Z ingi2575 $
*/

/*
 * MISRA-C:2004 Deviation List
 *
 * MISRA-1) Deviated Rule: 17.4 (required)
 * Array indexing shall be the only allowed form of pointer arithmetic.
 *
 * Reason:
 * Pointer arithmetic is used for classical tasks like iterating through
 * an configuration array to make them more readable and maintainable.
 *
 *
 * MISRA-2) Deviated Rule: 16.2 (required)
 * Functions shall not call themselves, either directly or indirectly.
 *
 * Reason:
 * Centralized error-handling facility (OS_Error, OS_ErrorAction) which
 * provides several different reactions for detected errors, including
 * system shutdown or the call of a hook function. Functions for
 * triggering these reactions (e.g. OS_CallErrorHook, OS_Shutdown)
 * also handle errors using OS_Error. Implementation of OS_Error and
 * OS_ErrorAction ensures that no recursion is possible.
*/

#include <Os_kernel.h>

/* Include definitions for tracing */
#include <Os_trace.h>

#include <memmap/Os_mm_code_begin.h>

/*!
 * OS_Error
 *
 * This function is called whenever an error condition is detected.
 * It looks up the calling function id and the error condition in the
 * error tables, and takes appropriate action.
 * Finally, it returns an appropriate error code (OS_E_XXX)
 * to the caller.
 *
 * Each element of the OS_serviceErrors array is a pointer to
 * another array containing a list of error entries. Each list is
 * terminated with an entry containing an invalid error as the
 * error condition. An error entry tells the error handler what
 * to do and what to return as a result of this error. The invalid
 * error entry tells what to do for an unexpected error.
 *
 * The OS_serviceErrors entry for a service that has no error
 * conditions simply points to an invalid error entry.
 *
 * The action taken after the error has been reported comes initially
 * from the error tables, and can be modified by the hook functions
 * with write access to the OS_errorStatus structure. Furthermore,
 * the return value of ProtectionHookcan modifies the action. Finally,
 * the action taken depends on exactly where the error occurred;
 * for example an action to kill a task or ISR is meaningless if the
 * error occurred in an alarm callback routine. The local function
 * ErrorAction() decides on and takes the corrective action.
 *
 * !LINKSTO Kernel.API.Hooks.ErrorHook, 1
 *
*/
/* Deviation MISRA-2 */
os_result_t OS_Error
(	const os_serviceinfo_t * const s,	/* Infomration about the calling service */
	os_error_t e,						/* The error code */
	os_paramtype_t *p					/* The parameters to the service, OS_NULL if none */
)
{
	os_result_t res;
	os_erroraction_t act;
	os_erroraction_t hooks;
	os_uint8_t ehNestingSave;
	const os_appcontext_t *hookAppSave;
	os_errorstatus_t esSave;
	const os_errorentry_t *ep;
	const os_appcontext_t *app;
	os_intstatus_t is;
	os_uint8_t en;

	/* Determine the application of the "caller" (whatever caused the error) so that we know which
	 * application's hook function to call.
    */
	app = OS_CurrentApp();

	ep = s->errorTable;
	en = s->nErrors;

	while ( (en > 0) && (ep->condition != e) )
	{
		/* Deviation MISRA-1 */
		ep++;
		en--;
	}

	if ( (ep->condition == e) )
	{
		act = ep->action;
		res = ep->result;
	}
	else
	{
		act = OS_ACTION_SHUTDOWN|OS_ACTION_ERRORHOOK;
		res = OS_E_INTERNAL;
	}

	hooks = act & OS_hookSelection;
	act &= OS_ACTION_MASK;

	/* Check if any hook calls are needed.
	 * Recursive calls to error hook function are prevented.
	*/
	if ( OS_IS_ACTION_HOOK(hooks) )
	{
		/* !LINKSTO Kernel.API.Hooks.ErrorHook.API, 1
		 * !LINKSTO Kernel.API.Hooks.ErrorHook.Error, 1
		*/
		/* Remember where we're called from and which error hooks are nested
		*/
		ehNestingSave = OS_errorHookNesting;
		esSave = OS_errorStatus;
		hookAppSave = OS_hookApp;

		/* Hooks are always called with interrupts disabled. Among
		 * other things, this assures the integrity of the error status
		 * information
		 *
		 * !LINKSTO Kernel.HookRoutines.PrioISRC2, 1
		*/
		is = OS_IntDisableConditional();

		/* Make the extended error information available to the
		 * error hook via the OS_GetExtendedErrorStatus() function.
		*/
		OS_errorStatus.calledFrom = OS_inFunction;
		OS_errorStatus.serviceId = s->sid;

		if ( p != OS_NULL )
		{
			/* !LINKSTO Kernel.API.Hooks.Macros, 1
			*/
			os_int_t i;

			for ( i = 0; i < OS_MAXPARAM; i++ )
			{
				OS_errorStatus.parameter[i] = p[i];
			}
		}

		OS_errorStatus.errorCondition = e;
		OS_errorStatus.action = act;
		OS_errorStatus.result = res;

		/* Normally there will be only one hook function set for each
		 * kind of error, but we handle the possibility that more might
		 * be set in the future
		 * UPDATE 21.7.2005 this came true in Autosar 1.5 ;-)
		 * And the order changed - now we must call the global error hook
		 * first - so we must test for no app-hook separately.
		*/
		if ( (hooks & OS_ACTION_ERRORHOOK_APP) != 0 )
		{
			/* The application's own error hook should be called.
			 * If the application cannot be determined or the application
			 * has no hook function, the global error hook is called instead.
			 *
			 * We don't call the application hook if we're already nested
			 * inside any error hook at all.
			*/
			if ( (OS_AppIsNull(app)) ||
				 (app->errorHook == OS_NULL) ||
				 (OS_errorHookNesting != 0) )
			{
				hooks |= OS_ACTION_ERRORHOOK;
			}
		}

		/* !LINKSTO Kernel.API.Hooks.ErrorHook.StatusType, 1
		 *
		 * We don't call the error hook function if we're already
		 * inside the error hook somewhere.
		 * This can also happen if a category 1 ISR interrupts the
		 * error hook and then itself causes an error.
		*/
		if ( ((hooks & (OS_ACTION_ERRORHOOK | OS_ACTION_ERRORHOOK_EB)) != 0) &&
			 ((OS_errorHookNesting & OS_EHN_ERR) == 0) )
		{
			OS_errorHookNesting = ehNestingSave | OS_EHN_ERR;
			OS_hookApp = OS_NULL;
			OS_CALLERRORHOOK(res);
			OS_errorHookNesting = ehNestingSave;
		}

		if ( (hooks & OS_ACTION_ERRORHOOK_APP) != 0 )
		{
			/* !LINKSTO Kernel.Autosar.OsApplication.ApplicationHooks.ErrorHook.Call, 1
			 * !LINKSTO Kernel.Autosar.OsApplication.ApplicationHooks.ErrorHook.Order, 1
			 * Call the application's own error hook if there is one.
			 * If the application cannot be determined or the application
			 * has no hook function, the global error hook is called instead.
			 * At the moment, a nontrusted app can't have an error hook.
			 *
			 * We don't call the application hook if we're already nested
			 * inside any error hook at all.
			*/
			if ( (OS_AppIsNull(app)) ||
				 (OS_appsStarted == 0) ||
				 (app->errorHook == OS_NULL) ||
				 (OS_errorHookNesting != 0) )
			{
				/* Use No Hooks! */
			}
			else
			{
				/* !LINKSTO Kernel.Autosar.OsApplication.ApplicationHooks.ErrorHook.AccessRights, 1
				 * Call application's error hook
				*/
				OS_errorHookNesting = ehNestingSave | OS_EHN_APP;
				OS_hookApp = app;
				OS_CallErrorHook(app, res);
				OS_errorHookNesting = ehNestingSave;
			}
		}

		/*
		 * We don't call the protection hook function if we're already
		 * inside the protection hook somewhere.
		 * This can also happen if a category 1 ISR interrupts the
		 * protection hook and then itself causes an protection fault.
		*/
		if ( ((hooks & OS_ACTION_PROTECTIONHOOK) != 0) &&
			 ((OS_errorHookNesting & OS_EHN_PROT) == 0) )
		{
			/* !LINKSTO Kernel.Autosar.Protection.ProtectionHook, 1
			 * !LINKSTO Kernel.Autosar.Protection.ProtectionHook.Call, 1
			*/
			OS_errorHookNesting = ehNestingSave | OS_EHN_PROT;
			OS_hookApp = OS_NULL;
			OS_CALLPROTECTIONHOOK(res, act);
			OS_errorHookNesting = ehNestingSave;

			/* If the return value is legal, we store it into the global
			 * location. Otherwise we leave the global location as it is.
			 * This permits full AUTOSAR conformance while at the same time
			 * permitting the ProtectionHook to return non-AUTOSAR
			 * values by writing to the error-status and then returning
			 * an illegal value.
			 *
			*/
			if ( act == OS_ACTION_DEFAULT )
			{
				/* The action in OS_errorStatus (which is the default from the
				 * error table unless the ProtectionHook has changed it directly) is the
				 * action we take. This is an EB extended feature.
				*/
			}
			else
			if ( (act == OS_ACTION_DONOTHING) && (res == OS_E_RATEPROT) )
			{
				/* This is the Autosar SWS "Ignore" return value (PRO_IGNORE).
				 * The semantics are unclear.
				 *
				 * !LINKSTO Kernel.Autosar.Protection.ProtectionHook.DoNothing, 1
				 * !LINKSTO Kernel.Autosar.Protection.ProtectionHook.ReturnValue.IGNORE, 1
				*/
				OS_errorStatus.action = OS_ACTION_RETURN;
			}
			else
			if ( OS_ActionIsValid(act) )
			{
				OS_errorStatus.action = act;
			}
			else
			{
				OS_errorStatus.action = OS_ACTION_SHUTDOWN;
			}
		}

		/* Local copies of act and res reloaded here before re-enabling
		 * interrupts
		*/
		act = OS_errorStatus.action;
		res = OS_errorStatus.result;

		OS_IntRestore(is);

		/* Restore saved states of these variables
		 * OS_errorHookNesting was restored each time it was modified,
		 * so no need to restore it again here.
		*/
		OS_hookApp = hookAppSave;
		OS_errorStatus = esSave;
	}

	/* Now do what we're supposed to do to the perpetrator.
	 * WARNING: OS_ERRORACTION might not return under some circumstances.
	*/
	return OS_ERRORACTION(act, res);
}

#include <memmap/Os_mm_code_end.h>

/* Editor settings; DO NOT DELETE
 * vi:set ts=4:
*/
