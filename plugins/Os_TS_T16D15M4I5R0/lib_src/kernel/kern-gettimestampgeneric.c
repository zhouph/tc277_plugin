/* kern-gettimestampgeneric.c
 *
 * This file contains the OS_GetTimeStampGeneric() function.
 *
 * CHECK: TABS 4 (see editor commands at end of file)
 *
 * Copyright 1998-2014 Elektrobit Automotive GmbH
 * All rights exclusively reserved for Elektrobit Automotive GmbH,
 * unless expressly agreed to otherwise.
 *
 * $Id: kern-gettimestampgeneric.c 18739 2014-09-04 13:29:44Z mist9353 $
*/

/*
 * MISRA-C:2004 Deviation List
 *
 * MISRA-1) Deviated Rule: 16.7 (required)
 * A pointer parameter in a function prototype should be declared as pointer
 * to const if the pointer is not used to modify the addressed object.
 *
 * Reason:
 * For some architectures, function implementation may be empty as the function
 * is not used. For all others, the pointer is used to modify the addressed
 * object.
*/
/*
 * TOOLDIAG List of possible tool diagnostics
 *
 * TOOLDIAG-1) Possible diagnostic: UnusedParameter
 *   unused parameter 'out'
 *
 * Reason:
 * For some architectures, function implementation may be empty as the function
 * is not used. For all others, the pointer is used to modify the addressed
 * object.
 */

#include <Os_kernel.h>
#include <Os_timestamp.h>

#include <memmap/Os_mm_code_begin.h>

/* OS_GetTimeStampGeneric
 *
 * This function puts a timestamp value into the indicated "out" location.
 *
 * The value stored is the sum of the last updated time stamp and the number of ticks that
 * have occurred since then. The global time stamp value is *not* updated!
*/
/* Possible diagnostic TOOLDIAG-1*/
/* Deviation MISRA-1 */
void OS_GetTimeStampGeneric(os_timestamp_t *out)
{
#if OS_USEGENERICTIMESTAMP
	os_timervalue_t newTmrVal;
	os_timervalue_t diff;
	os_intstatus_t is;

	OS_PARAM_UNUSED(out);

	if ( OS_timeStampTimer != OS_NULL )
	{
		is = OS_IntDisableAll();

		newTmrVal = OS_HwtRead(OS_timeStampTimer);
		diff = OS_HwtSub(OS_timeStampTimer, newTmrVal, OS_lastTimeStampTime);
		OS_TimeAdd32(out, OS_timeStampValue, diff);

		OS_IntRestoreAll(is);
	}
#endif
}

/* API entries for User's Guide
 * CHECK: NOPARSE

<api API="OS_USER">
  <name>OS_GetTimeStamp</name>
  <synopsis>Puts a timestamp value into the indicated location</synopsis>
  <syntax>
    void OS_GetTimeStamp
    (   os_timestamp_t out  /@ Destination location for timestamp value @/
    )
  </syntax>
  <description>
    <code>OS_GetTimeStamp()</code> stores the current timestamp value into the indicated
    "out" location. A timestamp is a counter that can never overflow during the expected
    up-time of the processor.
  </description>
  <availability>
    No restrictions.
  </availability>
</api>

<api API="OS_USER">
  <name>OS_TimeGetLo</name>
  <synopsis>Returns low word of a timestamp value</synopsis>
  <syntax>
    os_uint32_t OS_TimeGetLo
    ( os_timestamp_t t /@ Source timestamp value @/
    )
  </syntax>
  <description>
    <code>OS_TimeGetLo()</code> returns the low word of a given timestamp value.
  </description>
  <availability>
    No restrictions.
  </availability>
  <returns>=Low word of the timestamp value.</returns>
</api>

<api API="OS_USER">
  <name>OS_TimeGetHi</name>
  <synopsis>Returns high word of a timestamp value</synopsis>
  <syntax>
    os_uint32_t OS_TimeGetHi
    ( os_timestamp_t t /@ Source timestamp value @/
    )
  </syntax>
  <description>
    <code>OS_TimeGetHi()</code> returns the high word of a given timestamp value.
  </description>
  <availability>
    No restrictions.
  </availability>
  <returns>=High word of the timestamp value.</returns>
</api>

<api API="OS_USER">
  <name>OS_TimeSub64</name>
  <synopsis>Returns high word of a timestamp value</synopsis>
  <syntax>
    void OS_TimeSub64
    ( os_timestamp_t *diffTime /@ Destination @/,
      const os_timestamp_t *newTime /@ new time value @/,
      const os_timestamp_t *oldTime /@ old time value @/
    )
  </syntax>
  <description>
    OS_TimeSub64() calculates the difference
    (<code>newTime</code> - <code>oldTime</code>)
    (i.e. the duration of the interval that starts at
    <code>oldTime</code> and ends at <code>newTime</code>).
    The two input values are variables provided by
    the caller whose addresses are passed as parameters. The result is placed into
    the variable whose address is specified by the <code>diffTime</code>
    parameter. The caller must have permission to modify this variable.
  </description>
  <availability>
    No restrictions.
  </availability>
</api>

 * CHECK: PARSE
*/

#include <memmap/Os_mm_code_end.h>

/* Editor settings; DO NOT DELETE
 * vi:set ts=4:
*/
