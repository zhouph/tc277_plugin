/* kern-starttimestamptimergeneric.c
 *
 * This file contains the OS_StartTimestampTimerGeneric function.
 *
 * CHECK: TABS 4 (see editor commands at end of file)
 *
 * Copyright 1998-2014 Elektrobit Automotive GmbH
 * All rights exclusively reserved for Elektrobit Automotive GmbH,
 * unless expressly agreed to otherwise.
 *
 * $Id: kern-starttimestamptimergeneric.c 17602 2014-02-03 12:09:35Z tojo2507 $
*/

#include <Os_kernel.h>

#include <memmap/Os_mm_code_begin.h>

/* OS_StartTimeStampTimerGeneric
 *
 * The timestamp timer is started with an interrupt duration of its max delta.
 *
 * This function must be called during OS_KernStartOs(), after OS_InitTimers() and OS_InitInterrupts().
 * It must be called before any auto-activated alarms are started, in case the same timer is being
 * used to drive a hardware counter.
*/
void OS_StartTimeStampTimerGeneric(void)
{
#if OS_USEGENERICTIMESTAMP
	if ( OS_timeStampTimer != OS_NULL )
	{
		OS_HwtStart(OS_timeStampTimer, OS_lastTimeStampTime, OS_timeStampTimer->maxDelta);
	}
#endif
}

#include <memmap/Os_mm_code_end.h>

/* Editor settings; DO NOT DELETE
 * vi:set ts=4:
*/
