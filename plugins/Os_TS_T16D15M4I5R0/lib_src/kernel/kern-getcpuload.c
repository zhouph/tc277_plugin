/* kern-getcpuload.c
 *
 * This file contains the OS_KernGetCpuLoad() function.
 *
 * CHECK: TABS 4 (see editor commands at end of file)
 *
 * Copyright 1998-2014 Elektrobit Automotive GmbH
 * All rights exclusively reserved for Elektrobit Automotive GmbH,
 * unless expressly agreed to otherwise.
 *
 * $Id: kern-getcpuload.c 17602 2014-02-03 12:09:35Z tojo2507 $
*/

#include <Os_kernel.h>
#include <Os_cpuload.h>
#include <Os_cpuload_kernel.h>

#include <memmap/Os_mm_code_begin.h>

/* OS_KernGetCpuLoad
 *
 * This function returns either the current or the peak CPU load.
 *
 * Before returning the value, we update to include the current busy interval. To perform
 * the computation we "enter" the idle state and then "leave" it again.
 *
 * NOTE: this function must not clear the busyNestingCounter because we enter idle mode only temporarily,
 * and the nesting must remain correct when we return to busy mode at the end. For this reason we don't
 * use OS_LeaveIdleState() either.
 *
 * !LINKSTO Kernel.Feature.CpuLoadMeasurement, 1
 * !LINKSTO Kernel.Feature.CpuLoadMeasurement.API.GetCpuLoad, 1
 *
*/

os_uint8_t OS_KernGetCpuLoad(os_boolean_t getPeak)
{
	os_intstatus_t is;
	os_uint8_t result = 255u;		/* "Error" return value */

	if ( (OS_configMode & OS_CPULOAD) != 0 )
	{
		is = OS_IntDisableAll();
		OS_MeasureCpuLoad();
		result = getPeak ? OS_cpuLoad.peakLoad : OS_cpuLoad.currentLoad;
		OS_LeaveIdleState();
		OS_IntRestoreAll(is);
	}

	return result;
}

#include <memmap/Os_mm_code_end.h>

/* API entries for User's Guide
 * CHECK: NOPARSE

<api API="OS_USER">
  <name>OS_UserGetCpuLoad</name>
  <synopsis>Return the CPU load as an integer percentage.</synopsis>
  <syntax>
    os_uint8_t OS_UserGetCpuLoad
    (   os_boolean_t getPeak;    /@ Returns peak load if true, otherwise current load @/
    )
  </syntax>
  <description>
    <para>
    <code>OS_UserGetCpuLoad()</code> returns either the current or the peak CPU load, depending on the
    value of the <code>getPeak</code> parameter. The return value is a percentage in the range 0 to
    100.
    </para>
    <para>
    If CPU load measurement is disabled, no action takes place and the function returns 255 (0xff).
    </para>
  </description>
  <availability>
    Include the <code>Os_salsa.h</code> header file.
  </availability>
  <returns>0..100=CPU load as percentage</returns>
  <returns>255=Measurement is not enabled</returns>
</api>

<api API="OS_USER">
  <name>GetCpuLoad</name>
  <synopsis>Return the current CPU load as an integer percentage.</synopsis>
  <syntax>
    os_uint8_t GetCpuLoad (void)
  </syntax>
  <description>
    <para>
    <code>GetCpuLoad()</code> returns the current CPU load. The return value is a percentage in the range 0 to
    100.
    </para>
    <para>
    If CPU load measurement is disabled, no action takes place and the function returns 255 (0xff).
    </para>
  </description>
  <availability>
    Include the <code>Os_salsa.h</code> header file. This function must not be called from non-trusted
    applications; instead, <code>OS_UserGetCpuLoad()</code> can be used.
  </availability>
  <returns>0..100=CPU load as percentage</returns>
  <returns>255=Measurement is not enabled</returns>
</api>

<api API="OS_USER">
  <name>GetMaxCpuLoad</name>
  <synopsis>Return the peak CPU load as an integer percentage.</synopsis>
  <syntax>
    os_uint8_t GetMaxCpuLoad (void)
  </syntax>
  <description>
    <para>
    <code>GetMaxCpuLoad()</code> returns the peak CPU load. The return value is a percentage in the range 0 to
    100.
    </para>
    <para>
    If CPU load measurement is disabled, no action takes place and the function returns 255 (0xff).
    </para>
  </description>
  <availability>
    Include the <code>Os_salsa.h</code> header file. This function must not be called from non-trusted
    applications; instead, <code>OS_UserGetCpuLoad()</code> can be used.
  </availability>
  <returns>0..100=CPU load as percentage</returns>
  <returns>255=Measurement is not enabled</returns>
</api>

 * CHECK: PARSE
*/

/* Editor settings; DO NOT DELETE
 * vi:set ts=4:
*/
