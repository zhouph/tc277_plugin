/* lib-getcurrentstackarea.c
 *
 * This file contains the OS_GetCurrentStackArea function.
 *
 * CHECK: TABS 4 (see editor commands at end of file)
 *
 * Copyright 1998-2014 Elektrobit Automotive GmbH
 * All rights exclusively reserved for Elektrobit Automotive GmbH,
 * unless expressly agreed to otherwise.
 *
 * $Id: kern-getcurrentstackarea.c 17602 2014-02-03 12:09:35Z tojo2507 $
*/

#include <Os_api.h>
#include <Os_kernel.h>		/* For OS_GetCurrentSp(), os_task_t etc. */

#include <memmap/Os_mm_code_begin.h>

/*!
 * OS_GetCurrentStackArea() returns base and limit addresses for the stack area of the current executing thread
 *
 * !LINKSTO Kernel.Feature.GetCurrentStackArea, 1
 * !LINKSTO Kernel.Feature.GetCurrentStackArea.ValidContexts, 1
 *
 * On XC2000, the "generic" fields contain the software stack. The system stack fields are prefixed with "sys"
*/
void OS_GetCurrentStackArea(void **begin, void **end)
{
	os_address_t rBegin = 0;
	os_address_t rEnd = 0;
	const os_isr_t *isr;

	if ( OS_inFunction == OS_INTASK )
	{
		if ( OS_taskCurrent != OS_NULL )			/* Should always be true ... */
		{
			rBegin = (os_address_t)(OS_taskCurrent->stackBase);
			rEnd = rBegin + OS_taskCurrent->stackLen;
		}
	}
	else
	if ( OS_inFunction == OS_INCAT2 )
	{
		if ( OS_isrCurrent < OS_nInterrupts )		/* Should always be true ... */
		{
			isr = &OS_isrTableBase[OS_isrCurrent];

			rBegin = (os_address_t)(OS_GetIsrStackBase(isr));

			if ( rBegin == 0u )
			{
				rBegin = (os_address_t)OS_iStackBase;
				rEnd = rBegin + OS_iStackLen;
			}
			else
			{
				rEnd = rBegin + isr->stackLen;
			}
		}
	}
	else
	{
		/* MISRA-C */
	}

	if ( begin != OS_NULL )
	{
		*begin = (void *) rBegin;
	}

	if ( end != OS_NULL )
	{
		*end = (void *) rEnd;
	}
}

/* API entries for User's Guide
 * CHECK: NOPARSE

<api API="OS_USER">
  <name>OS_GetCurrentStackArea</name>
  <synopsis>Get current stack boundaries</synopsis>
  <syntax>
    void OS_GetCurrentStackArea(void **begin, void **end)
  </syntax>
  <description>
    <code>OS_GetCurrentStackArea()</code> it places
    the base and limit addresses of the stack of the currently-executing object into the two referenced variables.
    For a Task, this is simply the stack area as allocated by the OS generator. For ISRs, if the ISR has a private
    stack, this is returned. Otherwise the entire kernel stack area is returned. This does not imply that
    the whole area is accessible by the caller.
  </description>
  <availability>
    Can be used from all tasks and Category 2 ISRs.
  </availability>
</api>

 * CHECK: PARSE
*/

#include <memmap/Os_mm_code_end.h>

/* Editor settings; DO NOT DELETE
 * vi:set ts=4:
*/
