/* kern-startupchecksschedule.c
 *
 * This file contains the OS_StartupChecksSchedule function.
 *
 * CHECK: TABS 4 (see editor commands at end of file)
 *
 * Copyright 1998-2014 Elektrobit Automotive GmbH
 * All rights exclusively reserved for Elektrobit Automotive GmbH,
 * unless expressly agreed to otherwise.
 *
 * $Id: kern-startupchecksschedule.c 17602 2014-02-03 12:09:35Z tojo2507 $
*/

#include <Os_kernel.h>
#include <Os_panic.h>

#include <memmap/Os_mm_code_begin.h>

/* OS_StartupChecksSchedule
 *
*/
os_result_t OS_StartupChecksSchedule(void)
{
	os_result_t result = OS_E_OK;
	os_unsigned_t i;
	os_unsigned_t j;
	const os_schedule_t *schedule;
	const os_scheduleentry_t *evt;
	os_eventmask_t foundevents;
	os_tick_t counterMax;
	os_tick_t extraDelay;
	os_tick_t lastOffset;
	os_tick_t delay;
	os_boolean_t syncable;

	for ( i = 0; i < OS_nSchedules; i++ )
	{
		schedule = &OS_schedule[i];

		if ( (OS_GET_APP(schedule->app) != OS_NULL) &&
				((OS_GET_APP(schedule->app->permission) & OS_GET_APP(schedule->permissions)) == 0) )
		{
			result = OS_PANIC(OS_PANIC_SCHK_OwningApplicationHasNoPermission);
		}

		counterMax = OS_counter[OS_alarm[schedule->alarm].counter].maxallowedvalue;
		syncable = (schedule->flags & OS_ST_SYNCABLE) != 0;

		foundevents = 0;
		lastOffset = 0;
		extraDelay = schedule->period - schedule->table[schedule->nEntries-1].offset;

		for ( j = 0; j < schedule->nEntries; j++ )
		{
			evt = &schedule->table[j];

			/* Remember events that have been set at this offset.
			 * Reset memory of events at each new offset
			*/
			if ( evt->offset == lastOffset )
			{
				foundevents |= evt->event;
			}
			else
			{
				foundevents = 0;
			}

			if ( (foundevents != 0) && (evt->event == 0) )
			{
				result = OS_PANIC(OS_PANIC_SCHK_ScheduleTableActivateTaskAfterSetEvent);
			}

			if ( evt->offset < lastOffset )
			{
				result = OS_PANIC(OS_PANIC_SCHK_ScheduleTableEventsNotInOrder);
			}

			/* These checks don't make sense if the offset is wrong.
			 * Furthermore, the sync-range checks are only done for the first event at a given offset.
			 * The sync parameters for the remainder of the events (at the same offset) are not used,
			 * so the OS doesn't care. Maybe the generator sets them to zero, maybe to the same as the
			 * first event at this offset.
			*/
			if ( (j == 0) || (evt->offset != lastOffset) )
			{
				/* This is the delay from the last expiry point to this one.
				*/
				delay = (evt->offset - lastOffset) + extraDelay;

				if ( delay > counterMax )
				{
					result = OS_PANIC(OS_PANIC_SCHK_DelayGreaterThanCounterMax);
				}

				if ( syncable && ((delay + evt->maxIncrease) > counterMax) )
				{
					result = OS_PANIC(OS_PANIC_SCHK_DelayPlusMaxIncreaseGreaterThanCounterMax);
				}

				if ( syncable && (evt->maxDecrease >= delay) )
				{
					result = OS_PANIC(OS_PANIC_SCHK_MaxDecreaseGreaterThanDelay);
				}

				if ( syncable && ((evt->maxDecrease + evt->maxIncrease) > schedule->period) )
				{
					result = OS_PANIC(OS_PANIC_SCHK_SynchronisationRangeGreaterThanScheduleTableLength);
				}
			}
			lastOffset = evt->offset;
			extraDelay = 0;
		}
	}

	return result;
}

#include <memmap/Os_mm_code_end.h>

/* Editor settings; DO NOT DELETE
 * vi:set ts=4:
*/
