/* TRICORE-initsp.c
 *
 * CHECK: TABS 4 (see editor commands at end of file)
 *
 * Copyright 1998-2014 Elektrobit Automotive GmbH
 * All rights exclusively reserved for Elektrobit Automotive GmbH,
 * unless expressly agreed to otherwise.
 *
 * $Id: TRICORE-initsp.c 17602 2014-02-03 12:09:35Z tojo2507 $
*/

#include <Os_kernel.h>
#include <Os_tool.h>

#include <memmap/Os_mm_code_begin.h>

/* OS_InitSp() - initialise the stack pointer. DO NOT CALL FROM C!
 *
 * This function initialises the stack pointer for the startup code.
 * We can safely use the entire stack area, because StartOS is a
 * system call and so switches to the kernel/isr stack.
 *
 * It is called via a JL instruction from the startup code,
 * and therefore must return with a special instruction (JI A11).
 * No function calls are allowed inside this function.
 *
 * This function is needed by TRICORE BSPs.
 *
 * No COV macros because we're not in a proper C environment yet.
*/
void OS_InitSp(void);
void OS_InitSp(void)
{
	OS_MTSP(OS_iSpInitial);
	OS_RFJL();     /* Special "return" */
}

#include <memmap/Os_mm_code_end.h>

/* Editor settings; DO NOT DELETE
 * vi:set ts=4:
*/
