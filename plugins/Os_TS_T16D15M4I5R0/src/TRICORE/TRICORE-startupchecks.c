/* TRICORE-startupchecks.c
 *
 * This file contains the OS_StartupChecksTricore function.
 *
 * CHECK: TABS 4 (see editor commands at end of file)
 *
 * Copyright 1998-2014 Elektrobit Automotive GmbH
 * All rights exclusively reserved for Elektrobit Automotive GmbH,
 * unless expressly agreed to otherwise.
 *
 * $Id: TRICORE-startupchecks.c 17602 2014-02-03 12:09:35Z tojo2507 $
*/

#include <Os_configuration.h>
#include <Os_kernel.h>
#include <Os_panic.h>


#include <memmap/Os_mm_code_begin.h>

/* OS_StartupChecksTricore
 *
 * This function checks (on multicore processors) that the OS is executed on the correct core.
*/
os_result_t OS_StartupChecksTricore(void)
{
	os_result_t result = OS_E_OK;

#if (OS_TRICOREARCH == OS_TRICOREARCH_16EP)
	if ( OS_TricoreGetCoreId() != OS_TRICORE_CORE_ID )
	{
		result = OS_PANIC(OS_PANIC_SCHK_InvalidCore);
	}
#endif

	return result;
}

#include <memmap/Os_mm_code_end.h>

/* Editor settings; DO NOT DELETE
 * vi:set ts=4:
*/
