/* Os_configuration_TRICORE.c
 *
 * This file provides architecture-specific kernel configuration data
 * for TRICORE for a microkernel-based system.
 *
 * CHECK: TABS 4 (see editor commands at end of file)
 *
 * Copyright 1998-2014 Elektrobit Automotive GmbH
 * All rights exclusively reserved for Elektrobit Automotive GmbH,
 * unless expressly agreed to otherwise.
 *
 * $Id: Os_configuration_microkernel_TRICORE.c 17602 2014-02-03 12:09:35Z tojo2507 $
*/

#include <Os_configuration.h>
#include <Os_kernel.h>
#include <Mk_qmboard.h>
#include <TRICORE/Os_TRICORE_cpu.h>

#include <memmap/Os_mm_const_begin.h>

#if (OS_TOOL == OS_tasking)
/* This variable is used by assorted kernel and driver routines for flushing the processor
 * pipeline by reading back a hardware register immediately after writing a value to it.
 * It is only used for the Tasking compiler, and is only really necessary for older
 * versions (2.1, 2.2r1???)
 * See also Os_tool_TRICORE_tasking.h
*/
#include <memmap/Os_mm_var32_begin.h>
volatile os_uint32_t OS_junk;
#include <memmap/Os_mm_var32_end.h>

#endif



/* Editor settings - DO NOT DELETE
 * vi:set ts=4:
*/
