/* TRICORE-initcsalist.c
 *
 * CHECK: TABS 4 (see editor commands at end of file)
 *
 * Copyright 1998-2014 Elektrobit Automotive GmbH
 * All rights exclusively reserved for Elektrobit Automotive GmbH,
 * unless expressly agreed to otherwise.
 *
 * $Id: TRICORE-initcsalist.c 19578 2014-10-30 16:08:47Z thdr9337 $
*/

/* TOOLDIAG List of possible tool diagnostics
 *
 * TOOLDIAG-1) Possible diagnostic: InfiniteLoop
 *   Possible infinite loop.
 *
 * Reason: Infinite loop is wanted here.
 */

#include <Os.h>
#include <board.h>
#include <TRICORE/Os_TRICORE_core.h>
#include <Os_kernel.h>

#include <memmap/Os_mm_code_begin.h>

/* Default 6 extra CSAs to handle the "short of CSAs" exception.
*/
#ifdef OS_N_CSA_EXTRA
#if (OS_N_CSA_EXTRA > 0)
#define N_CSA_EXTRA		OS_N_CSA_EXTRA
#else
#define N_CSA_EXTRA		6u
#endif /* (OS_N_CSA_EXTRA > 0) */
#else
#define N_CSA_EXTRA		6u
#endif /* OS_N_CSA_EXTRA */

/* These are not real variables: they are marker symbols set up by the linker script
 * CHECK: SAVE
 * CHECK: RULE 401 OFF (not variables)
*/
extern os_uint32_t OS_CSAMEMORY_BEGIN, OS_CSAMEMORY_END;
/* CHECK: RESTORE
*/

/* OS_initCsaList() - initialise the free CSA list. DO NOT CALL FROM C!
 *
 * This function initialises the CSA list in internal memory. The number
 * of CSAs is specified by the configurator in os.h
 * It is called via a JL instruction from the startup code,
 * and therefore must return with a special instruction (JI A11).
 * No function calls are allowed inside this function. TRICORE_CxFromAddr
 * is therefore a macro.
 *
 * WARNING: inserting code coverage instrumentation in this function might break it.
*/
void OS_InitCsaList(void);
void OS_InitCsaList(void)
{
	register os_uint32_t align;
	register os_uint32_t cx;
	register os_uppercx_t *pcx;
	register os_uint32_t nCx;
	register os_uint32_t i;

	/* Force the CSA start address to the correct alignment (64 bytes)
	 * Lower 6 bits must be zero, and we always round upwards.
	*/
	align = (((os_uint32_t)&OS_CSAMEMORY_BEGIN) + 0x3fu) & 0xffffffc0u;
	pcx = (os_uppercx_t *)align;
	cx = OS_CxFromAddr(align);

	/* Force the CSA end address to the correct alignment (64 bytes)
	 * Lower 6 bits must be zero, and we always round downwards.
	*/
	align = ((os_uint32_t)&OS_CSAMEMORY_END) & 0xffffffc0u;
	nCx = (align - (os_uint32_t)pcx) >> 6;

	/* Check that we have enough CSAs available:
	 * - OS_CSAMEMORY_BEGIN must be smaller then OS_CSAMEMORY_END
	 * - Number of CSAs must be greater than N_CSA_EXTRA so that we have at least
	 *   1 CSA in the main list to work with before we go to exception handling
	*/
	if (    ((os_uint32_t)&OS_CSAMEMORY_BEGIN >= (os_uint32_t)&OS_CSAMEMORY_END)
	     || (nCx <= N_CSA_EXTRA)
	   )
	{
		/* Possible diagnostic TOOLDIAG-1 <1> */
		while (1)
		{
			/* can't continue: not enough CSAs */
		}
	}

	/* Head of the list.
	*/
	OS_MTCR(OS_FCX, cx);

	/* Set limit register (exception occurs when FCX gets here).
	 * This assumes that nCx is greater than N_CSA_EXTRA which is checked above.
	*/
	OS_MTCR(OS_LCX, cx + (nCx - N_CSA_EXTRA));

	/* CSA list, including "normal" CSAs and extra CSAs for handling exceptions.
	 * We initialise 1 CSA fewer than we have in this loop because the final CSA's
	 * link must be zero.
	*/
	for ( i = 0; i < (nCx - 1u); i++ )
	{
		cx++;
		pcx[i].pcxi = cx;
	}

	/* Tail of list. Another exception occurs when FCX is 0,
	 * but we can't handle it because there are no CSAs!
	*/
	pcx[i].pcxi = 0;

	OS_RFJL();		/* Special "return" */
}

#include <memmap/Os_mm_code_end.h>

/* Editor settings; DO NOT DELETE
 * vi:set ts=4:
*/
