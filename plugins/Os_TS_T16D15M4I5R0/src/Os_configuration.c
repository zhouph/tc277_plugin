/* Os_configuration.c
 *
 * This file contains the architecture-independent kernel configuration data
 *
 * CHECK: TABS 4 (see editor commands at end of file)
 *
 * Copyright 1998-2014 Elektrobit Automotive GmbH
 * All rights exclusively reserved for Elektrobit Automotive GmbH,
 * unless expressly agreed to otherwise.
 *
 * $Id: Os_configuration.c 20489 2015-02-18 13:51:28Z stpo8218 $
*/

/* MISRA-C:2004 Deviation List
 *
 * MISRA-1) Deviated Rule: 11.1 (required)
 * Conversions shall not be performed between a pointer to a function and
 * any type other than an integral type.
 *
 * Reason:
 * Depending on the configuration, a "null function" pointer using a compatible
 * prototype might be used instead of the real function pointer.
 *
 *
 * MISRA-2) Deviated Rule: 11.1 (required)
 * Conversions shall not be performed between a pointer to a function and
 * any type other than an integral type.
 *
 * Reason:
 * For filling the system call table, a "generic" function pointer using a
 * compatible prototype is used.
*/

#include <Os_configuration.h>
#include <Os_syscalls.h>
#include <Os_kernel.h>
#include <Os_cpuload_kernel.h>
#include <Os_cpuload.h>
#include <board.h>
#include <Os_proosek.h>
#include <Os_removeapi.h>
#include <Os_taskqueue.h>

#if (OS_IOC_ENABLED==1)
#include <Ioc/Ioc_kern.h>
#endif /* OS_IOC_ENABLED */

#ifndef OS_GENERATION_ID_OS_H
#error "OS_GENERATION_ID_OS_H is not defined."
#endif

#ifndef OS_GENERATION_ID_OS_CONF_H
#error "OS_GENERATION_ID_OS_CONF_H is not defined"
#endif

#if (defined(OS_GENERATION_ID_OS_H)) && (defined(OS_GENERATION_ID_OS_CONF_H))

#if (OS_GENERATION_ID_OS_H == OS_GENERATION_ID_OS_CONF_H)

#include <memmap/Os_mm_const32_begin.h>
const os_uint32_t OS_configurationId = OS_GENERATION_ID_OS_H;
#include <memmap/Os_mm_const32_end.h>

#else
#error "OS_GENERATION_ID_OS_H and OS_GENERATION_ID_OS_CONF_H are different"
#endif

#endif

/*!
 * OS_configMode
 *
 * This constant holds various aspects of the kernel's configuration.
 * There are sevaral bitfields, representing configuration modes
 *  - BCC1/BCC2/ECC1/ECC2
 *  - STANDARD/EXTENDED
 *  - etc. See Os_kernel.h
*/
#include <memmap/Os_mm_const16_begin.h>
const os_uint32_t OS_configMode = OS_CONFIGMODE;
#include <memmap/Os_mm_const16_end.h>

/*!
 * OS_iecMode
 *
 * This constant determines how the checks for "interrupts disabled"
 * in the system services is performed. It can take one of three values:
 *
 *	OS_IEC_OSEK - the kernel performs only those checks necessary
 *		for correct functioning
 *	OS_IEC_OSEKEXTRA - the kernel performs checks to achieve similar
 *		behaviour to ProOSEK4 with EXTRA_RUNTIME_CHECKS turned on,
 *		except that the errors are detected much earlier and don't
 *		result in ShutdownOS.
 *	OS_IEC_AUTOSAR - the kernel performs strict Autosar checking,
 *		which is somewhat pointless and might be a little irritating
 *		However, that's what is specified :-(
*/
#include <memmap/Os_mm_const8_begin.h>
const os_uint8_t OS_iecMode = OS_IECMODE;
#include <memmap/Os_mm_const8_end.h>

/*!
 * OS_hookSelection
 *
 * This constant determines globally which hook functions will be called.
 * It has 4 bits, corresponding to the OS_ACTION_xxxHOOKxxx constant macros in Os_error.h:
 *
 * OS_ACTION_PROTECTIONHOOK  - the protection hook will get called
 * OS_ACTION_ERRORHOOK_APP   - the application-specific error hook will get called
 * OS_ACTION_ERRORHOOK       - the global error hook will get called for StatusType APIs
 * OS_ACTION_ERRORHOOK_EB    - the global error hook will get called for non-StatusType APIs
*/
#include <memmap/Os_mm_const8_begin.h>
const os_uint8_t OS_hookSelection = OS_HOOKSELECTION;
#include <memmap/Os_mm_const8_end.h>

/*!
 * OS_nApps
 *
 * The number of applications in the application table.
 *
 * OS_appTable
 *
 * An array of application contexts. These specify which memory
 * the application is allowed to access.
*/

#if OS_NAPPS==0

#define OS_K_GETAPPLICATIONID		&OS_KernUnknownSyscall
#define OS_K_TERMINATEAPPLICATION	&OS_KernUnknownSyscall

#define OS_APPBASE					OS_NULL
#define OS_APPDYNAMICBASE			OS_NULL

#define OS_KILLHOOKFUNC				OS_NULL


#else

#define OS_CFG_INITAPPDATA			&OS_InitApplicationData
#define OS_APPBASE					OS_appTable

#define OS_KILLHOOKFUNC				&OS_KillHook

#include <memmap/Os_mm_const_begin.h>
const os_appcontext_t OS_appTable[OS_NAPPS] = { OS_APPCONFIG };
#include <memmap/Os_mm_const_end.h>

#ifdef OS_ARCH_HAS_APPDYNAMIC

#define OS_APPDYNAMICBASE			OS_appDynamic

#include <memmap/Os_mm_var_begin.h>
os_appdynamic_t OS_appDynamic[OS_NAPPS]; /* = { 0 }	*/
#include <memmap/Os_mm_var_end.h>

#else

#define OS_APPDYNAMICBASE			OS_NULL

#endif

#endif

#include <memmap/Os_mm_const_begin.h>
const os_appcontext_t * const OS_appTableBase = OS_APPBASE;
#ifdef OS_ARCH_HAS_APPDYNAMIC
os_appdynamic_t * const OS_appDynamicBase = OS_APPDYNAMICBASE;
#endif
const os_killhookfunc_t OS_killHookFunc = OS_KILLHOOKFUNC;
#include <memmap/Os_mm_const_end.h>

#include <memmap/Os_mm_const8_begin.h>
const os_uint8_t OS_nApps = OS_NAPPS;
#include <memmap/Os_mm_const8_end.h>

/*!
 * OS_nFunctions
 *
 * The number of trusted functions in the table.
 *
 * OS_functionTable
 *
 * An array of trusted functions. These specify the function address,
 * owner, permissions and stack requirements for each trusted function.
*/
#if OS_NFUNCTIONS==0

#define OS_K_CALLTRUSTEDFUNCTION	&OS_KernUnknownSyscall
#define OS_TFBASE					OS_NULL

#else

#define OS_TFBASE					OS_functionTable

const os_function_t OS_functionTable[OS_NFUNCTIONS] = { OS_FUNCTIONCONFIG };

#endif

#include <memmap/Os_mm_const_begin.h>
const os_functionid_t OS_nFunctions = OS_NFUNCTIONS;
const os_function_t * const OS_functionTableBase = OS_TFBASE;
#include <memmap/Os_mm_const_end.h>

/*!
 * OS_nTasks
 *
 * The number of tasks in the task table.
 *
 * OS_maxPrio
 *
 * This constant contains the highest priority that a task can take.
 * It is used is several places - for example, as the priority for a task that
 * takes a resource that is shared with an ISR.
 *
 * OS_taskTable
 *
 * OS_taskTable is an array of tasks containing the configuration of
 * each task.
 *
 * In principle we can choose whether to initialise the task stacks or
 * not. At the moment they are always initialised.
 *
 * OS_taskPtrs
 *
 * This array is a list of tasks corresponding to the OS_taskActivations
 * array. Each task owns 1 or more slots in the OS_taskActivations array,
 * and for each slot owned, the corresponding slot (same index) in
 * OS_taskPtrs points to the owner task. The first element of the array
 * is not used (OS_NULL). Index 0 is used as a null end-of-linked-list value.
*/
#include <memmap/Os_mm_const_begin.h>

#if OS_NTASKS==0

#define OS_ACTITASKFUNC			OS_NULL
#define OS_KILLTASKFUNC			(os_killtaskfunc_t)&OS_NullFunction

#define OS_K_ACTIVATETASK		&OS_KernUnknownSyscall
#define OS_K_TERMINATETASK		&OS_KernUnknownSyscall
#define OS_K_CHAINTASK			&OS_KernUnknownSyscall
#define OS_K_SCHEDULE			&OS_KernUnknownSyscall
#define OS_K_GETTASKID			&OS_KernUnknownSyscall
#define OS_K_GETTASKSTATE		&OS_KernUnknownSyscall

#define OS_TASKBASE				OS_NULL

#if OS_USE_CLZ_QUEUE_ALGORITHM
/* This wastes 4 (or 2) bytes or RAM, but the alternative would be to have a base pointer
 * which would slow down the scheduling a bit. Since having no tasks is a bit of an
 * unusual case the waste is probably OK
*/
#define OS_NSLAVEWORDS			1
#else
#define OS_TASKACT				OS_NULL
#endif

#else

#define OS_CFG_INITTASKS		&OS_InitTasks

#if (OS_CONFIGMODE & OS_STACKCHECK)
#define OS_CFG_INITTASKSTACKS	&OS_InitTaskStacks
#endif

#define OS_ACTITASKFUNC			&OS_ActiTask

#ifdef OS_EXCLUDE_KILLTASK
#define OS_KILLTASKFUNC			(os_killtaskfunc_t)&OS_NullFunction
#else
#define OS_KILLTASKFUNC			&OS_KillTask
#endif

#define OS_TASKBASE				OS_taskTable

#if OS_USE_CLZ_QUEUE_ALGORITHM
#define OS_NSLAVEWORDS			(((OS_NPRIORITIES + OS_CLZWORD_NBITS) - 1)/OS_CLZWORD_NBITS)
#else
#define OS_TASKACT				OS_NULL,OS_TASKACTIVATIONS
#endif

const os_task_t OS_taskTable[OS_NTASKS] = { OS_TASKCONFIG };

#endif

const os_taskid_t OS_nTasks = OS_NTASKS;

#if OS_USE_CLZ_QUEUE_ALGORITHM

const os_int_t OS_nPriorities = OS_NPRIORITIES;
const os_int_t OS_nPrioritySlots = OS_NPRIORITYSLOTS;

/* For OS_maxPrio: there is a generated macro OS_MAXTASKPRIO, but that doesn't include
 * RES_SCHEDULER (or any other highest-prio resource) in BCC1/ECC1, so it is not correct here
*/
const os_taskprio_t OS_maxPrio = ((OS_NPRIORITIES==0) ? 0 : (OS_NPRIORITIES-1));

#if OS_NPRIORITIES == 0
const os_priorityqueue_t OS_priorityQueue[1] = { { OS_NULL, OS_NULL, OS_NULL, 0, 0, 0 } };
#else
const os_priorityqueue_t OS_priorityQueue[OS_NPRIORITIES] = { OS_PRIORITYQUEUE };
#endif

#else

const os_task_t * const OS_taskPtrs[OS_NTASKACTIVATIONS+1] = { OS_TASKACT };

#endif

const os_task_t * const OS_taskTableBase = OS_TASKBASE;
const os_actitaskfunc_t OS_actiTaskFunc = OS_ACTITASKFUNC;
const os_killtaskfunc_t OS_killTaskFunc = OS_KILLTASKFUNC;

#include <memmap/Os_mm_const_end.h>

/*!
 * OS_nStartModes
 *
 * This constant contains the number of startup modes that have been
 * configured. There must be at least 1 startup mode.
*/
#if OS_NSTARTMODES==0

#error "Configuration error: there must be at least 1 startup mode"

#else

#include <memmap/Os_mm_const8_begin.h>
const os_uint8_t OS_nStartModes = OS_NSTARTMODES;
#include <memmap/Os_mm_const8_end.h>

#endif

/*!
 * OS_startModeTasks
 *
 * This array contains, for each defined start mode (of which there is
 * at least one) an index into the OS_autoStartTasks array.
 *
 * OS_autoStartTasks
 *
 * This array contains the ids of the tasks that should be started for
 * each start mode. Each mode's tasks are terminated by the null task id.
 * The index of the first entry for each mode is stored in the appropriate
 * location in OS_startModeTasks
*/
#ifdef OS_STARTMODETASKS

#define OS_CFG_ACTIVATEAUTOTASKS	&OS_ActivateAutoTasks

#include <memmap/Os_mm_const16_begin.h>
const os_uint16_t OS_startModeTasks[OS_NSTARTMODES] = { OS_STARTMODETASKS };
#include <memmap/Os_mm_const16_end.h>

#include <memmap/Os_mm_const_begin.h>
const os_taskid_t OS_autoStartTasks[] = { OS_AUTOSTARTTASKS };
#include <memmap/Os_mm_const_end.h>

#endif

/*!
 * OS_startModeAlarms
 *
 * This array contains, for each defined start mode (of which there is
 * at least one) an index into the OS_autoStartAlarms array.
 *
 * OS_autoStartAlarms
 *
 * This array contains the ids, times and cycles of the alarms that should
 * be started for each start mode. Each mode's alarms are terminated by the
 * null alarm id.
 * The index of the first entry for each mode is stored in the appropriate
 * location in OS_startModeAlarms
*/
#ifdef OS_STARTMODEALARMS

#define OS_CFG_ACTIVATEAUTOALARMS	&OS_ActivateAutoAlarms

#include <memmap/Os_mm_const16_begin.h>
const os_uint16_t OS_startModeAlarms[OS_NSTARTMODES] = { OS_STARTMODEALARMS };
#include <memmap/Os_mm_const16_end.h>

#include <memmap/Os_mm_const_begin.h>
const os_autoalarm_t OS_autoStartAlarms[] = { OS_AUTOSTARTALARMS };
#include <memmap/Os_mm_const_end.h>

#endif

/*!
 * OS_startModeSchedules
 *
 * This array contains, for each defined start mode (of which there is
 * at least one) an index into the OS_autoStartSchedules array.
 *
 * OS_autoStartSchedules
 *
 * This array contains the ids and offsets of the schedule tables that should
 * be started for each start mode. Each mode's schedule tables are terminated by the
 * null schedule table id.
 * The index of the first entry for each mode is stored in the appropriate
 * location in OS_startModeSchedules
*/
#ifdef OS_STARTMODESCHEDULES

#define OS_CFG_ACTIVATEAUTOSCHEDULES	&OS_ActivateAutoSchedules

#include <memmap/Os_mm_const16_begin.h>
const os_uint16_t OS_startModeSchedules[OS_NSTARTMODES] = { OS_STARTMODESCHEDULES };
#include <memmap/Os_mm_const16_end.h>

#include <memmap/Os_mm_const_begin.h>
const os_autoschedule_t OS_autoStartSchedules[] = { OS_AUTOSTARTSCHEDULES };
#include <memmap/Os_mm_const_end.h>

#endif

/*!
 * OS_iStackBase, OS_iStackLen
 *
 * The base address and length of the interrupt stack
*/
#if (OS_CONFIGMODE & OS_STACKCHECK)
#define OS_CFG_INITKERNSTACK	&OS_InitKernStack
#else
#endif

#include <memmap/Os_mm_const_begin.h>

void * const OS_iStackBase = (void *)OS_ISTACKBASE;
const os_stacklen_t OS_iStackLen = OS_ISTACKLEN;

#include <memmap/Os_mm_const_end.h>

/* NOTE: no memmap for OS_iSpInitial. It's type could be const or not, depending.
*/
#if OS_STACKGROWS == OS_STACKGROWSDOWN
os_ispinitial_t OS_iSpInitial =
	(void *)(&(OS_ISTACKBASE)[OS_ISTACKLEN / sizeof(OS_ISTACKBASE[0])]);
#else
os_ispinitial_t OS_iSpInitial = (void *)(OS_ISTACKBASE);
#endif

/*!
 * OS_nCounters
 *
 * This constant contains the number of counters configured.
 *
 * OS_counter
 *
 * This array contains the static part of the counters.
 *
 * We can omit the IncrementCounter system call if all the counters
 * are hardware counters; ie if NCOUNTERS is <= NHWTIMERS
*/
#if OS_NCOUNTERS==0

#define OS_K_GETCOUNTERVALUE		&OS_KernUnknownSyscall
#define OS_K_GETELAPSEDCOUNTERVALUE	&OS_KernUnknownSyscall

#define OS_CTRBASE				OS_NULL
#define OS_CTRDYNAMICBASE		OS_NULL

#else

#define OS_CFG_INITCOUNTERS		&OS_InitCounters
#define OS_CTRBASE				OS_counter
#define OS_CTRDYNAMICBASE		OS_counterDynamic

#include <memmap/Os_mm_const_begin.h>
const os_counter_t OS_counter[OS_NCOUNTERS] = { OS_COUNTER };
#include <memmap/Os_mm_const_end.h>

#endif

#include <memmap/Os_mm_const8_begin.h>
const os_uint8_t OS_nCounters = OS_NCOUNTERS;
#include <memmap/Os_mm_const8_end.h>


#include <memmap/Os_mm_const_begin.h>
const os_counter_t * const OS_counterTableBase = OS_CTRBASE;
os_counterdynamic_t * const OS_counterDynamicBase = OS_CTRDYNAMICBASE;
#include <memmap/Os_mm_const_end.h>

/*!
 * OS_nAlarms
 *
 * This constant contains the number of OSEK alarms configured.
 *
 * OS_totalAlarms
 *
 * This constant contains the total number of alarms configured. The
 * figure includes alarms that are present for other purposes such as
 * schedule tables etc.
 *
 * OS_alarm
 *
 * This array contains the static part of the alarms.
*/
#define OS_TOTALALARMS	(OS_NALARMS + OS_NSCHEDULES + OS_NINTERNALALARMS)

#if OS_NALARMS==0

/* No real alarms. We can set all related system calls to "Unknown
 * system call".
 * Note that there may be some alarms in the table due to
 * configured ScheduleTables, but these are not started or
 * manipulated using the normal alarm APIs.
*/

#define OS_K_GETALARMBASE		&OS_KernUnknownSyscall
#define OS_K_GETALARM			&OS_KernUnknownSyscall
#define OS_K_SETRELALARM		&OS_KernUnknownSyscall
#define OS_K_SETABSALARM		&OS_KernUnknownSyscall
#define OS_K_CANCELALARM		&OS_KernUnknownSyscall

#endif

#if OS_TOTALALARMS==0
/* No alarms. We can turn off the initialisation function.
*/

#define OS_ALARMBASE			OS_NULL
#define OS_ALARMDYNAMICBASE		OS_NULL
#define OS_KILLALARMFUNC		(os_killalarmfunc_t)&OS_NullFunction

#else

#define OS_CFG_INITALARMS		&OS_InitAlarms
#define OS_ALARMBASE			OS_alarm
#define OS_ALARMDYNAMICBASE		OS_alarmDynamic

#ifdef OS_EXCLUDE_KILLALARM
#define OS_KILLALARMFUNC		(os_killalarmfunc_t)&OS_NullFunction
#else
#define OS_KILLALARMFUNC		&OS_KillAlarm
#endif

#include <memmap/Os_mm_const_begin.h>
const os_alarm_t OS_alarm[OS_TOTALALARMS] = { OS_ALARM };
#include <memmap/Os_mm_const_end.h>

#if OS_NALARMCALLBACKS != 0
#include <memmap/Os_mm_const_begin.h>
const os_alarmcallback_t OS_alarmCallback[OS_NALARMCALLBACKS] = { OS_ALARMCALLBACK };
#include <memmap/Os_mm_const_end.h>
#endif

#endif

#include <memmap/Os_mm_const8_begin.h>
const os_uint8_t OS_nAlarms = OS_NALARMS;
const os_uint8_t OS_totalAlarms = OS_TOTALALARMS;
#include <memmap/Os_mm_const8_end.h>

#include <memmap/Os_mm_const_begin.h>
const os_alarm_t * const OS_alarmTableBase = OS_ALARMBASE;
os_alarmdynamic_t * const OS_alarmDynamicBase = OS_ALARMDYNAMICBASE;
const os_killalarmfunc_t OS_killAlarmFunc = OS_KILLALARMFUNC;
#include <memmap/Os_mm_const_end.h>

/*!
 * OS_nHwTimers
 *
 * This constant contains the number of hardware timers that need
 * initialising.
 *
 * OS_hwTimer
 *
 * This array contains an element for each hardware timer that needs
 * initialising.
 *
 * The format of the array is architecture-dependent, but includes a pointer
 * to a function that takes the address of the os_hwt_t as its
 * only parameter. Timer drivers therefore need to be written to conform
 * to this specification.
*/
const os_uint8_t OS_nHwTimers = OS_NHWTIMERS;

#if OS_NHWTIMERS!=0

#define OS_CFG_INITTIMERS	&OS_InitTimers

#include <memmap/Os_mm_var_begin.h>
static os_timervalue_t OS_hwtLastValue[OS_NHWTIMERS];
#include <memmap/Os_mm_var_end.h>

#include <memmap/Os_mm_const_begin.h>
const os_hwt_t OS_hwTimer[OS_NHWTIMERS] = { OS_HWTIMER };
#include <memmap/Os_mm_const_end.h>

#endif

/*!
 * Event-related system calls
 *
 * This macro is not used to initialise a constant because the number of
 * events is of no interest to the kernel. However, if there are _no_
 * events at all we can omit the event-related system calls.
*/
#if OS_NEVENTS==0

#define OS_K_SETEVENT		&OS_KernUnknownSyscall
#define OS_K_CLEAREVENT		&OS_KernUnknownSyscall
#define OS_K_WAITEVENT		&OS_KernUnknownSyscall

#endif

#if OS_NETASKS==0

#define OS_K_GETEVENT		&OS_KernUnknownSyscall

#endif


/*!
 * OS_nSchedules
 *
 * This constant contains the number of schedule tables configured.
 *
 * OS_schedule
 *
 * This array contains an element for each schedule table configured.
 *
 * OS_stEntries
 *
 * This array contains all the expiry points for all the configured
 * schedule tables.
*/
#if OS_NSCHEDULES==0

#define OS_K_STARTSCHEDULETABLE			&OS_KernUnknownSyscall
#define OS_K_STARTSCHEDULETABLESYNCHRON	&OS_KernUnknownSyscall
#define OS_K_CHAINSCHEDULETABLE			&OS_KernUnknownSyscall
#define OS_K_STOPSCHEDULETABLE			&OS_KernUnknownSyscall
#define OS_K_SYNCSCHEDULETABLE			&OS_KernUnknownSyscall
#define OS_K_SETSCHEDULETABLEASYNC		&OS_KernUnknownSyscall
#define OS_K_GETSCHEDULETABLESTATUS		&OS_KernUnknownSyscall

#define OS_SCHEDULEBASE					OS_NULL
#define OS_SCHEDULEDYNAMICBASE			OS_NULL
#define OS_KILLSCHEDULEFUNC				(os_killschedulefunc_t)&OS_NullFunction
#define OS_SYNCHRONISEFUNC				OS_NULL

#else

#define OS_SYNCHRONISEFUNC				&OS_Synchronise


#define OS_CFG_INITSCHEDULES			&OS_InitSchedules
#define OS_SCHEDULEBASE					OS_schedule
#define OS_SCHEDULEDYNAMICBASE			OS_scheduleDynamic
#define OS_KILLSCHEDULEFUNC				&OS_KillSchedule

#include <memmap/Os_mm_const_begin.h>
static const os_scheduleentry_t OS_stEntries[OS_NSTENTRIES] = { OS_STENTRIES };
const os_schedule_t OS_schedule[OS_NSCHEDULES] = { OS_STCONFIG };
#include <memmap/Os_mm_const_end.h>

#endif

#include <memmap/Os_mm_const8_begin.h>
const os_uint8_t OS_nSchedules = OS_NSCHEDULES;
#include <memmap/Os_mm_const8_end.h>

#include <memmap/Os_mm_const_begin.h>
const os_schedule_t * const OS_scheduleTableBase = OS_SCHEDULEBASE;
os_scheduledynamic_t * const OS_scheduleDynamicBase = OS_SCHEDULEDYNAMICBASE;
const os_killschedulefunc_t OS_killScheduleFunc = OS_KILLSCHEDULEFUNC;
const os_synchronisefunc_t OS_synchroniseFunc = OS_SYNCHRONISEFUNC;
#include <memmap/Os_mm_const_end.h>

/*!
 * OS_nResources
 *
 * This constant contains the number of resources configured.
 *
 * OS_resource
 *
 * This array contains the static part of the configured resources.
*/
#include <memmap/Os_mm_const_begin.h>

#if OS_NRESOURCES==0

#define OS_K_GETRESOURCE		&OS_KernUnknownSyscall
#define OS_K_RELEASERESOURCE	&OS_KernUnknownSyscall
#define OS_RESBASE				OS_NULL
#define OS_RESDYNAMICBASE		OS_NULL

#else

#define OS_CFG_INITRESOURCES	&OS_InitResources
#define OS_RESBASE				OS_resource
#define OS_RESDYNAMICBASE		OS_resourceDynamic

const os_resource_t OS_resource[OS_NRESOURCES] = { OS_RESOURCES };

#endif

const os_resource_t * const OS_resourceTableBase = OS_RESBASE;
os_resourcedynamic_t * const OS_resourceDynamicBase = OS_RESDYNAMICBASE;

#include <memmap/Os_mm_const_end.h>

#include <memmap/Os_mm_const8_begin.h>
const os_uint8_t OS_nResources = OS_NRESOURCES;
#include <memmap/Os_mm_const8_end.h>

/*!
 * OS_nInterrupts, OS_isrTable
 *
 * OS_nInterrupts is the number of interrupts configured - 0 to 255
 *
 * OS_isrTable is the table describing the interrupts to the kernel.
*/
#include <memmap/Os_mm_const_begin.h>

#if OS_NINTERRUPTS==0

#define OS_ISRBASE				OS_NULL
#define OS_ISRDBASE				OS_NULL
#define OS_KILLISRFUNC			OS_NULL

#define OS_K_DISABLEINTERRUPTSOURCE		&OS_KernUnknownSyscall
#define OS_K_ENABLEINTERRUPTSOURCE		&OS_KernUnknownSyscall

#else

#define OS_CFG_INITINTERRUPTS	&OS_InitInterrupts
#define OS_ISRBASE				OS_isrTable
#define OS_ISRDBASE				OS_isrDynamic

#ifdef OS_EXCLUDE_KILLISR
#define OS_KILLISRFUNC			OS_NULL
#else
#define OS_KILLISRFUNC			&OS_KillIsr
#endif

#if ((OS_CONFIGMODE & OS_STACKCHECK) != 0)
#define OS_CFG_INITISRSTACKS	&OS_InitIsrStacks
#endif /* ((OS_CONFIGMODE & OS_STACKCHECK) != 0) */

const os_isr_t OS_isrTable[OS_NINTERRUPTS] = { OS_ISRCONFIG };

#endif


const os_isr_t * const OS_isrTableBase = OS_ISRBASE;

const os_killisrfunc_t OS_killIsrFunc = OS_KILLISRFUNC;

const os_isrid_t OS_nInterrupts = OS_NINTERRUPTS;

#include <memmap/Os_mm_const_end.h>

#include <memmap/Os_mm_const_begin.h>
os_isrdynamic_t * const OS_isrDynamicBase = OS_ISRDBASE;
#include <memmap/Os_mm_const_end.h>

/*!
 * OS_NAPPSNONPRIV
 *
 * This macro defines the number of non-privileged applications in the system.
 * Its only action is to enable protected mode.
 *
 * Protected mode is also not enabled if the field OS_DBGPROT
 * of the macro OS_CONFIGMODE is set to OS_DBGPROT_OFF
 * The Generator sets this field in response to options in the OIL
 * file.
*/
#if (OS_NAPPSNONPRIV != 0)

#if (OS_HASMEMPROT != 0) && ((OS_CONFIGMODE & OS_DBGPROT) != OS_DBGPROT_OFF)
#define OS_CFG_INITPROTECTED	OS_EnterProtectedMode
#endif

#endif

/*!
 * Accounting
 *
 * Various macros define the different types of timing that are in progress:
 *  - basic execution timing for tasks and ISRs
 *  - interrupt lock timing for tasks and ISRs
 *  - resource lock timing
 *
 * Fortunately we can simplify the decisions somewhat:
 *  - if there are any tasks with accounting structures, we need start/stop/preempt for tasks.
 *  - if there are any ISRs with accounting structures we need start/stop/preempt/resume for ISRs.
 *  - if any tasks have interrupt lock timing we need start/stop int lock timing for tasks
 *  - if any ISRs have interrupt lock timing we need start/stop int lock timing for ISRs
 *  - if any tasks or ISRs have resource lock timing we need start/stop res lock timing
 *  - if there's any timing at all we need the init function.
*/
#if ( (OS_NTASKACCOUNTING == 0) && (OS_NISRACCOUNTING == 0) )

#define OS_HASEXECTIMING			0

#else

#define OS_HASEXECTIMING			1
#define OS_CFG_INITEXECTIME			&OS_InitExecutionTiming

#endif

#if ( OS_NTASKACCOUNTING == 0 )

#define OS_CFG_STARTTASKTIMING		(os_starttaskexectimingfp_t)&OS_NullFunction
#define OS_CFG_STOPTASKTIMING		(os_stoptaskexectimingfp_t)&OS_NullFunction
#define OS_CFG_PREEMPTTASKTIMING	(os_preempttaskexectimingfp_t)&OS_NullFunction

#else

#define OS_CFG_STARTTASKTIMING		&OS_StartTaskExecTiming
#define OS_CFG_STOPTASKTIMING		&OS_StopTaskExecTiming
#define OS_CFG_PREEMPTTASKTIMING	&OS_PreemptTaskExecTiming

#endif

#if ( OS_NISRACCOUNTING == 0 )

#define OS_CFG_STARTISRTIMING		(os_startisrexectimingfp_t)&OS_NullFunction
#define OS_CFG_PREEMPTISRTIMING		(os_preemptisrexectimingfp_t)&OS_NullFunction
#define OS_CFG_RESUMEISRTIMING		(os_resumeisrexectimingfp_t)&OS_NullFunction
#define OS_CFG_STOPISRTIMING		(os_stopisrexectimingfp_t)&OS_NullFunction

#else

#define OS_CFG_STARTISRTIMING		&OS_StartIsrExecTiming
#define OS_CFG_PREEMPTISRTIMING		&OS_PreemptIsrExecTiming
#define OS_CFG_RESUMEISRTIMING		&OS_ResumeIsrExecTiming
#define OS_CFG_STOPISRTIMING		&OS_StopIsrExecTiming

#endif

#if ( OS_NTASKSINTLOCKLIMIT == 0 )

#define OS_CFG_STARTTASKILOCKTIMING	(os_starttaskintlocktimingfp_t)&OS_NullFunction
#define OS_CFG_STOPTASKILOCKTIMING	(os_stoptaskintlocktimingfp_t)&OS_NullFunction

#else

#define OS_CFG_STARTTASKILOCKTIMING	&OS_StartTaskIntLockTiming
#define OS_CFG_STOPTASKILOCKTIMING	&OS_StopTaskIntLockTiming

#endif

#if ( OS_NISRSINTLOCKLIMIT == 0 )

#define OS_CFG_STARTISRILOCKTIMING	(os_startisrintlocktimingfp_t)&OS_NullFunction
#define OS_CFG_STOPISRILOCKTIMING	(os_stopisrintlocktimingfp_t)&OS_NullFunction

#else

#define OS_CFG_STARTISRILOCKTIMING	&OS_StartIsrIntLockTiming
#define OS_CFG_STOPISRILOCKTIMING	&OS_StopIsrIntLockTiming

#endif

#if ( (OS_NTASKSRESLOCKLIMIT == 0) && (OS_NISRSRESLOCKLIMIT == 0) )

#define OS_CFG_STARTRESLOCKTIMING	(os_startreslocktimingfp_t)&OS_NullFunction
#define OS_CFG_STOPRESLOCKTIMING	(os_stopreslocktimingfp_t)&OS_NullFunction

#else

#define OS_CFG_STARTRESLOCKTIMING	&OS_StartResLockTiming
#define OS_CFG_STOPRESLOCKTIMING	&OS_StopResLockTiming

#endif

/*!
 * OS_rateMonitor
 * OS_rateMonitorFunc
 *
 * OS_rateMonitor is the array of rate-monitors. If the size of the array
 * is 0 the array does not exist and the rate-monitor and rate monitor
 * initialisation function pointers are set to OS_NULL.
 *
 * OS_rateMonitorFunc is the address of the rate-monitor function, or
 * OS_NULL of no rate-monitoring is configured.
*/
#include <memmap/Os_mm_const_begin.h>

#if OS_NRATEMONS == 0

#define OS_CFG_RATEMONITOR	OS_NULL

#else

#define OS_CFG_RATEMONITOR	&OS_RateMonitor

const os_ratemonitor_t OS_rateMonitor[OS_NRATEMONS] = { OS_RATEMONCONFIG };

#endif

const os_ratemonitorid_t OS_nRateMonitors = OS_NRATEMONS;

#ifdef OS_EXECUTIONTIMERINDEX
const os_hwt_t * const OS_executionTimer = &OS_hwTimer[OS_EXECUTIONTIMERINDEX];
#else
const os_hwt_t * const OS_executionTimer = OS_NULL;
#endif

#if (!OS_USE_OPTIMIZATION_OPTIONS) || (!defined(OS_EXCLUDE_TIMINGPROTECTION))
const os_ratemonitorfunc_t OS_rateMonitorFunc = OS_CFG_RATEMONITOR;

/* Deviation MISRA-1 <START> */
const os_starttaskexectimingfp_t	OS_startTaskExecTimingFp	= OS_CFG_STARTTASKTIMING;
const os_preempttaskexectimingfp_t	OS_preemptTaskExecTimingFp	= OS_CFG_PREEMPTTASKTIMING;
const os_stoptaskexectimingfp_t		OS_stopTaskExecTimingFp		= OS_CFG_STOPTASKTIMING;
const os_starttaskintlocktimingfp_t	OS_startTaskIntLockTimingFp	= OS_CFG_STARTTASKILOCKTIMING;
const os_stoptaskintlocktimingfp_t	OS_stopTaskIntLockTimingFp	= OS_CFG_STOPTASKILOCKTIMING;
const os_startisrexectimingfp_t		OS_startIsrExecTimingFp		= OS_CFG_STARTISRTIMING;
const os_preemptisrexectimingfp_t	OS_preemptIsrExecTimingFp	= OS_CFG_PREEMPTISRTIMING;
const os_resumeisrexectimingfp_t	OS_resumeIsrExecTimingFp	= OS_CFG_RESUMEISRTIMING;
const os_stopisrexectimingfp_t		OS_stopIsrExecTimingFp		= OS_CFG_STOPISRTIMING;
const os_startisrintlocktimingfp_t	OS_startIsrIntLockTimingFp	= OS_CFG_STARTISRILOCKTIMING;
const os_stopisrintlocktimingfp_t	OS_stopIsrIntLockTimingFp	= OS_CFG_STOPISRILOCKTIMING;
const os_startreslocktimingfp_t		OS_startResLockTimingFp		= OS_CFG_STARTRESLOCKTIMING;
const os_stopreslocktimingfp_t		OS_stopResLockTimingFp		= OS_CFG_STOPRESLOCKTIMING;
/* Deviation MISRA-1 <STOP> */
#endif

/*!
 * OS_resourceLockTimes
 *
 * This is the (possibly optimised) table of resource lock times that
 * is referenced from the task and ISR tables.
*/
#if OS_NRESLOCKTIMES != 0
const os_tick_t OS_resourceLockTimes[OS_NRESLOCKTIMES] = { OS_RESLOCKTIMES };
#endif
#include <memmap/Os_mm_const_end.h>

/*!
 * OS_CFG_POSTINITARCH
 *
 * Allows post initialisation of specific hardware
 *
 * This is defined by the architecture ...
*/

/*!
 * OS_CFG_STARTTICKERS
 *
 * Allows a generated function void OS_StartTickers(void) to start all the internal drivers and GPT channels
 * that are configured to 'tick' software counters.
 * OS_StartTickers() must be called after all hardware initialisation is done.
 *
 * CAVEAT:
 * We assume that Gpt_Init() has already been called, which is not particularly pleasant.
 * We also assume that the Gpt API functions can be called safely from within the kernel. Since the
 * MCAL drivers and the OS are not tightly integrated this might not be the case. If there are
 * problems, address your complaints to those responsible for the Autosar Layered Architecture.
 *
 * If there are problems with any of the above points, define the macro OS_INHIBIT_GPT_STARTUP = 1 in
 * board.h, and call the function OS_StartGptTickers() from a task (typically the Init task), after
 * Gpt_Init() has been called.
 *
*/
#ifndef OS_INHIBIT_GPT_STARTUP
#define OS_INHIBIT_GPT_STARTUP	0
#endif

#ifndef OS_CFG_STARTTICKERS

#if (OS_NINTERNALTICKERS != 0)

#define OS_CFG_STARTTICKERS		(&OS_StartTickers)

#elif (!OS_INHIBIT_GPT_STARTUP) && (OS_NGPTTICKERS != 0)

#define OS_CFG_STARTTICKERS		(&OS_StartTickers)

#endif

#endif

/*
 * OS_timeStampTimer
 *
 * Configuration for timestamp (used by CPU load measurement and possibly others)
 *
 * If not configured, we choose OS_NULL. This supports older versions of the generator.
*/
#if OS_USEGENERICTIMESTAMP

#ifdef OS_TIMESTAMPTIMERINDEX

#define OS_TIMESTAMPTIMER				&OS_hwTimer[OS_TIMESTAMPTIMERINDEX]

#else

#define OS_TIMESTAMPTIMER				OS_NULL

#endif

#include <memmap/Os_mm_const_begin.h>
const os_hwt_t * const OS_timeStampTimer = OS_TIMESTAMPTIMER;
#include <memmap/Os_mm_const_end.h>

#endif

#if defined(OS_INITTIMESTAMP)
#define OS_CFG_INITTIMESTAMP		OS_INITTIMESTAMP
#endif

/*
 * OS_cpuLoadCfg
 *
 * Configuration for CPU load measurement
 *
 * We define a default if the generator has not alread defined one.
 * The default defines a dummy configuration that is internally consistent, in case any function
 * gets called (avoiding possible divide-by-zero problems, null-pointer accesses etc.)
 * Naturally, calling the APIs with the feature unconfigured will result in garbage answers.
 * The system-call version of the API is turned off.
*/
#if	(defined(OS_CPULOADCFG_NINTERVALS)) && (defined(OS_CPULOADCFG_INTERVAL)) \
	&& (defined(OS_CPULOADCFG_WINDOW)) && (defined(OS_CPULOADCFG_ROUNDING))

#define OS_CPULOADBUSYBUFFER			OS_cpuLoadBusyBuffer
#define OS_CPULOADCFG_BUSYOVERFLOWLIMIT	(OS_MAXTICK - OS_CPULOADCFG_ROUNDING)/(OS_CPULOADCFG_WINDOW)
#define OS_CPULOADCFG_WINDOW100			((OS_CPULOADCFG_WINDOW + 50)/100)
#define OS_CPULOADCFG_ROUNDING100		((OS_CPULOADCFG_ROUNDING + 50)/100)

#else

#define OS_CPULOADCFG_INTERVAL			1
#define OS_CPULOADCFG_NINTERVALS		1
#define OS_CPULOADCFG_WINDOW			1
#define OS_CPULOADCFG_ROUNDING			0
#define OS_CPULOADCFG_BUSYOVERFLOWLIMIT	OS_MAXTICK
#define OS_CPULOADCFG_WINDOW100			1
#define OS_CPULOADCFG_ROUNDING100		0
#define OS_CPULOADBUSYBUFFER			&OS_cpuLoad.busyTime

#define OS_K_GETCPULOAD					&OS_KernUnknownSyscall
#define OS_K_RESETPEAKCPULOAD			&OS_KernUnknownSyscall

#endif

#if defined(OS_EXCLUDE_CPULOAD)

/* If the load measurement is excluded from the build, there's
 * no need for the configuration structure
*/
#define OS_K_GETCPULOAD					&OS_KernUnknownSyscall
#define OS_K_RESETPEAKCPULOAD			&OS_KernUnknownSyscall
#else

#include <memmap/Os_mm_var_begin.h>
os_tick_t OS_cpuLoadBusyBuffer[OS_CPULOADCFG_NINTERVALS];
#include <memmap/Os_mm_var_end.h>

#include <memmap/Os_mm_const_begin.h>
const os_cpuloadcfg_t OS_cpuLoadCfg =
{
	OS_CPULOADCFG_INTERVAL,
	OS_CPULOADCFG_NINTERVALS,
	OS_CPULOADCFG_WINDOW,
	OS_CPULOADCFG_ROUNDING,
	OS_CPULOADBUSYBUFFER,
	OS_CPULOADCFG_BUSYOVERFLOWLIMIT,
	OS_CPULOADCFG_WINDOW100,
	OS_CPULOADCFG_ROUNDING100
};
#include <memmap/Os_mm_const_end.h>

#endif


#if (OS_IOC_ENABLED==1)
#define OS_K_IOCWRITE	&OS_KernIocWrite
#define OS_K_IOCREAD	&OS_KernIocRead
#define OS_K_IOCSEND	&OS_KernIocSend
#define OS_K_IOCRECEIVE	&OS_KernIocReceive
#define OS_K_EMPTYQUEUE	&OS_KernIocEmptyQueue
#else
#define OS_K_IOCWRITE	&OS_KernUnknownSyscall
#define OS_K_IOCREAD	&OS_KernUnknownSyscall
#define OS_K_IOCSEND	&OS_KernUnknownSyscall
#define OS_K_IOCRECEIVE	&OS_KernUnknownSyscall
#define OS_K_EMPTYQUEUE	&OS_KernUnknownSyscall
#endif

/*!
 * OS_startupCheckFunc
 *
 * This array contains pointers to functions that will
 * be called by OS_StartupChecks().
 *
 * If the startup checks are not enabled the OS_StartupChecks() function is not
 * called, so the array isn't needed.
*/
#if (OS_CONFIGMODE & OS_STARTUPCHECK)
#define OS_CFG_STARTUPCHECKS	OS_StartupChecks

#include <memmap/Os_mm_const_begin.h>
os_schkfunc_t const OS_startupCheckFunc[] =
{
#if OS_NAPPS!=0
	OS_StartupChecksApplication,
#endif

#if OS_NFUNCTIONS!=0
	OS_StartupChecksTrustedFunction,
#endif

#if OS_NTASKS != 0
	OS_StartupChecksTask,
#endif

#if OS_USE_CLZ_QUEUE_ALGORITHM && (OS_NPRIORITIES != 0)
	OS_StartupChecksQueue,
#endif

#if OS_NCOUNTERS != 0
	OS_StartupChecksCounter,
#endif

#if OS_NALARMS != 0
	OS_StartupChecksAlarm,
#endif

#if OS_NHWTIMERS != 0
	OS_StartupChecksTimer,
#endif

#if OS_NSCHEDULES != 0
	OS_StartupChecksSchedule,
#endif

#if OS_NRESOURCES != 0
	OS_StartupChecksResource,
#endif

#if OS_NINTERRUPTS != 0
	OS_StartupChecksIsr,
#endif

#ifdef OS_ARCH_STARTUPCHECKS
	OS_ARCH_STARTUPCHECKS,
#endif

	OS_NULL			/* Terminator: must be at end */
};
#include <memmap/Os_mm_const_end.h>
#endif

/*!
 * OS_initFunc
 *
 * This array contains pointers to initialisation functions that will
 * be called by OS_KernStartOs(). The entries will all be
 * specific initialisation functions.
 * The array is initialised using macros that are defined earlier in
 * this file depending on the generated configuration. Exception:
 * OS_CFG_INITARCH is normally defined by the architecture.
 * Macros that are not defined are not included in the list.
 * The list is null-terminated.
 *
 * This file therefore defines which init functions are called, and in
 * what order.
 *
 * !LINKSTO Kernel.Autosar.ScheduleTable.Autostart.Order, 1
*/
#include <memmap/Os_mm_const_begin.h>
os_initfunc_t const OS_initFunc[] =
{
#ifdef OS_CFG_STARTUPCHECKS
	OS_CFG_STARTUPCHECKS,
#endif

#ifdef OS_CFG_INITARCH
	OS_CFG_INITARCH,
#endif

#ifdef OS_CFG_INITKERNSTACK
	OS_CFG_INITKERNSTACK,
#endif

#ifdef OS_CFG_INITAPPDATA
	OS_CFG_INITAPPDATA,
#endif

#ifdef OS_CFG_INITTASKSTACKS
	OS_CFG_INITTASKSTACKS,
#endif

#ifdef OS_CFG_INITISRSTACKS
	OS_CFG_INITISRSTACKS,
#endif

#ifdef OS_CFG_INITTASKS
	OS_CFG_INITTASKS,
#endif

#ifdef OS_CFG_INITTIMERS
	OS_CFG_INITTIMERS,
#endif

#ifdef OS_CFG_INITCOUNTERS
	OS_CFG_INITCOUNTERS,
#endif

#ifdef OS_CFG_INITALARMS
	OS_CFG_INITALARMS,
#endif

#ifdef OS_CFG_INITSCHEDULES
	OS_CFG_INITSCHEDULES,
#endif

#ifdef OS_CFG_INITRESOURCES
	OS_CFG_INITRESOURCES,
#endif

#ifdef OS_CFG_INITINTERRUPTS
	OS_CFG_INITINTERRUPTS,
#endif

#ifdef OS_CFG_INITTIMESTAMP
	OS_CFG_INITTIMESTAMP,
#endif

#ifdef OS_CFG_ACTIVATEAUTOTASKS
	OS_CFG_ACTIVATEAUTOTASKS,
#endif

#ifdef OS_CFG_ACTIVATEAUTOALARMS
	OS_CFG_ACTIVATEAUTOALARMS,
#endif

#ifdef OS_CFG_ACTIVATEAUTOSCHEDULES
	OS_CFG_ACTIVATEAUTOSCHEDULES,
#endif

#ifdef OS_CFG_INITPROTECTED
	OS_CFG_INITPROTECTED,
#endif

#ifdef OS_CFG_INITEXECTIME
	OS_CFG_INITEXECTIME,
#endif

#ifdef OS_CFG_POSTINITARCH
	OS_CFG_POSTINITARCH,
#endif

#ifdef OS_CFG_STARTTICKERS
	OS_CFG_STARTTICKERS,
#endif

	OS_NULL
};
#include <memmap/Os_mm_const_end.h>

/*!
 * OS_taskDynamic
 *
 * The dynamic status of tasks, one entry per task. Note: this is not the
 * context of the task, though it may contain it.
*/

#if OS_NTASKS!=0
#include <memmap/Os_mm_var_begin.h>
os_taskdynamic_t OS_taskDynamic[OS_NTASKS];
#include <memmap/Os_mm_var_end.h>
#endif

/*!
 * OS_taskAccounting
 *
 * The accounting information for tasks, one entry per task that needs it.
*/

#if OS_NTASKACCOUNTING!=0
#include <memmap/Os_mm_var_begin.h>
os_taskaccounting_t OS_taskAccounting[OS_NTASKACCOUNTING];
#include <memmap/Os_mm_var_end.h>
#endif

/*!
 * OS_taskActivations
 *
 * The links in the linked list of the task queue. Each slot contains the
 * index of the next slot in the queue. The corresponding (same index)
 * entry in OS_taskPtrs[] contains the address of the task that owns
 * the slot. There is always at least 1 entry in this array. The first
 * entry (index 0) is not used, because 0 is a "null" value for the
 * linked-list functions.
*/
#include <memmap/Os_mm_var_begin.h>

#if OS_USE_CLZ_QUEUE_ALGORITHM

os_clzword_t OS_slavePrioWord[OS_NSLAVEWORDS];

#if OS_NPRIORITYQUEUEDYNAMICS > 0
os_priorityqueuedynamic_t OS_priorityQueueDynamic[OS_NPRIORITYQUEUEDYNAMICS];	/* = 0 */
#endif

#if OS_NPRIORITYSLOTS > 0
os_priorityqueueentry_t OS_prioritySlot[OS_NPRIORITYSLOTS];						/* = 0 */
#else
os_priorityqueueentry_t OS_prioritySlot[1];
#endif

#else

os_tasklink_t OS_taskActivations[OS_NTASKACTIVATIONS+1];

#endif

/*!
 * OS_isrDynamic
 *
 * The dynamic status of ISRs, one entry per ISR.
*/
#if OS_NINTERRUPTS!=0
os_isrdynamic_t OS_isrDynamic[OS_NINTERRUPTS];
#endif

/*!
 * OS_isrAccounting
 *
 * The accounting information for ISRs, one entry per ISR that needs it.
*/

#if OS_NISRACCOUNTING!=0
os_israccounting_t OS_isrAccounting[OS_NISRACCOUNTING];
#endif

/*!
 * OS_counterDynamic
 *
 * The dynamic state of all counters, one entry per counter.
 * The 'head' member must be set to OS_NULLALARM during StartOS().
*/
#if OS_NCOUNTERS!=0
os_counterdynamic_t OS_counterDynamic[OS_NCOUNTERS];
#endif

/*!
 * OS_alarmDynamic
 *
 * The dynamic state of all alarms, one entry per alarm.
*/
#if OS_TOTALALARMS!=0
os_alarmdynamic_t OS_alarmDynamic[OS_TOTALALARMS];
#endif

/*!
 * OS_scheduleDynamic
 *
 * The dynamic state of all schedule tablss, one entry per schedule table.
*/
#if OS_NSCHEDULES!=0
os_scheduledynamic_t OS_scheduleDynamic[OS_NSCHEDULES];
#endif

/*!
 * OS_resourceDynamic
 *
 * The dynamic state of all resources, one entry per resource
*/
#if OS_NRESOURCES==0
#define OS_RESDYN	OS_NULL
#else
os_resourcedynamic_t OS_resourceDynamic[OS_NRESOURCES];
#define OS_RESDYN	OS_resourceDynamic
#endif

/*!
 * OS_resourceDynamicPtr
 *
 * A constant pointer to the OS_resourceDynamic array, or OS_NULL of there
 * are no resources. This pointer is used in OS_KillTask instead of
 * using the array directly, because the array doesn't always exist.
*/
os_resourcedynamic_t * const OS_resourceDynamicPtr = OS_RESDYN;

/*!
 * OS_rateIndex
 * OS_rateTimer
 *
 * The dynamic state of all rate-monitors. The index array (one per
 * rate-monitor) is the index into the rate-monitor's own ring buffer.
 * The timer array is the aggregated ring buffer array, COUNTLIMIT
 * entries per rate-monitor.
*/
#if OS_NRATEMONS != 0
os_rateindex_t OS_rateIndex[OS_NRATEMONS];
os_timestamp_t OS_rateTimer[OS_NRATETIMERS];
#endif

#include <memmap/Os_mm_var_end.h>

/*!
 * OS_execBudgetExceeded
 *
 * This flag is set by the execution-budget-exceeded alarm wrapper. It
 * must be tested in the generated ISR for the counter, and if set
 * OS_ExceedExecTime() must be called. The alarm wrapper cannot call
 * OS_ExceedExecTime() directly because that function might not return,
 * which would leave the alarm in an indeterminate state.
*/
#if OS_HASEXECTIMING
#include <memmap/Os_mm_var8_begin.h>
os_uint8_t OS_execBudgetExceeded;	/* = 0 */
#include <memmap/Os_mm_var8_end.h>
#endif


/*!
 * OS_ptrXXXHook
 *
 * These pointers point to the hook functions if configured.
 * Otherwise, they can be OS_NULL or OS_NullFunction.
 *
 * The pointers are only present when the generic kernel library is in use. The
 * optimised libraries call the functions directly. This means that the optimised
 * library does not support the hook-renaming that the generic libraries (coupled with
 * this configuration file) support. However, the renaming is not conformamt with the
 * OSEK/AutosarOS standards, and is not supported by the generator.
 *
 * If the given macros are not defined, the pointers map to:
 *   void OS_NullFunction(void)								(StartupHook,PreTaskHook,PostTaskHook)
 *   void OS_NullErrorHook(os_result_t)						(ShutdownHook,ErrorHook)
 *   void OS_NullIsrHook(os_isrid_t)						(PreIsrHook,PostIsrHook)
 *   os_erroraction_t OS_NullProtectionHook(os_result_t)	(ProtectionHook)
 *
 * !LINKSTO Kernel.Autosar.Protection.ProtectionHook.NoHook, 2
*/
#if !OS_USE_OPTIMIZATION_OPTIONS

#ifndef OS_STARTUPHOOK
#define OS_STARTUPHOOK	&OS_NullFunction
#endif

#ifndef OS_SHUTDOWNHOOK
#define OS_SHUTDOWNHOOK	&OS_NullErrorHook
#endif

#ifndef OS_ERRORHOOK
#define OS_ERRORHOOK	&OS_NullErrorHook
#endif

#ifndef OS_PRETASKHOOK
#define OS_PRETASKHOOK	&OS_NullFunction
#endif

#ifndef OS_POSTTASKHOOK
#define OS_POSTTASKHOOK	&OS_NullFunction
#endif

#ifndef OS_PREISRHOOK
#define OS_PREISRHOOK	&OS_NullIsrHook
#endif

#ifndef OS_POSTISRHOOK
#define OS_POSTISRHOOK	&OS_NullIsrHook
#endif

#ifndef OS_PROTECTIONHOOK
#define OS_PROTECTIONHOOK	&OS_NullProtectionHook
#endif

#include <memmap/Os_mm_const_begin.h>
const os_startuphook_t		OS_ptrStartupHook =		OS_STARTUPHOOK;
const os_pretaskhook_t		OS_ptrPreTaskHook =		OS_PRETASKHOOK;
const os_posttaskhook_t		OS_ptrPostTaskHook =	OS_POSTTASKHOOK;
const os_preisrhook_t		OS_ptrPreIsrHook =		OS_PREISRHOOK;
const os_postisrhook_t		OS_ptrPostIsrHook =		OS_POSTISRHOOK;
const os_errorhook_t		OS_ptrErrorHook =		OS_ERRORHOOK;
const os_shutdownhook_t		OS_ptrShutdownHook =	OS_SHUTDOWNHOOK;
const os_protectionhook_t	OS_ptrProtectionHook =	OS_PROTECTIONHOOK;
#include <memmap/Os_mm_const_end.h>

#endif

/*!
 * Stack Checking
 *
 * If stack checking is not enabled, we can omit the stack info
 * system call.
 *
 * The initialisation of task and kernel stacks also depends on
 * the OS_STACKCHECK setting in OS_CONFIGMODE.
*/
#if ((OS_CONFIGMODE & OS_STACKCHECK) == 0)
#define OS_K_GETSTACKINFO		&OS_KernUnknownSyscall
#endif

/*!
 * IncrementCounter
 *
 * Do we need the IncrementCounter/IncrementCounter system call?
 *
 * Without execution timing, if the number of counters is the same as the number of timers
 * there are no software counters, so we can omit the system call.
 * If execution timing is configured there's one hardware timer for execution timing, so we only omit
 * the system call if the number of counters is strictly less than the number of timers.
*/
#if OS_HASEXECTIMING

#if OS_NCOUNTERS<OS_NHWTIMERS
#define OS_K_INCREMENTCOUNTER		&OS_KernUnknownSyscall
#endif

#else

#if OS_NCOUNTERS<=OS_NHWTIMERS
#define OS_K_INCREMENTCOUNTER		&OS_KernUnknownSyscall
#endif

#endif

/*!
 * OS_syscallTable
 *
 * This is the table of kernel functions used by the system-call dispatcher
 * to route OS system calls to the correct function.
 * The entries in here must be in exactly the right order as given by
 * the indices in Os_syscalls.h
 *
 * Entries 0 (UnknownSyscall) and 1 (StartOs) are always configured.
 *
 * We start by defining macros for all known system calls, but only if they
 * are not already defined. The configuration sections above can define
 * the macros as OS_KernUnknownSyscall if the feature is not
 * required.
 *
 * The whole system-call table is omitted for architectures that do not use it.
*/
#if (OS_KERNEL_TYPE==OS_SYSTEM_CALL)

#ifndef OS_K_ACTIVATETASK
#define OS_K_ACTIVATETASK				&OS_KernActivateTask
#endif

#ifndef OS_K_TERMINATETASK
#define OS_K_TERMINATETASK				&OS_KernTerminateTask
#endif

#ifndef OS_K_CHAINTASK
#define OS_K_CHAINTASK					&OS_KernChainTask
#endif

#ifndef OS_K_SCHEDULE
#define OS_K_SCHEDULE					&OS_KernSchedule
#endif

#ifndef OS_K_GETTASKID
#define OS_K_GETTASKID					&OS_KernGetTaskId
#endif

#ifndef OS_K_GETTASKSTATE
#define OS_K_GETTASKSTATE				&OS_KernGetTaskState
#endif

#ifndef OS_K_SUSPENDINTERRUPTS
#define OS_K_SUSPENDINTERRUPTS			&OS_KernSuspendInterrupts
#endif

#ifndef OS_K_RESUMEINTERRUPTS
#define OS_K_RESUMEINTERRUPTS			&OS_KernResumeInterrupts
#endif

#ifndef OS_K_GETRESOURCE
#define OS_K_GETRESOURCE				&OS_KernGetResource
#endif

#ifndef OS_K_RELEASERESOURCE
#define OS_K_RELEASERESOURCE			&OS_KernReleaseResource
#endif

#ifndef OS_K_SETEVENT
#define OS_K_SETEVENT					&OS_KernSetEvent
#endif

#ifndef OS_K_CLEAREVENT
#define OS_K_CLEAREVENT					&OS_KernClearEvent
#endif

#ifndef OS_K_GETEVENT
#define OS_K_GETEVENT					&OS_KernGetEvent
#endif

#ifndef OS_K_WAITEVENT
#define OS_K_WAITEVENT					&OS_KernWaitEvent
#endif

#ifndef OS_K_GETALARMBASE
#define OS_K_GETALARMBASE				&OS_KernGetAlarmBase
#endif

#ifndef OS_K_GETALARM
#define OS_K_GETALARM					&OS_KernGetAlarm
#endif

#ifndef OS_K_SETRELALARM
#define OS_K_SETRELALARM				&OS_KernSetRelAlarm
#endif

#ifndef OS_K_SETABSALARM
#define OS_K_SETABSALARM				&OS_KernSetAbsAlarm
#endif

#ifndef OS_K_CANCELALARM
#define OS_K_CANCELALARM				&OS_KernCancelAlarm
#endif

#ifndef OS_K_GETACTIVEAPPLICATIONMODE
#define OS_K_GETACTIVEAPPLICATIONMODE	&OS_KernGetActiveApplicationMode
#endif

#ifndef OS_K_SHUTDOWNOS
#define OS_K_SHUTDOWNOS					&OS_KernShutdownOs
#endif

#ifndef OS_K_INCREMENTCOUNTER
#define OS_K_INCREMENTCOUNTER			&OS_KernIncrementCounter
#endif

#ifndef OS_K_GETCOUNTERVALUE
#define OS_K_GETCOUNTERVALUE			&OS_KernGetCounterValue
#endif

#ifndef OS_K_GETELAPSEDCOUNTERVALUE
#define OS_K_GETELAPSEDCOUNTERVALUE		&OS_KernGetElapsedCounterValue
#endif

#ifndef OS_K_GETSTACKINFO
#define OS_K_GETSTACKINFO				&OS_KernGetStackInfo
#endif

#ifndef OS_K_STARTSCHEDULETABLE
#define OS_K_STARTSCHEDULETABLE			&OS_KernStartScheduleTable
#endif

#ifndef OS_K_STARTSCHEDULETABLESYNCHRON
#define OS_K_STARTSCHEDULETABLESYNCHRON	&OS_KernStartScheduleTableSynchron
#endif

#ifndef OS_K_CHAINSCHEDULETABLE
#define OS_K_CHAINSCHEDULETABLE			&OS_KernChainScheduleTable
#endif

#ifndef OS_K_STOPSCHEDULETABLE
#define OS_K_STOPSCHEDULETABLE			&OS_KernStopScheduleTable
#endif

#ifndef OS_K_SYNCSCHEDULETABLE
#define OS_K_SYNCSCHEDULETABLE			&OS_KernSyncScheduleTable
#endif

#ifndef OS_K_SETSCHEDULETABLEASYNC
#define OS_K_SETSCHEDULETABLEASYNC		&OS_KernSetScheduleTableAsync
#endif

#ifndef OS_K_GETSCHEDULETABLESTATUS
#define OS_K_GETSCHEDULETABLESTATUS		&OS_KernGetScheduleTableStatus
#endif

#ifndef OS_K_TERMINATEAPPLICATION
#define OS_K_TERMINATEAPPLICATION		&OS_KernTerminateApplication
#endif

#ifndef OS_K_GETAPPLICATIONID
#define OS_K_GETAPPLICATIONID			&OS_KernGetApplicationId
#endif

#ifndef OS_K_GETISRID
#define OS_K_GETISRID					&OS_KernGetIsrId
#endif

#ifndef OS_K_CALLTRUSTEDFUNCTION
#define OS_K_CALLTRUSTEDFUNCTION		&OS_KernCallTrustedFunction
#endif

#ifndef OS_K_CHECKISRMEMORYACCESS
#define OS_K_CHECKISRMEMORYACCESS		&OS_KernCheckIsrMemoryAccess
#endif

#ifndef OS_K_CHECKTASKMEMORYACCESS
#define OS_K_CHECKTASKMEMORYACCESS		&OS_KernCheckTaskMemoryAccess
#endif

#ifndef OS_K_CHECKOBJECTACCESS
#define OS_K_CHECKOBJECTACCESS			&OS_KernCheckObjectAccess
#endif

#ifndef OS_K_CHECKOBJECTOWNERSHIP
#define OS_K_CHECKOBJECTOWNERSHIP		&OS_KernCheckObjectOwnership
#endif

#ifndef OS_K_DISABLEINTERRUPTSOURCE
#define OS_K_DISABLEINTERRUPTSOURCE		&OS_KernDisableInterruptSource
#endif

#ifndef OS_K_ENABLEINTERRUPTSOURCE
#define OS_K_ENABLEINTERRUPTSOURCE		&OS_KernEnableInterruptSource
#endif

#ifndef OS_K_GETCPULOAD
#define OS_K_GETCPULOAD					&OS_KernGetCpuLoad
#endif

#ifndef OS_K_RESETPEAKCPULOAD
#define OS_K_RESETPEAKCPULOAD			&OS_KernResetPeakCpuLoad
#endif

#ifndef OS_K_TASKRETURN
#define OS_K_TASKRETURN					&OS_KernUnknownSyscall
#endif

#ifndef OS_K_RETURNFROMCALL
#define OS_K_RETURNFROMCALL				&OS_KernUnknownSyscall
#endif

#ifndef OS_K_SIMTIMERADVANCE
#define OS_K_SIMTIMERADVANCE			&OS_KernUnknownSyscall
#endif


#include <memmap/Os_mm_const_begin.h>
const os_syscallptr_t OS_syscallTable[OS_N_SYSCALL] =
{
/* Deviation MISRA-2 <START> */
	(os_syscallptr_t) &OS_KernUnknownSyscall,			/*  0 */
	(os_syscallptr_t) &OS_KernStartOs,					/*  1 */
	(os_syscallptr_t) OS_K_SUSPENDINTERRUPTS,			/*  2 */
	(os_syscallptr_t) OS_K_RESUMEINTERRUPTS,			/*  3 */
	(os_syscallptr_t) OS_K_CHAINTASK,					/*  4 */
	(os_syscallptr_t) OS_K_SCHEDULE,					/*  5 */
	(os_syscallptr_t) OS_K_GETTASKID,					/*  6 */
	(os_syscallptr_t) OS_K_GETTASKSTATE,				/*  7 */
	(os_syscallptr_t) OS_K_ACTIVATETASK,				/*  8 */
	(os_syscallptr_t) OS_K_TERMINATETASK,				/*  9 */
	(os_syscallptr_t) OS_K_GETRESOURCE,					/* 10 */
	(os_syscallptr_t) OS_K_RELEASERESOURCE,				/* 11 */
	(os_syscallptr_t) OS_K_SETEVENT,					/* 12 */
	(os_syscallptr_t) OS_K_CLEAREVENT,					/* 13 */
	(os_syscallptr_t) OS_K_GETEVENT,					/* 14 */
	(os_syscallptr_t) OS_K_WAITEVENT,					/* 15 */
	(os_syscallptr_t) OS_K_GETALARMBASE,				/* 16 */
	(os_syscallptr_t) OS_K_GETALARM,					/* 17 */
	(os_syscallptr_t) OS_K_SETRELALARM,					/* 18 */
	(os_syscallptr_t) OS_K_SETABSALARM,					/* 19 */
	(os_syscallptr_t) OS_K_CANCELALARM,					/* 20 */
	(os_syscallptr_t) OS_K_GETACTIVEAPPLICATIONMODE,	/* 21 */
	(os_syscallptr_t) OS_K_SHUTDOWNOS,					/* 22 */
	(os_syscallptr_t) OS_K_INCREMENTCOUNTER,			/* 23 */
	(os_syscallptr_t) OS_K_GETSTACKINFO,				/* 24 */
	(os_syscallptr_t) OS_K_STARTSCHEDULETABLE,			/* 25 */
	(os_syscallptr_t) OS_K_CHAINSCHEDULETABLE,			/* 26 */
	(os_syscallptr_t) OS_K_STOPSCHEDULETABLE,			/* 27 */
	(os_syscallptr_t) OS_K_SYNCSCHEDULETABLE,			/* 28 */
	(os_syscallptr_t) OS_K_SETSCHEDULETABLEASYNC,		/* 29 */
	(os_syscallptr_t) OS_K_GETSCHEDULETABLESTATUS,		/* 30 */
	(os_syscallptr_t) OS_K_TERMINATEAPPLICATION,		/* 31 */
	(os_syscallptr_t) OS_K_GETAPPLICATIONID,			/* 32 */
	(os_syscallptr_t) OS_K_GETISRID,					/* 33 */
	(os_syscallptr_t) OS_K_CALLTRUSTEDFUNCTION,			/* 34 */
	(os_syscallptr_t) OS_K_CHECKISRMEMORYACCESS,		/* 35 */
	(os_syscallptr_t) OS_K_CHECKTASKMEMORYACCESS,		/* 36 */
	(os_syscallptr_t) OS_K_CHECKOBJECTACCESS,			/* 37 */
	(os_syscallptr_t) OS_K_CHECKOBJECTOWNERSHIP,		/* 38 */
	(os_syscallptr_t) OS_K_DISABLEINTERRUPTSOURCE,		/* 39 */
	(os_syscallptr_t) OS_K_ENABLEINTERRUPTSOURCE,		/* 40 */
	(os_syscallptr_t) OS_K_GETCOUNTERVALUE,				/* 41 */
	(os_syscallptr_t) OS_K_GETELAPSEDCOUNTERVALUE,		/* 42 */
	(os_syscallptr_t) OS_K_STARTSCHEDULETABLESYNCHRON,	/* 43 */
	(os_syscallptr_t) OS_K_GETCPULOAD,					/* 44 */
	(os_syscallptr_t) OS_K_RESETPEAKCPULOAD,			/* 45 */
	(os_syscallptr_t) OS_K_TASKRETURN,					/* 46 */
	(os_syscallptr_t) OS_K_RETURNFROMCALL,				/* 47 */
	(os_syscallptr_t) OS_K_SIMTIMERADVANCE,				/* 48 */
	(os_syscallptr_t) OS_K_IOCWRITE,					/* 49 */
	(os_syscallptr_t) OS_K_IOCREAD,						/* 50 */
	(os_syscallptr_t) OS_K_IOCSEND,						/* 51 */
	(os_syscallptr_t) OS_K_IOCRECEIVE,					/* 52 */
	(os_syscallptr_t) OS_K_EMPTYQUEUE,					/* 53 */
	(os_syscallptr_t) &OS_KernUnknownSyscall,			/* 54 */
	(os_syscallptr_t) &OS_KernUnknownSyscall,			/* 55 */
	(os_syscallptr_t) &OS_KernUnknownSyscall,			/* 56 */
	(os_syscallptr_t) &OS_KernUnknownSyscall,			/* 57 */
	(os_syscallptr_t) &OS_KernUnknownSyscall,			/* 58 */
	(os_syscallptr_t) &OS_KernUnknownSyscall,			/* 59 */
	(os_syscallptr_t) &OS_KernUnknownSyscall,			/* 60 */
	(os_syscallptr_t) &OS_KernUnknownSyscall,			/* 61 */
	(os_syscallptr_t) &OS_KernUnknownSyscall,			/* 62 */
	(os_syscallptr_t) &OS_KernUnknownSyscall			/* 63 */
/* Deviation MISRA-2 <STOP> */
};
#include <memmap/Os_mm_const_end.h>

#endif

/*!
 * OS_permittedContext
 *
 * This constant pointer points to the base of a constant array of
 * context-flag words, one per system service.
 *
 * For conformant Autosar operation, the OS_permittedContextAutosar table should be used.
 * The generator can relax the conditions by supplying a different table.
*/
#ifndef	OS_PERMITTEDCONTEXT
#define OS_PERMITTEDCONTEXT		OS_permittedContextAutosar
#endif

#include <memmap/Os_mm_const_begin.h>
const os_callermask_t * const OS_permittedContext = OS_PERMITTEDCONTEXT;
#include <memmap/Os_mm_const_end.h>

/* Editor settings; DO NOT DELETE
 * vi:set ts=4:
*/
