<?xml version='1.0'?>
<AUTOSAR xmlns="http://autosar.org/schema/r4.0"
         xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
         xsi:schemaLocation="http://autosar.org/schema/r4.0 AUTOSAR_4-0-3.xsd">
  <AR-PACKAGES>
    <AR-PACKAGE>
      <SHORT-NAME>TS_TxDxM6I3R0</SHORT-NAME>
      <ELEMENTS>
        <ECUC-DEFINITION-COLLECTION>
          <SHORT-NAME>myEcuParameterDefinition</SHORT-NAME>
          <MODULE-REFS>
            <MODULE-REF DEST="ECUC-MODULE-DEF">/TS_TxDxM6I3R0/Det</MODULE-REF>
          </MODULE-REFS>
        </ECUC-DEFINITION-COLLECTION>
        <ECUC-MODULE-DEF UUID="250a5f5e-3271-4519-a975-fb12590b95ec">
          <SHORT-NAME>Det</SHORT-NAME>
          <DESC>
            <L-2 L="FOR-ALL">&lt;html&gt;
                    &lt;p&gt;Configuration of the Det (Development Error
                    Tracer) module.&lt;/p&gt;
                    &lt;/html&gt;</L-2>
          </DESC>
          <ADMIN-DATA>
            <LANGUAGE>EN</LANGUAGE>
            <DOC-REVISIONS>
              <DOC-REVISION>
                <REVISION-LABEL>4.2.0</REVISION-LABEL>
                <ISSUED-BY>AUTOSAR</ISSUED-BY>
                <DATE>2011-11-09T11:36:22Z</DATE>
              </DOC-REVISION>
              <DOC-REVISION>
                <REVISION-LABEL>6.3.0</REVISION-LABEL>
                <ISSUED-BY>Elektrobit Automotive GmbH</ISSUED-BY>
                <DATE>2013-02-28T23:59:59Z</DATE>
              </DOC-REVISION>
            </DOC-REVISIONS>
          </ADMIN-DATA>
          <REFINED-MODULE-DEF-REF DEST="ECUC-MODULE-DEF">/AUTOSAR/Det</REFINED-MODULE-DEF-REF>
          <SUPPORTED-CONFIG-VARIANTS>
            <SUPPORTED-CONFIG-VARIANT>VARIANT-PRE-COMPILE</SUPPORTED-CONFIG-VARIANT>
          </SUPPORTED-CONFIG-VARIANTS>
          <CONTAINERS>
            <ECUC-PARAM-CONF-CONTAINER-DEF UUID="ECUC:a00be3e0-8783-9123-2d52-1eb616737ca6">
              <SHORT-NAME>CommonPublishedInformation</SHORT-NAME>
              <DESC>
                <L-2 L="EN">
                    &lt;html&gt;
                      Common container, aggregated by all modules. It contains published information about vendor and versions.
                  &lt;/html&gt;</L-2>
              </DESC>
              <PARAMETERS>
                <ECUC-INTEGER-PARAM-DEF UUID="ECUC:948f3f2e-f129-4bc5-b4e1-7a7bdb8599e1">
                  <SHORT-NAME>ArMajorVersion</SHORT-NAME>
                  <DESC>
                    <L-2 L="EN">
                      &lt;html&gt;
                        Major version number of AUTOSAR specification on which the appropriate implementation is based on.
                    &lt;/html&gt;</L-2>
                  </DESC>
                  <IMPLEMENTATION-CONFIG-CLASSES>
                    <ECUC-IMPLEMENTATION-CONFIGURATION-CLASS>
                      <CONFIG-CLASS>PUBLISHED-INFORMATION</CONFIG-CLASS>
                      <CONFIG-VARIANT>VARIANT-PRE-COMPILE</CONFIG-VARIANT>
                    </ECUC-IMPLEMENTATION-CONFIGURATION-CLASS>
                  </IMPLEMENTATION-CONFIG-CLASSES>
                  <ORIGIN>Elektrobit Automotive GmbH</ORIGIN>
                  <SYMBOLIC-NAME-VALUE>false</SYMBOLIC-NAME-VALUE>
                  <DEFAULT-VALUE>3</DEFAULT-VALUE>
                </ECUC-INTEGER-PARAM-DEF>
                <ECUC-INTEGER-PARAM-DEF UUID="ECUC:2893b920-59d5-4ac2-b2c1-e23742e66d70">
                  <SHORT-NAME>ArMinorVersion</SHORT-NAME>
                  <DESC>
                    <L-2 L="EN">
                      &lt;html&gt;
                        Minor version number of AUTOSAR specification on which the appropriate implementation is based on.
                    &lt;/html&gt;</L-2>
                  </DESC>
                  <IMPLEMENTATION-CONFIG-CLASSES>
                    <ECUC-IMPLEMENTATION-CONFIGURATION-CLASS>
                      <CONFIG-CLASS>PUBLISHED-INFORMATION</CONFIG-CLASS>
                      <CONFIG-VARIANT>VARIANT-PRE-COMPILE</CONFIG-VARIANT>
                    </ECUC-IMPLEMENTATION-CONFIGURATION-CLASS>
                  </IMPLEMENTATION-CONFIG-CLASSES>
                  <ORIGIN>Elektrobit Automotive GmbH</ORIGIN>
                  <SYMBOLIC-NAME-VALUE>false</SYMBOLIC-NAME-VALUE>
                  <DEFAULT-VALUE>2</DEFAULT-VALUE>
                </ECUC-INTEGER-PARAM-DEF>
                <ECUC-INTEGER-PARAM-DEF UUID="ECUC:6428eb9b-8790-488a-b9a3-0fba52d0f59e">
                  <SHORT-NAME>ArPatchVersion</SHORT-NAME>
                  <DESC>
                    <L-2 L="EN">
                      &lt;html&gt;
                        Patch level version number of AUTOSAR specification on which the appropriate implementation is based on.
                    &lt;/html&gt;</L-2>
                  </DESC>
                  <IMPLEMENTATION-CONFIG-CLASSES>
                    <ECUC-IMPLEMENTATION-CONFIGURATION-CLASS>
                      <CONFIG-CLASS>PUBLISHED-INFORMATION</CONFIG-CLASS>
                      <CONFIG-VARIANT>VARIANT-PRE-COMPILE</CONFIG-VARIANT>
                    </ECUC-IMPLEMENTATION-CONFIGURATION-CLASS>
                  </IMPLEMENTATION-CONFIG-CLASSES>
                  <ORIGIN>Elektrobit Automotive GmbH</ORIGIN>
                  <SYMBOLIC-NAME-VALUE>false</SYMBOLIC-NAME-VALUE>
                  <DEFAULT-VALUE>0</DEFAULT-VALUE>
                </ECUC-INTEGER-PARAM-DEF>
                <ECUC-INTEGER-PARAM-DEF UUID="ECUC:605b18ae-3f9a-41d4-9225-67c9c5f6fc34">
                  <SHORT-NAME>SwMajorVersion</SHORT-NAME>
                  <DESC>
                    <L-2 L="EN">
                      &lt;html&gt;
                        Major version number of the vendor specific implementation of the module.
                    &lt;/html&gt;</L-2>
                  </DESC>
                  <IMPLEMENTATION-CONFIG-CLASSES>
                    <ECUC-IMPLEMENTATION-CONFIGURATION-CLASS>
                      <CONFIG-CLASS>PUBLISHED-INFORMATION</CONFIG-CLASS>
                      <CONFIG-VARIANT>VARIANT-PRE-COMPILE</CONFIG-VARIANT>
                    </ECUC-IMPLEMENTATION-CONFIGURATION-CLASS>
                  </IMPLEMENTATION-CONFIG-CLASSES>
                  <ORIGIN>Elektrobit Automotive GmbH</ORIGIN>
                  <SYMBOLIC-NAME-VALUE>false</SYMBOLIC-NAME-VALUE>
                  <DEFAULT-VALUE>6</DEFAULT-VALUE>
                </ECUC-INTEGER-PARAM-DEF>
                <ECUC-INTEGER-PARAM-DEF UUID="a7fe44dc-ea25-4b8d-8fad-9fbcae86d56f">
                  <SHORT-NAME>SwMinorVersion</SHORT-NAME>
                  <DESC>
                    <L-2 L="EN">
                      &lt;html&gt;
                        Minor version number of the vendor specific implementation of the module. The numbering is vendor specific.
                    &lt;/html&gt;</L-2>
                  </DESC>
                  <IMPLEMENTATION-CONFIG-CLASSES>
                    <ECUC-IMPLEMENTATION-CONFIGURATION-CLASS>
                      <CONFIG-CLASS>PUBLISHED-INFORMATION</CONFIG-CLASS>
                      <CONFIG-VARIANT>VARIANT-PRE-COMPILE</CONFIG-VARIANT>
                    </ECUC-IMPLEMENTATION-CONFIGURATION-CLASS>
                  </IMPLEMENTATION-CONFIG-CLASSES>
                  <ORIGIN>Elektrobit Automotive GmbH</ORIGIN>
                  <SYMBOLIC-NAME-VALUE>false</SYMBOLIC-NAME-VALUE>
                  <DEFAULT-VALUE>3</DEFAULT-VALUE>
                </ECUC-INTEGER-PARAM-DEF>
                <ECUC-INTEGER-PARAM-DEF UUID="a318d2d9-0e75-49da-ac43-e7e4e682e2f9">
                  <SHORT-NAME>SwPatchVersion</SHORT-NAME>
                  <DESC>
                    <L-2 L="EN">
                      &lt;html&gt;
                        Patch level version number of the vendor specific implementation of the module. The numbering is vendor specific.
                    &lt;/html&gt;</L-2>
                  </DESC>
                  <IMPLEMENTATION-CONFIG-CLASSES>
                    <ECUC-IMPLEMENTATION-CONFIGURATION-CLASS>
                      <CONFIG-CLASS>PUBLISHED-INFORMATION</CONFIG-CLASS>
                      <CONFIG-VARIANT>VARIANT-PRE-COMPILE</CONFIG-VARIANT>
                    </ECUC-IMPLEMENTATION-CONFIGURATION-CLASS>
                  </IMPLEMENTATION-CONFIG-CLASSES>
                  <ORIGIN>Elektrobit Automotive GmbH</ORIGIN>
                  <SYMBOLIC-NAME-VALUE>false</SYMBOLIC-NAME-VALUE>
                  <DEFAULT-VALUE>0</DEFAULT-VALUE>
                </ECUC-INTEGER-PARAM-DEF>
                <ECUC-INTEGER-PARAM-DEF UUID="ECUC:78bc8362-080f-4253-b3da-804ab69a7154">
                  <SHORT-NAME>ModuleId</SHORT-NAME>
                  <DESC>
                    <L-2 L="EN">
                      &lt;html&gt;
                        Module ID of this module from Module List
                    &lt;/html&gt;</L-2>
                  </DESC>
                  <IMPLEMENTATION-CONFIG-CLASSES>
                    <ECUC-IMPLEMENTATION-CONFIGURATION-CLASS>
                      <CONFIG-CLASS>PUBLISHED-INFORMATION</CONFIG-CLASS>
                      <CONFIG-VARIANT>VARIANT-PRE-COMPILE</CONFIG-VARIANT>
                    </ECUC-IMPLEMENTATION-CONFIGURATION-CLASS>
                  </IMPLEMENTATION-CONFIG-CLASSES>
                  <ORIGIN>Elektrobit Automotive GmbH</ORIGIN>
                  <SYMBOLIC-NAME-VALUE>false</SYMBOLIC-NAME-VALUE>
                  <DEFAULT-VALUE>15</DEFAULT-VALUE>
                </ECUC-INTEGER-PARAM-DEF>
                <ECUC-INTEGER-PARAM-DEF UUID="ECUC:01b0f467-6943-4558-b4f2-3fa1fad28449">
                  <SHORT-NAME>VendorId</SHORT-NAME>
                  <DESC>
                    <L-2 L="EN">
                      &lt;html&gt;
                        Vendor ID of the dedicated implementation of this module according to the AUTOSAR vendor list
                    &lt;/html&gt;</L-2>
                  </DESC>
                  <IMPLEMENTATION-CONFIG-CLASSES>
                    <ECUC-IMPLEMENTATION-CONFIGURATION-CLASS>
                      <CONFIG-CLASS>PUBLISHED-INFORMATION</CONFIG-CLASS>
                      <CONFIG-VARIANT>VARIANT-PRE-COMPILE</CONFIG-VARIANT>
                    </ECUC-IMPLEMENTATION-CONFIGURATION-CLASS>
                  </IMPLEMENTATION-CONFIG-CLASSES>
                  <ORIGIN>Elektrobit Automotive GmbH</ORIGIN>
                  <SYMBOLIC-NAME-VALUE>false</SYMBOLIC-NAME-VALUE>
                  <DEFAULT-VALUE>1</DEFAULT-VALUE>
                </ECUC-INTEGER-PARAM-DEF>
                <ECUC-STRING-PARAM-DEF UUID="ECUC:1c68a547-f24e-4a4e-9540-69fbd466ec89">
                  <SHORT-NAME>VendorApiInfix</SHORT-NAME>
                  <LOWER-MULTIPLICITY>0</LOWER-MULTIPLICITY>
                  <UPPER-MULTIPLICITY>1</UPPER-MULTIPLICITY>
                  <IMPLEMENTATION-CONFIG-CLASSES>
                    <ECUC-IMPLEMENTATION-CONFIGURATION-CLASS>
                      <CONFIG-CLASS>PUBLISHED-INFORMATION</CONFIG-CLASS>
                      <CONFIG-VARIANT>VARIANT-PRE-COMPILE</CONFIG-VARIANT>
                    </ECUC-IMPLEMENTATION-CONFIGURATION-CLASS>
                  </IMPLEMENTATION-CONFIG-CLASSES>
                  <ORIGIN>Elektrobit Automotive GmbH</ORIGIN>
                  <SYMBOLIC-NAME-VALUE>false</SYMBOLIC-NAME-VALUE>
                  <ECUC-STRING-PARAM-DEF-VARIANTS>
                    <ECUC-STRING-PARAM-DEF-CONDITIONAL>
                      <DEFAULT-VALUE></DEFAULT-VALUE>
                    </ECUC-STRING-PARAM-DEF-CONDITIONAL>
                  </ECUC-STRING-PARAM-DEF-VARIANTS>
                </ECUC-STRING-PARAM-DEF>
                <ECUC-STRING-PARAM-DEF UUID="ECUC:1c68a547-f24e-4a4e-9540-69fbd533ec89">
                  <SHORT-NAME>Release</SHORT-NAME>
                  <IMPLEMENTATION-CONFIG-CLASSES>
                    <ECUC-IMPLEMENTATION-CONFIGURATION-CLASS>
                      <CONFIG-CLASS>PUBLISHED-INFORMATION</CONFIG-CLASS>
                      <CONFIG-VARIANT>VARIANT-PRE-COMPILE</CONFIG-VARIANT>
                    </ECUC-IMPLEMENTATION-CONFIGURATION-CLASS>
                  </IMPLEMENTATION-CONFIG-CLASSES>
                  <ORIGIN>Elektrobit Automotive GmbH</ORIGIN>
                  <SYMBOLIC-NAME-VALUE>false</SYMBOLIC-NAME-VALUE>
                  <ECUC-STRING-PARAM-DEF-VARIANTS>
                    <ECUC-STRING-PARAM-DEF-CONDITIONAL>
                      <DEFAULT-VALUE></DEFAULT-VALUE>
                    </ECUC-STRING-PARAM-DEF-CONDITIONAL>
                  </ECUC-STRING-PARAM-DEF-VARIANTS>
                </ECUC-STRING-PARAM-DEF>
              </PARAMETERS>
            </ECUC-PARAM-CONF-CONTAINER-DEF>
            <ECUC-PARAM-CONF-CONTAINER-DEF UUID="425da8a7-dffe-47a3-9ded-9195aa2f5084">
              <SHORT-NAME>DetGeneral</SHORT-NAME>
              <DESC>
                <L-2 L="FOR-ALL">&lt;html&gt;
                        General parameter definitions for the Det
                      &lt;/html&gt;</L-2>
              </DESC>
              <PARAMETERS>
                <ECUC-BOOLEAN-PARAM-DEF UUID="9790491f-c408-4499-b33e-826515314f5e">
                  <SHORT-NAME>DetForwardToDlt</SHORT-NAME>
                  <DESC>
                    <L-2 L="FOR-ALL">&lt;html&gt;
                        &lt;p&gt;&lt;em&gt;The functionality related to this parameter
                        is not supported by the current implementation.&lt;/em&gt;&lt;/p&gt;

                        &lt;p&gt;Only if the parameter is present and set to
                        true, the Det requires the Dlt interface and
                        forwards it&apos;s call to the function
                        &lt;code&gt;Dlt_DetForwardErrorTrace()&lt;/code&gt;. In this case the
                        optional interface to &lt;code&gt;Dlt_Det&lt;/code&gt; is required.&lt;/p&gt;
                      &lt;/html&gt;</L-2>
                  </DESC>
                  <LOWER-MULTIPLICITY>0</LOWER-MULTIPLICITY>
                  <UPPER-MULTIPLICITY>1</UPPER-MULTIPLICITY>
                  <IMPLEMENTATION-CONFIG-CLASSES>
                    <ECUC-IMPLEMENTATION-CONFIGURATION-CLASS>
                      <CONFIG-CLASS>PRE-COMPILE</CONFIG-CLASS>
                      <CONFIG-VARIANT>VARIANT-PRE-COMPILE</CONFIG-VARIANT>
                    </ECUC-IMPLEMENTATION-CONFIGURATION-CLASS>
                  </IMPLEMENTATION-CONFIG-CLASSES>
                  <ORIGIN>AUTOSAR_ECUC</ORIGIN>
                  <SYMBOLIC-NAME-VALUE>false</SYMBOLIC-NAME-VALUE>
                  <DEFAULT-VALUE>true</DEFAULT-VALUE>
                </ECUC-BOOLEAN-PARAM-DEF>
                <ECUC-BOOLEAN-PARAM-DEF UUID="bb8a08be-fe49-450f-93f3-c954d93414c6">
                  <SHORT-NAME>DetVersionInfoApi</SHORT-NAME>
                  <DESC>
                    <L-2 L="FOR-ALL">&lt;html&gt;
                          &lt;p&gt;Switch to enable/disable the API function
                          &lt;code&gt;Det_GetVersionInfo()&lt;/code&gt; to read
                          out the module&apos;s version information.&lt;/p&gt;
                          &lt;ul&gt;
                            &lt;li&gt;&lt;code&gt;true&lt;/code&gt;: Version info API enabled.&lt;/li&gt;
                            &lt;li&gt;&lt;code&gt;false&lt;/code&gt;: Version info APIdisabled.&lt;/li&gt;
                          &lt;/ul&gt;
                        &lt;/html&gt;</L-2>
                  </DESC>
                  <IMPLEMENTATION-CONFIG-CLASSES>
                    <ECUC-IMPLEMENTATION-CONFIGURATION-CLASS>
                      <CONFIG-CLASS>PRE-COMPILE</CONFIG-CLASS>
                      <CONFIG-VARIANT>VARIANT-PRE-COMPILE</CONFIG-VARIANT>
                    </ECUC-IMPLEMENTATION-CONFIGURATION-CLASS>
                  </IMPLEMENTATION-CONFIG-CLASSES>
                  <ORIGIN>AUTOSAR_ECUC</ORIGIN>
                  <SYMBOLIC-NAME-VALUE>false</SYMBOLIC-NAME-VALUE>
                  <DEFAULT-VALUE>false</DEFAULT-VALUE>
                </ECUC-BOOLEAN-PARAM-DEF>
                <ECUC-ENUMERATION-PARAM-DEF>
                  <SHORT-NAME>LoggingMode</SHORT-NAME>
                  <DESC>
                    <L-2 L="FOR-ALL">&lt;html&gt;
                        &lt;p&gt;Logging mode of the Det:&lt;/p&gt;
                        &lt;ul&gt;
                        &lt;li&gt;&lt;code&gt;Internal&lt;/code&gt;: error reports are
                        stored in an internal buffer&lt;/li&gt;
                        &lt;li&gt;&lt;code&gt;Breakpoint&lt;/code&gt;: the debugger
                        halts the system&lt;/li&gt;
                        &lt;/ul&gt;
                        &lt;/html&gt;</L-2>
                  </DESC>
                  <IMPLEMENTATION-CONFIG-CLASSES>
                    <ECUC-IMPLEMENTATION-CONFIGURATION-CLASS>
                      <CONFIG-CLASS>PRE-COMPILE</CONFIG-CLASS>
                      <CONFIG-VARIANT>VARIANT-PRE-COMPILE</CONFIG-VARIANT>
                    </ECUC-IMPLEMENTATION-CONFIGURATION-CLASS>
                  </IMPLEMENTATION-CONFIG-CLASSES>
                  <ORIGIN>Elektrobit Automotive GmbH</ORIGIN>
                  <DEFAULT-VALUE>Internal</DEFAULT-VALUE>
                  <LITERALS>
                    <ECUC-ENUMERATION-LITERAL-DEF>
                      <SHORT-NAME>Internal</SHORT-NAME>
                      <ORIGIN>Elektrobit Automotive GmbH</ORIGIN>
                    </ECUC-ENUMERATION-LITERAL-DEF>
                    <ECUC-ENUMERATION-LITERAL-DEF>
                      <SHORT-NAME>Breakpoint</SHORT-NAME>
                      <ORIGIN>Elektrobit Automotive GmbH</ORIGIN>
                    </ECUC-ENUMERATION-LITERAL-DEF>
                  </LITERALS>
                </ECUC-ENUMERATION-PARAM-DEF>
                <ECUC-INTEGER-PARAM-DEF>
                  <SHORT-NAME>BufferSize</SHORT-NAME>
                  <DESC>
                    <L-2 L="FOR-ALL">&lt;html&gt;
                          Number of recordable entries used if error
                          reports are buffered depending of the
                          logging mode
                        &lt;/html&gt;</L-2>
                  </DESC>
                  <IMPLEMENTATION-CONFIG-CLASSES>
                    <ECUC-IMPLEMENTATION-CONFIGURATION-CLASS>
                      <CONFIG-CLASS>PRE-COMPILE</CONFIG-CLASS>
                      <CONFIG-VARIANT>VARIANT-PRE-COMPILE</CONFIG-VARIANT>
                    </ECUC-IMPLEMENTATION-CONFIGURATION-CLASS>
                  </IMPLEMENTATION-CONFIG-CLASSES>
                  <ORIGIN>Elektrobit Automotive GmbH</ORIGIN>
                  <DEFAULT-VALUE>10</DEFAULT-VALUE>
                  <MAX>32767</MAX>
                  <MIN>1</MIN>
                </ECUC-INTEGER-PARAM-DEF>
                <ECUC-BOOLEAN-PARAM-DEF>
                  <SHORT-NAME>KeepFirst</SHORT-NAME>
                  <DESC>
                    <L-2 L="FOR-ALL">&lt;html&gt;
                          &lt;p&gt;Reports to keep if buffer overflows:
                          either the first (value is
                          &lt;code&gt;true&lt;/code&gt;) or last entries (value is
                          &lt;code&gt;false&lt;/code&gt;)&lt;/p&gt;
                        &lt;/html&gt;</L-2>
                  </DESC>
                  <IMPLEMENTATION-CONFIG-CLASSES>
                    <ECUC-IMPLEMENTATION-CONFIGURATION-CLASS>
                      <CONFIG-CLASS>PRE-COMPILE</CONFIG-CLASS>
                      <CONFIG-VARIANT>VARIANT-PRE-COMPILE</CONFIG-VARIANT>
                    </ECUC-IMPLEMENTATION-CONFIGURATION-CLASS>
                  </IMPLEMENTATION-CONFIG-CLASSES>
                  <ORIGIN>Elektrobit Automotive GmbH</ORIGIN>
                  <DEFAULT-VALUE>false</DEFAULT-VALUE>
                </ECUC-BOOLEAN-PARAM-DEF>
                <ECUC-BOOLEAN-PARAM-DEF>
                  <SHORT-NAME>DetRteUsage</SHORT-NAME>
                  <DESC>
                    <L-2 L="FOR-ALL">&lt;html&gt;
                      &lt;p&gt;This parameter enables the usage of the RTE
                      for this module.&lt;/p&gt;

                      &lt;p&gt;For an easy integration it is recommended to
                      disable the usage of the RTE at the beginning of
                      the integration work.&lt;/p&gt;
                      &lt;/html&gt;</L-2>
                  </DESC>
                  <IMPLEMENTATION-CONFIG-CLASSES>
                    <ECUC-IMPLEMENTATION-CONFIGURATION-CLASS>
                      <CONFIG-CLASS>PRE-COMPILE</CONFIG-CLASS>
                      <CONFIG-VARIANT>VARIANT-PRE-COMPILE</CONFIG-VARIANT>
                    </ECUC-IMPLEMENTATION-CONFIGURATION-CLASS>
                  </IMPLEMENTATION-CONFIG-CLASSES>
                  <ORIGIN>Elektrobit Automotive GmbH</ORIGIN>
                  <SYMBOLIC-NAME-VALUE>false</SYMBOLIC-NAME-VALUE>
                  <DEFAULT-VALUE>false</DEFAULT-VALUE>
                </ECUC-BOOLEAN-PARAM-DEF>
              </PARAMETERS>
              <SUB-CONTAINERS>
                <ECUC-PARAM-CONF-CONTAINER-DEF UUID="ECUC:ea7efc20-8263-11e2-9e96-0800200c9a66">
                  <SHORT-NAME>DetServiceAPI</SHORT-NAME>
                  <DESC>
                    <L-2 L="FOR-ALL">&lt;html&gt;
                        &lt;p&gt;
                          Container for configuration of the service API of Det.
                        &lt;/p&gt;
                        &lt;p&gt;
                          Check &quot;Enable Rte Usage&quot; in order to enable this configuration item.
                        &lt;/p&gt;
                      &lt;/html&gt;</L-2>
                  </DESC>
                  <PARAMETERS>
                    <ECUC-BOOLEAN-PARAM-DEF UUID="ECUC:ea7efc21-8263-11e2-9e96-0800200c9a66">
                      <SHORT-NAME>DetEnableASR32ServiceAPI</SHORT-NAME>
                      <DESC>
                        <L-2 L="FOR-ALL">&lt;html&gt;
                          &lt;p&gt;
                            Configures whether the AUTOSAR 3.2 service API shall be provided.
                          &lt;/p&gt;
                          &lt;ul&gt;
                            &lt;li&gt; &lt;code&gt;TRUE&lt;/code&gt; = Enables AUTOSAR 3.2 service API.&lt;/li&gt;
                            &lt;li&gt; &lt;code&gt;FALSE&lt;/code&gt; = Disables AUTOSAR 3.2 service API.&lt;/li&gt;
                          &lt;/ul&gt; 
                        &lt;/html&gt;</L-2>
                      </DESC>
                      <IMPLEMENTATION-CONFIG-CLASSES>
                        <ECUC-IMPLEMENTATION-CONFIGURATION-CLASS>
                          <CONFIG-CLASS>PRE-COMPILE</CONFIG-CLASS>
                          <CONFIG-VARIANT>VARIANT-PRE-COMPILE</CONFIG-VARIANT>
                        </ECUC-IMPLEMENTATION-CONFIGURATION-CLASS>
                      </IMPLEMENTATION-CONFIG-CLASSES>
                      <ORIGIN>Elektrobit Automotive GmbH</ORIGIN>
                      <SYMBOLIC-NAME-VALUE>false</SYMBOLIC-NAME-VALUE>
                      <DEFAULT-VALUE>false</DEFAULT-VALUE>
                    </ECUC-BOOLEAN-PARAM-DEF>
                    <ECUC-BOOLEAN-PARAM-DEF UUID="ECUC:ea7efc22-8263-11e2-9e96-0800200c9a66">
                      <SHORT-NAME>DetEnableASR40ServiceAPI</SHORT-NAME>
                      <DESC>
                        <L-2 L="FOR-ALL">&lt;html&gt;
                        &lt;p&gt;
                          Configures whether the AUTOSAR 4.0 service API shall be provided.&lt;/p&gt;
                          &lt;ul&gt;
                            &lt;li&gt;&lt;code&gt;TRUE&lt;/code&gt; = Enables AUTOSAR 4.0 service API.&lt;/li&gt;
                            &lt;li&gt;&lt;code&gt;FALSE&lt;/code&gt; = Disables AUTOSAR 4.0 service API.&lt;/li&gt;
                          &lt;/ul&gt;
                        &lt;/html&gt;</L-2>
                      </DESC>
                      <IMPLEMENTATION-CONFIG-CLASSES>
                        <ECUC-IMPLEMENTATION-CONFIGURATION-CLASS>
                          <CONFIG-CLASS>PRE-COMPILE</CONFIG-CLASS>
                          <CONFIG-VARIANT>VARIANT-PRE-COMPILE</CONFIG-VARIANT>
                        </ECUC-IMPLEMENTATION-CONFIGURATION-CLASS>
                      </IMPLEMENTATION-CONFIG-CLASSES>
                      <ORIGIN>Elektrobit Automotive GmbH</ORIGIN>
                      <SYMBOLIC-NAME-VALUE>false</SYMBOLIC-NAME-VALUE>
                      <DEFAULT-VALUE>false</DEFAULT-VALUE>
                    </ECUC-BOOLEAN-PARAM-DEF>
                    <ECUC-ENUMERATION-PARAM-DEF UUID="ECUC:ea7efc23-8263-11e2-9e96-0800200c9a66">
                      <SHORT-NAME>DetDefaultASRServiceAPI</SHORT-NAME>
                      <DESC>
                        <L-2 L="FOR-ALL">&lt;html&gt;
                          &lt;p&gt;Defines the default AUTOSAR service API.&lt;/p&gt;
                            &lt;ul&gt;
                              &lt;li&gt;&lt;code&gt;AUTOSAR_32&lt;/code&gt; = AUTOSAR 3.2 service API is the default one.&lt;/li&gt;
                              &lt;li&gt;&lt;code&gt;AUTOSAR_40&lt;/code&gt; = AUTOSAR 4.0 service API is the default one.&lt;/li&gt;
                              &lt;li&gt;&lt;code&gt;NONE&lt;/code&gt; = No default AUTOSAR service API is provided.&lt;/li&gt;
                            &lt;/ul&gt;
                          &lt;/html&gt;</L-2>
                      </DESC>
                      <IMPLEMENTATION-CONFIG-CLASSES>
                        <ECUC-IMPLEMENTATION-CONFIGURATION-CLASS>
                          <CONFIG-CLASS>PRE-COMPILE</CONFIG-CLASS>
                          <CONFIG-VARIANT>VARIANT-PRE-COMPILE</CONFIG-VARIANT>
                        </ECUC-IMPLEMENTATION-CONFIGURATION-CLASS>
                      </IMPLEMENTATION-CONFIG-CLASSES>
                      <ORIGIN>Elektrobit Automotive GmbH</ORIGIN>
                      <SYMBOLIC-NAME-VALUE>false</SYMBOLIC-NAME-VALUE>
                      <DEFAULT-VALUE>AUTOSAR_40</DEFAULT-VALUE>
                      <LITERALS>
                        <ECUC-ENUMERATION-LITERAL-DEF>
                          <SHORT-NAME>AUTOSAR_32</SHORT-NAME>
                          <ORIGIN>Elektrobit Automotive GmbH</ORIGIN>
                        </ECUC-ENUMERATION-LITERAL-DEF>
                        <ECUC-ENUMERATION-LITERAL-DEF>
                          <SHORT-NAME>AUTOSAR_40</SHORT-NAME>
                          <ORIGIN>Elektrobit Automotive GmbH</ORIGIN>
                        </ECUC-ENUMERATION-LITERAL-DEF>
                        <ECUC-ENUMERATION-LITERAL-DEF>
                          <SHORT-NAME>NONE</SHORT-NAME>
                          <ORIGIN>Elektrobit Automotive GmbH</ORIGIN>
                        </ECUC-ENUMERATION-LITERAL-DEF>
                      </LITERALS>
                    </ECUC-ENUMERATION-PARAM-DEF>
                  </PARAMETERS>
                </ECUC-PARAM-CONF-CONTAINER-DEF>
              </SUB-CONTAINERS>
            </ECUC-PARAM-CONF-CONTAINER-DEF>
            <ECUC-PARAM-CONF-CONTAINER-DEF UUID="1460095e-b7c6-4590-8963-bf811a7d92e3">
              <SHORT-NAME>DetNotification</SHORT-NAME>
              <DESC>
                <L-2 L="FOR-ALL">&lt;html&gt;
                      &lt;p&gt;Configuration of the notification functions.&lt;/p&gt;
                    &lt;/html&gt;</L-2>
              </DESC>
              <LOWER-MULTIPLICITY>0</LOWER-MULTIPLICITY>
              <UPPER-MULTIPLICITY>1</UPPER-MULTIPLICITY>
              <PARAMETERS>
                <ECUC-FUNCTION-NAME-DEF UUID="bf4f3122-96ea-4429-9e22-578eb3615fbd">
                  <SHORT-NAME>DetErrorHook</SHORT-NAME>
                  <DESC>
                    <L-2 L="FOR-ALL">&lt;html&gt;
                          &lt;p&gt;Optional list of functions to be called
                          by the Development Error Tracer in context
                          of each call of Det_ReportError.&lt;/p&gt;

                          &lt;p&gt;The type of these functions must be
                          identical to the type of
                          &lt;code&gt;Det_ReportError()&lt;/code&gt; itself:
                          &lt;code&gt;Std_ReturnType (*f)(uint16, uint8,
                          uint8, uint8)&lt;/code&gt;.&lt;/p&gt;
                        &lt;/html&gt;</L-2>
                  </DESC>
                  <LOWER-MULTIPLICITY>0</LOWER-MULTIPLICITY>
                  <UPPER-MULTIPLICITY-INFINITE>1</UPPER-MULTIPLICITY-INFINITE>
                  <IMPLEMENTATION-CONFIG-CLASSES>
                    <ECUC-IMPLEMENTATION-CONFIGURATION-CLASS>
                      <CONFIG-CLASS>PRE-COMPILE</CONFIG-CLASS>
                      <CONFIG-VARIANT>VARIANT-PRE-COMPILE</CONFIG-VARIANT>
                    </ECUC-IMPLEMENTATION-CONFIGURATION-CLASS>
                  </IMPLEMENTATION-CONFIG-CLASSES>
                  <ORIGIN>AUTOSAR_ECUC</ORIGIN>
                  <SYMBOLIC-NAME-VALUE>false</SYMBOLIC-NAME-VALUE>
                  <ECUC-FUNCTION-NAME-DEF-VARIANTS>
                    <ECUC-FUNCTION-NAME-DEF-CONDITIONAL>
                      <DEFAULT-VALUE>XXX_DetCallback</DEFAULT-VALUE>
                    </ECUC-FUNCTION-NAME-DEF-CONDITIONAL>
                  </ECUC-FUNCTION-NAME-DEF-VARIANTS>
                </ECUC-FUNCTION-NAME-DEF>
                <ECUC-STRING-PARAM-DEF>
                  <SHORT-NAME>DetHeaderFile</SHORT-NAME>
                  <DESC>
                    <L-2 L="FOR-ALL">&lt;html&gt;
                          &lt;p&gt;Optional list of header files which are
                          included.  These header files must contain
                          the function declarations of the
                          notification functions given in
                          DetErrorHook.&lt;/p&gt;
                        &lt;/html&gt;</L-2>
                  </DESC>
                  <LOWER-MULTIPLICITY>0</LOWER-MULTIPLICITY>
                  <UPPER-MULTIPLICITY-INFINITE>1</UPPER-MULTIPLICITY-INFINITE>
                  <IMPLEMENTATION-CONFIG-CLASSES>
                    <ECUC-IMPLEMENTATION-CONFIGURATION-CLASS>
                      <CONFIG-CLASS>PRE-COMPILE</CONFIG-CLASS>
                      <CONFIG-VARIANT>VARIANT-PRE-COMPILE</CONFIG-VARIANT>
                    </ECUC-IMPLEMENTATION-CONFIGURATION-CLASS>
                  </IMPLEMENTATION-CONFIG-CLASSES>
                  <ORIGIN>Elektrobit Automotive GmbH</ORIGIN>
                  <ECUC-STRING-PARAM-DEF-VARIANTS>
                    <ECUC-STRING-PARAM-DEF-CONDITIONAL>
                      <DEFAULT-VALUE>XXX_cbk.h</DEFAULT-VALUE>
                    </ECUC-STRING-PARAM-DEF-CONDITIONAL>
                  </ECUC-STRING-PARAM-DEF-VARIANTS>
                </ECUC-STRING-PARAM-DEF>
              </PARAMETERS>
            </ECUC-PARAM-CONF-CONTAINER-DEF>
            <ECUC-PARAM-CONF-CONTAINER-DEF>
              <SHORT-NAME>DetDefensiveProgramming</SHORT-NAME>
              <DESC>
                <L-2 L="FOR-ALL">&lt;html&gt;
                      &lt;p&gt;Parameters for defensive programming&lt;/p&gt;
                    &lt;/html&gt;</L-2>
              </DESC>
              <PARAMETERS>
                <ECUC-BOOLEAN-PARAM-DEF>
                  <SHORT-NAME>DetDefensiveProgrammingEnabled</SHORT-NAME>
                  <DESC>
                    <L-2 L="FOR-ALL">&lt;html&gt;
                        &lt;p&gt;Enables or disables the feature &apos;defensive
                        programming&apos; for all software modules&lt;/p&gt;
                      &lt;/html&gt;</L-2>
                  </DESC>
                  <IMPLEMENTATION-CONFIG-CLASSES>
                    <ECUC-IMPLEMENTATION-CONFIGURATION-CLASS>
                      <CONFIG-CLASS>PRE-COMPILE</CONFIG-CLASS>
                      <CONFIG-VARIANT>VARIANT-PRE-COMPILE</CONFIG-VARIANT>
                    </ECUC-IMPLEMENTATION-CONFIGURATION-CLASS>
                  </IMPLEMENTATION-CONFIG-CLASSES>
                  <ORIGIN>Elektrobit Automotive GmbH</ORIGIN>
                  <DEFAULT-VALUE>false</DEFAULT-VALUE>
                </ECUC-BOOLEAN-PARAM-DEF>
                <ECUC-BOOLEAN-PARAM-DEF>
                  <SHORT-NAME>DetPreconditionAssertEnabled</SHORT-NAME>
                  <DESC>
                    <L-2 L="FOR-ALL">&lt;html&gt;
                        Enables handling of precondition assertion checks reported from
                        functions in other modules.
                      &lt;/html&gt;</L-2>
                  </DESC>
                  <IMPLEMENTATION-CONFIG-CLASSES>
                    <ECUC-IMPLEMENTATION-CONFIGURATION-CLASS>
                      <CONFIG-CLASS>PRE-COMPILE</CONFIG-CLASS>
                      <CONFIG-VARIANT>VARIANT-PRE-COMPILE</CONFIG-VARIANT>
                    </ECUC-IMPLEMENTATION-CONFIGURATION-CLASS>
                  </IMPLEMENTATION-CONFIG-CLASSES>
                  <ORIGIN>Elektrobit Automotive GmbH</ORIGIN>
                  <DEFAULT-VALUE>true</DEFAULT-VALUE>
                </ECUC-BOOLEAN-PARAM-DEF>
                <ECUC-BOOLEAN-PARAM-DEF>
                  <SHORT-NAME>DetPostconditionAssertEnabled</SHORT-NAME>
                  <DESC>
                    <L-2 L="FOR-ALL">&lt;html&gt;
                       Enables handling of postcondition assertion checks reported from
                       functions in other modules.
                      &lt;/html&gt;</L-2>
                  </DESC>
                  <IMPLEMENTATION-CONFIG-CLASSES>
                    <ECUC-IMPLEMENTATION-CONFIGURATION-CLASS>
                      <CONFIG-CLASS>PRE-COMPILE</CONFIG-CLASS>
                      <CONFIG-VARIANT>VARIANT-PRE-COMPILE</CONFIG-VARIANT>
                    </ECUC-IMPLEMENTATION-CONFIGURATION-CLASS>
                  </IMPLEMENTATION-CONFIG-CLASSES>
                  <ORIGIN>Elektrobit Automotive GmbH</ORIGIN>
                  <DEFAULT-VALUE>true</DEFAULT-VALUE>
                </ECUC-BOOLEAN-PARAM-DEF>
                <ECUC-BOOLEAN-PARAM-DEF>
                  <SHORT-NAME>DetStaticAssertEnabled</SHORT-NAME>
                  <DESC>
                    <L-2 L="FOR-ALL">&lt;html&gt;
                        Enables handling of static assertion checks reported from
                        functions in other modules.
                      &lt;/html&gt;</L-2>
                  </DESC>
                  <IMPLEMENTATION-CONFIG-CLASSES>
                    <ECUC-IMPLEMENTATION-CONFIGURATION-CLASS>
                      <CONFIG-CLASS>PRE-COMPILE</CONFIG-CLASS>
                      <CONFIG-VARIANT>VARIANT-PRE-COMPILE</CONFIG-VARIANT>
                    </ECUC-IMPLEMENTATION-CONFIGURATION-CLASS>
                  </IMPLEMENTATION-CONFIG-CLASSES>
                  <ORIGIN>Elektrobit Automotive GmbH</ORIGIN>
                  <DEFAULT-VALUE>true</DEFAULT-VALUE>
                </ECUC-BOOLEAN-PARAM-DEF>
                <ECUC-BOOLEAN-PARAM-DEF>
                  <SHORT-NAME>DetUnreachableCodeAssertEnabled</SHORT-NAME>
                  <DESC>
                    <L-2 L="FOR-ALL">&lt;html&gt;
                        Enables handling of unreachable code assertion checks
                        reported from functions in other modules.
                      &lt;/html&gt;</L-2>
                  </DESC>
                  <IMPLEMENTATION-CONFIG-CLASSES>
                    <ECUC-IMPLEMENTATION-CONFIGURATION-CLASS>
                      <CONFIG-CLASS>PRE-COMPILE</CONFIG-CLASS>
                      <CONFIG-VARIANT>VARIANT-PRE-COMPILE</CONFIG-VARIANT>
                    </ECUC-IMPLEMENTATION-CONFIGURATION-CLASS>
                  </IMPLEMENTATION-CONFIG-CLASSES>
                  <ORIGIN>Elektrobit Automotive GmbH</ORIGIN>
                  <DEFAULT-VALUE>true</DEFAULT-VALUE>
                </ECUC-BOOLEAN-PARAM-DEF>
                <ECUC-BOOLEAN-PARAM-DEF>
                  <SHORT-NAME>DetInvariantAssertEnabled</SHORT-NAME>
                  <DESC>
                    <L-2 L="FOR-ALL">&lt;html&gt;
                        Enables handling of invariant assertion checks
                        reported from functions in other modules.
                      &lt;/html&gt;</L-2>
                  </DESC>
                  <IMPLEMENTATION-CONFIG-CLASSES>
                    <ECUC-IMPLEMENTATION-CONFIGURATION-CLASS>
                      <CONFIG-CLASS>PRE-COMPILE</CONFIG-CLASS>
                      <CONFIG-VARIANT>VARIANT-PRE-COMPILE</CONFIG-VARIANT>
                    </ECUC-IMPLEMENTATION-CONFIGURATION-CLASS>
                  </IMPLEMENTATION-CONFIG-CLASSES>
                  <ORIGIN>Elektrobit Automotive GmbH</ORIGIN>
                  <DEFAULT-VALUE>true</DEFAULT-VALUE>
                </ECUC-BOOLEAN-PARAM-DEF>
              </PARAMETERS>
            </ECUC-PARAM-CONF-CONTAINER-DEF>
            <ECUC-PARAM-CONF-CONTAINER-DEF>
              <SHORT-NAME>SoftwareComponentList</SHORT-NAME>
              <DESC>
                <L-2 L="FOR-ALL">&lt;html&gt;
                          &lt;p&gt;Definition of all Software Components
                          that use the Det service port
                          &lt;code&gt;ReportError&lt;/code&gt;.  Its multiplicity
                          describes the number of applications
                          accessing the Det.&lt;/p&gt;

                          &lt;p&gt;The name of list entries are taken to
                          construct the provide port names.  These
                          provide ports must than be connected to the
                          require ports of the corresponding SW-C with
                          the RteAssistant.  Therefore it is advisable
                          to take the real SW-C name to make these
                          connection process easy.&lt;/p&gt;
                        &lt;/html&gt;</L-2>
              </DESC>
              <LOWER-MULTIPLICITY>0</LOWER-MULTIPLICITY>
              <UPPER-MULTIPLICITY-INFINITE>1</UPPER-MULTIPLICITY-INFINITE>
              <PARAMETERS>
                <ECUC-INTEGER-PARAM-DEF>
                  <SHORT-NAME>ModuleId</SHORT-NAME>
                  <DESC>
                    <L-2 L="FOR-ALL">&lt;html&gt;
                            &lt;p&gt;This parameter defines the unique module ID of
                            the Software Component.&lt;/p&gt;

                            &lt;p&gt;Values in the range of 0..255 are
                            reserved for Basic Software Modules. Valid
                            module IDs for external applications range
                            from 256..65535.&lt;/p&gt;
                          &lt;/html&gt;</L-2>
                  </DESC>
                  <IMPLEMENTATION-CONFIG-CLASSES>
                    <ECUC-IMPLEMENTATION-CONFIGURATION-CLASS>
                      <CONFIG-CLASS>PRE-COMPILE</CONFIG-CLASS>
                      <CONFIG-VARIANT>VARIANT-PRE-COMPILE</CONFIG-VARIANT>
                    </ECUC-IMPLEMENTATION-CONFIGURATION-CLASS>
                  </IMPLEMENTATION-CONFIG-CLASSES>
                  <ORIGIN>Elektrobit Automotive GmbH</ORIGIN>
                  <!-- @Calculated /TS_TxDxM6I3R0/Det/SoftwareComponentList/ModuleId: DEFAULT(XPath: num:i(../@index + 256))-->
                  <MAX>65535</MAX>
                  <MIN>256</MIN>
                </ECUC-INTEGER-PARAM-DEF>
              </PARAMETERS>
            </ECUC-PARAM-CONF-CONTAINER-DEF>
          </CONTAINERS>
        </ECUC-MODULE-DEF>
      </ELEMENTS>
    </AR-PACKAGE>
  </AR-PACKAGES>
</AUTOSAR>
