# \file
#
# \brief AUTOSAR PduR
#
# This file contains the implementation of the AUTOSAR
# module PduR.
#
# \author Elektrobit Automotive GmbH, 91058 Erlangen, Germany
#
# Copyright 2005 - 2013 Elektrobit Automotive GmbH
# All rights exclusively reserved for Elektrobit Automotive GmbH,
# unless expressly agreed to otherwise.

#################################################################
# DEFINITIONS

ifndef TS_MERGED_COMPILE
TS_MERGED_COMPILE := TRUE
endif

ifndef TS_PDUR_MERGED_COMPILE
TS_PDUR_MERGED_COMPILE := $(TS_MERGED_COMPILE)
endif

ifndef TS_PDUR_COMPILE_WITH_POSTBUILD
TS_PDUR_COMPILE_WITH_POSTBUILD := TRUE
endif

ifndef TS_BUILD_POST_BUILD_BINARY
TS_BUILD_POST_BUILD_BINARY := FALSE
endif

#################################################################
# REGISTRY

PduR_src_FILES_static      += \
   $(PduR_CORE_PATH)\src\PduR_GetVersionInfo.c \
   $(PduR_CORE_PATH)\src\PduR_GateIfRxIndicationDf.c \
   $(PduR_CORE_PATH)\src\PduR_GateIfTxConfirmationDf.c \
   $(PduR_CORE_PATH)\src\PduR_GateIfRxIndicationTf.c \
   $(PduR_CORE_PATH)\src\PduR_GateIfTriggerTransmitTf.c \
   $(PduR_CORE_PATH)\src\PduR_GateIfTxConfirmationTf.c \
   $(PduR_CORE_PATH)\src\PduR_GateIfRxIndicationSb.c \
   $(PduR_CORE_PATH)\src\PduR_GateIfTriggerTransmitSb.c \
   $(PduR_CORE_PATH)\src\PduR_GateIfRxIndicationNb.c \
   $(PduR_CORE_PATH)\src\PduR_GateTp.c \
   $(PduR_CORE_PATH)\src\PduR_GateTpUpReception.c \
   $(PduR_CORE_PATH)\src\PduR_GateTpStartOfReception.c \
   $(PduR_CORE_PATH)\src\PduR_GateTpCopyRxData.c \
   $(PduR_CORE_PATH)\src\PduR_GateTpRxIndication.c \
   $(PduR_CORE_PATH)\src\PduR_GateTpCopyTxData.c \
   $(PduR_CORE_PATH)\src\PduR_GateTpTxConfirmation.c \
   $(PduR_CORE_PATH)\src\PduR_MultiUpIfTransmit.c \
   $(PduR_CORE_PATH)\src\PduR_MultiIfRxIndication.c \
   $(PduR_CORE_PATH)\src\PduR_MultiTpCopyTxData.c \
   $(PduR_CORE_PATH)\src\PduR_MultiTpTxConfirmation.c \
   $(PduR_CORE_PATH)\src\PduR_MultiTpTransmit.c \
   $(PduR_CORE_PATH)\src\PduR_MultiTp.c \
   $(PduR_CORE_PATH)\src\PduR_GenericIfRxIndication.c \
   $(PduR_CORE_PATH)\src\PduR_GenericIfTransmit.c \
   $(PduR_CORE_PATH)\src\PduR_GenericIfTriggerTransmit.c \
   $(PduR_CORE_PATH)\src\PduR_GenericIfTxConfirmation.c \
   $(PduR_CORE_PATH)\src\PduR_GenericTpChangeParameter.c \
   $(PduR_CORE_PATH)\src\PduR_GenericTpCancelTransmit.c \
   $(PduR_CORE_PATH)\src\PduR_GenericTpCancelReceive.c \
   $(PduR_CORE_PATH)\src\PduR_GenericTpStartOfReception.c \
   $(PduR_CORE_PATH)\src\PduR_GenericTpCopyRxData.c \
   $(PduR_CORE_PATH)\src\PduR_GenericTpRxIndication.c \
   $(PduR_CORE_PATH)\src\PduR_GenericTpCopyTxData.c \
   $(PduR_CORE_PATH)\src\PduR_GenericTpTxConfirmation.c \
   $(PduR_CORE_PATH)\src\PduR_GenericTpTransmit.c \
   $(PduR_CORE_PATH)\src\PduR_Init.c \
   $(PduR_CORE_PATH)\src\PduR_IsValidConfig.c \
   $(PduR_CORE_PATH)\src\PduR_WrapASR32.c \
   $(PduR_CORE_PATH)\src\PduR_GetConfigurationId.c

# Static source dynamic pay load
PduR_src_FILES_static += \
   $(PduR_CORE_PATH)\src\PduR_GateIfTriggerTransmitSbDynPyld.c \
   $(PduR_CORE_PATH)\src\PduR_GateIfRxIndicationSbDynPyld.c \
   $(PduR_CORE_PATH)\src\PduR_GateIfTxConfirmationDfDynPyld.c \
   $(PduR_CORE_PATH)\src\PduR_GateIfRxIndicationDfDynPyld.c \
   $(PduR_CORE_PATH)\src\PduR_GateIfTriggerTransmitTfDynPyld.c \
   $(PduR_CORE_PATH)\src\PduR_GateIfTxConfirmationTfDynPyld.c \
   $(PduR_CORE_PATH)\src\PduR_GateIfRxIndicationTfDynPyld.c


LIBRARIES_TO_BUILD += PduR_src

PduR_lib_FILES +=


ifeq ($(TS_PDUR_MERGED_COMPILE),TRUE)
# Merged file compile
PduR_src_FILES := $(PduR_CORE_PATH)\src\PduR_Merged.c
else
# Single file compile
PduR_src_FILES := $(PduR_CORE_PATH)\src\PduR.c
PduR_src_FILES += $(PduR_src_FILES_static)
endif

ifeq ($(TS_PDUR_COMPILE_WITH_POSTBUILD),TRUE)
# Compile with post build
PduR_src_FILES += $(PduR_OUTPUT_PATH)/src/PduR_PBcfg.c
endif

# Add generated files to the list of source files
PduR_src_FILES += $(PduR_OUTPUT_PATH)/src/PduR_AdjLayerApi.c
PduR_src_FILES += $(PduR_OUTPUT_PATH)/src/PduR_Lcfg.c

ifeq ($(TS_BUILD_POST_BUILD_BINARY),TRUE)
# If the post build binary file shall be built do not compile any files other then the post build files.
PduR_src_FILES :=
endif

# Fill the list with post build configuration files needed to build the post build binary.
PduR_pbconfig_FILES := $(PduR_OUTPUT_PATH)/src/PduR_PBcfg.c

define definePduRLibOutputPATH
$(1)_OBJ_OUTPUT_PATH    := $(PduR_lib_LIB_OUTPUT_PATH)
endef

$(foreach SRC,$(basename $(notdir $(subst \,/,$(PduR_lib_FILES)))),$(eval $(call definePduRLibOutputPATH,$(SRC))))

#################################################################
# DEPENDENCIES (only for assembler files)
#

#################################################################
# RULES
