
#ifndef CAN_17_MCANP_IRQ_H
#define CAN_17_MCANP_IRQ_H

/*******************************************************************************
**                      Includes                                              **
*******************************************************************************/

#include "Std_Types.h"                                                        

#ifdef OS_KERNEL_TYPE                                                           
#include <Os.h>        /* OS interrupt services */                              
#endif


/*******************************************************************************
**                      Public Macro Definitions                              **
*******************************************************************************/

/************************* interrupt CATEGORY *********************************/

#ifdef IRQ_CAT1
#if (IRQ_CAT1 != 1)
#error IRQ_CAT1 already defined with a wrong value
#endif
#else
#define IRQ_CAT1                    (1)
#endif

#ifdef IRQ_CAT23
#if (IRQ_CAT23 != 2)
#error IRQ_CAT23 already defined with a wrong value
#endif
#else
#define IRQ_CAT23                    (2)
#endif



/* The name of the ISRs shall be the same name than the ISR       *
 * functions provided by Infineon                                 */

/*************************CANSR0_ISR*********************************/          

#ifdef CANSR0_ISR
#define IRQ_CAN0_EXIST      STD_ON
#define IRQ_CAN_SR0_PRIO    CANSR0_ISR_ISR_LEVEL
#define IRQ_CAN_SR0_CAT     CANSR0_ISR_ISR_CATEGORY
#else
#define IRQ_CAN0_EXIST      STD_OFF
#endif

/*************************CANSR1_ISR*********************************/          

#ifdef CANSR1_ISR
#define IRQ_CAN1_EXIST      STD_ON
#define IRQ_CAN_SR1_PRIO    CANSR1_ISR_ISR_LEVEL
#define IRQ_CAN_SR1_CAT     CANSR1_ISR_ISR_CATEGORY
#else
#define IRQ_CAN1_EXIST      STD_OFF
#endif

/*************************CANSR2_ISR*********************************/          

#ifdef CANSR2_ISR
#define IRQ_CAN2_EXIST      STD_ON
#define IRQ_CAN_SR2_PRIO    CANSR2_ISR_ISR_LEVEL
#define IRQ_CAN_SR2_CAT     CANSR2_ISR_ISR_CATEGORY
#else
#define IRQ_CAN2_EXIST      STD_OFF
#endif

/*************************CANSR3_ISR*********************************/          

#ifdef CANSR3_ISR
#define IRQ_CAN3_EXIST      STD_ON
#define IRQ_CAN_SR3_PRIO    CANSR3_ISR_ISR_LEVEL
#define IRQ_CAN_SR3_CAT     CANSR3_ISR_ISR_CATEGORY
#else
#define IRQ_CAN3_EXIST      STD_OFF
#endif

/*************************CANSR4_ISR*********************************/          

#ifdef CANSR4_ISR
#define IRQ_CAN4_EXIST      STD_ON
#define IRQ_CAN_SR4_PRIO    CANSR4_ISR_ISR_LEVEL
#define IRQ_CAN_SR4_CAT     CANSR4_ISR_ISR_CATEGORY
#else
#define IRQ_CAN4_EXIST      STD_OFF
#endif

/*************************CANSR5_ISR*********************************/          

#ifdef CANSR5_ISR
#define IRQ_CAN5_EXIST      STD_ON
#define IRQ_CAN_SR5_PRIO    CANSR5_ISR_ISR_LEVEL
#define IRQ_CAN_SR5_CAT     CANSR5_ISR_ISR_CATEGORY
#else
#define IRQ_CAN5_EXIST      STD_OFF
#endif

/*************************CANSR6_ISR*********************************/          

#ifdef CANSR6_ISR
#define IRQ_CAN6_EXIST      STD_ON
#define IRQ_CAN_SR6_PRIO    CANSR6_ISR_ISR_LEVEL
#define IRQ_CAN_SR6_CAT     CANSR6_ISR_ISR_CATEGORY
#else
#define IRQ_CAN6_EXIST      STD_OFF
#endif

/*************************CANSR7_ISR*********************************/          

#ifdef CANSR7_ISR
#define IRQ_CAN7_EXIST      STD_ON
#define IRQ_CAN_SR7_PRIO    CANSR7_ISR_ISR_LEVEL
#define IRQ_CAN_SR7_CAT     CANSR7_ISR_ISR_CATEGORY
#else
#define IRQ_CAN7_EXIST      STD_OFF
#endif

/*************************CANSR8_ISR*********************************/          

#ifdef CANSR8_ISR
#define IRQ_CAN8_EXIST      STD_ON
#define IRQ_CAN_SR8_PRIO    CANSR8_ISR_ISR_LEVEL
#define IRQ_CAN_SR8_CAT     CANSR8_ISR_ISR_CATEGORY
#else
#define IRQ_CAN8_EXIST      STD_OFF
#endif

/*************************CANSR9_ISR*********************************/          

#ifdef CANSR9_ISR
#define IRQ_CAN9_EXIST      STD_ON
#define IRQ_CAN_SR9_PRIO    CANSR9_ISR_ISR_LEVEL
#define IRQ_CAN_SR9_CAT     CANSR9_ISR_ISR_CATEGORY
#else
#define IRQ_CAN9_EXIST      STD_OFF
#endif

/*************************CANSR10_ISR*********************************/          

#ifdef CANSR10_ISR
#define IRQ_CAN10_EXIST      STD_ON
#define IRQ_CAN_SR10_PRIO    CANSR10_ISR_ISR_LEVEL
#define IRQ_CAN_SR10_CAT     CANSR10_ISR_ISR_CATEGORY
#else
#define IRQ_CAN10_EXIST      STD_OFF
#endif

/*************************CANSR11_ISR*********************************/          

#ifdef CANSR11_ISR
#define IRQ_CAN11_EXIST      STD_ON
#define IRQ_CAN_SR11_PRIO    CANSR11_ISR_ISR_LEVEL
#define IRQ_CAN_SR11_CAT     CANSR11_ISR_ISR_CATEGORY
#else
#define IRQ_CAN11_EXIST      STD_OFF
#endif

/*************************CANSR16_ISR*********************************/          

#ifdef CANSR16_ISR
#define IRQ_CAN16_EXIST      STD_ON
#define IRQ_CAN_SR16_PRIO    CANSR16_ISR_ISR_LEVEL
#define IRQ_CAN_SR16_CAT     CANSR16_ISR_ISR_CATEGORY
#else
#define IRQ_CAN16_EXIST      STD_OFF
#endif

/*************************CANSR17_ISR*********************************/          

#ifdef CANSR17_ISR
#define IRQ_CAN17_EXIST      STD_ON
#define IRQ_CAN_SR17_PRIO    CANSR17_ISR_ISR_LEVEL
#define IRQ_CAN_SR17_CAT     CANSR17_ISR_ISR_CATEGORY
#else
#define IRQ_CAN17_EXIST      STD_OFF
#endif

/*************************CANSR18_ISR*********************************/          

#ifdef CANSR18_ISR
#define IRQ_CAN18_EXIST      STD_ON
#define IRQ_CAN_SR18_PRIO    CANSR18_ISR_ISR_LEVEL
#define IRQ_CAN_SR18_CAT     CANSR18_ISR_ISR_CATEGORY
#else
#define IRQ_CAN18_EXIST      STD_OFF
#endif

/*************************CANSR19_ISR*********************************/          

#ifdef CANSR19_ISR
#define IRQ_CAN19_EXIST      STD_ON
#define IRQ_CAN_SR19_PRIO    CANSR19_ISR_ISR_LEVEL
#define IRQ_CAN_SR19_CAT     CANSR19_ISR_ISR_CATEGORY
#else
#define IRQ_CAN19_EXIST      STD_OFF
#endif

/*************************CANSR20_ISR*********************************/          

#ifdef CANSR20_ISR
#define IRQ_CAN20_EXIST      STD_ON
#define IRQ_CAN_SR20_PRIO    CANSR20_ISR_ISR_LEVEL
#define IRQ_CAN_SR20_CAT     CANSR20_ISR_ISR_CATEGORY
#else
#define IRQ_CAN20_EXIST      STD_OFF
#endif

/*************************CANSR21_ISR*********************************/          

#ifdef CANSR21_ISR
#define IRQ_CAN21_EXIST      STD_ON
#define IRQ_CAN_SR21_PRIO    CANSR21_ISR_ISR_LEVEL
#define IRQ_CAN_SR21_CAT     CANSR21_ISR_ISR_CATEGORY
#else
#define IRQ_CAN21_EXIST      STD_OFF
#endif

#endif /* #ifndef CAN_17_MCANP_IRQ_H */

