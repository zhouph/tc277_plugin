/**
 * \file
 *
 * \brief AUTOSAR FrSM
 *
 * This file contains the implementation of the AUTOSAR
 * module FrSM.
 *
 * \author Elektrobit Automotive GmbH, 91058 Erlangen, Germany
 *
 * Copyright 2005 - 2013 Elektrobit Automotive GmbH
 * All rights exclusively reserved for Elektrobit Automotive GmbH,
 * unless expressly agreed to otherwise.
 */

/*==================[includes]==============================================*/

#include <FrSM_Lcfg.h>
#include <FrSM_Int.h>

/*==================[external data]=========================================*/

#define FRSM_START_SEC_VAR_NO_INIT_UNSPECIFIED
#include <MemMap.h>  /* !LINKSTO FrSm.ASR40.FrSm057,1 */

VAR(FrSM_ClstRuntimeDataType,FRSM_VAR_NOINIT) FrSM_ClstRuntimeData[[!"num:integer(count(FrSMConfig/*[1]/FrSMCluster/*))"!]U];

#define FRSM_STOP_SEC_VAR_NO_INIT_UNSPECIFIED
#include <MemMap.h>  /* !LINKSTO FrSm.ASR40.FrSm057,1 */

/*==================[external function definitions]=========================*/

#define FRSM_START_SEC_CODE
#include <MemMap.h>  /* !LINKSTO FrSm.ASR40.FrSm057,1 */

[!VAR "LoopCounter" = "0"!][!//
[!LOOP "FrSMConfig/*[1]/FrSMCluster/*"!]
/* FrSM_MainFunction_[!"as:ref(./FrSMFrIfClusterRef)/FrIfClstIdx"!]() implementation */
FUNC(void,FRSM_CODE) FrSM_MainFunction_[!"as:ref(./FrSMFrIfClusterRef)/FrIfClstIdx"!](void)
{
    FrSM_MainFunction([!"num:integer($LoopCounter)"!]U);[!// Translate FrIfClstIdx to FrSM index
}
[!VAR "LoopCounter"="$LoopCounter + 1"!][!//
[!ENDLOOP!]

#define FRSM_STOP_SEC_CODE
#include <MemMap.h>  /* !LINKSTO FrSm.ASR40.FrSm057,1 */

/*==================[end of file]===========================================*/
